package innasoft.com.mhcone.fragments;



import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.CountrycodeActivity;
import innasoft.com.mhcone.activities.SubTestsActivity;
import innasoft.com.mhcone.adapters.TestsAdapter;
import innasoft.com.mhcone.holders.TestsHolder;
import innasoft.com.mhcone.models.TestsModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.ConnectionDetector;
import innasoft.com.mhcone.utilities.GPSTracker;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.UserSessionManager;


/**
 * A simple {@link Fragment} subclass.
 */
public class TestsFragment extends Fragment implements TestsHolder.MyViewHolder, View.OnClickListener {
    ProgressDialog pDialog;
    ArrayList<TestsModel> testsList;
    TestsAdapter adapter;
    RecyclerView testrecyclerView;
    Button gridButton,listButton;
    public   Dialog dialog;
    FloatingActionButton fab;

    UserSessionManager userSessionManager;

    NetworkStatus ns;
    Boolean isOnline = false;

     Intent intent ;

    TextView defaultTExt,noInternetConnection;

    Button retryAfterInernetConnected;


    ConnectionDetector cd;
    Boolean isInternetPresent = false;
    Typeface typeface;


    public TestsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_tests, container, false);
        testsList = new ArrayList<TestsModel>();

        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

        userSessionManager = new UserSessionManager(getActivity());

        gridButton = (Button) view.findViewById(R.id.selectGridView);
        listButton = (Button) view.findViewById(R.id.selectLinear);

        typeface = Typeface.createFromAsset(getActivity().getAssets(), "latoregular.ttf");

        defaultTExt = (TextView)view.findViewById(R.id.defaultText);
        noInternetConnection = (TextView)view.findViewById(R.id.noInternetConnect);
        retryAfterInernetConnected = (Button)view.findViewById(R.id.retryAfterInternetConnected);


        fab = (FloatingActionButton) view.findViewById(R.id.fab);

        ns = new NetworkStatus();
        isOnline = ns.isOnline(getActivity());

        testrecyclerView = (RecyclerView) view.findViewById(R.id.tests_recycler_view);
        //testrecyclerView.setHasFixedSize(true);
       // RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        adapter = new TestsAdapter(testsList , TestsFragment.this, R.layout.row_tests_items);
        testrecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //final TrendringAdapter adapter1=new TrendringAdapter(this,topicList);
        //testrecyclerView.setAdapter(adapter);

        intent = new Intent(getActivity(), CountrycodeActivity.class);

        pDialog = new ProgressDialog(getActivity());

        pDialog.setMessage("Please wait...");

        //pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setProgressStyle(R.style.DialogTheme);
        pDialog.setCancelable(false);


        if(isOnline)
        {
            getttingTestsList();

            gridButton.setOnClickListener(this);
            listButton.setOnClickListener(this);
            // set Click listener
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // creating custom Floating Action button
                    customDialog();
                }
            });
        }

        else {
            pDialog.dismiss();

            showInternetStatus();
        }

        /*if(isInternetPresent) {
            getttingTestsList();

            gridButton.setOnClickListener(this);
            listButton.setOnClickListener(this);
            // set Click listener
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // creating custom Floating Action button
                    customDialog();
                }
            });
        }
        else {
            pDialog.dismiss();

            showInternetStatus();
        }

*/
        return view;
    }

    private void showInternetStatus() {
        noInternetConnection.setTypeface(typeface);
        noInternetConnection.setVisibility(View.VISIBLE);
        retryAfterInernetConnected.setVisibility(View.VISIBLE);

        /*AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("No Internet connection.");
        builder.setMessage("You have no internet connection");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });
        builder.show();*/
    }

    public void customDialog(){
        fab.setVisibility(View.INVISIBLE);

        dialog = new Dialog(getActivity());
        // it remove the dialog title
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // set the laytout in the dialog
        dialog.setContentView(R.layout.dialogbox);
        //dialog.setCancelable(false);
        // set the background partial transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        Window window = dialog.getWindow();
        WindowManager.LayoutParams param = window.getAttributes();
        // set the layout at right bottom
        param.gravity = Gravity.BOTTOM | Gravity.RIGHT;
        // it dismiss the dialog when click outside the dialog frame
        dialog.setCanceledOnTouchOutside(false);
        // initialize the item of the dialog box, whose id is demo1
        View demodialog =(View) dialog.findViewById(R.id.cross);
        /*View one = dialog.findViewById(R.id.demo1);
        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "HELLO", Toast.LENGTH_SHORT).show();
            }
        });*/
        View gridView = dialog.findViewById(R.id.gridLayout);
        gridView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab.setVisibility(View.VISIBLE);
                adapter = new TestsAdapter(testsList , TestsFragment.this, R.layout.row_tests_items_grid);
                GridLayoutManager mGridLayoutManager = new GridLayoutManager(getActivity(), 2); // (Context context, int spanCount)
                testrecyclerView.setLayoutManager(mGridLayoutManager);
                testrecyclerView.setAdapter(adapter);
                dialog.dismiss();
            }
        });
        View listView = dialog.findViewById(R.id.listView);
        listView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab.setVisibility(View.VISIBLE);
                adapter = new TestsAdapter(testsList , TestsFragment.this, R.layout.row_tests_items);
                LinearLayoutManager mLinearLayoutManagerVertical = new LinearLayoutManager(getActivity()); // (Context context)
                mLinearLayoutManagerVertical.setOrientation(LinearLayoutManager.VERTICAL);
                testrecyclerView.setLayoutManager(mLinearLayoutManagerVertical);
                testrecyclerView.setAdapter(adapter);
                dialog.dismiss();
            }
        });
        // it call when click on the item whose id is demo1.
        demodialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // diss miss the dialog
                fab.setVisibility(View.VISIBLE);
                dialog.dismiss();
            }
        });

        // it show the dialog box
        dialog.show();

    }

    private void getttingTestsList() {


        if(isInternetPresent) {
            pDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.TESTS_URL,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            testsList.clear();
                            noInternetConnection.setVisibility(View.INVISIBLE);
                            //tv_no_recordfound.setVisibility(View.INVISIBLE);
                            Log.d("Purushotham", "Sucesses-TestsList : " + response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                if(jsonArray.length() !=0 ) {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("test");
                                        TestsModel tm = new TestsModel();
                                        tm.setTest_id(jsonObject1.getString("id"));
                                        Log.d("Killer", AppUrls.BASE_URL + jsonObject1.getString("test_image"));
                                        tm.setTest_image(AppUrls.BASE_URL + jsonObject1.getString("test_image"));
                                        tm.setTest_name(jsonObject1.getString("test_name"));

                                        testsList.add(tm);
                                    }
                                    testrecyclerView.setAdapter(adapter);
                                    pDialog.dismiss();
                                }
                                else {
                                    pDialog.dismiss();
                                    noInternetConnection.setTypeface(typeface);
                                    noInternetConnection.setText("No Tests Found");
                                    noInternetConnection.setVisibility(View.VISIBLE);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    pDialog.dismiss();


                                }
                            });


                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }


                        }
                    }
            ) {

            };
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        }else {
            showInternetStatus();
        }
    }

    @Override
    public void cardViewListener(int position) {
        //if(gpsTracker.getIsGPSTrackingEnabled()) {


        if(userSessionManager.checkLogin() != false)
        {
         Toast.makeText(getActivity(), "Please Login...!", Toast.LENGTH_SHORT).show();
        }
        else {
            Intent intent = new Intent(getActivity(), SubTestsActivity.class);
            intent.putExtra("test_id", testsList.get(position));
            startActivity(intent);
        }

      //  startActivityForResult(intent, 1);


        /*}else {
            gpsTracker.showSettingsAlert();
        }*/


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == Activity.RESULT_OK){
            String countryCode = data.getStringExtra(CountrycodeActivity.RESULT_CONTRYCODE);
            String countyName = data.getStringExtra(CountrycodeActivity.COUNTARY_NAME);
            Toast.makeText(getActivity(), "You selected countrycode: " + countryCode+",\n Name : "+countyName, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if(view == gridButton)
        {
            adapter = new TestsAdapter(testsList , TestsFragment.this, R.layout.row_tests_items_grid);
            GridLayoutManager mGridLayoutManager = new GridLayoutManager(getActivity(), 2); // (Context context, int spanCount)
            testrecyclerView.setLayoutManager(mGridLayoutManager);
            testrecyclerView.setAdapter(adapter);

        }
        if(view == listButton)
        {
            adapter = new TestsAdapter(testsList , TestsFragment.this, R.layout.row_tests_items);
            LinearLayoutManager mLinearLayoutManagerVertical = new LinearLayoutManager(getActivity()); // (Context context)
            mLinearLayoutManagerVertical.setOrientation(LinearLayoutManager.VERTICAL);
            testrecyclerView.setLayoutManager(mLinearLayoutManagerVertical);
            testrecyclerView.setAdapter(adapter);
        }
    }
}
