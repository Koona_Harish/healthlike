package innasoft.com.mhcone.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.BillingPageForLabBooking;
import innasoft.com.mhcone.adapters.BookAppointmentAdapter;
import innasoft.com.mhcone.adapters.FourthFragCitiesAdapter;
import innasoft.com.mhcone.models.BookAppointmentModel;
import innasoft.com.mhcone.models.CitiesModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.DividerItemDecoration;
import innasoft.com.mhcone.utilities.GPSTrackers;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.StartLocationAlert;
import innasoft.com.mhcone.utilities.UserSessionManager;
import innasoft.com.mhcone.utilities.VerticalSpaceItemDecoration;

public class FourthFrag extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private View rootView;

    ArrayList<String> labArrayList = new ArrayList<String>();

    ArrayList<BookAppointmentModel> showlabslistList = new ArrayList<BookAppointmentModel>();
    BookAppointmentAdapter adapter;
    RecyclerView showlabslistrecyclerview;
    StartLocationAlert startLocationAlert;
    UserSessionManager userSessionManager;
    int flag;
    private Object labsList;
    private FloatingActionButton fabPackageLabs;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    String userID;
    Typeface typeface;
    TextView defaultTExt, noInternetConnection;
    String status_payment_lab, status_payment_online, status_tele_booking;
    SearchView searchView;
    NetworkStatus ns, ns2, ns3, ns4, ns5;
    Boolean isOnline = false, isOnline2 = false, isOnline3 = false, isOnline4 = false, isOnline5 = false;
    GPSTrackers gps, gps1;
    String lat, lng;
    ProgressDialog progressDialog;
    int prograssStaticValue = 0;
    private Typeface type_lato;
    RelativeLayout rl;
    TextView selectCity;
    ImageView selectLocation;


    Geocoder geocoder;
    List<Address> cuurentaddresses = new ArrayList<Address>();
    String address = null;
    String city;
    String state;
    String country;
    String pincode;
    double latitude;
    double longitude;

    int first_time_flag = 0;
    int filterFlag = 0;

    private static final int VERTICAL_ITEM_SPACE = 0;


        /*Cities*/

    ArrayList<CitiesModel> citiesModelArrayList = new ArrayList<CitiesModel>();
    FourthFragCitiesAdapter citiesAdapter;
    AlertDialog dialog;

    public FourthFrag() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        type_lato = Typeface.createFromAsset(getContext().getAssets(), "latoregular.ttf");

        gps = new GPSTrackers(getContext());


        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> user = userSessionManager.getUserDetails();

        userID = user.get(UserSessionManager.USER_ID);
        rootView = inflater.inflate(R.layout.fragment_fourth, container, false);
        ns = new NetworkStatus();
        isOnline = ns.isOnline(getActivity());

        fabPackageLabs = (FloatingActionButton) rootView.findViewById(R.id.filter_labs_list);
        fabPackageLabs.setOnClickListener(this);

        rl = (RelativeLayout) rootView.findViewById(R.id.location_rl);
        rl.setOnClickListener(this);

        selectCity = (TextView) rootView.findViewById(R.id.select_city);
        selectCity.setOnClickListener(this);

        selectLocation = (ImageView) rootView.findViewById(R.id.select_location);
        selectLocation.setOnClickListener(this);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.fourth_frag_swipe_layout);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,

                android.R.color.holo_green_light,

                android.R.color.holo_orange_light,

                android.R.color.holo_red_light);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        searchView = (SearchView) rootView.findViewById(R.id.mSearchlabs_fragment_fourth);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });

        defaultTExt = (TextView) rootView.findViewById(R.id.defaultText_b);
        noInternetConnection = (TextView) rootView.findViewById(R.id.noInternetConnect_b);

        if (isOnline) {
            if (gps.canGetLocation()) {

                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
                geocoder = new Geocoder(getActivity(), Locale.getDefault());

                try {

                    cuurentaddresses = geocoder.getFromLocation(latitude, longitude, 1);

                } catch (IOException e) {
                    e.printStackTrace();
                }

                lat = String.valueOf(latitude);
                lng = String.valueOf(longitude);
                showlabslistrecyclerview = (RecyclerView) rootView.findViewById(R.id.showlabslist_recycler_view_b);
                showlabslistrecyclerview.setHasFixedSize(true);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                showlabslistrecyclerview.setLayoutManager(layoutManager);

                adapter = new BookAppointmentAdapter(showlabslistList, FourthFrag.this, R.layout.row_book_appointment_items4);


                showlabslistrecyclerview.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
                showlabslistrecyclerview.addItemDecoration(new DividerItemDecoration(getActivity()));
                showlabslistrecyclerview.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.recycler_divider));

                if (filterFlag == 0) {
                    String filterString = "distance_in_km asc";
                    getLabsList(filterString);
                }
                if (cuurentaddresses.size() != 0) {

                    city = cuurentaddresses.get(0).getLocality();
                    selectCity.setText(city);
                }

            } else {
                startLocationAlert = new StartLocationAlert(getActivity());
            }

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String query) {

                    adapter.getFilter().filter(query);
                    return false;
                }
            });
        } else {
            showInternetStatus2();
        }
        mSwipeRefreshLayout.setOnRefreshListener(this);
        return rootView;
    }

    private void showInternetStatus2() {
        progressDialog.dismiss();
        noInternetConnection.setTypeface(type_lato);
        noInternetConnection.setVisibility(View.VISIBLE);

    }

    private void showInternetStatus() {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
        TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
        textView.setTypeface(type_lato);
        Toast toast = new Toast(getActivity());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(toastLayout);
        toast.show();
    }

    public void cardViewListener(int position) {

        final int pos;
        pos = position;

        if (userSessionManager.checkLogin() != false) {
            Toast.makeText(getActivity(), "Please Login...!", Toast.LENGTH_SHORT).show();
        } else {

            final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    progressDialog.dismiss();
                    Log.d("Purushotham ", "Account CHECK in FROURTHFRAG BBOKLAB" + response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String responceCode = jsonObject.getString("status");
                        if (responceCode.equals("19999")) {
                            Intent intent = new Intent(getActivity(), BillingPageForLabBooking.class);
                            intent.putExtra("selectedLabPhone", showlabslistList.get(pos).getShow_lab_book_mobile());
                            intent.putExtra("selectedLabId", showlabslistList.get(pos).getShow_lab_book_id());//selectedLabId,selectedLabName
                            intent.putExtra("selectedLabName", showlabslistList.get(pos).getShow_lab_book_user_name());
                            intent.putExtra("payment_lab", showlabslistList.get(pos).getShow_lab_payment_lab());
                            intent.putExtra("payment_online", showlabslistList.get(pos).getShow_lab_payment_online());
                            intent.putExtra("tele_booking", showlabslistList.get(pos).getShow_lab_tele_booking());
                            startActivity(intent);
                        }

                        if (responceCode.equals("10140")) {
                            userSessionManager.logoutUser();
                            Toast.makeText(getActivity(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            startActivity(intent);
                        }

                        if (responceCode.equals("10150")) {
                            userSessionManager.logoutUser();
                            Toast.makeText(getActivity(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            startActivity(intent);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", userID);
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        }
    }

    private void getLabsList(final String filterValue, final String filterValue2) {
        showlabslistList.clear();
        noInternetConnection.setVisibility(View.GONE);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GETTING_LABS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    if (jsonArray.length() != 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");

                            BookAppointmentModel sllm = new BookAppointmentModel();

                            sllm.setShow_lab_book_id(jsonObject1.getString("id"));
                            sllm.setShow_lab_book_user_name(jsonObject1.getString("user_name"));
                            sllm.setShow_lab_book_email(jsonObject1.getString("email"));
                            sllm.setShow_lab_book_mobile(jsonObject1.getString("mobile"));
                            sllm.setShow_lab_book_role_id(jsonObject1.getString("role_id"));
                            sllm.setShow_lab_book_created_time(jsonObject1.getString("created_time"));
                            sllm.setCertificate(jsonObject1.getString("certificate"));
                            sllm.setLocation(jsonObject1.getString("location"));
                            if (jsonObject1.getString("rating").equals("null")) {
                                sllm.setShow_lab_ratting("0");
                            } else {
                                sllm.setShow_lab_ratting(jsonObject1.getString("rating"));
                            }
                            sllm.setShow_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            sllm.setShow_lab_payment_online(jsonObject1.getString("payment_online"));
                            sllm.setShow_lab_tele_booking(jsonObject1.getString("tele_booking"));
                            sllm.setProfile_pic_url(jsonObject1.getString("profile_pic"));
                            sllm.setShow_lab_book_address(jsonObject1.getString("address"));

                            String workingDayfromServer = jsonObject1.getString("current_week_name");
                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];

                            if (segments[2].equals("1")) {
                                sllm.setShow_lab_book_workingdays("Open Today:" + segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (segments[2].equals("0")) {
                                sllm.setShow_lab_book_workingdays("Today lab Close");
                            }

                            sllm.setShow_lab_book_lat(jsonObject1.getString("lat"));
                            sllm.setShow_lab_book_lang(jsonObject1.getString("lang"));
                            sllm.setShow_lab_book_home_collections(jsonObject1.getString("home_collections"));
                            sllm.setShow_lab_book_distance(jsonObject1.getString("distance_in_km"));

                            showlabslistList.add(sllm);

                        }

                        showlabslistrecyclerview.setAdapter(adapter);
                        mSwipeRefreshLayout.setRefreshing(false);
                        progressDialog.dismiss();
                    } else {
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                        noInternetConnection.setTypeface(type_lato);
                        noInternetConnection.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnection.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("lat", lat);
                params.put("lang", lng);
                params.put("order_type", filterValue);
                params.put("home_collections", filterValue2);
                if (first_time_flag == 1) {
                    params.put("city", city);
                }
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void getLabsList(final String filterValue) {
        showlabslistList.clear();
        noInternetConnection.setVisibility(View.GONE);
        if (prograssStaticValue == 1) {
            progressDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GETTING_LABS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");

                            BookAppointmentModel sllm = new BookAppointmentModel();

                            sllm.setShow_lab_book_id(jsonObject1.getString("id"));
                            sllm.setShow_lab_book_user_name(jsonObject1.getString("user_name"));
                            sllm.setShow_lab_book_email(jsonObject1.getString("email"));
                            sllm.setShow_lab_book_mobile(jsonObject1.getString("mobile"));
                            sllm.setShow_lab_book_role_id(jsonObject1.getString("role_id"));
                            sllm.setShow_lab_book_created_time(jsonObject1.getString("created_time"));
                            sllm.setCertificate(jsonObject1.getString("certificate"));
                            sllm.setLocation(jsonObject1.getString("location"));
                            if (jsonObject1.getString("rating").equals("null")) {
                                sllm.setShow_lab_ratting("0");
                            } else {
                                sllm.setShow_lab_ratting(jsonObject1.getString("rating"));
                            }
                            sllm.setShow_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            sllm.setShow_lab_payment_online(jsonObject1.getString("payment_online"));
                            sllm.setShow_lab_tele_booking(jsonObject1.getString("tele_booking"));
                            sllm.setProfile_pic_url(jsonObject1.getString("profile_pic"));
                            sllm.setShow_lab_book_address(jsonObject1.getString("address"));

                            String workingDayfromServer = jsonObject1.getString("current_week_name");
                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];
                            if (segments[2].equals("1")) {
                                sllm.setShow_lab_book_workingdays("Open Today:" + segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (segments[2].equals("0")) {
                                sllm.setShow_lab_book_workingdays("Today lab Close");
                            }

                            sllm.setShow_lab_book_lat(jsonObject1.getString("lat"));
                            sllm.setShow_lab_book_lang(jsonObject1.getString("lang"));
                            sllm.setShow_lab_book_home_collections(jsonObject1.getString("home_collections"));
                            sllm.setShow_lab_book_distance(jsonObject1.getString("distance_in_km"));

                            showlabslistList.add(sllm);

                        }
                        mSwipeRefreshLayout.setRefreshing(false);
                        progressDialog.dismiss();
                        showlabslistrecyclerview.setAdapter(adapter);

                    } else {
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                        Log.d("PLEASECHEKONCE", "200ERROR");
                        noInternetConnection.setTypeface(type_lato);
                        noInternetConnection.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnection.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.show();
                mSwipeRefreshLayout.setRefreshing(false);

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("lat", lat);
                params.put("lang", lng);
                params.put("order_type", filterValue);
                if (first_time_flag == 1) {
                    params.put("city", city);
                }
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {
        if (v == rl) {
            progressDialog.show();
            labArrayList.clear();

            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.LAB_LOCATIONS, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        citiesModelArrayList.clear();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            CitiesModel citiesModel = new CitiesModel();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("city");
                            String city_name = jsonObject1.getString("city_name");
                            citiesModel.setCity_name(city_name);
                            // labArrayList.add(city_name);

                            citiesModelArrayList.add(citiesModel);

                        }
                        alertLabList(citiesModelArrayList);


                        //alertLabList(labArrayList);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    String responseBody = null;

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        }


        if (v == selectCity) {
            ns3 = new NetworkStatus();
            isOnline3 = ns3.isOnline(getActivity());
            if (isOnline3) {
                progressDialog.show();
                labArrayList.clear();

                StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.LAB_LOCATIONS, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            citiesModelArrayList.clear();

                            for (int i = 0; i < jsonArray.length(); i++) {
                                CitiesModel citiesModel = new CitiesModel();
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("city");
                                String city_name = jsonObject1.getString("city_name");
                                citiesModel.setCity_name(city_name);
                                labArrayList.add(city_name);

                                citiesModelArrayList.add(citiesModel);

                            }
                            alertLabList(citiesModelArrayList);
                            //  alertLabList(labArrayList);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        String responseBody = null;

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {
                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {
                        } else if (error instanceof ParseError) {
                        }
                    }
                });

                RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                requestQueue.add(stringRequest);
            } else {
                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getActivity(), "No Internet Connection....", Toast.LENGTH_SHORT).show();
                showInternetStatus2();
                showInternetStatus2();
            }
        }

        if (v == selectLocation) {
            ns3 = new NetworkStatus();
            isOnline3 = ns3.isOnline(getActivity());
            if (isOnline3) {
                progressDialog.show();
                labArrayList.clear();

                StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.LAB_LOCATIONS, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            citiesModelArrayList.clear();

                            for (int i = 0; i < jsonArray.length(); i++) {
                                CitiesModel citiesModel = new CitiesModel();
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("city");
                                String city_name = jsonObject1.getString("city_name");
                                citiesModel.setCity_name(city_name);
                                labArrayList.add(city_name);

                                citiesModelArrayList.add(citiesModel);

                            }
                            alertLabList(citiesModelArrayList);
                            //  alertLabList(labArrayList);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        String responseBody = null;

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {
                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {
                        } else if (error instanceof ParseError) {
                        }
                    }
                });

                RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                requestQueue.add(stringRequest);
            } else {
                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getActivity(), "No Internet Connection....", Toast.LENGTH_SHORT).show();
                showInternetStatus2();
                showInternetStatus2();
            }
        }

        if (v == fabPackageLabs) {
            ns5 = new NetworkStatus();
            isOnline5 = ns5.isOnline(getActivity());
            if (isOnline5) {

                final String[] country = {"Distance Low to High", "Distance High to Low", "Names A to Z", "Names Z to A", "Home Collection"
                        , "Ratting High to Low", "Ratting Low to High", "NABL/CAP Accreditation"};

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setTitle("Choose One")
                        .setItems(country, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                String selectedCountry = country[i];
                                String cityName = selectCity.getText().toString();

                                if (selectedCountry.equals("Distance Low to High")) {

                                    prograssStaticValue = 1;
                                    String vlaue = "distance_in_km asc";
                                    gettingLabswithCityNamewithFilter(vlaue, cityName);

                                }
                                if (selectedCountry.equals("Distance High to Low")) {

                                    prograssStaticValue = 1;
                                    String vlaue = "distance_in_km desc";
                                    gettingLabswithCityNamewithFilter(vlaue, cityName);

                                }
                                if (selectedCountry.equals("Names A to Z")) {

                                    prograssStaticValue = 1;
                                    String vlaue = "users.user_name asc";
                                    gettingLabswithCityNamewithFilter(vlaue, cityName);

                                }
                                if (selectedCountry.equals("Names Z to A")) {

                                    prograssStaticValue = 1;
                                    String vlaue = "users.user_name desc";
                                    gettingLabswithCityNamewithFilter(vlaue, cityName);

                                }
                                if (selectedCountry.equals("Home Collection")) {

                                    String vlaue = "distance_in_km asc";
                                    String value2 = "lab_users.home_collections = 1";
                                    gettingLabswithCityNamewithFilterTwo(vlaue, value2, cityName);

                                }
                                if (selectedCountry.equals("NABL/CAP Accreditation")) {

                                    String vlaue = "distance_in_km asc";
                                    String value2 = "lab_users.certificate = 1";
                                    gettingLabswithCityNamewithFilterTwo(vlaue, value2, cityName);

                                }

                                if (selectedCountry.equals("Ratting High to Low")) {

                                    prograssStaticValue = 1;
                                    String vlaue = "rating desc";
                                    gettingLabswithCityNamewithFilter(vlaue, cityName);
                                }

                                if (selectedCountry.equals("Ratting Low to High")) {

                                    prograssStaticValue = 1;
                                    String vlaue = "rating asc";
                                    gettingLabswithCityNamewithFilter(vlaue, cityName);
                                }
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getActivity(), "No Internet Connection....", Toast.LENGTH_SHORT).show();
                showInternetStatus2();
            }
        }
    }

    private void gettingLabswithCityNamewithFilterTwo(final String vlaue, final String value2, final String cityName) {

        showlabslistList.clear();
        noInternetConnection.setVisibility(View.GONE);
        if (prograssStaticValue == 1) {
            progressDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GETTING_LABS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");

                            BookAppointmentModel sllm = new BookAppointmentModel();

                            sllm.setShow_lab_book_id(jsonObject1.getString("id"));
                            sllm.setShow_lab_book_user_name(jsonObject1.getString("user_name"));
                            sllm.setShow_lab_book_email(jsonObject1.getString("email"));
                            sllm.setShow_lab_book_mobile(jsonObject1.getString("mobile"));
                            sllm.setShow_lab_book_role_id(jsonObject1.getString("role_id"));
                            sllm.setShow_lab_book_created_time(jsonObject1.getString("created_time"));
                            sllm.setCertificate(jsonObject1.getString("certificate"));
                            sllm.setLocation(jsonObject1.getString("location"));
                            if (jsonObject1.getString("rating").equals("null")) {
                                sllm.setShow_lab_ratting("0");
                            } else {
                                sllm.setShow_lab_ratting(jsonObject1.getString("rating"));
                            }
                            sllm.setShow_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            sllm.setShow_lab_payment_online(jsonObject1.getString("payment_online"));
                            sllm.setShow_lab_tele_booking(jsonObject1.getString("tele_booking"));
                            sllm.setProfile_pic_url(jsonObject1.getString("profile_pic"));
                            sllm.setShow_lab_book_address(jsonObject1.getString("address"));

                            String workingDayfromServer = jsonObject1.getString("current_week_name");
                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];
                            if (segments[2].equals("1")) {
                                sllm.setShow_lab_book_workingdays("Open Today:" + segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (segments[2].equals("0")) {
                                sllm.setShow_lab_book_workingdays("Today lab Close");
                            }

                            sllm.setShow_lab_book_lat(jsonObject1.getString("lat"));
                            sllm.setShow_lab_book_lang(jsonObject1.getString("lang"));
                            sllm.setShow_lab_book_home_collections(jsonObject1.getString("home_collections"));
                            sllm.setShow_lab_book_distance(jsonObject1.getString("distance_in_km"));

                            showlabslistList.add(sllm);

                        }
                        mSwipeRefreshLayout.setRefreshing(false);
                        progressDialog.dismiss();
                        showlabslistrecyclerview.setAdapter(adapter);
                    } else {
                        showlabslistList.clear();
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);

                        noInternetConnection.setTypeface(type_lato);
                        noInternetConnection.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnection.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.show();
                mSwipeRefreshLayout.setRefreshing(false);

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("lat", lat);
                params.put("lang", lng);
                params.put("order_type", vlaue);
                params.put("home_collections", value2);
                if (first_time_flag == 1) {
                    params.put("city", cityName);
                }
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void gettingLabswithCityNamewithFilter(final String vlaue, final String cityName) {

        showlabslistList.clear();
        noInternetConnection.setVisibility(View.GONE);
        if (prograssStaticValue == 1) {
            progressDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GETTING_LABS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("LABLIST ", response);
                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");

                            BookAppointmentModel sllm = new BookAppointmentModel();

                            sllm.setShow_lab_book_id(jsonObject1.getString("id"));
                            sllm.setShow_lab_book_user_name(jsonObject1.getString("user_name"));
                            sllm.setShow_lab_book_email(jsonObject1.getString("email"));
                            sllm.setShow_lab_book_mobile(jsonObject1.getString("mobile"));
                            sllm.setShow_lab_book_role_id(jsonObject1.getString("role_id"));
                            sllm.setShow_lab_book_created_time(jsonObject1.getString("created_time"));
                            sllm.setCertificate(jsonObject1.getString("certificate"));
                            sllm.setLocation(jsonObject1.getString("location"));
                            if (jsonObject1.getString("rating").equals("null")) {
                                sllm.setShow_lab_ratting("0");
                            } else {
                                sllm.setShow_lab_ratting(jsonObject1.getString("rating"));
                            }
                            sllm.setShow_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            sllm.setShow_lab_payment_online(jsonObject1.getString("payment_online"));
                            sllm.setShow_lab_tele_booking(jsonObject1.getString("tele_booking"));
                            sllm.setProfile_pic_url(jsonObject1.getString("profile_pic"));
                            sllm.setShow_lab_book_address(jsonObject1.getString("address"));

                            String workingDayfromServer = jsonObject1.getString("current_week_name");
                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];
                            if (segments[2].equals("1")) {
                                sllm.setShow_lab_book_workingdays("Open Today:" + segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (segments[2].equals("0")) {
                                sllm.setShow_lab_book_workingdays("Today lab Close");
                            }
                            sllm.setShow_lab_book_lat(jsonObject1.getString("lat"));
                            sllm.setShow_lab_book_lang(jsonObject1.getString("lang"));
                            sllm.setShow_lab_book_home_collections(jsonObject1.getString("home_collections"));
                            sllm.setShow_lab_book_distance(jsonObject1.getString("distance_in_km"));

                            showlabslistList.add(sllm);

                        }
                        mSwipeRefreshLayout.setRefreshing(false);
                        progressDialog.dismiss();
                        showlabslistrecyclerview.setAdapter(adapter);
                    } else {
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                        showlabslistList.clear();
                        noInternetConnection.setTypeface(type_lato);
                        noInternetConnection.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnection.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.show();
                mSwipeRefreshLayout.setRefreshing(false);

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("lat", lat);
                params.put("lang", lng);
                params.put("order_type", vlaue);
                if (first_time_flag == 1) {
                    params.put("city", cityName);
                }
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }

    private void alertLabList(ArrayList<CitiesModel> labArrayList) {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.custom_dialog_layout_cities, null);
        RecyclerView cities_recycler_view = (RecyclerView) dialog_layout.findViewById(R.id.cities_recycler_view);
        final SearchView mSearch_list_cities = (SearchView) dialog_layout.findViewById(R.id.mSearch_list_cities);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        cities_recycler_view.setLayoutManager(layoutManager);


        citiesAdapter = new FourthFragCitiesAdapter(labArrayList, FourthFrag.this, R.layout.row_cities);


       /* cities_recycler_view.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        cities_recycler_view.addItemDecoration(new DividerItemDecoration(getActivity()));
        cities_recycler_view.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.recycler_divider));*/
        cities_recycler_view.setAdapter(citiesAdapter);


        mSearch_list_cities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearch_list_cities.setIconified(false);
            }
        });


        builder.setView(dialog_layout)


                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        progressDialog.dismiss();
                        dialogInterface.dismiss();
                    }
                });

        dialog = builder.create();

        mSearch_list_cities.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                citiesAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog.show();



        /*final String[] items = labArrayList.toArray(new String[labArrayList.size()]);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select City").setItems(items, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {

                String selectedCountry = items[i];
                selectCity.setText(selectedCountry);
                first_time_flag = 1;

                gettingLabsotherCities(selectedCountry);

            }
        });

        AlertDialog dialog = builder.create();
        progressDialog.dismiss();
        dialog.show();*/


    }

    private void gettingLabsotherCities(String selectedCountry) {
        gps1 = new GPSTrackers(getContext());

        if (gps1.canGetLocation()) {

            latitude = gps1.getLatitude();
            longitude = gps1.getLongitude();
            geocoder = new Geocoder(getActivity(), Locale.getDefault());

            try {

                cuurentaddresses = geocoder.getFromLocation(latitude, longitude, 1);

            } catch (IOException e) {

                e.printStackTrace();
            }

            lat = String.valueOf(latitude);
            lng = String.valueOf(longitude);

            showlabslistrecyclerview = (RecyclerView) rootView.findViewById(R.id.showlabslist_recycler_view_b);
            showlabslistrecyclerview.setHasFixedSize(true);

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
            showlabslistrecyclerview.setLayoutManager(layoutManager);
            adapter = new BookAppointmentAdapter(showlabslistList, FourthFrag.this, R.layout.row_book_appointment_items4);

            showlabslistrecyclerview.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
            showlabslistrecyclerview.addItemDecoration(new DividerItemDecoration(getActivity()));
            showlabslistrecyclerview.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.recycler_divider));

            if (filterFlag == 0) {
                String filterString = "distance_in_km asc";
                gettingLabsOtherCity(selectedCountry, filterString);
            }
        } else {
            mSwipeRefreshLayout.setRefreshing(false);
            startLocationAlert = new StartLocationAlert(getActivity());
        }
    }

    private void gettingLabsOtherCity(final String selectedCountry, final String filterString) {

        showlabslistList.clear();
        noInternetConnection.setVisibility(View.GONE);
        if (prograssStaticValue == 1) {
            progressDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GETTING_LABS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");

                            BookAppointmentModel sllm = new BookAppointmentModel();

                            sllm.setShow_lab_book_id(jsonObject1.getString("id"));
                            sllm.setShow_lab_book_user_name(jsonObject1.getString("user_name"));
                            sllm.setShow_lab_book_email(jsonObject1.getString("email"));
                            sllm.setShow_lab_book_mobile(jsonObject1.getString("mobile"));
                            sllm.setShow_lab_book_role_id(jsonObject1.getString("role_id"));
                            sllm.setShow_lab_book_created_time(jsonObject1.getString("created_time"));
                            sllm.setCertificate(jsonObject1.getString("certificate"));
                            sllm.setLocation(jsonObject1.getString("location"));
                            if (jsonObject1.getString("rating").equals("null")) {
                                sllm.setShow_lab_ratting("0");
                            } else {
                                sllm.setShow_lab_ratting(jsonObject1.getString("rating"));
                            }
                            sllm.setShow_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            sllm.setShow_lab_payment_online(jsonObject1.getString("payment_online"));
                            sllm.setShow_lab_tele_booking(jsonObject1.getString("tele_booking"));
                            sllm.setProfile_pic_url(jsonObject1.getString("profile_pic"));
                            sllm.setShow_lab_book_address(jsonObject1.getString("address"));

                            String workingDayfromServer = jsonObject1.getString("current_week_name");
                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];
                            if (segments[2].equals("1")) {
                                sllm.setShow_lab_book_workingdays("Open Today:" + segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (segments[2].equals("0")) {
                                sllm.setShow_lab_book_workingdays("Today lab Close");
                            }

                            sllm.setShow_lab_book_lat(jsonObject1.getString("lat"));
                            sllm.setShow_lab_book_lang(jsonObject1.getString("lang"));
                            sllm.setShow_lab_book_home_collections(jsonObject1.getString("home_collections"));
                            sllm.setShow_lab_book_distance(jsonObject1.getString("distance_in_km"));

                            showlabslistList.add(sllm);

                        }
                        mSwipeRefreshLayout.setRefreshing(false);
                        progressDialog.dismiss();
                        showlabslistrecyclerview.setAdapter(adapter);
                    } else {
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                        Log.d("PLEASECHEKONCE", "200ERROR");
                        noInternetConnection.setTypeface(type_lato);
                        noInternetConnection.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnection.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.show();
                mSwipeRefreshLayout.setRefreshing(false);

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("lat", lat);
                params.put("lang", lng);
                params.put("order_type", filterString);
                if (first_time_flag == 1) {
                    params.put("city", selectedCountry);
                }
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onRefresh() {

        ns2 = new NetworkStatus();
        isOnline2 = ns2.isOnline(getActivity());
        if (isOnline2) {

            gps1 = new GPSTrackers(getContext());
            if (gps1.canGetLocation()) {

                latitude = gps1.getLatitude();
                longitude = gps1.getLongitude();
                geocoder = new Geocoder(getActivity(), Locale.getDefault());

                try {
                    cuurentaddresses = geocoder.getFromLocation(latitude, longitude, 1);

                } catch (IOException e) {
                    e.printStackTrace();
                }

                lat = String.valueOf(latitude);
                lng = String.valueOf(longitude);
                showlabslistrecyclerview = (RecyclerView) rootView.findViewById(R.id.showlabslist_recycler_view_b);
                showlabslistrecyclerview.setHasFixedSize(true);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                showlabslistrecyclerview.setLayoutManager(layoutManager);
                adapter = new BookAppointmentAdapter(showlabslistList, FourthFrag.this, R.layout.row_book_appointment_items4);
                showlabslistrecyclerview.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
                showlabslistrecyclerview.addItemDecoration(new DividerItemDecoration(getActivity()));
                showlabslistrecyclerview.addItemDecoration(
                        new DividerItemDecoration(getActivity(), R.drawable.recycler_divider));
                if (filterFlag == 0) {
                    String filterString = "distance_in_km asc";
                    gettingLabsotherCities(selectCity.getText().toString());
                }

                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String query) {
                        adapter.getFilter().filter(query);
                        return false;
                    }
                });

            } else {
                mSwipeRefreshLayout.setRefreshing(false);
                startLocationAlert = new StartLocationAlert(getActivity());
            }
        } else {
            progressDialog.dismiss();
            mSwipeRefreshLayout.setRefreshing(false);
            Toast.makeText(getActivity(), "No Internet Connection....", Toast.LENGTH_SHORT).show();
            noInternetConnection.setTypeface(type_lato);
            noInternetConnection.setText("We are not in your location now, we will resume to your location shortly");
            noInternetConnection.setVisibility(View.VISIBLE);
        }
    }


    public void refreshMyList(String selectedCountry) {
        dialog.dismiss();
        adapter.notifyDataSetChanged();
        first_time_flag = 1;

        selectCity.setText(selectedCountry);
        gettingLabsotherCities(selectedCountry);


    }


}