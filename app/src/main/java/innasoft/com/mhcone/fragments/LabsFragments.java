package innasoft.com.mhcone.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.LabPackages;
import innasoft.com.mhcone.adapters.LabsFragmentCitiesAdapter;
import innasoft.com.mhcone.adapters.ShowLabsListAdapter;
import innasoft.com.mhcone.dbhelper.MyHealthDBHelper;
import innasoft.com.mhcone.models.CitiesModel;
import innasoft.com.mhcone.models.ShowLabsListModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.DividerItemDecoration;
import innasoft.com.mhcone.utilities.GPSTrackers;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.StartLocationAlert;
import innasoft.com.mhcone.utilities.UserSessionManager;
import innasoft.com.mhcone.utilities.VerticalSpaceItemDecoration;

public class LabsFragments extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private View rootView;

    ArrayList<String> labArrayList = new ArrayList<String>();

    ArrayList<ShowLabsListModel> showlabslistList = new ArrayList<ShowLabsListModel>();
    ;
    ShowLabsListAdapter adapter;
    RecyclerView showlabslistrecyclerview;
    StartLocationAlert startLocationAlert;
    UserSessionManager userSessionManager;
    int flag;
    private Object labsList;
    private FloatingActionButton fabPackageLabs;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    Typeface typeface;
    TextView defaultTExt, noInternetConnection;
    Button retry_btn;
    private TextView Enable_Gps, Disable_Gps;
    private GoogleApiClient googleApiClient;

    final static int REQUEST_LOCATION = 199;

    SearchView searchView;
    NetworkStatus ns, ns2, ns3, ns4;
    Boolean isOnline = false, isOnline2 = false, isOnline3 = false, isOnline4 = false;
    GPSTrackers gps, gps1;
    String lat, lng;
    ProgressDialog progressDialog;
    int prograssStaticValue = 0;

    private Typeface type_lato;

    RelativeLayout rl;
    TextView selectCity;
    ImageView selectLocation, no_labs_found_iv;

    /*Current Address*/
    Geocoder geocoder;
    List<Address> cuurentaddresses = new ArrayList<Address>();
    String address = null;
    String city;
    String state;
    String country;
    String pincode;
    double latitude;
    double longitude;
    int filterFlag = 0;
    int first_time_flag = 0;

    private static final int VERTICAL_ITEM_SPACE = 0;

    ArrayList<CitiesModel> citiesModelArrayList = new ArrayList<CitiesModel>();
    LabsFragmentCitiesAdapter citiesAdapter;
    AlertDialog dialog;
    public String global_lat, global_lng;
    int fabaction_status = 0;

    private boolean statusFlag;

    MyHealthDBHelper myHealthDBHelper;

    public LabsFragments() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        type_lato = Typeface.createFromAsset(getContext().getAssets(), "robotoregular.ttf");

        userSessionManager = new UserSessionManager(getActivity());

        myHealthDBHelper = new MyHealthDBHelper(getActivity());

        rootView = inflater.inflate(R.layout.fragment_labs_fragments, container, false);
        ns = new NetworkStatus();
        isOnline = ns.isOnline(getActivity());

        fabPackageLabs = (FloatingActionButton) rootView.findViewById(R.id.filter_package_labs);

        rl = (RelativeLayout) rootView.findViewById(R.id.location_rl);
        rl.setOnClickListener(this);

        selectCity = (TextView) rootView.findViewById(R.id.select_city);
        selectCity.setOnClickListener(this);

        retry_btn = (Button) rootView.findViewById(R.id.retry_btn);
        retry_btn.setTypeface(typeface);
        retry_btn.setOnClickListener(this);

        selectLocation = (ImageView) rootView.findViewById(R.id.select_location);
        selectLocation.setOnClickListener(this);
        no_labs_found_iv = (ImageView) rootView.findViewById(R.id.no_labs_found_iv);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.labs_frag_swipe_layout);

        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,

                android.R.color.holo_green_light,

                android.R.color.holo_orange_light,

                android.R.color.holo_red_light);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        searchView = (SearchView) rootView.findViewById(R.id.mSearchlabs_labs_fragment);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);

            }
        });

        defaultTExt = (TextView) rootView.findViewById(R.id.defaultText);
        noInternetConnection = (TextView) rootView.findViewById(R.id.noInternetConnect);

        if (isOnline) {


            if (!hasGPSDevice(getActivity())) {
                Toast.makeText(getActivity(), "Gps not Supported", Toast.LENGTH_SHORT).show();
            }
            final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(getActivity())) {
                enableLoc();
            } else {

                gps = new GPSTrackers(getContext());

                latitude = gps.getLatitude();
                longitude = gps.getLongitude();


                geocoder = new Geocoder(getActivity(), Locale.getDefault());

                try {

                    cuurentaddresses = geocoder.getFromLocation(latitude, longitude, 1);

                } catch (IOException e) {

                    e.printStackTrace();
                }

                lat = String.valueOf(latitude);
                lng = String.valueOf(longitude);
                showlabslistrecyclerview = (RecyclerView) rootView.findViewById(R.id.showlabslist_recycler_view);
                showlabslistrecyclerview.setHasFixedSize(true);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                showlabslistrecyclerview.setLayoutManager(layoutManager);
                adapter = new ShowLabsListAdapter(showlabslistList, LabsFragments.this, R.layout.row_labs_list4);

                showlabslistrecyclerview.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
                showlabslistrecyclerview.addItemDecoration(new DividerItemDecoration(getActivity()));
                showlabslistrecyclerview.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.recycler_divider));

                if (filterFlag == 0) {
                    String filterString = "distance_in_km asc";
                    getLabsList(filterString);
                }
                if (cuurentaddresses.size() != 0) {

                    city = cuurentaddresses.get(0).getLocality();
                    selectCity.setText(city);
                } else {

                }

            }

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String query) {

                    adapter.getFilter().filter(query);
                    adapter.notifyDataSetChanged();
                    return true;
                }
            });
        } else {
            showInternetStatus2();
        }

        fabPackageLabs.setOnClickListener(this);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        return rootView;
    }

    private void enableLoc() {

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {

                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            googleApiClient.connect();
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {

                            Log.d("Location error", "Location error " + connectionResult.getErrorCode());
                        }
                    }).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                status.startResolutionForResult(getActivity(), REQUEST_LOCATION);
                            } catch (IntentSender.SendIntentException e) {

                            }
                            break;
                    }
                }
            });
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_CANCELED: {
                        // The user was asked to change settings, but chose not to
                        enableLoc();
                        break;
                    }
                    case Activity.RESULT_OK: {

//                        if (userSessionManager.checkLogin() != false) {

//                            Intent i = new Intent(getActivity(), StaticActivity.class);
//                            startActivity(i);
//                            getActivity().finish();
//                        }
//                        else {
                            Intent i = new Intent(getActivity(), MainActivity.class);
                            startActivity(i);
                            getActivity().finish();
//                        }
                        break;


                    }
                    default: {
                        break;
                    }
                }
                break;
        }

    }

    private void showInternetStatus2() {
        progressDialog.dismiss();
        noInternetConnection.setTypeface(type_lato);
        noInternetConnection.setVisibility(View.VISIBLE);
    }

    public void cardViewListener(int position) {

        if (userSessionManager.checkLogin() != false) {
            Toast.makeText(getActivity(), "Please Login...!", Toast.LENGTH_SHORT).show();
        } else {
            String lab_id = showlabslistList.get(position).getShow_lab_list_id();
            String lab_name = showlabslistList.get(position).getShow_lab_list_user_name();

            Intent intent = new Intent(getActivity(), LabPackages.class);
            intent.putExtra("labId", lab_id);
            intent.putExtra("labName", lab_name);
            intent.putExtra("payment_lab", showlabslistList.get(position).getShow_lab_payment_lab());
            intent.putExtra("payment_online", showlabslistList.get(position).getShow_lab_payment_online());
            intent.putExtra("tele_booking", showlabslistList.get(position).getShow_lab_tele_booking());
            intent.putExtra("labPhone", showlabslistList.get(position).getShow_lab_list_mobile());
            startActivity(intent);
        }
    }

    private void getLabsList(final String filterValue) {
        showlabslistList.clear();
        no_labs_found_iv.setVisibility(View.GONE);
        noInternetConnection.setVisibility(View.GONE);
        if (prograssStaticValue == 1) {
            progressDialog.show();
        }
        Log.d("URLLL ", AppUrls.BASE_URL + AppUrls.GETTING_LABS);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GETTING_LABS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("LABLIST ", response);
                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");

                            ShowLabsListModel sllm = new ShowLabsListModel();

                            sllm.setShow_lab_list_id(jsonObject1.getString("id"));
                            sllm.setShow_lab_list_user_name(jsonObject1.getString("user_name"));
                            sllm.setShow_lab_list_email(jsonObject1.getString("email"));
                            sllm.setShow_lab_list_mobile(jsonObject1.getString("mobile"));
                            sllm.setShow_lab_list_role_id(jsonObject1.getString("role_id"));
                            sllm.setShow_lab_list_created_time(jsonObject1.getString("created_time"));
                            sllm.setCertificate(jsonObject1.getString("certificate"));
                            sllm.setProfile_pic_url(AppUrls.IMAGE_URL + jsonObject1.getString("profile_pic"));
                            sllm.setLocation(jsonObject1.getString("location"));
                            if (jsonObject1.getString("rating").equals("null")) {
                                sllm.setShow_lab_ratting("0");
                            } else {
                                sllm.setShow_lab_ratting(jsonObject1.getString("rating"));
                            }
                            sllm.setShow_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            sllm.setShow_lab_payment_online(jsonObject1.getString("payment_online"));
                            sllm.setShow_lab_tele_booking(jsonObject1.getString("tele_booking"));
                            sllm.setShow_lab_list_workingdays(jsonObject1.getString("workingdays"));
                            sllm.setShow_lab_list_address(jsonObject1.getString("address"));
                            String workingDayfromServer = jsonObject1.getString("current_week_name");
                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];
                            if (conditionCheck.equals("1")) {
                                sllm.setShow_lab_list_workingtime("Open Today:" + segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (conditionCheck.equals("0")) {
                                sllm.setShow_lab_list_workingtime("Today lab Close");
                            }
                            sllm.setShow_lab_list_lat(jsonObject1.getString("lat"));
                            sllm.setShow_lab_list_lang(jsonObject1.getString("lang"));
                            sllm.setShow_lab_list_home_collections(jsonObject1.getString("home_collections"));
                            sllm.setShow_lab_list_distance(jsonObject1.getString("distance_in_km"));

                            showlabslistList.add(sllm);
                            fabaction_status = 0;

                        }
                        mSwipeRefreshLayout.setRefreshing(false);
                        progressDialog.dismiss();
                        showlabslistrecyclerview.setAdapter(adapter);
                    } else {
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                        fabaction_status = 1;
                        noInternetConnection.setTypeface(type_lato);
                        no_labs_found_iv.setVisibility(View.VISIBLE);
                        noInternetConnection.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnection.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //progressDialog.show();
                mSwipeRefreshLayout.setRefreshing(false);

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("order_type", filterValue);
                if (first_time_flag == 1) {
                    params.put("lat", global_lat);
                    params.put("lang", global_lng);
                } else {
                    params.put("lat", lat);
                    params.put("lang", lng);
                }
                Log.d("LABSLISTggg", filterValue + "\n" + global_lat + "\n" + global_lng + "\n" + lat + "\n" + lng);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {

        if (v == retry_btn) {
            //getLabsList(filterString);
            onRefresh();
        }

        if (v == rl) {
            progressDialog.show();
            labArrayList.clear();

            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.LAB_LOCATIONS, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        citiesModelArrayList.clear();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            CitiesModel citiesModel = new CitiesModel();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("city");
                            labArrayList.add(jsonObject1.getString("city_name"));

                            String city_name = jsonObject1.getString("city_name");
                            String city_latitude = jsonObject1.getString("lat");
                            String city_longitude = jsonObject1.getString("lang");

                            citiesModel.setCity_name(city_name);
                            citiesModel.setLat(city_latitude);
                            citiesModel.setLang(city_longitude);

                            citiesModelArrayList.add(citiesModel);
                        }
                        alertLabList(citiesModelArrayList);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        }

        if (v == selectCity) {
            ns3 = new NetworkStatus();
            isOnline3 = ns3.isOnline(getActivity());
            if (isOnline3) {
                progressDialog.show();
                labArrayList.clear();

                StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.LAB_LOCATIONS, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            citiesModelArrayList.clear();
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                CitiesModel citiesModel = new CitiesModel();

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("city");

                                String city_name = jsonObject1.getString("city_name");
                                String city_latitude = jsonObject1.getString("lat");
                                String city_longitude = jsonObject1.getString("lang");

                                citiesModel.setCity_name(city_name);
                                citiesModel.setLat(city_latitude);
                                citiesModel.setLang(city_longitude);

                                labArrayList.add(city_name);

                                citiesModelArrayList.add(citiesModel);
                            }
                            alertLabList(citiesModelArrayList);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        String responseBody = null;

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        } else if (error instanceof AuthFailureError) {
                        } else if (error instanceof ServerError) {
                        } else if (error instanceof NetworkError) {
                        } else if (error instanceof ParseError) {
                        }
                    }
                });

                RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                requestQueue.add(stringRequest);
            } else {
                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getActivity(), "No Internet Connection....", Toast.LENGTH_SHORT).show();
                noInternetConnection.setTypeface(type_lato);
                no_labs_found_iv.setVisibility(View.VISIBLE);
                noInternetConnection.setText("We are not in your location now, we will resume to your location shortly");
                noInternetConnection.setVisibility(View.VISIBLE);
            }
        }

        if (v == selectLocation) {
            ns3 = new NetworkStatus();
            isOnline3 = ns3.isOnline(getActivity());
            if (isOnline3) {
                progressDialog.show();
                labArrayList.clear();

                StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.LAB_LOCATIONS, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            citiesModelArrayList.clear();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                CitiesModel citiesModel = new CitiesModel();

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("city");

                                String city_name = jsonObject1.getString("city_name");
                                String city_latitude = jsonObject1.getString("lat");
                                String city_longitude = jsonObject1.getString("lang");

                                citiesModel.setCity_name(city_name);
                                citiesModel.setLat(city_latitude);
                                citiesModel.setLang(city_longitude);

                                labArrayList.add(city_name);
                                citiesModelArrayList.add(citiesModel);
                            }
                            alertLabList(citiesModelArrayList);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        } else if (error instanceof AuthFailureError) {
                        } else if (error instanceof ServerError) {
                        } else if (error instanceof NetworkError) {
                        } else if (error instanceof ParseError) {
                        }
                    }
                });

                RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                requestQueue.add(stringRequest);
            } else {
                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getActivity(), "No Internet Connection....", Toast.LENGTH_SHORT).show();
                noInternetConnection.setTypeface(type_lato);
                no_labs_found_iv.setVisibility(View.VISIBLE);
                noInternetConnection.setText("We are not in your location now, we will resume to your location shortly");
                noInternetConnection.setVisibility(View.VISIBLE);
            }
        }

        if (v == fabPackageLabs) {
            ns4 = new NetworkStatus();
            isOnline4 = ns4.isOnline(getActivity());

            if (isOnline4) {
                if (fabaction_status == 0) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    LayoutInflater inflater = getActivity().getLayoutInflater();

                    View dialog_layout = inflater.inflate(R.layout.custom_dialog_filter, null);

                    TextView dialog_title = (TextView) dialog_layout.findViewById(R.id.dialog_title);
                    dialog_title.setTypeface(typeface);

                    TextView dlh_txt = (TextView) dialog_layout.findViewById(R.id.dlh_txt);
                    dlh_txt.setTypeface(typeface);

                    TextView plh_txt = (TextView) dialog_layout.findViewById(R.id.plh_txt);
                    plh_txt.setTypeface(typeface);
                    plh_txt.setVisibility(View.GONE);

                    View view_filter = dialog_layout.findViewById(R.id.view_filter);
                    view_filter.setVisibility(View.GONE);

                    TextView phl_txt = (TextView) dialog_layout.findViewById(R.id.phl_txt);
                    phl_txt.setTypeface(typeface);
                    phl_txt.setVisibility(View.GONE);

                    final String cityName = selectCity.getText().toString();

                    dlh_txt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            prograssStaticValue = 1;
                            String vlaue = "distance_in_km asc";

                            if (first_time_flag == 1) {
                                gettingLabswithCityNamewithFilter(vlaue, global_lat, global_lng, cityName);
                            } else {
                                gettingLabswithCityNamewithFilter(vlaue, lat, lng, cityName);
                            }
                            dialog.dismiss();
                        }
                    });

                    TextView dhl_txt = (TextView) dialog_layout.findViewById(R.id.dhl_txt);
                    dhl_txt.setTypeface(typeface);
                    dhl_txt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            prograssStaticValue = 1;
                            String vlaue = "distance_in_km desc";

                            if (first_time_flag == 1) {
                                gettingLabswithCityNamewithFilter(vlaue, global_lat, global_lng, cityName);
                            } else {
                                gettingLabswithCityNamewithFilter(vlaue, lat, lng, cityName);
                            }
                            dialog.dismiss();
                        }
                    });

                    TextView naz_txt = (TextView) dialog_layout.findViewById(R.id.naz_txt);
                    naz_txt.setTypeface(typeface);
                    naz_txt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            prograssStaticValue = 1;
                            String vlaue = "users.user_name asc";

                            if (first_time_flag == 1) {
                                gettingLabswithCityNamewithFilter(vlaue, global_lat, global_lng, cityName);
                            } else {
                                gettingLabswithCityNamewithFilter(vlaue, lat, lng, cityName);
                            }
                            dialog.dismiss();
                        }
                    });


                    TextView nza_txt = (TextView) dialog_layout.findViewById(R.id.nza_txt);
                    nza_txt.setTypeface(typeface);
                    nza_txt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            prograssStaticValue = 1;
                            String vlaue = "users.user_name desc";

                            if (first_time_flag == 1) {
                                gettingLabswithCityNamewithFilter(vlaue, global_lat, global_lng, cityName);
                            } else {
                                gettingLabswithCityNamewithFilter(vlaue, lat, lng, cityName);
                            }
                            dialog.dismiss();
                        }
                    });

                    TextView hc_txt = (TextView) dialog_layout.findViewById(R.id.hc_txt);
                    hc_txt.setTypeface(typeface);
                    hc_txt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String vlaue = "distance_in_km asc";
                            String value2 = "lab_users.home_collections = 1";

                            if (first_time_flag == 1) {
                                gettingLabswithCityNamewithFilterTwo(vlaue, global_lat, global_lng, value2, cityName);
                            } else {
                                gettingLabswithCityNamewithFilterTwo(vlaue, lat, lng, value2, cityName);
                            }
                            dialog.dismiss();
                        }
                    });

                    TextView rhl_txt = (TextView) dialog_layout.findViewById(R.id.rhl_txt);
                    rhl_txt.setTypeface(typeface);
                    rhl_txt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            prograssStaticValue = 1;
                            String vlaue = "rating desc";

                            if (first_time_flag == 1) {
                                gettingLabswithCityNamewithFilter(vlaue, global_lat, global_lng, cityName);
                            } else {
                                gettingLabswithCityNamewithFilter(vlaue, lat, lng, cityName);
                            }
                            dialog.dismiss();
                        }
                    });


                    TextView rlh_txt = (TextView) dialog_layout.findViewById(R.id.rlh_txt);
                    rlh_txt.setTypeface(typeface);
                    rlh_txt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            prograssStaticValue = 1;
                            String vlaue = "rating asc";

                            if (first_time_flag == 1) {
                                gettingLabswithCityNamewithFilter(vlaue, global_lat, global_lng, cityName);
                            } else {
                                gettingLabswithCityNamewithFilter(vlaue, lat, lng, cityName);
                            }
                            dialog.dismiss();
                        }
                    });


                    TextView nabl_txt = (TextView) dialog_layout.findViewById(R.id.nabl_txt);
                    nabl_txt.setTypeface(typeface);
                    nabl_txt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String vlaue = "distance_in_km asc";
                            String value2 = "lab_users.certificate = 1";

                            if (first_time_flag == 1) {
                                gettingLabswithCityNamewithFilterTwo(vlaue, global_lat, global_lng, value2, cityName);
                            } else {
                                gettingLabswithCityNamewithFilterTwo(vlaue, lat, lng, value2, cityName);
                            }
                            dialog.dismiss();
                        }
                    });


                    builder.setView(dialog_layout)
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });

                    dialog = builder.create();
                    dialog.show();



                    /*final String[] country = {"Distance Low to High", "Distance High to Low", "Names A to Z", "Names Z to A", "Home Collection"
                            , "Ratting High to Low", "Ratting Low to High", "NABL/CAP Accreditation"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Choose One")
                            .setItems(country, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    String selectedCountry = country[i];
                                    String cityName = selectCity.getText().toString();

                                    if (selectedCountry.equals("Distance Low to High")) {
                                        prograssStaticValue = 1;
                                        String vlaue = "distance_in_km asc";

                                        if (first_time_flag == 1) {
                                            gettingLabswithCityNamewithFilter(vlaue,  global_lat, global_lng, cityName);
                                        } else {
                                            gettingLabswithCityNamewithFilter(vlaue,  lat, lng, cityName);
                                        }
                                    }
                                    if (selectedCountry.equals("Distance High to Low")) {
                                        prograssStaticValue = 1;
                                        String vlaue = "distance_in_km desc";

                                        if (first_time_flag == 1) {
                                            gettingLabswithCityNamewithFilter(vlaue,  global_lat, global_lng, cityName);
                                        } else {
                                            gettingLabswithCityNamewithFilter(vlaue,  lat, lng, cityName);
                                        }
                                    }
                                    if (selectedCountry.equals("Names A to Z")) {
                                        prograssStaticValue = 1;
                                        String vlaue = "users.user_name asc";

                                        if (first_time_flag == 1) {
                                            gettingLabswithCityNamewithFilter(vlaue,  global_lat, global_lng, cityName);
                                        } else {
                                            gettingLabswithCityNamewithFilter(vlaue,  lat, lng, cityName);
                                        }
                                    }
                                    if (selectedCountry.equals("Names Z to A")) {
                                        prograssStaticValue = 1;
                                        String vlaue = "users.user_name desc";

                                        if (first_time_flag == 1) {
                                            gettingLabswithCityNamewithFilter(vlaue,  global_lat, global_lng, cityName);
                                        } else {
                                            gettingLabswithCityNamewithFilter(vlaue,  lat, lng, cityName);
                                        }
                                    }
                                    if (selectedCountry.equals("Home Collection")) {

                                        String vlaue = "distance_in_km asc";
                                        String value2 = "lab_users.home_collections = 1";

                                        if (first_time_flag == 1) {
                                            gettingLabswithCityNamewithFilterTwo(vlaue,  global_lat, global_lng, value2, cityName);
                                        } else {
                                            gettingLabswithCityNamewithFilterTwo(vlaue,  lat, lng, value2, cityName);
                                        }
                                    }

                                    if (selectedCountry.equals("NABL/CAP Accreditation")) {
                                        String vlaue = "distance_in_km asc";
                                        String value2 = "lab_users.certificate = 1";

                                        if (first_time_flag == 1) {
                                            gettingLabswithCityNamewithFilterTwo(vlaue,  global_lat, global_lng, value2, cityName);
                                        } else {
                                            gettingLabswithCityNamewithFilterTwo(vlaue,  lat, lng, value2, cityName);
                                        }
                                    }

                                    if (selectedCountry.equals("Ratting High to Low")) {
                                        prograssStaticValue = 1;
                                        String vlaue = "rating desc";

                                        if (first_time_flag == 1) {
                                            gettingLabswithCityNamewithFilter(vlaue,  global_lat, global_lng, cityName);
                                        } else {
                                            gettingLabswithCityNamewithFilter(vlaue,  lat, lng, cityName);
                                        }
                                    }

                                    if (selectedCountry.equals("Ratting Low to High")) {
                                        prograssStaticValue = 1;
                                        String vlaue = "rating asc";

                                        if (first_time_flag == 1) {
                                            gettingLabswithCityNamewithFilter(vlaue,  global_lat, global_lng, cityName);
                                        } else {
                                            gettingLabswithCityNamewithFilter(vlaue,  lat, lng, cityName);
                                        }
                                    }
                                }
                            });

                    AlertDialog dialog = builder.create();
                    dialog.show();*/
                } else {
                    no_labs_found_iv.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), "No Labs found....!", Toast.LENGTH_SHORT).show();
                }
            } else {
                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getActivity(), "No Internet Connection....", Toast.LENGTH_SHORT).show();
                showInternetStatus2();
            }
        }
    }

    private void gettingLabswithCityNamewithFilterTwo(final String vlaue, final String lat, final String lng, final String value2, final String cityName) {

        showlabslistList.clear();
        noInternetConnection.setVisibility(View.GONE);
        no_labs_found_iv.setVisibility(View.GONE);
        if (prograssStaticValue == 1) {
            progressDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GETTING_LABS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");

                            ShowLabsListModel sllm = new ShowLabsListModel();

                            sllm.setShow_lab_list_id(jsonObject1.getString("id"));
                            sllm.setShow_lab_list_user_name(jsonObject1.getString("user_name"));
                            sllm.setShow_lab_list_email(jsonObject1.getString("email"));
                            sllm.setShow_lab_list_mobile(jsonObject1.getString("mobile"));
                            sllm.setShow_lab_list_role_id(jsonObject1.getString("role_id"));
                            sllm.setShow_lab_list_created_time(jsonObject1.getString("created_time"));
                            sllm.setCertificate(jsonObject1.getString("certificate"));
                            sllm.setProfile_pic_url(jsonObject1.getString("profile_pic"));
                            sllm.setLocation(jsonObject1.getString("location"));
                            if (jsonObject1.getString("rating").equals("null")) {
                                sllm.setShow_lab_ratting("0");
                            } else {
                                sllm.setShow_lab_ratting(jsonObject1.getString("rating"));
                            }
                            sllm.setShow_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            sllm.setShow_lab_payment_online(jsonObject1.getString("payment_online"));
                            sllm.setShow_lab_tele_booking(jsonObject1.getString("tele_booking"));
                            sllm.setShow_lab_list_workingdays(jsonObject1.getString("workingdays"));
                            sllm.setShow_lab_list_address(jsonObject1.getString("address"));
                            String workingDayfromServer = jsonObject1.getString("current_week_name");
                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];
                            if (conditionCheck.equals("1")) {
                                sllm.setShow_lab_list_workingtime("Open Today:" + segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (conditionCheck.equals("0")) {
                                sllm.setShow_lab_list_workingtime("Today lab Close");
                            }
                            sllm.setShow_lab_list_lat(jsonObject1.getString("lat"));
                            sllm.setShow_lab_list_lang(jsonObject1.getString("lang"));
                            sllm.setShow_lab_list_home_collections(jsonObject1.getString("home_collections"));
                            sllm.setShow_lab_list_distance(jsonObject1.getString("distance_in_km"));

                            showlabslistList.add(sllm);
                            fabaction_status = 0;
                        }
                        mSwipeRefreshLayout.setRefreshing(false);
                        progressDialog.dismiss();
                        showlabslistrecyclerview.setAdapter(adapter);
                    } else {
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                        showlabslistList.clear();
                        fabaction_status = 1;
                        noInternetConnection.setTypeface(type_lato);
                        no_labs_found_iv.setVisibility(View.VISIBLE);
                        noInternetConnection.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnection.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.show();
                mSwipeRefreshLayout.setRefreshing(false);

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("lat", lat);
                params.put("lang", lng);
                params.put("order_type", vlaue);
                params.put("home_collections", value2);
                if (first_time_flag == 1) {
                    params.put("lat", global_lat);
                    params.put("lang", global_lng);
                } else {
                    params.put("lat", lat);
                    params.put("lang", lng);
                }
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void gettingLabswithCityNamewithFilter(final String vlaue, final String lat, final String lng, String cityName) {

        showlabslistList.clear();
        noInternetConnection.setVisibility(View.GONE);
        no_labs_found_iv.setVisibility(View.GONE);
        if (prograssStaticValue == 1) {
            progressDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GETTING_LABS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");

                            ShowLabsListModel sllm = new ShowLabsListModel();

                            sllm.setShow_lab_list_id(jsonObject1.getString("id"));
                            sllm.setShow_lab_list_user_name(jsonObject1.getString("user_name"));
                            sllm.setShow_lab_list_email(jsonObject1.getString("email"));
                            sllm.setShow_lab_list_mobile(jsonObject1.getString("mobile"));
                            sllm.setShow_lab_list_role_id(jsonObject1.getString("role_id"));
                            sllm.setShow_lab_list_created_time(jsonObject1.getString("created_time"));
                            sllm.setCertificate(jsonObject1.getString("certificate"));
                            sllm.setProfile_pic_url(jsonObject1.getString("profile_pic"));
                            sllm.setLocation(jsonObject1.getString("location"));
                            if (jsonObject1.getString("rating").equals("null")) {
                                sllm.setShow_lab_ratting("0");
                            } else {
                                sllm.setShow_lab_ratting(jsonObject1.getString("rating"));
                            }
                            sllm.setShow_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            sllm.setShow_lab_payment_online(jsonObject1.getString("payment_online"));
                            sllm.setShow_lab_tele_booking(jsonObject1.getString("tele_booking"));
                            sllm.setShow_lab_list_workingdays(jsonObject1.getString("workingdays"));
                            sllm.setShow_lab_list_address(jsonObject1.getString("address"));
                            String workingDayfromServer = jsonObject1.getString("current_week_name");
                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];
                            if (conditionCheck.equals("1")) {
                                sllm.setShow_lab_list_workingtime("Open Today:" + segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (conditionCheck.equals("0")) {
                                sllm.setShow_lab_list_workingtime("Today lab Close");
                            }
                            sllm.setShow_lab_list_lat(jsonObject1.getString("lat"));
                            sllm.setShow_lab_list_lang(jsonObject1.getString("lang"));
                            sllm.setShow_lab_list_home_collections(jsonObject1.getString("home_collections"));
                            sllm.setShow_lab_list_distance(jsonObject1.getString("distance_in_km"));

                            showlabslistList.add(sllm);
                            fabaction_status = 0;

                        }
                        mSwipeRefreshLayout.setRefreshing(false);
                        progressDialog.dismiss();
                        showlabslistrecyclerview.setAdapter(adapter);
                    } else {
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                        showlabslistList.clear();
                        fabaction_status = 1;
                        noInternetConnection.setTypeface(type_lato);
                        no_labs_found_iv.setVisibility(View.VISIBLE);
                        noInternetConnection.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnection.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.show();
                mSwipeRefreshLayout.setRefreshing(false);

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("lat", lat);
                params.put("lang", lng);
                params.put("order_type", vlaue);
                if (first_time_flag == 1) {
                    params.put("lat", global_lat);
                    params.put("lang", global_lng);
                } else {
                    params.put("lat", lat);
                    params.put("lang", lng);
                }
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }

    private void alertLabList(ArrayList<CitiesModel> labArrayList) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.custom_dialog_layout_cities, null);
        RecyclerView cities_recycler_view = (RecyclerView) dialog_layout.findViewById(R.id.cities_recycler_view);
        final SearchView mSearch_list_cities = (SearchView) dialog_layout.findViewById(R.id.mSearch_list_cities);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        cities_recycler_view.setLayoutManager(layoutManager);

        citiesAdapter = new LabsFragmentCitiesAdapter(labArrayList, LabsFragments.this, R.layout.row_cities);

        cities_recycler_view.setAdapter(citiesAdapter);

        mSearch_list_cities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearch_list_cities.setIconified(false);
            }
        });

        builder.setView(dialog_layout)

                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        progressDialog.dismiss();
                        dialogInterface.dismiss();
                    }
                });

        dialog = builder.create();
        filterFlag = 1;

        mSearch_list_cities.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                citiesAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog.show();
    }

    private void gettingLabsotherCities(String selectedCountry, String strLat, String strLng) {


        if (!hasGPSDevice(getActivity())) {
            Toast.makeText(getActivity(), "Gps not Supported", Toast.LENGTH_SHORT).show();
        }
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(getActivity())) {
            //Toast.makeText(getActivity(),"Gps not enabled",Toast.LENGTH_SHORT).show();
            enableLoc();
        } else {

            gps1 = new GPSTrackers(getContext());

            latitude = gps1.getLatitude();
            longitude = gps1.getLongitude();

            geocoder = new Geocoder(getActivity(), Locale.getDefault());

            try {
                cuurentaddresses = geocoder.getFromLocation(latitude, longitude, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            lat = String.valueOf(latitude);
            lng = String.valueOf(longitude);

            showlabslistrecyclerview = (RecyclerView) rootView.findViewById(R.id.showlabslist_recycler_view);
            showlabslistrecyclerview.setHasFixedSize(true);

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
            showlabslistrecyclerview.setLayoutManager(layoutManager);
            adapter = new ShowLabsListAdapter(showlabslistList, LabsFragments.this, R.layout.row_labs_list4);

            showlabslistrecyclerview.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
            showlabslistrecyclerview.addItemDecoration(new DividerItemDecoration(getActivity()));
            showlabslistrecyclerview.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.recycler_divider));
            String filterString = "distance_in_km asc";
            gettingLabsOtherCity(selectedCountry, filterString);
            //Toast.makeText(getActivity(),"Gps already enabled",Toast.LENGTH_SHORT).show();
        }

    }

    private void gettingLabsOtherCity(final String selectedCountry, final String filterString) {
        showlabslistList.clear();
        noInternetConnection.setVisibility(View.GONE);
        no_labs_found_iv.setVisibility(View.GONE);
        if (prograssStaticValue == 1) {
            progressDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GETTING_LABS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("PARAMAMAMMAMMA", response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");

                            ShowLabsListModel sllm = new ShowLabsListModel();

                            sllm.setShow_lab_list_id(jsonObject1.getString("id"));
                            sllm.setShow_lab_list_user_name(jsonObject1.getString("user_name"));
                            sllm.setShow_lab_list_email(jsonObject1.getString("email"));
                            sllm.setShow_lab_list_mobile(jsonObject1.getString("mobile"));
                            sllm.setShow_lab_list_role_id(jsonObject1.getString("role_id"));
                            sllm.setShow_lab_list_created_time(jsonObject1.getString("created_time"));
                            sllm.setCertificate(jsonObject1.getString("certificate"));
                            sllm.setProfile_pic_url(jsonObject1.getString("profile_pic"));
                            sllm.setLocation(jsonObject1.getString("location"));
                            if (jsonObject1.getString("rating").equals("null")) {
                                sllm.setShow_lab_ratting("0");
                            } else {
                                sllm.setShow_lab_ratting(jsonObject1.getString("rating"));
                            }
                            sllm.setShow_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            sllm.setShow_lab_payment_online(jsonObject1.getString("payment_online"));
                            sllm.setShow_lab_tele_booking(jsonObject1.getString("tele_booking"));
                            sllm.setShow_lab_list_workingdays(jsonObject1.getString("workingdays"));
                            sllm.setShow_lab_list_address(jsonObject1.getString("address"));
                            String workingDayfromServer = jsonObject1.getString("current_week_name");
                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];
                            if (conditionCheck.equals("1")) {
                                sllm.setShow_lab_list_workingtime("Open Today:" + segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (conditionCheck.equals("0")) {
                                sllm.setShow_lab_list_workingtime("Today lab Close");
                            }
                            sllm.setShow_lab_list_lat(jsonObject1.getString("lat"));
                            sllm.setShow_lab_list_lang(jsonObject1.getString("lang"));
                            sllm.setShow_lab_list_home_collections(jsonObject1.getString("home_collections"));
                            sllm.setShow_lab_list_distance(jsonObject1.getString("distance_in_km"));

                            showlabslistList.add(sllm);
                            fabaction_status = 0;
                        }
                        mSwipeRefreshLayout.setRefreshing(false);
                        progressDialog.dismiss();
                        showlabslistrecyclerview.setAdapter(adapter);
                    } else {
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                        showlabslistList.clear();
                        fabaction_status = 1;
                        noInternetConnection.setTypeface(type_lato);
                        no_labs_found_iv.setVisibility(View.VISIBLE);
                        noInternetConnection.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnection.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.show();
                mSwipeRefreshLayout.setRefreshing(false);

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("lat", lat);
                params.put("lang", lng);
                params.put("order_type", filterString);
                if (first_time_flag == 1) {
                    params.put("lat", global_lat);
                    params.put("lang", global_lng);
                } else {
                    params.put("lat", lat);
                    params.put("lang", lng);
                }
                Log.d("PARAMAMAMMAMMA", params.toString());
                return params;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onRefresh() {

        ns2 = new NetworkStatus();
        isOnline2 = ns2.isOnline(getActivity());
        if (isOnline2) {

            if (!hasGPSDevice(getActivity())) {
                Toast.makeText(getActivity(), "Gps not Supported", Toast.LENGTH_SHORT).show();
            }
            final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(getActivity())) {
                //Toast.makeText(getActivity(),"Gps not enabled",Toast.LENGTH_SHORT).show();
                mSwipeRefreshLayout.setRefreshing(false);
                enableLoc();
            } else {


                gps1 = new GPSTrackers(getContext());
                latitude = gps1.getLatitude();
                longitude = gps1.getLongitude();

                geocoder = new Geocoder(getActivity(), Locale.getDefault());

                try {
                    cuurentaddresses = geocoder.getFromLocation(latitude, longitude, 1);

                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                lat = String.valueOf(latitude);
                lng = String.valueOf(longitude);
                // showlabslistList = new ArrayList<ShowLabsListModel>();
                showlabslistrecyclerview = (RecyclerView) rootView.findViewById(R.id.showlabslist_recycler_view);
                showlabslistrecyclerview.setHasFixedSize(true);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                showlabslistrecyclerview.setLayoutManager(layoutManager);
                adapter = new ShowLabsListAdapter(showlabslistList, LabsFragments.this, R.layout.row_labs_list4);

                showlabslistrecyclerview.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
                //or
                showlabslistrecyclerview.addItemDecoration(new DividerItemDecoration(getActivity()));
                //or
                showlabslistrecyclerview.addItemDecoration(
                        new DividerItemDecoration(getActivity(), R.drawable.recycler_divider));
                if (filterFlag == 0) {

                    String filterString = "distance_in_km asc";
                    //getLabsList(filterString);
                    getLabsList(filterString);

                    if (cuurentaddresses.size() != 0) {
                        //address = cuurentaddresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        city = cuurentaddresses.get(0).getLocality();
                        //state = cuurentaddresses.get(0).getAdminArea();
                        //country = cuurentaddresses.get(0).getCountryName();
                        //pincode = cuurentaddresses.get(0).getAddressLine(1);

                        selectCity.setText(city);
                    } else {
                        //  selectCity.setText("Address");
                    }
                } else {
                    String vlaue = "distance_in_km asc";


                    gettingLabswithCityNamewithFilter(vlaue, global_lat, global_lng, selectCity.getText().toString().trim());
                }

                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        //notifyAll();
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String query) {
                        //FILTER AS YOU TYPE
                        adapter.getFilter().filter(query);

                        adapter.notifyDataSetChanged();

                        return true;
                    }
                });
                //Toast.makeText(getActivity(),"Gps already enabled",Toast.LENGTH_SHORT).show();
            }

        } else {
            progressDialog.dismiss();
            mSwipeRefreshLayout.setRefreshing(false);
            Toast.makeText(getActivity(), "No Internet Connection....", Toast.LENGTH_SHORT).show();
            noInternetConnection.setTypeface(type_lato);
            //noInternetConnection.setText("We are not in your location now, we will resume to your location shortly");
            noInternetConnection.setVisibility(View.VISIBLE);
        }


    }

    public void refreshMyList(String selectedCountry, String selected_city_lat, String selected_city_lng) {

        dialog.dismiss();

        first_time_flag = 1;
        selectCity.setText(selectedCountry);
        global_lat = selected_city_lat;
        global_lng = selected_city_lng;
        gettingLabsotherCities(selectedCountry, global_lat, global_lng);
        adapter.notifyDataSetChanged();
    }

    private boolean hasGPSDevice(Context context) {
        final LocationManager mgr = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        if (mgr == null)
            return false;
        final List<String> providers = mgr.getAllProviders();
        if (providers == null)
            return false;
        return providers.contains(LocationManager.GPS_PROVIDER);
    }
}