package innasoft.com.mhcone.fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import innasoft.com.mhcone.R;

public class BookAppointmentFragment extends Fragment {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;

    public BookAppointmentFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_updates, container, false);

        mFragmentManager = getFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView23,new SubTabFragment()).commit();

        return view;
    }
}
