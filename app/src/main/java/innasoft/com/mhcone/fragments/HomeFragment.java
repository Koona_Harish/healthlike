package innasoft.com.mhcone.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.Sliding;
import innasoft.com.mhcone.activities.TabLayoutActivity;
import innasoft.com.mhcone.adapters.PopularLabAdapter;
import innasoft.com.mhcone.adapters.PopularPackageAdapter;
import innasoft.com.mhcone.adapters.PopularTestAdapter;
import innasoft.com.mhcone.models.PopularLabModel;
import innasoft.com.mhcone.models.PopularPackageModel;
import innasoft.com.mhcone.models.PopularTestModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.NetworkChecking;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static android.support.design.R.dimen.abc_action_button_min_width_material;

public class HomeFragment extends Fragment {

    View rootView;
    Typeface typeface,typeface2;
    TextView packages_txt,tests_txt,labs_txt,packages_text,labs_text,tests_text,searchby;
    private boolean checkInternet;
    ProgressDialog progressDialog;

    /*Home Search*/
    Spinner spinner_list;
    String selectedSubject;

    /*Packages*/
    RecyclerView packages_recyclerview;
    PopularPackageAdapter popularPackageAdapter;
    ArrayList<PopularPackageModel> popularPackageModels = new ArrayList<PopularPackageModel>();

    /*Tests*/
    RecyclerView tests_recyclerview;
    PopularTestAdapter popularTestAdapter;
    ArrayList<PopularTestModel> popularTestModels = new ArrayList<PopularTestModel>();

    /*Labs*/
    RecyclerView labs_recyclerview;
    PopularLabAdapter popularLabAdapter;
    ArrayList<PopularLabModel> popularLabModels = new ArrayList<PopularLabModel>();
    ArrayAdapter<String> spinnerArrayAdapter;
    int key=0;
    Sliding popup;
    public HomeFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_home, container, false);

        typeface = Typeface.createFromAsset(getContext().getAssets(), "robotoregular.ttf");
        typeface2 = Typeface.createFromAsset(getContext().getAssets(), "robotobold.ttf");

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
       //  popup = (Sliding) rootView.findViewById(R.id.sliding1);
       // popup.setVisibility(View.GONE);
       /* packages_txt = (TextView) rootView.findViewById(R.id.packages_txt);
        packages_txt.setTypeface(typeface2);
        tests_txt = (TextView) rootView.findViewById(R.id.tests_txt);
        tests_txt.setTypeface(typeface2);
        labs_txt = (TextView) rootView.findViewById(R.id.labs_txt);
        labs_txt.setTypeface(typeface2);
        packages_text = (TextView) rootView.findViewById(R.id.packages_text);
        packages_text.setTypeface(typeface2);*/
        /*searchby = (TextView) rootView.findViewById(R.id.searchby);
        searchby.setTypeface(typeface2);
        packages_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go=new Intent(getActivity(),TabLayoutActivity.class);
                Bundle b=new Bundle();
                b.putString("condition","PACKAG");
                go.putExtras(b);
                startActivity(go);
            }
        });*/
        /*searchby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(key==0){
                    key=1;
                    popup.setVisibility(View.VISIBLE);


                    *//*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        circleReveal(R.id.sliding1,1,true,false);
                    }*//*
                }
                else if(key==1){
                    key=0;
                    popup.setVisibility(View.GONE);
                    *//*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        circleReveal(R.id.sliding1,1,true,true);
                    }*//*
                }
            }
        });*/
        /*labs_text = (TextView) rootView.findViewById(R.id.labs_text);
        labs_text.setTypeface(typeface2);
        labs_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go=new Intent(getActivity(),TabLayoutActivity.class);
                Bundle b=new Bundle();
                b.putString("condition","LABS");
                go.putExtras(b);
                startActivity(go);
            }
        });
        tests_text = (TextView) rootView.findViewById(R.id.tests_text);
        tests_text.setTypeface(typeface2);
        tests_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go=new Intent(getActivity(),TabLayoutActivity.class);
                Bundle b=new Bundle();
                b.putString("condition","TEST");
                go.putExtras(b);
                startActivity(go);
            }
        });*/
        spinner_list = (Spinner) rootView.findViewById(R.id.spinner_list);

        String[] listSubjects = new String[]
                {
                        "Search by",
                        "Packages",
                        "Tests",
                        "Labs",
                };

        ArrayList<String> listallSubjects=new ArrayList<String>(Arrays.asList(listSubjects));
        spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(),R.layout.spinner_row,listallSubjects);
        spinner_list.setAdapter(spinnerArrayAdapter);

        spinner_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if(position == 0)
                {

                }
                if(position==1)
                {
                    Intent go=new Intent(getActivity(),TabLayoutActivity.class);
                    Bundle b=new Bundle();
                    b.putString("condition","PACKAG");
                    go.putExtras(b);
                    startActivity(go);

                }
                if(position==2)
                {
                    Intent go=new Intent(getActivity(),TabLayoutActivity.class);
                    Bundle b=new Bundle();
                    b.putString("condition","TEST");

                    go.putExtras(b);
                    startActivity(go);
                }
                if(position==3)
                {
                    Intent go=new Intent(getActivity(),TabLayoutActivity.class);
                    Bundle b=new Bundle();
                    b.putString("condition","LABS");


                    go.putExtras(b);
                    startActivity(go);
                }

                /*if (position == 0)
                {

                }
                if (position == 1)
                {
                    Intent intent = new Intent(getActivity(), SearchPackageActivity.class);
                    startActivity(intent);
                }else
                    {
                    selectedSubject = (String) parent.getItemAtPosition(position);

                    Intent intent = new Intent(getActivity(), SearchPackageActivity.class);
                    startActivity(intent);
                }*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        popularPackageModels = new ArrayList<PopularPackageModel>();
        packages_recyclerview = (RecyclerView) rootView.findViewById(R.id.packages_recyclerview);
        popularPackageAdapter = new PopularPackageAdapter(popularPackageModels , HomeFragment.this, R.layout.row_home_packages);
        packages_recyclerview.setLayoutManager(new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false));
        packages_recyclerview.setNestedScrollingEnabled(false);
        packages_recyclerview.setSaveFromParentEnabled(true);
      //  packages_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        popularTestModels = new ArrayList<PopularTestModel>();
        tests_recyclerview = (RecyclerView) rootView.findViewById(R.id.tests_recyclerview);
        popularTestAdapter = new PopularTestAdapter(popularTestModels , HomeFragment.this, R.layout.row_home_tests);
        tests_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        popularLabModels = new ArrayList<PopularLabModel>();
        labs_recyclerview = (RecyclerView) rootView.findViewById(R.id.labs_recyclerview);
        popularLabAdapter = new PopularLabAdapter(popularLabModels , HomeFragment.this, R.layout.row_home_labs_dummy);
        labs_recyclerview.setLayoutManager(new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false));
        labs_recyclerview.setNestedScrollingEnabled(false);
        labs_recyclerview.setSaveFromParentEnabled(true);
       // labs_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        getPopularPackages();
        getPopularTests();
        getPopularLabs();

        return rootView;
    }

    private void getPopularPackages() {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet)
        {
            progressDialog.show();

            String url = AppUrls.BASE_URL+AppUrls.POPULAR_PACKAGES;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.POPULAR_PACKAGES,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response)
                        {
                            Log.d("POPULARPACKAGES",response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                /*String status = jsonObject.getString("status");
                                if (status.equals("10100")) {*/

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        PopularPackageModel ppm = new PopularPackageModel();
                                        ppm.setId(jsonObject1.getString("id"));
                                        ppm.setPackage_name(jsonObject1.getString("package_name"));
                                        ppm.setPackage_image(AppUrls.IMAGE_URL + jsonObject1.getString("package_image"));

                                        popularPackageModels.add(ppm);

                                        Log.d("DATAAAAA",popularPackageModels.toString());
                                    }
                                    packages_recyclerview.setAdapter(popularPackageAdapter);
                                    progressDialog.dismiss();

                                //}

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<String, String>();
                    params.put("limit", "10");
                    Log.d("SHOWWWWWWWWWW",params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        }
    }

    private void getPopularTests() {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet)
        {
            progressDialog.show();

            String url = AppUrls.BASE_URL+AppUrls.POPULAR_TESTS;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.POPULAR_TESTS,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response)
                        {
                            Log.d("POPULARTESTS",response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                /*String status = jsonObject.getString("status");
                                if (status.equals("10100")) {*/

                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                    PopularTestModel ppm = new PopularTestModel();
                                    ppm.setId(jsonObject1.getString("id"));
                                    ppm.setTest_name(jsonObject1.getString("test_name"));
                                    ppm.setTest_image(AppUrls.IMAGE_URL + jsonObject1.getString("test_image"));

                                    popularTestModels.add(ppm);

                                    Log.d("DATAAAAA",popularTestModels.toString());
                                }
                                tests_recyclerview.setAdapter(popularTestAdapter);
                                progressDialog.dismiss();

                                //}

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<String, String>();
                    params.put("limit", "5");
                    Log.d("SHOWWWWWWWWWW",params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        }
    }

    private void getPopularLabs() {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet)
        {
            progressDialog.show();

            String url = AppUrls.BASE_URL+AppUrls.POPULAR_LABS;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.POPULAR_LABS,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response)
                        {
                            Log.d("POPULARPACKAGES",response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                /*String status = jsonObject.getString("status");
                                if (status.equals("10100")) {*/

                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                    PopularLabModel ppm = new PopularLabModel();
                                    ppm.setId(jsonObject1.getString("id"));
                                    ppm.setUser_name(jsonObject1.getString("user_name"));
                                    ppm.setLat(jsonObject1.getString("lat"));
                                    ppm.setLang(jsonObject1.getString("lang"));
                                    ppm.setMobile(jsonObject1.getString("mobile"));
                                    ppm.setTele_booking(jsonObject1.getString("tele_booking"));
                                    ppm.setPayment_lab(jsonObject1.getString("payment_lab"));
                                    ppm.setPayment_online(jsonObject1.getString("payment_online"));
                                    ppm.setProfile_pic(AppUrls.IMAGE_URL + jsonObject1.getString("profile_pic"));

                                    popularLabModels.add(ppm);

                                    Log.d("DATAAAAA",popularLabModels.toString());
                                }
                                labs_recyclerview.setAdapter(popularLabAdapter);
                                progressDialog.dismiss();

                                //}

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<String, String>();
                    params.put("limit", "5");
                    Log.d("SHOWWWWWWWWWW",params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        }
    }
    
    @Override
    public void onResume(){
        super.onResume();
        spinner_list.setAdapter(spinnerArrayAdapter);

    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void circleReveal(int viewID, int posFromRight, boolean containsOverflow, final boolean isShow)
    {

        final View myView = rootView.findViewById(viewID);
        int width=myView.getWidth();

        if(posFromRight>0)


        if(containsOverflow)
            width-=getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material);

        int cx=width;
        int cy=myView.getHeight()/2;

        Animator anim;
        if(isShow)
            anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0,(float)width);
        else
            anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, (float)width, 0);

        anim.setDuration((long)220);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if(!isShow)
                {
                    super.onAnimationEnd(animation);
                    myView.setVisibility(View.INVISIBLE);
                }
            }
        });

        // make the view visible and start the animation
        if(isShow)
            myView.setVisibility(View.VISIBLE);

        // start the animation
        anim.start();


    }
}
