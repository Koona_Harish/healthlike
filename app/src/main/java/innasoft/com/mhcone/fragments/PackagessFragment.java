package innasoft.com.mhcone.fragments;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.PackagesSubTests;
import innasoft.com.mhcone.activities.SubPackagesActivity;
import innasoft.com.mhcone.activities.SubTestsActivity;
import innasoft.com.mhcone.adapters.PackagesAdapter;
import innasoft.com.mhcone.adapters.TestsAdapter;
import innasoft.com.mhcone.dbhelper.MyHealthDBHelper;
import innasoft.com.mhcone.holders.PackagesHolder;
import innasoft.com.mhcone.models.PackagesModel;
import innasoft.com.mhcone.models.TestsModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.ConnectionDetector;
import innasoft.com.mhcone.utilities.DividerItemDecoration;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.NetworkUtil;
import innasoft.com.mhcone.utilities.UserSessionManager;
import innasoft.com.mhcone.utilities.VerticalSpaceItemDecoration;

import static innasoft.com.mhcone.R.drawable.view;

public class PackagessFragment extends Fragment implements  SwipeRefreshLayout.OnRefreshListener
{

    ProgressDialog progressDialog;
    ArrayList<PackagesModel> packagesModels;
    PackagesAdapter adapter;
    public RecyclerView packagesrecyclerView;
    UserSessionManager userSessionManager;
    ConnectionDetector cd;
    Boolean isInternetPresent = false;
    Typeface typeface;
    TextView defaultTExt,noInternetConnection;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    SearchView sv;
    View view;
    ImageView no_packages_found_iv;

    private static final int VERTICAL_ITEM_SPACE = 0;

    NetworkStatus ns,ns2,ns3;
    Boolean isOnline = false,isOnline2 = false,isOnline3 = false;

    private boolean statusFlag;
    MyHealthDBHelper myHealthDBHelper;

    public PackagessFragment() {

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {

        view = inflater.inflate(R.layout.fragment_packages, container, false);

        myHealthDBHelper = new MyHealthDBHelper(getActivity());

        ns = new NetworkStatus();
        isOnline = ns.isOnline(getActivity());
        packagesrecyclerView = (RecyclerView) view.findViewById(R.id.packages_recycler_view);

        mSwipeRefreshLayout = (SwipeRefreshLayout)view. findViewById(R.id.packages_frag_swipe_layout);

        packagesModels = new ArrayList<PackagesModel>();

        typeface = Typeface.createFromAsset(getContext().getAssets(), getResources().getString(R.string.robotobold));

        progressDialog = new ProgressDialog(getActivity(), R.style.DialogTheme);
        progressDialog.setMessage("Please wait......");

        no_packages_found_iv = (ImageView) view.findViewById(R.id.no_packages_found_iv);

        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,

                android.R.color.holo_green_light,

                android.R.color.holo_orange_light,

                android.R.color.holo_red_light);

        progressDialog.setCancelable(false);

        userSessionManager = new UserSessionManager(getActivity());
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();
        sv= (SearchView) view.findViewById(R.id.mSearch);
        sv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sv.setIconified(false);
            }
        });
        defaultTExt = (TextView)view.findViewById(R.id.defaultText);
        noInternetConnection = (TextView)view.findViewById(R.id.noInternetConnect);


        packagesrecyclerView.setHasFixedSize(true);

        if(isOnline) {


            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
            packagesrecyclerView.setLayoutManager(layoutManager);
            adapter = new PackagesAdapter(packagesModels , PackagessFragment.this, R.layout.row_packages_items);
            packagesrecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
            packagesrecyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
            packagesrecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.recycler_divider));
            getttingPackagesList();

        }
        else {
            retrievePackageList();
            showInternetStatus();
        }

        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(this);
        return view;
    }

    private void showInternetStatus() {
        progressDialog.dismiss();
        noInternetConnection.setTypeface(typeface);
        //noInternetConnection.setVisibility(View.VISIBLE);
    }

    private void getttingPackagesList()
    {
/*
        ns3 = new NetworkStatus();
        isOnline3 = ns3.isOnline(getActivity());
        if (isOnline3) {*/

            noInternetConnection.setVisibility(View.GONE);
        no_packages_found_iv.setVisibility(View.GONE);
            if (statusFlag = NetworkUtil.isConnected(getActivity())) {
                StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.PACKAGES_URL,
                        new Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {
                                packagesModels.clear();
                                myHealthDBHelper.deleteAllPackages();

                                Log.d("Purushotham", "---Sucesses-PackagesList : " + response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    ContentValues values = new ContentValues();
                                    if (jsonArray.length() != 0) {
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("test-sub");

                                            PackagesModel pm=new PackagesModel();
                                            pm.setP_id(jsonObject1.getString("id"));
                                            pm.setPackage_id(jsonObject1.getString("package_id"));
                                            pm.setP_name(jsonObject1.getString("package_name"));
                                            packagesModels.add(pm);

                                            values.put(MyHealthDBHelper.P_ID, jsonObject1.getString("id"));
                                            values.put(MyHealthDBHelper.PACKAGE_IMAGE, AppUrls.IMAGE_URL + jsonObject1.getString("package_image"));
                                            values.put(MyHealthDBHelper.PACKAGE_NAME, jsonObject1.getString("package_name"));
                                            values.put(MyHealthDBHelper.PACKAGE_ID, jsonObject1.getString("package_id"));
                                            values.put(MyHealthDBHelper.PACKAGE_STATUS, jsonObject1.getString("status"));

                                            myHealthDBHelper.addPackagesList(values);

                                            /*PackagesModel pm = new PackagesModel();
                                            pm.setP_id(jsonObject1.getString("id"));
                                            pm.setP_images_url(AppUrls.BASE_URL + jsonObject1.getString("package_image"));
                                            pm.setP_name(jsonObject1.getString("package_name"));
                                            pm.setP_status(jsonObject1.getString("status"));
                                            packagesModels.add(pm);*/

                                        }
                                        retrievePackageList();
                                        //packagesrecyclerView.setAdapter(adapter);
                                        mSwipeRefreshLayout.setRefreshing(false);
                                    } else {
                                        progressDialog.dismiss();
                                        mSwipeRefreshLayout.setRefreshing(false);
                                        no_packages_found_iv.setVisibility(View.VISIBLE);
                                        noInternetConnection.setText("No Packages found.");
                                        noInternetConnection.setVisibility(View.VISIBLE);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                mSwipeRefreshLayout.setRefreshing(false);
                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        })
                {

                };
                RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                requestQueue.add(stringRequest);
            }else {
                retrievePackageList();
                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getActivity(),"No Internet Connection....",Toast.LENGTH_SHORT).show();
                showInternetStatus();
            }
      /*  }else {
            progressDialog.dismiss();
            mSwipeRefreshLayout.setRefreshing(false);
            Toast.makeText(getActivity(),"No Internet Connection....",Toast.LENGTH_SHORT).show();
            showInternetStatus();
        }*/
    }

    private void  retrievePackageList()
    {
        packagesModels.clear();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        packagesrecyclerView.setLayoutManager(layoutManager);
        adapter = new PackagesAdapter(packagesModels , PackagessFragment.this, R.layout.row_packages_items);

        packagesrecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        packagesrecyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        packagesrecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.recycler_divider));

        MyHealthDBHelper db = new MyHealthDBHelper(getActivity());
        List<String> p_Id = db.getP_Id();
        List<String> p_Name = db.getP_Name();
        List<String> p_Image = db.getP_Image();

        List<String> p_Status = db.getP_Status();


        for (int i = 0; i < p_Id.size(); i++){
            PackagesModel pm = new PackagesModel();
            pm.setP_id(p_Id.get(i));
            pm.setP_name(p_Name.get(i));
            pm.setP_images_url(p_Image.get(i));

            pm.setP_status(p_Status.get(i));

            packagesModels.add(pm);
        }
        packagesrecyclerView.setAdapter(adapter);
        db.close();
    }

    public void cardViewListener(int position)
    {

        if(userSessionManager.checkLogin() != false)
        {
            Toast.makeText(getActivity(), "Please Login...!", Toast.LENGTH_SHORT).show();
        }
        else
            {
            Intent intent = new Intent(getActivity(), PackagesSubTests.class);
            intent.putExtra("package_id", packagesModels.get(position).getPackage_id());
            intent.putExtra("package_name", packagesModels.get(position).getP_name());

                Log.d("PACKINFOFRAG:", packagesModels.get(position).getPackage_id()+" / "+packagesModels.get(position).getP_name());
            startActivity(intent);
        }
    }

    @Override
    public void onRefresh()
    {
        ns2 = new NetworkStatus();
        isOnline2 = ns2.isOnline(getActivity());
        if (isOnline2)
        {
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
            packagesrecyclerView.setLayoutManager(layoutManager);
            adapter = new PackagesAdapter(packagesModels , PackagessFragment.this, R.layout.row_packages_items);

            packagesrecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
            packagesrecyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
            packagesrecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.recycler_divider));
            getttingPackagesList();

        }
        else {

            progressDialog.dismiss();
            mSwipeRefreshLayout.setRefreshing(false);
           // Toast.makeText(getActivity(),"No Internet Connection....",Toast.LENGTH_SHORT).show();
            showInternetStatus();
            retrievePackageList();
        }
    }
}
