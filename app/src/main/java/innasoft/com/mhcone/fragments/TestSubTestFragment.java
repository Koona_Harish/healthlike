package innasoft.com.mhcone.fragments;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.apache.http.HttpEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.PackagesSubTests;
import innasoft.com.mhcone.activities.ShowLabsTest;
import innasoft.com.mhcone.adapters.CustomerAdapter;
import innasoft.com.mhcone.adapters.GetMemGroupAdapter;
import innasoft.com.mhcone.adapters.MyExpandableListAdapter;
import innasoft.com.mhcone.adapters.TestSubAdapter;
import innasoft.com.mhcone.dbhelper.MyHealthDBHelper;
import innasoft.com.mhcone.dbhelper.MyHealthTestSubTestDBHelper;
import innasoft.com.mhcone.dbhelper.NewMyHealthTestSubTestDBHelper;
import innasoft.com.mhcone.models.GetMemInGroupModel;
import innasoft.com.mhcone.models.MainSubtestModel;
import innasoft.com.mhcone.models.MainTestsModel;
import innasoft.com.mhcone.models.NewTestsModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.ConnectionDetector;
import innasoft.com.mhcone.utilities.DBHandler;
import innasoft.com.mhcone.utilities.DividerItemDecoration;
import innasoft.com.mhcone.utilities.GPSTrackers;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.NetworkUtil;
import innasoft.com.mhcone.utilities.StartLocationAlert;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class TestSubTestFragment extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    public ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<MainSubtestModel> subItemList = null;
    List<MainTestsModel> superitemList = null;
    MainSubtestModel subtestModel = null;
    MainTestsModel superItem = null;
    Button get, clear_data;
    DBHandler db;
    ImageView no_test_found_iv;
    ProgressDialog progressDialog;
    ConnectionDetector cd;
    GPSTrackers gps;
    StartLocationAlert startLocationAlert;
    private int lastExpandedPosition = -1;
    Typeface typeface, typebold;
    TextView defaultTExt, noInternetConnection;
    UserSessionManager userSessionManager;
    String access_key, disp_userName, disp_email;
    String userID;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    View view;
    Toast toast;
    MyHealthTestSubTestDBHelper myHealthTestSubTestDBHelper;
    ArrayList<Object> arraylistsubtestNamesObject;
    ArrayList<Object> arraylistsubtestIdsObject;
    ArrayList<Object> arraylisttestIdsObject;

    Boolean isInternetPresent = false;
    NetworkStatus ns, ns2, ns3;
    Boolean isOnline = false, isOnline2 = false, isOnline3 = false;
    SearchView mSearch;
    private boolean statusFlag;
    RecyclerView getaddMemRecycler;
    LinearLayoutManager lLMan;
    GetMemGroupAdapter getMemAdap;
    public static ArrayList<GetMemInGroupModel> getMemList = new ArrayList<GetMemInGroupModel>();
    String selected_gmNames, selected_gmIds, update_key, update_group_id;
    HttpEntity resEntity;
    GridLayoutManager gridLayoutManagerVertical;
    //////////////////////////////////////
    public RecyclerView packagesrecyclerView;
    TestSubAdapter adapter;
    ArrayList<NewTestsModel> packagesModels;
    MyHealthDBHelper myHealthDBHelper;
    NewMyHealthTestSubTestDBHelper newdb;
    //////////////////////////////////
    ArrayList<String> gm_ids = new ArrayList<String>();
    ArrayList<String> gm_names = new ArrayList<String>();
    ArrayList<String> gm_sub_ids = new ArrayList<String>();
    ArrayList<String> gm_ids_list = new ArrayList<String>();
    ArrayList<String> gm_sub_ids_list = new ArrayList<String>();
    ArrayList<String> gm_test_names = new ArrayList<String>();
    AutoCompleteTextView search_auto;
    //    ArrayAdapter<String> auto_adapter;
    CustomerAdapter auto_adapter;

    public TestSubTestFragment() {

    }

    List<String> CityData;
    List<String> CityData1;
    Button search_lab;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_test_sub_test, container, false);
        packagesrecyclerView = (RecyclerView) view.findViewById(R.id.packages_recycler_view);
        search_auto = (AutoCompleteTextView) view.findViewById(R.id.search_auto);
        search_lab = (Button) view.findViewById(R.id.search_lab);
        search_lab.setOnClickListener(this);
        packagesModels = new ArrayList<NewTestsModel>();
        CityData = new ArrayList<>();
        CityData1 = new ArrayList<>();

        packagesrecyclerView.setHasFixedSize(true);
        myHealthDBHelper = new MyHealthDBHelper(getActivity());
//        auto_adapter = new ArrayAdapter<String>(getContext(),
//                android.R.layout.simple_dropdown_item_1line, CityData);

        auto_adapter = new CustomerAdapter(getActivity(), packagesModels);

        search_auto.setThreshold(2);

/*        search_auto.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                auto_adapter.getFilter().filter(editable.toString());
                search_auto.showDropDown();
            }
        });*/

        /*search_auto.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {



                Toast.makeText(getContext(), packagesModels.get(position).getTest_sub_name() + "----" + packagesModels.get(position).getTest_id() + " ---- " + packagesModels.get(position).getId(), Toast.LENGTH_SHORT).show();

                Toast.makeText(getContext(), arg0.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();

                Log.d("your selected item", "" + arg0.getItemAtPosition(position));

                gm_ids.add(packagesModels.get(position).getTest_id());
                HashSet hs = new HashSet();
                hs.addAll(gm_ids);
                gm_ids.clear();
                gm_ids.addAll(hs);

                gm_names.add(packagesModels.get(position).getTest_sub_name());
                HashSet hs1 = new HashSet();
                hs1.addAll(gm_names);
                gm_names.clear();
                gm_names.addAll(hs1);

                gm_sub_ids.add(packagesModels.get(position).getId());
                HashSet hs2 = new HashSet();
                hs1.addAll(gm_sub_ids);
                gm_sub_ids.clear();
                gm_sub_ids.addAll(hs2);

                setGmName(gm_ids, gm_names, gm_sub_ids);
                search_auto.setText("");
                //s1.get(position) is name selected from autocompletetextview
                // now you can show the value on textview.
            }
        });*/
        newdb = new NewMyHealthTestSubTestDBHelper(getActivity());
        typeface = Typeface.createFromAsset(this.getActivity().getAssets(), "robotoregular.ttf");
        typebold = Typeface.createFromAsset(this.getActivity().getAssets(), "robotobold.ttf");

        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> user = userSessionManager.getUserDetails();
        access_key = user.get(UserSessionManager.KEY_ACCSES);

        disp_userName = user.get(UserSessionManager.USER_NAME);
        disp_email = user.get(UserSessionManager.USER_EMAIL);
        userID = user.get(UserSessionManager.USER_ID);

        no_test_found_iv = (ImageView) view.findViewById(R.id.no_test_found_iv);

        defaultTExt = (TextView) view.findViewById(R.id.defaultText);
        noInternetConnection = (TextView) view.findViewById(R.id.noInternetConnect);
        noInternetConnection.setOnClickListener(this);

        myHealthTestSubTestDBHelper = new MyHealthTestSubTestDBHelper(getActivity());
        mSearch = (SearchView) view.findViewById(R.id.mSearch);
        expandableListView = (ExpandableListView) view.findViewById(R.id.expandableListView);

        db = new DBHandler(getActivity());
        get = (Button) view.findViewById(R.id.getData);
        get.setTypeface(typebold);

        clear_data = (Button) view.findViewById(R.id.clear_data);
        clear_data.setTypeface(typebold);

        get.setOnClickListener(this);
        clear_data.setOnClickListener(this);

        ns = new NetworkStatus();
        isOnline = ns.isOnline(getActivity());

        progressDialog = new ProgressDialog(getActivity(), R.style.DialogTheme);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.test_sub_tests_frag_swipe_layout);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,

                android.R.color.holo_green_light,

                android.R.color.holo_orange_light,

                android.R.color.holo_red_light);

        progressDialog.setMessage("Please wait......");
        progressDialog.setCancelable(false);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1 && groupPosition != lastExpandedPosition) {
                    expandableListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

        if (isOnline) {
            fillData();
        } else {
            retriveTestsSubTests();
        }
        if (isOnline) {


            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
            packagesrecyclerView.setLayoutManager(layoutManager);
            adapter = new TestSubAdapter(packagesModels, TestSubTestFragment.this, R.layout.row_packages_items);
            //  packagesrecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
            packagesrecyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
            packagesrecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.recycler_divider));
            getttingPackagesList();

        } else {
            retrievePackageList();
            showInternetStatus();
        }
        mSwipeRefreshLayout.setOnRefreshListener(this);


        expandableListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                boolean enable = false;
                if (expandableListView != null && expandableListView.getChildCount() > 0) {
                    boolean firstItemVisible = expandableListView.getFirstVisiblePosition() == 0;
                    boolean topOfFirstItemVisible = expandableListView.getChildAt(0).getTop() == 0;
                    enable = firstItemVisible && topOfFirstItemVisible;
                }
                mSwipeRefreshLayout.setEnabled(enable);
            }
        });
        mSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });
        getaddMemRecycler = (RecyclerView) view.findViewById(R.id.getaddMemRecycler);
        getMemAdap = new GetMemGroupAdapter(getMemList, getActivity(), R.layout.row_get_mem_in_group);
        lLMan = new LinearLayoutManager(getContext());
        getaddMemRecycler.setNestedScrollingEnabled(false);
        getaddMemRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        StaggeredGridLayoutManager gridLayoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        gridLayoutManagerVertical = new GridLayoutManager(getContext(), 3, LinearLayoutManager.VERTICAL, false);
        gridLayoutManager.setReverseLayout(true);
        getaddMemRecycler.setLayoutManager(gridLayoutManagerVertical);

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver, new IntentFilter("custom-event-name"));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(removeReceiver, new IntentFilter("remove-data"));

        return view;
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String name = intent.getStringExtra("SubName");
            String id = intent.getStringExtra("id");
            String testId = intent.getStringExtra("testId");

            if (!gm_names.contains(name)) {
                gm_ids.add(testId);
                gm_names.add(name);
                gm_sub_ids.add(id);
            }
//            HashSet hs = new HashSet();
//            hs.addAll(gm_ids);
//            gm_ids.clear();
//            gm_ids.addAll(hs);
//
//            HashSet hs1 = new HashSet();
//            hs1.addAll(gm_names);
//            gm_names.clear();
//            gm_names.addAll(hs1);
//
//            HashSet hs2 = new HashSet();
//            hs2.addAll(gm_sub_ids);
//            gm_sub_ids.clear();
//            gm_sub_ids.addAll(hs2);

            setGmName(gm_ids, gm_names, gm_sub_ids);
            search_auto.setText("");
        }
    };


    private void showInternetStatus() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
        TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
        textView.setTypeface(typeface);
        Toast toast = new Toast(getActivity());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(toastLayout);
        toast.show();
    }

    private void showInternetStatus2() {
        progressDialog.dismiss();
        mSwipeRefreshLayout.setRefreshing(false);
        noInternetConnection.setTypeface(typeface);
        noInternetConnection.setVisibility(View.VISIBLE);
    }

    private void fillData() {
        ns = new NetworkStatus();
        no_test_found_iv.setVisibility(View.GONE);
        isOnline = ns.isOnline(getActivity());
        if (isOnline) {
            progressDialog.show();
            noInternetConnection.setVisibility(View.GONE);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GETTING_TESTS_AND_SUBTESTS,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                myHealthTestSubTestDBHelper.deleteAllTests();
                                myHealthTestSubTestDBHelper.deleteAllSubTests();
                                JSONObject jsonObject = new JSONObject(response);

                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                ContentValues values = new ContentValues();
                                if (jsonArray.length() != 0) {

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        values.put(MyHealthTestSubTestDBHelper.TEST_ID, jsonObject1.getString("test_id"));
                                        values.put(MyHealthTestSubTestDBHelper.TEST_IMAGE, AppUrls.IMAGE_URL + jsonObject1.getString("test_image"));
                                        values.put(MyHealthTestSubTestDBHelper.TEST_NAME, jsonObject1.getString("test_name"));

                                        myHealthTestSubTestDBHelper.addTestsList(values);

                                        JSONArray jsonArray1 = jsonObject1.getJSONArray("sub_test_wise");
                                        if (jsonArray1 != null && jsonArray1.length() != 0) {
                                            ContentValues values2 = new ContentValues();
                                            for (int j = 0; j < jsonArray1.length(); j++) {
                                                JSONObject jsonObject2 = jsonArray1.getJSONObject(j);
                                                values2.put(MyHealthTestSubTestDBHelper.S_TEST_ID, jsonObject2.getString("test_id"));
                                                values2.put(MyHealthTestSubTestDBHelper.S_SUB_TEST_ID, jsonObject2.getString("sub_test_id"));
                                                values2.put(MyHealthTestSubTestDBHelper.S_SUB_TEST_NAME, jsonObject2.getString("sub_test_name"));
                                                myHealthTestSubTestDBHelper.addSubTestsList(values2);
                                            }

                                            progressDialog.dismiss();
                                            mSwipeRefreshLayout.setRefreshing(false);
                                        }
                                    }

                                    retriveTestsSubTests();

                                } else {
                                    progressDialog.dismiss();
                                    mSwipeRefreshLayout.setRefreshing(false);
                                    noInternetConnection.setTypeface(typeface);
                                    no_test_found_iv.setVisibility(View.VISIBLE);
                                    noInternetConnection.setText("No Tests Found");
                                    noInternetConnection.setVisibility(View.VISIBLE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            mSwipeRefreshLayout.setRefreshing(false);
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }
            );
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        } else {
            progressDialog.dismiss();
            mSwipeRefreshLayout.setRefreshing(false);
            noInternetConnection.setTypeface(typeface);
            noInternetConnection.setVisibility(View.VISIBLE);
        }
    }

    private void retriveTestsSubTests() {

        superitemList = new ArrayList<MainTestsModel>();

        MyHealthTestSubTestDBHelper db = new MyHealthTestSubTestDBHelper(getActivity());
        List<String> test_ids = db.getTest_Id();
        List<String> test_names = db.getTest_Name();
        List<String> test_images = db.getTest_Image();

        for (int i = 0; i < test_ids.size(); i++) {
            superItem = new MainTestsModel();
            subItemList = new ArrayList<MainSubtestModel>();

            superItem.setTest_name(test_names.get(i));
            superItem.setTest_id(test_ids.get(i));
            superItem.setTest_image(test_images.get(i));

            List<String> s_test_id = db.getS_Test_Id(test_ids.get(i));
            List<String> s_sub_test_id = db.getS_Sub_Test_Id(test_ids.get(i));
            List<String> s_sub_test_name = db.getS_Sub_Test_Name(test_ids.get(i));

            for (int j = 0; j < s_test_id.size(); j++) {
                subtestModel = new MainSubtestModel();

                subtestModel.setSub_test_name(s_sub_test_name.get(j));
                subtestModel.setTest_id(s_test_id.get(j));
                subtestModel.setSub_test_id(s_sub_test_id.get(j));
                subItemList.add(subtestModel);
            }
            superItem.setSubtestModel(subItemList);
            superitemList.add(superItem);


        }
        expandableListAdapter = new MyExpandableListAdapter(TestSubTestFragment.this, superitemList);
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Toast.makeText(getActivity(), superitemList.get(groupPosition).getTest_id() + " : " + superitemList.get(groupPosition).getSubtestModel().get(childPosition).getSub_test_id(), Toast.LENGTH_SHORT).show();
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == noInternetConnection) {
            expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                @Override
                public void onGroupExpand(int groupPosition) {
                    if (lastExpandedPosition != -1 && groupPosition != lastExpandedPosition) {
                        expandableListView.collapseGroup(lastExpandedPosition);
                    }
                    lastExpandedPosition = groupPosition;
                }
            });
            fillData();
        }
        if (v == get) {
            ns3 = new NetworkStatus();
            isOnline3 = ns3.isOnline(getActivity());
            if (isOnline3) {
                List<String> listSubTestIds = db.getAllSubTestIds();

                gps = new GPSTrackers(getActivity());

                if (listSubTestIds.size() != 0) {
                    progressDialog.show();

                    if (gps.canGetLocation()) {
                        if (userSessionManager.checkLogin() != false) {
                            Toast.makeText(getActivity(), "Please Login...!", Toast.LENGTH_SHORT).show();
                        } else {
                            double latitude = gps.getLatitude();
                            double longitude = gps.getLongitude();

                            List<String> subtestIds = db.getAllSubTestIds();
                            List<String> subtestName = db.getAllSubTestNames();
                            List<String> testIds = db.getAllTestIds();
                            String subTestIDString = subtestIds.toString();
                            String subtestNames = subtestName.toString();
                            String SendSubTestIDs = subTestIDString.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", "");
                            String sendSubtestNames = subtestNames.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", "");

                            progressDialog.dismiss();
                            mSwipeRefreshLayout.setRefreshing(false);

                            Log.d("MIRACALE", subTestIDString + "--" + subtestNames + "--" + SendSubTestIDs + "--" + sendSubtestNames + "--" + latitude + "--" + longitude);
                            Intent intent = new Intent(getActivity(), ShowLabsTest.class);
                            intent.putExtra("latValue", Double.toString(latitude));
                            intent.putExtra("longValue", Double.toString(longitude));
                            intent.putExtra("subTestId", SendSubTestIDs);
                            intent.putExtra("subTestName", sendSubtestNames);
                            Bundle args = new Bundle();
                            args.putSerializable("arraylistsubtestNames", (Serializable) subtestName);
                            args.putSerializable("arraylistsubtestIds", (Serializable) subtestIds);
                            args.putSerializable("arraylisttestIds", (Serializable) testIds);
                            intent.putExtra("bundelData", args);
                            startActivity(intent);
                        }

                    } else {
                        mSwipeRefreshLayout.setRefreshing(false);
                        progressDialog.dismiss();
                        startLocationAlert = new StartLocationAlert(getActivity());
                    }
                } else {
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_package, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
                    textView.setTypeface(typeface);
                    textView.setText("Please Select atleast one Test...!");
                    toast = new Toast(getActivity());
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setView(toastLayout);
                    toast.show();

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            toast.cancel();
                        }
                    }, 100);
                }
            } else {
                mSwipeRefreshLayout.setRefreshing(false);
                showInternetStatus();
            }
        }
        if (v == search_lab) {
            ns3 = new NetworkStatus();
            isOnline3 = ns3.isOnline(getActivity());
            if (isOnline3) {
                List<String> listSubTestIds = gm_ids_list;

                gps = new GPSTrackers(getActivity());

                if (listSubTestIds.size() != 0) {
                    progressDialog.show();

                    if (gps.canGetLocation()) {
//                        if (userSessionManager.checkLogin() != false) {
//                            Toast.makeText(getActivity(), "Please Login...!", Toast.LENGTH_SHORT).show();
//                        } else {
                        double latitude = gps.getLatitude();
                        double longitude = gps.getLongitude();

                        List<String> subtestIds = gm_sub_ids_list;
                        List<String> subtestName = gm_names;
                        List<String> testIds = gm_ids_list;
                        String subTestIDString = subtestIds.toString();
                        String subtestNames = subtestName.toString();
                        String SendSubTestIDs = subTestIDString.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", "");
                        String sendSubtestNames = subtestNames.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", "");

                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);

                        Log.d("MIRACALE", subTestIDString + "--" + subtestNames + "--" + SendSubTestIDs + "--" + sendSubtestNames + "--" + latitude + "--" + longitude);
                        Intent intent = new Intent(getActivity(), ShowLabsTest.class);
                        intent.putExtra("latValue", Double.toString(latitude));
                        intent.putExtra("longValue", Double.toString(longitude));
                        intent.putExtra("subTestId", SendSubTestIDs);
                        intent.putExtra("subTestName", sendSubtestNames);
                        Bundle args = new Bundle();
                        args.putSerializable("arraylistsubtestNames", (Serializable) subtestName);
                        args.putSerializable("arraylistsubtestIds", (Serializable) subtestIds);
                        args.putSerializable("arraylisttestIds", (Serializable) testIds);
                        intent.putExtra("bundelData", args);
                        startActivity(intent);
//                        }

                    } else {
                        mSwipeRefreshLayout.setRefreshing(false);
                        progressDialog.dismiss();
                        startLocationAlert = new StartLocationAlert(getActivity());
                    }
                } else {
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View toastLayout = inflater.inflate(R.layout.custom_toast_package, (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout));
                    TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
                    textView.setTypeface(typeface);
                    textView.setText("Please Select atleast one Test...!");
                    toast = new Toast(getActivity());
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setView(toastLayout);
                    toast.show();

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            toast.cancel();
                        }
                    }, 100);
                }
            } else {
                mSwipeRefreshLayout.setRefreshing(false);
                showInternetStatus();
            }
        }

        if (v == clear_data) {
            if (userSessionManager.checkLogin() != false) {
                Toast.makeText(getActivity(), "Please Login", Toast.LENGTH_SHORT).show();
            } else {
                List<String> listSubTestIds1 = db.getAllSubTestIds();
                List<String> listSubTestIds = db.getAllSubTestIds();
                if (listSubTestIds.size() != 0) {
                    db.deleteTableData();
                    Toast.makeText(getActivity(), "Successfully cleared your selection", Toast.LENGTH_SHORT).show();
                    retriveTestsSubTests();

                } else if (listSubTestIds1.size() != 0) {
                    db.deleteTableData();
                    Toast.makeText(getActivity(), " cleared your selection", Toast.LENGTH_SHORT).show();
                    retrievePackageList();
                    getMemList.clear();
                } else {

                    final Toast toast = Toast.makeText(getActivity(), "Invalid selection....!", Toast.LENGTH_SHORT);
                    toast.show();

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            toast.cancel();
                        }
                    }, 100);
                }
            }
        }
    }

    @Override
    public void onRefresh() {
        ns2 = new NetworkStatus();
        isOnline2 = ns2.isOnline(getActivity());
        if (isOnline2) {
            expandableListView = (ExpandableListView) view.findViewById(R.id.expandableListView);

            expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                @Override
                public void onGroupExpand(int groupPosition) {
                    if (lastExpandedPosition != -1
                            && groupPosition != lastExpandedPosition) {
                        expandableListView.collapseGroup(lastExpandedPosition);
                    }
                    lastExpandedPosition = groupPosition;
                }
            });

            fillData();

        } else {
            mSwipeRefreshLayout.setRefreshing(false);
            showInternetStatus2();
        }
    }

    /* @Override
     public void onResume() {
        // db.deleteTableData();
         if (userSessionManager.checkLogin() != false)
         {
             Toast.makeText(getActivity(), "Please Login", Toast.LENGTH_SHORT).show();
         }
         else
         {
             List<String> listSubTestIds = db.getAllSubTestIds();
             if (listSubTestIds.size() != 0) {
                 AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                 alertDialogBuilder.setMessage("Do you want Fresh start or Continue");
                 alertDialogBuilder.setPositiveButton("Fresh Start",
                         new DialogInterface.OnClickListener() {
                             @Override
                             public void onClick(DialogInterface arg0, int arg1) {
                                 db.deleteTableData();
                                 Toast.makeText(getActivity(), "Successfully cleared your selection", Toast.LENGTH_SHORT).show();
                                 retriveTestsSubTests();
                             }
                         });

                 alertDialogBuilder.setNegativeButton("Continue",
                         new DialogInterface.OnClickListener() {
                             public void onClick(DialogInterface dialog, int which) {
                                 dialog.dismiss();
                             }
                         });

                 AlertDialog alertDialog = alertDialogBuilder.create();
                 alertDialog.show();


             }else {


             }
         }



         super.onResume();
     }*/
    private void getttingPackagesList() {
/*
        ns3 = new NetworkStatus();
        isOnline3 = ns3.isOnline(getActivity());
        if (isOnline3) {*/

        noInternetConnection.setVisibility(View.GONE);
        //no_packages_found_iv.setVisibility(View.GONE);
        if (statusFlag = NetworkUtil.isConnected(getActivity())) {
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.NEW_TESTS_LIST,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            packagesModels.clear();
                            myHealthDBHelper.deleteAllNewTests();

                            Log.d("Purushotham", "---Sucesses-PackagesList : " + response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                ContentValues values = new ContentValues();
                                if (jsonArray.length() != 0) {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        NewTestsModel pm = new NewTestsModel();
                                        pm.setId(jsonObject1.getString("id"));
                                        pm.setTest_id(jsonObject1.getString("test_id"));
                                        pm.setTest_sub_name(jsonObject1.getString("test_sub_name"));
                                        pm.setHome_collection_status(jsonObject1.getString("home_collection_status"));
                                        pm.setStatus(jsonObject1.getString("status"));
                                        pm.setCreated_time(jsonObject1.getString("created_time"));
                                        pm.setUpdated_time(jsonObject1.getString("updated_time"));
                                        pm.setTest_name(jsonObject1.getString("test_name"));
                                        pm.setTest_image(jsonObject1.getString("test_image"));
                                        // packagesModels.add(pm);

                                        values.put(MyHealthDBHelper.NEW_ID, jsonObject1.getString("id"));
                                        values.put(MyHealthDBHelper.NEW_TEST_ID, jsonObject1.getString("test_id"));
                                        values.put(MyHealthDBHelper.NEW_TEST_SUB_NAME, jsonObject1.getString("test_sub_name"));
                                        values.put(MyHealthDBHelper.NEW_HOME_COLLECTION_STATUS, jsonObject1.getString("home_collection_status"));
                                        values.put(MyHealthDBHelper.NEW_STATUS, jsonObject1.getString("status"));
                                        values.put(MyHealthDBHelper.NEW_CREATED_TIME, jsonObject1.getString("created_time"));
                                        values.put(MyHealthDBHelper.NEW_UPDATED_TIME, jsonObject1.getString("updated_time"));
                                        values.put(MyHealthDBHelper.NEW_TEST_NAME, jsonObject1.getString("test_name"));
                                        values.put(MyHealthDBHelper.NEW_TEST_IMAGE, AppUrls.IMAGE_URL + jsonObject1.getString("test_image"));
                                        myHealthDBHelper.addNewTestTable(values);

                                            /*PackagesModel pm = new PackagesModel();
                                            pm.setP_id(jsonObject1.getString("id"));
                                            pm.setP_images_url(AppUrls.BASE_URL + jsonObject1.getString("package_image"));
                                            pm.setP_name(jsonObject1.getString("package_name"));
                                            pm.setP_status(jsonObject1.getString("status"));
                                            packagesModels.add(pm);*/

                                    }
                                    retrievePackageList();
                                    //packagesrecyclerView.setAdapter(adapter);
                                    // mSwipeRefreshLayout.setRefreshing(false);
                                } else {
                                    progressDialog.dismiss();
                                    // mSwipeRefreshLayout.setRefreshing(false);
                                    //  no_packages_found_iv.setVisibility(View.VISIBLE);
                                    noInternetConnection.setText("No Packages found.");
                                    noInternetConnection.setVisibility(View.VISIBLE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            mSwipeRefreshLayout.setRefreshing(false);
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

            };
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        } else {
            retrievePackageList();
            progressDialog.dismiss();
            mSwipeRefreshLayout.setRefreshing(false);
            Toast.makeText(getActivity(), "No Internet Connection....", Toast.LENGTH_SHORT).show();
            showInternetStatus();
        }
      /*  }else {
            progressDialog.dismiss();
            mSwipeRefreshLayout.setRefreshing(false);
            Toast.makeText(getActivity(),"No Internet Connection....",Toast.LENGTH_SHORT).show();
            showInternetStatus();
        }*/
    }

    public void retrievePackageList() {
        packagesModels.clear();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        packagesrecyclerView.setLayoutManager(layoutManager);
        adapter = new TestSubAdapter(packagesModels, TestSubTestFragment.this, R.layout.new_tests_row);

        //  packagesrecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        packagesrecyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        packagesrecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.recycler_divider));

        MyHealthDBHelper db = new MyHealthDBHelper(getActivity());
        List<String> id = db.getNew_ID();
        List<String> test_id = db.getNew_Test_ID();
        List<String> test_sub_name = db.getNew_Test_Sub_Name();
        List<String> home_collection_status = db.getNew_Home_Collection_Status();
        List<String> status = db.getNew_Status();
        List<String> created_time = db.getNew_Created_Time();
        List<String> updated_time = db.getNew_Updated_Time();
        List<String> test_name = db.getNew_Test_Name();
        List<String> test_image = db.getNew_Test_Image();

        for (int i = 0; i < id.size(); i++) {
            NewTestsModel pm = new NewTestsModel();
            pm.setId(id.get(i));
            pm.setTest_id(test_id.get(i));
            pm.setTest_sub_name(test_sub_name.get(i));
            pm.setHome_collection_status(home_collection_status.get(i));
            pm.setStatus(status.get(i));
            pm.setCreated_time(created_time.get(i));
            pm.setUpdated_time(updated_time.get(i));
            pm.setTest_name(test_name.get(i));
            pm.setTest_image(test_image.get(i));
            packagesModels.add(pm);
            CityData.add(test_sub_name.get(i) + "--" + id.get(i) + "--" + test_id.get(i));
        }

        packagesrecyclerView.setAdapter(adapter);
        search_auto.setAdapter(auto_adapter);
//        db.close();
    }

    public void cardViewListener(int position) {

        if (userSessionManager.checkLogin() != false) {
            Toast.makeText(getActivity(), "Please Login...!", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(getActivity(), PackagesSubTests.class);
            intent.putExtra("package_id", packagesModels.get(position).getTest_id());
            intent.putExtra("package_name", packagesModels.get(position).getTest_name());
            Log.d("PACKINFOFRAG:", packagesModels.get(position).getTest_id() + " / " + packagesModels.get(position).getTest_name());
            startActivity(intent);
        }
    }

    public void setGmName(ArrayList<String> gm_ids, ArrayList<String> gm_names, ArrayList<String> g_id) {
        selected_gmIds = gm_ids.toString();
        //NewMyHealthTestSubTestDBHelper db = new NewMyHealthTestSubTestDBHelper(getActivity());

        ContentValues values = new ContentValues();
        getMemList.clear();
        gm_ids_list.clear();
        gm_sub_ids_list.clear();

        for (int i = 0; i < gm_names.size(); i++) {
            GetMemInGroupModel getMemL = new GetMemInGroupModel();
            selected_gmNames = gm_names.get(i);
            getMemL.setName(selected_gmNames);
            getMemList.add(getMemL);
            gm_ids_list.add(gm_ids.get(i));
            gm_test_names.add(gm_names.get(i));
            gm_sub_ids_list.add(g_id.get(i));
            //values.put(MyHealthDBHelper.NEW_ID, gm_ids.get(i));
            // values.put(MyHealthDBHelper.NEW_TEST_ID, gm_test_ids.get(i));
            // values.put(MyHealthDBHelper.NEW_TEST_SUB_NAME, gm_names.get(i));
            // newdb.addTestsList(values);
            //  Log.d("GetMemName", selected_gmNames);
            //   Log.d("GetMemId", gm_ids.get(i));
        }

        getaddMemRecycler.setAdapter(getMemAdap);
        //retrievePackageList();
    }

    private BroadcastReceiver removeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent

            String name = intent.getStringExtra("Name");
            int position = intent.getIntExtra("position", -1);
            getMemList.remove(position);
            getMemAdap.notifyDataSetChanged();

            for (int i = 0; i < gm_names.size(); i++) {
                if (name.equalsIgnoreCase(gm_names.get(i))) {
                    gm_names.remove(i);
                    gm_sub_ids.remove(i);
                    gm_ids.remove(i);
                    gm_ids_list.remove(i);
                    gm_test_names.remove(i);
                    gm_sub_ids_list.remove(i);
                }
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(removeReceiver);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(removeReceiver);
    }
}