package innasoft.com.mhcone.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import innasoft.com.mhcone.R;

/**
 * Created by User on 8/9/2016.
 */
public class SubSubPackagesHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public TextView subSubPackName,subSubPackCost;
    public ImageView subsubPackImage;
    MyViewHolder context;
    public SubSubPackagesHolder(View itemView, MyViewHolder context) {
        super(itemView);
        this.context =context;
        itemView.setOnClickListener(this);
        subSubPackName = (TextView) itemView.findViewById(R.id.sub_sub_packagestitle);
        //subSubPackCost = (TextView) itemView.findViewById(R.id.sub_sub_packages_price);
    }

    @Override
    public void onClick(View view) {
        context.cardViewListener(getPosition());
    }
    public interface MyViewHolder
    {
        void cardViewListener(int position);
    }
}
