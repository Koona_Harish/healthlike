package innasoft.com.mhcone.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import innasoft.com.mhcone.R;

/**
 * Created by User on 8/22/2016.
 */
public class ViewUserHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public TextView tv_subuserName,tv_Gender,tv_dob,tv_subuserRelation,tv_Email,tv_Phone;
    public TextView h_subuserName,h_Gender,h_dob,h_subuserRelation,h_Email,h_Phone;
    public Button viewUserButton,deleteUserButton,editUserButton,close_view_sub_user,deleteUserButton1,editUserButton1;
    public LinearLayout lenearLayoutLL,upLL,downLL,lled_dl_set_two,lled_dl_set_one;
    MyViewHolder context;
    public ViewUserHolder(View itemView, MyViewHolder context) {
        super(itemView);
        this.context = context;
        itemView.setOnClickListener(this);
        tv_subuserName = (TextView) itemView.findViewById(R.id.sub_user_name);
        tv_Gender = (TextView) itemView.findViewById(R.id.gender_care);
        tv_dob = (TextView) itemView.findViewById(R.id.dob_care);
        tv_subuserRelation = (TextView) itemView.findViewById(R.id.sub_user_relation);
        tv_Email = (TextView) itemView.findViewById(R.id.email_care);
        tv_Phone = (TextView) itemView.findViewById(R.id.phone_care);

        h_subuserName = (TextView) itemView.findViewById(R.id.name);
        h_Gender = (TextView) itemView.findViewById(R.id.gender);
        h_dob = (TextView) itemView.findViewById(R.id.dob);
        h_subuserRelation = (TextView) itemView.findViewById(R.id.relation);
        h_Email = (TextView) itemView.findViewById(R.id.email);
        h_Phone = (TextView) itemView.findViewById(R.id.phone);

        viewUserButton = (Button) itemView.findViewById(R.id.view_sub_user);
        deleteUserButton = (Button) itemView.findViewById(R.id.delecteUser);
        editUserButton = (Button) itemView.findViewById(R.id.edit_sub_user);

        deleteUserButton1 = (Button) itemView.findViewById(R.id.delecteUser1);
        editUserButton1 = (Button) itemView.findViewById(R.id.edit_sub_user1);

        lled_dl_set_two = (LinearLayout) itemView.findViewById(R.id.lled_dl_set_two);
        lled_dl_set_one = (LinearLayout) itemView.findViewById(R.id.lled_dl_set_one);


        lenearLayoutLL = (LinearLayout) itemView.findViewById(R.id.llhide_ll);
        upLL = (LinearLayout) itemView.findViewById(R.id.up_ll);
        downLL = (LinearLayout) itemView.findViewById(R.id.down_ll);
        close_view_sub_user = (Button) itemView.findViewById(R.id.close_view_sub_user);
    }

    @Override
    public void onClick(View view) {
        context.cardViewListener(getPosition());
    }

    public interface MyViewHolder
    {
        void cardViewListener(int position);
    }
}
