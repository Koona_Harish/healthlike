package innasoft.com.mhcone.holders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.ShowUserTestLabs;
import innasoft.com.mhcone.models.SubViewMyPackageModel;
import innasoft.com.mhcone.utilities.GPSTrackers;
import innasoft.com.mhcone.utilities.StartLocationAlert;

/**
 * Created by purushotham on 5/10/16.
 */

public class SubViewMyPackageHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public TextView itemHeading;

    GPSTrackers gps;

    Context context;
    String packageName;
    ArrayList<SubViewMyPackageModel> arrayList = new ArrayList<SubViewMyPackageModel>();
    public SubViewMyPackageHolder(String packageName, View itemView , ArrayList<SubViewMyPackageModel> arrayList, Context context) {
        super(itemView);

        this.context = context;
        this.arrayList = arrayList;
        this.packageName = packageName;
        itemView.setOnClickListener(this);


        gps = new GPSTrackers(context);

        itemHeading = (TextView) itemView.findViewById(R.id.itemHeadding);
    }

    @Override
    public void onClick(View v) {

        int posistion = getAdapterPosition();


        List<String> subtestName = new ArrayList<String>();
        List<String> subtestIds = new ArrayList<String>();
        List<String> stringFormateforIDs = new ArrayList<String>();
        for (int i = 0; i <arrayList.size(); i++)
        {
            subtestIds.add(arrayList.get(i).sub_test_id);
            subtestName.add(arrayList.get(i).sub_test_name);
            stringFormateforIDs.add("0-"+arrayList.get(i).test_id+"-"+arrayList.get(i).sub_test_id);
        }


        //int posistion = getAdapterPosition();

        SubViewMyPackageModel newsListModel = this.arrayList.get(posistion);
        //Toast.makeText(context, packageName+"\n"+subtestIds+"\n"+subtestName, Toast.LENGTH_SHORT).show();

        if (gps.canGetLocation()) {

            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            Intent intent = new Intent(context, ShowUserTestLabs.class);
            intent.putExtra("latValue", Double.toString(latitude));
            intent.putExtra("longValue", Double.toString(longitude));

            intent.putExtra("packageName", packageName);
            intent.putExtra("subtestIdswithformate", subtestIds.toString().replaceAll("\\[", "").replaceAll("\\]","").replaceAll(" ", ""));
            intent.putExtra("subtestNameswithfromate", subtestName.toString().replaceAll("\\[", "").replaceAll("\\]","").replaceAll(" ", ""));
            intent.putExtra("forbillingPageIDs", stringFormateforIDs.toString().replaceAll("\\[", "").replaceAll("\\]","").replaceAll(" ", ""));
         //   intent.putExtra("subTestId", SendSubTestIDs);
         //   intent.putExtra("subTestName", sendSubtestNames);
            context.startActivity(intent);


        }
        else {
            Toast.makeText(context, "Please Check GPS connection ....!", Toast.LENGTH_SHORT).show();
        }

    }
}
