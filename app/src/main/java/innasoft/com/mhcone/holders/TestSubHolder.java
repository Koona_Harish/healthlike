package innasoft.com.mhcone.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashSet;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.itemclicklistener.PackageItemClickListener;

/**
 * Created by User on 8/9/2016.
 */
public class TestSubHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public TextView packageName;
    public ImageView package_pic;
    public RelativeLayout parent_layout;
    PackageItemClickListener itemClickListener;

    public TestSubHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);
        packageName = (TextView) itemView.findViewById(R.id.packagestitle);
        package_pic = (ImageView) itemView.findViewById(R.id.alnewsImagePackage);
        parent_layout = (RelativeLayout) itemView.findViewById(R.id.parent_layout);



    }

    @Override
    public void onClick(View view) {
        //context.cardViewListener(getPosition());
        this.itemClickListener.onItemClick(view,getLayoutPosition());

    }
   /* public interface MyViewHolder
    {
        void cardViewListener(int position);
    }*/

    public void setItemClickListener(PackageItemClickListener ic)
    {
        this.itemClickListener=ic;
    }
}
