package innasoft.com.mhcone.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import innasoft.com.mhcone.R;

/**
 * Created by purushotham on 17/10/16.
 */

public class UserSubPackageHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public TextView usersubpackageName;
    public ImageView package_pic;
    MyViewHolder context;

    public UserSubPackageHolder(View itemView , MyViewHolder context) {
        super(itemView);
        this.context = context;
        itemView.setOnClickListener(this);
        usersubpackageName = (TextView) itemView.findViewById(R.id.user_sub_packagestitle);
    }

    @Override
    public void onClick(View v) {
        context.cardViewListener(getPosition());
    }

    public interface MyViewHolder
    {
        void cardViewListener(int position);
    }
}
