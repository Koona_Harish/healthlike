package innasoft.com.mhcone.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import innasoft.com.mhcone.R;

public class LabPackagesHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public TextView labPackageName,labPackageSubTestList,lab_packages_lab_price,lab_packages_app_price;
    public TextView labPackageNameTxt,labPackageSubTestListTxt,labPackagePriceTxt,labPackageLabPriceTxt;
    public ImageView clrImage;
    public CheckBox labPackageChk;
    public ImageView view_test,hide_tests;
    public LinearLayout linearLayout_tests;


    MyLabPackagesHolder context;

    public LabPackagesHolder(View itemView ,MyLabPackagesHolder context)
    {
        super(itemView);
        this.context = context;
        itemView.setOnClickListener(this);

        //labPackageNameTxt = (TextView) itemView.findViewById(R.id.lab_packages_name_txt);
        labPackageSubTestListTxt = (TextView) itemView.findViewById(R.id.lab_packages_list_txt);
        //labPackagePriceTxt = (TextView) itemView.findViewById(R.id.lab_packages_price_txt);
        //labPackageLabPriceTxt = (TextView) itemView.findViewById(R.id.lab_packages_lab_price_txt);

        labPackageName = (TextView) itemView.findViewById(R.id.lab_packages_name);
        labPackageSubTestList = (TextView) itemView.findViewById(R.id.lab_packages_list);
        lab_packages_lab_price = (TextView) itemView.findViewById(R.id.lab_packages_lab_price);
        lab_packages_app_price = (TextView) itemView.findViewById(R.id.lab_packages_app_price);
        clrImage = (ImageView) itemView.findViewById(R.id.lab_packages_clrimage);
        view_test = (ImageView) itemView.findViewById(R.id.view_test);
        hide_tests = (ImageView) itemView.findViewById(R.id.hide_tests);
        linearLayout_tests = (LinearLayout) itemView.findViewById(R.id.linearLayout_tests);

     //   labPackageChk = (CheckBox) itemView.findViewById(R.id.lab_packages_chk);

    }

    @Override
    public void onClick(View view)
    {
        context.cardViewListener(getPosition());

    }

    public interface MyLabPackagesHolder
    {
        void cardViewListener(int position);
    }
}
