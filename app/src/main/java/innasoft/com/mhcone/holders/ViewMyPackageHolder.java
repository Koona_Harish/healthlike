package innasoft.com.mhcone.holders;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.ViewMyPackageAdapter;
import innasoft.com.mhcone.itemclicklistener.ViewMyPackageItemClickListener;
import innasoft.com.mhcone.models.SubViewMyPackageModel;
import innasoft.com.mhcone.models.ViewMyPackageModel;

public class ViewMyPackageHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{

    public TextView mPackageTv;
    ViewMyPackageItemClickListener allRelationItemClickListener;

    //public ImageView viewCompletePack,deletePack;

    public RecyclerView subRecyclerView;

    ArrayList<ViewMyPackageModel> itemListPackList = new ArrayList<ViewMyPackageModel>();
    Context context;
    public ViewMyPackageHolder(View itemView, ArrayList<ViewMyPackageModel> arrayList, Context context) {
        super(itemView);
        this.context = context;
        itemView.setOnClickListener(this);
        this.itemListPackList = arrayList;
        mPackageTv = (TextView) itemView.findViewById(R.id.itemHeadding);


       /* subRecyclerView = (RecyclerView) itemView.findViewById(R.id.subtest_recycler_view);
        subRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        subRecyclerView.setLayoutManager(layoutManager);*/


       /* viewCompletePack = (ImageView) itemView.findViewById(R.id.viewCompleteInfoMyPack);
        deletePack = (ImageView) itemView.findViewById(R.id.deleteMyPackage);*/
    }

    @Override
    public void onClick(View view) {

        this.allRelationItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(ViewMyPackageItemClickListener ic)
    {
        this.allRelationItemClickListener=ic;
    }

}
