package innasoft.com.mhcone.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.itemclicklistener.ShowLabsPackagesClickListener;

public class ShowLabsPackageHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public TextView show_lab_address_tittle,show_lab_package_name_txt,show_lab_package_address_txt,show_lab_package_timings_txt
            ,show_lab_package_distance_txt, show_lab_package_homecollection_txt,show_lab_package_final_price_txt,show_lab_package_lab_price_txt;
    public ImageView show_info_lab;
    public ImageView checktickmarkPackage;
    public RatingBar ratingBar_package;
    public ImageView labProfilePic;
    public TextView certified_package;
    ShowLabsPackagesClickListener showLabsPackagesClickListener;

    //MyShowLabsPackageHolder context;
    public ShowLabsPackageHolder(View itemView) {
        super(itemView);
        //this.context = context;
        itemView.setOnClickListener(this);

        show_lab_address_tittle = (TextView) itemView.findViewById(R.id.row_address_tittle);
        show_lab_package_name_txt = (TextView) itemView.findViewById(R.id.row_lab_package_name);
        show_lab_package_address_txt = (TextView) itemView.findViewById(R.id.row_package_address);
        show_lab_package_timings_txt = (TextView) itemView.findViewById(R.id.row_package_timings);
        show_lab_package_distance_txt = (TextView) itemView.findViewById(R.id.row_package_distance);
        show_lab_package_final_price_txt = (TextView) itemView.findViewById(R.id.row_package_final_price);
        show_lab_package_homecollection_txt = (TextView) itemView.findViewById(R.id.homecollection_package);
        show_info_lab = (ImageView) itemView.findViewById(R.id.row_pack_lab_view);
        show_lab_package_lab_price_txt = (TextView) itemView.findViewById(R.id.row_package_lab_price);
        checktickmarkPackage = (ImageView) itemView.findViewById(R.id.checktickmark_package);
        ratingBar_package = (RatingBar) itemView.findViewById(R.id.ratingbar_package);
        labProfilePic = (ImageView) itemView.findViewById(R.id.package_prifileImage);
        certified_package = (TextView) itemView.findViewById(R.id.certified_package);

    }

    @Override
    public void onClick(View view) {
       // context.cardViewListener(getPosition());
        this.showLabsPackagesClickListener.onItemClick(view,getLayoutPosition());

    }
    public void setItemClickListener(ShowLabsPackagesClickListener ic)
    {
        this.showLabsPackagesClickListener=ic;
    }
   /* public interface MyShowLabsPackageHolder
    {
        void cardViewListener(int position);
    }*/
}
