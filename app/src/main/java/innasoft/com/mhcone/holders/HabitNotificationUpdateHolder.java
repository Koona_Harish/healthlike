package innasoft.com.mhcone.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;

import innasoft.com.mhcone.R;

/**
 * Created by purushotham on 18/10/16.
 */

public class HabitNotificationUpdateHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{

    public CheckBox habitUpdateCK;

    MyHabitUpdateHolder context;
    public HabitNotificationUpdateHolder(View itemView , MyHabitUpdateHolder context) {
        super(itemView);
        this.context = context;
        itemView.setOnClickListener(this);
        habitUpdateCK = (CheckBox) itemView.findViewById(R.id.notification_checkbox);

    }

    @Override
    public void onClick(View v) {
        context.cardViewListener(getPosition());
    }


    public interface MyHabitUpdateHolder
    {
        void cardViewListener(int position);
    }
}
