package innasoft.com.mhcone.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import innasoft.com.mhcone.R;

/**
 * Created by User on 8/8/2016.
 */
public class TestsHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public ImageView testsImage;
    public TextView testsTitle;

    MyViewHolder context;

    public TestsHolder(View itemView, MyViewHolder context) {
        super(itemView);
        this.context = context;
        itemView.setOnClickListener(this);
        testsTitle = (TextView) itemView.findViewById(R.id.teststitle);
        testsImage = (ImageView) itemView.findViewById(R.id.alnewsImage);
    }




    @Override
    public void onClick(View view) {
        context.cardViewListener(getPosition());
    }
    public interface MyViewHolder
    {
        void cardViewListener(int position);
    }
}
