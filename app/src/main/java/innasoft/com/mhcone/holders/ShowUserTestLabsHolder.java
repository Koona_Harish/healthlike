package innasoft.com.mhcone.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.itemclicklistener.ShowLabsUserPackagesClickListener;

public class ShowUserTestLabsHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{

    public TextView show_address_tittle,show_user_test_lab_name_txt,show_user_test_lab_address_txt,show_user_test_lab_timings_txt,
            show_user_test_lab_distance_txt, show_user_test_lab_homecollection_txt,show_user_test_lab_final_price_txt,show_user_test_lab_lab_price_txt;

    public ImageView show_user_test_lab_View;
    public ImageView checktickmarkUserTestLabs;
    public RatingBar ratingBar_user_test_labs;
    public ImageView prifileImage;
    public TextView certified_user_package;
    ShowLabsUserPackagesClickListener showLabsUserPackagesClickListener;


    //MyShowUserTestLabsHolder context;

    public ShowUserTestLabsHolder(View itemView)
    {

        super(itemView);
       // this.context = context;
        itemView.setOnClickListener(this);

        show_address_tittle = (TextView) itemView.findViewById(R.id.row_lab_tittle);
        show_user_test_lab_name_txt = (TextView) itemView.findViewById(R.id.user_test_lab_name);
        show_user_test_lab_address_txt = (TextView) itemView.findViewById(R.id.user_test_lab_address);
        show_user_test_lab_timings_txt = (TextView) itemView.findViewById(R.id.user_test_lab_timings);
        show_user_test_lab_distance_txt = (TextView) itemView.findViewById(R.id.user_test_lab_distance);
        show_user_test_lab_homecollection_txt = (TextView) itemView.findViewById(R.id.homecollection_utl);
        show_user_test_lab_final_price_txt = (TextView) itemView.findViewById(R.id.user_test_final_price);
        show_user_test_lab_lab_price_txt = (TextView) itemView.findViewById(R.id.user_test_lab_price);
        checktickmarkUserTestLabs = (ImageView) itemView.findViewById(R.id.checktickmark);
        ratingBar_user_test_labs = (RatingBar) itemView.findViewById(R.id.ratingbar_user_test_lab);
        show_user_test_lab_View = (ImageView) itemView.findViewById(R.id.user_test_lab_view);
        prifileImage = (ImageView) itemView.findViewById(R.id.prifileImage);
        certified_user_package = (TextView) itemView.findViewById(R.id.certified_user_package);

    }

    @Override
    public void onClick(View view)
    {

        //context.cardViewListener(getPosition());
        this.showLabsUserPackagesClickListener.onItemClick(view,getLayoutPosition());
    }

    public void setItemClickListener(ShowLabsUserPackagesClickListener ic)
    {
        this.showLabsUserPackagesClickListener=ic;
    }

    /*public interface MyShowUserTestLabsHolder
    {

        void cardViewListener(int position);
    }*/
}
