package innasoft.com.mhcone.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.itemclicklistener.LabsPackagesClickListener;

public class ShowLabsListHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public TextView show_lab_tittle,show_lab_list_name_txt,show_lab_list_email_txt,show_lab_list_mobile_txt,show_lab_list_address_txt, show_lab_list_workingdays_txt,
            show_lab_list_distance_txt,show_lab_list_homecollection_txt;

    public ImageView btnLabList;
    public ImageView labs_checktickmark;
    public RatingBar ratingBar_lab_list;
    public ImageView lab_list_pic;
    public TextView certified_labs_list;
   // MyShowLabsListHolder context;
    LabsPackagesClickListener labsPackagesClickListener;
    public ShowLabsListHolder(View itemView) {
        super(itemView);
        //this.context = context;
        itemView.setOnClickListener(this);

        show_lab_tittle = (TextView) itemView.findViewById(R.id.row_lab_tittle);
        show_lab_list_name_txt = (TextView) itemView.findViewById(R.id.row_lab_list_name);
     //   show_lab_list_email_txt = (TextView) itemView.findViewById(R.id.row_lab_list_email);
      //  show_lab_list_mobile_txt = (TextView) itemView.findViewById(R.id.row_lab_list_mobile);
        show_lab_list_workingdays_txt = (TextView) itemView.findViewById(R.id.row_lab_list_workingdays);
        show_lab_list_address_txt = (TextView) itemView.findViewById(R.id.row_lab_list_address);
        show_lab_list_distance_txt = (TextView) itemView.findViewById(R.id.row_lab_list_distance);
        show_lab_list_homecollection_txt = (TextView) itemView.findViewById(R.id.homecollection_ll);
        lab_list_pic = (ImageView) itemView.findViewById(R.id.lab_progfile_Image);

        ratingBar_lab_list = (RatingBar) itemView.findViewById(R.id.ratingbar_lab_list);

        btnLabList = (ImageView) itemView.findViewById(R.id.row_lab_list_view);

        labs_checktickmark = (ImageView) itemView.findViewById(R.id.checktickmarkLab);
        certified_labs_list = (TextView) itemView.findViewById(R.id.certified_labs_list);

    }

    @Override
    public void onClick(View view) {
        //context.cardViewListener(getAdapterPosition());
        this.labsPackagesClickListener.onItemClick(view,getLayoutPosition());

    }



  /*  public interface MyShowLabsListHolder
    {
        void cardViewListener(int position);
    }*/

    public void setItemClickListener(LabsPackagesClickListener ic)
    {
        this.labsPackagesClickListener=ic;
    }
}
