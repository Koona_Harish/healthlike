package innasoft.com.mhcone.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.itemclicklistener.ShowLabsTestsClickListener;

/**
 * Created by User on 8/31/2016.
 */
public class ShowLabsTestHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public TextView show_address_tittle_txt,show_lab_name_txt,show_address_txt,show_timings_txt,show_distance_txt,
            show_certified_txt,show_homecollection_txt, show_final_price_txt,show_lab_price_txt;
  //  public Button showLabInfo;
    public ImageView test_View;
    public ImageView checktickmark;
    public RatingBar ratingBar_tests;
    public ImageView prifileImage;
   // MyShowLabsTestHolder context;
    public TextView certified_test;
    ShowLabsTestsClickListener showLabsTestsClickListener;
    public ShowLabsTestHolder(View itemView) {
        super(itemView);
       // this.context = context;
        itemView.setOnClickListener(this);

        show_address_tittle_txt = (TextView) itemView.findViewById(R.id.row_address_tittle);
        show_lab_name_txt = (TextView) itemView.findViewById(R.id.row_lab_name);
        show_certified_txt = (TextView) itemView.findViewById(R.id.certified_test);
        show_address_txt = (TextView) itemView.findViewById(R.id.row_address);
        show_timings_txt = (TextView) itemView.findViewById(R.id.row_timings);
        show_distance_txt = (TextView) itemView.findViewById(R.id.row_distance);
        show_homecollection_txt = (TextView) itemView.findViewById(R.id.homecollection_test);
        show_final_price_txt = (TextView) itemView.findViewById(R.id.row_final_price);
        show_lab_price_txt = (TextView) itemView.findViewById(R.id.row_lab_price);
        checktickmark = (ImageView) itemView.findViewById(R.id.checktickmark);
        certified_test = (TextView) itemView.findViewById(R.id.certified_test);
     //   showLabInfo = (Button) itemView.findViewById(R.id.row_test_lab_view);

        ratingBar_tests = (RatingBar) itemView.findViewById(R.id.ratingbar_test);

        test_View = (ImageView) itemView.findViewById(R.id.row_test_view);
        prifileImage = (ImageView) itemView.findViewById(R.id.prifileImage);

    }

    @Override
    public void onClick(View view) {
        //context.cardViewListener(getPosition());
        this.showLabsTestsClickListener.onItemClick(view,getLayoutPosition());

    }



    /*public interface MyShowLabsTestHolder
    {
        void cardViewListener(int position);
    }*/

    public void setItemClickListener(ShowLabsTestsClickListener ic)
    {
        this.showLabsTestsClickListener=ic;
    }
}
