package innasoft.com.mhcone.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.itemclicklistener.PopularPackageItemClickListener;


public class PopularPackageHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public TextView package_name_txt;
    public ImageView package_img;
    PopularPackageItemClickListener popularPackageItemClickListener;

    public PopularPackageHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);
        package_name_txt = (TextView) itemView.findViewById(R.id.package_name_txt);
        package_img = (ImageView) itemView.findViewById(R.id.package_img);

    }

    @Override
    public void onClick(View view) {
        this.popularPackageItemClickListener.onItemClick(view,getLayoutPosition());

    }

    public void setItemClickListener(PopularPackageItemClickListener ic)
    {
        this.popularPackageItemClickListener = ic;
    }
}
