package innasoft.com.mhcone.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import innasoft.com.mhcone.R;

import innasoft.com.mhcone.itemclicklistener.RelatedHabitsListClickListener;


public class RelatedHabitsListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView relatedhabitsName,relatedhabits_list_category;
    public WebView relatedhabitsDescription;
    public ImageView relatedhabitImage;
    RelatedHabitsListClickListener relatedhabitsListClickListener;


    public RelatedHabitsListHolder(View itemView )
    {
        super(itemView);
        //this.context = context;
        itemView.setOnClickListener(this);

        relatedhabitsName = (TextView) itemView.findViewById(R.id.relate_ar_name);
       relatedhabitsDescription = (WebView) itemView.findViewById(R.id.related_habits_list_description);
        relatedhabitImage = (ImageView) itemView.findViewById(R.id.related_habits_list_image);
        relatedhabits_list_category = (TextView) itemView.findViewById(R.id.related_habits_list_category);


    }

    @Override
    public void onClick(View view)
    {
        //context.cardViewListener(getPosition());
        this.relatedhabitsListClickListener.onItemClick(view,getLayoutPosition());

    }

    public void setItemClickListener(RelatedHabitsListClickListener ic)
    {
        this.relatedhabitsListClickListener=ic;
    }




}
