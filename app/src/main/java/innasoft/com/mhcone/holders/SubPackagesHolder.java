package innasoft.com.mhcone.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import innasoft.com.mhcone.R;

/**
 * Created by User on 8/9/2016.
 */
public class SubPackagesHolder extends RecyclerView.ViewHolder implements OnClickListener
{
    public TextView subPackName,subtestPrice;
    ImageView subtestImage;
    MyViewHolder context;
    public SubPackagesHolder(View itemView, MyViewHolder context) {
        super(itemView);
        this.context = context;
        itemView.setOnClickListener(this);
        subPackName = (TextView) itemView.findViewById(R.id.sub_packagestitle);
        //subtestPrice = (TextView) itemView.findViewById(R.id.sub_test_price);

    }

    @Override
    public void onClick(View view) {
        context.cardViewListener(getPosition());
    }
    public interface MyViewHolder
    {
        void cardViewListener(int position);
    }
}
