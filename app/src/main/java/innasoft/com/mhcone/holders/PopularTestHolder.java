package innasoft.com.mhcone.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.itemclicklistener.PopularTestItemClickListener;


public class PopularTestHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public TextView test_name_txt;
    public ImageView test_img;
    PopularTestItemClickListener popularTestItemClickListener;

    public PopularTestHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);
        test_name_txt = (TextView) itemView.findViewById(R.id.test_name_txt);
        test_img = (ImageView) itemView.findViewById(R.id.test_img);
    }

    @Override
    public void onClick(View view) {
        this.popularTestItemClickListener.onItemClick(view,getLayoutPosition());

    }

    public void setItemClickListener(PopularTestItemClickListener ic)
    {
        this.popularTestItemClickListener = ic;
    }
}
