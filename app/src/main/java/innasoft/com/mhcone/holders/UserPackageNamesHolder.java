package innasoft.com.mhcone.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.itemclicklistener.LabsPackagesClickListener;
import innasoft.com.mhcone.itemclicklistener.ViewMyPackagesClickListener;

/**
 * Created by purushotham on 17/10/16.
 */

public class UserPackageNamesHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{

    public TextView userpackageName;
    public ImageView package_pic;
    ViewMyPackagesClickListener viewMyPackagesClickListener;
   // MyViewHolder context;
    public UserPackageNamesHolder(View itemView) {
        super(itemView);
        //this.context = context;
        itemView.setOnClickListener(this);
        userpackageName = (TextView) itemView.findViewById(R.id.packagestitle);

    }

    @Override
    public void onClick(View v) {
        //context.cardViewListener(getPosition());
        this.viewMyPackagesClickListener.onItemClick(v,getLayoutPosition());
    }

    public void setItemClickListener(ViewMyPackagesClickListener ic)
    {
        this.viewMyPackagesClickListener=ic;
    }
   /* public interface MyViewHolder
    {
        void cardViewListener(int position);
    }*/
}
