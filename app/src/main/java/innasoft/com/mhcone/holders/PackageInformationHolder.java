package innasoft.com.mhcone.holders;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.itemclicklistener.PackageInfoItemClickListener;
import innasoft.com.mhcone.itemclicklistener.ViewMyPackageItemClickListener;
import innasoft.com.mhcone.models.ViewMyPackageModel;

public class PackageInformationHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{

    public TextView mPackageTv;
    PackageInfoItemClickListener packageInfoItemClickListener;


    public PackageInformationHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        mPackageTv = (TextView) itemView.findViewById(R.id.itemHeadding);

    }

    @Override
    public void onClick(View view) {

        this.packageInfoItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(PackageInfoItemClickListener ic)
    {
        this.packageInfoItemClickListener=ic;
    }



}
