package innasoft.com.mhcone.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.itemclicklistener.HabitsListClickListener;
import innasoft.com.mhcone.itemclicklistener.LabsPackagesClickListener;


public class HabitsListHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public TextView habitsName,habits_list_category;
    public WebView habitsDescription;
    public ImageView habitImage;
    public HabitsListClickListener habitsListClickListener;
 //   public RelativeLayout habits_webview_rl;

//    MyHabitsListHolder context;

    public HabitsListHolder(View itemView )
    {
        super(itemView);
        //this.context = context;
        itemView.setOnClickListener(this);

        habitsName = (TextView) itemView.findViewById(R.id.habits_list_name);
        habitsDescription = (WebView) itemView.findViewById(R.id.habits_list_description);
        habitImage = (ImageView) itemView.findViewById(R.id.habits_list_image);
        habits_list_category = (TextView) itemView.findViewById(R.id.habits_list_category);
    //    habits_webview_rl = (RelativeLayout) itemView.findViewById(R.id.habits_webview_rl);
    }

    @Override
    public void onClick(View view)
    {
        //context.cardViewListener(getPosition());
        this.habitsListClickListener.onItemClick(view,getLayoutPosition());

    }

    public void setItemClickListener(HabitsListClickListener ic)
    {
        this.habitsListClickListener=ic;
    }

   /* public interface MyHabitsListHolder
    {
        void cardViewListener(int position);
    }*/
}
