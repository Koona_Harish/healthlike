package innasoft.com.mhcone.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import innasoft.com.mhcone.R;

/**
 * Created by androiddev2 on 30/9/16.
 */

public class OrdersListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView ordersListId,ordersListLabName,ordersListBookingDate,orderListPrice,ordersListStatus,ordersListBookedDate;
    public TextView orderslistId,orderslistLabName,orderslistBookingDate,orderlistPrice,booked_date,orderStatus,pay_through_value;
    public Button ordersListbtn;

    MyOrdersListHolder context;

    public OrdersListHolder(View itemView, MyOrdersListHolder context) {

        super(itemView);
        this.context = context;
        itemView.setOnClickListener(this);

        orderslistId = (TextView) itemView.findViewById(R.id.order_id);
        orderslistBookingDate = (TextView) itemView.findViewById(R.id.booking_date);
        orderlistPrice = (TextView) itemView.findViewById(R.id.order_price);

        booked_date = (TextView) itemView.findViewById(R.id.booked_date);
        orderStatus = (TextView) itemView.findViewById(R.id.order_status);

        ordersListId = (TextView) itemView.findViewById(R.id.row_order_id);
        ordersListLabName = (TextView) itemView.findViewById(R.id.row_order_name);
        ordersListBookingDate = (TextView) itemView.findViewById(R.id.row_booking_date);
        orderListPrice = (TextView) itemView.findViewById(R.id.row_order_price);
        ordersListStatus = (TextView) itemView.findViewById(R.id.row_order_status);
        ordersListBookedDate = (TextView) itemView.findViewById(R.id.row_booked_date);
        pay_through_value = (TextView) itemView.findViewById(R.id.pay_through_value);
       // ordersListbtn = (Button) itemView.findViewById(R.id.row_order_btn);
    }

    @Override
    public void onClick(View v) {
        context.cardViewListener(getPosition());
    }

    public interface MyOrdersListHolder
    {
        void cardViewListener(int position);
    }
}
