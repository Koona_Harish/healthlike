package innasoft.com.mhcone.holders;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.itemclicklistener.CitiesClickListener;

/**
 * Created by purushotham on 27/1/17.
 */

public class CitiesHolder  extends RecyclerView.ViewHolder implements View.OnClickListener
{

    public TextView city_name_tv;
    public Fragment context;
    CitiesClickListener citiesClickListener;
    public CitiesHolder(View itemView) {
        super(itemView);
        this.context = context;
        itemView.setOnClickListener(this);
        city_name_tv = (TextView) itemView.findViewById(R.id.city_name_tv);
    }

    @Override
    public void onClick(View view) {

        this.citiesClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(CitiesClickListener ic)
    {
        this.citiesClickListener=ic;
    }
}
