package innasoft.com.mhcone.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import innasoft.com.mhcone.R;

/**
 * Created by User on 8/9/2016.
 */
public class SubTestHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public TextView subtestName,subtestPrice;
    ImageView subtestImage;
    public CheckBox selectSubTestsCK;
    MyViewHolder context;
    public SubTestHolder(View itemView, MyViewHolder context) {
        super(itemView);
        this.context = context;
        itemView.setOnClickListener(this);
        subtestName = (TextView) itemView.findViewById(R.id.sub_teststitle);
        selectSubTestsCK = (CheckBox) itemView.findViewById(R.id.chkSelectedTests);
        //selectSubTestsCK.setChecked(false);
       // subtestPrice = (TextView) itemView.findViewById(R.id.sub_test_price);

    }

    @Override
    public void onClick(View view) {
        context.cardViewListener(getPosition());
    }
    public interface MyViewHolder
    {
        void cardViewListener(int position);
    }
}
