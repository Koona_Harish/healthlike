package innasoft.com.mhcone.holders;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.itemclicklistener.BookLabClickListener;

public class BookAppointmentHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public TextView show_book_tittle,show_book_list_name_txt,show_book_list_email_txt,show_book_list_mobile_txt,
            show_book_list_address_txt, show_book_list_workingdays_txt, show_book_list_distance_txt,show_book_list_homecollection_txt;

    public ImageView btnBookList;
    public ImageView book_checktickmark;
    public RatingBar ratingBar_book_appointment;
    public ImageView prifileImage;
    public TextView certified_book_appointment;
    BookLabClickListener bookLabClickListener;

    //MyBookAppointmentHolder context;
    public BookAppointmentHolder(View itemView) {
        super(itemView);
        //this.context = context;
        itemView.setOnClickListener(this);

        show_book_tittle = (TextView) itemView.findViewById(R.id.row_book_tittle);
        show_book_list_name_txt = (TextView) itemView.findViewById(R.id.row_book_list_name);
     //   show_book_list_email_txt = (TextView) itemView.findViewById(R.id.row_book_list_email);
      //  show_book_list_mobile_txt = (TextView) itemView.findViewById(R.id.row_book_list_mobile);
        show_book_list_workingdays_txt = (TextView) itemView.findViewById(R.id.row_book_list_workingdays);
        show_book_list_address_txt = (TextView) itemView.findViewById(R.id.row_book_list_address);
        ratingBar_book_appointment = (RatingBar) itemView.findViewById(R.id.ratingbar_book_appointment);
        show_book_list_distance_txt = (TextView) itemView.findViewById(R.id.row_book_list_distance);
        show_book_list_homecollection_txt = (TextView) itemView.findViewById(R.id.homecollection_ba);

        btnBookList = (ImageView) itemView.findViewById(R.id.row_book_list_view);
        book_checktickmark = (ImageView) itemView.findViewById(R.id.checktickmarkbook);
        prifileImage = (ImageView) itemView.findViewById(R.id.prifileImage);
        certified_book_appointment = (TextView) itemView.findViewById(R.id.certified_book_appointment);
    }

    @Override
    public void onClick(View view) {
        //context.cardViewListener(getPosition());
        this.bookLabClickListener.onItemClick(view,getLayoutPosition());

    }

   /* public interface MyBookAppointmentHolder
    {
        void cardViewListener(int position);
    }*/
   public void setItemClickListener(BookLabClickListener ic)
   {
       this.bookLabClickListener=ic;
   }
}
