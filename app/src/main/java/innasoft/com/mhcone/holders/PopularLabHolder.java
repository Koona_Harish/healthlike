package innasoft.com.mhcone.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.itemclicklistener.PopularLabItemClickListener;


public class PopularLabHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public TextView lab_name_txt;
    public ImageView lab_img;
    PopularLabItemClickListener popularLabItemClickListener;

    public PopularLabHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);
        lab_name_txt = (TextView) itemView.findViewById(R.id.lab_name_txt);
        lab_img = (ImageView) itemView.findViewById(R.id.lab_img);
    }

    @Override
    public void onClick(View view) {
        this.popularLabItemClickListener.onItemClick(view,getLayoutPosition());

    }

    public void setItemClickListener(PopularLabItemClickListener ic)
    {
        this.popularLabItemClickListener = ic;
    }
}
