package innasoft.com.mhcone;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import innasoft.com.mhcone.activities.AboutUs;
import innasoft.com.mhcone.activities.ChangePassword;
import innasoft.com.mhcone.activities.CreateMyPackage;
import innasoft.com.mhcone.activities.HabitNotificationUpdate;
import innasoft.com.mhcone.activities.HabitsList;
import innasoft.com.mhcone.activities.LoginActivity;
import innasoft.com.mhcone.activities.OrdersList;
import innasoft.com.mhcone.activities.ProfileActivity;
import innasoft.com.mhcone.activities.RegisterActivity;
import innasoft.com.mhcone.activities.ViewMyPackage;
import innasoft.com.mhcone.activities.ViewUsers;
import innasoft.com.mhcone.fragments.DummyHomeFragment;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.CustomTypefaceSpan;
import innasoft.com.mhcone.utilities.FCM.Config;
import innasoft.com.mhcone.utilities.FilePath;
import innasoft.com.mhcone.utilities.ImagePermissions;
import innasoft.com.mhcone.utilities.NetworkChecking;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.NetworkUtil;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Typeface typeface, typeface2;
    TextView toolbar_title;
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    UserSessionManager userSessionManager;
    private boolean statusFlag;
    private Boolean exit = false;
    Toast toast;
    ProgressDialog progressDialog;
    private static final int CAMERA_REQUEST_IMAGE = 1888;
    private static final int GALLERY_REQUEST_IMAGE = 1999;
    private String selectedFilePath;
    HttpEntity resEntity;
    private Uri fileUri;

    /*Navigation Headerview*/
    ImageView profile_imag, navdrawer_image, profile_imag1;
    TextView user_name_txt, email_txt, txtViewName, login, register_action;

    private String userChoosenTask;
    String user_id, user_name, email, access_key, profile_pic;
    NetworkStatus ns;
    Boolean isOnline = false;
    RelativeLayout loginLayout;
    LinearLayout loggedLayout;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final int MEDIA_TYPE_IMAGE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.robotoregular));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.robotobold));

        ns = new NetworkStatus();
        isOnline = ns.isOnline(MainActivity.this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        progressDialog.setCancelable(false);

        userSessionManager = new UserSessionManager(getApplicationContext());
/*        if (!userSessionManager.isUserLoggedIn()) {
            HashMap<String, String> user = userSessionManager.getUserDetails();
            access_key = user.get(UserSessionManager.KEY_ACCSES);
            user_name = user.get(UserSessionManager.USER_NAME);
            email = user.get(UserSessionManager.USER_EMAIL);
            user_id = user.get(UserSessionManager.USER_ID);

            if (isOnline) {
                gettingProfileInformation(user_id);
            } else {
                progressDialog.dismiss();
            }
        }*/

        //Log.d("USER_ID",user_id);

        //  toolbar_title = (TextView) findViewById(R.id.toolbar);
        //   toolbar_title.setTypeface(typeface2);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);

        View navigationheadderView = mNavigationView.getHeaderView(0);

        loginLayout = (RelativeLayout) navigationheadderView.findViewById(R.id.loginLayout);
        loggedLayout = (LinearLayout) navigationheadderView.findViewById(R.id.loggedLayout);

        if (!userSessionManager.isUserLoggedIn()) {
            loginLayout.setVisibility(View.VISIBLE);
            loggedLayout.setVisibility(View.GONE);
        } else {
            loginLayout.setVisibility(View.GONE);
            loggedLayout.setVisibility(View.VISIBLE);
        }

        profile_imag = (ImageView) navigationheadderView.findViewById(R.id.profile_imag);
        profile_imag.setOnClickListener(this);
        user_name_txt = (TextView) navigationheadderView.findViewById(R.id.user_name_txt);
        user_name_txt.setTypeface(typeface2);
        //user_name_txt.setText(user_name);
        email_txt = (TextView) navigationheadderView.findViewById(R.id.email_txt);
        email_txt.setTypeface(typeface);
        //email_txt.setText(email);

        navdrawer_image = (ImageView) navigationheadderView.findViewById(R.id.navdrawer_image);
        profile_imag1 = (ImageView) navigationheadderView.findViewById(R.id.prifileImage);
        txtViewName = (TextView) navigationheadderView.findViewById(R.id.txtViewName);
        txtViewName.setTypeface(typeface);
        login = (TextView) navigationheadderView.findViewById(R.id.login);
        login.setOnClickListener(this);
        login.setTypeface(typeface2);

        register_action = (TextView) navigationheadderView.findViewById(R.id.register_action);
        register_action.setOnClickListener(this);
        register_action.setTypeface(typeface);


        Menu m = mNavigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            if (!userSessionManager.isUserLoggedIn()) {
                if (m.getItem(i).getTitle().equals("Profile"))
                    m.getItem(i).setVisible(false);
                if (m.getItem(i).getTitle().equals("Change Password"))
                    m.getItem(i).setVisible(false);
                if (m.getItem(i).getTitle().equals("Logout"))
                    m.getItem(i).setVisible(false);
            }

            MenuItem mi = m.getItem(i);
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            applyFontToMenuItem(mi);
        }

        mNavigationView.setItemIconTintList(null);

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();


        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.fragment_switch, new DummyHomeFragment()).commit();

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                mDrawerLayout.closeDrawers();

                switch (menuItem.getItemId()) {
                    case R.id.home:
                        if (statusFlag = NetworkChecking.isConnected(getApplicationContext())) {
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        } else {
                            Snackbar snack = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet connection...!", Snackbar.LENGTH_SHORT);
                            View view = snack.getView();
                            TextView tv = (TextView) view.findViewById(R.id.snackbar_text);
                            tv.setTextColor(Color.WHITE);
                            snack.show();
                        }
                        break;
                    case R.id.profile:
                        if (NetworkChecking.isConnected(MainActivity.this)) {
                            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    progressDialog.dismiss();

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String responceCode = jsonObject.getString("status");
                                        if (responceCode.equals("19999")) {
                                            progressDialog.show();
                                            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.VIEW_PROFILE_URL,
                                                    new Response.Listener<String>() {
                                                        @Override
                                                        public void onResponse(String response) {

                                                            try {
                                                                JSONObject jsonObject = new JSONObject(response);
                                                                String successCode = jsonObject.getString("status");
                                                                if (successCode.equals("10190")) {
                                                                    String profilename, profileage, profilegunder, profileemail, profilephone;
                                                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                                                    profilename = jsonObject1.getString("user_name");
                                                                    profileage = jsonObject1.getString("dob");
                                                                    profilegunder = jsonObject1.getString("gender");
                                                                    profileemail = jsonObject1.getString("email");
                                                                    profilephone = jsonObject1.getString("mobile");
                                                                    Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                                                                    intent.putExtra("pName", profilename);
                                                                    intent.putExtra("pAge", profileage);
                                                                    intent.putExtra("pGunder", profilegunder);
                                                                    intent.putExtra("pMail", profileemail);
                                                                    intent.putExtra("pPhone", profilephone);
                                                                    progressDialog.dismiss();
                                                                    startActivity(intent);
                                                                }
                                                            } catch (JSONException e) {
                                                                progressDialog.dismiss();
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    },
                                                    new Response.ErrorListener() {
                                                        @Override
                                                        public void onErrorResponse(VolleyError error) {
                                                            progressDialog.dismiss();

                                                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                                            } else if (error instanceof AuthFailureError) {

                                                            } else if (error instanceof ServerError) {

                                                            } else if (error instanceof NetworkError) {

                                                            } else if (error instanceof ParseError) {

                                                            }
                                                        }
                                                    }
                                            ) {
                                                @Override
                                                protected Map<String, String> getParams() throws AuthFailureError {
                                                    Map<String, String> params = new HashMap<String, String>();
                                                    params.put("user_id", user_id);
                                                    return params;
                                                }
                                            };
                                            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                                            requestQueue.add(stringRequest);
                                        }
                                        if (responceCode.equals("10140")) {

                                            userSessionManager.logoutUser();
                                            Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                            startActivity(intent);
                                        }
                                        if (responceCode.equals("10150")) {

                                            userSessionManager.logoutUser();
                                            Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                            startActivity(intent);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    progressDialog.dismiss();
                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    } else if (error instanceof AuthFailureError) {

                                    } else if (error instanceof ServerError) {

                                    } else if (error instanceof NetworkError) {

                                    } else if (error instanceof ParseError) {

                                    }
                                }
                            }) {
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    Map<String, String> params = new HashMap<String, String>();
                                    params.put("user_id", user_id);
                                    return params;
                                }
                            };
                            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
                            requestQueue.add(stringRequest);
                        } else {
                            showInternetStatus();
                        }

                        break;
                    case R.id.i_care_for:
                        if (userSessionManager.isUserLoggedIn()) {
                            startActivity(new Intent(getApplicationContext(), ViewUsers.class));
                        } else {

                            showAlert("I Care For");

//                            Intent intent = new Intent(getApplicationContext(), StaticLogin.class);
//                            intent.putExtra("getMessage", "I Care For");
//                            startActivity(intent);

                        }
                        break;
                    case R.id.articles:
                        if (userSessionManager.isUserLoggedIn()) {
                            Intent intent = new Intent(getApplicationContext(), HabitsList.class);
                            intent.putExtra("userId", user_id);
                            startActivity(intent);
                        } else {
                            showAlert("Habits Info");

//                            Intent intent = new Intent(getApplicationContext(), StaticLogin.class);
//                            intent.putExtra("getMessage", "Habits Info");
//                            startActivity(intent);
                        }
                        break;
                    case R.id.pref_articles:
                        if (userSessionManager.isUserLoggedIn()) {
                            if (NetworkUtil.isConnected(MainActivity.this)) {

                                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        progressDialog.dismiss();

                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            String responceCode = jsonObject.getString("status");
                                            if (responceCode.equals("19999")) {

                                                Intent intent = new Intent(getApplicationContext(), HabitNotificationUpdate.class);
                                                startActivity(intent);
                                            }
                                            if (responceCode.equals("10140")) {

                                                userSessionManager.logoutUser();
                                                Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                startActivity(intent);
                                            }

                                            if (responceCode.equals("10150")) {

                                                userSessionManager.logoutUser();
                                                Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                startActivity(intent);
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        progressDialog.dismiss();
                                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                        } else if (error instanceof AuthFailureError) {

                                        } else if (error instanceof ServerError) {

                                        } else if (error instanceof NetworkError) {

                                        } else if (error instanceof ParseError) {

                                        }
                                    }
                                }) {
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("user_id", user_id);
                                        return params;
                                    }
                                };
                                RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
                                requestQueue.add(stringRequest);
                            } else {
                                showInternetStatus();
                            }
                        } else {
                            showAlert("Habits Notifications Update ");

//                            Intent intent = new Intent(getApplicationContext(), StaticLogin.class);
//                            intent.putExtra("getMessage", "Habits Notifications Update ");
//                            startActivity(intent);
                        }
                        break;
                    case R.id.appointments:
                        if (userSessionManager.isUserLoggedIn()) {
                            Intent intent = new Intent(getApplicationContext(), OrdersList.class);
                            startActivity(intent);
                        } else {

                            showAlert("My Appointments");

//                            Intent intent = new Intent(getApplicationContext(), StaticLogin.class);
//                            intent.putExtra("getMessage", "My Appointments");
//                            startActivity(intent);
                        }
                        break;
                    case R.id.create_Packages:
                        if (userSessionManager.isUserLoggedIn()) {
                            if (NetworkUtil.isConnected(MainActivity.this)) {
                                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        progressDialog.dismiss();

                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            String responceCode = jsonObject.getString("status");
                                            if (responceCode.equals("19999")) {
                                                Intent intent = new Intent(getApplicationContext(), CreateMyPackage.class);
                                                startActivity(intent);
                                            }
                                            if (responceCode.equals("10140")) {
                                                userSessionManager.logoutUser();
                                                Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                startActivity(intent);
                                            }

                                            if (responceCode.equals("10150")) {

                                                userSessionManager.logoutUser();
                                                Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                startActivity(intent);
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        progressDialog.dismiss();
                                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                        } else if (error instanceof AuthFailureError) {

                                        } else if (error instanceof ServerError) {

                                        } else if (error instanceof NetworkError) {

                                        } else if (error instanceof ParseError) {

                                        }
                                    }
                                }) {
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("user_id", user_id);
                                        return params;
                                    }
                                };
                                RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
                                requestQueue.add(stringRequest);
                            } else {
                                showInternetStatus();
                            }
                        } else {

                            showAlert("My Packages");

//                            Intent intent = new Intent(getApplicationContext(), StaticLogin.class);
//                            intent.putExtra("getMessage", "My Packages");
//                            startActivity(intent);
                        }
                        break;
                    case R.id.view_Packages:
                        if (userSessionManager.isUserLoggedIn()) {

                            if (NetworkUtil.isConnected(MainActivity.this)) {
                                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        progressDialog.dismiss();

                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            String responceCode = jsonObject.getString("status");
                                            if (responceCode.equals("19999")) {
                                                Intent intent = new Intent(getApplicationContext(), ViewMyPackage.class);
                                                startActivity(intent);
                                            }
                                            if (responceCode.equals("10140")) {
                                                userSessionManager.logoutUser();
                                                Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                startActivity(intent);
                                            }

                                            if (responceCode.equals("10150")) {
                                                userSessionManager.logoutUser();
                                                Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                startActivity(intent);
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        progressDialog.dismiss();
                                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                                        } else if (error instanceof AuthFailureError) {

                                        } else if (error instanceof ServerError) {

                                        } else if (error instanceof NetworkError) {

                                        } else if (error instanceof ParseError) {

                                        }
                                    }
                                }) {
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("user_id", user_id);
                                        return params;
                                    }
                                };
                                RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
                                requestQueue.add(stringRequest);
                            } else {
                                showInternetStatus();
                            }
                        } else {

                            showAlert("View Package");

//                            Intent intent = new Intent(getApplicationContext(), StaticLogin.class);
//                            intent.putExtra("getMessage", "View Package");
//                            startActivity(intent);
                        }
                        break;
                    case R.id.rate:
                        Uri uri = Uri.parse("market://details?id=innasoft.com.mhcone&hl=en");
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);

                        try {
                            startActivity(goToMarket);

                        } catch (ActivityNotFoundException e) {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("https://play.google.com/store/apps/details?id=innasoft.com.mhcone&hl=en")));
                        }
                        break;
                    case R.id.about:
                        Intent intent = new Intent(getApplicationContext(), AboutUs.class);
                        startActivity(intent);
                        break;
                    case R.id.share:
                        if (statusFlag = NetworkChecking.isConnected(getApplicationContext())) {
                            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                            sharingIntent.setType("text/plain");
                            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Helthlike");
                            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=innasoft.com.mhcone&hl=en");
                            startActivity(Intent.createChooser(sharingIntent, "Share via"));
                        } else {
                            Snackbar snack = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet connection...!", Snackbar.LENGTH_SHORT);
                            View view = snack.getView();
                            TextView tv = (TextView) view.findViewById(R.id.snackbar_text);
                            tv.setTextColor(Color.WHITE);
                            snack.show();
                        }
                        break;
                    case R.id.change_password:
                        if (statusFlag = NetworkChecking.isConnected(getApplicationContext())) {
                            startActivity(new Intent(getApplicationContext(), ChangePassword.class));
                        } else {
                            Snackbar snack = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet connection...!", Snackbar.LENGTH_SHORT);
                            View view = snack.getView();
                            TextView tv = (TextView) view.findViewById(R.id.snackbar_text);
                            tv.setTextColor(Color.WHITE);
                            snack.show();
                        }
                        break;
                    case R.id.logout:
                        if (userSessionManager.isUserLoggedIn()) {

                            if (statusFlag = NetworkChecking.isConnected(getApplicationContext())) {
                                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOGOUT_URL,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                userSessionManager.logoutUser();
                                                Toast.makeText(MainActivity.this, "You Successfully Logged Out..!", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                startActivity(intent);
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                try {
                                                    String responseBody = new String(error.networkResponse.data, "utf-8");

                                                } catch (UnsupportedEncodingException e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        }
                                ) {
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {

                                        Map<String, String> params = new HashMap<String, String>();

                                        params.put("user_id", user_id);
                                        return params;
                                    }
                                };
                                RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
                                requestQueue.add(stringRequest);
                            } else {
                                Snackbar snack = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet connection...!", Snackbar.LENGTH_SHORT);
                                View view = snack.getView();
                                TextView tv = (TextView) view.findViewById(R.id.snackbar_text);
                                tv.setTextColor(Color.WHITE);
                                snack.show();
                            }
                        }
                        break;
                }
                return false;
            }

        });

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name, R.string.app_name);

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();
    }

    @Override
    protected void onResume() {
        super.onResume();
         if (userSessionManager.isUserLoggedIn()) {
                HashMap<String, String> user = userSessionManager.getUserDetails();
                access_key = user.get(UserSessionManager.KEY_ACCSES);
                user_name = user.get(UserSessionManager.USER_NAME);
                email = user.get(UserSessionManager.USER_EMAIL);
                user_id = user.get(UserSessionManager.USER_ID);
            if (isOnline) {
                gettingProfileInformation(user_id);
            } else {
                progressDialog.dismiss();
            }
            mNavigationView.getMenu().findItem(R.id.profile).setVisible(true);
             mNavigationView.getMenu().findItem(R.id.change_password).setVisible(true);
             mNavigationView.getMenu().findItem(R.id.logout).setVisible(true);
        }
        if (!userSessionManager.isUserLoggedIn()) {
            loginLayout.setVisibility(View.VISIBLE);
            loggedLayout.setVisibility(View.GONE);
        } else {
            loginLayout.setVisibility(View.GONE);
            loggedLayout.setVisibility(View.VISIBLE);
        }

    }

    private void showAlert(String title) {
        AlertDialog.Builder builder;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            builder = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Dialog_Alert);
//        } else {
            builder = new AlertDialog.Builder(MainActivity.this);
//        }
        builder.setTitle(title)
                .setMessage("Please Login...")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .show();
    }


    private void applyFontToMenuItem(MenuItem mi) {
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.robotoregular));
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", typeface), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    private void showInternetStatus() {

        LayoutInflater inflater = getLayoutInflater();
        View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
        TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
        textView.setTypeface(typeface);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(toastLayout);
        toast.show();
    }

    @Override
    public void onBackPressed() {

        FragmentManager manager = getSupportFragmentManager();

        if (this.mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.mDrawerLayout.closeDrawer(GravityCompat.START);

            if (exit) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                toast.cancel();
                startActivity(intent);
                moveTaskToBack(true);

            } else {

                LayoutInflater inflater = getLayoutInflater();
                View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
                textView.setTypeface(typeface);
                textView.setText("Press Back again to Exit.");
                toast = new Toast(getApplicationContext());

                manager.popBackStack();

                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setView(toastLayout);
                toast.show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 2 * 1000);
            }


        } else {

            if (exit) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                toast.cancel();
                startActivity(intent);

                moveTaskToBack(true);

            } else {

                LayoutInflater inflater = getLayoutInflater();
                View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
                textView.setTypeface(typeface);
                textView.setText("Press Back again to Exit.");
                toast = new Toast(getApplicationContext());

                manager.popBackStack();

                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setView(toastLayout);
                toast.show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 2 * 1000);
            }
        }
    }

    private void gettingProfileInformation(final String user_id) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_PROFILE_INFO,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Purushotham", "Sucesses-MainActivity - GETTINGPROFILE : " + response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("my-pic");

                                String userNameStringFromUrl = jsonObject1.getString("user_name");
                                String emailStringUrl = jsonObject1.getString("email");
                                String imageURL = AppUrls.IMAGE_URL + jsonObject1.getString("profile_pic");

                                user_name_txt.setText(userNameStringFromUrl);
                                email_txt.setText(emailStringUrl);
                                Picasso.with(getApplicationContext())
                                        .load(imageURL)
                                        .placeholder(R.drawable.myhealthlogo)
                                        .into(profile_imag);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                Log.d("IDFIND", user_id);
                params.put("user_id", user_id);
                return params;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.profile_imag:
                if (isOnline) {
                    final String[] country = {"Select from Gallery", "Take a Photo"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Select One")
                            .setItems(country, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    String selectedCountry = country[i];

                                    boolean result = ImagePermissions.checkPermission(MainActivity.this);

                                    if (selectedCountry.equals("Take a Photo")) {
                                        userChoosenTask = "Take a Photo";
                                        if (result)
                                            cameraIntent();

                                    } else if (selectedCountry.equals("Select from Gallery")) {
                                        userChoosenTask = "Select from Gallery";
                                        if (result)
                                            galleryIntent();

                                    }
                                }
                            });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else
                    showInternetStatus();
                break;
            case R.id.register_action:
                startActivity(new Intent(MainActivity.this, RegisterActivity.class));
                break;
            case R.id.login:
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                break;

        }
    }

    private void cameraIntent() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ContentValues values = new ContentValues(1);
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
            fileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            captureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            startActivityForResult(captureIntent, CAMERA_REQUEST_IMAGE);
        } else {
            try {
                Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
                captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                startActivityForResult(captureIntent, CAMERA_REQUEST_IMAGE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

/*        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file=getOutputMediaFile(1);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, CAMERA_REQUEST_IMAGE);*/
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), GALLERY_REQUEST_IMAGE);
    }

    private static File getOutputMediaFile(int type) {

        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), Config.IMAGE_DIRECTORY_NAME);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("TAG", "Oops! Failed create " + Config.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    private Uri getOutputMediaFileUri(int mediaTypeVideo) {

        return Uri.fromFile(getOutputMediaFile(mediaTypeVideo));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST_IMAGE) {
                try {
                    onSelectFromGalleryResult(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == CAMERA_REQUEST_IMAGE) {
                try {
                    onCaptureImageResult(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void onCaptureImageResult(Intent data) throws IOException {

        selectedFilePath = FilePath.getPath(this, fileUri);
        if (selectedFilePath != null && !selectedFilePath.equals("")) {

            try {
                File f = new File(selectedFilePath);
                ExifInterface exif = new ExifInterface(f.getPath());
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                int angle = 0;

                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                    angle = 90;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                    angle = 180;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                    angle = 270;
                }

                Matrix mat = new Matrix();
                mat.postRotate(angle);

                Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, null);
                Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);
                profile_imag.setImageBitmap(correctBmp);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                correctBmp.compress(Bitmap.CompressFormat.JPEG, 70, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                final String sendingimagepath = destination.getPath();

                Thread thread = new Thread(new Runnable() {
                    public void run() {
                        String responce_value = doFileUpload(sendingimagepath);
                        try {
                            JSONObject jsonObject = new JSONObject(responce_value);
                            String responceCode = jsonObject.getString("status");

                            if (responceCode.equals("50100")) {
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        try {
                                            runOnUiThread(new Runnable() {
                                                public void run() {
                                                    if (progressDialog.isShowing())
                                                        progressDialog.dismiss();
                                                }
                                            });

                                            Toast.makeText(getApplicationContext(), "Successfully Uploaded...!", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(MainActivity.this, MainActivity.class);
                                            startActivity(intent);


                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }


                                });
                            }
                            if (responceCode.equals("2000")) {
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        try {
                                            runOnUiThread(new Runnable() {
                                                public void run() {
                                                    if (progressDialog.isShowing())
                                                        progressDialog.dismiss();
                                                }
                                            });

                                            Toast.makeText(getApplicationContext(), "Successfully Uploaded...!", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(MainActivity.this, MainActivity.class);
                                            startActivity(intent);


                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }


                                });
                            }
                            if (responceCode.equals("5000")) {
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        try {
                                            runOnUiThread(new Runnable() {
                                                public void run() {
                                                    if (progressDialog.isShowing())
                                                        progressDialog.dismiss();
                                                }
                                            });


                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }


                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("RASPONCEDATAVALUE", responce_value);
                    }
                });
                thread.start();
            } catch (IOException e) {
                Log.w("TAG", "-- Error in setting image");
            } catch (OutOfMemoryError oom) {
                Log.w("TAG", "-- OOM Error in setting image");
            }
        }
    }

    private void onSelectFromGalleryResult(Intent data) throws IOException {

        Uri selectedFileUri = data.getData();
        selectedFilePath = FilePath.getPath(this, selectedFileUri);
        if (selectedFilePath != null && !selectedFilePath.equals("")) {

            try {
                File f = new File(selectedFilePath);
                ExifInterface exif = new ExifInterface(f.getPath());
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                int angle = 0;

                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                    angle = 90;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                    angle = 180;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                    angle = 270;
                }

                Matrix mat = new Matrix();
                mat.postRotate(angle);

                Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, null);
                Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);
                profile_imag.setImageBitmap(correctBmp);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                correctBmp.compress(Bitmap.CompressFormat.JPEG, 70, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                final String sendingimagepath = destination.getPath();

                Thread thread = new Thread(new Runnable() {
                    public void run() {
                        String responce_value = doFileUpload(sendingimagepath);
                        try {
                            JSONObject jsonObject = new JSONObject(responce_value);
                            String responceCode = jsonObject.getString("status");

                            if (responceCode.equals("50100")) {
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        try {
                                            runOnUiThread(new Runnable() {
                                                public void run() {
                                                    if (progressDialog.isShowing())
                                                        progressDialog.dismiss();
                                                }
                                            });

                                            Toast.makeText(getApplicationContext(), "Successfully Uploaded...!", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(MainActivity.this, MainActivity.class);
                                            startActivity(intent);


                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }


                                });
                            }
                            if (responceCode.equals("2000")) {
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        try {
                                            runOnUiThread(new Runnable() {
                                                public void run() {
                                                    if (progressDialog.isShowing())
                                                        progressDialog.dismiss();
                                                }
                                            });

                                            Toast.makeText(getApplicationContext(), "Successfully Uploaded...!", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(MainActivity.this, MainActivity.class);
                                            startActivity(intent);


                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }


                                });
                            }
                            if (responceCode.equals("5000")) {
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        try {
                                            runOnUiThread(new Runnable() {
                                                public void run() {
                                                    if (progressDialog.isShowing())
                                                        progressDialog.dismiss();
                                                }
                                            });
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }


                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.d("RASPONCEDATAVALUE", responce_value);
                    }
                });
                thread.start();
            } catch (IOException e) {
                Log.w("TAG", "-- Error in setting image");
            } catch (OutOfMemoryError oom) {
                Log.w("TAG", "-- OOM Error in setting image");
            }

        }
    }

    private String doFileUpload(String selectedFilePath) {

        MainActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.show();
            }
        });
        String response_str = null;
        File file = null;
        FileBody bin = null;
        file = new File(selectedFilePath.replaceAll("file://", "").replace("%20", " "));
        String urlString = AppUrls.UPDATE_PROFILE_PIC;
        Log.d("USERIDASAD", "URL   " + urlString);
        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(urlString);
            bin = new FileBody(file);
            MultipartEntity reqEntity = new MultipartEntity();

            reqEntity.addPart("user_id", new StringBody(user_id));
            reqEntity.addPart("userfile", bin);
            Log.d("USERIDASAD", "USERIDSD" + user_id);

            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post);
            resEntity = response.getEntity();
            response_str = EntityUtils.toString(resEntity);
            Log.d("USERIDASAD", "REPONCESEAFAF " + response_str);

        } catch (Exception ex) {
            Log.e("Debug", "error: " + ex.getMessage(), ex);
        }
        return response_str;
    }
}
