package innasoft.com.mhcone.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class MyHealthDBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 5;
    private static final String DATABASE_NAME = "myhealth.db";

    private static final String TABLE_ORDERSLIST = "orders";
    private static final String TABLE_PACKAGE = "packages";
    private static final String TABLE_HABITS = "habits";
    private static final String TABLE_ICARE = "icare";
    private static final String NEW_TEST_TABLE = "new_test_table";

    /*Package*/
    public static final String P_ID = "id";
    public static final String PACKAGE_NAME = "package_name";
    public static final String PACKAGE_IMAGE = "package_image";
    public static final String PACKAGE_ID = "package_id";
    public static final String PACKAGE_STATUS = "status";

    /*Habits*/
    public static final String HABITS_CATEGORY_ID = "habits_category_id";
    public static final String HABITS_CATEGORY = "habits_category";
    public static final String TITTLE = "tittle";
    public static final String ID = "id";
    public static final String HABITS_DESCRIPTION = "habits_description";
    public static final String STATUS = "status";
    public static final String CREATED_TIME = "created_time";
    public static final String USER_ID = "user_id";
    public static final String HABIT_IMAGE = "habit_image";


    /*Orders*/
    public static final String ORDERS_ID = "id";
    public static final String ORDERS_BOOKING_ID = "booking_id";
    public static final String ORDERS_BOOKING_DATE = "booking_date";
    public static final String ORDERS_GRAND_TOTAL = "grand_total";
    public static final String ORDERS_USER_ID = "user_id";
    public static final String ORDERS_FORM_TYPE = "form_type";
    public static final String ORDERS_LAB_ID = "lab_id";
    public static final String ORDERS_CREATED_TIME = "created_time";
    public static final String ORDERS_STATUS = "status";
    public static final String ORDERS_LAB_NAME = "lab_name";
    public static final String ORDERS_PAYMENT_TYPE = "payment_type";


    /*Icarefor*/
    public static final String ICARE_ID = "id";
    public static final String ICARE_USER_ID = "user_id";
    public static final String ICARE_SUBNAME = "subname";
    public static final String ICARE_RELATION = "relation";
    public static final String ICARE_DOB = "dob";
    public static final String ICARE_AGE = "age";
    public static final String ICARE_GENDER = "gender";
    public static final String ICARE_EMAIL = "email";
    public static final String ICARE_MOBILE = "mobile";
    public static final String ICARE_STATUS = "status";
    public static final String ICARE_CREATED_TIME = "created_time";

    /*new_package*/
    public static final String NEW_ID = "id";
    public static final String NEW_TEST_ID = "test_id";
    public static final String NEW_TEST_SUB_NAME = "test_sub_name";
    public static final String NEW_HOME_COLLECTION_STATUS = "home_collection_status";
    public static final String NEW_STATUS = "status";
    public static final String NEW_CREATED_TIME = "created_time";
    public static final String NEW_UPDATED_TIME = "updated_time";
    public static final String NEW_TEST_NAME = "test_name";
    public static final String NEW_TEST_IMAGE = "test_image";


    public MyHealthDBHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public MyHealthDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PACKAGE_TABLE = "CREATE TABLE " + TABLE_PACKAGE + "("
                + P_ID + " TEXT ,"
                + PACKAGE_NAME + " TEXT ,"
                + PACKAGE_IMAGE + " TEXT ,"
                + PACKAGE_ID + " TEXT ,"
                + PACKAGE_STATUS + " TEXT )";

        db.execSQL(CREATE_PACKAGE_TABLE);

        String CREATE_HABITS_TABLE = "CREATE TABLE " + TABLE_HABITS + "("
                + HABITS_CATEGORY_ID + " TEXT ,"
                + HABITS_CATEGORY + " TEXT ,"
                + TITTLE + " TEXT ,"
                + ID + " TEXT ,"
                + HABITS_DESCRIPTION + " TEXT ,"
                + STATUS + " TEXT ,"
                + CREATED_TIME + " TEXT ,"
                + USER_ID + " TEXT ,"
                + HABIT_IMAGE + " TEXT )";

        db.execSQL(CREATE_HABITS_TABLE);

        String CREATE_ORDERS_TABLE = "CREATE TABLE " + TABLE_ORDERSLIST + "("
                + ORDERS_ID + " TEXT ,"
                + ORDERS_BOOKING_ID + " TEXT ,"
                + ORDERS_BOOKING_DATE + " TEXT ,"
                + ORDERS_GRAND_TOTAL + " TEXT ,"
                + ORDERS_USER_ID + " TEXT ,"
                + ORDERS_FORM_TYPE + " TEXT ,"
                + ORDERS_LAB_ID + " TEXT ,"
                + ORDERS_CREATED_TIME + " TEXT ,"
                + ORDERS_STATUS + " TEXT ,"
                + ORDERS_LAB_NAME + " TEXT ,"
                + ORDERS_PAYMENT_TYPE + " TEXT)";

        db.execSQL(CREATE_ORDERS_TABLE);

        String CREATE_ICARE_TABLE = "CREATE TABLE " + TABLE_ICARE + "("
                + ICARE_ID + " TEXT ,"
                + ICARE_USER_ID + " TEXT ,"
                + ICARE_SUBNAME + " TEXT ,"
                + ICARE_RELATION + " TEXT ,"
                + ICARE_DOB + " TEXT ,"
                + ICARE_AGE + " TEXT ,"
                + ICARE_GENDER + " TEXT ,"
                + ICARE_EMAIL + " TEXT ,"
                + ICARE_MOBILE + " TEXT ,"
                + ICARE_STATUS + " TEXT ,"
                + ICARE_CREATED_TIME + " TEXT )";

        db.execSQL(CREATE_ICARE_TABLE);



    String CREATE_NEW_TEST_TABLE = "CREATE TABLE " + NEW_TEST_TABLE + "("
            + NEW_ID + " TEXT ,"
            + NEW_TEST_ID + " TEXT ,"
            + NEW_TEST_SUB_NAME + " TEXT ,"
            + NEW_HOME_COLLECTION_STATUS + " TEXT ,"
            + NEW_STATUS + " TEXT ,"
            + NEW_CREATED_TIME + " TEXT ,"
            + NEW_UPDATED_TIME + " TEXT ,"
            + NEW_TEST_NAME + " TEXT ,"
            + NEW_TEST_IMAGE + " TEXT )";

        db.execSQL(CREATE_NEW_TEST_TABLE);

}
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PACKAGE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HABITS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDERSLIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ICARE);
        db.execSQL("DROP TABLE IF EXISTS " + NEW_TEST_TABLE);

        onCreate(db);
    }
    /*IcareList*/
    public void addNewTestTable(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(NEW_TEST_TABLE, null, contentValues);
        db.close();
    }

    /*IcareList*/
    public void addIcareList(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_ICARE, null, contentValues);
        db.close();
    }

    /*OrdersList*/
    public void addOrdersList(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_ORDERSLIST, null, contentValues);
        db.close();
    }

    /*packages*/
    public void addPackagesList(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_PACKAGE, null, contentValues);
        db.close();
    }

    /*habits*/
    public void addHabitsList(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_HABITS, null, contentValues);
        db.close();
    }

    /*getNewTests*/
    public List<String> getNew_ID() {
        List<String> New_id = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + NEW_TEST_TABLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                New_id.add(cursor.getString(0));

            } while (cursor.moveToNext());
        }
        return New_id;
    }


    public List<String> getNew_Test_ID() {
        List<String> New_Test_Id = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + NEW_TEST_TABLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                New_Test_Id.add(cursor.getString(1));

            } while (cursor.moveToNext());
        }
        return New_Test_Id;
    }

    public List<String> getNew_Test_Sub_Name() {
        List<String> New_Test_Sub_Name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + NEW_TEST_TABLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                New_Test_Sub_Name.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }
        return New_Test_Sub_Name;
    }

    public List<String> getNew_Home_Collection_Status() {
        List<String> New_Home_Collection_Status = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + NEW_TEST_TABLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                New_Home_Collection_Status.add(cursor.getString(3));

            } while (cursor.moveToNext());
        }
        return New_Home_Collection_Status;
    }

    public List<String> getNew_Status() {
        List<String> New_Status = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + NEW_TEST_TABLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                New_Status.add(cursor.getString(4));

            } while (cursor.moveToNext());
        }
        return New_Status;
    }


    public List<String> getNew_Created_Time() {
        List<String> New_Created_Time = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + NEW_TEST_TABLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                New_Created_Time.add(cursor.getString(5));

            } while (cursor.moveToNext());
        }
        return New_Created_Time;
    }

    public List<String> getNew_Updated_Time() {
        List<String> New_Updated_Time = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + NEW_TEST_TABLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                New_Updated_Time.add(cursor.getString(6));

            } while (cursor.moveToNext());
        }
        return New_Updated_Time;
    }

    public List<String> getNew_Test_Name() {
        List<String> New_Test_Name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + NEW_TEST_TABLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                New_Test_Name.add(cursor.getString(7));

            } while (cursor.moveToNext());
        }
        return New_Test_Name;
    }

    public List<String> getNew_Test_Image() {
        List<String> New_Test_Image = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + NEW_TEST_TABLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                New_Test_Image.add(cursor.getString(8));

            } while (cursor.moveToNext());
        }
        return New_Test_Image;
    }


    /*getOrders*/
    public List<String> getI_ID() {
        List<String> I_id = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ICARE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                I_id.add(cursor.getString(0));

            } while (cursor.moveToNext());
        }
        return I_id;
    }


    public List<String> getI_UserID() {
        List<String> I_Userid = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ICARE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                I_Userid.add(cursor.getString(1));

            } while (cursor.moveToNext());
        }
        return I_Userid;
    }

    public List<String> getI_Subname() {
        List<String> I_Subname = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ICARE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                I_Subname.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }
        return I_Subname;
    }

    public List<String> getI_Relation() {
        List<String> I_Relation = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ICARE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                I_Relation.add(cursor.getString(3));

            } while (cursor.moveToNext());
        }
        return I_Relation;
    }

    public List<String> getI_Dob() {
        List<String> I_Dob = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ICARE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                I_Dob.add(cursor.getString(4));

            } while (cursor.moveToNext());
        }
        return I_Dob;
    }


    public List<String> getI_Age() {
        List<String> I_Age = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ICARE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                I_Age.add(cursor.getString(5));

            } while (cursor.moveToNext());
        }
        return I_Age;
    }

    public List<String> getI_Gender() {
        List<String> I_Gender = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ICARE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                I_Gender.add(cursor.getString(6));

            } while (cursor.moveToNext());
        }
        return I_Gender;
    }

    public List<String> getI_Email() {
        List<String> I_Email = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ICARE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                I_Email.add(cursor.getString(7));

            } while (cursor.moveToNext());
        }
        return I_Email;
    }

    public List<String> getI_Mobile() {
        List<String> I_Mobile = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ICARE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                I_Mobile.add(cursor.getString(8));

            } while (cursor.moveToNext());
        }
        return I_Mobile;
    }

    public List<String> getI_Status() {
        List<String> I_Status = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ICARE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                I_Status.add(cursor.getString(9));

            } while (cursor.moveToNext());
        }
        return I_Status;
    }

    public List<String> getI_Created_Time() {
        List<String> I_Created_Time = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ICARE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                I_Created_Time.add(cursor.getString(10));

            } while (cursor.moveToNext());
        }
        return I_Created_Time;
    }


    /*getOrders*/
    public List<String> getO_ID() {
        List<String> O_id = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ORDERSLIST;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                O_id.add(cursor.getString(0));

            } while (cursor.moveToNext());
        }
        return O_id;
    }


    public List<String> getO_BookingID() {
        List<String> O_Bookingid = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ORDERSLIST;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                O_Bookingid.add(cursor.getString(1));

            } while (cursor.moveToNext());
        }
        return O_Bookingid;
    }

    public List<String> getO_BookingDate() {
        List<String> O_Bookingdate = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ORDERSLIST;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                O_Bookingdate.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }
        return O_Bookingdate;
    }

    public List<String> getO_GrandTotal() {
        List<String> O_GrandTotal = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ORDERSLIST;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                O_GrandTotal.add(cursor.getString(3));

            } while (cursor.moveToNext());
        }
        return O_GrandTotal;
    }


    public List<String> getO_User_Id() {
        List<String> O_UserIds = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ORDERSLIST;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                O_UserIds.add(cursor.getString(4));

            } while (cursor.moveToNext());
        }
        return O_UserIds;
    }


    public List<String> getO_Form_Type() {
        List<String> O_Form_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ORDERSLIST;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                O_Form_type.add(cursor.getString(5));

            } while (cursor.moveToNext());
        }
        return O_Form_type;
    }

    public List<String> getO_Lab_Id() {
        List<String> O_Lab_id = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ORDERSLIST;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                O_Lab_id.add(cursor.getString(6));

            } while (cursor.moveToNext());
        }
        return O_Lab_id;
    }

    public List<String> getO_Created_time() {
        List<String> O_Created_time = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ORDERSLIST;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                O_Created_time.add(cursor.getString(7));

            } while (cursor.moveToNext());
        }
        return O_Created_time;
    }

    public List<String> getO_Status() {
        List<String> O_Status = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ORDERSLIST;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                O_Status.add(cursor.getString(8));

            } while (cursor.moveToNext());
        }
        return O_Status;
    }

    public List<String> getO_Lab_Name() {
        List<String> O_Lab_name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ORDERSLIST;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                O_Lab_name.add(cursor.getString(9));

            } while (cursor.moveToNext());
        }
        return O_Lab_name;
    }
    public List<String> getO_payment_type() {
        List<String> O_payment_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ORDERSLIST;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                O_payment_type.add(cursor.getString(10));

            } while (cursor.moveToNext());
        }
        return O_payment_type;
    }




    /*getPackages*/
    public List<String> getP_Id() {
        List<String> p_id = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_PACKAGE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                p_id.add(cursor.getString(0));

            } while (cursor.moveToNext());
        }
        return p_id;
    }

    public List<String> getP_Name() {
        List<String> p_Name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_PACKAGE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                p_Name.add(cursor.getString(1));

            } while (cursor.moveToNext());
        }
        return p_Name;
    }

    public List<String> getP_Image() {
        List<String> p_Image = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_PACKAGE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                p_Image.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }
        return p_Image;
    }

   /* public List<String> getPackage_Id() {
        List<String> package_Id = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_PACKAGE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                package_Id.add(cursor.get(3));

            } while (cursor.moveToNext());
        }
        return package_Id;
    }*/
    public List<String> getP_Status() {
        List<String> p_Status = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_PACKAGE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                p_Status.add(cursor.getString(3));

            } while (cursor.moveToNext());
        }
        return p_Status;
    }

    /*getHabits*/
    public List<String> getHABITS_CATEGORY_ID() {
        List<String> habits_Category_Id = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_HABITS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                habits_Category_Id.add(cursor.getString(0));

            } while (cursor.moveToNext());
        }
        return habits_Category_Id;
    }

    public List<String> getHABITS_CATEGORY() {
        List<String> habits_Category = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_HABITS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                habits_Category.add(cursor.getString(1));

            } while (cursor.moveToNext());
        }
        return habits_Category;
    }

    public List<String> getTittle() {
        List<String> tittle = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_HABITS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                tittle.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }
        return tittle;
    }

     public List<String> getId() {
         List<String> id = new ArrayList<String>();

         String selectQuery = "SELECT * FROM " + TABLE_HABITS;
         SQLiteDatabase db = this.getWritableDatabase();
         Cursor cursor = db.rawQuery(selectQuery, null);
         if (cursor.moveToFirst()){
             do {
                 id.add(cursor.getString(3));

             } while (cursor.moveToNext());
         }
         return id;
     }

    public List<String> getHabits_Description() {
        List<String> habits_Description = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_HABITS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                habits_Description.add(cursor.getString(4));

            } while (cursor.moveToNext());
        }
        return habits_Description;
    }

    public List<String> getStatus() {
        List<String> status = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_HABITS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                status.add(cursor.getString(5));

            } while (cursor.moveToNext());
        }
        return status;
    }

    public List<String> getCreated_Time() {
        List<String> created_Time = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_HABITS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                created_Time.add(cursor.getString(6));

            } while (cursor.moveToNext());
        }
        return created_Time;
    }

    public List<String> getUser_Id() {
        List<String> user_Id = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_HABITS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_Id.add(cursor.getString(7));

            } while (cursor.moveToNext());
        }
        return user_Id;
    }

    public List<String> getHabit_Image() {
        List<String> habit_Image = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_HABITS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                habit_Image.add(cursor.getString(8));

            } while (cursor.moveToNext());
        }
        return habit_Image;
    }
    public void deleteAllNewTests()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+NEW_TEST_TABLE); //delete all rows in a table
        db.close();
    }

    public void deleteAllHabitsRecords()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_HABITS); //delete all rows in a table
        db.close();
    }

    public void deleteAllPackages()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_PACKAGE); //delete all rows in a table
        db.close();
    }


    public void deleteAllOrders()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_ORDERSLIST); //delete all rows in a table
        db.close();
    }

    public void deleteAllIcare()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_ICARE); //delete all rows in a table
        db.close();
    }
}
