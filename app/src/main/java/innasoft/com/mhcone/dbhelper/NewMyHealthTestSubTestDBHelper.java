package innasoft.com.mhcone.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by purushotham on 23/2/17.
 */

public class NewMyHealthTestSubTestDBHelper extends SQLiteOpenHelper
{

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "myhealthtestsubtests.db";

    public static final String TABLE_TESTS = "tests";
    public static final String TABLE_SUBTESTS = "subtests";

    public static final String TEST_ID = "test_id";
    public static final String TEST_NAME = "test_name";
    public static final String TEST_IMAGE = "test_image";

    public static final String S_TEST_ID = "test_id";
    public static final String S_SUB_TEST_ID = "sub_test_id";
    public static final String S_SUB_TEST_NAME = "sub_test_name";


    public NewMyHealthTestSubTestDBHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }



    public NewMyHealthTestSubTestDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TEST_TABLE = "CREATE TABLE " + TABLE_TESTS + "("
                + TEST_ID + " TEXT ,"
                + TEST_NAME + " TEXT ,"
                + TEST_IMAGE + " TEXT )";

        db.execSQL(CREATE_TEST_TABLE);

        String CREATE_SUB_TEST_TABLE = "CREATE TABLE " + TABLE_SUBTESTS + "("
                + S_TEST_ID + " TEXT ,"
                + S_SUB_TEST_ID + " TEXT ,"
                + S_SUB_TEST_NAME + " TEXT )";

        db.execSQL(CREATE_SUB_TEST_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TESTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUBTESTS);

        onCreate(db);
    }



    public void addTestsList(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_TESTS, null, contentValues);
        db.close();
    }
    public void addSubTestsList(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_SUBTESTS, null, contentValues);
        db.close();
    }


    public List<String> getS_Test_Id() {
        List<String> s_Test_Id = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_SUBTESTS;

        Log.d("QUERYGGG", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                s_Test_Id.add(cursor.getString(0));

            } while (cursor.moveToNext());
        }
        return s_Test_Id;
    }

    public List<String> getS_Sub_Test_Id(String test_id) {
        List<String> s_Sub_Test_Id = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_SUBTESTS +" WHERE "+S_TEST_ID+" = "+test_id;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                s_Sub_Test_Id.add(cursor.getString(1));

            } while (cursor.moveToNext());
        }
        return s_Sub_Test_Id;
    }

    public List<String> getS_Sub_Test_Name(String test_id) {
        List<String> s_Sub_Test_Name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_SUBTESTS +" WHERE "+S_TEST_ID+" = "+test_id;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                s_Sub_Test_Name.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }
        return s_Sub_Test_Name;
    }

    public List<String> getTest_Id() {
        List<String> test_Id = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_TESTS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                test_Id.add(cursor.getString(0));

            } while (cursor.moveToNext());
        }
        return test_Id;
    }

    public List<String> getTest_Name() {
        List<String> test_Name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_TESTS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                test_Name.add(cursor.getString(1));

            } while (cursor.moveToNext());
        }
        return test_Name;
    }

    public List<String> getTest_Image() {
        List<String> test_Image = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_TESTS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                test_Image.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }
        return test_Image;
    }


    public void deleteAllTests()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_TESTS); //delete all rows in a table
        db.close();
    }

    public void deleteAllSubTests()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_SUBTESTS); //delete all rows in a table
        db.close();
    }
}
