package innasoft.com.mhcone.filters;

import android.widget.Filter;

import java.util.ArrayList;


import innasoft.com.mhcone.adapters.ShowLabsPackageAdapter;
import innasoft.com.mhcone.models.PackagesModel;
import innasoft.com.mhcone.models.ShowLabsPackageModel;

public class CustomFilterforShowLabsPackages extends Filter {

    ShowLabsPackageAdapter adapter;
    ArrayList<ShowLabsPackageModel> filterList;

    public CustomFilterforShowLabsPackages(ArrayList<ShowLabsPackageModel> filterList, ShowLabsPackageAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();
            ArrayList<ShowLabsPackageModel> filteredPlayers=new ArrayList<>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getShow_lab_package_user_name().toUpperCase().contains(constraint) || filterList.get(i).getShow_lab_package_address().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }

            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.showLabsPackageList = (ArrayList<ShowLabsPackageModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
