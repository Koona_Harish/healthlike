package innasoft.com.mhcone.filters;

import android.widget.Filter;

import java.util.ArrayList;

import innasoft.com.mhcone.adapters.PackagesAdapter;
import innasoft.com.mhcone.adapters.ShowLabsTestAdapter;
import innasoft.com.mhcone.models.PackagesModel;
import innasoft.com.mhcone.models.ShowLabsTestModel;

public class CustomFilterforShowLabsTest extends Filter
{
    ShowLabsTestAdapter adapter;
    ArrayList<ShowLabsTestModel> filterList;

    public CustomFilterforShowLabsTest(ArrayList<ShowLabsTestModel> filterList, ShowLabsTestAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();
            ArrayList<ShowLabsTestModel> filteredPlayers=new ArrayList<>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getShow_lab_user_name().toUpperCase().contains(constraint) || filterList.get(i).getShow_lab_address().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;

        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.showLabsTestList = (ArrayList<ShowLabsTestModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
