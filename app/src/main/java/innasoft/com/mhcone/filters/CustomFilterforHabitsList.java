package innasoft.com.mhcone.filters;

import android.widget.Filter;

import java.util.ArrayList;

import innasoft.com.mhcone.adapters.HabitsListAdapter;
import innasoft.com.mhcone.models.HabitsListModel;

public class CustomFilterforHabitsList extends Filter{

    HabitsListAdapter adapter;
    ArrayList<HabitsListModel> filterList;

    public CustomFilterforHabitsList(ArrayList<HabitsListModel> filterList, HabitsListAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<HabitsListModel> filteredPlayers=new ArrayList<>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getHabitsName().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.habitsListList = (ArrayList<HabitsListModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
