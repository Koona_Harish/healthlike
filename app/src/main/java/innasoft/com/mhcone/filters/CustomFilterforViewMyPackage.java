package innasoft.com.mhcone.filters;

import android.widget.Filter;

import java.util.ArrayList;

import innasoft.com.mhcone.adapters.ShowLabsTestAdapter;
import innasoft.com.mhcone.adapters.UserPackageNamesAdapter;
import innasoft.com.mhcone.adapters.ViewMyPackageAdapter;
import innasoft.com.mhcone.models.ShowLabsTestModel;
import innasoft.com.mhcone.models.UserPackageNamesModel;
import innasoft.com.mhcone.models.ViewMyPackageModel;

public class CustomFilterforViewMyPackage extends Filter
{
    UserPackageNamesAdapter adapter;
    ArrayList<UserPackageNamesModel> filterList;

    public CustomFilterforViewMyPackage(ArrayList<UserPackageNamesModel> filterList, UserPackageNamesAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();
            ArrayList<UserPackageNamesModel> filteredPlayers=new ArrayList<>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getUser_package_name().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }

            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.userpackageNamesModels = (ArrayList<UserPackageNamesModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
