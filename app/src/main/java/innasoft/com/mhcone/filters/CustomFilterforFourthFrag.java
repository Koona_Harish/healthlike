package innasoft.com.mhcone.filters;

import android.widget.Filter;

import java.util.ArrayList;

import innasoft.com.mhcone.adapters.BookAppointmentAdapter;
import innasoft.com.mhcone.adapters.ShowLabsListAdapter;
import innasoft.com.mhcone.models.BookAppointmentModel;
import innasoft.com.mhcone.models.ShowLabsListModel;

public class CustomFilterforFourthFrag extends Filter{

    BookAppointmentAdapter adapter;
    ArrayList<BookAppointmentModel> filterList;

    public CustomFilterforFourthFrag(ArrayList<BookAppointmentModel> filterList, BookAppointmentAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<BookAppointmentModel> filteredPlayers=new ArrayList<>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getShow_lab_book_user_name().toUpperCase().contains(constraint) || filterList.get(i).getShow_lab_book_address().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.bookAppointmentList = (ArrayList<BookAppointmentModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
