package innasoft.com.mhcone.filters;

import android.widget.Filter;

import java.util.ArrayList;

import innasoft.com.mhcone.adapters.ShowLabsPackageCitiesAdapter;
import innasoft.com.mhcone.adapters.ShowUserTestLabsCitiesAdapter;
import innasoft.com.mhcone.models.CitiesModel;

/**
 * Created by purushotham on 27/1/17.
 */

public class ShowUserTestLabsCustomFilterForCities extends Filter
{

    ShowUserTestLabsCitiesAdapter adapter;
    ArrayList<CitiesModel> filterList;
    public ShowUserTestLabsCustomFilterForCities(ArrayList<CitiesModel> filterList, ShowUserTestLabsCitiesAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<CitiesModel> filteredPlayers=new ArrayList<CitiesModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getCity_name().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.citiesModelArrayList = (ArrayList<CitiesModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
