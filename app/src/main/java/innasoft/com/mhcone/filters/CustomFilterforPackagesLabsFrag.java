package innasoft.com.mhcone.filters;

import android.widget.Filter;

import java.util.ArrayList;

import innasoft.com.mhcone.adapters.LabPackagesAdapter;
import innasoft.com.mhcone.models.LabPackagesModel;

public class CustomFilterforPackagesLabsFrag extends Filter
{
    LabPackagesAdapter adapter;
    ArrayList<LabPackagesModel> filterList;

    public CustomFilterforPackagesLabsFrag(ArrayList<LabPackagesModel> filterList, LabPackagesAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();
            ArrayList<LabPackagesModel> filteredPlayers=new ArrayList<>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getLab_packages_package_name().toUpperCase().contains(constraint) || filterList.get(i).getLab_packages_sub_test().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;

        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.labPackagesModelList = (ArrayList<LabPackagesModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
