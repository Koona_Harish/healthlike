package innasoft.com.mhcone.filters;

import android.widget.Filter;

import java.util.ArrayList;

import innasoft.com.mhcone.adapters.PackagesAdapter;
import innasoft.com.mhcone.adapters.TestSubAdapter;
import innasoft.com.mhcone.models.NewTestsModel;
import innasoft.com.mhcone.models.PackagesModel;

public class CustomFilterforTestsubtest extends Filter
{
    TestSubAdapter adapter;
    ArrayList<NewTestsModel> filterList;

    public CustomFilterforTestsubtest(ArrayList<NewTestsModel> filterList, TestSubAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();
            ArrayList<NewTestsModel> filteredPlayers=new ArrayList<>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getTest_sub_name().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }

            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.packagesModels = (ArrayList<NewTestsModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
