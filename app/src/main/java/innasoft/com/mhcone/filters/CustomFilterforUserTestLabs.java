package innasoft.com.mhcone.filters;

import android.widget.Filter;

import java.util.ArrayList;

import innasoft.com.mhcone.adapters.ShowUserTestLabsAdapter;
import innasoft.com.mhcone.adapters.UserPackageNamesAdapter;
import innasoft.com.mhcone.models.ShowUserTestLabsModel;
import innasoft.com.mhcone.models.UserPackageNamesModel;

public class CustomFilterforUserTestLabs extends Filter
{
    ShowUserTestLabsAdapter adapter;
    ArrayList<ShowUserTestLabsModel> filterList;

    public CustomFilterforUserTestLabs(ArrayList<ShowUserTestLabsModel> filterList, ShowUserTestLabsAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();
            ArrayList<ShowUserTestLabsModel> filteredPlayers=new ArrayList<>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getShow_user_test_lab_user_name().toUpperCase().contains(constraint) || filterList.get(i).getShow_user_test_lab_address().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;

        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.showUserTestLabsList = (ArrayList<ShowUserTestLabsModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
