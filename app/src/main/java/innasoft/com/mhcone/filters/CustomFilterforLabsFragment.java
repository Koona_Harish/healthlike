package innasoft.com.mhcone.filters;

import android.widget.Filter;

import java.util.ArrayList;

import innasoft.com.mhcone.adapters.ShowLabsListAdapter;
import innasoft.com.mhcone.models.ShowLabsListModel;

public class CustomFilterforLabsFragment extends Filter{

    ShowLabsListAdapter adapter;
    ArrayList<ShowLabsListModel> filterList;

    public CustomFilterforLabsFragment(ArrayList<ShowLabsListModel> filterList, ShowLabsListAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {

            constraint=constraint.toString().toUpperCase();
            ArrayList<ShowLabsListModel> filteredPlayers=new ArrayList<>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getShow_lab_list_user_name().toUpperCase().contains(constraint) || filterList.get(i).getShow_lab_list_address().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.showLabsListList = (ArrayList<ShowLabsListModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
