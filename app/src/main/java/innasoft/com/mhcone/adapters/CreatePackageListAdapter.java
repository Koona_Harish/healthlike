package innasoft.com.mhcone.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.List;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.CreateMyPackage;
import innasoft.com.mhcone.models.CreatePackageSubtestModel;
import innasoft.com.mhcone.models.CreatePackageTestsModel;
import innasoft.com.mhcone.utilities.CreatePackageDBHandler;

public class CreatePackageListAdapter extends BaseExpandableListAdapter {

    Context context;
    public static List<CreatePackageTestsModel> superItemList;
    CreatePackageDBHandler dbHandler;
    Typeface type_lato,typeface;
    public CreatePackageListAdapter(CreateMyPackage mainActivity, List<CreatePackageTestsModel> superItemList) {

        this.context = mainActivity;
        this.superItemList = superItemList;

        dbHandler = new CreatePackageDBHandler(context);
        type_lato = Typeface.createFromAsset(context.getAssets(), "robotoregular.ttf");
        typeface = Typeface.createFromAsset(context.getAssets(), "robotobold.ttf");
    }

    @Override
    public int getGroupCount() {
        return superItemList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        System.out.println("groupPositiongroupPosition" + groupPosition);

        return superItemList.get(groupPosition).getSubtestModel().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return superItemList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return superItemList.get(groupPosition).getSubtestModel().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        CreatePackageTestsModel tm = (CreatePackageTestsModel) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.create_package_list_parenet, null);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.txtParent);
        textView.setTypeface(typeface);
        String test_name = tm.getTest_name().substring(0, 1).toUpperCase()+ tm.getTest_name().substring(1);
        textView.setText(test_name);

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final CreatePackageSubtestModel subTest = (CreatePackageSubtestModel) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.create_package_list_child, null);
        }

        List<String> labs = dbHandler.getAllSubTests();

        CheckBox cbChildView = (CheckBox) convertView.findViewById(R.id.cbChildView);

        cbChildView.setTag(R.string.child, childPosition);
        cbChildView.setTag(R.string.group, groupPosition);
        cbChildView.setOnCheckedChangeListener(null);
        cbChildView.setChecked(subTest.isChecked());
        cbChildView.setTypeface(type_lato);
        String subtest_name = subTest.getSub_test_name().substring(0, 1).toUpperCase()+ subTest.getSub_test_name().substring(1);
        cbChildView.setText(subtest_name);

        cbChildView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                int group = (int) compoundButton.getTag(R.string.group);
                int child = (int) compoundButton.getTag(R.string.child);

                if(b)
                {
                    ContentValues values = new ContentValues();
                    values.put(CreatePackageDBHandler.KEY_SUB_TEST_ID, subTest.getSub_test_id()); // Shop Name
                    values.put(CreatePackageDBHandler.KEY_SUB_TEST_NAME, subTest.getSub_test_name()); // Shop Phone Number
                    values.put(CreatePackageDBHandler.KEY_SUB_TEST_STATUS, "true");

                    values.put(CreatePackageDBHandler.KEY_TEST_NAME, superItemList.get(groupPosition).getTest_name());
                    values.put(CreatePackageDBHandler.KEY_TEST_ID, subTest.getTest_id());

                    dbHandler.addSubtest(values);
                }
                else {
                    dbHandler.deleteSubtestID(subTest.getSub_test_id());
                }
                superItemList.get(group).getSubtestModel().get(child).setChecked(b);
            }
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
