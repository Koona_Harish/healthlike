package innasoft.com.mhcone.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.fragments.TestsFragment;
import innasoft.com.mhcone.holders.TestsHolder;
import innasoft.com.mhcone.models.TestsModel;

public class TestsAdapter extends RecyclerView.Adapter<TestsHolder>
{
    private ArrayList<TestsModel> testList;
    private TestsFragment context;
    LayoutInflater vi;
    int resource;
    private Typeface type_lato;

    public TestsAdapter(ArrayList<TestsModel> testList, TestsFragment context, int resource) {
        this.testList = testList;
        this.context = context;
        this.resource = resource;
        vi = (LayoutInflater)context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public TestsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = vi.inflate(resource, parent,false);
        TestsHolder ndvh = new TestsHolder(layoutView, (TestsHolder.MyViewHolder) context);
        return ndvh;
    }

    @Override
    public void onBindViewHolder(TestsHolder holder, int position) {
        type_lato = Typeface.createFromAsset(context.getActivity().getAssets(), "robotoregular.ttf");

        holder.testsTitle.setTypeface(type_lato);
        holder.testsTitle.setText(testList.get(position).getTest_name());
        Picasso.with(context.getActivity())
                .load(testList.get(position).getTest_image())
                .placeholder(R.drawable.myhealthlogo)
                .fit()
                .into(holder.testsImage);
    }

    @Override
    public int getItemCount() {
       return this.testList.size();
    }
}
