package innasoft.com.mhcone.adapters;

/**
 * Created by administator on 4/3/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.models.NewTestsModel;


public class CustomerAdapter extends ArrayAdapter<NewTestsModel> {

    Context mContext;
    ArrayList<NewTestsModel> customers;
    LayoutInflater layoutInflater;
    private ValueFilter valueFilter;
    ArrayList<NewTestsModel> categoryFilterList;


    public CustomerAdapter(Context context, ArrayList<NewTestsModel> objects) {
        super(context, R.layout.customer_row, objects);
        mContext = context;
        this.customers = objects;
        categoryFilterList = objects;
    }

    static class ViewHolder {
        TextView textTitle;
    }

    @Override
    public int getCount() {
        return customers.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        layoutInflater = LayoutInflater.from(mContext);
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.customer_row, null);
            holder = new ViewHolder();
            holder.textTitle = (TextView) convertView.findViewById(R.id.tvCustomer);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.textTitle.setText(customers.get(position).getTest_sub_name());


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v("Category name", customers.get(position).getTest_sub_name());
                Log.v("Category id", customers.get(position).getTest_id());

//                showSubcategory(listData.get(position).id);
//                MainActivity.dialog.dismiss();

                Intent intent = new Intent("custom-event-name");
                // You can also include some extra data.
                intent.putExtra("SubName", customers.get(position).getTest_sub_name());
                intent.putExtra("id", customers.get(position).getId());
                intent.putExtra("testId", customers.get(position).getTest_id());
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

            }
        });

        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }

        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<NewTestsModel> filterList = new ArrayList<>();
                for (int i = 0; i < categoryFilterList.size(); i++) {
                    if ((categoryFilterList.get(i).getTest_sub_name().toLowerCase()).contains(constraint.toString().toLowerCase())) {
                        filterList.add(categoryFilterList.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = categoryFilterList.size();
                results.values = categoryFilterList;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            customers = (ArrayList<NewTestsModel>) results.values;
            notifyDataSetChanged();
        }

    }
}