package innasoft.com.mhcone.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.LabPackages;
import innasoft.com.mhcone.activities.ShowLabInformation;
import innasoft.com.mhcone.holders.PopularLabHolder;
import innasoft.com.mhcone.itemclicklistener.PopularLabItemClickListener;
import innasoft.com.mhcone.models.PopularLabModel;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PopularLabAdapter extends RecyclerView.Adapter<PopularLabHolder>{

    public ArrayList<PopularLabModel> popularLabModels;
    Fragment context;
    Activity context2;
    LayoutInflater li;
    int resource;
    Typeface typeface,typeface2;
    private boolean checkInternet;

    public PopularLabAdapter(ArrayList<PopularLabModel> popularLabModels, Fragment context, int resource) {
        this.popularLabModels = popularLabModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.robotoregular));
        typeface2 = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.robotobold));
    }

    @Override
    public PopularLabHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        PopularLabHolder slh = new PopularLabHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final PopularLabHolder holder, final int position)
    {

        holder.lab_name_txt.setTypeface(typeface2);
        String popular_lab=popularLabModels.get(position).getUser_name().substring(0, 1).toUpperCase()+ popularLabModels.get(position).getUser_name().substring(1);
        holder.lab_name_txt.setText(popular_lab);

        Picasso.with(context.getActivity())
                .load(popularLabModels.get(position).getProfile_pic())
                .placeholder(R.drawable.logo)
                .into(holder.lab_img);

        holder.setItemClickListener(new PopularLabItemClickListener() {
            @Override
            public void onItemClick(View v, int pos)
            {
                Intent intent = new Intent(context.getActivity(), LabPackages.class);
                intent.putExtra("labId",popularLabModels.get(position).getId());
                intent.putExtra("labName",popularLabModels.get(position).getUser_name());
                intent.putExtra("payment_lab", popularLabModels.get(position).getPayment_lab());
                intent.putExtra("payment_online", popularLabModels.get(position).getPayment_online());
                intent.putExtra("tele_booking", popularLabModels.get(position).getTele_booking());
                intent.putExtra("labPhone", popularLabModels.get(position).getUser_name());
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.popularLabModels.size();
    }

}
