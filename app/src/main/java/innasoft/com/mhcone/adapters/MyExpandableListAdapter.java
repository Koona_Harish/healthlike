package innasoft.com.mhcone.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.List;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.fragments.TestSubTestFragment;
import innasoft.com.mhcone.models.MainSubtestModel;
import innasoft.com.mhcone.models.MainTestsModel;
import innasoft.com.mhcone.utilities.DBHandler;

public class MyExpandableListAdapter extends BaseExpandableListAdapter {

    TestSubTestFragment context;
    public static List<MainTestsModel> superItemList;
    DBHandler dbHandler;
    public Typeface type_lato,typeface;

    public MyExpandableListAdapter(TestSubTestFragment mainActivity, List<MainTestsModel> superItemList) {

        this.context = mainActivity;
        this.superItemList = superItemList;
        dbHandler = new DBHandler(context.getActivity());
        type_lato = Typeface.createFromAsset(context.getActivity().getAssets(), "robotoregular.ttf");
        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), "robotobold.ttf");
    }

    @Override
    public int getGroupCount() {
        return superItemList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        return superItemList.get(groupPosition).getSubtestModel().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return superItemList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return superItemList.get(groupPosition).getSubtestModel().get
                (childPosition);
    }

    @Override
    public long getGroupId(int groupPosition)
    {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        MainTestsModel tm = (MainTestsModel) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getActivity().getSystemService(context.getActivity().LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_parenet, null);

        }
        TextView textView = (TextView) convertView.findViewById(R.id.txtParent);
        textView.setTypeface(typeface);
        String test_name = tm.getTest_name().substring(0, 1).toUpperCase()+ tm.getTest_name().substring(1);
        textView.setText(test_name);

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final MainSubtestModel subTest = (MainSubtestModel) getChild
                (groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getActivity
                    ().getSystemService(context.getActivity().LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_child, null);

        }

        List<String> labs = dbHandler.getAllSubTests();

        CheckBox cbChildView = (CheckBox) convertView.findViewById(R.id.cbChildView);

        cbChildView.setTag(R.string.child, childPosition);
        cbChildView.setTag(R.string.group, groupPosition);
        cbChildView.setOnCheckedChangeListener(null);
        cbChildView.setChecked(subTest.isChecked());
        cbChildView.setTypeface(type_lato);
        String subtest_name = subTest.getSub_test_name().substring(0, 1).toUpperCase()+ subTest.getSub_test_name().substring(1);
        cbChildView.setText(subtest_name);

        cbChildView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton,boolean b) {
                int group = (int) compoundButton.getTag(R.string.group);
                int child = (int) compoundButton.getTag(R.string.child);

                if(b)
                {
                    ContentValues values = new ContentValues();
                    values.put(DBHandler.KEY_SUB_TEST_ID,
                            subTest.getSub_test_id()); // Shop Name
                    values.put(DBHandler.KEY_SUB_TEST_NAME,
                            subTest.getSub_test_name()); // Shop Phone Number
                    values.put(DBHandler.KEY_SUB_TEST_STATUS, "true");
                    values.put(DBHandler.KEY_TEST_NAME, superItemList.get
                            (groupPosition).getTest_name());
                    values.put(DBHandler.KEY_TEST_ID, subTest.getTest_id());
                    dbHandler.addSubtest(values);
                }
                else {
                    dbHandler.deleteSubtestID(subTest.getSub_test_id());
                }
                superItemList.get(group).getSubtestModel().get
                        (child).setChecked(b);
            }
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
