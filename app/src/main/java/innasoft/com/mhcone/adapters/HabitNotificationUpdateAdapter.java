package innasoft.com.mhcone.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.util.ArrayList;

import innasoft.com.mhcone.activities.HabitNotificationUpdate;
import innasoft.com.mhcone.holders.HabitNotificationUpdateHolder;
import innasoft.com.mhcone.models.HabitNotificationUpdateModel;

public class HabitNotificationUpdateAdapter extends RecyclerView.Adapter<HabitNotificationUpdateHolder>
{
    private ArrayList<HabitNotificationUpdateModel> habitNotificationUpdateModelList;
    HabitNotificationUpdate context;
    LayoutInflater vl;
    int resource;
    Typeface typeface;

    public HabitNotificationUpdateAdapter(ArrayList<HabitNotificationUpdateModel> habitNotificationUpdateModelList, HabitNotificationUpdate context, int resource) {
        this.habitNotificationUpdateModelList = habitNotificationUpdateModelList;
        this.context = context;
        this.resource = resource;
        vl = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), "robotoregular.ttf");
    }

    @Override
    public HabitNotificationUpdateHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutLabPackages = vl.inflate(resource,parent,false);
        HabitNotificationUpdateHolder hnuh = new HabitNotificationUpdateHolder(layoutLabPackages, (HabitNotificationUpdateHolder.MyHabitUpdateHolder) context);
        return hnuh;
    }

    @Override
    public void onBindViewHolder(final HabitNotificationUpdateHolder holder, final int position) {
           holder.habitUpdateCK.setTypeface(typeface);
            holder.habitUpdateCK.setText(habitNotificationUpdateModelList.get(position).getHabit_name());

        if(habitNotificationUpdateModelList.get(position).getStatus().equals("0"))
        {
            holder.habitUpdateCK.setChecked(true);
        }
        holder.habitUpdateCK.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked())
                {
                    habitNotificationUpdateModelList.get(position).setStatus("0");
                }
                else {
                    habitNotificationUpdateModelList.get(position).setStatus("1");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.habitNotificationUpdateModelList.size();
    }
}
