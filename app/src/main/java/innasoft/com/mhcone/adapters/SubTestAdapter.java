package innasoft.com.mhcone.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import innasoft.com.mhcone.activities.SubTestsActivity;
import innasoft.com.mhcone.holders.SubTestHolder;
import innasoft.com.mhcone.models.SubTestModel;
import innasoft.com.mhcone.utilities.DBHandler;

public class SubTestAdapter extends RecyclerView.Adapter<SubTestHolder>
{
    private ArrayList<SubTestModel> subTestList;
    private SubTestsActivity context;
    LayoutInflater vi;
    int resource;
    private Typeface typeface;
    DBHandler db;

    public SubTestAdapter(ArrayList<SubTestModel> subTestList, SubTestsActivity context, int resource) {
        this.subTestList = subTestList;
        this.context = context;
        this.resource = resource;
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        db = new DBHandler(context);
    }

    @Override
    public SubTestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = vi.inflate(resource, parent,false);
        SubTestHolder stvh = new SubTestHolder(layoutView, (SubTestHolder.MyViewHolder) context);
        return stvh;
    }

    @Override
    public void onBindViewHolder(SubTestHolder holder, final int position) {
        typeface = Typeface.createFromAsset(context.getAssets(), "robotoregular.ttf");
        holder.subtestName.setTypeface(typeface);
        holder.subtestName.setText(subTestList.get(position).getSub_test_name());

        List<String> labs = db.getAllSubTests(subTestList.get(position).getTest_id());

        if(labs.toString().contains(subTestList.get(position).getSub_test_id()))
        {
            holder.selectSubTestsCK.setChecked(true);
        }

        holder.selectSubTestsCK.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    subTestList.get(position).setIschecked("true");
                    ContentValues values = new ContentValues();
                    values.put(DBHandler.KEY_SUB_TEST_ID, subTestList.get(position).getSub_test_id()); // Shop Name
                    values.put(DBHandler.KEY_SUB_TEST_NAME, subTestList.get(position).getSub_test_name()); // Shop Phone Number
                    values.put(DBHandler.KEY_SUB_TEST_STATUS, subTestList.get(position).getIschecked());
                    values.put(DBHandler.KEY_TEST_ID, subTestList.get(position).getTest_id());

                    db.addSubtest(values);

                }
                else {
                    subTestList.get(position).setIschecked("false");
                    db.deleteSubtestID(subTestList.get(position).getSub_test_id());
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.subTestList.size();
    }
}
