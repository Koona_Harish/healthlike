package innasoft.com.mhcone.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.PackagesSubTests;
import innasoft.com.mhcone.holders.PopularPackageHolder;
import innasoft.com.mhcone.itemclicklistener.PopularPackageItemClickListener;
import innasoft.com.mhcone.models.PopularPackageModel;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class PopularPackageAdapter extends RecyclerView.Adapter<PopularPackageHolder>{

    public ArrayList<PopularPackageModel> popularPackageModels;
    Fragment context;
    Activity context2;
    LayoutInflater li;
    int resource;
    Typeface typeface,typeface2;
    UserSessionManager userSessionManager;
    NetworkStatus ns;
    Boolean isOnline = false;
    private boolean checkInternet;

    public PopularPackageAdapter(ArrayList<PopularPackageModel> popularPackageModels, Fragment context, int resource) {
        this.popularPackageModels = popularPackageModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        userSessionManager = new UserSessionManager(context.getActivity());

        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.robotoregular));
        typeface2 = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.robotobold));
    }

    @Override
    public PopularPackageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        PopularPackageHolder slh = new PopularPackageHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final PopularPackageHolder holder, final int position)
    {

        holder.package_name_txt.setTypeface(typeface2);
        String popular_pack=popularPackageModels.get(position).getPackage_name().substring(0, 1).toUpperCase()+ popularPackageModels.get(position).getPackage_name().substring(1);
        holder.package_name_txt.setText(popular_pack);

        Picasso.with(context.getActivity())
                .load(popularPackageModels.get(position).getPackage_image())
                .placeholder(R.drawable.logo)
                .into(holder.package_img);

        holder.setItemClickListener(new PopularPackageItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {
                ns = new NetworkStatus();
                isOnline = ns.isOnline(context.getActivity());


                if(isOnline) {
//                    if (userSessionManager.checkLogin() != false)
//                    {
//
//                        Toast.makeText(context.getActivity(), "Please Login...!", Toast.LENGTH_SHORT).show();
//
//                    } else {
                        Intent intent = new Intent(context.getActivity(), PackagesSubTests.class);
                        intent.putExtra("package_id", popularPackageModels.get(pos).getId());
                        intent.putExtra("package_name", popularPackageModels.get(pos).getPackage_name());
                      //  intent.putExtra("package_popular","package_popular");
                        context.startActivity(intent);
//                    }
                }else {
                    Toast.makeText(context.getActivity(), "Please Check your Internet connection..!", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.popularPackageModels.size();
    }

}
