package innasoft.com.mhcone.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.PackagesSubTests;
import innasoft.com.mhcone.filters.CustomFilterforPackages;
import innasoft.com.mhcone.fragments.PackagessFragment;
import innasoft.com.mhcone.holders.PackagesHolder;
import innasoft.com.mhcone.itemclicklistener.PackageItemClickListener;
import innasoft.com.mhcone.models.PackagesModel;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class PackagesAdapter extends RecyclerView.Adapter<PackagesHolder> implements Filterable {

    public ArrayList<PackagesModel> packagesModels, filterList;
    private PackagessFragment context;
    LayoutInflater vi;
    int resource;
    private Typeface typeface, typeface2;
    UserSessionManager userSessionManager;
    NetworkStatus ns;
    String packg_name, pack_id;
    Boolean isOnline = false;
    CustomFilterforPackages filter;
    String pack_name;

    public PackagesAdapter(ArrayList<PackagesModel> packagesModels, PackagessFragment context, int resource) {
        this.packagesModels = packagesModels;
        this.context = context;
        this.resource = resource;
        this.filterList = packagesModels;
        vi = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        userSessionManager = new UserSessionManager(context.getActivity());
    }

    @Override
    public PackagesHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        PackagesHolder holder = new PackagesHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(PackagesHolder holder, final int position) {

        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), "robotoregular.ttf");
        typeface2 = Typeface.createFromAsset(context.getActivity().getAssets(), "robotobold.ttf");

        holder.packageName.setTypeface(typeface2);
        pack_name = packagesModels.get(position).getP_name().substring(0, 1).toUpperCase() + packagesModels.get(position).getP_name().substring(1);
        holder.packageName.setText(pack_name);

        Log.d("PATHHH", packagesModels.get(position).getP_images_url());
        Picasso.with(context.getActivity())
                .load(packagesModels.get(position).getP_images_url())
                .placeholder(R.drawable.myhealthlogo)
                .fit()
                .into(holder.package_pic);

        pack_id = packagesModels.get(position).getPackage_id();

        holder.setItemClickListener(new PackageItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

                ns = new NetworkStatus();
                isOnline = ns.isOnline(context.getActivity());


                if (isOnline) {
//                    if (userSessionManager.checkLogin() != false) {
//                        Toast.makeText(context.getActivity(), "Please Login...!", Toast.LENGTH_SHORT).show();
//
//                    } else {
                        Intent intent = new Intent(context.getActivity(), PackagesSubTests.class);
                        intent.putExtra("package_id", packagesModels.get(pos).getP_id());
                        intent.putExtra("package_name", packagesModels.get(pos).getP_name());

                        Log.d("PACKINFOADAPTER:", packagesModels.get(pos).getPackage_id() + " / " + pack_name);
                        context.startActivity(intent);
//                    }
                } else {
                    Toast.makeText(context.getActivity(), "Please Check your Internet connection..!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.packagesModels.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterforPackages(filterList, this);
        }

        return filter;
    }
}
