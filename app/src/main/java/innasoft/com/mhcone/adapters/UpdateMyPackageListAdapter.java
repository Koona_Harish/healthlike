package innasoft.com.mhcone.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.UpdateMyPackage;
import innasoft.com.mhcone.models.UpdateMyPackageSubtestModel;
import innasoft.com.mhcone.models.UpdateMyPackageTestsModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.UpdatePackageDBHandler;

public class UpdateMyPackageListAdapter extends BaseExpandableListAdapter {

    Context context;
    public static List<UpdateMyPackageTestsModel> superItemList;
    String myPackageId;
    String selecteList;
    String selectedList = null;
    UpdatePackageDBHandler dbHandler;

    public UpdateMyPackageListAdapter(UpdateMyPackage mainActivity, List<UpdateMyPackageTestsModel> superItemList, String myPackageId) {

        this.context = mainActivity;
        this.superItemList = superItemList;
        this.myPackageId = myPackageId;
        dbHandler = new UpdatePackageDBHandler(context);
        gettinMySelectedList(myPackageId);

    }

    private void gettinMySelectedList(final String myPackageId) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.MYPACKAGES_INFO,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            selectedList = jsonObject.getString("sub_test_id");
                            returnSelectetdIds(selectedList);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_package_id", myPackageId);


                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    private void returnSelectetdIds(String selectedList) {
        selecteList = selectedList;
    }

    @Override
    public int getGroupCount() {
        return superItemList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        return superItemList.get(groupPosition).getSubtestModel().size();

    }

    @Override
    public Object getGroup(int groupPosition) {
        return superItemList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return superItemList.get(groupPosition).getSubtestModel().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        UpdateMyPackageTestsModel tm = (UpdateMyPackageTestsModel) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.update_my_package_list_parenet, null);

        }
        TextView textView = (TextView) convertView.findViewById(R.id.txtParent);
        textView.setText(tm.getTest_name());

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final UpdateMyPackageSubtestModel subTest = (UpdateMyPackageSubtestModel) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.update_my_package_list_child, null);

        }

        CheckBox cbChildView = (CheckBox) convertView.findViewById(R.id.cbChildView);

        cbChildView.setTag(R.string.child, childPosition);
        cbChildView.setTag(R.string.group, groupPosition);
        cbChildView.setOnCheckedChangeListener(null);
        cbChildView.setChecked(subTest.isChecked());
        cbChildView.setText(subTest.getSub_test_name());

         if(selecteList.toString().contains(subTest.getSub_test_id()))
        {
            List<String> insertedList = dbHandler.getAllSubTestIds();
            String convertedString = insertedList.toString().toString().replaceAll("\\[", "").replaceAll("\\]","").replaceAll(" ", "");

            if(convertedString.contains(subTest.getSub_test_id())) {
                if (!cbChildView.isChecked()) {
                    cbChildView.setChecked(true);
                }
            }
            else {

            }
        }

        cbChildView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                int group = (int) compoundButton.getTag(R.string.group);
                int child = (int) compoundButton.getTag(R.string.child);

                if(b)
                {
                    ContentValues values = new ContentValues();
                    values.put(UpdatePackageDBHandler.KEY_SUB_TEST_ID, subTest.getSub_test_id());
                    values.put(UpdatePackageDBHandler.KEY_SUB_TEST_NAME, subTest.getSub_test_name());
                    values.put(UpdatePackageDBHandler.KEY_SUB_TEST_STATUS, "true");
                    values.put(UpdatePackageDBHandler.KEY_TEST_NAME, superItemList.get(groupPosition).getTest_name());
                    values.put(UpdatePackageDBHandler.KEY_TEST_ID, subTest.getTest_id());

                    dbHandler.addSubtest(values);
                }
                else {
                    dbHandler.deleteSubtestID(subTest.getSub_test_id());
                }
                superItemList.get(group).getSubtestModel().get(child).setChecked(b);
            }
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
