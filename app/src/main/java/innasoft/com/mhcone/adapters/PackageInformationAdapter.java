package innasoft.com.mhcone.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import innasoft.com.mhcone.activities.PackageInformation;
import innasoft.com.mhcone.holders.PackageInformationHolder;
import innasoft.com.mhcone.itemclicklistener.PackageInfoItemClickListener;
import innasoft.com.mhcone.models.PackageInformationModel;

public class PackageInformationAdapter extends RecyclerView.Adapter<PackageInformationHolder>
{
    private ArrayList<PackageInformationModel> packageInformationModels;
    public PackageInformation context;
    LayoutInflater vi;
    int resource;
    private Typeface typeface;

    public PackageInformationAdapter(ArrayList<PackageInformationModel> packageInformationModels, PackageInformation context, int resource) {
        this.packageInformationModels = packageInformationModels;
        this.context = context;
        this.resource = resource;
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public PackageInformationHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = vi.inflate(resource, parent,false);
        PackageInformationHolder ssvh = new PackageInformationHolder(layoutView);
        return ssvh;
    }

    @Override
    public void onBindViewHolder(PackageInformationHolder holder, int position) {
        typeface = Typeface.createFromAsset(context.getAssets(), "robotoregular.ttf");
        holder.mPackageTv.setTypeface(typeface);
        String pack_name = packageInformationModels.get(position).getSub_test_name().substring(0, 1).toUpperCase()+ packageInformationModels.get(position).getSub_test_name().substring(1);
        holder.mPackageTv.setText(pack_name);
        holder.setItemClickListener(new PackageInfoItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.packageInformationModels.size();
    }
}
