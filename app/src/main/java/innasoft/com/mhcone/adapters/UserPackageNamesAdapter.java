package innasoft.com.mhcone.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import java.util.ArrayList;

import innasoft.com.mhcone.activities.ViewMyPackageDetails;
import innasoft.com.mhcone.activities.ViewMyPackageNames;
import innasoft.com.mhcone.filters.CustomFilterforViewMyPackage;
import innasoft.com.mhcone.holders.UserPackageNamesHolder;
import innasoft.com.mhcone.itemclicklistener.ViewMyPackagesClickListener;
import innasoft.com.mhcone.models.UserPackageNamesModel;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class UserPackageNamesAdapter extends RecyclerView.Adapter<UserPackageNamesHolder> implements Filterable {

    public ArrayList<UserPackageNamesModel> userpackageNamesModels,filterList;
    private ViewMyPackageNames context;
    LayoutInflater vi;
    int resource;
    private Typeface typeface;
    CustomFilterforViewMyPackage filter;
    UserSessionManager userSessionManager;

    public UserPackageNamesAdapter(ArrayList<UserPackageNamesModel> userpackageNamesModels, ViewMyPackageNames context, int resource) {
        this.userpackageNamesModels = userpackageNamesModels;
        this.context = context;
        this.resource = resource;
        this.filterList = userpackageNamesModels;
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        userSessionManager = new UserSessionManager(context.getApplicationContext());
    }

    @Override
    public UserPackageNamesHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(resource,parent,false);
        UserPackageNamesHolder holder=new UserPackageNamesHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(UserPackageNamesHolder holder, int position) {

        typeface = Typeface.createFromAsset(context.getAssets(), "robotoregular.ttf");

        holder.userpackageName.setTypeface(typeface);
        holder.userpackageName.setText(userpackageNamesModels.get(position).getUser_package_name());
        holder.setItemClickListener(new ViewMyPackagesClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                if(userSessionManager.checkLogin() != false)
                {
                    Toast.makeText(context.getApplicationContext(), "Please Login...!", Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent intent = new Intent(context.getApplicationContext(), ViewMyPackageDetails.class);
                    intent.putExtra("package_id", userpackageNamesModels.get(pos));
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.userpackageNamesModels.size();
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new CustomFilterforViewMyPackage(filterList,this);
        }

        return filter;
    }

}
