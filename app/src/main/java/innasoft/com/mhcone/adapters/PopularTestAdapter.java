package innasoft.com.mhcone.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.fragments.TestSubTestFragment;
import innasoft.com.mhcone.holders.PopularTestHolder;
import innasoft.com.mhcone.itemclicklistener.PopularTestItemClickListener;
import innasoft.com.mhcone.models.PopularTestModel;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PopularTestAdapter extends RecyclerView.Adapter<PopularTestHolder>{

    public ArrayList<PopularTestModel> popularTestModels;
    Fragment context;
    Activity context2;
    LayoutInflater li;
    int resource;
    Typeface typeface,typeface2;
    private boolean checkInternet;

    public PopularTestAdapter(ArrayList<PopularTestModel> popularTestModels, Fragment context, int resource) {
        this.popularTestModels = popularTestModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.robotoregular));
        typeface2 = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.robotobold));
    }

    @Override
    public PopularTestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        PopularTestHolder slh = new PopularTestHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final PopularTestHolder holder, final int position)
    {

        holder.test_name_txt.setTypeface(typeface2);
        holder.test_name_txt.setText(popularTestModels.get(position).getTest_name());

        Picasso.with(context.getActivity())
                .load(popularTestModels.get(position).getTest_image())
                .placeholder(R.drawable.logo)
                .into(holder.test_img);

        holder.setItemClickListener(new PopularTestItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.popularTestModels.size();
    }

}
