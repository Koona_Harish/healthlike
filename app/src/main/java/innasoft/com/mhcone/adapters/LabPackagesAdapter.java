package innasoft.com.mhcone.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.LabPackages;
import innasoft.com.mhcone.filters.CustomFilterforPackagesLabsFrag;
import innasoft.com.mhcone.holders.LabPackagesHolder;
import innasoft.com.mhcone.models.LabPackagesModel;

public class LabPackagesAdapter extends RecyclerView.Adapter<LabPackagesHolder> implements Filterable
{
    public ArrayList<LabPackagesModel> labPackagesModelList,filterList;
    LabPackages context;
    LayoutInflater vl;
    int resource;
    private Typeface typeface,typeface2;
    CustomFilterforPackagesLabsFrag filter;

    public LabPackagesAdapter(ArrayList<LabPackagesModel> labPackagesModelList, LabPackages context, int resource) {
        this.labPackagesModelList = labPackagesModelList;
        this.context = context;
        this.resource = resource;
        this.filterList = labPackagesModelList;
        vl = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public LabPackagesHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View layoutLabPackages = vl.inflate(resource,parent,false);
        LabPackagesHolder lph = new LabPackagesHolder(layoutLabPackages, (LabPackagesHolder.MyLabPackagesHolder) context);
        return lph;
    }

    @Override
    public void onBindViewHolder(final LabPackagesHolder holder, final int position)
    {
        typeface = Typeface.createFromAsset(context.getAssets(),"robotoregular.ttf");
        typeface2 = Typeface.createFromAsset(context.getAssets(),"robotobold.ttf");

        holder.labPackageSubTestList.setTypeface(typeface2);

        holder.labPackageName.setTypeface(typeface);
        String p_name = labPackagesModelList.get(position).getLab_packages_package_name().substring(0, 1).toUpperCase()+
                labPackagesModelList.get(position).getLab_packages_package_name().substring(1);
        holder.labPackageName.setText(p_name);

        holder.labPackageSubTestList.setTypeface(typeface);
        holder.labPackageSubTestList.setText(labPackagesModelList.get(position).getLab_packages_sub_test());

        holder.lab_packages_lab_price.setTypeface(typeface2);
        double appPriceDouble = Double.valueOf(labPackagesModelList.get(position).getLab_packages_lab_price());
        holder.lab_packages_lab_price.setText("MRP: Rs. "+new DecimalFormat("##.###").format(appPriceDouble));
        holder.lab_packages_lab_price.setPaintFlags(holder.lab_packages_lab_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        holder.lab_packages_app_price.setTypeface(typeface2);
        double labPriceDouble = Double.valueOf(labPackagesModelList.get(position).getLab_packages_final_price());
        holder.lab_packages_app_price.setText("Our Price: Rs. "+new DecimalFormat("##.###").format(labPriceDouble));

        Picasso.with(context)
                .load(labPackagesModelList.get(position).getLab_packages_package_image())
                .placeholder(R.drawable.myhealthlogo)
                .fit()
                .into(holder.clrImage );


        /* public ImageView view_test,hide_tests;
    public LinearLayout linearLayout_tests;*/
        holder.view_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.linearLayout_tests.setVisibility(View.VISIBLE);
                holder.view_test.setVisibility(View.INVISIBLE);
            }
        });
        holder.hide_tests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.linearLayout_tests.setVisibility(View.GONE);
                holder.view_test.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return this.labPackagesModelList.size();
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new CustomFilterforPackagesLabsFrag(filterList,this);
        }

        return filter;
    }
}
