package innasoft.com.mhcone.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.HabitInformation;
import innasoft.com.mhcone.activities.HabitsList;
import innasoft.com.mhcone.activities.ShowLabInformation;
import innasoft.com.mhcone.filters.CustomFilterforHabitsList;
import innasoft.com.mhcone.holders.HabitsListHolder;
import innasoft.com.mhcone.itemclicklistener.HabitsListClickListener;
import innasoft.com.mhcone.models.HabitsListModel;

public class HabitsListAdapter extends RecyclerView.Adapter<HabitsListHolder> implements Filterable
{
    public ArrayList<HabitsListModel> habitsListList,filterList;
    HabitsList context;
    LayoutInflater vl;
    int resource;
    private Typeface typeface,typeface2;
    CustomFilterforHabitsList filter;

    public HabitsListAdapter(ArrayList<HabitsListModel> habitsListList, HabitsList context, int resource) {
        this.habitsListList = habitsListList;
        this.context = context;
        this.resource = resource;
        this.filterList = habitsListList;
        vl = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public HabitsListHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {

        View v= LayoutInflater.from(parent.getContext()).inflate(resource,parent,false);
        HabitsListHolder holder=new HabitsListHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(HabitsListHolder holder, final int position)
    {
        typeface = Typeface.createFromAsset(context.getAssets(),"robotoregular.ttf");
        typeface2 = Typeface.createFromAsset(context.getAssets(),"robotobold.ttf");

        String in_name = habitsListList.get(position).getHabitsName().substring(0, 1).toUpperCase() + habitsListList.get(position).getHabitsName().substring(1);
      //  holder.habitsName.setText(Html.fromHtml(habitsListList.get(position).getHabitsName()));
        holder.habitsName.setText(in_name);
        holder.habitsName.setTypeface(typeface2);

        holder.habitsDescription.getSettings().setJavaScriptEnabled(true);
        holder.habitsDescription.loadData(habitsListList.get(position).getHabitsDescription(), "text/html", "UTF-8");
        /*holder.habitsDescription.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Intent intent = new Intent(context.getApplicationContext(), HabitInformation.class);
                intent.putExtra("habitInformationCheck","1");
                intent.putExtra("habitInfo",habitsListList.get(position));
                context.startActivity(intent);
                return false;
            }
        });*/
        holder.habits_list_category.setTypeface(typeface2);
        Picasso.with(context)
                .load(habitsListList.get(position).getHabitsImage()).placeholder(R.drawable.myhealthlogo).fit().into(holder.habitImage);

        holder.setItemClickListener(new HabitsListClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                Intent intent = new Intent(context.getApplicationContext(), HabitInformation.class);
                intent.putExtra("habitInformationCheck","1");
                intent.putExtra("habitInfo",habitsListList.get(pos));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return this.habitsListList.size();
    }

    @Override
    public Filter getFilter()
    {
        if(filter==null)
        {
            filter=new CustomFilterforHabitsList(filterList,this);
        }

        return filter;
    }
}
