package innasoft.com.mhcone.adapters;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import java.util.List;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.PackagesSubTests;
import innasoft.com.mhcone.models.MainPackageModel;
import innasoft.com.mhcone.models.MainSubPackageModel;

public class MyExpandablePackageListAdapter extends BaseExpandableListAdapter {

    PackagesSubTests context;
    public static List<MainPackageModel> superItemList;
    Typeface type_lato,typeface;

    public MyExpandablePackageListAdapter(PackagesSubTests mainActivity, List<MainPackageModel> superItemList) {

        this.context = mainActivity;
        this.superItemList = superItemList;
        type_lato = Typeface.createFromAsset(context.getAssets(), "robotoregular.ttf");
        typeface = Typeface.createFromAsset(context.getAssets(), "robotobold.ttf");

    }

    @Override
    public int getGroupCount() {
        return superItemList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        return superItemList.get(groupPosition).getSubPackageModel().size();

    }

    @Override
    public Object getGroup(int groupPosition) {
        return superItemList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return superItemList.get(groupPosition).getSubPackageModel().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        MainPackageModel tm = (MainPackageModel) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_package_parenet, null);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.txt_pack_parent);
        textView.setTypeface(typeface);
        String pack_name = tm.getPackage_sub_name().substring(0, 1).toUpperCase()+ tm.getPackage_sub_name().substring(1);
        textView.setText(pack_name);

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final MainSubPackageModel subTest = (MainSubPackageModel) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_package_child, null);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.txt_pack_child);
        textView.setTypeface(type_lato);
        String subpack_name = subTest.getSub_package_name().substring(0, 1).toUpperCase()+ subTest.getSub_package_name().substring(1);
        textView.setText(subpack_name);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
