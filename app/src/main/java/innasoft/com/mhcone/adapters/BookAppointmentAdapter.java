package innasoft.com.mhcone.adapters;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.BillingPageForLabBooking;
import innasoft.com.mhcone.activities.ShowLabInformation;
import innasoft.com.mhcone.filters.CustomFilterforFourthFrag;
import innasoft.com.mhcone.fragments.FourthFrag;
import innasoft.com.mhcone.holders.BookAppointmentHolder;
import innasoft.com.mhcone.itemclicklistener.BookLabClickListener;
import innasoft.com.mhcone.models.BookAppointmentModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class BookAppointmentAdapter extends RecyclerView.Adapter<BookAppointmentHolder> implements Filterable {
    public ArrayList<BookAppointmentModel> bookAppointmentList, filterList;
    LayoutInflater bookLabLI;
    FourthFrag context;
    LayoutInflater vl;
    private Typeface type_lato, type_lato2;
    int resource;
    CustomFilterforFourthFrag filter;
    UserSessionManager userSessionManager;
    String userID;
    ProgressDialog progressDialog;
    NetworkStatus ns;
    Boolean isOnline = false;

    public BookAppointmentAdapter(ArrayList<BookAppointmentModel> bookAppointmentList, FourthFrag context, int resource) {
        this.bookAppointmentList = bookAppointmentList;
        this.context = context;
        this.resource = resource;
        this.filterList = bookAppointmentList;
        vl = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        userSessionManager = new UserSessionManager(context.getActivity());
        HashMap<String, String> user = userSessionManager.getUserDetails();

        userID = user.get(UserSessionManager.USER_ID);

        progressDialog = new ProgressDialog(context.getActivity());
        progressDialog.setMessage("Please wait......");

        progressDialog.setProgressStyle(R.style.DialogTheme);
    }


    @Override
    public BookAppointmentHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        BookAppointmentHolder holder = new BookAppointmentHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(BookAppointmentHolder holder, final int position) {
        type_lato = Typeface.createFromAsset(context.getActivity().getAssets(), "robotoregular.ttf");
        type_lato2 = Typeface.createFromAsset(context.getActivity().getAssets(), "robotobold.ttf");
        holder.show_book_tittle.setTypeface(type_lato2);
        holder.show_book_list_name_txt.setTypeface(type_lato2);
        holder.show_book_list_name_txt.setText(bookAppointmentList.get(position).getShow_lab_book_user_name());
        holder.show_book_list_address_txt.setTypeface(type_lato);
        holder.show_book_list_address_txt.setText(bookAppointmentList.get(position).getLocation());

        float rattingValue = Float.valueOf(bookAppointmentList.get(position).getShow_lab_ratting());
        holder.ratingBar_book_appointment.setRating(rattingValue);

        if (bookAppointmentList.get(position).getCertificate().equals("1")) {
            holder.certified_book_appointment.setTypeface(type_lato);
            holder.certified_book_appointment.setText("NABL/CAP Accreditation");
        } else {
            holder.certified_book_appointment.setTypeface(type_lato);
            holder.certified_book_appointment.setText("");
        }
        holder.show_book_list_workingdays_txt.setTypeface(type_lato);
        if (bookAppointmentList.get(position).getProfile_pic_url().equals("null")) {
            Picasso.with(context.getActivity())
                    .load(R.drawable.lab_profile_pic_default)
                    .placeholder(R.drawable.lab_profile_pic_default)
                    .fit()
                    .into(holder.prifileImage);
        } else {


            Picasso.with(context.getActivity())
                    .load(AppUrls.BASE_URL + bookAppointmentList.get(position).getProfile_pic_url())
                    .placeholder(R.drawable.lab_profile_pic_default)
                    .fit()
                    .into(holder.prifileImage);
        }

        holder.show_book_list_workingdays_txt.setText(bookAppointmentList.get(position).getShow_lab_book_workingdays());
        holder.show_book_list_distance_txt.setTypeface(type_lato);

        double distance = Double.valueOf(bookAppointmentList.get(position).getShow_lab_book_distance());
        holder.show_book_list_distance_txt.setText("~" + new DecimalFormat("##.###").format(distance) + " KM");

        String homeCollection = bookAppointmentList.get(position).getShow_lab_book_home_collections();

        if (homeCollection.equals("1")) {
            holder.show_book_list_homecollection_txt.setTypeface(type_lato);
            holder.book_checktickmark.setImageResource(R.drawable.right_mark_new);

        } else if (homeCollection.equals("0")) {
            holder.show_book_list_homecollection_txt.setTypeface(type_lato);
            holder.book_checktickmark.setImageResource(R.drawable.access_denied);

        }

        holder.btnBookList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String LabId = bookAppointmentList.get(position).getShow_lab_book_id();

                Intent intent = new Intent(context.getActivity(), ShowLabInformation.class);
                intent.putExtra("toLat", bookAppointmentList.get(position).getShow_lab_book_lat());
                intent.putExtra("tolng", bookAppointmentList.get(position).getShow_lab_book_lang());
                intent.putExtra("LabID", LabId);
                context.startActivity(intent);

            }
        });
        holder.setItemClickListener(new BookLabClickListener() {
            @Override
            public void onItemClick(View v, final int pos) {

                final int poss = pos;
                ns = new NetworkStatus();
                isOnline = ns.isOnline(context.getActivity());

                if (isOnline) {

                    if (userSessionManager.checkLogin() != false) {
                        Toast.makeText(context.getActivity(), "Please Login...!", Toast.LENGTH_SHORT).show();
                    } else {
                        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                progressDialog.dismiss();
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String responceCode = jsonObject.getString("status");
                                    if (responceCode.equals("19999")) {
                                        Intent intent = new Intent(context.getActivity(), BillingPageForLabBooking.class);
                                        intent.putExtra("selectedLabPhone", bookAppointmentList.get(poss).getShow_lab_book_mobile());
                                        intent.putExtra("selectedLabId", bookAppointmentList.get(poss).getShow_lab_book_id());//selectedLabId,selectedLabName
                                        intent.putExtra("selectedLabName", bookAppointmentList.get(poss).getShow_lab_book_user_name());
                                        intent.putExtra("payment_lab", bookAppointmentList.get(poss).getShow_lab_payment_lab());
                                        intent.putExtra("payment_online", bookAppointmentList.get(poss).getShow_lab_payment_online());
                                        intent.putExtra("tele_booking", bookAppointmentList.get(poss).getShow_lab_tele_booking());
                                        context.startActivity(intent);
                                    }
                                    if (responceCode.equals("10140")) {
                                        userSessionManager.logoutUser();
                                        Toast.makeText(context.getActivity(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(context.getActivity(), MainActivity.class);
                                        context.startActivity(intent);
                                    }

                                    if (responceCode.equals("10150")) {
                                        userSessionManager.logoutUser();
                                        Toast.makeText(context.getActivity(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(context.getActivity(), MainActivity.class);
                                        context.startActivity(intent);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                progressDialog.dismiss();
                                String responseBody = null;

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("user_id", userID);
                                return params;
                            }
                        };
                        RequestQueue requestQueue = Volley.newRequestQueue(context.getActivity());
                        requestQueue.add(stringRequest);
                    }
                } else {
                    Toast.makeText(context.getActivity(), "Please Check your Internet connection..!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.bookAppointmentList.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterforFourthFrag(filterList, this);
        }

        return filter;
    }
}
