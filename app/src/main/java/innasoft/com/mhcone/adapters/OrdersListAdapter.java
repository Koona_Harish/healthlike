package innasoft.com.mhcone.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.OrdersList;
import innasoft.com.mhcone.holders.OrdersListHolder;
import innasoft.com.mhcone.models.OrdersListModel;

public class OrdersListAdapter extends RecyclerView.Adapter<OrdersListHolder> {

    private ArrayList<OrdersListModel> ordersListModelList;
    OrdersList context;
    LayoutInflater vl;
    int resource;
    private Typeface typeface,typeface2;

    public OrdersListAdapter(ArrayList<OrdersListModel> ordersListModelList, OrdersList context, int resource) {
        this.ordersListModelList = ordersListModelList;
        this.context = context;
        this.resource = resource;
        vl = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public OrdersListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutLabPackages = vl.inflate(resource,parent,false);
        OrdersListHolder olh = new OrdersListHolder(layoutLabPackages, (OrdersListHolder.MyOrdersListHolder) context);
        return olh;
    }

    @Override
    public void onBindViewHolder(OrdersListHolder holder, int position)
    {
        typeface = Typeface.createFromAsset(context.getAssets(),"robotoregular.ttf");
        typeface2 = Typeface.createFromAsset(context.getAssets(),"robotobold.ttf");

        holder.orderslistId.setTypeface(typeface2);
        holder.orderslistBookingDate.setTypeface(typeface2);
        holder.orderlistPrice.setTypeface(typeface2);
        holder.pay_through_value.setTypeface(typeface2);

        holder.ordersListId.setTypeface(typeface);
        holder.ordersListId.setText(ordersListModelList.get(position).getOrders_list_id());
        holder.pay_through_value.setText(ordersListModelList.get(position).getPayment_type());
        holder.orderStatus.setTypeface(typeface2);
        holder.booked_date.setTypeface(typeface2);


        holder.ordersListLabName.setTypeface(typeface2);
        if (ordersListModelList.get(position).getOrders_list_lab_name().equals("null") || ordersListModelList.get(position).getOrders_list_lab_name().equals("")){
            holder.ordersListLabName.setText("");
        }else {
            holder.ordersListLabName.setText(ordersListModelList.get(position).getOrders_list_lab_name());
        }


        holder.ordersListBookingDate.setTypeface(typeface);
        holder.ordersListBookingDate.setText(ordersListModelList.get(position).getOrders_list_bookingdate());


        holder.orderListPrice.setTypeface(typeface);
        holder.orderListPrice.setText(ordersListModelList.get(position).getOrder_list_price());


        if((ordersListModelList.get(position).getOrders_list_status()).equals("0")) {
            holder.ordersListStatus.setTypeface(typeface2);
            holder.ordersListStatus.setTextColor(context.getResources().getColor(R.color.orange_color));
            holder.ordersListStatus.setText("Inprocess");
        }
        if((ordersListModelList.get(position).getOrders_list_status()).equals("1")) {
            holder.ordersListStatus.setTypeface(typeface2);
            holder.ordersListStatus.setTextColor(context.getResources().getColor(R.color.solid_red));
            holder.ordersListStatus.setText("canceled");
        }

        if((ordersListModelList.get(position).getOrders_list_status()).equals("2"))
        {
            holder.ordersListStatus.setTypeface(typeface2);
            holder.ordersListStatus.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.ordersListStatus.setText("Confirmed");
        }
        if((ordersListModelList.get(position).getOrders_list_status()).equals("3"))
        {
            holder.ordersListStatus.setTypeface(typeface2);
            holder.ordersListStatus.setTextColor(context.getResources().getColor(R.color.solid_red));
            holder.ordersListStatus.setText("Rejected");
        }
        if((ordersListModelList.get(position).getOrders_list_status()).equals("4"))
        {
            holder.ordersListStatus.setTypeface(typeface2);
            holder.ordersListStatus.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.ordersListStatus.setText("Completed");
        }

        holder.ordersListBookedDate.setTypeface(typeface);
        holder.ordersListBookedDate.setText(ordersListModelList.get(position).getOrders_list_bookeddate());

    }

    @Override
    public int getItemCount() {
        return this.ordersListModelList.size();
    }
}
