package innasoft.com.mhcone.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.LabPackages;
import innasoft.com.mhcone.activities.ShowLabInformation;
import innasoft.com.mhcone.filters.CustomFilterforLabsFragment;
import innasoft.com.mhcone.fragments.LabsFragments;
import innasoft.com.mhcone.holders.ShowLabsListHolder;
import innasoft.com.mhcone.itemclicklistener.LabsPackagesClickListener;
import innasoft.com.mhcone.models.ShowLabsListModel;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class ShowLabsListAdapter extends RecyclerView.Adapter<ShowLabsListHolder> implements Filterable {

    public ArrayList<ShowLabsListModel> showLabsListList, filterList;
    LabsFragments context;
    LayoutInflater vl;
    private Typeface type_lato, type_lato2;
    int resource;
    UserSessionManager userSessionManager;
    CustomFilterforLabsFragment filter;
    NetworkStatus ns;
    Boolean isOnline = false;

    public ShowLabsListAdapter(ArrayList<ShowLabsListModel> showLabsListList, LabsFragments context, int resource) {
        this.showLabsListList = showLabsListList;
        this.context = context;
        this.resource = resource;
        this.filterList = showLabsListList;
        vl = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        userSessionManager = new UserSessionManager(context.getActivity());
    }

    @Override
    public ShowLabsListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        ShowLabsListHolder holder = new ShowLabsListHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ShowLabsListHolder holder, final int position) {
        type_lato = Typeface.createFromAsset(context.getActivity().getAssets(), "robotoregular.ttf");
        type_lato2 = Typeface.createFromAsset(context.getActivity().getAssets(), "robotobold.ttf");

        holder.show_lab_tittle.setTypeface(type_lato);
        holder.show_lab_list_name_txt.setTypeface(type_lato2);
        String lab_name = showLabsListList.get(position).getShow_lab_list_user_name().substring(0, 1).toUpperCase() + showLabsListList.get(position).getShow_lab_list_user_name().substring(1);
        holder.show_lab_list_name_txt.setText(lab_name);

        if (showLabsListList.get(position).getCertificate().equals("1")) {
            holder.certified_labs_list.setTypeface(type_lato);
            holder.certified_labs_list.setText("NABL/CAP Accreditation");
        } else {
            holder.certified_labs_list.setTypeface(type_lato);
            holder.certified_labs_list.setText("");
        }

        holder.show_lab_list_address_txt.setTypeface(type_lato);
        holder.show_lab_list_address_txt.setText(showLabsListList.get(position).getLocation());
        holder.show_lab_list_workingdays_txt.setTypeface(type_lato);

        if (showLabsListList.get(position).getProfile_pic_url().equals("null") || showLabsListList.get(position).getProfile_pic_url().equals("")) {
            Picasso.with(context.getActivity())
                    .load(R.drawable.lab_profile_pic_default)
                    .placeholder(R.drawable.lab_profile_pic_default)
                    .fit()
                    .into(holder.lab_list_pic);
        } else {
            Picasso.with(context.getActivity())
                    .load(showLabsListList.get(position).getProfile_pic_url())
                    .placeholder(R.drawable.lab_profile_pic_default)
                    .fit()
                    .into(holder.lab_list_pic);
        }

        float rattingValue = Float.valueOf(showLabsListList.get(position).getShow_lab_ratting());

        holder.ratingBar_lab_list.setRating(rattingValue);
        holder.show_lab_list_workingdays_txt.setText(showLabsListList.get(position).getShow_lab_list_workingtime());

        holder.show_lab_list_distance_txt.setTypeface(type_lato);

        double distance = Double.valueOf(showLabsListList.get(position).getShow_lab_list_distance());
        holder.show_lab_list_distance_txt.setText("~" + new DecimalFormat("##.###").format(distance) + " KM");

        String homeCollection = showLabsListList.get(position).getShow_lab_list_home_collections();
        holder.show_lab_list_homecollection_txt.setTypeface(type_lato);
        if (homeCollection.equals("1")) {

            holder.labs_checktickmark.setImageResource(R.drawable.right_mark_new);

        } else if (homeCollection.equals("0")) {

            holder.labs_checktickmark.setImageResource(R.drawable.access_denied);

        }

        holder.btnLabList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String LabId = showLabsListList.get(position).getShow_lab_list_id();

                Intent intent = new Intent(context.getActivity(), ShowLabInformation.class);
                intent.putExtra("toLat", showLabsListList.get(position).getShow_lab_list_lat());
                intent.putExtra("tolng", showLabsListList.get(position).getShow_lab_list_lang());
                intent.putExtra("LabID", LabId);
                context.startActivity(intent);

            }
        });

        holder.setItemClickListener(new LabsPackagesClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                ns = new NetworkStatus();
                isOnline = ns.isOnline(context.getActivity());

                if (isOnline) {
//               if(userSessionManager.checkLogin() != false)
//               {
//                   Toast.makeText(context.getActivity(), "Please Login...!", Toast.LENGTH_SHORT).show();
//               }
//               else {
                    String lab_id = showLabsListList.get(pos).getShow_lab_list_id();
                    String lab_name = showLabsListList.get(pos).getShow_lab_list_user_name();

                    Intent intent = new Intent(context.getActivity(), LabPackages.class);
                    intent.putExtra("labId", lab_id);
                    intent.putExtra("labName", lab_name);
                    intent.putExtra("payment_lab", showLabsListList.get(pos).getShow_lab_payment_lab());
                    intent.putExtra("payment_online", showLabsListList.get(pos).getShow_lab_payment_online());
                    intent.putExtra("tele_booking", showLabsListList.get(pos).getShow_lab_tele_booking());
                    intent.putExtra("labPhone", showLabsListList.get(pos).getShow_lab_list_mobile());
                    context.startActivity(intent);
//               }
                } else {
                    Toast.makeText(context.getActivity(), "Please Check your Internet connection..!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {

        return this.showLabsListList.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterforLabsFragment(filterList, this);
        }

        return filter;
    }
}
