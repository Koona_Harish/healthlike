package innasoft.com.mhcone.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.EditSubUser;
import innasoft.com.mhcone.activities.ViewUsers;
import innasoft.com.mhcone.holders.ViewUserHolder;
import innasoft.com.mhcone.models.ViewUsersModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.NetworkUtil;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class ViewUsersAdapter extends RecyclerView.Adapter<ViewUserHolder>
{
    private ArrayList<ViewUsersModel> viewUsersModels;
    private ViewUsers context;
    LayoutInflater li;
    int resource;
    String subuserId,nameTest,userID;
    Typeface typeface,typeface2;
    UserSessionManager userSessionManager;
    private boolean statusFlag;

    public ViewUsersAdapter(ArrayList<ViewUsersModel> viewUsersModels, ViewUsers context, int resource) {
        this.viewUsersModels = viewUsersModels;
        this.context = context;
        this.resource = resource;
        typeface = Typeface.createFromAsset(context.getAssets(),"robotoregular.ttf");
        typeface2 = Typeface.createFromAsset(context.getAssets(),"robotobold.ttf");
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        userSessionManager = new UserSessionManager(context);
    }

    @Override
    public ViewUserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = li.inflate(resource, parent,false);
        ViewUserHolder vuh = new ViewUserHolder(layoutView, (ViewUserHolder.MyViewHolder) context);
        return vuh;
    }

    @Override
    public void onBindViewHolder(final ViewUserHolder holder, final int position) {

        holder.h_subuserName.setTypeface(typeface2);
        holder.h_subuserRelation.setTypeface(typeface2);
        holder.h_Gender.setTypeface(typeface2);
        holder.h_dob.setTypeface(typeface2);
        holder.h_Email.setTypeface(typeface2);
        holder.h_Phone.setTypeface(typeface2);
        holder.tv_subuserName.setTypeface(typeface);
        holder.tv_subuserName.setText(viewUsersModels.get(position).getVu_user_sub_name());
        holder.tv_subuserRelation.setTypeface(typeface);
        holder.tv_subuserRelation.setText(viewUsersModels.get(position).getVu_relation());
        holder.tv_Gender.setTypeface(typeface);
        holder.tv_Gender.setText(viewUsersModels.get(position).getVu_gender());
        holder.tv_dob.setTypeface(typeface);
        holder.tv_Email.setTypeface(typeface);
        holder.tv_Phone.setTypeface(typeface);
        holder.tv_dob.setText(viewUsersModels.get(position).getVu_age());
        holder.tv_Email.setText(viewUsersModels.get(position).getVu_email());

        String phone_number = viewUsersModels.get(position).getVu_mobile();

        if(phone_number.equals("0") || phone_number.equals(null)) {
            holder.tv_Phone.setText("");
            holder.h_Phone.setVisibility(View.GONE);
        }else {
            holder.h_Phone.setVisibility(View.VISIBLE);
            holder.tv_Phone.setText(phone_number);
        }

        String email = viewUsersModels.get(position).getVu_email();

        if(email.equals("") || email.equals(null)) {
            holder.tv_Email.setText("");
            holder.h_Email.setVisibility(View.GONE);
        }else {
            holder.h_Email.setVisibility(View.VISIBLE);
            holder.tv_Email.setText(email);
        }

        holder.viewUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                holder.lenearLayoutLL.setVisibility(View.VISIBLE);
                holder.downLL.setVisibility(View.GONE);
                holder.upLL.setVisibility(View.VISIBLE);
                holder.lled_dl_set_one.setVisibility(View.GONE);
                holder.lled_dl_set_two.setVisibility(View.VISIBLE);
            }
        });

        holder.downLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.lenearLayoutLL.setVisibility(View.VISIBLE);
                holder.downLL.setVisibility(View.GONE);
                holder.upLL.setVisibility(View.VISIBLE);
                holder.lled_dl_set_one.setVisibility(View.GONE);
                holder.lled_dl_set_two.setVisibility(View.VISIBLE);
            }
        });

        holder.close_view_sub_user.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                holder.lenearLayoutLL.setVisibility(View.GONE);
                holder.upLL.setVisibility(View.GONE);
                holder.downLL.setVisibility(View.VISIBLE);
                holder.lled_dl_set_one.setVisibility(View.VISIBLE);
                holder.lled_dl_set_two.setVisibility(View.GONE);
            }
        });

        holder.upLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.lenearLayoutLL.setVisibility(View.GONE);
                holder.upLL.setVisibility(View.GONE);
                holder.downLL.setVisibility(View.VISIBLE);
                holder.lled_dl_set_one.setVisibility(View.VISIBLE);
                holder.lled_dl_set_two.setVisibility(View.GONE);
            }
        });

        holder.deleteUserButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                subuserId = viewUsersModels.get(position).getVu_id();
                userID = viewUsersModels.get(position).getVu_user_id();

                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                builder.setTitle("Delete");
                builder.setMessage("Are you sure?");
                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        statusFlag = NetworkUtil.isConnected(context);
                        if (statusFlag) {

                            deleteItem(subuserId, userID);
                        }else {
                            showInternetStatus();
                        }
                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        holder.deleteUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                subuserId = viewUsersModels.get(position).getVu_id();
                userID = viewUsersModels.get(position).getVu_user_id();

                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                builder.setTitle("Delete");
                builder.setMessage("Are you sure?");

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        statusFlag = NetworkUtil.isConnected(context);
                        if (statusFlag) {
                        deleteItem(subuserId,userID);
                        }else {
                            showInternetStatus();
                        }
                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        holder.editUserButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String userSubId = viewUsersModels.get(position).getVu_id();
                String userId = viewUsersModels.get(position).getVu_user_id();
                String userName = viewUsersModels.get(position).getVu_user_sub_name();
                String userAge = viewUsersModels.get(position).getVu_age();
                String userGender = viewUsersModels.get(position).getVu_gender();
                String userRelation = viewUsersModels.get(position).getVu_relation();
                String userEmail = viewUsersModels.get(position).getVu_email();
                String userMobile = viewUsersModels.get(position).getVu_mobile();

                Intent intent = new Intent(context , EditSubUser.class);

                intent.putExtra("subid",userSubId);
                intent.putExtra("id",userId);
                intent.putExtra("name",userName);
                intent.putExtra("age",userAge);
                intent.putExtra("gender",userGender);
                intent.putExtra("relation",userRelation);
                intent.putExtra("email",userEmail);
                intent.putExtra("mobile",userMobile);
                context.startActivity(intent);
            }
        });
        holder.editUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String userSubId = viewUsersModels.get(position).getVu_id();
                String userId = viewUsersModels.get(position).getVu_user_id();
                String userName = viewUsersModels.get(position).getVu_user_sub_name();
                String userAge = viewUsersModels.get(position).getVu_age();
                String userGender = viewUsersModels.get(position).getVu_gender();
                String userRelation = viewUsersModels.get(position).getVu_relation();
                String userEmail = viewUsersModels.get(position).getVu_email();
                String userMobile = viewUsersModels.get(position).getVu_mobile();

                Intent intent = new Intent(context , EditSubUser.class);

                intent.putExtra("subid",userSubId);
                intent.putExtra("id",userId);
                intent.putExtra("name",userName);
                intent.putExtra("age",userAge);
                intent.putExtra("gender",userGender);
                intent.putExtra("relation",userRelation);
                intent.putExtra("email",userEmail);
                intent.putExtra("mobile",userMobile);
                context.startActivity(intent);
            }
        });
    }

    private void deleteItem(final String subuserId, final String userID) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.DELETE_USER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String responceCode = jsonObject.getString("status");

                            if (responceCode.equals("10100")) {
                                context.refreshMyUploadList();
                            }
                            if (responceCode.equals("10140")) {

                                userSessionManager.logoutUser();
                                Toast.makeText(context, "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(context, MainActivity.class);
                                context.startActivity(intent);
                            }

                            if (responceCode.equals("10150")) {

                                userSessionManager.logoutUser();
                                Toast.makeText(context, "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(context, MainActivity.class);
                                context.startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("sub_user_id", subuserId);
                params.put("user_id", userID);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    private void showInternetStatus()
    {

        LayoutInflater inflater = context.getLayoutInflater();
        View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) context.findViewById(R.id.custom_toast_layout));
        TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
        textView.setTypeface(typeface);
        Toast toast = new Toast(context);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(toastLayout);
        toast.show();

    }

    @Override
    public int getItemCount() {
        return this.viewUsersModels.size();
    }
}
