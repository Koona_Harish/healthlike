package innasoft.com.mhcone.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;

import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.HabitInformation;
import innasoft.com.mhcone.holders.RelatedHabitsListHolder;
import innasoft.com.mhcone.itemclicklistener.RelatedHabitsListClickListener;
import innasoft.com.mhcone.models.RelatedHabitsListModel;

public class RelatedHabitsListAdapter extends RecyclerView.Adapter<RelatedHabitsListHolder>
{
    public ArrayList<RelatedHabitsListModel> relatedhabitsListList;
    HabitInformation context;
    LayoutInflater vl;
    int resource;
    private Typeface typeface,typeface2;

    public RelatedHabitsListAdapter(ArrayList<RelatedHabitsListModel> relatedhabitsListList, HabitInformation context, int resource) {
        this.relatedhabitsListList = relatedhabitsListList;
        this.context = context;
        this.resource = resource;

        vl = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RelatedHabitsListHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v= LayoutInflater.from(parent.getContext()).inflate(resource,parent,false);
        RelatedHabitsListHolder holder=new RelatedHabitsListHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RelatedHabitsListHolder holder, final int position)
    {

        typeface = Typeface.createFromAsset(context.getAssets(),"robotoregular.ttf");
        typeface2 = Typeface.createFromAsset(context.getAssets(),"robotobold.ttf");


        String in_name = relatedhabitsListList.get(position).getRelatedhabitsName().substring(0, 1).toUpperCase() + relatedhabitsListList.get(position).getRelatedhabitsName().substring(1);
        holder.relatedhabitsName.setText(in_name);
        //holder.relatedhabitsName.setText(Html.fromHtml(relatedhabitsListList.get(position).getRelatedhabitsName()));

        holder.relatedhabitsName.setTypeface(typeface2);
       WebSettings webSettings = holder.relatedhabitsDescription.getSettings();
       webSettings.setDefaultFontSize(8);
       holder.relatedhabitsDescription.getSettings().setJavaScriptEnabled(true);
        holder.relatedhabitsDescription.loadData(relatedhabitsListList.get(position).getRelatedhabitsDescription(), "text/html", "UTF-8");

        Picasso.with(context)
                .load(relatedhabitsListList.get(position).getRelatedhabitsImage()).placeholder(R.drawable.myhealthlogo).fit().into(holder.relatedhabitImage);

        holder.setItemClickListener(new RelatedHabitsListClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

                Intent intent = new Intent(context.getApplicationContext(), HabitInformation.class);
                intent.putExtra("habitInformationCheck","2");
                intent.putExtra("habitInfo",relatedhabitsListList.get(pos));
                context.startActivity(intent);
                context.finish();
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return this.relatedhabitsListList.size();
    }


}
