package innasoft.com.mhcone.adapters;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.activities.PackageInformation;
import innasoft.com.mhcone.activities.ViewMyPackage;
import innasoft.com.mhcone.holders.ViewMyPackageHolder;
import innasoft.com.mhcone.itemclicklistener.ViewMyPackageItemClickListener;
import innasoft.com.mhcone.models.ViewMyPackageModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class ViewMyPackageAdapter extends RecyclerView.Adapter<ViewMyPackageHolder>
{
    public ArrayList<ViewMyPackageModel> viewMyPackageModel;
    private ViewMyPackage context;
    LayoutInflater vi;
    int resource;
    private Typeface type_lato;
    UserSessionManager userSessionManager;
    String p_name;

    public ViewMyPackageAdapter(int resource, ArrayList<ViewMyPackageModel> viewMyPackageModel, ViewMyPackage context) {
        this.viewMyPackageModel = viewMyPackageModel;
        this.context = context;
        this.resource = resource;
        userSessionManager = new UserSessionManager(context);
    }

    @Override
    public ViewMyPackageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        ViewMyPackageHolder newsHolder = new ViewMyPackageHolder(view, viewMyPackageModel, context);
        return newsHolder;
    }

    @Override
    public void onBindViewHolder(ViewMyPackageHolder holder, final int position) {

        type_lato = Typeface.createFromAsset(context.getAssets(), "robotoregular.ttf");

        holder.mPackageTv.setTypeface(type_lato);
        p_name = viewMyPackageModel.get(position).package_name.substring(0, 1).toUpperCase()+ viewMyPackageModel.get(position).package_name.substring(1);
        holder.mPackageTv.setText(p_name);

        holder.setItemClickListener(new ViewMyPackageItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                Intent intent = new Intent(context.getApplicationContext(), PackageInformation.class);
                intent.putExtra("package_name", viewMyPackageModel.get(pos).package_name);
                //intent.putExtra("package_name", p_name);
                intent.putExtra("user_package_id",viewMyPackageModel.get(pos).user_package_id);
                context.startActivity(intent);
                context.finish();
            }
        });
    }

    private void deleteSelectetedPackage(final String packageId) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.DELETE_MYPACKAGE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                        JSONObject jsonObject =new JSONObject(response);
                        String responceCode = jsonObject.getString("status");
                        if(responceCode.equals("20100")) {

                            context.refreshMyPackageList();
                        }
                            if(responceCode.equals("10140"))
                            {
                                userSessionManager.logoutUser();
                                Toast.makeText(context, "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(context, MainActivity.class);
                                context.startActivity(intent);
                            }

                            if(responceCode.equals("10150"))
                            {
                                userSessionManager.logoutUser();
                                Toast.makeText(context, "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(context, MainActivity.class);
                                context.startActivity(intent);
                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("package_id", packageId);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public int getItemCount() {
        return this.viewMyPackageModel.size();
    }

}
