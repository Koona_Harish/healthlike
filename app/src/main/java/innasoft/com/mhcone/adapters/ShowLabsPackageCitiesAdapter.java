package innasoft.com.mhcone.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

import innasoft.com.mhcone.activities.ShowLabsPackage;
import innasoft.com.mhcone.filters.LabsFragmentCustomFilterForCities;
import innasoft.com.mhcone.filters.ShowLabsPackageCustomFilterForCities;
import innasoft.com.mhcone.fragments.LabsFragments;
import innasoft.com.mhcone.holders.CitiesHolder;
import innasoft.com.mhcone.itemclicklistener.CitiesClickListener;
import innasoft.com.mhcone.models.CitiesModel;

public class ShowLabsPackageCitiesAdapter extends RecyclerView.Adapter<CitiesHolder>implements Filterable
{
    public ArrayList<CitiesModel> citiesModelArrayList,filterList;
    public ShowLabsPackage context;
    ShowLabsPackageCustomFilterForCities filter;
    LayoutInflater li;
    int resource;
    Typeface type_lato;

    public ShowLabsPackageCitiesAdapter(ArrayList<CitiesModel> citiesModelArrayList, ShowLabsPackage context, int resource) {
        this.citiesModelArrayList = citiesModelArrayList;
        this.filterList = citiesModelArrayList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new ShowLabsPackageCustomFilterForCities(filterList,this);
        }

        return filter;
    }

    @Override
    public CitiesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutAerator = li.inflate(resource,parent,false);
        CitiesHolder ah = new CitiesHolder(layoutAerator);
        return ah;
    }

    @Override
    public void onBindViewHolder(CitiesHolder holder, int position) {

        type_lato = Typeface.createFromAsset(context.getAssets(), "robotoregular.ttf");

        holder.city_name_tv.setTypeface(type_lato);
        holder.city_name_tv.setText(citiesModelArrayList.get(position).city_name);

        holder.setItemClickListener(new CitiesClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

                context.refreshMyList(citiesModelArrayList.get(pos).getCity_name(), citiesModelArrayList.get(pos).lat, citiesModelArrayList.get(pos).lang);

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.citiesModelArrayList.size();
    }
}
