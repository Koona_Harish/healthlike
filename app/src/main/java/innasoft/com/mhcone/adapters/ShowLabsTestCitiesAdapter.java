package innasoft.com.mhcone.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

import innasoft.com.mhcone.activities.ShowLabsPackage;
import innasoft.com.mhcone.activities.ShowLabsTest;
import innasoft.com.mhcone.filters.ShowLabsPackageCustomFilterForCities;
import innasoft.com.mhcone.filters.ShowLabsTestCustomFilterForCities;
import innasoft.com.mhcone.holders.CitiesHolder;
import innasoft.com.mhcone.itemclicklistener.CitiesClickListener;
import innasoft.com.mhcone.models.CitiesModel;

/**
 * Created by purushotham on 27/1/17.
 */

public class ShowLabsTestCitiesAdapter extends RecyclerView.Adapter<CitiesHolder>implements Filterable
{
    public ArrayList<CitiesModel> citiesModelArrayList,filterList;
    public ShowLabsTest context;
    ShowLabsTestCustomFilterForCities filter;
    LayoutInflater li;
    int resource;
    Typeface type_lato;



    public ShowLabsTestCitiesAdapter(ArrayList<CitiesModel> citiesModelArrayList, ShowLabsTest context, int resource) {
        this.citiesModelArrayList = citiesModelArrayList;
        this.filterList = citiesModelArrayList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new ShowLabsTestCustomFilterForCities(filterList,this);
        }

        return filter;
    }

    @Override
    public CitiesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutAerator = li.inflate(resource,parent,false);
        CitiesHolder ah = new CitiesHolder(layoutAerator);
        return ah;
    }

    @Override
    public void onBindViewHolder(CitiesHolder holder, int position) {
        type_lato = Typeface.createFromAsset(context.getAssets(), "robotoregular.ttf");
        holder.city_name_tv.setTypeface(type_lato);
        holder.city_name_tv.setText(citiesModelArrayList.get(position).city_name);
        holder.setItemClickListener(new CitiesClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
               // Toast.makeText(context.getActivity(), ""+citiesModelArrayList.get(pos).getCity_name(), Toast.LENGTH_SHORT).show();
                //refreshMyList
                context.refreshMyList(citiesModelArrayList.get(pos).getCity_name(), citiesModelArrayList.get(pos).lat, citiesModelArrayList.get(pos).lang );

            }
        });

    }

    @Override
    public int getItemCount() {
        return this.citiesModelArrayList.size();
    }
}
