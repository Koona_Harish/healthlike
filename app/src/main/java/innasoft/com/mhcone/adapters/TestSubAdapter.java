package innasoft.com.mhcone.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashSet;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.filters.CustomFilterforTestsubtest;
import innasoft.com.mhcone.fragments.TestSubTestFragment;
import innasoft.com.mhcone.holders.TestSubHolder;
import innasoft.com.mhcone.itemclicklistener.PackageItemClickListener;
import innasoft.com.mhcone.models.NewTestsModel;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class TestSubAdapter extends RecyclerView.Adapter<TestSubHolder> implements Filterable {

    public ArrayList<NewTestsModel> packagesModels,filterList;
    private TestSubTestFragment context;
    LayoutInflater vi;
    int resource;
    private Typeface typeface,typeface2;
    UserSessionManager userSessionManager;
    NetworkStatus ns;
    String packg_name,pack_id;
    Boolean isOnline = false;
    CustomFilterforTestsubtest  filter;
    String pack_name;
    ArrayList<String> gm_ids = new ArrayList<String>();
    ArrayList<String> gm_names = new ArrayList<String>();
    ArrayList<String> gm_sub_ids = new ArrayList<String>();
    public TestSubAdapter(ArrayList<NewTestsModel> packagesModels, TestSubTestFragment context, int resource) {
        this.packagesModels = packagesModels;
        this.context = context;
        this.resource = resource;
        this.filterList = packagesModels;
        vi = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        userSessionManager = new UserSessionManager(context.getActivity());
    }

    @Override
    public TestSubHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(resource,parent,false);
        TestSubHolder holder=new TestSubHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final TestSubHolder holder, final int position) {

        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), "robotoregular.ttf");
        typeface2 = Typeface.createFromAsset(context.getActivity().getAssets(), "robotobold.ttf");

        holder.packageName.setTypeface(typeface2);
        pack_name = packagesModels.get(position).getTest_sub_name().substring(0, 1).toUpperCase()+ packagesModels.get(position).getTest_sub_name().substring(1);
        holder.packageName.setText(pack_name);

        Log.d("PATHHH",packagesModels.get(position).getTest_image());
        Picasso.with(context.getActivity())
                .load(packagesModels.get(position).getTest_image())
                .placeholder(R.drawable.myhealthlogo)
                .fit()
                .into(holder.package_pic);

        pack_id=  packagesModels.get(position).getTest_id();
        /*holder.packageName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int pos = position;
                if (isChecked) {
                    gm_ids.add(packagesModels.get(pos).getTest_id());
                    HashSet hs = new HashSet();
                    hs.addAll(gm_ids);
                    gm_ids.clear();
                    gm_ids.addAll(hs);

                    gm_names.add(packagesModels.get(pos).getTest_sub_name());
                    HashSet hs1 = new HashSet();
                    hs1.addAll(gm_names);
                    gm_names.clear();
                    gm_names.addAll(hs1);


                    context.setGmName(gm_ids, gm_names);
                    packagesModels.get(pos).setIschecked(true);
                    holder.packageName.isChecked();

                } else {
                    gm_ids.remove(packagesModels.get(pos).getId());
                    gm_names.remove(packagesModels.get(pos).getTest_sub_name());
                    context.setGmName(gm_ids, gm_names);
                    packagesModels.get(pos).setIschecked(false);

                }
            }
        });*/

        holder.setItemClickListener(new PackageItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {

                ns = new NetworkStatus();
                isOnline = ns.isOnline(context.getActivity());


                if(isOnline) {
                    if (userSessionManager.checkLogin() != false)
                    {
                        Toast.makeText(context.getActivity(), "Please Login...!", Toast.LENGTH_SHORT).show();

                    } else
                        {

                            gm_ids.add(packagesModels.get(pos).getId());
                            HashSet hs = new HashSet();
                            hs.addAll(gm_ids);
                            gm_ids.clear();
                            gm_ids.addAll(hs);

                            gm_names.add(packagesModels.get(pos).getTest_sub_name());
                            HashSet hs1 = new HashSet();
                            hs1.addAll(gm_names);
                            gm_names.clear();
                            gm_names.addAll(hs1);

                            gm_sub_ids.add(packagesModels.get(pos).getTest_id());
                            HashSet hs2 = new HashSet();
                            hs1.addAll(gm_sub_ids);
                            gm_sub_ids.clear();
                            gm_sub_ids.addAll(hs2);

                        //    context.setGmName(gm_ids, gm_names,gm_sub_ids);
                           // context.retrievePackageList();
                           /* Intent intent = new Intent(context.getActivity(), PackagesSubTests.class);
                            intent.putExtra("package_id", packagesModels.get(pos).getTest_id());
                            intent.putExtra("package_name",packagesModels.get(pos).getTest_name());

                        Log.d("PACKINFOADAPTER:", packagesModels.get(pos).getTest_id()+" / "+pack_name);
                        context.startActivity(intent);*/
                    }
                }else {
                    Toast.makeText(context.getActivity(), "Please Check your Internet connection..!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.packagesModels.size();
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new CustomFilterforTestsubtest(filterList,this);
        }

        return filter;
    }
}
