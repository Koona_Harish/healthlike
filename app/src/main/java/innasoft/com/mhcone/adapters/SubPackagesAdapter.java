package innasoft.com.mhcone.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import innasoft.com.mhcone.activities.SubPackagesActivity;
import innasoft.com.mhcone.activities.SubTestsActivity;
import innasoft.com.mhcone.holders.SubPackagesHolder;
import innasoft.com.mhcone.holders.SubTestHolder;
import innasoft.com.mhcone.models.SubPackagesModel;
import innasoft.com.mhcone.models.SubTestModel;

public class SubPackagesAdapter extends RecyclerView.Adapter<SubPackagesHolder>
{
    private ArrayList<SubPackagesModel> subPackagesModels;
    private SubPackagesActivity context;
    LayoutInflater vi;
    int resource;
    private Typeface typeface;

    public SubPackagesAdapter(ArrayList<SubPackagesModel> subPackagesModels, SubPackagesActivity context, int resource) {
        this.subPackagesModels = subPackagesModels;
        this.context = context;
        this.resource = resource;
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public SubPackagesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = vi.inflate(resource, parent,false);
        SubPackagesHolder spvh = new SubPackagesHolder(layoutView, (SubPackagesHolder.MyViewHolder) context);
        return spvh;
    }

    @Override
    public void onBindViewHolder(SubPackagesHolder holder, int position) {
        typeface = Typeface.createFromAsset(context.getAssets(), "robotoregular.ttf");
        holder.subPackName.setTypeface(typeface);
        holder.subPackName.setText(subPackagesModels.get(position).getSub_package_name());
    }

    @Override
    public int getItemCount() {
        return this.subPackagesModels.size();
    }
}
