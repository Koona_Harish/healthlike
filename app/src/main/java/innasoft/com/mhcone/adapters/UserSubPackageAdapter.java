package innasoft.com.mhcone.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import innasoft.com.mhcone.activities.ViewMyPackageDetails;
import innasoft.com.mhcone.holders.UserSubPackageHolder;
import innasoft.com.mhcone.models.UserSubPackageModel;

public class UserSubPackageAdapter extends RecyclerView.Adapter<UserSubPackageHolder> {

    private ArrayList<UserSubPackageModel> userSubPackagesModels;
    private ViewMyPackageDetails context;
    LayoutInflater vi;
    int resource;
    private Typeface typeface;

    public UserSubPackageAdapter(ArrayList<UserSubPackageModel> userSubPackagesModels, ViewMyPackageDetails context, int resource) {
        this.userSubPackagesModels = userSubPackagesModels;
        this.context = context;
        this.resource = resource;
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public UserSubPackageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = vi.inflate(resource, parent,false);
        UserSubPackageHolder uspvh = new UserSubPackageHolder(layoutView, (UserSubPackageHolder.MyViewHolder) context);
        return uspvh;
    }

    @Override
    public void onBindViewHolder(UserSubPackageHolder holder, int position) {
        typeface = Typeface.createFromAsset(context.getAssets(), "robotoregular.ttf");
        holder.usersubpackageName.setTypeface(typeface);
        holder.usersubpackageName.setText(userSubPackagesModels.get(position).getSub_test_name());
    }

    @Override
    public int getItemCount() {
        return this.userSubPackagesModels.size();
    }
}
