package innasoft.com.mhcone.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import innasoft.com.mhcone.activities.ViewMyPackage;
import innasoft.com.mhcone.holders.SubViewMyPackageHolder;
import innasoft.com.mhcone.models.SubViewMyPackageModel;

public class SubViewMyPackageAdapter extends RecyclerView.Adapter<SubViewMyPackageHolder>
{
    private ArrayList<SubViewMyPackageModel> itemsList;
    private ViewMyPackage context;
    int resource;
    String packageName;

    public SubViewMyPackageAdapter(String PackageName,ArrayList<SubViewMyPackageModel> itemsList, ViewMyPackage context, int resource) {
        this.itemsList = itemsList;
        this.context = context;
        this.resource = resource;
        this.packageName = PackageName;
    }

    @Override
    public SubViewMyPackageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        SubViewMyPackageHolder newsHolder = new SubViewMyPackageHolder(packageName, view, itemsList, context);
        return newsHolder;
    }

    @Override
    public void onBindViewHolder(SubViewMyPackageHolder holder, int position) {

        holder.itemHeading.setText(itemsList.get(position).sub_test_name);
    }

    @Override
    public int getItemCount() {
        return this.itemsList.size();
    }
}
