package innasoft.com.mhcone.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.DummyTestBillingActivity;
import innasoft.com.mhcone.activities.LoginActivity;
import innasoft.com.mhcone.activities.LoginViaOTPActivity;
import innasoft.com.mhcone.activities.ShowLabInformation;
import innasoft.com.mhcone.activities.ShowLabsTest;
import innasoft.com.mhcone.filters.CustomFilterforShowLabsTest;
import innasoft.com.mhcone.holders.ShowLabsTestHolder;
import innasoft.com.mhcone.itemclicklistener.ShowLabsTestsClickListener;
import innasoft.com.mhcone.models.ShowLabsTestModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class ShowLabsTestAdapter extends RecyclerView.Adapter<ShowLabsTestHolder> implements Filterable {
    public ArrayList<ShowLabsTestModel> showLabsTestList, filterList;
    LayoutInflater showLabLI;
    ShowLabsTest context;
    LayoutInflater vi;
    int resource;
    private Typeface type_lato, type_lato2;
    CustomFilterforShowLabsTest filter;
    String sub_testId;
    ArrayList<Object> arraylistsubtestNamesObject;
    ArrayList<Object> arraylistsubtestIdsObject;
    ArrayList<Object> arraylisttestIdsObject;
    String sub_testName;
    UserSessionManager userSessionManager;

    public ShowLabsTestAdapter(String sub_testName, ArrayList<ShowLabsTestModel> showLabsTestList, ShowLabsTest context, int resource, String sub_testId
            , ArrayList<Object> arraylistsubtestNamesObject, ArrayList<Object> arraylistsubtestIdsObject, ArrayList<Object> arraylisttestIdsObject) {
        this.showLabsTestList = showLabsTestList;
        this.context = context;
        this.resource = resource;
        this.filterList = showLabsTestList;
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.sub_testId = sub_testId;
        this.arraylistsubtestNamesObject = arraylistsubtestNamesObject;
        this.arraylistsubtestIdsObject = arraylistsubtestIdsObject;
        this.arraylisttestIdsObject = arraylisttestIdsObject;
        this.sub_testName = sub_testName;
        userSessionManager = new UserSessionManager(context.getApplicationContext());
    }

    @Override
    public ShowLabsTestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        ShowLabsTestHolder holder = new ShowLabsTestHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ShowLabsTestHolder holder, final int position) {
        type_lato = Typeface.createFromAsset(context.getAssets(), "robotoregular.ttf");
        type_lato2 = Typeface.createFromAsset(context.getAssets(), "robotobold.ttf");

        holder.show_address_tittle_txt.setTypeface(type_lato);
        holder.show_certified_txt.setTypeface(type_lato);

        holder.show_lab_name_txt.setTypeface(type_lato2);
        holder.show_lab_name_txt.setText(showLabsTestList.get(position).getShow_lab_user_name());

        holder.show_address_txt.setTypeface(type_lato);
        holder.show_address_txt.setText(showLabsTestList.get(position).getLocation());

        if (showLabsTestList.get(position).getCertificate().equals("1")) {
            holder.certified_test.setText("NABL/CAP Accreditation");
        } else {
            holder.certified_test.setText("");
        }

        if (showLabsTestList.get(position).getProfile_pic_url().equals("null")) {
            Picasso.with(context)
                    .load(R.drawable.lab_profile_pic_default)
                    .placeholder(R.drawable.lab_profile_pic_default)
                    .fit()
                    .into(holder.prifileImage);
        } else {
            Picasso.with(context)
                    .load(AppUrls.BASE_URL + showLabsTestList.get(position).getProfile_pic_url())
                    .placeholder(R.drawable.lab_profile_pic_default)
                    .fit()
                    .into(holder.prifileImage);
        }


        float rattingValue = Float.valueOf(showLabsTestList.get(position).getShow_lab_ratting());
        holder.ratingBar_tests.setRating(rattingValue);
        holder.show_timings_txt.setTypeface(type_lato);

        if (showLabsTestList.get(position).getShow_lab_workingtime().equals("Today lab Close")) {
            holder.show_timings_txt.setText(showLabsTestList.get(position).getShow_lab_workingtime());
        } else {
            holder.show_timings_txt.setText(showLabsTestList.get(position).getShow_lab_workingtime());
        }

        holder.show_lab_price_txt.setTypeface(type_lato2);
        double labPriceDouble = Double.valueOf(showLabsTestList.get(position).getShow_lab_lab_price());
        holder.show_lab_price_txt.setText("MRP : Rs. " + new DecimalFormat("##.###").format(labPriceDouble));

        holder.show_final_price_txt.setTypeface(type_lato2);
        double appPriceDouble = Double.valueOf(showLabsTestList.get(position).getShow_lab_final_price());
        holder.show_final_price_txt.setText("Our Price : Rs. " + new DecimalFormat("##.###").format(appPriceDouble));

        double distance = Double.valueOf(showLabsTestList.get(position).getShow_lab_distance());
        holder.show_distance_txt.setTypeface(type_lato);
        holder.show_distance_txt.setText("~ " + new DecimalFormat("##.###").format(distance) + " KM");

        String homeCollection = showLabsTestList.get(position).getShow_lab_home_collections();

        if (homeCollection.equals("1")) {

            holder.show_homecollection_txt.setTypeface(type_lato);
            holder.checktickmark.setImageResource(R.drawable.right_mark_new);
        } else if (homeCollection.equals("0")) {
            holder.show_homecollection_txt.setTypeface(type_lato);
            holder.checktickmark.setImageResource(R.drawable.access_denied);
        }

        holder.test_View.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ShowLabInformation.class);
                intent.putExtra("toLat", showLabsTestList.get(position).getShow_lab_lat());
                intent.putExtra("tolng", showLabsTestList.get(position).getShow_lab_lang());
                intent.putExtra("LabID", showLabsTestList.get(position).getShow_lab_id());
                context.startActivity(intent);
            }
        });

        holder.setItemClickListener(new ShowLabsTestsClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                if (!userSessionManager.isUserLoggedIn()) {

                    Toast.makeText(context, "Please Login...!", Toast.LENGTH_SHORT).show();
                    Intent loginIntent = new Intent(context, LoginViaOTPActivity.class);
                    loginIntent.putExtra("Login", "other");
                    context.startActivity(loginIntent);

                } else {
                    String selectedlabId = showLabsTestList.get(pos).getShow_lab_id();
                    String selectedLabName = showLabsTestList.get(pos).getShow_lab_user_name();
                    String selectedLabAddress = showLabsTestList.get(pos).getShow_lab_address();
                    String selectedLabPrice = showLabsTestList.get(pos).getShow_lab_final_price();
                    String selectedLabPhoneNumber = showLabsTestList.get(pos).getShow_lab_mobile();

                    Intent intent = new Intent(context, DummyTestBillingActivity.class);
                    intent.putExtra("selectedLabId", selectedlabId);
                    intent.putExtra("selectedLabName", selectedLabName);
                    intent.putExtra("selectedLabAddress", selectedLabAddress);
                    intent.putExtra("subTestId", sub_testId);
                    intent.putExtra("selecteLabPhone", selectedLabPhoneNumber);
                    intent.putExtra("payment_lab", showLabsTestList.get(pos).getShow_lab_payment_lab());
                    intent.putExtra("payment_online", showLabsTestList.get(pos).getShow_lab_payment_online());
                    intent.putExtra("tele_booking", showLabsTestList.get(pos).getShow_lab_tele_booking());

                    Bundle args = new Bundle();
                    args.putSerializable("arraylistsubtestNames", (Serializable) arraylistsubtestNamesObject);
                    args.putSerializable("arraylistsubtestIds", (Serializable) arraylistsubtestIdsObject);
                    args.putSerializable("arraylisttestIds", (Serializable) arraylisttestIdsObject);
                    intent.putExtra("bundelData", args);

                    intent.putExtra("subTestName", sub_testName);
                    intent.putExtra("selectedLabPrice", selectedLabPrice);
                    context.startActivity(intent);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.showLabsTestList.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterforShowLabsTest(filterList, this);
        }
        return filter;
    }
}
