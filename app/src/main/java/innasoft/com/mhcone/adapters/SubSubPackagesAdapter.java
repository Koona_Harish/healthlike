package innasoft.com.mhcone.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import innasoft.com.mhcone.activities.SubPackagesActivity;
import innasoft.com.mhcone.activities.SubSubPackagesActivity;
import innasoft.com.mhcone.holders.SubSubPackagesHolder;
import innasoft.com.mhcone.models.SubPackagesModel;
import innasoft.com.mhcone.models.SubSubPackagesModel;

public class SubSubPackagesAdapter extends RecyclerView.Adapter<SubSubPackagesHolder>
{
    private ArrayList<SubSubPackagesModel> subSubPackagesModels;
    private SubSubPackagesActivity context;
    LayoutInflater vi;
    int resource;
    private Typeface typeface;

    public SubSubPackagesAdapter(ArrayList<SubSubPackagesModel> subSubPackagesModels, SubSubPackagesActivity context, int resource) {
        this.subSubPackagesModels = subSubPackagesModels;
        this.context = context;
        this.resource = resource;
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public SubSubPackagesHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = vi.inflate(resource, parent,false);
        SubSubPackagesHolder ssvh = new SubSubPackagesHolder(layoutView, (SubSubPackagesHolder.MyViewHolder) context);
        return ssvh;
    }

    @Override
    public void onBindViewHolder(SubSubPackagesHolder holder, int position) {
        typeface = Typeface.createFromAsset(context.getAssets(), "robotoregular.ttf");
        holder.subSubPackName.setTypeface(typeface);
        holder.subSubPackName.setText(subSubPackagesModels.get(position).getSub_sub_pack_name());
    }

    @Override
    public int getItemCount() {
        return this.subSubPackagesModels.size();
    }
}
