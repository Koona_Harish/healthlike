package innasoft.com.mhcone.adapters;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.DummyBillingActivity;
import innasoft.com.mhcone.activities.LoginViaOTPActivity;
import innasoft.com.mhcone.activities.RegisterActivity;
import innasoft.com.mhcone.activities.ShowLabInformation;
import innasoft.com.mhcone.activities.ShowLabsPackage;
import innasoft.com.mhcone.filters.CustomFilterforShowLabsPackages;
import innasoft.com.mhcone.holders.ShowLabsPackageHolder;
import innasoft.com.mhcone.itemclicklistener.ShowLabsPackagesClickListener;
import innasoft.com.mhcone.models.ShowLabsPackageModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.FCM.Config;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class ShowLabsPackageAdapter extends RecyclerView.Adapter<ShowLabsPackageHolder> implements Filterable {
    public ArrayList<ShowLabsPackageModel> showLabsPackageList, filterList;
    LayoutInflater showLabLI;
    ShowLabsPackage context;
    LayoutInflater vi;
    private Typeface type_lato, type_lato2;
    int resource;
    String package_id, package_name, subtestNames, bridgepackTestandSubIDswithformat, showlabspackageList, subTestIdsList;

    CustomFilterforShowLabsPackages filter;
    UserSessionManager userSessionManager;
    ProgressDialog progressDialog;
    String token;

    public ShowLabsPackageAdapter(String package_id, String package_name, String subtestNames, String bridgepackTestandSubIDswithformat, ArrayList<ShowLabsPackageModel> showLabsPackageList, ShowLabsPackage context, int resource, String subTestIdsList) {
        this.showLabsPackageList = showLabsPackageList;
        this.context = context;
        this.resource = resource;
        this.filterList = showLabsPackageList;
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.package_id = package_id;
        this.package_name = package_name;
        this.subtestNames = subtestNames;
        this.subTestIdsList = subTestIdsList;
        this.bridgepackTestandSubIDswithformat = bridgepackTestandSubIDswithformat;
        userSessionManager = new UserSessionManager(context);
        progressDialog = new ProgressDialog(context, R.style.DialogTheme);
        progressDialog.setMessage("Please wait......");

        SharedPreferences pref = context.getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        if (!TextUtils.isEmpty(regId)) {
            token = regId;
            Log.d("TOKENVALUE", token);
        }
    }

    @Override
    public ShowLabsPackageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        ShowLabsPackageHolder holder = new ShowLabsPackageHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ShowLabsPackageHolder holder, final int position) {
        type_lato = Typeface.createFromAsset(context.getAssets(), "robotoregular.ttf");
        type_lato2 = Typeface.createFromAsset(context.getAssets(), "robotobold.ttf");

        holder.show_lab_package_name_txt.setTypeface(type_lato2);
        holder.show_lab_package_name_txt.setText(showLabsPackageList.get(position).getShow_lab_package_user_name());
        holder.show_lab_package_address_txt.setTypeface(type_lato);
        holder.show_lab_package_address_txt.setText(showLabsPackageList.get(position).getLocation());

        float rattingValue = Float.valueOf(showLabsPackageList.get(position).getShow_lab_ratting());
        holder.ratingBar_package.setRating(rattingValue);

        if (showLabsPackageList.get(position).getProfile_pic_url().equals("null")) {
            Picasso.with(context)
                    .load(R.drawable.lab_profile_pic_default)
                    .placeholder(R.drawable.lab_profile_pic_default)
                    .fit()
                    .into(holder.labProfilePic);
        } else {
            Picasso.with(context)
                    .load(showLabsPackageList.get(position).getProfile_pic_url())
                    .placeholder(R.drawable.lab_profile_pic_default)
                    .fit()
                    .into(holder.labProfilePic);
        }

        holder.show_lab_package_timings_txt.setTypeface(type_lato);
        if (showLabsPackageList.get(position).getCertificate().equals("1")) {
            holder.certified_package.setTypeface(type_lato);
            holder.certified_package.setText("NABL/CAP Accreditation");
        } else {
            holder.certified_package.setTypeface(type_lato);
            holder.certified_package.setText("");
        }
        if (showLabsPackageList.get(position).getShow_lab_package_workingtime().equals("Today lab Close")) {

            holder.show_lab_package_timings_txt.setText(showLabsPackageList.get(position).getShow_lab_package_workingtime());
        } else {
            holder.show_lab_package_timings_txt.setText(showLabsPackageList.get(position).getShow_lab_package_workingtime());
        }

        holder.show_lab_package_lab_price_txt.setTypeface(type_lato2);
        double labPriceDouble = Double.valueOf(showLabsPackageList.get(position).getShow_lab_package_lab_price());
        holder.show_lab_package_lab_price_txt.setText(" MRP: Rs." + new DecimalFormat("##.###").format(labPriceDouble));
        holder.show_lab_package_lab_price_txt.setPaintFlags(holder.show_lab_package_lab_price_txt.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        holder.show_lab_package_final_price_txt.setTypeface(type_lato2);
        double appPriceDouble = Double.valueOf(showLabsPackageList.get(position).getShow_lab_package_final_price());
        holder.show_lab_package_final_price_txt.setText("Our Price: Rs." + new DecimalFormat("##.###").format(appPriceDouble));

        double distance = Double.valueOf(showLabsPackageList.get(position).getShow_lab_package_distance());
        holder.show_lab_package_distance_txt.setTypeface(type_lato);
        holder.show_lab_package_distance_txt.setText("~" + new DecimalFormat("##.###").format(distance) + " KM");

        String homeCollection = showLabsPackageList.get(position).getShow_lab_package_home_collections();
        if (homeCollection.equals("1")) {

            holder.show_lab_package_homecollection_txt.setTypeface(type_lato);
            holder.checktickmarkPackage.setImageResource(R.drawable.right_mark_new);

        } else if (homeCollection.equals("0")) {

            holder.show_lab_package_homecollection_txt.setTypeface(type_lato);
            holder.checktickmarkPackage.setImageResource(R.drawable.access_denied);
        }

        holder.show_info_lab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ShowLabInformation.class);
                intent.putExtra("toLat", showLabsPackageList.get(position).getShow_lab_package_lat());
                intent.putExtra("tolng", showLabsPackageList.get(position).getShow_lab_package_lang());
                intent.putExtra("LabID", showLabsPackageList.get(position).getShow_lab_package_id());
                context.startActivity(intent);
            }
        });
        holder.setItemClickListener(new ShowLabsPackagesClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                if (!userSessionManager.isUserLoggedIn()) {
                    Toast.makeText(context, "Please Login...!", Toast.LENGTH_SHORT).show();
                    Intent loginIntent = new Intent(context, LoginViaOTPActivity.class);
                    loginIntent.putExtra("Login","other");
                    context.startActivity(loginIntent);
                } else {
                    String selectedlabId = showLabsPackageList.get(pos).getShow_lab_package_id();
                    String selectedLabName = showLabsPackageList.get(pos).getShow_lab_package_user_name();
                    String selectedLabAddress = showLabsPackageList.get(pos).getShow_lab_package_address();
                    String selectedLabPrice = showLabsPackageList.get(pos).getShow_lab_package_final_price();

                    //Intent intent=new Intent(context,BillingPageForPackageBooking.class);
                    Intent intent = new Intent(context, DummyBillingActivity.class);
                    intent.putExtra("selectedLabPhone", showLabsPackageList.get(pos).getShow_lab_package_mobile());
                    intent.putExtra("selecteLabId", selectedlabId);
                    intent.putExtra("packageId", package_id);
                    intent.putExtra("payment_lab", showLabsPackageList.get(pos).getShow_lab_payment_lab());
                    intent.putExtra("payment_online", showLabsPackageList.get(pos).getShow_lab_payment_online());
                    intent.putExtra("tele_booking", showLabsPackageList.get(pos).getShow_lab_tele_booking());
                    intent.putExtra("subTestName", package_name);
                    intent.putExtra("subTestsNAMES", subtestNames);
                    intent.putExtra("selectedLabPrice", selectedLabPrice);
                    intent.putExtra("selectedLabName", selectedLabName);
                    intent.putExtra("selectedLabAddress", selectedLabAddress);
                    intent.putExtra("subTestIdsList", subTestIdsList);
                    intent.putExtra("packTestandSubIDswithformat", bridgepackTestandSubIDswithformat);
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.showLabsPackageList.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterforShowLabsPackages(filterList, this);
        }

        return filter;
    }
}
