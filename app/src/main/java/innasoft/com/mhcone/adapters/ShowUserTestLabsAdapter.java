package innasoft.com.mhcone.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.BillingPageForUserPackage;
import innasoft.com.mhcone.activities.DummyUserPAckageBillingActivity;
import innasoft.com.mhcone.activities.ShowLabInformation;
import innasoft.com.mhcone.activities.ShowUserTestLabs;
import innasoft.com.mhcone.filters.CustomFilterforUserTestLabs;
import innasoft.com.mhcone.holders.ShowUserTestLabsHolder;
import innasoft.com.mhcone.itemclicklistener.ShowLabsUserPackagesClickListener;
import innasoft.com.mhcone.models.ShowUserTestLabsModel;
import innasoft.com.mhcone.utilities.AppUrls;

public class ShowUserTestLabsAdapter extends RecyclerView.Adapter<ShowUserTestLabsHolder> implements Filterable {

    public ArrayList<ShowUserTestLabsModel> showUserTestLabsList,filterList;
    ShowUserTestLabs context;
    LayoutInflater vi;
    int resource;
    private Typeface type_lato,type_lato2;
    CustomFilterforUserTestLabs filter;
    String labID,labName,price,total;
    String lat,lng,sub_testId,sub_testName,billingPageValuesIds,packageName;

    public ShowUserTestLabsAdapter(String lat,String lng,String sub_testId,String sub_testName,String billingPageValuesIds,String packageName,ArrayList<ShowUserTestLabsModel> showUserTestLabsList, ShowUserTestLabs context, int resource) {
        this.showUserTestLabsList = showUserTestLabsList;
        this.context = context;
        this.resource = resource;
        this.filterList = showUserTestLabsList;
        this.lat = lat;
        this.lng = lng;
        this.sub_testId = sub_testId;
        this.sub_testName = sub_testName;
        this.packageName = packageName;
        this.billingPageValuesIds = billingPageValuesIds;
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public ShowUserTestLabsHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v= LayoutInflater.from(parent.getContext()).inflate(resource,parent,false);
        ShowUserTestLabsHolder holder=new ShowUserTestLabsHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ShowUserTestLabsHolder holder, final int position)
    {
        type_lato = Typeface.createFromAsset(context.getAssets(), "robotoregular.ttf");
        type_lato2 = Typeface.createFromAsset(context.getAssets(), "robotobold.ttf");

        holder.show_address_tittle.setTypeface(type_lato2);

        holder.show_user_test_lab_name_txt.setTypeface(type_lato2);
        holder.show_user_test_lab_name_txt.setText(showUserTestLabsList.get(position).getShow_user_test_lab_user_name());

        holder.show_user_test_lab_address_txt.setTypeface(type_lato);
        holder.show_user_test_lab_address_txt.setText(showUserTestLabsList.get(position).getLocation());

        if(showUserTestLabsList.get(position).getCertificate().equals("1"))
        {
            holder.certified_user_package.setTypeface(type_lato);
            holder.certified_user_package.setText("NABL/CAP Accreditation");
        }
        else {
            holder.certified_user_package.setTypeface(type_lato);
            holder.certified_user_package.setText("");
        }

          if(showUserTestLabsList.get(position).getProfile_pic_url().equals("null"))
        {
        Picasso.with(context)
                .load(R.drawable.lab_profile_pic_default)
                .placeholder(R.drawable.lab_profile_pic_default)
                .fit()
                .into(holder.prifileImage);
        }
        else {

            Picasso.with(context)
                    .load(AppUrls.BASE_URL+showUserTestLabsList.get(position).getProfile_pic_url())
                    .placeholder(R.drawable.lab_profile_pic_default)
                    .fit()
                    .into(holder.prifileImage);
        }

        float rattingValue = Float.valueOf(showUserTestLabsList.get(position).getShow_user_test_lab_ratting());
        holder.ratingBar_user_test_labs.setRating(rattingValue);
        holder.show_user_test_lab_timings_txt.setTypeface(type_lato);

        if(showUserTestLabsList.get(position).getShow_user_test_lab_workingtime().equals("Today lab Close")) {

            holder.show_user_test_lab_timings_txt.setText(showUserTestLabsList.get(position).getShow_user_test_lab_workingtime());
        }
        else {
            holder.show_user_test_lab_timings_txt.setText("Open Today:"+showUserTestLabsList.get(position).getShow_user_test_lab_workingtime());
        }

        holder.show_user_test_lab_final_price_txt.setTypeface(type_lato2);
        double appPriceDouble = Double.valueOf(showUserTestLabsList.get(position).getShow_user_test_lab_final_price());
        holder.show_user_test_lab_final_price_txt.setText("Rs. "+new DecimalFormat("##.###").format(appPriceDouble));

        holder.show_user_test_lab_lab_price_txt.setTypeface(type_lato2);
        double labPriceDouble = Double.valueOf(showUserTestLabsList.get(position).getShow_user_test_lab_lab_price());
        holder.show_user_test_lab_lab_price_txt.setText("Rs. "+new DecimalFormat("##.###").format(labPriceDouble));
        holder.show_user_test_lab_lab_price_txt.setPaintFlags(holder.show_user_test_lab_lab_price_txt.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        double distance = Double.valueOf(showUserTestLabsList.get(position).getShow_user_test_lab_distance());
        holder.show_user_test_lab_distance_txt.setTypeface(type_lato);
        holder.show_user_test_lab_distance_txt.setText("~"+new DecimalFormat("##.###").format(distance)+" KM");

        String homeCollection = showUserTestLabsList.get(position).getShow_user_test_lab_home_collections();

        holder.show_user_test_lab_homecollection_txt.setTypeface(type_lato);
        if(homeCollection.equals("1")){
            holder.checktickmarkUserTestLabs.setImageResource(R.drawable.right_mark_new);

        }else if (homeCollection.equals("0")){
            holder.checktickmarkUserTestLabs.setImageResource(R.drawable.access_denied);

        }

        holder.show_user_test_lab_View.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ShowLabInformation.class);
                intent.putExtra("toLat", showUserTestLabsList.get(position).getShow_user_test_lab_lat());
                intent.putExtra("tolng", showUserTestLabsList.get(position).getShow_user_test_lab_lang());
                intent.putExtra("LabID", showUserTestLabsList.get(position).getShow_user_test_lab_id());
                context.startActivity(intent);
            }
        });
        holder.setItemClickListener(new ShowLabsUserPackagesClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                labID = showUserTestLabsList.get(pos).getShow_user_test_lab_id();
                labName = showUserTestLabsList.get(pos).getShow_user_test_lab_user_name();
                price = showUserTestLabsList.get(pos).getShow_user_test_lab_final_price();

                Intent intent = new Intent(context.getApplicationContext(),DummyUserPAckageBillingActivity.class);
                intent.putExtra("LAT",lat);
                intent.putExtra("LNG",lng);
                intent.putExtra("SUBTESTID",sub_testId);
                intent.putExtra("selectedLabPone", showUserTestLabsList.get(pos).getShow_user_test_lab_mobile());
                intent.putExtra("SUBTESTNAME",sub_testName);
                intent.putExtra("BILLINGPAGEVALUESIDS",billingPageValuesIds);
                intent.putExtra("PACKAGENAME",packageName);
                intent.putExtra("LABID",labID);
                intent.putExtra("LABNAME",labName);
                intent.putExtra("PRICE",price);
                intent.putExtra("payment_lab", showUserTestLabsList.get(pos).getShow_user_test_user_test_lab_payment_lab());
                intent.putExtra("payment_online", showUserTestLabsList.get(pos).getShow_user_test_lab_payment_online());
                intent.putExtra("tele_booking", showUserTestLabsList.get(pos).getShow_user_test_lab_tele_booking());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return this.showUserTestLabsList.size();
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new CustomFilterforUserTestLabs(filterList,this);
        }

        return filter;
    }
}
