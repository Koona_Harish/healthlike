package innasoft.com.mhcone.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import innasoft.com.mhcone.holders.GetMemInGroupHolder;
import innasoft.com.mhcone.itemclicklistener.GetMemInGroupClickListner;
import innasoft.com.mhcone.models.GetMemInGroupModel;

/**
 * Created by admin on 12/18/2017.
 */

public class GetMemGroupAdapter extends RecyclerView.Adapter<GetMemInGroupHolder> {
    public ArrayList<GetMemInGroupModel> getMemList;
    public Context context;
    LayoutInflater li;
    int resource;


    public GetMemGroupAdapter(ArrayList<GetMemInGroupModel> getMemList, Context context, int resource) {
        this.getMemList = getMemList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public GetMemInGroupHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, null);
        GetMemInGroupHolder slh = new GetMemInGroupHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final GetMemInGroupHolder holder, final int position) {
        holder.memNAME.setText(getMemList.get(position).getName());


        holder.setItemClickListener(new GetMemInGroupClickListner() {
            @Override
            public void onItemClick(View view, int layoutPosition) {
                /*getMemList.remove(layoutPosition);
                notifyItemRemoved(layoutPosition);
                notifyItemRangeChanged(layoutPosition, getMemList.size());*/

                Intent intent = new Intent("remove-data");
                intent.putExtra("position", position);
                intent.putExtra("Name", getMemList.get(position).getName());
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return getMemList.size();
    }

}
