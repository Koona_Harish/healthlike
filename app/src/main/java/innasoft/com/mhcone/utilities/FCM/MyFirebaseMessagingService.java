package innasoft.com.mhcone.utilities.FCM;

/**
 * Created by administator on 12/29/2017.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.activities.HelathTipsNotification;
import innasoft.com.mhcone.activities.LabRatingUsingPushNotificationMess;
import innasoft.com.mhcone.activities.NotificationBookingInformation;
import innasoft.com.mhcone.activities.YourNotification;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FirebaseMessageService";
    Bitmap bitmap;
    int notificationId = 0;
    final String GROUP_KEY_TIPS = "health_tips";
    final String GROUP_KEY_RATTING = "health_ratting";

    Random random = new Random();
    int m = random.nextInt(9999 - 1000) + 1000;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {



        //String title = remoteMessage.getNotification().getTitle();
        // String message = remoteMessage.getNotification().getBody();



        /*Intent intent = new Intent(this, HelathTipsNotification.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder notificationbuilder = new NotificationCompat.Builder(this);
        notificationbuilder.setContentTitle(title);
        notificationbuilder.setContentText(message);
        notificationbuilder.setAutoCancel(true);
        notificationbuilder.setSmallIcon(R.mipmap.ic_launcher);
        notificationbuilder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationbuilder.build());*/
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }


        String message = remoteMessage.getData().get("message");
        //imageUri will contain URL of the image to be displayed with Notification
        String imageUri = remoteMessage.getData().get("image");
        String title = remoteMessage.getData().get("title");
        String notification_id = remoteMessage.getData().get("notification_id");

        //If the key AnotherActivity has  value as True then when the user taps on notification, in the app AnotherActivity will be opened.
        //If the key AnotherActivity has  value as False then when the user taps on notification, in the app MainActivity will be opened.
        String TrueOrFlase = remoteMessage.getData().get("AnotherActivity");

        //To get a Bitmap image from the URL received
        bitmap = getBitmapfromUrl(imageUri);
        if(TrueOrFlase.equals("habit_activicty")) {
            sendNotification(message, bitmap, TrueOrFlase, imageUri, message, title,notification_id);
        }
        if(TrueOrFlase.equals("rating_activicty"))
        {
            String title2 = remoteMessage.getData().get("title2");
            String title3 = remoteMessage.getData().get("title3");
            requesrtToRating(message, bitmap, TrueOrFlase, imageUri, message, title, title2, title3);
        }

        if(TrueOrFlase.equals("order_status"))
        {

            orderIdStatus(message, bitmap, TrueOrFlase, imageUri, message, title,notification_id);
        }

        if(TrueOrFlase.equals("notification_activicty"))
        {
            String mytitle = remoteMessage.getData().get("title");
            String mymsg = remoteMessage.getData().get("message");
            requestYourNotification(mytitle, mymsg);
        }

    }

    private void orderIdStatus(String messageBody, Bitmap image, String TrueOrFalse, String imageurl,String message, String title, String notification_id) {
        Intent intent = new Intent(this, NotificationBookingInformation.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("AnotherActivity", TrueOrFalse);
        intent.putExtra("title", title);
        intent.putExtra("imageUrl", imageurl); //imageUrl,message
        intent.putExtra("message", message);
        intent.putExtra("notification_id", notification_id);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0/* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.myhealthlogo))/*Notification icon image*/
                .setSmallIcon(R.drawable.myhealthlogo)
                .setContentTitle(Html.fromHtml(Html.fromHtml(title).toString()))
                .setWhen(System.currentTimeMillis())


                .setContentText(Html.fromHtml(Html.fromHtml(messageBody).toString()))
                .setAutoCancel(true)
                /*.setGroup(GROUP_KEY_TIPS)
                .setGroupSummary(true)*/
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationCompat.BigPictureStyle s = new NotificationCompat.BigPictureStyle().bigPicture(image);
        s.setSummaryText(Html.fromHtml(Html.fromHtml(messageBody).toString()));
        notificationBuilder.setStyle(s);

        Notification notification = notificationBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND;

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(m /* ID of notification */, notification);
    }

    private void requestYourNotification(String mytitle, String mymsg) {
        Intent intent = new Intent(this, YourNotification.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        // intent.putExtra("AnotherActivity", TrueOrFalse);
        intent.putExtra("title", mytitle);

        intent.putExtra("message", mymsg);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0/* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.myhealthlogo))/*Notification icon image*/
                .setSmallIcon(R.drawable.myhealthlogo)
                .setContentTitle(Html.fromHtml(Html.fromHtml(mytitle).toString()))
                .setWhen(System.currentTimeMillis())


                .setContentText(Html.fromHtml(Html.fromHtml(mymsg).toString()))
                .setAutoCancel(true)
                /*.setGroup(GROUP_KEY_TIPS)
                .setGroupSummary(true)*/
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

       /* NotificationCompat.BigPictureStyle s = new NotificationCompat.BigPictureStyle().bigPicture(image);
        s.setSummaryText(Html.fromHtml(Html.fromHtml(messageBody).toString()));
        notificationBuilder.setStyle(s);*/

        Notification notification = notificationBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND;

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(m /* ID of notification */, notification);
    }

    private void requesrtToRating(String message, Bitmap bitmap, String trueOrFlase, String imageUri, String message1, String title, String title2, String title3) {
        Intent intent = new Intent(this, LabRatingUsingPushNotificationMess.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("AnotherActivity", trueOrFlase);
        intent.putExtra("title", title);
        intent.putExtra("title2", title2);
        intent.putExtra("title3", title3);
        intent.putExtra("imageUrl", imageUri); //imageUrl,message
        intent.putExtra("message", message1);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                0);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.myhealthlogo))/*Notification icon image*/
                .setSmallIcon(R.drawable.myhealthlogo)
                .setContentTitle(Html.fromHtml(Html.fromHtml(message).toString()))

                .setWhen(System.currentTimeMillis())
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(bitmap))/*Notification with Image*/
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent); //.setContentText(Html.fromHtml(Html.fromHtml(message).toString()))

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(m /* ID of notification */, notificationBuilder.build());
    }


    private void sendNotification(String messageBody, Bitmap image, String TrueOrFalse, String imageurl,String message, String title, String notification_id) {
        Intent intent = new Intent(this, HelathTipsNotification.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("AnotherActivity", TrueOrFalse);
        intent.putExtra("title", title);
        intent.putExtra("imageUrl", imageurl); //imageUrl,message
        intent.putExtra("message", message);
        intent.putExtra("notification_id", notification_id);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0/* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.myhealthlogo))/*Notification icon image*/
                .setSmallIcon(R.drawable.myhealthlogo)
                .setContentTitle(Html.fromHtml(Html.fromHtml(title).toString()))
                .setWhen(System.currentTimeMillis())


                .setContentText(Html.fromHtml(Html.fromHtml(messageBody).toString()))
                .setAutoCancel(true)
                /*.setGroup(GROUP_KEY_TIPS)
                .setGroupSummary(true)*/
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationCompat.BigPictureStyle s = new NotificationCompat.BigPictureStyle().bigPicture(image);
        s.setSummaryText(Html.fromHtml(Html.fromHtml(messageBody).toString()));
        notificationBuilder.setStyle(s);

        Notification notification = notificationBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND;

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(m /* ID of notification */, notification);
    }

    /*
    *To get a Bitmap image from the URL received
    * */
    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }
}
