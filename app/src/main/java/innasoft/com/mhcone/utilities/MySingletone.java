package innasoft.com.mhcone.utilities;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Purushotham-Book on 9/28/2016.
 */
public class MySingletone
{
    private static MySingletone mySingletone;
    private static Context mCtx;
    private RequestQueue requestQueue;

    private MySingletone(Context cntx)
    {
        mCtx = cntx;
        requestQueue = getRequestQueue();
    }

    private RequestQueue getRequestQueue()
    {
        if(requestQueue == null)

        {
            requestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return requestQueue;
    }

    public static synchronized MySingletone getmInstance(Context context)
    {
        if(mySingletone == null)
        {
            mySingletone = new MySingletone(context);
        }
        return mySingletone;

    }

    public<T> void addToRequestQueue(Request<T> request)
    {
       getRequestQueue().add(request);
    }
}
