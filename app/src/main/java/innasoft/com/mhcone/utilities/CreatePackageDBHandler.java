package innasoft.com.mhcone.utilities;

/**
 * Created by Purushotham-Book on 9/29/2016.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;


public class CreatePackageDBHandler extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "mypackagesMHC.db";

    // Contacts table name
    private static final String TABLE_SUB_TESTS = "subtests";

    // Shops Table Columns names
    public static final String KEY_ID = "id";
    public static final String KEY_SUB_TEST_ID = "sub_test_id";
    public static final String KEY_SUB_TEST_NAME = "sub_test_name";
    public static final String KEY_SUB_TEST_STATUS = "sub_test_state";
    public static final String KEY_TEST_ID = "test_id";
    public static final String KEY_TEST_NAME ="test_name";

    public CreatePackageDBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        /*String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_SUB_TESTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY ," + KEY_SUB_TEST_ID + " TEXT,"
                + KEY_SUB_TEST_NAME + " TEXT," + KEY_SUB_TEST_STATUS+ " TEXT"+ ")";*/

        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_SUB_TESTS  + "("
                + KEY_ID  + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + KEY_SUB_TEST_ID + " TEXT , "
                + KEY_SUB_TEST_NAME + " TEXT, "
                + KEY_TEST_ID + " TEXT, "
                + KEY_TEST_NAME + " TEST, "
                + KEY_SUB_TEST_STATUS + " TEXT )";



        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUB_TESTS);
        // Creating tables again
        onCreate(db);
    }

    //Add  sub-test id (while check the check box)
    public void addSubtest(ContentValues contentValues) {
        SQLiteDatabase db = this.getWritableDatabase();


        // Inserting Row
        db.insert(TABLE_SUB_TESTS, null, contentValues);
        db.close(); // Closing database connection
    }

    // Getting one shop
   /* public Shop getShop(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SHOPS, new String[]{KEY_ID,
                        KEY_NAME, KEY_SH_ADDR}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Shop contact = new Shop(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));
        // return shop
        return contact;
    }*/

    // Getting All Shops
    public List<String> getAllSubTests(String testID) {
        List<String> labsList = new ArrayList<String>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SUB_TESTS +" WHERE "+KEY_TEST_ID+" = "+testID;


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {


                //shop.setId(Integer.parseInt(cursor.getString(0)));
                //shop.setName(cursor.getString(1));
                //shop.setAddress(cursor.getString(2));
                // Adding contact to list
                labsList.add(cursor.getString(1));
                //shopList.add(shop);
            } while (cursor.moveToNext());
        }

        // return contact list
        return labsList;
    }



    public void deleteTableData()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_SUB_TESTS);
    }


    public List<String> getAllSubTests() {
        List<String> labsList = new ArrayList<String>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SUB_TESTS ;


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {


                //shop.setId(Integer.parseInt(cursor.getString(0)));
                //shop.setName(cursor.getString(1));
                //shop.setAddress(cursor.getString(2));
                // Adding contact to list
                labsList.add(cursor.getString(1));
                //shopList.add(shop);
            } while (cursor.moveToNext());
        }

        // return contact list
        return labsList;
    }

    public List<String> getAllSubTestIds() {
        List<String> labsList = new ArrayList<String>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SUB_TESTS ;
       // Log.d("QUERYTEST", selectQuery);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {


                //shop.setId(Integer.parseInt(cursor.getString(0)));
                //shop.setName(cursor.getString(1));
                //shop.setAddress(cursor.getString(2));
                // Adding contact to list
                labsList.add(cursor.getString(1));
                //shopList.add(shop);
            } while (cursor.moveToNext());
        }

        // return contact list
        return labsList;
    }
    public List<String> gettingDistName()
    {
        List<String> testNames = null;

        testNames = new ArrayList<String>();
        String query = "SELECT DISTINCT("+KEY_TEST_NAME+") FROM "+TABLE_SUB_TESTS+";";

        Log.d("QUERYTEST", query);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {


                //shop.setId(Integer.parseInt(cursor.getString(0)));
                //shop.setName(cursor.getString(1));
                //shop.setAddress(cursor.getString(2));
                // Adding contact to list
                testNames.add(cursor.getString(2));
                //shopList.add(shop);
            } while (cursor.moveToNext());
        }

        // return contact list
        return testNames;

    }
    public List<String> getAllTestIds() {
        List<String> labsList = new ArrayList<String>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SUB_TESTS ;
        Log.d("ASDFGHJKL", selectQuery);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {


                //shop.setId(Integer.parseInt(cursor.getString(0)));
                //shop.setName(cursor.getString(1));
                //shop.setAddress(cursor.getString(2));
                // Adding contact to list
                labsList.add(cursor.getString(3));
                //shopList.add(shop);
            } while (cursor.moveToNext());
        }

        // return contact list
        return labsList;
    }




    public List<String> getAllSubTestNames() {
        List<String> labsList = new ArrayList<String>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SUB_TESTS ;
        Log.d("QUERYTEST", selectQuery);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {


                //shop.setId(Integer.parseInt(cursor.getString(0)));
                //shop.setName(cursor.getString(1));
                //shop.setAddress(cursor.getString(2));
                // Adding contact to list
                labsList.add(cursor.getString(2));
                //shopList.add(shop);
            } while (cursor.moveToNext());
        }

        // return contact list
        return labsList;
    }
    // Getting shops Count
   /* public int getShopsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_SHOPS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }*/

    // Updating a shop
   /* public int updateShop(Shop shop) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, shop.getName());
        values.put(KEY_SH_ADDR, shop.getAddress());

        // updating row
        return db.update(TABLE_SHOPS, values, KEY_ID + " = ?",
                new String[]{String.valueOf(shop.getId())});
    }*/

    // Deleting a shop
    /*public void deleteSubtest(ContentValues contentValues) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SUB_TESTS, KEY_SUB_TEST_ID + "="+contentValues,
                null);
        db.close();
    }*/


    //Delete specific sub-test id (while un-check the check box)
    public boolean deleteSubtestID(String id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_SUB_TESTS, KEY_SUB_TEST_ID + "=" + id, null) > 0;
    }
}
