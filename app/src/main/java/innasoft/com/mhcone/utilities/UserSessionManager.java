package innasoft.com.mhcone.utilities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.activities.GetStartedActivity;
import innasoft.com.mhcone.activities.LoginActivity;


public class UserSessionManager {

    SharedPreferences pref, preferences;
    Editor editor, editor2;
    Context _context;
    int PRIVATE_MODE = 0;

    private static final String PREFER_NAME = "mymedicalcheckup";
    private static final String IS_USER_LOGIN = "IsUserLoggedIn";
    public static final String KEY_ACCSES = "access_key";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_MOBILE = "user_mobile";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_AGE = "age";
    public static final String ISFIRTTIMEAPP = "isfirsttime";
    public static final String INTRO_SLIDE = "intorslide";


    public UserSessionManager(Context context) {
        this._context = context;

        preferences = context.getSharedPreferences(INTRO_SLIDE, PRIVATE_MODE);
        editor2 = preferences.edit();

        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    public void createUserLoginSession(String key, String userId, String userName, String mobile, String age, String email) {

        editor.putBoolean(IS_USER_LOGIN, true);
        editor.putString(KEY_ACCSES, key);
        editor.putString(USER_ID, userId);
        editor.putString(USER_NAME, userName);
        editor.putString(USER_MOBILE, mobile);
        editor.putString(USER_EMAIL, email);
        editor.putString(USER_AGE, age);
        editor.commit();
    }


    public HashMap<String, String> getUserDetails() {


        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_ACCSES, pref.getString(KEY_ACCSES, null));
        user.put(USER_ID, pref.getString(USER_ID, null));
        user.put(USER_NAME, pref.getString(USER_NAME, null));
        user.put(USER_MOBILE, pref.getString(USER_MOBILE, null));
        user.put(USER_AGE, pref.getString(USER_AGE, null));
        user.put(USER_EMAIL, pref.getString(USER_EMAIL, null));

        return user;
    }


    public void logoutUser() {

        editor.clear();
        editor.commit();

        Intent i = new Intent(_context, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
    }

    public boolean isUserLoggedIn() {
        return pref.getBoolean(IS_USER_LOGIN, false);
    }

    public void createIsFirstTimeAppLunch() {
        editor2.putBoolean(ISFIRTTIMEAPP, true);
        editor2.commit();
    }

    public boolean checkIsFirstTime() {

        if (!this.isFirstTimeAppLunch()) {

            Intent i = new Intent(_context, GetStartedActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);

            return true;
        }
        return false;
    }

    public boolean isFirstTimeAppLunch() {
        return preferences.getBoolean(ISFIRTTIMEAPP, false);
    }


    public boolean checkLogin() {

        if (!this.isUserLoggedIn()) {

//            Intent i = new Intent(_context, MainActivity.class);
//            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            _context.startActivity(i);
            Intent loginIntent = new Intent(_context, LoginActivity.class);
            loginIntent.putExtra("Login", "other");
            _context.startActivity(loginIntent);
            return true;
        }
        return false;
    }

}