package innasoft.com.mhcone.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.FCM.Config;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class LoginViaOTPActivity extends AppCompatActivity {
    TextView tv_donthave, tv_register_here, activity_headding;
    TextInputLayout userNameTil;
    EditText et_username;
    Button bt_login, bt_cancel;
    Typeface typeface, typeface2;
    UserSessionManager session;
    NetworkStatus ns;
    Boolean isOnline = false;
    ProgressDialog progressDialog;
    AlertDialog.Builder alertDialogBuilder;
    String token;
    String loginIntent = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_dialog);

        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typeface2 = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        session = new UserSessionManager(getApplicationContext());


        ns = new NetworkStatus();
        isOnline = ns.isOnline(LoginViaOTPActivity.this);

        alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());

        progressDialog = new ProgressDialog(LoginViaOTPActivity.this, R.style.DialogTheme);
        progressDialog.setMessage("Please wait......");

        ConnectivityManager connManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        userNameTil = (TextInputLayout) findViewById(R.id.login_et_user_nametil);

        activity_headding = (TextView) findViewById(R.id.activity_headding);
        activity_headding.setTypeface(typeface2);

        tv_donthave = (TextView) findViewById(R.id.login_donthaveaccount);
        tv_donthave.setTypeface(typeface);

        tv_register_here = (TextView) findViewById(R.id.registerhere);
        tv_register_here.setTypeface(typeface);

        et_username = (EditText) findViewById(R.id.login_et_user_name);
        et_username.setTypeface(typeface);

        bt_login = (Button) findViewById(R.id.submit_login);
        bt_login.setTypeface(typeface);

        bt_cancel = (Button) findViewById(R.id.submit_cancel);
        bt_cancel.setTypeface(typeface);

        if (getIntent() != null) {
            if (getIntent().getStringExtra("Login") != null) {
                loginIntent = getIntent().getStringExtra("Login");
            }
        }

        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        getToken();


        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ((et_username.getText().toString().trim().length() > 0)) {

                    if (isOnline) {
                        progressDialog.show();

                        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SEND_OTP_BEFORE_LOGIN, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("JSONDATAVALUES", response);
                                progressDialog.dismiss();
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String statusCode = jsonObject.getString("status");

                                    if (statusCode.equalsIgnoreCase("10100")) {
                                        Toast.makeText(getApplicationContext(), "Please verify your account by entering OTP....!", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getApplicationContext(), AccountVerificationViaOtp.class);
                                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                        String email = jsonObject1.getString("email");
                                        String user_id = jsonObject1.getString("user_id");
                                        intent.putExtra("userId", user_id);
                                        intent.putExtra("email", email);
                                        startActivity(intent);
                                        finish();
                                    }

                                    if (statusCode.equalsIgnoreCase("10300")) {
                                        Toast.makeText(getApplicationContext(), "Please register", Toast.LENGTH_SHORT).show();

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                        progressDialog.dismiss();

                                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                        } else if (error instanceof AuthFailureError) {

                                        } else if (error instanceof ServerError) {

                                        } else if (error instanceof NetworkError) {

                                        } else if (error instanceof ParseError) {

                                        }

                                    }
                                }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("user_name", et_username.getText().toString().trim());
                                Log.d("LOGINPARAMS", params.toString());
                                return params;
                            }
                        };
                        RequestQueue requestQueue = Volley.newRequestQueue(LoginViaOTPActivity.this);
                        requestQueue.add(stringRequest);
                    } else {
                        showInternetStatus();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please provide all the details..!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        tv_register_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                intent.putExtra("Login", "other");
                startActivity(intent);
            }
        });
    }

    private void showInternetStatus() {

        LayoutInflater inflater = getLayoutInflater();
        View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
        TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
        textView.setTypeface(typeface);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(toastLayout);
        toast.show();
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    public void getToken() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        if (!TextUtils.isEmpty(regId)) {
            token = regId;
            Log.d("TOKENVALUE", token);
        }
    }
}
