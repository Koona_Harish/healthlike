package innasoft.com.mhcone.activities;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.fragments.BookAppointmentFragment;
import innasoft.com.mhcone.fragments.TestSubTestFragment;
import innasoft.com.mhcone.utilities.CustomTypefaceSpan;
import innasoft.com.mhcone.utilities.NetworkStatus;

public class Static2Activity extends AppCompatActivity implements View.OnClickListener{
    private DrawerLayout mDrawerLayout;
    TextView nameProfile,registerAction;
    Typeface typeface;
    NetworkStatus ns;
    Boolean isOnline = false;
    Toast toast;

    private Boolean exit = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static2);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        typeface = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        View navigationheadderView = navigationView.getHeaderView(0);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        Menu m = navigationView.getMenu();
        for (int i=0;i<m.size();i++) {
            MenuItem mi = m.getItem(i);

            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }
            applyFontToMenuItem(mi);
        }

        nameProfile = (TextView) navigationheadderView.findViewById(R.id.login);
        nameProfile.setOnClickListener(this);

        registerAction = (TextView) navigationheadderView.findViewById(R.id.register_action);
        registerAction.setOnClickListener(this);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        if (viewPager != null) {
            setupViewPager(viewPager);
        }

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private void setNightMode(@AppCompatDelegate.NightMode int nightMode) {
        AppCompatDelegate.setDefaultNightMode(nightMode);

        if (Build.VERSION.SDK_INT >= 11) {
            recreate();
        }
    }

    private void applyFontToMenuItem(MenuItem mi) {

        Typeface font = Typeface.createFromAsset(getAssets(), "latobold.ttf");

        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    private void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(new BookAppointmentFragment(), "Packages");
        adapter.addFragment(new TestSubTestFragment(), "Tests");

        viewPager.setAdapter(adapter);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();


                        ns = new NetworkStatus();
                        isOnline = ns.isOnline(Static2Activity.this);
                      /*  if (isOnline) {*/


                            if (menuItem.getItemId() == R.id.nav_item_inbox) {


                                Intent intent = new Intent(getApplicationContext(), Static2Activity.class);
                                startActivity(intent);
                            }



                            if (menuItem.getItemId() == R.id.nav_item_view_Packages) {


                                Intent intent = new Intent(getApplicationContext(), StaticLogin.class);
                                intent.putExtra("getMessage", "View Package");
                                startActivity(intent);
                            }


                            if (menuItem.getItemId() == R.id.nav_item_habits_filter) {


                                Intent intent = new Intent(getApplicationContext(), StaticLogin.class);
                                intent.putExtra("getMessage", "Habits Notifications Update ");
                                startActivity(intent);
                            }

                            if (menuItem.getItemId() == R.id.nav_item_myappointments) {


                                Intent intent = new Intent(getApplicationContext(), StaticLogin.class);
                                intent.putExtra("getMessage", "My Appointments");
                                startActivity(intent);
                            }


                            if (menuItem.getItemId() == R.id.nav_item_Packages) {


                                Intent intent = new Intent(getApplicationContext(), StaticLogin.class);
                                intent.putExtra("getMessage", "My Packages");
                                startActivity(intent);
                            }



                            if (menuItem.getItemId() == R.id.nav_item_view_user) {



                                Intent intent = new Intent(getApplicationContext(), StaticLogin.class);
                                intent.putExtra("getMessage", "I Care For");
                                startActivity(intent);
                            }

                            if (menuItem.getItemId() == R.id.nav_item_habits) {


                                Intent intent = new Intent(getApplicationContext(), StaticLogin.class);
                                intent.putExtra("getMessage", "Habits Info");
                                startActivity(intent);
                            }


                            if (menuItem.getItemId() == R.id.nav_item_share) {
                                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                                sharingIntent.setType("text/plain");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "My Health");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=innasoft.com.mhcone&hl=en");
                                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                            }

                            if (menuItem.getItemId() == R.id.nav_item_about) {
                                Intent intent = new Intent(getApplicationContext(), AboutUs.class);
                                startActivity(intent);
                            }

                            if (menuItem.getItemId() == R.id.nav_item_rate) {

                                Uri uri = Uri.parse("market://details?id=innasoft.com.mhcone&hl=en");
                                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);

                                try {
                                    startActivity(goToMarket);

                                } catch (ActivityNotFoundException e) {
                                    startActivity(new Intent(Intent.ACTION_VIEW,
                                            Uri.parse("https://play.google.com/store/apps/details?id=innasoft.com.mhcone&hl=en")));
                                }

                            }
                       /* }else {
                            showInternetStatus();

                        }*/
                        return false;
                    }
                });
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }



    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.login:
/*
                if (isOnline) {*/

                    nameProfile.setTextColor(Color.parseColor("#43A047"));
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
               /* }else {
                    showInternetStatus();
                }*/
                break;
            case R.id.register_action:
                /*if (isOnline) {*/
                    registerAction.setTextColor(Color.parseColor("#43A047"));
                    Intent intent1 = new Intent(getApplicationContext(), RegisterActivity.class);
                    startActivity(intent1);
              /*  }else {
                    showInternetStatus();
                }*/
                break;
        }
    }


    private void showInternetStatus()
    {

        LayoutInflater inflater = getLayoutInflater();
        View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
        TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
        textView.setTypeface(typeface);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(toastLayout);
        toast.show();



    }

    @Override
    public void onBackPressed() {

        FragmentManager manager = getSupportFragmentManager();

        if (exit) {


            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

            toast.cancel();

            moveTaskToBack(true);



        } else {

            LayoutInflater inflater = getLayoutInflater();
            View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
            TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
            textView.setTypeface(typeface);
            textView.setText("Press Back again to Exit.");
            toast = new Toast(getApplicationContext());

            manager.popBackStack();

            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(toastLayout);
            toast.show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);

        }

    }
}
