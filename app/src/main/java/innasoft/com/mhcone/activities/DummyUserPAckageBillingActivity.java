package innasoft.com.mhcone.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class DummyUserPAckageBillingActivity extends AppCompatActivity implements  View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    TextView txt_package_name,user_package_name,user_package_name_txt, user_package_test_name,user_package_test_name_txt,user_package_lab_name
            ,user_package_lab_name_txt,user_package_price,user_package_price_txt,user_package_total, user_package_total_txt,user_package_tittle
            ,tv_tele,tv_pal,tv_pay;
    String phoneNumber;
    RelativeLayout row;
    Button btnSelectICareFor,btn_billing_cal,btn_billing_pay,user_package_tele_book_btn,proceed;
    ImageView btnDatePicker, btnTimePicker;
    ImageButton closeWindow;
    int rmdyear,rmdmonth,rmdday,rmdhour,rmdmint;
    TextView txtDate, txtTime;
    private int mYear, mMonth, mDay, mHour, mMinute;
    String sendDate,sendTime;
    int userCount = 0;
    ProgressDialog progressDialog;
    NetworkStatus ns;
    Boolean isOnline = false;
    double dtotal,dgrandtotal;
    DatePickerDialog datePickerDialog;
    UserSessionManager userSessionManager;
    String access_key,disp_userName,disp_email;
    String userID,userMailId,userMobile,userName;
    public static final String TAG = "Payment Status";
    String Lat,Lng,SubTestId,SubTestName,BillingPageValuesIds,PackageName,LabId,LabName,Price, home_collection_charges = null,lab_home_collections = null;
    String status_payment_lab,status_payment_online,status_tele_booking;
    Typeface type_lato,typebold;
    ArrayList<String> selectedItems = new ArrayList<String>();
    Map<String, String> selectedItemmap = new HashMap();
    ArrayList<String> usersListId = new ArrayList<String>();
    CheckBox homecollection_chk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dummy_user_package_billing);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("User Package Payment");

        ns = new NetworkStatus();
        isOnline = ns.isOnline(DummyUserPAckageBillingActivity.this);

        type_lato = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typebold = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        txt_package_name = (TextView) findViewById(R.id.txt_package_name);
        txt_package_name.setTypeface(typebold);

        user_package_tittle = (TextView) findViewById(R.id.user_package_name_txt);
        user_package_tittle.setTypeface(typebold);
        user_package_name = (TextView) findViewById(R.id.user_package_name);
        user_package_name.setTypeface(type_lato);
        user_package_test_name = (TextView) findViewById(R.id.user_package_test_name_txt);
        user_package_test_name.setTypeface(typebold);
        user_package_lab_name = (TextView) findViewById(R.id.user_package_lab_name_txt);
        user_package_lab_name.setTypeface(typebold);
        /*user_package_price = (TextView) findViewById(R.id.user_package_price_txt);
        user_package_price.setTypeface(type_lato);*/
        user_package_total = (TextView) findViewById(R.id.user_package_total_txt);
        user_package_total.setTypeface(typebold);
        tv_tele = (TextView) findViewById(R.id.tv_tele);
        tv_pal = (TextView) findViewById(R.id.tv_pal);
        tv_pay = (TextView) findViewById(R.id.tv_pay);

        Bundle bundle = getIntent().getExtras();
        Lat = bundle.getString("LAT");
        Lng = bundle.getString("LNG");
        SubTestId = bundle.getString("SUBTESTID");
        SubTestName = bundle.getString("SUBTESTNAME");
        BillingPageValuesIds = bundle.getString("BILLINGPAGEVALUESIDS");
        PackageName = bundle.getString("PACKAGENAME");
        LabId = bundle.getString("LABID");
        LabName = bundle.getString("LABNAME");
        Price = bundle.getString("PRICE");

        double labPriceDouble = Double.valueOf(Price);

        dtotal = Double.valueOf(new DecimalFormat("##.###").format(labPriceDouble));
        phoneNumber = bundle.getString("selectedLabPone");

        status_payment_lab = bundle.getString("payment_lab");
        status_payment_online = bundle.getString("payment_online");
        status_tele_booking = bundle.getString("tele_booking");

        progressDialog = new ProgressDialog(DummyUserPAckageBillingActivity.this, R.style.DialogTheme);
        progressDialog.setMessage("Please Wait......");
        progressDialog.setCancelable(false);

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("bundelData");

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = userSessionManager.getUserDetails();
        access_key = user.get(UserSessionManager.KEY_ACCSES);

        disp_userName = user.get(UserSessionManager.USER_NAME);
        disp_email = user.get(UserSessionManager.USER_EMAIL);
        userID = user.get(UserSessionManager.USER_ID);
        userMailId = user.get(UserSessionManager.USER_EMAIL);
        userName = user.get(UserSessionManager.USER_NAME);
        userMobile = user.get(UserSessionManager.USER_MOBILE);

        row = (RelativeLayout) findViewById(R.id.row);

        btn_billing_cal = (Button) findViewById(R.id.user_package_cal_btn);
        btn_billing_cal.setTypeface(type_lato);
        btn_billing_pay = (Button) findViewById(R.id.user_package_pay_btn);
        btn_billing_pay.setTypeface(type_lato);
        user_package_tele_book_btn = (Button) findViewById(R.id.user_package_tele_book_btn);
        user_package_tele_book_btn.setTypeface(type_lato);
        user_package_name_txt = (TextView) findViewById(R.id.user_package_name);
        user_package_name_txt.setTypeface(type_lato);
        user_package_name_txt.setText(PackageName);
        user_package_test_name_txt = (TextView) findViewById(R.id.user_package_test_name);
        user_package_test_name_txt.setTypeface(type_lato);
        user_package_test_name_txt.setText(SubTestName);
        user_package_lab_name_txt = (TextView) findViewById(R.id.user_package_lab_name);
        user_package_lab_name_txt.setTypeface(type_lato);

        homecollection_chk = (CheckBox) findViewById(R.id.homecollection_chk);
        homecollection_chk.setTypeface(type_lato);

        user_package_lab_name_txt.setText(LabName);
        /*user_package_price_txt = (TextView) findViewById(R.id.user_package_price);
        user_package_price_txt.setTypeface(type_lato);
        user_package_price_txt.setText("Rs. "+dtotal);*/
        user_package_total_txt = (TextView) findViewById(R.id.user_package_total);
        user_package_total_txt.setTypeface(type_lato);
        btnDatePicker = (ImageView) findViewById(R.id.user_package_date_btn);
        btnTimePicker = (ImageView) findViewById(R.id.user_package_time_btn);
        btnSelectICareFor = (Button) findViewById(R.id.user_package_icarefor);
        btnSelectICareFor.setTypeface(type_lato);
        txtDate = (TextView) findViewById(R.id.user_package_date);
        txtDate.setTypeface(type_lato);
        txtTime = (TextView) findViewById(R.id.user_package_time);
        txtTime.setTypeface(type_lato);

        closeWindow = (ImageButton) findViewById(R.id.user_package_billing_close);
        closeWindow.setOnClickListener(this);

        btnDatePicker.setOnClickListener(this);
        btnTimePicker.setOnClickListener(this);
        btnSelectICareFor.setOnClickListener(this);
        /*btn_billing_pay.setOnClickListener(this);
        btn_billing_cal.setOnClickListener(this);
        user_package_tele_book_btn.setOnClickListener(this);*/

        proceed = (Button) findViewById(R.id.proceed);
        proceed.setTypeface(type_lato);
        proceed.setOnClickListener(this);

        if(status_payment_lab.equals("0"))
        {
            btn_billing_cal.setVisibility(View.INVISIBLE);
            tv_pal.setVisibility(View.INVISIBLE);
        }else {
            btn_billing_cal.setVisibility(View.VISIBLE);
            tv_pal.setVisibility(View.VISIBLE);
        }

        if(status_tele_booking.equals("0"))
        {
            user_package_tele_book_btn.setVisibility(View.INVISIBLE);
            tv_tele.setVisibility(View.INVISIBLE);
        }else {
            user_package_tele_book_btn.setVisibility(View.VISIBLE);
            tv_tele.setVisibility(View.VISIBLE);
        }

        if(status_payment_online.equals("0"))
        {
            btn_billing_pay.setVisibility(View.INVISIBLE);
            tv_pay.setVisibility(View.INVISIBLE);
        }else {
            btn_billing_pay.setVisibility(View.VISIBLE);
            tv_pay.setVisibility(View.VISIBLE);
        }

        homeCollectionStatus();

        homecollection_chk.setOnCheckedChangeListener(this);
    }

    private void homeCollectionStatus() {
        StringRequest stringRequest2 = new StringRequest(Request.Method.POST, AppUrls.HOME_COLLECTION_STATUS, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                progressDialog.dismiss();

                try {
                    JSONObject jsonObject =new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    if(responceCode.equals("19990"))
                    {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        JSONObject jsonObject2 = jsonObject1.getJSONObject("check-home-collection");
                        lab_home_collections = jsonObject2.getString("lab_home_collections");
                        home_collection_charges = jsonObject2.getString("home_collection_price");
                        homecollection_chk.setText("Home collection Rs."+home_collection_charges);


                    }
                    if(responceCode.equals("18880"))
                    {
                        homecollection_chk.setEnabled(false);
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        JSONObject jsonObject2 = jsonObject1.getJSONObject("check-home-collection");
                        lab_home_collections = jsonObject2.getString("lab_home_collections");
                        home_collection_charges = jsonObject2.getString("home_collection_price");
                        homecollection_chk.setText("Home Collection Not available");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {


                } else if (error instanceof ParseError) {

                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String,String> params = new HashMap<String, String>();
                params.put("test_id", SubTestId);
                params.put("lab_id", LabId);
                return params;
            }
        };
        stringRequest2.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );

        RequestQueue requestQueue2 = Volley.newRequestQueue(DummyUserPAckageBillingActivity.this);
        requestQueue2.add(stringRequest2);
    }

    private void showInternetStatus() {

        LayoutInflater inflater = getLayoutInflater();
        View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
        TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
        textView.setTypeface(type_lato);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(toastLayout);
        toast.show();
    }

    @Override
    public void onClick(View v) {

        if(v == btn_billing_cal)
        {
            if (isOnline) {

                if(home_collection_charges.equals("null") || home_collection_charges.equals(null)){
                    dgrandtotal = dtotal * userCount;
                }else {
                    if(homecollection_chk.isChecked()) {
                        dgrandtotal = dtotal * userCount + Double.valueOf(home_collection_charges);
                    }else {
                        dgrandtotal = dtotal * userCount;
                    }
                }

                if (sendDate != null && sendTime != null && userCount != 0) {

                    btn_billing_cal.setClickable(false);

                    StringRequest stringRequest2 = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>()
                    {
                        @Override
                        public void onResponse(String response)
                        {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject =new JSONObject(response);

                                String responceCode = jsonObject.getString("status");
                                if(responceCode.equals("19999"))
                                {
                                    progressDialog.show();
                                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BILLINGURL,
                                            new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String response) {
                                                    Log.d("BILLINGPAGEERROR", response);

                                                    try {
                                                        JSONObject jsonObject = new JSONObject(response);
                                                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                                        String paymentType = jsonObject1.getString("payment-type");
                                                        String bookingId = jsonObject1.getString("booking-id");
                                                        String message = jsonObject1.getString("msg");
                                                        progressDialog.dismiss();

                                                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                                        Intent intent = new Intent(getApplicationContext(), BookingSuccessPage.class);
                                                        intent.putExtra("rmdyear", rmdyear + "");
                                                        intent.putExtra("rmdmonth", rmdmonth + "");
                                                        intent.putExtra("rmdday", rmdday + "");
                                                        intent.putExtra("rmdhour", rmdhour + "");
                                                        intent.putExtra("rmdmint", rmdmint + "");
                                                        intent.putExtra("descr", "You are book " + LabName + " Appointment");
                                                        intent.putExtra("bookingId", bookingId);
                                                        startActivity(intent);

                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            },
                                            new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {

                                                    progressDialog.dismiss();

                                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                                    } else if (error instanceof AuthFailureError) {

                                                    } else if (error instanceof ServerError) {

                                                    } else if (error instanceof NetworkError) {

                                                    } else if (error instanceof ParseError) {

                                                    }

                                                }
                                            }
                                    ) {
                                        @Override
                                        protected Map<String, String> getParams() throws AuthFailureError {
                                            Map<String, String> params = new HashMap<String, String>();
                                            params.put("user_id", userID);
                                            params.put("lab_id", LabId);
                                            params.put("payment_type", "cal");
                                            params.put("booking_date", sendDate + " " + sendTime);
                                            params.put("user_count", userCount + "");
                                            params.put("price", dtotal+"");
                                            params.put("grand_total", dgrandtotal + "");
                                            params.put("form_type", "test");
                                            params.put("transaction_id", "0");
                                            Log.d("BOOKINGINFO", "USERPACKAGESSBOOKING "+BillingPageValuesIds);
                                            params.put("package_id", BillingPageValuesIds);
                                            params.put("sub_user_id", usersListId.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(",", "-").replaceAll(" ", ""));
                                            params.put("type", "testlab");

                                            /*Log.d("CAL-DATA","\n"+"user_id = "+userID+"\n"+"lab_id = "+LabId+"\n"+"payment_type = "+"cal"+"\n"+"booking_date = "+sendDate + " " + sendTime+"\n"+"user_count = "+userCount + ""+"\n"+"price = "+dtotal+""
                                                  +"\n"+"grand_total = "+dgrandtotal + ""+"\n"+"form_type = "+"test"+"\n"+"transaction_id = "+"0"+"\n"+"package_id = "+BillingPageValuesIds+"\n"+"sub_user_id = "+usersListId.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(",", "-").replaceAll(" ", "")+
                                                  "\n"+"type = "+"testlab");*/
                                            return params;
                                        }
                                    };
                                    stringRequest.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );

                                    RequestQueue requestQueue = Volley.newRequestQueue(DummyUserPAckageBillingActivity.this);
                                    requestQueue.add(stringRequest);
                                }
                                if(responceCode.equals("10140"))
                                {
                                    userSessionManager.logoutUser();
                                    Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);
                                }

                                if(responceCode.equals("10150"))
                                {
                                    userSessionManager.logoutUser();
                                    Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            progressDialog.dismiss();

                            String responseBody = null;
                            try {
                                responseBody = new String( error.networkResponse.data, "utf-8" );
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            Log.d("Purushotham","Error-BILLINGPAGEFORUSERS : "+responseBody);

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    })
                    {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError
                        {
                            Map<String,String> params = new HashMap<String, String>();
                            params.put("user_id",userID);
                            return params;
                        }
                    };
                    stringRequest2.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );

                    RequestQueue requestQueue2 = Volley.newRequestQueue(DummyUserPAckageBillingActivity.this);
                    requestQueue2.add(stringRequest2);

                } else {
                    Toast.makeText(getApplicationContext(), "Please provide all data...!", Toast.LENGTH_SHORT).show();
                }
            }else {
                showInternetStatus();
            }
        }
        if(v == btn_billing_pay)
        {
            if (isOnline) {
                if(home_collection_charges.equals("null") || home_collection_charges.equals(null)){
                    dgrandtotal = dtotal * userCount;
                }else {
                    if(homecollection_chk.isChecked()) {
                        dgrandtotal = dtotal * userCount + Double.valueOf(home_collection_charges);
                    }else {
                        dgrandtotal = dtotal * userCount;
                    }
                }

                if (sendDate != null && sendTime != null && userCount != 0) {

                    if(dgrandtotal != 0.0) {

                        btn_billing_pay.setClickable(false);

                        StringRequest stringRequest2 = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response)
                            {
                                progressDialog.dismiss();

                                try {
                                    JSONObject jsonObject =new JSONObject(response);
                                    String responceCode = jsonObject.getString("status");
                                    if(responceCode.equals("19999"))
                                    {
                                        String getFname = userName;
                                        String getPhone = userMobile;
                                        String getEmail = userMailId;
                                        String getAmt   = dgrandtotal+"";

                                        Intent intent = new Intent(getApplicationContext(), PayMentGateWay.class);
                                        intent.putExtra("FIRST_NAME",getFname);
                                        intent.putExtra("PHONE_NUMBER",getPhone);
                                        intent.putExtra("EMAIL_ADDRESS",getEmail);
                                        intent.putExtra("RECHARGE_AMT",getAmt);
                                        intent.putExtra("rmdyear", rmdyear+"");
                                        intent.putExtra("rmdmonth", rmdmonth+"");
                                        intent.putExtra("rmdday", rmdday+"");
                                        intent.putExtra("rmdhour", rmdhour+"");
                                        intent.putExtra("rmdmint", rmdmint+"");
                                        intent.putExtra("descr", LabName);
                                        intent.putExtra("user_id", userID);
                                        intent.putExtra("lab_id", LabId);
                                        intent.putExtra("payment_type", "payment");
                                        intent.putExtra("booking_date", sendDate + " " + sendTime);
                                        intent.putExtra("user_count", userCount + "");
                                        intent.putExtra("price", dtotal+"");
                                        intent.putExtra("grand_total", dgrandtotal + "");
                                        intent.putExtra("form_type", "test");
                                        intent.putExtra("package_id", BillingPageValuesIds);
                                        intent.putExtra("sub_user_id", usersListId.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(",", "-").replaceAll(" ", ""));
                                        intent.putExtra("type", "testlab");
                                        startActivity(intent);
                                    }
                                    if(responceCode.equals("10140"))
                                    {
                                        userSessionManager.logoutUser();
                                        Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        startActivity(intent);
                                    }

                                    if(responceCode.equals("10150"))
                                    {
                                        userSessionManager.logoutUser();
                                        Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        startActivity(intent);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error)
                            {
                                progressDialog.dismiss();
                                String responseBody = null;

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        })
                        {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError
                            {
                                Map<String,String> params = new HashMap<String, String>();
                                params.put("user_id",userID);
                                return params;
                            }
                        };
                        stringRequest2.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );

                        RequestQueue requestQueue2 = Volley.newRequestQueue(DummyUserPAckageBillingActivity.this);
                        requestQueue2.add(stringRequest2);

                    }else {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Your are not allowed to pay Rs.0/- ", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please provide all data...!", Toast.LENGTH_SHORT).show();
                }
            }else {
                showInternetStatus();
            }
        }

        if (v == btnDatePicker) {

            final java.util.Calendar c = java.util.Calendar.getInstance();
            mYear = c.get(java.util.Calendar.YEAR);
            mMonth = c.get(java.util.Calendar.MONTH);
            mDay = c.get(java.util.Calendar.DAY_OF_MONTH);

            datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,int monthOfYear, int dayOfMonth) {

                            rmdyear = year;
                            rmdmonth = monthOfYear+1;
                            rmdday = dayOfMonth;
                            sendDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                            txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        }

                    }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();
        }
        if (v == btnTimePicker) {

            final java.util.Calendar c = java.util.Calendar.getInstance();
            mHour = c.get(java.util.Calendar.HOUR_OF_DAY);
            mMinute = c.get(java.util.Calendar.MINUTE);

            final TimePickerDialog timePickerDialog = new TimePickerDialog(this,android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            rmdhour = Integer.valueOf(String.format("%02d", hourOfDay));
                            rmdmint = Integer.valueOf(String.format("%02d", minute));
                            String hourchange = String.format("%02d", hourOfDay);
                            String minitChange = String.format("%02d", minute);
                            String disptime = updateTime(hourOfDay , minute);
                            sendTime = disptime;
                            txtTime.setText(disptime);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialogInterface) {
                    timePickerDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setVisibility(View.GONE);
                }
            });

            timePickerDialog.show();
        }

        if (v == closeWindow){

            finish();
        }
        if(v == user_package_tele_book_btn)
        {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:"+Uri.encode(phoneNumber.trim())));
            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(callIntent);
        }

        if (v == btnSelectICareFor) {
            if (isOnline) {

                DummyUserPAckageBillingActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        progressDialog.show();
                    }
                });

                StringRequest stringRequest2 = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        try {
                            JSONObject jsonObject =new JSONObject(response);
                            String responceCode = jsonObject.getString("status");
                            if(responceCode.equals("19999"))
                            {
                                userCount = 0;
                                usersListId.clear();

                                final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.VIEW_SUB_USERS_URL,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {

                                                try {
                                                    selectedItems.clear();
                                                    JSONObject jsonObject = new JSONObject(response);
                                                    String successCode = jsonObject.getString("status");
                                                    if (successCode.equals("10190")) {
                                                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                                                        for (int i = 0; i < jsonArray.length(); i++) {
                                                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("sub-users");
                                                            jsonObject1.getString("id");
                                                            jsonObject1.getString("user_id");
                                                            jsonObject1.getString("user_sub_name");
                                                            jsonObject1.getString("relation");
                                                            jsonObject1.getString("age");
                                                            jsonObject1.getString("gender");
                                                            jsonObject1.getString("email");
                                                            jsonObject1.getString("mobile");
                                                            jsonObject1.getString("status");
                                                            jsonObject1.getString("created_time");
                                                            selectedItemmap.put(jsonObject1.getString("user_sub_name"), jsonObject1.getString("id"));
                                                            selectedItems.add(jsonObject1.getString("user_sub_name"));
                                                        }

                                                    }
                                                    selectedItems.add("Self");
                                                    selectedItemmap.put("Self", "0");
                                                    progressDialog.dismiss();
                                                    alertUsersList(selectedItems, selectedItemmap);

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                progressDialog.dismiss();

                                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                                } else if (error instanceof AuthFailureError) {

                                                } else if (error instanceof ServerError) {

                                                } else if (error instanceof NetworkError) {

                                                } else if (error instanceof ParseError) {

                                                }
                                            }
                                        })
                                {
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("user_id", userID);
                                        return params;
                                    }
                                };
                                stringRequest.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );

                                RequestQueue requestQueue = Volley.newRequestQueue(DummyUserPAckageBillingActivity.this);
                                requestQueue.add(stringRequest);
                            }
                            if(responceCode.equals("10140"))
                            {
                                userSessionManager.logoutUser();
                                Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }

                            if(responceCode.equals("10150"))
                            {
                                userSessionManager.logoutUser();
                                Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        progressDialog.dismiss();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                })
                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError
                    {
                        Map<String,String> params = new HashMap<String, String>();
                        params.put("user_id",userID);
                        return params;
                    }
                };
                stringRequest2.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );

                RequestQueue requestQueue2 = Volley.newRequestQueue(DummyUserPAckageBillingActivity.this);
                requestQueue2.add(stringRequest2);

            }else {
                showInternetStatus();
            }
        }

        if (v == proceed) {
            if (isOnline) {

                if (home_collection_charges.equals("null") || home_collection_charges.equals(null)) {
                    dgrandtotal = dtotal * userCount;
                } else {
                    if (homecollection_chk.isChecked()) {
                        dgrandtotal = dtotal * userCount + Double.valueOf(home_collection_charges);
                    } else {
                        dgrandtotal = dtotal * userCount;
                    }
                }

                if (sendDate != null && sendTime != null && userCount != 0) {

                    if (dgrandtotal != 0.0) {

                        String getFname = userName;
                        String getPhone = userMobile;
                        String getEmail = userMailId;
                        String getAmt = dgrandtotal + "";
                        String sub_user_id = usersListId.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(",", "-").replaceAll(" ", "");
                        String booking_date = sendDate + " " + sendTime;
                        dgrandtotal = Double.parseDouble(user_package_total_txt.getText().toString().replace("Rs. ", ""));

                        Intent intent = new Intent(DummyUserPAckageBillingActivity.this, UserPackageConfirmationActivity.class);
                        intent.putExtra("FIRST_NAME", getFname);
                        intent.putExtra("PHONE_NUMBER", getPhone);
                        intent.putExtra("phoneNumber", phoneNumber);
                        intent.putExtra("EMAIL_ADDRESS", getEmail);
                        intent.putExtra("RECHARGE_AMT", getAmt);
                        intent.putExtra("rmdyear", rmdyear + "");
                        intent.putExtra("rmdmonth", rmdmonth + "");
                        intent.putExtra("rmdday", rmdday + "");
                        intent.putExtra("rmdhour", rmdhour + "");
                        intent.putExtra("rmdmint", rmdmint + "");
                        intent.putExtra("descr", LabName);
                        intent.putExtra("user_id", userID);
                        intent.putExtra("lab_id", LabId);
                        intent.putExtra("payment_type", "payment");
                        intent.putExtra("booking_date", booking_date);
                        intent.putExtra("user_count", userCount + "");
                        intent.putExtra("price", dtotal + "");
                        intent.putExtra("grand_total", dgrandtotal + "");
                        intent.putExtra("form_type", "test");
                        intent.putExtra("package_id", BillingPageValuesIds);
                        //intent.putExtra("sub_user_id", sub_user_id);
                        intent.putExtra("sub_user_id", usersListId.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(",", "-").replaceAll(" ", ""));
                        intent.putExtra("type", "testlab");
                        intent.putExtra("labName", LabName);
                        intent.putExtra("sendpackTestandSubIDswithformat", BillingPageValuesIds);

                        intent.putExtra("status_payment_lab", status_payment_lab);
                        intent.putExtra("status_payment_online", status_payment_online);
                        intent.putExtra("status_tele_booking", status_tele_booking);

                        Log.d("BUNDLE", getFname + "\n" + getPhone + "\n" + getEmail + "\n" + getAmt + "\n" + rmdyear + "\n" + rmdmonth + "\n" + rmdday + "\n" + rmdhour + "\n" +
                                rmdmint + "\n" + LabName + "\n" + userID + "\n" + LabId + "\n" + "payment" + "\n" + booking_date + "\n" + userCount + "\n" + dtotal + "\n" + dgrandtotal + "\n" +
                                "test" + "\n" + BillingPageValuesIds + "\n" + sub_user_id + "\n" + "testlab" + "\n" + status_payment_lab + "\n" + status_payment_online + "\n" + status_tele_booking);

                        startActivity(intent);
                    } else {
                        Toast.makeText(this, "Please Provide All Details..!", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Please provide all data...!", Toast.LENGTH_SHORT).show();

                }
            } else {
                showInternetStatus();
            }
        }
    }

    private void showDialogMessage(String message) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(TAG);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void alertUsersList(final ArrayList<String> dataArrayList, final Map<String, String> keyvaluePair) {
        if (dataArrayList.size() != 0) {
            final String[] items = dataArrayList.toArray(new String[dataArrayList.size()]);

            final ArrayList<String> mselectedItems = new ArrayList<>();

            if (!mselectedItems.isEmpty()) {

                mselectedItems.clear();
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(DummyUserPAckageBillingActivity.this);
            userCount = 0;
            builder.setTitle("Choose Your I Care For ")
                    .setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which, boolean isChecked) {

                            if (isChecked) {
                                String selectedItems = items[which];
                                mselectedItems.add(selectedItems);
                                String value = (String) keyvaluePair.get(selectedItems);
                                usersListId.add(value);
                                userCount++;
                            }
                            else {
                                userCount--;
                                mselectedItems.remove(items[which]);
                                String value = (String) keyvaluePair.get(items[which]);
                                usersListId.remove(value);
                            }
                        }
                    })
                    .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            List<String> list = new ArrayList<String>();
                            for (String items : mselectedItems) {
                                list.add(items);
                            }

                            if(list.size() != 0)
                            {
                                btnSelectICareFor.setText(list.toString().replaceAll("\\[", "").replaceAll("\\]",""));
                            }
                            else {
                                Toast.makeText(getApplicationContext(),"Please Select...",Toast.LENGTH_SHORT).show();
                                btnSelectICareFor.setText("Select I Care For");
                            }

                            if(home_collection_charges.equals("null") || home_collection_charges.equals(null) || home_collection_charges.equals("0") ||
                                    home_collection_charges.equals("")){
                                row.setVisibility(View.VISIBLE);
                                String abc = ("Rs. "+dtotal*userCount+"").replace(".0","");

                                user_package_total_txt.setText(abc);
                                homecollection_chk.setClickable(false);
                            }else {
                                Log.d("VALUESCHANGES", (dtotal*userCount+Double.valueOf(home_collection_charges))+"");

                                if(homecollection_chk.isChecked()) {
                                    row.setVisibility(View.VISIBLE);
                                    String abc = ("Rs. " + (dtotal * userCount + Double.valueOf(home_collection_charges)) + "").replace(".0","");
                                    user_package_total_txt.setText(abc);
                                }else {
                                    String abc = ("Rs. "+dtotal*userCount+"").replace(".0","");
                                    row.setVisibility(View.VISIBLE);
                                    user_package_total_txt.setText(abc);
                                }

                            }







                            /*if(home_collection_charges.equals("null") || home_collection_charges.equals(null)){
                                String abc = ("Rs. "+dtotal*userCount+"").replace(".0","");
                                user_package_total_txt.setText(abc);
                                homecollection_chk.setClickable(false);
                            }else {
                                Log.d("VALUESCHANGES", (dtotal*userCount+Double.valueOf(home_collection_charges))+"");

                                if(homecollection_chk.isChecked()) {
                                    String abc = ("Rs. " + (dtotal * userCount + Double.valueOf(home_collection_charges)) + "").replace(".0","");
                                    user_package_total_txt.setText(abc);
                                }else {
                                    String abc = ("Rs. "+dtotal*userCount+"").replace(".0","");
                                    user_package_total_txt.setText(abc);
                                }

                            }*/


                        }
                    });

            DummyUserPAckageBillingActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    progressDialog.dismiss();

                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
        else {
            AlertDialog.Builder builder = new AlertDialog.Builder(DummyUserPAckageBillingActivity.this);
            builder.setMessage("Your I Care for List is Empty...!");
            builder.setTitle("Alert");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });

            DummyUserPAckageBillingActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    progressDialog.dismiss();

                }
            });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    }

    private String updateTime(int hours, int mins) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";

        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);
        String hourss = "";
        if(hours < 10)
            hourss = "0"  + hours;
        else
            hourss = String.valueOf(hours);

        String aTime = new StringBuilder().append(hourss).append(':').append(minutes).append(" ").append(timeSet).toString();

        return aTime;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        if(homecollection_chk == compoundButton)
        {
            if(isChecked)
            {
                row.setVisibility(View.VISIBLE);

                if (btnSelectICareFor.equals("Select I Care For")) {
                    String abc = ("Rs. " + Double.valueOf(home_collection_charges) + "").replace(".0","");
                    user_package_total_txt.setText(abc);
                }else {
                    String abc = ("Rs. " + (dtotal * userCount + Double.valueOf(home_collection_charges)) + "").replace(".0","");
                    user_package_total_txt.setText(abc);
                }
            }else {
                row.setVisibility(View.VISIBLE);
                if (btnSelectICareFor.equals("Select I Care For")) {
                    String abc = ("Rs. " + Double.valueOf(home_collection_charges) + "").replace(".0","");
                    user_package_total_txt.setText(abc);
                }else {
                    String abc = ("Rs. " + (dtotal * userCount)+"").replace(".0","");
                    user_package_total_txt.setText(abc);
                }
            }




            /*if(isChecked)
            {
                user_package_total_txt.setText("Rs. " + (dtotal * userCount + Double.valueOf(home_collection_charges)) + "");
            }
            else {
                user_package_total_txt.setText("Rs. " + (dtotal * userCount)+"");
            }*/
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}