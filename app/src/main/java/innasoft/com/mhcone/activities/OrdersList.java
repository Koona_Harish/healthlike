package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.OrdersListAdapter;
import innasoft.com.mhcone.dbhelper.MyHealthDBHelper;
import innasoft.com.mhcone.holders.OrdersListHolder;
import innasoft.com.mhcone.models.OrdersListModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.DividerItemDecoration;
import innasoft.com.mhcone.utilities.NetworkUtil;
import innasoft.com.mhcone.utilities.UserSessionManager;
import innasoft.com.mhcone.utilities.VerticalSpaceItemDecoration;

public class OrdersList extends AppCompatActivity implements OrdersListHolder.MyOrdersListHolder, SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    ArrayList<OrdersListModel> ordersList;
    OrdersListAdapter adapter;
    RecyclerView ordersListRecyclerview;

    UserSessionManager userSessionManager;
    String access_key,disp_userName,disp_email;
    String userID;
    ProgressDialog progressDialog;
    Typeface type_lato,typebold;
    TextView title,noInternetConnect_b,defaultText_b;
    ImageView failureimage;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private static final int VERTICAL_ITEM_SPACE = 20;
    UserSessionManager session;
    ImageButton cancelOrdersList;

    private boolean statusFlag;

    MyHealthDBHelper myHealthDBHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_list);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Appointments");

        type_lato = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typebold = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        myHealthDBHelper = new MyHealthDBHelper(this);

        session = new UserSessionManager(getApplicationContext());

        title = (TextView) findViewById(R.id.order_tittle);
        title.setTypeface(typebold);

        failureimage = (ImageView) findViewById(R.id.failureimage);

        noInternetConnect_b = (TextView) findViewById(R.id.noInternetConnect_b);
        noInternetConnect_b.setTypeface(type_lato);

        defaultText_b = (TextView) findViewById(R.id.defaultText_b);
        defaultText_b.setTypeface(type_lato);
        defaultText_b.setOnClickListener(this);

        progressDialog = new ProgressDialog(OrdersList.this, R.style.DialogTheme);
        //dialog.setProgressStyle(BIND_ADJUST_WITH_ACTIVITY);

        progressDialog.setMessage("Please wait......");

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = userSessionManager.getUserDetails();
        access_key = user.get(UserSessionManager.KEY_ACCSES);

        disp_userName = user.get(UserSessionManager.USER_NAME);
        disp_email = user.get(UserSessionManager.USER_EMAIL);
        userID = user.get(UserSessionManager.USER_ID);

        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.orders_list_swipe_layout);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,

                android.R.color.holo_green_light,

                android.R.color.holo_orange_light,

                android.R.color.holo_red_light);

        mSwipeRefreshLayout.setOnRefreshListener(this);

        cancelOrdersList = (ImageButton)findViewById(R.id.order_list_btn_close);
        cancelOrdersList.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        ordersList = new ArrayList<OrdersListModel>();

        ordersListRecyclerview = (RecyclerView) findViewById(R.id.order_list_recycler_view);
        ordersListRecyclerview.setHasFixedSize(true);
        ordersListRecyclerview.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        ordersListRecyclerview.addItemDecoration(new DividerItemDecoration(getApplicationContext()));
        ordersListRecyclerview.addItemDecoration(new DividerItemDecoration(getApplicationContext(), R.drawable.recycler_divider));

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        ordersListRecyclerview.setLayoutManager(layoutManager);
        adapter = new OrdersListAdapter(ordersList, OrdersList.this, R.layout.row_orders_list);

        getLabPackages();
    }

    private void getLabPackages() {

        statusFlag = NetworkUtil.isConnected(OrdersList.this);
        if (statusFlag) {
            progressDialog.show();
            noInternetConnect_b.setVisibility(View.INVISIBLE);
            defaultText_b.setVisibility(View.GONE);
            ordersList.clear();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.LISTOFBOOKINGORDERS, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        Log.d("ORDERSLIST", response);
                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        ContentValues values = new ContentValues();

                        if (jsonArray.length() != 0) {
                            myHealthDBHelper.deleteAllOrders();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("order-list");

                                values.put(MyHealthDBHelper.ORDERS_ID, jsonObject1.getString("id"));
                                values.put(MyHealthDBHelper.ORDERS_BOOKING_ID, jsonObject1.getString("booking_id"));
                                values.put(MyHealthDBHelper.ORDERS_BOOKING_DATE, jsonObject1.getString("booking_date"));
                                values.put(MyHealthDBHelper.ORDERS_GRAND_TOTAL, jsonObject1.getString("grand_total"));
                                values.put(MyHealthDBHelper.ORDERS_USER_ID, jsonObject1.getString("user_id"));
                                values.put(MyHealthDBHelper.ORDERS_FORM_TYPE, jsonObject1.getString("form_type"));
                                values.put(MyHealthDBHelper.ORDERS_LAB_ID, jsonObject1.getString("lab_id"));
                                values.put(MyHealthDBHelper.ORDERS_CREATED_TIME, jsonObject1.getString("created_time"));
                                values.put(MyHealthDBHelper.ORDERS_STATUS, jsonObject1.getString("status"));
                                values.put(MyHealthDBHelper.ORDERS_LAB_NAME, jsonObject1.getString("lab_name"));
                                values.put(MyHealthDBHelper.ORDERS_PAYMENT_TYPE, jsonObject1.getString("payment_type"));
                                myHealthDBHelper.addOrdersList(values);

                                /*OrdersListModel olm = new OrdersListModel();
                                olm.setOrders_list_id(jsonObject1.getString("booking_id"));
                                olm.setOrder_list_order_type(jsonObject1.getString("form_type"));
                                olm.setOrders_list_lab_name(jsonObject1.getString("lab_name"));
                                olm.setOrders_list_bookingdate(jsonObject1.getString("booking_date"));
                                olm.setOrders_list_status(jsonObject1.getString("status"));
                                olm.setOrders_list_bookeddate(jsonObject1.getString("created_time"));
                                olm.setOrder_list_price(jsonObject1.getString("grand_total"));

                                jsonObject1.getString("id");
                                jsonObject1.getString("user_id");
                                jsonObject1.getString("lab_id");

                                ordersList.add(olm);*/
                            }
                            //ordersListRecyclerview.setAdapter(adapter);
                            progressDialog.dismiss();
                            mSwipeRefreshLayout.setRefreshing(false);
                            retriveOrderList();
                        } else {
                            progressDialog.dismiss();
                            mSwipeRefreshLayout.setRefreshing(false);
                            failureimage.setVisibility(View.VISIBLE);
                            defaultText_b.setVisibility(View.VISIBLE);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    String responseBody = null;

                    progressDialog.dismiss();
                    mSwipeRefreshLayout.setRefreshing(false);

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", userID);
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);
        }
        else {
            mSwipeRefreshLayout.setRefreshing(false);
            retriveOrderList();
        }
    }

    private void retriveOrderList() {
        ordersList.clear();

        MyHealthDBHelper db = new MyHealthDBHelper(this);
        List<String> order_id = db.getO_ID();
        List<String> order_booking_id = db.getO_BookingID();
        List<String> order_booking_date = db.getO_BookingDate();
        List<String> order_grand_total = db.getO_GrandTotal();
        List<String> order_user_id = db.getO_User_Id();
        List<String> order_form_type = db.getO_Form_Type();
        List<String> order_lab_id = db.getO_Lab_Id();
        List<String> order_created_time = db.getO_Created_time();
        List<String> order_lab_name = db.getO_Lab_Name();
        List<String> order_status = db.getO_Status();
        List<String> payment_type = db.getO_payment_type();

        for (int i = 0; i < order_id.size(); i++){

            OrdersListModel olm = new OrdersListModel();
            olm.setOrders_list_id(order_booking_id.get(i));
            olm.setOrders_list_booking_id(order_id.get(i));
            olm.setOrders_list_bookingdate(order_booking_date.get(i));
            olm.setOrder_list_price(order_grand_total.get(i));
            olm.setOrders_list_user_id(order_user_id.get(i));
            olm.setOrder_list_order_type(order_form_type.get(i));
            olm.setOrders_list_lab_id(order_lab_id.get(i));
            olm.setOrders_list_bookeddate(order_created_time.get(i));
            olm.setOrders_list_lab_name(order_lab_name.get(i));
            olm.setOrders_list_status(order_status.get(i));
            olm.setPayment_type(payment_type.get(i));

            ordersList.add(olm);
        }
        ordersListRecyclerview.setAdapter(adapter);
        db.close();
    }

    @Override
    public void cardViewListener(int position) {

        statusFlag = NetworkUtil.isConnected(OrdersList.this);
        if (statusFlag) {
            String BookingId = ordersList.get(position).getOrders_list_id();
            String booked_date = ordersList.get(position).getOrders_list_bookeddate();
            String booking_date = ordersList.get(position).getOrders_list_bookingdate();
            String price = ordersList.get(position).getOrder_list_price();
            String form_type = ordersList.get(position).getOrder_list_order_type();

            Intent intent = new Intent(getApplicationContext(), BookingInformation.class);

            intent.putExtra("BookingID", BookingId);
            intent.putExtra("booked_date", booked_date);
            intent.putExtra("booking_date", booking_date);
            intent.putExtra("price", price);
            intent.putExtra("form_type", form_type);
            intent.putExtra("payment_type", ordersList.get(position).getPayment_type());
            startActivity(intent);

        }else {
            Snackbar snack = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_SHORT);
            View view = snack.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
            snack.show();
        }

    }

    @Override
    public void onRefresh()
    {
        getLabPackages();
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

        if (v == defaultText_b){

            if(session.checkLogin() != false) {

//                Intent i = new Intent(OrdersList.this, StaticActivity.class);
//                startActivity(i);
//                finish();
//            }
//            else {
                Intent i = new Intent(OrdersList.this, MainActivity.class);
                startActivity(i);
                finish();
            }

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
