package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.ViewMyPackageAdapter;
import innasoft.com.mhcone.models.SubViewMyPackageModel;
import innasoft.com.mhcone.models.ViewMyPackageModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class ViewMyPackage extends AppCompatActivity implements View.OnClickListener {

    ImageButton closeButton;
    TextView txt_test_name,default_txt;
    Typeface typeface,typebold;
    ProgressDialog progressDialog;
    ArrayList<ViewMyPackageModel> viewPackageModel1;
    ViewMyPackageAdapter adapter;
    RecyclerView subTextrecyclerView;
    UserSessionManager userSessionManager;
    String access_key,disp_userName,disp_email;
    String userID;
    ImageView no_packages_found_iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_my_package);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Packages");

        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typebold = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        txt_test_name = (TextView) findViewById(R.id.txt_test_name);
        txt_test_name.setTypeface(typebold);
        default_txt = (TextView) findViewById(R.id.default_txt);
        default_txt.setTypeface(typebold);
        default_txt.setOnClickListener(this);

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = userSessionManager.getUserDetails();
        access_key = user.get(UserSessionManager.KEY_ACCSES);

        no_packages_found_iv = (ImageView) findViewById(R.id.no_packages_found_iv);

        disp_userName = user.get(UserSessionManager.USER_NAME);
        disp_email = user.get(UserSessionManager.USER_EMAIL);
        userID = user.get(UserSessionManager.USER_ID);

        viewPackageModel1 = new ArrayList<ViewMyPackageModel>();
        subTextrecyclerView = (RecyclerView) findViewById(R.id.sub_tests_recycler_view);
        subTextrecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        subTextrecyclerView.setLayoutManager(layoutManager);

        gettingMyPackages();

        closeButton = (ImageButton) findViewById(R.id.btn_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view == closeButton) {

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("19999")) {
//                                    if (userSessionManager.checkLogin() != false) {

//                                        Intent i = new Intent(ViewMyPackage.this, StaticActivity.class);
//                                        startActivity(i);
//                                        finish();
//                                    } else {
                                        Intent i = new Intent(ViewMyPackage.this, MainActivity.class);
                                        startActivity(i);
                                        finish();
//                                    }
                                }
                                if (responceCode.equals("10140")) {

                                    userSessionManager.logoutUser();
                                    Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);
                                }

                                if (responceCode.equals("10150")) {

                                    userSessionManager.logoutUser();
                                    Toast.makeText(getApplicationContext(), "Your Account deleted Successfully.....!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("user_id", userID);
                            return params;
                        }
                    };
                    RequestQueue requestQueue = Volley.newRequestQueue(ViewMyPackage.this);
                    requestQueue.add(stringRequest);
                }
            }
        });

    }

    private void gettingMyPackages() {
        no_packages_found_iv.setVisibility(View.GONE);
        default_txt.setVisibility(View.GONE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equals("19999")) {
                        ViewMyPackage.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.show();

                            }
                        });

                        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_MY_PACKAGES_LIST,
                                new Response.Listener<String>() {

                                    @Override
                                    public void onResponse(String response) {
                                        default_txt.setVisibility(View.GONE);
                                        viewPackageModel1.clear();

                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            String statusCode = jsonObject.getString("status");
                                            if(statusCode.equals("19999")) {
                                                ArrayList<ViewMyPackageModel> predictions = new ArrayList<ViewMyPackageModel>();

                                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("my_package_name");

                                                    ViewMyPackageModel viewMyPackageModel = new ViewMyPackageModel();
                                                    viewMyPackageModel.package_name = jsonObject1.getString("package_name");
                                                    viewMyPackageModel.user_package_id = jsonObject1.getString("user_package_id");
                                                    JSONArray termArray = jsonObject1.getJSONArray("test_wise");
                                                    for (int j = 0; j < termArray.length(); j++) {
                                                        SubViewMyPackageModel subViewMyPackageModel = new SubViewMyPackageModel();

                                                        subViewMyPackageModel.test_id = termArray.getJSONObject(j).getString("test_id");
                                                        subViewMyPackageModel.sub_test_id = termArray.getJSONObject(j).getString("sub_test_id");
                                                        subViewMyPackageModel.sub_test_name = termArray.getJSONObject(j).getString("sub_test_name");

                                                        viewMyPackageModel.tests.add(subViewMyPackageModel);

                                                    }

                                                    predictions.add(viewMyPackageModel);

                                                }
                                                adapter = new ViewMyPackageAdapter(R.layout.row_view_my_package, predictions, ViewMyPackage.this);
                                                subTextrecyclerView.setAdapter(adapter);

                                                ViewMyPackage.this.runOnUiThread(new Runnable() {
                                                    public void run() {
                                                        progressDialog.dismiss();

                                                    }
                                                });
                                            }
                                            if(statusCode.equals("18888"))
                                            {
                                                ViewMyPackage.this.runOnUiThread(new Runnable() {
                                                    public void run() {
                                                        progressDialog.dismiss();
                                                    }
                                                });
                                                no_packages_found_iv.setVisibility(View.VISIBLE);
                                                default_txt.setVisibility(View.VISIBLE);

                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                        progressDialog.dismiss();

                                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                        } else if (error instanceof AuthFailureError) {

                                        } else if (error instanceof ServerError) {

                                        } else if (error instanceof NetworkError) {

                                        } else if (error instanceof ParseError) {

                                        }
                                    }
                                }
                        ){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("user_id", userID);
                                return params;
                            }
                        };
                        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                        requestQueue.add(stringRequest);
                    }
                    if (responceCode.equals("10140")) {

                        userSessionManager.logoutUser();
                        Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }

                    if (responceCode.equals("10150")) {

                        userSessionManager.logoutUser();
                        Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", userID);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(ViewMyPackage.this);
        requestQueue.add(stringRequest);

    }

    public void refreshMyPackageList() {
        adapter.notifyDataSetChanged();

        gettingMyPackages();
    }

    @Override
    public void onClick(View v) {

        if (v == default_txt){

            Intent intent = new Intent(this,CreateMyPackage.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
