package innasoft.com.mhcone.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import innasoft.com.mhcone.R;

public class AboutUs extends AppCompatActivity implements View.OnClickListener {

    Typeface typeface,typeface2;
    TextView about_txt,about_us_tittle,about_us_desp,mail_txt,phone_txt,tc,tc_txt;
    ImageButton closeAboutUsBt;
    ImageView phone_img,mail_img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typeface2 = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("About Healthlike");

        about_txt = (TextView) findViewById(R.id.about_txt);
        about_txt.setTypeface(typeface2);

        about_us_tittle = (TextView) findViewById(R.id.about_us_tittle);
        about_us_tittle.setTypeface(typeface2);

        about_us_desp = (TextView) findViewById(R.id.about_us_desp);
        about_us_desp.setTypeface(typeface);

        mail_img = (ImageView) findViewById(R.id.mail_img);
        mail_img.setOnClickListener(this);

        mail_txt = (TextView) findViewById(R.id.mail);
        mail_txt.setTypeface(typeface2);

        phone_img = (ImageView) findViewById(R.id.phone_img);
        phone_img.setOnClickListener(this);


        phone_txt = (TextView) findViewById(R.id.phone);
        phone_txt.setTypeface(typeface2);
        phone_txt.setOnClickListener(this);

        tc_txt = (TextView) findViewById(R.id.tc_txt);
        tc_txt.setTypeface(typeface);

        tc = (TextView) findViewById(R.id.tc);

        String udata="Terms&Conditions";
        SpannableString content = new SpannableString(udata);
        content.setSpan(new UnderlineSpan(), 0, udata.length(), 0);
        tc.setText(content);

        tc.setOnClickListener(this);

        closeAboutUsBt = (ImageButton) findViewById(R.id.about_btn_close);

        closeAboutUsBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
    }

    @Override
    public void onClick(View v)
    {
        if (v == phone_img)
        {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:"+Uri.encode(phone_txt.getText().toString().trim())));
            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(callIntent);
        }

        if (v == phone_txt)
        {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:"+Uri.encode(phone_txt.getText().toString().trim())));
            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(callIntent);
        }

        if (v == tc)
        {
            tc.setTextColor(Color.parseColor("#43A047"));
            Intent intent = new Intent(getApplicationContext(),TermsConditions.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
