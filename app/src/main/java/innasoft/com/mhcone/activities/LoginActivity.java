package innasoft.com.mhcone.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.FCM.Config;
import innasoft.com.mhcone.utilities.FCM.NotificationUtils;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class LoginActivity extends AppCompatActivity {
    TextView tv_login_forget, tv_donthave, tv_register_here, activity_headding;
    TextInputLayout userNameTil, passwordTil;
    EditText et_username;
    TextInputEditText et_password;
    Button bt_login, bt_cancel;
    Typeface typeface, typeface2;
    UserSessionManager session;
    NetworkStatus ns;
    Boolean isOnline = false;
    ProgressDialog progressDialog;
    AlertDialog.Builder alertDialogBuilder;
    String token;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    String loginIntent = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_dummy);

        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typeface2 = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        session = new UserSessionManager(getApplicationContext());


        ns = new NetworkStatus();
        isOnline = ns.isOnline(LoginActivity.this);

        alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());

        progressDialog = new ProgressDialog(LoginActivity.this, R.style.DialogTheme);
        progressDialog.setMessage("Please wait......");

        ConnectivityManager connManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        userNameTil = (TextInputLayout) findViewById(R.id.login_et_user_nametil);
        passwordTil = (TextInputLayout) findViewById(R.id.login_et_user_passwordtil);

        tv_login_forget = (TextView) findViewById(R.id.login_forgotpassword);
        tv_login_forget.setTypeface(typeface);

        activity_headding = (TextView) findViewById(R.id.activity_headding);
        activity_headding.setTypeface(typeface2);

        tv_donthave = (TextView) findViewById(R.id.login_donthaveaccount);
        tv_donthave.setTypeface(typeface);

        tv_register_here = (TextView) findViewById(R.id.registerhere);
        tv_register_here.setTypeface(typeface);

        et_username = (EditText) findViewById(R.id.login_et_user_name);
        et_username.setTypeface(typeface);

        et_password = (TextInputEditText) findViewById(R.id.login_et_user_password);
        et_password.setTypeface(typeface);

        bt_login = (Button) findViewById(R.id.submit_login);
        bt_login.setTypeface(typeface);

        bt_cancel = (Button) findViewById(R.id.submit_cancel);
        bt_cancel.setTypeface(typeface);

        if (getIntent() != null) {
            if (getIntent().getStringExtra("Login") != null) {
                loginIntent = getIntent().getStringExtra("Login");
            }
        }

        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
        getToken();
        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate()) {
                    if ((et_username.getText().toString().trim().length() > 0) && (et_password.getText().toString().trim().length() > 0)) {

                        if (isOnline) {
                            progressDialog.show();

                            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOGIN_URL, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("JSONDATAVALUES", response);
                                    progressDialog.dismiss();
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String editSuccessResponceCode = jsonObject.getString("status");

                                        if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                            String message = jsonObject1.getString("msg");
                                            String user_id = jsonObject1.getString("user_id");
                                            String user_name = jsonObject1.getString("user_name");
                                            String mobile = jsonObject1.getString("mobile");
                                            String email = jsonObject1.getString("email");
                                            String age = jsonObject1.getString("age");
                                            String key = jsonObject1.getString("key");
                                            session.createUserLoginSession(key, user_id, user_name, mobile, age, email);
                                            //Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                                            Toast.makeText(getApplicationContext(), "You are logged in successfully...!", Toast.LENGTH_SHORT).show();
                                            if (loginIntent.length() > 0 && loginIntent.equalsIgnoreCase("other")) {
                                                finish();
                                            } else {
                                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                intent.putExtra("username", user_name);
                                                intent.putExtra("mobile", mobile);
                                                intent.putExtra("email", email);
                                                intent.putExtra("age", age);
                                                startActivity(intent);
                                            }
                                        }

                                        if (editSuccessResponceCode.equals("10110")) {
                                            progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Password Wrong", Toast.LENGTH_SHORT).show();
                                        }
                                        if (editSuccessResponceCode.equals("10120")) {
                                            progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Invalid username or password", Toast.LENGTH_SHORT).show();
                                        }
                                        if (editSuccessResponceCode.equals("10130")) {
                                            progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Please provide all details....!", Toast.LENGTH_SHORT).show();
                                        }
                                        if (editSuccessResponceCode.equals("10140")) {
                                            progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Admin approval required....!", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                            startActivity(intent);
                                        }
                                        if (editSuccessResponceCode.equals("10150")) {
                                            progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Please verify your account by entering OTP....!", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(getApplicationContext(), AccountVerification.class);
                                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                            String email = jsonObject1.getString("email");
                                            String user_id = jsonObject1.getString("user_id");
                                            intent.putExtra("userId", user_id);
                                            intent.putExtra("email", email);
                                            startActivity(intent);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {

                                            progressDialog.dismiss();

                                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                            } else if (error instanceof AuthFailureError) {

                                            } else if (error instanceof ServerError) {

                                            } else if (error instanceof NetworkError) {

                                            } else if (error instanceof ParseError) {

                                            }

                                        }
                                    }) {
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    Map<String, String> params = new HashMap<String, String>();
                                    Log.d("MYVALUES", et_username.getText().toString().trim() + "\n" + et_password.getText().toString().trim() + "\n" + token);
                                    params.put("mobile", et_username.getText().toString().trim());
                                    params.put("password", et_password.getText().toString().trim());
                                    params.put("token", token);
                                    Log.d("LOGINPARAMS", params.toString());
                                    return params;
                                }
                            };
                            RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                            requestQueue.add(stringRequest);
                        } else {
                            showInternetStatus();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Please provide all the details..!!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        tv_register_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOnline) {
                    if (loginIntent.length() > 0 && loginIntent.equalsIgnoreCase("others")) {
                        Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                        intent.putExtra("Login","other");
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                        startActivity(intent);
                    }
                } else
                    showInternetStatus();
            }
        });
        tv_login_forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOnline) {
                    Intent intent = new Intent(getApplicationContext(), ForgetPassword.class);
                    startActivity(intent);
                } else
                    showInternetStatus();
            }
        });
    }

    private void showInternetStatus() {

        LayoutInflater inflater = getLayoutInflater();
        View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
        TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
        textView.setTypeface(typeface);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(toastLayout);
        toast.show();
    }

    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.sucess : R.drawable.fail);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private boolean validate() {

        boolean result = true;
        String userName = et_username.getText().toString().trim();
        if (userName == null || userName.equals("")) {
            userNameTil.setError(getString(R.string.invalidData));
            result = false;
        } else
            userNameTil.setErrorEnabled(false);

        String psw = et_password.getText().toString().trim();
        if (psw == null || psw.equals("") || psw.length() < 6) {
            passwordTil.setError("Password Wrong");
            result = false;
        } else

            passwordTil.setErrorEnabled(false);

        return result;
    }

    @Override
    public void onBackPressed() {
        if (loginIntent.length() > 0 && loginIntent.equalsIgnoreCase("other"))
            finish();
        else {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }
    }

    public void getToken() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    //Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();


                }
            }
        };

        displayFirebaseRegId();
    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);


        if (!TextUtils.isEmpty(regId)) {
            //  Toast.makeText(getApplicationContext(), "Firebase Reg Id: " + regId, Toast.LENGTH_LONG).show();
            // sendFCMTokes(user_id, user_type, regId);
            token = regId;
            Log.d("TOKENVALUE", token);
        } else {
            // Toast.makeText(getApplicationContext(), "Firebase Reg Id is not received yet!", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);

        super.onPause();
    }
}
