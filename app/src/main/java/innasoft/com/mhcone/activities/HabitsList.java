package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.HabitsListAdapter;
import innasoft.com.mhcone.dbhelper.MyHealthDBHelper;
import innasoft.com.mhcone.models.HabitsListModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.NetworkUtil;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class HabitsList extends AppCompatActivity implements  SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    ArrayList<HabitsListModel> habitslistList;
    HabitsListAdapter adapter;
    RecyclerView habitslistrecyclerview;
    Typeface typeface,typebold;
    TextView habitsListTittle;
    ImageButton HabitsListBtn ;
    String strUserId;
    UserSessionManager userSessionManager;
    String userID;
    TextView defaultText;
    SearchView searchView;
    SearchManager manager;
    SearchView search;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    ProgressDialog progressDialog;

    private boolean statusFlag;
    MyHealthDBHelper myHealthDBHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_habits_list);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Article List");

        typeface = Typeface.createFromAsset(getApplicationContext().getAssets(),"robotoregular.ttf");
        typebold = Typeface.createFromAsset(getApplicationContext().getAssets(),"robotobold.ttf");

        myHealthDBHelper = new MyHealthDBHelper(this);

        habitsListTittle = (TextView) findViewById(R.id.habits_list_txt);
        habitsListTittle.setTypeface(typeface);

        progressDialog = new ProgressDialog(HabitsList.this, R.style.DialogTheme);
        progressDialog.setMessage("Please wait......");

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = userSessionManager.getUserDetails();
        userID = user.get(UserSessionManager.USER_ID);
        defaultText = (TextView) findViewById(R.id.defaultText);
        searchView = (SearchView) findViewById(R.id.mSearchlabs_show_labs_test);

        Bundle bundle = getIntent().getExtras();
        strUserId = bundle.getString("userId");

        HabitsListBtn = (ImageButton) findViewById(R.id.habits_list_cancel_btn);
        HabitsListBtn.setOnClickListener(this);

        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.habits_list_activity_swipe_layout);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,

                android.R.color.holo_green_light,

                android.R.color.holo_orange_light,

                android.R.color.holo_red_light);

        habitslistList = new ArrayList<HabitsListModel>();
        habitslistrecyclerview = (RecyclerView) findViewById(R.id.habits_list_recyclerview);
        habitslistrecyclerview.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        habitslistrecyclerview.setLayoutManager(layoutManager);
        adapter = new HabitsListAdapter(habitslistList, HabitsList.this, R.layout.row_habits_list);
        getHabitsList();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    private void getHabitsList()
    {

        if (statusFlag = NetworkUtil.isConnected(this)) {
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_HABITS_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        habitslistList.clear();

                        defaultText.setVisibility(View.INVISIBLE);

                        JSONObject jsonObject = new JSONObject(response);
                        String responceCode = jsonObject.getString("status");
                        if (responceCode.equals("19999")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            ContentValues values = new ContentValues();
                            myHealthDBHelper.deleteAllHabitsRecords();

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("habit_user");

                                values.put(MyHealthDBHelper.ID, jsonObject1.getString("id"));
                                values.put(MyHealthDBHelper.HABIT_IMAGE, AppUrls.IMAGE_URL + jsonObject1.getString("habit_image"));
                                values.put(MyHealthDBHelper.TITTLE, jsonObject1.getString("title"));
                                values.put(MyHealthDBHelper.HABITS_CATEGORY_ID, jsonObject1.getString("habits_category_id"));
                                values.put(MyHealthDBHelper.HABITS_CATEGORY, jsonObject1.getString("habits_category"));
                                values.put(MyHealthDBHelper.STATUS, jsonObject1.getString("status"));
                                values.put(MyHealthDBHelper.CREATED_TIME, jsonObject1.getString("created_time"));
                                values.put(MyHealthDBHelper.USER_ID, "0");
                                values.put(MyHealthDBHelper.HABITS_DESCRIPTION, jsonObject1.getString("description"));

                                myHealthDBHelper.addHabitsList(values);

                                /*HabitsListModel hlm = new HabitsListModel();
                                hlm.setHabit_id(jsonObject1.getString("id"));
                                hlm.setHabitsName(jsonObject1.getString("title"));
                                hlm.setHabitsCategory(jsonObject1.getString("habits_category"));
                                hlm.setHabitsCategory_id(jsonObject1.getString("habits_category_id"));
                                hlm.setHabitsDescription(jsonObject1.getString("description"));
                                hlm.setHabitsImage(AppUrls.BASE_URL + jsonObject1.getString("habit_image"));

                                habitslistList.add(hlm);
                                progressDialog.dismiss();*/

                            }
                            retriveHabitList();
                            progressDialog.dismiss();
                            mSwipeRefreshLayout.setRefreshing(false);
                            //habitslistrecyclerview.setAdapter(adapter);
                        }
                        if (responceCode.equals("18888")) {
                            progressDialog.dismiss();
                            mSwipeRefreshLayout.setRefreshing(false);
                            defaultText.setText("No articles found.");
                            defaultText.setVisibility(View.VISIBLE);
                        }
                        if (responceCode.equals("10140")) {
                            progressDialog.dismiss();
                            mSwipeRefreshLayout.setRefreshing(false);
                            userSessionManager.logoutUser();
                            Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        }

                        if (responceCode.equals("10150")) {
                            progressDialog.dismiss();
                            mSwipeRefreshLayout.setRefreshing(false);
                            userSessionManager.logoutUser();
                            Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", userID);
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);
        }else {
            retriveHabitList();
        }
    }

    private void retriveHabitList() {
        habitslistList.clear();

        MyHealthDBHelper db = new MyHealthDBHelper(this);
        List<String> habit_category_id = db.getHABITS_CATEGORY_ID();
        List<String> habit_category = db.getHABITS_CATEGORY();
        List<String> habit_image = db.getHabit_Image();
        List<String> id = db.getId();
        List<String> status = db.getStatus();
        List<String> created_time = db.getCreated_Time();
        List<String> habit_description = db.getHabits_Description();
        List<String> user_id = db.getUser_Id();
        List<String> tittle = db.getTittle();

        for (int i = 0; i < habit_category_id.size(); i++){

            HabitsListModel hlm = new HabitsListModel();
            hlm.setHabitsCategory_id(habit_category_id.get(i));
            hlm.setHabitsCategory(habit_category.get(i));
            hlm.setHabitsImage(habit_image.get(i));
            hlm.setHabit_id(id.get(i));
            hlm.setStatus(status.get(i));
            hlm.setCreated_time(created_time.get(i));
            hlm.setHabitsDescription(habit_description.get(i));
            hlm.setUser_id(user_id.get(i));
            hlm.setHabitsName(tittle.get(i));

            habitslistList.add(hlm);
        }
        habitslistrecyclerview.setAdapter(adapter);
        db.close();
    }

    public void cardViewListener(int position)
    {

        Intent intent = new Intent(getApplicationContext(), HabitInformation.class);
        intent.putExtra("habitInfo",habitslistList.get(position));
        startActivity(intent);

    }
    
    @Override
    public void onRefresh() {

        statusFlag = NetworkUtil.isConnected(HabitsList.this);
        if (statusFlag){


            getHabitsList();

        }else {

            mSwipeRefreshLayout.setRefreshing(false);
            habitslistList.clear();
            retriveHabitList();

        }
    }

    @Override
    public void onClick(View v)
    {
        if (v == HabitsListBtn)
        {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.search_article_list, menu);

         manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

         search = (SearchView) menu.findItem(R.id.mSearchlabs_show_labs_test).getActionView();

        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });


        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;



            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

