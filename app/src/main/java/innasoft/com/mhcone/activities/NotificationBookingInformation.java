package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class NotificationBookingInformation extends AppCompatActivity implements View.OnClickListener {
    ProgressDialog progressDialog;
    Typeface type_lato, type_lato2;

    TextView booking_information_tittle_txt, labName_txt, packageName_txt, subTestsName_txt, bookingId_txt, whoBooked_txt, forWho_txt, price, booking_information_booking_date, booking_information_booked_date, booking_information_status_successes;

    TextView package_Name, subTestsName, booking_Id, whoBooked, forWho, price_txt, booking_information_booking_date_txt, booking_information_booked_date_txt;

    Button can_resc_button, resc_button;
    String form_type, package_name = "null", lab_name = "null", test_name = "null";
    String access_key, disp_userName = "", disp_email;
    String booking_date, booked_date, price_disp = "";
    UserSessionManager session;
    ImageButton booking_information_close;
    LinearLayout booked_for_ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_booking_information);


        session = new UserSessionManager(getApplicationContext());

        progressDialog = new ProgressDialog(NotificationBookingInformation.this, R.style.DialogTheme);
        progressDialog.setMessage("Please wait......");
        progressDialog.setCancelable(false);

        type_lato = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        type_lato2 = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        Bundle bundle = getIntent().getExtras();

        final String booking_id = bundle.getString("notification_id");


        package_Name = (TextView) findViewById(R.id.package_name);
        package_Name.setTypeface(type_lato2);

        booking_information_close = (ImageButton) findViewById(R.id.booking_information_close);
        booking_information_close.setOnClickListener(this);

        booked_for_ll = (LinearLayout) findViewById(R.id.booked_for_ll);

        subTestsName = (TextView) findViewById(R.id.sub_tests);
        subTestsName.setTypeface(type_lato2);

        booking_Id = (TextView) findViewById(R.id.booking_id);
        booking_Id.setTypeface(type_lato2);

        whoBooked = (TextView) findViewById(R.id.who_booked);
        whoBooked.setTypeface(type_lato2);

        forWho = (TextView) findViewById(R.id.for_who);
        forWho.setTypeface(type_lato2);


        can_resc_button = (Button) findViewById(R.id.can_resc_button);
        can_resc_button.setTypeface(type_lato2);

        resc_button = (Button) findViewById(R.id.resc_button);
        resc_button.setTypeface(type_lato2);

        booking_information_booking_date_txt = (TextView) findViewById(R.id.booking_date);
        booking_information_booking_date_txt.setTypeface(type_lato2);

        booking_information_booked_date_txt = (TextView) findViewById(R.id.booked_date);
        booking_information_booked_date_txt.setTypeface(type_lato2);

        price_txt = (TextView) findViewById(R.id.price);
        price_txt.setTypeface(type_lato2);

        booking_information_status_successes = (TextView) findViewById(R.id.booking_information_status_successes);
        booking_information_status_successes.setTypeface(type_lato);


        booking_information_tittle_txt = (TextView) findViewById(R.id.booking_information_tittle);
        booking_information_tittle_txt.setTypeface(type_lato2);

        labName_txt = (TextView) findViewById(R.id.booking_information_lab_name);
        labName_txt.setTypeface(type_lato2);
        packageName_txt = (TextView) findViewById(R.id.booking_information_package_name);
        packageName_txt.setTypeface(type_lato);
        subTestsName_txt = (TextView) findViewById(R.id.booking_information_sub_tests);
        subTestsName_txt.setTypeface(type_lato);
        bookingId_txt = (TextView) findViewById(R.id.booking_information_booking_id);
        bookingId_txt.setTypeface(type_lato);
        whoBooked_txt = (TextView) findViewById(R.id.booking_information_who_booked);
        whoBooked_txt.setTypeface(type_lato);
        forWho_txt = (TextView) findViewById(R.id.booking_information_for_who);
        forWho_txt.setTypeface(type_lato);
        booking_information_booking_date = (TextView) findViewById(R.id.booking_information_booking_date);
        booking_information_booking_date.setTypeface(type_lato);
        booking_information_booked_date = (TextView) findViewById(R.id.booking_information_booked_date);
        booking_information_booked_date.setTypeface(type_lato);

        price = (TextView) findViewById(R.id.booking_information_price);
        price.setTypeface(type_lato);


        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.BOOKING_INFORMATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("REPONCEMY", response);
                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                            JSONObject jsonObject2 = jsonObject1.getJSONObject("booking-order-list");
                            jsonObject2.getString("lab_id");
                            jsonObject2.getString("package_id");
                            price_disp = jsonObject2.getString("grand_total");
                            jsonObject2.getString("test_id");
                            jsonObject2.getString("sub_test_id");
                            String status = jsonObject2.getString("status");
                            String cancel_status = jsonObject2.getString("cancel_status");
                            String reschedule_status = jsonObject2.getString("reschedule_status");
                            Log.d("CANCELSTATUS", cancel_status);
                            if (cancel_status.equals("1")) {
                                can_resc_button.setVisibility(View.VISIBLE);
                                // can_resc_button.setBackgroundColor(Color.parseColor("#D3D3D3"));
                                //can_resc_button.setTextColor(Color.parseColor("#000000"));
                                can_resc_button.setClickable(true);
                            } else {
                                can_resc_button.setVisibility(View.GONE);
                            }
                            if (reschedule_status.equals("0")) {
                                resc_button.setVisibility(View.GONE);
                                resc_button.setBackgroundColor(Color.parseColor("#D3D3D3"));
                                resc_button.setTextColor(Color.parseColor("#000000"));
                                resc_button.setClickable(false);
                            } else {
                                resc_button.setVisibility(View.VISIBLE);
                            }
                            if (status.equals("2")) {
                                booking_information_status_successes.setVisibility(View.VISIBLE);
                            }
                            if (!jsonObject2.getString("package_name").equals("null")) {
                                package_name = jsonObject2.getString("package_name");
                                packageName_txt.setText(" " + package_name);
                            } else {
                                package_Name.setVisibility(View.GONE);
                                packageName_txt.setVisibility(View.GONE);
                            }
                            if (!jsonObject2.getString("sub_test").equals("null")) {
                                test_name = jsonObject2.getString("sub_test");
                                subTestsName_txt.setText(test_name);
                            } else {
                                subTestsName_txt.setVisibility(View.GONE);
                            }
                            lab_name = jsonObject2.getString("lab_name");
                            labName_txt.setText(lab_name);

                            booking_date = jsonObject2.getString("booking_date");

                            JSONObject jsonObject3 = jsonObject.getJSONObject("user-data");
                            JSONObject jsonObject4 = jsonObject3.getJSONObject("booking-user-list");
                            jsonObject4.getString("booking_id");
                            jsonObject4.getString("user_id");
                            bookingId_txt.setText(jsonObject4.getString("booking_id"));
                            whoBooked_txt.setText(jsonObject4.getString("user_name"));
                            String renameDispName = jsonObject4.getString("user_sub_name");
                            String finalDispNames = "";
                            if (session.checkLogin() != false) {
                                finalDispNames = renameDispName.replaceAll("/self/", disp_userName);

                            } else {
                                HashMap<String, String> user = session.getUserDetails();
                                disp_userName = user.get(UserSessionManager.USER_NAME);
                                finalDispNames = renameDispName.replaceAll("/self/", disp_userName);
                            }
                            if (renameDispName.equals(null)||renameDispName.equals("null")||renameDispName.equals("")){
                                booked_for_ll.setVisibility(View.GONE);
                            }else {
                                booked_for_ll.setVisibility(View.VISIBLE);
                                forWho_txt.setText(finalDispNames);
                            }
                            booking_information_booking_date.setText(booking_date);
                            booking_information_booked_date.setText(booked_date);
                            price.setText(price_disp);
                            progressDialog.cancel();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressDialog.cancel();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.cancel();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("booking_id", booking_id);
                return params;
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(NotificationBookingInformation.this);
        requestQueue.add(stringRequest);

    }

    @Override
    public void onBackPressed() {
//        if (session.checkLogin() != false) {

//            Intent i = new Intent(NotificationBookingInformation.this, StaticActivity.class);
//            startActivity(i);
//            finish();
//        } else {
            Intent i = new Intent(NotificationBookingInformation.this, MainActivity.class);
            startActivity(i);
            finish();
//        }
    }

    @Override
    public void onClick(View v) {
        if (v == booking_information_close) {
//            if (session.checkLogin() != false) {

//                Intent i = new Intent(NotificationBookingInformation.this, StaticActivity.class);
//                startActivity(i);
//                finish();
//            } else {
                Intent i = new Intent(NotificationBookingInformation.this, MainActivity.class);
                startActivity(i);
                finish();
//            }
        }
    }
}
