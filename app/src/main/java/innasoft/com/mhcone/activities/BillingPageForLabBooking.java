package innasoft.com.mhcone.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.UserSessionManager;

/*import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;*/

public class BillingPageForLabBooking extends AppCompatActivity implements View.OnClickListener {

    TextView book_lab_lab_name, billing_lab_name_txt,book_lab_headding,tv_tele,tv_book;

    ProgressDialog progressDialog;
    Button btnDatePicker, btnTimePicker, btnSelectICareFor, bookLab, btn_tele_book;
    ImageButton closeWindow;
    TextView txtDate, txtTime;
    private int mYear, mMonth, mDay, mHour, mMinute;

    UserSessionManager userSessionManager;
    String access_key,disp_userName,disp_email;
    String userID;
    Typeface type_lato,typebold;
    String sendDate,sendTime;
    int userCount = 0;
    NetworkStatus ns;
    Boolean isOnline = false;
    DatePickerDialog datePickerDialog;

    String labId, labName, packageName;

    ArrayList<String> selectedItems = new ArrayList<String>();
    Map<String, String> selectedItemmap = new HashMap();
    ArrayList<String> usersListId = new ArrayList<String>();
    int rmdyear,rmdmonth,rmdday,rmdhour,rmdmint;
    String phoneNumber;
    String status_payment_lab,status_payment_online,status_tele_booking;

    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing_page_for_lab_booking2);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Pyment");

        type_lato = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typebold = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        book_lab_headding = (TextView) findViewById(R.id.txt_package_name);
        book_lab_headding.setTypeface(type_lato);

        book_lab_lab_name = (TextView) findViewById(R.id.book_lab_lab_name);
        book_lab_lab_name.setTypeface(type_lato);

        ns = new NetworkStatus();
        isOnline = ns.isOnline(BillingPageForLabBooking.this);

        Bundle bundle = getIntent().getExtras();
        labId = bundle.getString("selectedLabId");
        labName = bundle.getString("selectedLabName");
        phoneNumber = bundle.getString("selectedLabPhone");
        status_payment_lab = bundle.getString("payment_lab");
        status_payment_online = bundle.getString("payment_online");
        status_tele_booking = bundle.getString("tele_booking");

        tv_tele = (TextView) findViewById(R.id.tv_tele);
        tv_book = (TextView) findViewById(R.id.tv_book);

        bookLab = (Button) findViewById(R.id.btn_billing);
        bookLab.setTypeface(type_lato);
        btn_tele_book = (Button) findViewById(R.id.btn_tele_book);
        btn_tele_book.setTypeface(type_lato);

        progressDialog = new ProgressDialog(BillingPageForLabBooking.this, R.style.DialogTheme);
        progressDialog.setMessage("Please Wait......");
        progressDialog.setCancelable(false);

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = userSessionManager.getUserDetails();
        access_key = user.get(UserSessionManager.KEY_ACCSES);

        disp_userName = user.get(UserSessionManager.USER_NAME);
        disp_email = user.get(UserSessionManager.USER_EMAIL);
        userID = user.get(UserSessionManager.USER_ID);

        billing_lab_name_txt = (TextView) findViewById(R.id.booklab_lab_name);
        billing_lab_name_txt.setTypeface(type_lato);
        billing_lab_name_txt.setText(labName);

        btnDatePicker = (Button) findViewById(R.id.btn_date);
        btnTimePicker = (Button) findViewById(R.id.btn_time);
        btnSelectICareFor = (Button) findViewById(R.id.booklab_btn_select_icarefor);
        btnSelectICareFor.setTypeface(type_lato);
        txtDate = (TextView) findViewById(R.id.booklab_in_date);
        txtDate.setTypeface(type_lato);
        txtTime = (TextView) findViewById(R.id.booklab_in_time);
        txtTime.setTypeface(type_lato);
        closeWindow = (ImageButton) findViewById(R.id.billing_close);
        closeWindow.setOnClickListener(this);

        btnDatePicker.setOnClickListener(this);
        btnTimePicker.setOnClickListener(this);
        btnSelectICareFor.setOnClickListener(this);
        bookLab.setOnClickListener(this);
        btn_tele_book.setOnClickListener(this);

        if(status_payment_lab.equals("0"))
        {
            bookLab.setVisibility(View.INVISIBLE);
            tv_book.setVisibility(View.INVISIBLE);
        }else {
            bookLab.setVisibility(View.VISIBLE);
            tv_book.setVisibility(View.VISIBLE);
        }

        if(status_tele_booking.equals("0"))
        {
            btn_tele_book.setVisibility(View.INVISIBLE);
            tv_tele.setVisibility(View.INVISIBLE);
        }else {
            btn_tele_book.setVisibility(View.VISIBLE);
            tv_tele.setVisibility(View.VISIBLE);
        }
        /*client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();*/
    }

    @Override
    public void onClick(View v) {


        if(v == bookLab)
        {
            if(sendDate != null && sendTime != null && userCount !=0)
            {
                bookLab.setClickable(false);

                StringRequest stringRequest2 = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject =new JSONObject(response);
                            String responceCode = jsonObject.getString("status");
                            if(responceCode.equals("19999"))
                            {
                                progressDialog.show();
                                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BILLINGURL,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {

                                                try {
                                                    JSONObject jsonObject = new JSONObject(response);
                                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                                    String paymentType = jsonObject1.getString("payment-type");
                                                    String bookingId = jsonObject1.getString("booking-id");
                                                    String message = jsonObject1.getString("msg");
                                                    progressDialog.dismiss();

                                                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                                    Intent intent = new Intent(getApplicationContext(), BookingSuccessPage.class);
                                                    intent.putExtra("bookingId", bookingId);
                                                    intent.putExtra("rmdyear", rmdyear+"");
                                                    intent.putExtra("rmdmonth", rmdmonth+"");
                                                    intent.putExtra("rmdday", rmdday+"");
                                                    intent.putExtra("rmdhour", rmdhour+"");
                                                    intent.putExtra("rmdmint", rmdmint+"");
                                                    intent.putExtra("descr", "You are book "+labName+" Appointment");
                                                    startActivity(intent);

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {

                                                progressDialog.dismiss();

                                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                                } else if (error instanceof AuthFailureError) {

                                                } else if (error instanceof ServerError) {

                                                } else if (error instanceof NetworkError) {

                                                } else if (error instanceof ParseError) {

                                                }

                                            }
                                        })
                                {
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("user_id", userID);
                                        params.put("lab_id", labId);
                                        params.put("payment_type", "appointment");
                                        params.put("booking_date", sendDate+" "+sendTime);
                                        params.put("user_count", userCount+"");
                                        params.put("price", "0");
                                        params.put("grand_total", "0");
                                        params.put("form_type", "booklab");
                                        params.put("transaction_id", "0");

                                        String userIDs = usersListId.toString().replaceAll("\\[", "").replaceAll("\\]","").replaceAll(",", "-").replaceAll(" ", "");

                                        params.put("sub_user_id", usersListId.toString().replaceAll("\\[", "").replaceAll("\\]","").replaceAll(",", "-").replaceAll(" ", ""));
                                        params.put("type", "booklab");
                                        return params;
                                    }
                                };
                                stringRequest.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );
                                RequestQueue requestQueue = Volley.newRequestQueue(BillingPageForLabBooking.this);
                                requestQueue.add(stringRequest);
                            }
                            if(responceCode.equals("10140"))
                            {
                                userSessionManager.logoutUser();
                                Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }

                            if(responceCode.equals("10150"))
                            {
                                userSessionManager.logoutUser();
                                Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        progressDialog.dismiss();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {


                        } else if (error instanceof ParseError) {

                        }
                    }
                })
                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError
                    {
                        Map<String,String> params = new HashMap<String, String>();
                        params.put("user_id",userID);
                        return params;
                    }
                };
                stringRequest2.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );
                RequestQueue requestQueue2 = Volley.newRequestQueue(BillingPageForLabBooking.this);
                requestQueue2.add(stringRequest2);

            }
            else {
                Toast.makeText(getApplicationContext(), "Please provide all data...!", Toast.LENGTH_SHORT).show();
            }
        }

        if (v == btnDatePicker) {

            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,int monthOfYear, int dayOfMonth) {

                            rmdyear = year;
                            rmdmonth = monthOfYear+1;
                            rmdday = dayOfMonth;
                            sendDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                            txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        }

                    }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

            datePickerDialog.show();
        }
        if (v == btnTimePicker) {

            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            final TimePickerDialog timePickerDialog = new TimePickerDialog(this,android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            rmdhour = Integer.valueOf(String.format("%02d", hourOfDay));
                            rmdmint = Integer.valueOf(String.format("%02d", minute));
                            String hourchange = String.format("%02d", hourOfDay);
                            String minitChange = String.format("%02d", minute);
                            String disptime = updateTime(hourOfDay, minute);
                            sendTime = disptime;
                            txtTime.setText(disptime);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialogInterface) {
                    timePickerDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setVisibility(View.GONE);
                }
            });

            timePickerDialog.show();
        }

        if (v == closeWindow) {

            finish();
        }
        if(v == btn_tele_book)
        {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:"+Uri.encode(phoneNumber.trim())));
            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(callIntent);
        }
        if (v == btnSelectICareFor)
        {

            BillingPageForLabBooking.this.runOnUiThread(new Runnable() {
                public void run() {
                    progressDialog.show();

                }
            });

            StringRequest stringRequest2 = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>()
            {
                @Override
                public void onResponse(String response)
                {
                    try {
                        JSONObject jsonObject =new JSONObject(response);
                        String responceCode = jsonObject.getString("status");
                        if(responceCode.equals("19999"))
                        {
                            userCount = 0;
                            usersListId.clear();

                            final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.VIEW_SUB_USERS_URL,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {

                                            try {
                                                selectedItems.clear();
                                                JSONObject jsonObject = new JSONObject(response);
                                                String successCode = jsonObject.getString("status");
                                                if(successCode.equals("10190")) {
                                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                                    for (int i = 0; i < jsonArray.length(); i++) {
                                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("sub-users");
                                                        jsonObject1.getString("id");
                                                        jsonObject1.getString("user_id");
                                                        jsonObject1.getString("user_sub_name");
                                                        jsonObject1.getString("relation");
                                                        jsonObject1.getString("age");
                                                        jsonObject1.getString("gender");
                                                        jsonObject1.getString("email");
                                                        jsonObject1.getString("mobile");
                                                        jsonObject1.getString("status");
                                                        jsonObject1.getString("created_time");
                                                        selectedItemmap.put(jsonObject1.getString("user_sub_name"), jsonObject1.getString("id"));
                                                        selectedItems.add(jsonObject1.getString("user_sub_name"));
                                                    }
                                                }
                                                selectedItems.add("Self");
                                                selectedItemmap.put("Self", "0");
                                                progressDialog.dismiss();
                                                alertUsersList(selectedItems,selectedItemmap);

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            progressDialog.dismiss();

                                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                            } else if (error instanceof AuthFailureError) {

                                            } else if (error instanceof ServerError) {

                                            } else if (error instanceof NetworkError) {

                                            } else if (error instanceof ParseError) {

                                            }
                                        }
                                    })
                            {
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    Map<String, String> params = new HashMap<String, String>();
                                    params.put("user_id", userID);
                                    return params;
                                }
                            };
                            RequestQueue requestQueue = Volley.newRequestQueue(BillingPageForLabBooking.this);
                            requestQueue.add(stringRequest);
                        }
                        if(responceCode.equals("10140"))
                        {
                            userSessionManager.logoutUser();
                            Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        }

                        if(responceCode.equals("10150"))
                        {
                            userSessionManager.logoutUser();
                            Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
            })
            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError
                {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("user_id",userID);
                    return params;
                }
            };
            stringRequest2.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );
            RequestQueue requestQueue2 = Volley.newRequestQueue(BillingPageForLabBooking.this);
            requestQueue2.add(stringRequest2);
        }
    }

    private void showInternetStatus() {
        LayoutInflater inflater = getLayoutInflater();
        View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
        TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(toastLayout);
        toast.show();
    }

    public void alertUsersList(final ArrayList<String> dataArrayList, final Map<String, String> keyvaluePair) {
        if (dataArrayList.size() != 0) {
            final String[] items = dataArrayList.toArray(new String[dataArrayList.size()]);

            final ArrayList<String> mselectedItems = new ArrayList<>();

            if (!mselectedItems.isEmpty()) {

                mselectedItems.clear();
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(BillingPageForLabBooking.this);
            userCount = 0;
            builder.setTitle("Choose You I Care For ")
                    .setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which, boolean isChecked) {

                            if (isChecked) {

                                String selectedItems = items[which];
                                mselectedItems.add(selectedItems);
                                String value = (String) keyvaluePair.get(selectedItems);
                                usersListId.add(value);
                                userCount++;

                            } else {
                                userCount--;
                                mselectedItems.remove(items[which]);
                                String value = (String) keyvaluePair.get(items[which]);
                                usersListId.remove(value);
                            }

                        }
                    })
                    .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            List<String> list = new ArrayList<String>();
                            for (String items : mselectedItems) {
                                list.add(items);
                            }

                            if (list.size() != 0) {
                                btnSelectICareFor.setText(list.toString().replaceAll("\\[", "").replaceAll("\\]", ""));
                            } else {
                                Toast.makeText(getApplicationContext(), "Please Select...", Toast.LENGTH_SHORT).show();
                                btnSelectICareFor.setText("Select I Care For");
                            }

                        }
                    });

            BillingPageForLabBooking.this.runOnUiThread(new Runnable() {
                public void run() {
                    progressDialog.dismiss();

                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();

        }
    }

    private String updateTime(int hours, int mins) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";

        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);
        String hourss = "";
        if (hours < 10)
            hourss = "0" + hours;
        else
            hourss = String.valueOf(hours);

        String aTime = new StringBuilder().append(hourss).append(':').append(minutes).append(" ").append(timeSet).toString();

        return aTime;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    /*@Override
    public void onBackPressed() {
        finish();
    }*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}