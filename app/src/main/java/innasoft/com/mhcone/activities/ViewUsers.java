package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.ViewUsersAdapter;
import innasoft.com.mhcone.dbhelper.MyHealthDBHelper;
import innasoft.com.mhcone.holders.ViewUserHolder;
import innasoft.com.mhcone.models.ViewUsersModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.NetworkUtil;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class ViewUsers extends AppCompatActivity implements ViewUserHolder.MyViewHolder, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    ArrayList<ViewUsersModel> usersModels;
    RecyclerView recyclerView;
    ViewUsersAdapter adapter;
    UserSessionManager userSessionManager;
    String userID;
    ImageButton closeWindow;
    TextView defaultTextValue, txt_package_name;
    ImageView addIcareForButton;
    ProgressDialog progressDialog;
    Typeface typeface, typebold;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private boolean statusFlag;

    MyHealthDBHelper myHealthDBHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.sendbird_slide_in_from_bottom, R.anim.sendbird_slide_out_to_top);
        setContentView(R.layout.activity_view_users);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("I Care For");

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typebold = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        myHealthDBHelper = new MyHealthDBHelper(this);

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = userSessionManager.getUserDetails();
        userID = user.get(UserSessionManager.USER_ID);

        progressDialog = new ProgressDialog(ViewUsers.this, R.style.DialogTheme);
        progressDialog.setMessage("Please wait......");

        txt_package_name = (TextView) findViewById(R.id.txt_package_name);
        txt_package_name.setTypeface(typebold);
        defaultTextValue = (TextView) findViewById(R.id.defaultText);
        defaultTextValue.setTypeface(typeface);
        closeWindow = (ImageButton) findViewById(R.id.view_user_close);
        closeWindow.setOnClickListener(this);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.view_users_activity_swipe_layout);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,

                android.R.color.holo_green_light,

                android.R.color.holo_orange_light,

                android.R.color.holo_red_light);
        usersModels = new ArrayList<ViewUsersModel>();

        addIcareForButton = (ImageView) findViewById(R.id.addIcareFor);
        recyclerView = (RecyclerView) findViewById(R.id.viewusers_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ViewUsersAdapter(usersModels, ViewUsers.this, R.layout.row_sub_users_items);
        getUsers();

        addIcareForButton.setOnClickListener(this);

        mSwipeRefreshLayout.setOnRefreshListener(this);

    }

    private void getUsers() {
        statusFlag = NetworkUtil.isConnected(ViewUsers.this);
        if (statusFlag) {
            usersModels.clear();
            progressDialog.show();
            final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.VIEW_SUB_USERS_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("status");
                                if (successResponceCode.equals("10190")) {
                                    myHealthDBHelper.deleteAllIcare();
                                    defaultTextValue.setVisibility(View.INVISIBLE);

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    ContentValues values = new ContentValues();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("sub-users");

                                        values.put(MyHealthDBHelper.ICARE_ID, jsonObject1.getString("id"));
                                        values.put(MyHealthDBHelper.ICARE_USER_ID, jsonObject1.getString("user_id"));
                                        values.put(MyHealthDBHelper.ICARE_SUBNAME, jsonObject1.getString("user_sub_name"));
                                        values.put(MyHealthDBHelper.ICARE_RELATION, jsonObject1.getString("relation"));
                                        values.put(MyHealthDBHelper.ICARE_DOB, jsonObject1.getString("dob"));
                                        values.put(MyHealthDBHelper.ICARE_AGE, jsonObject1.getString("age"));
                                        values.put(MyHealthDBHelper.ICARE_GENDER, jsonObject1.getString("gender"));
                                        values.put(MyHealthDBHelper.ICARE_EMAIL, jsonObject1.getString("email"));
                                        values.put(MyHealthDBHelper.ICARE_MOBILE, jsonObject1.getString("mobile"));
                                        values.put(MyHealthDBHelper.ICARE_STATUS, jsonObject1.getString("status"));
                                        values.put(MyHealthDBHelper.ICARE_CREATED_TIME, jsonObject1.getString("created_time"));

                                        myHealthDBHelper.addIcareList(values);

                                        /*ViewUsersModel viewUsersModel = new ViewUsersModel();
                                        viewUsersModel.setVu_id(jsonObject1.getString("id"));
                                        viewUsersModel.setVu_user_id(jsonObject1.getString("user_id"));
                                        viewUsersModel.setVu_user_sub_name(jsonObject1.getString("user_sub_name"));
                                        viewUsersModel.setVu_relation(jsonObject1.getString("relation"));
                                        viewUsersModel.setVu_age(jsonObject1.getString("dob"));
                                        viewUsersModel.setVu_gender(jsonObject1.getString("gender"));
                                        viewUsersModel.setVu_email(jsonObject1.getString("email"));
                                        viewUsersModel.setVu_mobile(jsonObject1.getString("mobile"));
                                        viewUsersModel.setVu_status(jsonObject1.getString("status"));
                                        usersModels.add(viewUsersModel);*/
                                    }
                                    //recyclerView.setAdapter(adapter);
                                    progressDialog.dismiss();
                                    mSwipeRefreshLayout.setRefreshing(false);
                                    retrieveIcare();
                                }
                                if (successResponceCode.equals("10180")) {
                                    progressDialog.dismiss();
                                    mSwipeRefreshLayout.setRefreshing(false);
                                    defaultTextValue.setVisibility(View.VISIBLE);
                                }

                                if (successResponceCode.equals("10140")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);
                                }

                                if (successResponceCode.equals("10150")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Your Account Deleted....!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            progressDialog.dismiss();
                            mSwipeRefreshLayout.setRefreshing(false);
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", userID);
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            progressDialog.dismiss();
            mSwipeRefreshLayout.setRefreshing(false);
            retrieveIcare();
        }
    }

    private void retrieveIcare() {
        usersModels.clear();

        MyHealthDBHelper db = new MyHealthDBHelper(this);
        List<String> icare_id = db.getI_ID();
        List<String> icare_user_id = db.getI_UserID();
        List<String> icare_subname = db.getI_Subname();
        List<String> icare_relation = db.getI_Relation();
        List<String> icare_dob = db.getI_Age();
        List<String> icare_age = db.getI_Dob();
        List<String> icare_gender = db.getI_Gender();
        List<String> icare_email = db.getI_Email();
        List<String> icare_mobile = db.getI_Mobile();
        List<String> icare_status = db.getI_Status();
        List<String> icare_created_time = db.getI_Created_Time();

        for (int i = 0; i < icare_id.size(); i++) {

            ViewUsersModel vum = new ViewUsersModel();
            vum.setVu_id(icare_id.get(i));
            vum.setVu_user_id(icare_user_id.get(i));
            vum.setVu_user_sub_name(icare_subname.get(i));
            vum.setVu_relation(icare_relation.get(i));
            vum.setVu_dob(icare_dob.get(i));
            vum.setVu_age(icare_age.get(i));
            vum.setVu_gender(icare_gender.get(i));
            vum.setVu_email(icare_email.get(i));
            vum.setVu_mobile(icare_mobile.get(i));
            vum.setVu_status(icare_status.get(i));
            vum.setVu_created_time(icare_created_time.get(i));

            usersModels.add(vum);
        }
        recyclerView.setAdapter(adapter);
        db.close();
    }

    @Override
    public void cardViewListener(int position) {

    }

    @Override
    public void onClick(View view) {
        if (view == closeWindow) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String responceCode = jsonObject.getString("status");
                        if (responceCode.equals("19999")) {
//                            if (userSessionManager.checkLogin() != false) {

//                                Intent i = new Intent(ViewUsers.this, StaticActivity.class);
//                                startActivity(i);
//                                finish();
//                            } else {
                            Intent i = new Intent(ViewUsers.this, MainActivity.class);
                            startActivity(i);
                            finish();
//                            }
                        }
                        if (responceCode.equals("10140")) {

                            userSessionManager.logoutUser();
                            Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        }

                        if (responceCode.equals("10150")) {

                            userSessionManager.logoutUser();
                            Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", userID);
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(ViewUsers.this);
            requestQueue.add(stringRequest);
        }
      /*  if(view == addIcareForButton)
        {
            Intent intent = new Intent(getApplicationContext(), AddUsers.class);
            startActivity(intent);
        }*/
    }

    public void refreshMyUploadList() {
        adapter.notifyDataSetChanged();


        getUsers();
    }

    @Override
    public void onRefresh() {

        getUsers();
    }

    @Override
    public void onBackPressed() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equals("19999")) {
//                        if (userSessionManager.checkLogin() != false) {

//                            Intent i = new Intent(ViewUsers.this, StaticActivity.class);
//                            startActivity(i);
//                            finish();
//                        } else {
                        Intent i = new Intent(ViewUsers.this, MainActivity.class);
                        startActivity(i);
                        finish();
//                        }
                    }
                    if (responceCode.equals("10140")) {

                        userSessionManager.logoutUser();
                        Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }

                    if (responceCode.equals("10150")) {

                        userSessionManager.logoutUser();
                        Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", userID);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(ViewUsers.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_user_care, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            case R.id.addIcareFor:

                Intent intent = new Intent(getApplicationContext(), AddUsers.class);
                startActivity(intent);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
