package innasoft.com.mhcone.activities;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.PackageLabsTestsAdapter;
import innasoft.com.mhcone.fragments.LabsFragments;
import innasoft.com.mhcone.fragments.PackagessFragment;
import innasoft.com.mhcone.fragments.TestSubTestFragment;


public class TabLayoutActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    ViewPager view_pager_test_pack_lbs;
    TabLayout tabLayout_test_pack_lab;
    Typeface typeface;
    Toolbar toolbar_tab_lyout;
    PackageLabsTestsAdapter adapter;
    String type, test, pack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_layout);

        Bundle b = getIntent().getExtras();

        type = b.getString("condition");

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.robotobold));

        toolbar_tab_lyout = (Toolbar) findViewById(R.id.toolbar_tab_lyout);
        toolbar_tab_lyout.setNavigationIcon(R.drawable.ic_arrow_left_white_24dp);
        toolbar_tab_lyout.setTitleTextColor(Color.parseColor("#FFFFFF"));
        setSupportActionBar(toolbar_tab_lyout);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        view_pager_test_pack_lbs = (ViewPager) findViewById(R.id.view_pager_test_pack_lbs);
        setupViewPager(view_pager_test_pack_lbs);
        tabLayout_test_pack_lab = (TabLayout) findViewById(R.id.tabLayout_test_pack_lab);
        tabLayout_test_pack_lab.setupWithViewPager(view_pager_test_pack_lbs);
        tabLayout_test_pack_lab.setOnTabSelectedListener(this);

    }


    private void setupViewPager(ViewPager view_pager_test_pack_lbs) {

        adapter = new PackageLabsTestsAdapter(getSupportFragmentManager());

        if (type.equals("LABS")) {
            view_pager_test_pack_lbs.setCurrentItem(3);
            adapter.addFrag(new LabsFragments(), "LABS");
            adapter.addFrag(new PackagessFragment(), "PACKAGES");
            adapter.addFrag(new TestSubTestFragment(), "TESTS");
        }

        if (type.equals("PACKAG")) {
            view_pager_test_pack_lbs.setCurrentItem(1);
            adapter.addFrag(new PackagessFragment(), "PACKAGES");
            adapter.addFrag(new TestSubTestFragment(), "TESTS");
            adapter.addFrag(new LabsFragments(), "LABS");
        }

        if (type.equals("TEST")) {
            view_pager_test_pack_lbs.setCurrentItem(2);
            adapter.addFrag(new TestSubTestFragment(), "TESTS");
            adapter.addFrag(new PackagessFragment(), "PACKAGES");
            adapter.addFrag(new LabsFragments(), "LABS");
        }


        // adapter.addFrag(new PackagesFragment(), "PACKAGES");
        // adapter.addFrag(new LabsFragment(), "LABS");
        //   adapter.addFrag(new TestSubTestFragment(),"TESTS");
        view_pager_test_pack_lbs.setAdapter(adapter);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {


    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {


    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
