package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.ShowLabsTestAdapter;
import innasoft.com.mhcone.adapters.ShowLabsTestCitiesAdapter;
import innasoft.com.mhcone.models.CitiesModel;
import innasoft.com.mhcone.models.ShowLabsTestModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.DividerItemDecoration;
import innasoft.com.mhcone.utilities.VerticalSpaceItemDecoration;

public class ShowLabsTest extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    ArrayList<ShowLabsTestModel> showlabstestList;
    ShowLabsTestAdapter adapter;
    RecyclerView showlabstestrecyclerview;
    String lat, lng, sub_testId, sub_testName;
    final static int SHOW_LABS_REQ_CODE = 26;
    ImageButton closeShowLabsTestsBt;
    ProgressDialog progressDialog;
    ArrayList<Object> arraylistsubtestNamesObject;
    ArrayList<Object> arraylistsubtestIdsObject;
    ArrayList<Object> arraylisttestIdsObject;
    private Typeface type_lato, typebold;
    SearchView searchView;
    int filterFlag = 0;
    String filterString;
    int first_time_flag = 0;
    ArrayList<String> labsTestArrayList = new ArrayList<String>();
    RelativeLayout yourpackageRL;
    ImageView yourpackageLocation, no_labs_found_iv;
    TextView yourpackageCity;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    TextView list_of_labs_txt, noInternetConnect;
    private FloatingActionButton fabTests;
    private static final int VERTICAL_ITEM_SPACE = 0;
    AlertDialog dialog;

    Geocoder geocoder;
    List<Address> cuurentaddresses;
    String address = null;
    String city;
    String state;
    String country;
    String pincode;
    double latitude;
    double longitude;

    ArrayList<CitiesModel> citiesModelArrayList = new ArrayList<CitiesModel>();
    ShowLabsTestCitiesAdapter citiesAdapter;
    AlertDialog city_dialog;

    String global_lat, global_lng;
    int fabaction_status = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_labs_test);

        type_lato = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typebold = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        Bundle bundle = getIntent().getExtras();
        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("bundelData");

        arraylistsubtestNamesObject = (ArrayList<Object>) args.getSerializable("arraylistsubtestNames");
        arraylistsubtestIdsObject = (ArrayList<Object>) args.getSerializable("arraylistsubtestIds");
        arraylisttestIdsObject = (ArrayList<Object>) args.getSerializable("arraylisttestIds");

        lat = bundle.getString("latValue");
        lng = bundle.getString("longValue");
        sub_testId = bundle.getString("subTestId");
        sub_testName = bundle.getString("subTestName");

        geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

        latitude = Double.parseDouble(lat);
        longitude = Double.parseDouble(lng);

        try {
            cuurentaddresses = geocoder.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        list_of_labs_txt = (TextView) findViewById(R.id.txt_package_name);
        list_of_labs_txt.setTypeface(typebold);

        noInternetConnect = (TextView) findViewById(R.id.noInternetConnect);
        noInternetConnect.setTypeface(type_lato);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.show_labs_test_swipe_layout);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,

                android.R.color.holo_green_light,

                android.R.color.holo_orange_light,

                android.R.color.holo_red_light);

        mSwipeRefreshLayout.setOnRefreshListener(this);

        fabTests = (FloatingActionButton) findViewById(R.id.filter_tests);
        fabTests.setOnClickListener(this);

        yourpackageRL = (RelativeLayout) findViewById(R.id.yourpackage_rl);
        yourpackageRL.setOnClickListener(this);

        yourpackageLocation = (ImageView) findViewById(R.id.yourpackage_location);
        yourpackageLocation.setOnClickListener(this);
        no_labs_found_iv = (ImageView) findViewById(R.id.no_labs_found_iv);

        yourpackageCity = (TextView) findViewById(R.id.yourpackage_city);
        yourpackageCity.setOnClickListener(this);

        if (cuurentaddresses.size() != 0) {
            city = cuurentaddresses.get(0).getLocality();
            yourpackageCity.setText(city);
        } else {

        }

        searchView = (SearchView) findViewById(R.id.mSearchlabs_show_labs_test);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");

        progressDialog.setProgressStyle(R.style.DialogTheme);

        showlabstestList = new ArrayList<ShowLabsTestModel>();

        showlabstestrecyclerview = (RecyclerView) findViewById(R.id.showlabstest_recycler_view);
        showlabstestrecyclerview.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        showlabstestrecyclerview.setLayoutManager(layoutManager);

        showlabstestrecyclerview.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));

        showlabstestrecyclerview.addItemDecoration(new DividerItemDecoration(getApplicationContext()));

        showlabstestrecyclerview.addItemDecoration(
                new DividerItemDecoration(getApplicationContext(), R.drawable.recycler_divider));

        closeShowLabsTestsBt = (ImageButton) findViewById(R.id.sub_testsshow_lab_btn_close);

        adapter = new ShowLabsTestAdapter(sub_testName, showlabstestList, ShowLabsTest.this, R.layout.row_labs_test_items3, sub_testId, arraylistsubtestNamesObject, arraylistsubtestIdsObject, arraylisttestIdsObject);

        if (filterFlag == 0) {

            filterString = "distance_in_km asc";

            getLabsTest(filterString);
        }

        closeShowLabsTestsBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(SHOW_LABS_REQ_CODE, intent);
                finish();
            }
        });


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                adapter.getFilter().filter(query);
                return false;
            }
        });

    }

    private void getLabsTest(final String filterValue) {
        showlabstestList.clear();
        noInternetConnect.setVisibility(View.GONE);
        no_labs_found_iv.setVisibility(View.GONE);
        ShowLabsTest.this.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.show();
            }
        });
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_LABS_FOR_TESTS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    Log.d("SHOWLABSTESTSRES", response);
                    if (status.equals("19999")) {


                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        if (jsonArray.length() != 0) {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");
                                ShowLabsTestModel sltm = new ShowLabsTestModel();
                                sltm.setShow_lab_id(jsonObject1.getString("id"));
                                sltm.setShow_lab_user_name(jsonObject1.getString("user_name"));
                                sltm.setShow_lab_email(jsonObject1.getString("email"));
                                sltm.setShow_lab_mobile(jsonObject1.getString("mobile"));
                                sltm.setShow_lab_role_id(jsonObject1.getString("role_id"));
                                sltm.setCertificate(jsonObject1.getString("certificate"));
                                sltm.setLocation(jsonObject1.getString("location"));
                                sltm.setProfile_pic_url(jsonObject1.getString("profile_pic"));
                                sltm.setShow_lab_created_time(jsonObject1.getString("created_time"));

                                if (jsonObject1.getString("rating").equals("null")) {
                                    sltm.setShow_lab_ratting("0");
                                } else {
                                    sltm.setShow_lab_ratting(jsonObject1.getString("rating"));
                                }
                                sltm.setShow_lab_payment_lab(jsonObject1.getString("payment_lab"));
                                sltm.setShow_lab_payment_online(jsonObject1.getString("payment_online"));
                                sltm.setShow_lab_tele_booking(jsonObject1.getString("tele_booking"));
                                sltm.setShow_lab_address(jsonObject1.getString("address"));
                                sltm.setShow_lab_lat(jsonObject1.getString("lat"));
                                sltm.setShow_lab_lang(jsonObject1.getString("lang"));
                                sltm.setShow_lab_home_collections(jsonObject1.getString("home_collections"));
                                String workingtimefromServer = jsonObject1.getString("current_week_time");
                                String workingDayfromServer = jsonObject1.getString("current_week_name");

                                String removeSpace = workingtimefromServer.replace(" ", "");
                                String segments[] = removeSpace.split("-");
                                String conditionCheck = segments[segments.length - 1];
                                if (conditionCheck.equals("1")) {
                                    sltm.setShow_lab_workingtime("OpenToday:" + segments[segments.length - 3] + "To" + segments[segments.length - 2]);
                                }
                                if (conditionCheck.equals("0")) {
                                    sltm.setShow_lab_workingtime("Today lab Close");
                                }
                                sltm.setShow_lab_lab_price(jsonObject1.getString("lab_price"));
                                sltm.setShow_lab_final_price(jsonObject1.getString("final_price"));
                                sltm.setShow_lab_test_id(jsonObject1.getString("test_id"));
                                sltm.setShow_lab_sub_test_id(jsonObject1.getString("sub_test_id"));
                                sltm.setShow_lab_distance(jsonObject1.getString("distance_in_km"));
                                showlabstestList.add(sltm);
                                fabaction_status = 0;
                            }
                            showlabstestrecyclerview.setAdapter(adapter);

                            ShowLabsTest.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    progressDialog.dismiss();

                                }
                            });
                            mSwipeRefreshLayout.setRefreshing(false);
                        } else if (status.equals("18888")) {
                            ShowLabsTest.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    no_labs_found_iv.setVisibility(View.VISIBLE);
                                    progressDialog.dismiss();


                                }
                            });
                            mSwipeRefreshLayout.setRefreshing(false);

                        } else {
                            Toast.makeText(getApplicationContext(), "Please Try after some time...!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        ShowLabsTest.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        fabaction_status = 1;
                        mSwipeRefreshLayout.setRefreshing(false);
                        no_labs_found_iv.setVisibility(View.VISIBLE);
                        noInternetConnect.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnect.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                String responseBody = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("SHOWLABSTESTSSEND", sub_testId + "," + filterValue);

                params.put("test_id", sub_testId);

                if (first_time_flag == 1) {
                    Log.d("LABFINDSENDVALUES", sub_testId + "\n" + global_lat + "\n" + global_lng + "\n" + filterValue);
                    params.put("lat", global_lat);
                    params.put("lang", global_lng);
                } else {
                    Log.d("LABFINDSENDVALUES", sub_testId + "\n" + lat + "\n" + lng + "\n" + filterValue);
                    params.put("lat", lat);
                    params.put("lang", lng);
                }

                params.put("order_type", filterValue);
                Log.d("ORDER_TYPE", filterValue);
                Log.d("SDKAKFKAFKA", params.toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    public void cardViewListener(int position) {

        String selectedlabId = showlabstestList.get(position).getShow_lab_id();
        String selectedLabName = showlabstestList.get(position).getShow_lab_user_name();
        String selectedLabAddress = showlabstestList.get(position).getShow_lab_address();
        String selectedLabPrice = showlabstestList.get(position).getShow_lab_final_price();
        String selectedLabPhoneNumber = showlabstestList.get(position).getShow_lab_mobile();

        Intent intent = new Intent(this, BillingPage.class);
        intent.putExtra("selectedLabId", selectedlabId);
        intent.putExtra("selectedLabName", selectedLabName);
        intent.putExtra("selectedLabAddress", selectedLabAddress);
        intent.putExtra("subTestId", sub_testId);
        intent.putExtra("selecteLabPhone", selectedLabPhoneNumber);
        intent.putExtra("payment_lab", showlabstestList.get(position).getShow_lab_payment_lab());
        intent.putExtra("payment_online", showlabstestList.get(position).getShow_lab_payment_online());
        intent.putExtra("tele_booking", showlabstestList.get(position).getShow_lab_tele_booking());

        Bundle args = new Bundle();
        args.putSerializable("arraylistsubtestNames", (Serializable) arraylistsubtestNamesObject);
        args.putSerializable("arraylistsubtestIds", (Serializable) arraylistsubtestIdsObject);
        args.putSerializable("arraylisttestIds", (Serializable) arraylisttestIdsObject);
        intent.putExtra("bundelData", args);

        intent.putExtra("subTestName", sub_testName);
        intent.putExtra("selectedLabPrice", selectedLabPrice);

        startActivity(intent);

    }

    @Override
    public void onClick(View v) {


        if (v == yourpackageRL) {

            progressDialog.show();


            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.LAB_LOCATIONS, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        citiesModelArrayList.clear();
                        for (int i = 0; i < jsonArray.length(); i++)

                        {
                            CitiesModel citiesModel = new CitiesModel();

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("city");

                            String city_name = jsonObject1.getString("city_name");
                            String city_latitude = jsonObject1.getString("lat");
                            String city_longitude = jsonObject1.getString("lang");

                            citiesModel.setCity_name(city_name);
                            citiesModel.setLat(city_latitude);
                            citiesModel.setLang(city_longitude);

                            // labArrayList.add(city_name);

                            citiesModelArrayList.add(citiesModel);

                        }

                        alertLabsTest(citiesModelArrayList);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    String responseBody = null;

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(ShowLabsTest.this);
            requestQueue.add(stringRequest);
        }

        if (v == yourpackageLocation) {

            progressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.LAB_LOCATIONS, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        citiesModelArrayList.clear();
                        for (int i = 0; i < jsonArray.length(); i++) {

                            CitiesModel citiesModel = new CitiesModel();

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("city");

                            String city_name = jsonObject1.getString("city_name");
                            String city_latitude = jsonObject1.getString("lat");
                            String city_longitude = jsonObject1.getString("lang");

                            citiesModel.setCity_name(city_name);
                            citiesModel.setLat(city_latitude);
                            citiesModel.setLang(city_longitude);

                            // labArrayList.add(city_name);

                            citiesModelArrayList.add(citiesModel);
                        }

                        alertLabsTest(citiesModelArrayList);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    String responseBody = null;


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(ShowLabsTest.this);
            requestQueue.add(stringRequest);
        }


        if (v == yourpackageCity) {

            progressDialog.show();


            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.LAB_LOCATIONS, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        citiesModelArrayList.clear();
                        for (int i = 0; i < jsonArray.length(); i++)


                        {

                            CitiesModel citiesModel = new CitiesModel();

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("city");

                            String city_name = jsonObject1.getString("city_name");
                            String city_latitude = jsonObject1.getString("lat");
                            String city_longitude = jsonObject1.getString("lang");

                            citiesModel.setCity_name(city_name);
                            citiesModel.setLat(city_latitude);
                            citiesModel.setLang(city_longitude);

                            // labArrayList.add(city_name);

                            citiesModelArrayList.add(citiesModel);

                        }

                        alertLabsTest(citiesModelArrayList);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    String responseBody = null;


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(ShowLabsTest.this);
            requestQueue.add(stringRequest);
        }


        if (v == fabTests) {
            if (fabaction_status == 0) {

                AlertDialog.Builder builder = new AlertDialog.Builder(ShowLabsTest.this);
                LayoutInflater inflater = getLayoutInflater();

                View dialog_layout = inflater.inflate(R.layout.custom_dialog_filter, null);

                TextView dialog_title = (TextView) dialog_layout.findViewById(R.id.dialog_title);
                dialog_title.setTypeface(type_lato);

                TextView dlh_txt = (TextView) dialog_layout.findViewById(R.id.dlh_txt);
                dlh_txt.setTypeface(type_lato);

                final String cityName = yourpackageCity.getText().toString();

                dlh_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "distance_in_km asc";
                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                        } else {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                        }
                        dialog.dismiss();
                    }
                });

                TextView dhl_txt = (TextView) dialog_layout.findViewById(R.id.dhl_txt);
                dhl_txt.setTypeface(type_lato);
                dhl_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "distance_in_km desc";

                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                        } else {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                        }
                        dialog.dismiss();
                    }
                });

                TextView plh_txt = (TextView) dialog_layout.findViewById(R.id.plh_txt);
                plh_txt.setTypeface(type_lato);
                plh_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "final_price asc";
                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                        } else {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                        }
                        dialog.dismiss();
                    }
                });

                TextView phl_txt = (TextView) dialog_layout.findViewById(R.id.phl_txt);
                phl_txt.setTypeface(type_lato);
                phl_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "final_price desc";
                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                        } else {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                        }
                        dialog.dismiss();
                    }
                });

                TextView naz_txt = (TextView) dialog_layout.findViewById(R.id.naz_txt);
                naz_txt.setTypeface(type_lato);
                naz_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "users.user_name asc";
                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                        } else {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                        }
                        dialog.dismiss();
                    }
                });


                TextView nza_txt = (TextView) dialog_layout.findViewById(R.id.nza_txt);
                nza_txt.setTypeface(type_lato);
                nza_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "users.user_name desc";
                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                        } else {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                        }
                        dialog.dismiss();
                    }
                });

                TextView hc_txt = (TextView) dialog_layout.findViewById(R.id.hc_txt);
                hc_txt.setTypeface(type_lato);
                hc_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "distance_in_km asc";
                        String value2 = "lab_users.home_collections = 1";
                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilterTwo(vlaue, sub_testId, global_lat, global_lng, value2, cityName);
                        } else {
                            gettingLabswithCityNamewithFilterTwo(vlaue, sub_testId, lat, lng, value2, cityName);
                        }
                        dialog.dismiss();
                    }
                });

                TextView rhl_txt = (TextView) dialog_layout.findViewById(R.id.rhl_txt);
                rhl_txt.setTypeface(type_lato);
                rhl_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "rating desc";
                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                        } else {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                        }
                        dialog.dismiss();
                    }
                });


                TextView rlh_txt = (TextView) dialog_layout.findViewById(R.id.rlh_txt);
                rlh_txt.setTypeface(type_lato);
                rlh_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "rating asc";
                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                        } else {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                        }
                        dialog.dismiss();
                    }
                });


                TextView nabl_txt = (TextView) dialog_layout.findViewById(R.id.nabl_txt);
                nabl_txt.setTypeface(type_lato);
                nabl_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "distance_in_km asc";
                        String value2 = "lab_users.certificate = 1";

                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilterTwo(vlaue, sub_testId, global_lat, global_lng, value2, cityName);
                        } else {
                            gettingLabswithCityNamewithFilterTwo(vlaue, sub_testId, lat, lng, value2, cityName);
                        }
                        dialog.dismiss();
                    }
                });


                builder.setView(dialog_layout)
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });

                dialog = builder.create();
                dialog.show();

                /*final String[] country = {"Distance Low to High", "Distance High to Low", "Names A to Z", "Names Z to A"
                        , "Price Low to High", "Price High to Low", "Home Collection", "Ratting High to Low", "Ratting Low to High", "NABL/CAP Accreditation"};
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Choose One")
                        .setItems(country, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                String selectedCountry = country[i];

                                String cityName = yourpackageCity.getText().toString();

                                if (selectedCountry.equals("Distance Low to High")) {
                                    String vlaue = "distance_in_km asc";
                                    if (first_time_flag == 1) {
                                        gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                                    } else {
                                        gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                                    }


                                }
                                if (selectedCountry.equals("Distance High to Low")) {
                                    String vlaue = "distance_in_km desc";

                                    if (first_time_flag == 1) {
                                        gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                                    } else {
                                        gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                                    }
                                }
                                if (selectedCountry.equals("Names A to Z")) {
                                    String vlaue = "users.user_name asc";

                                    if (first_time_flag == 1) {
                                        gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                                    } else {
                                        gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                                    }
                                }
                                if (selectedCountry.equals("Names Z to A")) {
                                    String vlaue = "users.user_name desc";

                                    if (first_time_flag == 1) {
                                        gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                                    } else {
                                        gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                                    }
                                }
                                if (selectedCountry.equals("Price Low to High")) {
                                    String vlaue = "final_price asc";

                                    if (first_time_flag == 1) {
                                        gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                                    } else {
                                        gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                                    }
                                }
                                if (selectedCountry.equals("Price High to Low")) {
                                    String vlaue = "final_price desc";

                                    if (first_time_flag == 1) {
                                        gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                                    } else {
                                        gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                                    }
                                }


                                if (selectedCountry.equals("Home Collection")) {
                                    String vlaue = "distance_in_km asc";
                                    String value2 = "lab_users.home_collections = 1";
                                    if (first_time_flag == 1) {
                                        //gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng , cityName);
                                        gettingLabswithCityNamewithFilterTwo(vlaue, sub_testId, global_lat, global_lng, value2, cityName);
                                    } else {
                                        //gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng , cityName);
                                        gettingLabswithCityNamewithFilterTwo(vlaue, sub_testId, lat, lng, value2, cityName);
                                    }


                                }


                                if (selectedCountry.equals("NABL/CAP Accreditation")) {
                                    String vlaue = "distance_in_km asc";
                                    String value2 = "lab_users.certificate = 1";

                                    if (first_time_flag == 1) {
                                        //gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng , cityName);
                                        gettingLabswithCityNamewithFilterTwo(vlaue, sub_testId, global_lat, global_lng, value2, cityName);
                                    } else {
                                        //gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng , cityName);
                                        gettingLabswithCityNamewithFilterTwo(vlaue, sub_testId, lat, lng, value2, cityName);
                                    }
                                }


                                if (selectedCountry.equals("Ratting High to Low")) {
                                    String vlaue = "rating desc";

                                    if (first_time_flag == 1) {
                                        gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                                    } else {
                                        gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                                    }
                                }


                                if (selectedCountry.equals("Ratting Low to High")) {
                                    String vlaue = "rating asc";

                                    if (first_time_flag == 1) {
                                        gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                                    } else {
                                        gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                                    }
                                }

                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();*/
            } else {
                Toast.makeText(getApplicationContext(), "No Labs found....!", Toast.LENGTH_SHORT).show();
            }
        }


    }

    private void gettingLabswithCityNamewithFilterTwo(final String value, final String sub_testId, final String lat, final String lng, final String value2, final String cityName) {
        showlabstestList.clear();
        no_labs_found_iv.setVisibility(View.GONE);
        noInternetConnect.setVisibility(View.GONE);
        ShowLabsTest.this.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.show();
            }
        });
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_LABS_FOR_TESTS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);


                    String status = jsonObject.getString("status");

                    if (status.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");
                            ShowLabsTestModel sltm = new ShowLabsTestModel();
                            sltm.setShow_lab_id(jsonObject1.getString("id"));
                            sltm.setShow_lab_user_name(jsonObject1.getString("user_name"));
                            sltm.setShow_lab_email(jsonObject1.getString("email"));
                            sltm.setShow_lab_mobile(jsonObject1.getString("mobile"));
                            sltm.setShow_lab_role_id(jsonObject1.getString("role_id"));
                            sltm.setCertificate(jsonObject1.getString("certificate"));
                            sltm.setLocation(jsonObject1.getString("location"));

                            sltm.setProfile_pic_url(jsonObject1.getString("profile_pic"));


                            if (jsonObject1.getString("rating").equals("null")) {
                                sltm.setShow_lab_ratting("0");
                            } else {
                                sltm.setShow_lab_ratting(jsonObject1.getString("rating"));
                            }
                            sltm.setShow_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            sltm.setShow_lab_payment_online(jsonObject1.getString("payment_online"));
                            sltm.setShow_lab_tele_booking(jsonObject1.getString("tele_booking"));
                            sltm.setShow_lab_created_time(jsonObject1.getString("created_time"));

                            sltm.setShow_lab_address(jsonObject1.getString("address"));

                            sltm.setShow_lab_lat(jsonObject1.getString("lat"));
                            sltm.setShow_lab_lang(jsonObject1.getString("lang"));
                            sltm.setShow_lab_home_collections(jsonObject1.getString("home_collections"));
                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String workingDayfromServer = jsonObject1.getString("current_week_name");
                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];
                            if (conditionCheck.equals("1")) {
                                sltm.setShow_lab_workingtime("OpenToday:" + segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (conditionCheck.equals("0")) {
                                sltm.setShow_lab_workingtime("Today lab Close");
                            }
                            sltm.setShow_lab_lab_price(jsonObject1.getString("lab_price"));
                            sltm.setShow_lab_final_price(jsonObject1.getString("final_price"));
                            sltm.setShow_lab_test_id(jsonObject1.getString("test_id"));
                            sltm.setShow_lab_sub_test_id(jsonObject1.getString("sub_test_id"));


                            sltm.setShow_lab_distance(jsonObject1.getString("distance_in_km"));
                            showlabstestList.add(sltm);
                            fabaction_status = 0;
                        }
                        showlabstestrecyclerview.setAdapter(adapter);

                        ShowLabsTest.this.runOnUiThread(new Runnable() {
                            public void run() {
                                no_labs_found_iv.setVisibility(View.VISIBLE);
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                    } else {
                        ShowLabsTest.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        fabaction_status = 1;
                        mSwipeRefreshLayout.setRefreshing(false);

                        mSwipeRefreshLayout.setRefreshing(false);
                        no_labs_found_iv.setVisibility(View.VISIBLE);
                        noInternetConnect.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnect.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                String responseBody = null;


                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {


                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("test_id", sub_testId);

                if (first_time_flag == 1) {
                    Log.d("LABFINDSENDVALUES", sub_testId + "\n" + global_lat + "\n" + global_lng + "\n" + value);
                    params.put("lat", global_lat);
                    params.put("lang", global_lng);
                } else {
                    Log.d("LABFINDSENDVALUES", sub_testId + "\n" + lat + "\n" + lng + "\n" + value);
                    params.put("lat", lat);
                    params.put("lang", lng);
                }
                params.put("order_type", value);
                params.put("home_collections", value2);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void gettingLabswithCityNamewithFilter(final String value, final String sub_testId, final String lat, final String lng, String cityName) {
        showlabstestList.clear();
        ShowLabsTest.this.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.show();
            }
        });
        noInternetConnect.setVisibility(View.GONE);
        no_labs_found_iv.setVisibility(View.GONE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_LABS_FOR_TESTS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");


                    if (status.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");
                            ShowLabsTestModel sltm = new ShowLabsTestModel();
                            sltm.setShow_lab_id(jsonObject1.getString("id"));
                            sltm.setShow_lab_user_name(jsonObject1.getString("user_name"));
                            sltm.setShow_lab_email(jsonObject1.getString("email"));
                            sltm.setShow_lab_mobile(jsonObject1.getString("mobile"));
                            sltm.setLocation(jsonObject1.getString("location"));

                            sltm.setProfile_pic_url(jsonObject1.getString("profile_pic"));


                            sltm.setShow_lab_role_id(jsonObject1.getString("role_id"));
                            if (jsonObject1.getString("rating").equals("null")) {
                                sltm.setShow_lab_ratting("0");
                            } else {
                                sltm.setShow_lab_ratting(jsonObject1.getString("rating"));
                            }
                            sltm.setShow_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            sltm.setShow_lab_payment_online(jsonObject1.getString("payment_online"));
                            sltm.setShow_lab_tele_booking(jsonObject1.getString("tele_booking"));
                            sltm.setShow_lab_created_time(jsonObject1.getString("created_time"));
                            sltm.setCertificate(jsonObject1.getString("certificate"));

                            sltm.setShow_lab_address(jsonObject1.getString("address"));

                            sltm.setShow_lab_lat(jsonObject1.getString("lat"));
                            sltm.setShow_lab_lang(jsonObject1.getString("lang"));
                            sltm.setShow_lab_home_collections(jsonObject1.getString("home_collections"));
                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String workingDayfromServer = jsonObject1.getString("current_week_name");
                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];
                            if (conditionCheck.equals("1")) {
                                sltm.setShow_lab_workingtime("OpenToday:" + segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (conditionCheck.equals("0")) {
                                sltm.setShow_lab_workingtime("Today lab Close");
                            }
                            sltm.setShow_lab_lab_price(jsonObject1.getString("lab_price"));
                            sltm.setShow_lab_final_price(jsonObject1.getString("final_price"));
                            sltm.setShow_lab_test_id(jsonObject1.getString("test_id"));
                            sltm.setShow_lab_sub_test_id(jsonObject1.getString("sub_test_id"));


                            sltm.setShow_lab_distance(jsonObject1.getString("distance_in_km"));
                            showlabstestList.add(sltm);
                            fabaction_status = 0;
                        }
                        showlabstestrecyclerview.setAdapter(adapter);

                        ShowLabsTest.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                    } else {
                        ShowLabsTest.this.runOnUiThread(new Runnable() {
                            public void run() {
                                no_labs_found_iv.setVisibility(View.VISIBLE);
                                progressDialog.dismiss();

                            }
                        });
                        fabaction_status = 1;
                        mSwipeRefreshLayout.setRefreshing(false);
                        no_labs_found_iv.setVisibility(View.VISIBLE);
                        noInternetConnect.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnect.setVisibility(View.VISIBLE);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                String responseBody = null;


                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {


                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("test_id", sub_testId);


                if (first_time_flag == 1) {
                    Log.d("LABFINDSENDVALUES", sub_testId + "\n" + global_lat + "\n" + global_lng + "\n" + value);
                    params.put("lat", global_lat);
                    params.put("lang", global_lng);
                } else {
                    Log.d("LABFINDSENDVALUES", sub_testId + "\n" + lat + "\n" + lng + "\n" + value);
                    params.put("lat", lat);
                    params.put("lang", lng);
                }
                params.put("order_type", value);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void alertLabsTest(ArrayList<CitiesModel> labArrayList) {


        AlertDialog.Builder builder = new AlertDialog.Builder(ShowLabsTest.this);
        LayoutInflater inflater = this.getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.custom_dialog_layout_cities, null);
        RecyclerView cities_recycler_view = (RecyclerView) dialog_layout.findViewById(R.id.cities_recycler_view);
        final SearchView mSearch_list_cities = (SearchView) dialog_layout.findViewById(R.id.mSearch_list_cities);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ShowLabsTest.this);
        cities_recycler_view.setLayoutManager(layoutManager);


        citiesAdapter = new ShowLabsTestCitiesAdapter(labArrayList, ShowLabsTest.this, R.layout.row_cities);


        cities_recycler_view.setAdapter(citiesAdapter);


        mSearch_list_cities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearch_list_cities.setIconified(false);
            }
        });


        builder.setView(dialog_layout)


                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        progressDialog.dismiss();
                        dialogInterface.dismiss();
                    }
                });

        city_dialog = builder.create();

        mSearch_list_cities.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                citiesAdapter.getFilter().filter(query);
                return false;
            }
        });
        city_dialog.show();


    }

    private void gettingLabsotherCities(String selectedCountry, String strLat, String strLng) {


        showlabstestrecyclerview = (RecyclerView) findViewById(R.id.showlabstest_recycler_view);
        showlabstestrecyclerview.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        showlabstestrecyclerview.setLayoutManager(layoutManager);

        showlabstestrecyclerview.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        //or
        showlabstestrecyclerview.addItemDecoration(new DividerItemDecoration(getApplicationContext()));
        //or
        showlabstestrecyclerview.addItemDecoration(
                new DividerItemDecoration(getApplicationContext(), R.drawable.recycler_divider));

        closeShowLabsTestsBt = (ImageButton) findViewById(R.id.sub_testsshow_lab_btn_close);

        adapter = new ShowLabsTestAdapter(sub_testName, showlabstestList, ShowLabsTest.this, R.layout.row_labs_test_items3, sub_testId, arraylistsubtestNamesObject, arraylistsubtestIdsObject, arraylisttestIdsObject);

        //if(filterFlag == 0) {

        filterString = "distance_in_km asc";

        gettingLabsOtherCity(selectedCountry, filterString, strLat, strLng);
      /*  }else {

        }*/
    }

    private void gettingLabsOtherCity(final String selectedCountry, final String filterString, final String strLat, final String strLng) {
        showlabstestList.clear();
        ShowLabsTest.this.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.show();
            }
        });
        noInternetConnect.setVisibility(View.GONE);
        no_labs_found_iv.setVisibility(View.GONE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_LABS_FOR_TESTS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);
                    Log.d("Purushotham-----", response);

                    String status = jsonObject.getString("status");

                    if (status.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");
                            ShowLabsTestModel sltm = new ShowLabsTestModel();
                            sltm.setShow_lab_id(jsonObject1.getString("id"));
                            sltm.setShow_lab_user_name(jsonObject1.getString("user_name"));
                            sltm.setShow_lab_email(jsonObject1.getString("email"));
                            sltm.setShow_lab_mobile(jsonObject1.getString("mobile"));
                            sltm.setLocation(jsonObject1.getString("location"));

                            sltm.setProfile_pic_url(jsonObject1.getString("profile_pic"));


                            sltm.setShow_lab_role_id(jsonObject1.getString("role_id"));
                            if (jsonObject1.getString("rating").equals("null")) {
                                sltm.setShow_lab_ratting("0");
                            } else {
                                sltm.setShow_lab_ratting(jsonObject1.getString("rating"));
                            }
                            sltm.setShow_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            sltm.setShow_lab_payment_online(jsonObject1.getString("payment_online"));
                            sltm.setShow_lab_tele_booking(jsonObject1.getString("tele_booking"));
                            sltm.setShow_lab_created_time(jsonObject1.getString("created_time"));
                            sltm.setCertificate(jsonObject1.getString("certificate"));

                            sltm.setShow_lab_address(jsonObject1.getString("address"));

                            sltm.setShow_lab_lat(jsonObject1.getString("lat"));
                            sltm.setShow_lab_lang(jsonObject1.getString("lang"));
                            sltm.setShow_lab_home_collections(jsonObject1.getString("home_collections"));
                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String workingDayfromServer = jsonObject1.getString("current_week_name");
                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];
                            if (conditionCheck.equals("1")) {
                                sltm.setShow_lab_workingtime("OpenToday:" + segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (conditionCheck.equals("0")) {
                                sltm.setShow_lab_workingtime("Today lab Close");
                            }
                            sltm.setShow_lab_lab_price(jsonObject1.getString("lab_price"));
                            sltm.setShow_lab_final_price(jsonObject1.getString("final_price"));
                            sltm.setShow_lab_test_id(jsonObject1.getString("test_id"));
                            sltm.setShow_lab_sub_test_id(jsonObject1.getString("sub_test_id"));


                            sltm.setShow_lab_distance(jsonObject1.getString("distance_in_km"));
                            showlabstestList.add(sltm);
                            fabaction_status = 0;
                        }
                        showlabstestrecyclerview.setAdapter(adapter);

                        ShowLabsTest.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                    if (status.equals("18888")) {
                        ShowLabsTest.this.runOnUiThread(new Runnable() {
                            public void run() {
                                no_labs_found_iv.setVisibility(View.VISIBLE);
                                progressDialog.dismiss();

                            }
                        });
                        fabaction_status = 1;
                        mSwipeRefreshLayout.setRefreshing(false);
                        no_labs_found_iv.setVisibility(View.VISIBLE);
                        noInternetConnect.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnect.setVisibility(View.VISIBLE);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                String responseBody = null;

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {


                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("test_id", sub_testId);


                params.put("order_type", filterString);
                if (first_time_flag == 1) {
                    Log.d("LABFINDSENDVALUES", sub_testId + "\n" + global_lat + "\n" + global_lng + "\n" + filterString);
                    params.put("lat", global_lat);
                    params.put("lang", global_lng);
                } else {
                    Log.d("LABFINDSENDVALUES", sub_testId + "\n" + lat + "\n" + lng + "\n" + filterString);
                    params.put("lat", lat);
                    params.put("lang", lng);
                }
                Log.d("PARAMAMMMMMMMMMMMMM:", params.toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onRefresh() {
        showlabstestList = new ArrayList<ShowLabsTestModel>();

        showlabstestrecyclerview = (RecyclerView) findViewById(R.id.showlabstest_recycler_view);
        showlabstestrecyclerview.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        showlabstestrecyclerview.setLayoutManager(layoutManager);

        showlabstestrecyclerview.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        //or
        showlabstestrecyclerview.addItemDecoration(new DividerItemDecoration(getApplicationContext()));
        //or
        showlabstestrecyclerview.addItemDecoration(
                new DividerItemDecoration(getApplicationContext(), R.drawable.recycler_divider));

        closeShowLabsTestsBt = (ImageButton) findViewById(R.id.sub_testsshow_lab_btn_close);

        adapter = new ShowLabsTestAdapter(sub_testName, showlabstestList, ShowLabsTest.this, R.layout.row_labs_test_items3, sub_testId, arraylistsubtestNamesObject, arraylistsubtestIdsObject, arraylisttestIdsObject);

        if (filterFlag == 0) {

            filterString = "distance_in_km asc";

            getLabsTest(filterString);
        }
    }


    public void refreshMyList(String selectedCountry, String selected_city_lat, String selected_city_lng) {


        city_dialog.dismiss();


        first_time_flag = 1;

        yourpackageCity.setText(selectedCountry);


        global_lat = selected_city_lat;
        global_lng = selected_city_lng;

        gettingLabsotherCities(selectedCountry, global_lat, global_lng);
        adapter.notifyDataSetChanged();


    }

}
