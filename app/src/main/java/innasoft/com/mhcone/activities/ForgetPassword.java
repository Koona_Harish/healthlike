package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;

public class ForgetPassword extends AppCompatActivity implements View.OnClickListener {
    TextView forget_password_headding;
    TextInputLayout forget_email_til;
    EditText forget_password_et_email;
    Button forgetpassword_submit, cancel_otp;
    Typeface typeface;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password_dummy);

        progressDialog = new ProgressDialog(ForgetPassword.this, R.style.DialogTheme);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait......");

        typeface = Typeface.createFromAsset(this.getAssets(),"robotoregular.ttf");

        forget_password_headding = (TextView) findViewById(R.id.forget_activity_headding);
        forget_password_headding.setTypeface(typeface);
        forget_email_til = (TextInputLayout) findViewById(R.id.forget_email_til);
        forget_email_til.setTypeface(typeface);
        forget_password_et_email = (EditText) findViewById(R.id.forget_et_email);
        forget_password_et_email.setTypeface(typeface);
        forgetpassword_submit = (Button) findViewById(R.id.send_otp);
        forgetpassword_submit.setTypeface(typeface);

        cancel_otp = (Button)findViewById(R.id.cancel_otp);
        cancel_otp.setTypeface(typeface);

        forgetpassword_submit.setOnClickListener(this);
        cancel_otp.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view == forgetpassword_submit) {

            if (validate()) {

                switch (view.getId()) {
                    case R.id.send_otp:

                        if ((forget_password_et_email.getText().toString().trim().length() > 0)) {

                            forgetpassword_submit.setClickable(false);
                            progressDialog.show();
                            final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.FORGETPASSWORD_URL,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            Log.d("FORGETPASSWORDTEST", response);

                                            try {
                                                JSONObject jsonObject = new JSONObject(response);
                                                String successResponceCodeForget = jsonObject.getString("status");
                                                if (successResponceCodeForget.equals("50100")) {
                                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                                    String emaiOTP = jsonObject1.getString("email");
                                                    String user_id = jsonObject1.getString("user_id");
                                                    String mobile = jsonObject1.getString("mobile");
                                                    String msg = jsonObject1.getString("msg");
                                                    progressDialog.dismiss();
                                                    Toast.makeText(getApplicationContext(), "OTP has been sent to your registered email id...!", Toast.LENGTH_SHORT).show();
                                                    Intent intent = new Intent(getApplicationContext(), OTPVerification.class);
                                                    intent.putExtra("otpEmail", emaiOTP);
                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("mobile", mobile);
                                                    startActivity(intent);
                                                }

                                                if (successResponceCodeForget.equals("50112")) {
                                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                                    String emaiOTP = jsonObject1.getString("email");
                                                    String user_id = jsonObject1.getString("user_id");
                                                    String mobile = jsonObject1.getString("mobile");
                                                    String msg = jsonObject1.getString("msg");
                                                    progressDialog.dismiss();
                                                    Toast.makeText(getApplicationContext(), "Your account not verified...!", Toast.LENGTH_SHORT).show();
                                                    Intent intent = new Intent(getApplicationContext(), OTPVerification.class);
                                                    intent.putExtra("otpEmail", emaiOTP);
                                                    intent.putExtra("user_id", user_id);
                                                    intent.putExtra("mobile", mobile);
                                                    startActivity(intent);
                                                }
                                                if (successResponceCodeForget.equals("50111")) {
                                                    progressDialog.dismiss();
                                                    forgetpassword_submit.setClickable(true);
                                                    Toast.makeText(getApplicationContext(), "Your email id is not registered.", Toast.LENGTH_SHORT).show();
                                                }

                                                if(successResponceCodeForget.equals("10140"))
                                                {
                                                    progressDialog.dismiss();
                                                    Toast.makeText(getApplicationContext(), "Admin Aproved Required....!",Toast.LENGTH_SHORT).show();
                                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                    startActivity(intent);
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {

                                            progressDialog.dismiss();
                                            forgetpassword_submit.setClickable(true);

                                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                            } else if (error instanceof AuthFailureError) {

                                            } else if (error instanceof ServerError) {

                                            } else if (error instanceof NetworkError) {

                                            } else if (error instanceof ParseError) {

                                            }
                                        }
                                    })
                            {
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    Map<String, String> params = new HashMap<String, String>();
                                    params.put("forgot_email", forget_password_et_email.getText().toString().trim());
                                    return params;
                                }
                            };
                            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                            requestQueue.add(stringRequest);
                        } else {
                            Toast.makeText(getApplicationContext(), "Please provide all the details..!!", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
            }
        }
        if ((view == cancel_otp))
        {
            finish();
        }
    }

    private boolean validate() {
        boolean result = true;

        String email = forget_password_et_email.getText().toString().trim();
        if (email == null || email.equals("")) {
            forget_email_til.setError(getString(R.string.invalidData));
            result = false;
        }
        else
            forget_email_til.setErrorEnabled(false);

        return result;
    }
}
