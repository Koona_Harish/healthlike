package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.HabitNotificationUpdateAdapter;
import innasoft.com.mhcone.holders.HabitNotificationUpdateHolder;
import innasoft.com.mhcone.models.HabitNotificationUpdateModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.DividerItemDecoration;
import innasoft.com.mhcone.utilities.UserSessionManager;
import innasoft.com.mhcone.utilities.VerticalSpaceItemDecoration;

public class HabitNotificationUpdate extends AppCompatActivity implements HabitNotificationUpdateHolder.MyHabitUpdateHolder, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    ArrayList<HabitNotificationUpdateModel> habitsNotificationUpdateList;
    HabitNotificationUpdateAdapter adapter;
    RecyclerView habitsupdaterecyclerview;
    private static final int VERTICAL_ITEM_SPACE = 20;
    int flag;
    RecyclerView.LayoutManager layoutManager;
    UserSessionManager userSessionManager;
    String access_key,disp_userName,disp_email;
    String userID;
    Button btUpdateHabitNotification;
    ProgressDialog progressDialog;
    Typeface typeface,typeface2;
    ImageView notifcation_update_cancel_btn;
    TextView lab_packages_txt;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_habit_notification_update);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Articles Preferences Update");

        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typeface2 = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        progressDialog = new ProgressDialog(HabitNotificationUpdate.this, R.style.DialogTheme);
        progressDialog.setMessage("Please wait......");

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = userSessionManager.getUserDetails();
        access_key = user.get(UserSessionManager.KEY_ACCSES);

        userID = user.get(UserSessionManager.USER_ID);

        lab_packages_txt = (TextView) findViewById(R.id.lab_packages_txt);
        lab_packages_txt.setTypeface(typeface);

        notifcation_update_cancel_btn = (ImageView) findViewById(R.id.notifcation_update_cancel_btn);
        btUpdateHabitNotification = (Button) findViewById(R.id.btUpdateHabitNotification);
        btUpdateHabitNotification.setTypeface(typeface2);

        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.habits_update_activity_swipe_layout);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,

                android.R.color.holo_green_light,

                android.R.color.holo_orange_light,

                android.R.color.holo_red_light);


        habitsNotificationUpdateList = new ArrayList<HabitNotificationUpdateModel>();

        habitsupdaterecyclerview = (RecyclerView) findViewById(R.id.notification_update_recyclerview);
        habitsupdaterecyclerview.setHasFixedSize(true);
        habitsupdaterecyclerview.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        habitsupdaterecyclerview.addItemDecoration(new DividerItemDecoration(getApplicationContext()));
        habitsupdaterecyclerview.addItemDecoration(
                new DividerItemDecoration(getApplicationContext(), R.drawable.recycler_divider));

        layoutManager = new LinearLayoutManager(this);
        habitsupdaterecyclerview.setLayoutManager(layoutManager);

        adapter = new HabitNotificationUpdateAdapter(habitsNotificationUpdateList, HabitNotificationUpdate.this, R.layout.row_habits_notification_update);
        getLabPackages(userID);
        notifcation_update_cancel_btn.setOnClickListener(this);
        btUpdateHabitNotification.setOnClickListener(this);
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    private void getLabPackages(final String userID) {
        progressDialog.show();
        habitsNotificationUpdateList.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.USER_SELECTED_HABITS_NOTIFICATIOS, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                try {

                    JSONObject jsonObject =new JSONObject(response);
                    String stausCode = jsonObject.getString("status");
                    if(stausCode.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("habit_user");
                            HabitNotificationUpdateModel hnum = new HabitNotificationUpdateModel();

                            hnum.setId(jsonObject1.getString("id"));
                            hnum.setUser_id(jsonObject1.getString("user_id"));
                            hnum.setHabit_id(jsonObject1.getString("habit_id"));
                            hnum.setStatus(jsonObject1.getString("status"));
                            hnum.setCreated_time(jsonObject1.getString("created_time"));
                            hnum.setHabit_name(jsonObject1.getString("habit_name"));

                            habitsNotificationUpdateList.add(hnum);
                        }
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                        habitsupdaterecyclerview.setAdapter(adapter);
                    }
                    if(stausCode.equals("18888"))
                    {
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                    if(stausCode.equals("10140"))
                    {
                        mSwipeRefreshLayout.setRefreshing(false);
                        userSessionManager.logoutUser();
                        Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }

                    if(stausCode.equals("10150"))
                    {
                        mSwipeRefreshLayout.setRefreshing(false);
                        userSessionManager.logoutUser();
                        Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String,String> params = new HashMap<String, String>();

                params.put("user_id", userID);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void cardViewListener(int position) {

    }

    @Override
    public void onClick(View v) {
        if(v == notifcation_update_cancel_btn)
        {
            finish();
        }
        if(v == btUpdateHabitNotification)
        {
            List<String> updatnotificationId = new ArrayList<String>();
            List<String> updateNotificationStatus = new ArrayList<String>();
            List<String> name = new ArrayList<String>();
            List<String> sendUpdateHabitFormat = new ArrayList<String>();

            for(int i = 0; i < habitsNotificationUpdateList.size(); i++)
            {
                updatnotificationId.add(habitsNotificationUpdateList.get(i).getId());
                updateNotificationStatus.add(habitsNotificationUpdateList.get(i).getStatus());
                name.add(habitsNotificationUpdateList.get(i).getHabit_name());
                sendUpdateHabitFormat.add(habitsNotificationUpdateList.get(i).getId()+"-"+habitsNotificationUpdateList.get(i).getStatus()+"-"+habitsNotificationUpdateList.get(i).getHabit_id());
            }
            final String finalSendValue = sendUpdateHabitFormat.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", "");

            progressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.USER_SELECTED_HABITS_NOTIFICATION_UPDATE, new Response.Listener<String>()
            {
                @Override
                public void onResponse(String response)
                {
                    try {

                        JSONObject jsonObject =new JSONObject(response);
                        String stausCode = jsonObject.getString("status");
                        if(stausCode.equals("20100"))
                        {
                            progressDialog.dismiss();

                            final Toast toast = Toast.makeText(getApplicationContext(), "Article Preferences Updated Successfully...!", Toast.LENGTH_SHORT);
                            toast.show();

                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    toast.cancel();
                                }
                            }, 100);
                            Intent intent = new Intent(HabitNotificationUpdate.this, MainActivity.class);
                            startActivity(intent);
                        }

                        if(stausCode.equals("20130"))
                        {
                            progressDialog.dismiss();
                            userSessionManager.logoutUser();

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        }
                        if(stausCode.equals("10140"))
                        {
                            progressDialog.dismiss();
                            userSessionManager.logoutUser();
                            Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        }

                        if(stausCode.equals("10150"))
                        {
                            progressDialog.dismiss();
                            userSessionManager.logoutUser();
                            Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        }
                        progressDialog.dismiss();
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
            })
            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError
                {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("user_id", userID);
                    params.put("status", finalSendValue);
                    Log.d("FORMATTTTT",userID+"\n"+finalSendValue);
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);
        }
    }

    @Override
    public void onRefresh() {
        getLabPackages(userID);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
