package innasoft.com.mhcone.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.GPSTrackers;
import innasoft.com.mhcone.utilities.StartLocationAlert;


public class ShowLabInformation extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener,
        View.OnClickListener, CompoundButton.OnCheckedChangeListener, android.location.LocationListener {


    ImageView checktickmarkLI;
    TextView Lab_details_name, Lab_details_email, Lab_details_phone, Lab_details_address, Lab_details_workingdays,
            Lab_details_workingtime, Lab_details_homecollection,lab_info_text;
    TextView labdetails_phone, Lab_email, Lab_address;
    SliderLayout imageSlider;
    StartLocationAlert startLocationAlert;

    TextView labInfoText;
    ImageButton labInfoClose;

    ProgressDialog progressDialog;
    LinearLayout show_lab_info;

    GoogleMap mMap;
    //GoogleMap mGoogleMap;
    //SupportMapFragment mapFrag;
    LocationRequest mLocationRequest;
    //GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    Typeface type_lato,type_bold;


    GPSTrackers gps;
    String currentLat, currentLng;
    private double longitude;
    private double latitude;
    Double lat_istitute, longi_istitute;
    ImageView labInformationCall;
    ArrayList<LatLng> mMarkerPoints;
    String lab_phone;
    TextView mondayId, tuesdayId, wednesdayId, thursdayId, fridayId, saturdayId, sundayId;
    String lab_name;

    SupportMapFragment mapFragment;
    private GoogleApiClient googleApiClient;
    public static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 99;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_lab_information);

        type_lato = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        type_bold = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Healthlike");

        lab_info_text = (TextView) findViewById(R.id.lab_info_text);
        lab_info_text.setTypeface(type_bold);

        imageSlider = (SliderLayout) findViewById(R.id.slider);

        checktickmarkLI = (ImageView) findViewById(R.id.checktickmark_li);

        labInformationCall = (ImageView) findViewById(R.id.labinformation_call);
        labInformationCall.setOnClickListener(this);

        show_lab_info = (LinearLayout) findViewById(R.id.lab_info_rl);
        show_lab_info.setOnClickListener(this);

        Lab_details_workingtime = (TextView) findViewById(R.id.workingtime_li);
        Lab_details_workingtime.setTypeface(type_bold);

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        /*if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }*/

        mondayId = (TextView) findViewById(R.id.mondaytv);
        mondayId.setTypeface(type_lato);
        tuesdayId = (TextView) findViewById(R.id.tuesdaytv);
        tuesdayId.setTypeface(type_lato);
        wednesdayId = (TextView) findViewById(R.id.wednesdaytv);
        wednesdayId.setTypeface(type_lato);
        thursdayId = (TextView) findViewById(R.id.thursdaytv);
        thursdayId.setTypeface(type_lato);
        fridayId = (TextView) findViewById(R.id.fridaytv);
        fridayId.setTypeface(type_lato);
        saturdayId = (TextView) findViewById(R.id.saturdaytv);
        saturdayId.setTypeface(type_lato);
        sundayId = (TextView) findViewById(R.id.sundaytv);
        sundayId.setTypeface(type_lato);

        Bundle bundle = getIntent().getExtras();
        String getLat = bundle.getString("toLat");
        String getLng = bundle.getString("tolng");

        Log.d("asdfdsfdfd",getLat+"//"+getLng);
        lat_istitute = Double.valueOf(getLat);
        longi_istitute = Double.valueOf(getLng);

        String labId = bundle.getString("LabID");


        gps = new GPSTrackers(getApplicationContext());

        mMarkerPoints = new ArrayList<LatLng>();

        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();

            currentLat = String.valueOf(latitude);
            currentLng = String.valueOf(longitude);
        } else {
            startLocationAlert = new StartLocationAlert(ShowLabInformation.this);
        }


        Lab_details_name = (TextView) findViewById(R.id.lab_details_name);
        Lab_details_name.setTypeface(type_lato);

        Lab_email = (TextView) findViewById(R.id.lab_email);
        Lab_email.setTypeface(type_bold);

        Lab_details_email = (TextView) findViewById(R.id.lab_details_email);
        Lab_details_email.setTypeface(type_lato);

        Lab_details_phone = (TextView) findViewById(R.id.lab_details_phone);
        Lab_details_phone.setTypeface(type_lato);

        labdetails_phone = (TextView) findViewById(R.id.labdetails_phone);
        labdetails_phone.setTypeface(type_bold);

        Lab_address = (TextView) findViewById(R.id.lab_address);
        Lab_address.setTypeface(type_bold);

        Lab_details_address = (TextView) findViewById(R.id.lab_details_address);
        Lab_details_address.setTypeface(type_lato);

        Lab_details_homecollection = (TextView) findViewById(R.id.lab_details_homecollection);
        Lab_details_homecollection.setTypeface(type_bold);

        labInfoClose = (ImageButton) findViewById(R.id.labinfo_btn_close);
        labInfoText = (TextView) findViewById(R.id.lab_info_text);
        labInfoText.setTypeface(type_lato);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");

        progressDialog.setProgressStyle(R.style.DialogTheme);

        labInfoClose.setOnClickListener(this);


        getSlider(labId);
        getDetails(labId);


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        /*mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);*/

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    private void getSlider(final String labId) {
        ShowLabInformation.this.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.show();

            }
        });


        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_LAB_PROFILE_PICS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                         Log.d("SLIDERRESP:",response);
                        HashMap<String, String> url_maps = new HashMap<String, String>();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                            JSONArray jsonArray = jsonObject1.getJSONArray("lab_picks");
                            //JSONArray jsonArray = jsonObject1.getJSONArray("products");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonobject3 = jsonArray.getJSONObject(i);

                                String url = AppUrls.IMAGE_URL + jsonobject3.getString("profile_pic");

                                String name = jsonobject3.getString("id");

                                url_maps.put(name, url);


                            }


                            for (String name : url_maps.keySet()) {
                                TextSliderView textSliderView = new TextSliderView(getApplicationContext());

                                textSliderView
                                        .description("")
                                        .image(url_maps.get(name))
                                        .setScaleType(BaseSliderView.ScaleType.Fit);



                                textSliderView.bundle(new Bundle());


                                imageSlider.addSlider(textSliderView);
                            }
                            ShowLabInformation.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    progressDialog.dismiss();

                                }
                            });
                            imageSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
                            imageSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);

                            imageSlider.setDuration(4000);



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();


                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }

                    }


                }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("lab_id", labId);
                Log.d("SLIDERRESPARAM:",params.toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    private void getDetails(final String labId) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_LAB_INFO, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response);

                    Log.d("MAPPPPPP",response);

                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                    String lab_lat = jsonObject1.getString("lat");
                    String lab_lang = jsonObject1.getString("lang");

                    //dtoLat = Double.valueOf(lab_lat);
                    //dtoLng = Double.valueOf(lab_lang);


                    lab_name = jsonObject1.getString("user_name");
                    Lab_details_name.setText(lab_name);
                    Lab_details_name.setTypeface(type_bold);

                    if(lab_lat.equals("") || lab_lang.equals("")||lab_lat.equals("null") || lab_lang.equals("null")||lab_lat.equals(null) || lab_lang.equals(null))
                    {
                        mapFragment.getView().setVisibility(View.GONE);
                    }
                    else
                    {
                        lat_istitute = Double.valueOf(lab_lat);
                        longi_istitute = Double.valueOf(lab_lang);

                        Log.d("WWWWWWWW",lat_istitute+"\n"+longi_istitute);
                        moveMap(Double.parseDouble(lab_lat), Double.parseDouble(lab_lang), lab_name);
                    }

                    String lab_email = jsonObject1.getString("email");
                    Lab_details_email.setText(lab_email);

                    lab_phone = jsonObject1.getString("mobile");
                    Lab_details_phone.setText(lab_phone);

                    String lab_address = jsonObject1.getString("address");
                    Lab_details_address.setText(lab_address);

                    String lab_workingdays = jsonObject1.getString("workingdays");


                    String lab_workingtime = jsonObject1.getString("workingtime");


                    String lab_homecollection = jsonObject1.getString("home_collections");
                    if (lab_homecollection.equals("1")) {

                        Lab_details_homecollection.setTypeface(type_bold);

                        checktickmarkLI.setImageResource(R.drawable.right_mark_new);
                    } else if (lab_homecollection.equals("0")) {

                        Lab_details_homecollection.setTypeface(type_bold);

                        checktickmarkLI.setImageResource(R.drawable.access_denied);


                    }

                    String monGettingData = jsonObject1.getString("mon");
                    String monsegments[] = monGettingData.split("-");

                    String conditionCheckMonday = monsegments[monsegments.length - 1];
                    if (conditionCheckMonday.equals("0")) {

                        mondayId.setText("Closed");
                    }
                    if (conditionCheckMonday.equals("1")) {
                        mondayId.setText(monsegments[monsegments.length - 3] + " to " + monsegments[monsegments.length - 2]);
                    }


                    String tueGettingData = jsonObject1.getString("tue");
                    String tuesegments[] = tueGettingData.split("-");

                    String conditionCheckTuesday = tuesegments[tuesegments.length - 1];
                    if (conditionCheckTuesday.equals("0")) {

                        tuesdayId.setText("Closed");
                    }
                    if (conditionCheckTuesday.equals("1")) {
                        tuesdayId.setText(tuesegments[tuesegments.length - 3] + " to " + tuesegments[tuesegments.length - 2]);
                    }


                    String wedGettingData = jsonObject1.getString("wed");
                    String wedsegments[] = wedGettingData.split("-");

                    String conditionCheckWednesday = wedsegments[wedsegments.length - 1];
                    if (conditionCheckWednesday.equals("0")) {

                        wednesdayId.setText("Closed");
                    }
                    if (conditionCheckWednesday.equals("1")) {
                        wednesdayId.setText(wedsegments[wedsegments.length - 3] + " to " + wedsegments[wedsegments.length - 2]);
                    }


                    String thuGettingData = jsonObject1.getString("thu");
                    String thusegments[] = thuGettingData.split("-");

                    String conditionCheckThursday = thusegments[thusegments.length - 1];
                    if (conditionCheckThursday.equals("0")) {

                        thursdayId.setText("Closed");
                    }
                    if (conditionCheckThursday.equals("1")) {
                        thursdayId.setText(thusegments[thusegments.length - 3] + " to " + thusegments[thusegments.length - 2]);
                    }


                    String friGettingData = jsonObject1.getString("fri");
                    String frisegments[] = friGettingData.split("-");

                    String conditionCheckFriday = frisegments[frisegments.length - 1];
                    if (conditionCheckFriday.equals("0")) {

                        fridayId.setText("Closed");
                    }
                    if (conditionCheckFriday.equals("1")) {
                        fridayId.setText(frisegments[frisegments.length - 3] + " to " + frisegments[frisegments.length - 2]);
                    }


                    String satGettingData = jsonObject1.getString("sat");
                    String satsegments[] = satGettingData.split("-");

                    String conditionCheckSaturady = satsegments[satsegments.length - 1];
                    if (conditionCheckSaturady.equals("0")) {

                        saturdayId.setText("Closed");
                    }
                    if (conditionCheckSaturady.equals("1")) {
                        saturdayId.setText(satsegments[satsegments.length - 3] + " to " + satsegments[satsegments.length - 2]);
                    }


                    String sunGettingData = jsonObject1.getString("sun");
                    String sunsegments[] = sunGettingData.split("-");

                    String conditionCheckSunday = sunsegments[sunsegments.length - 1];
                    if (conditionCheckSunday.equals("0")) {

                        sundayId.setText("Closed");
                    }
                    if (conditionCheckSunday.equals("1")) {
                        sundayId.setText(sunsegments[sunsegments.length - 3] + " to " + sunsegments[sunsegments.length - 2]);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("lab_id", labId);

                Log.d("DETAILRESPARAM:",params.toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void moveMap(double latitude, double longitude, String in_name) {

        String msg = latitude + ", "+longitude;
        LatLng latLng = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true).title(in_name));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
    }

    @Override
    protected void onStop() {

        imageSlider.stopAutoCycle();

        super.onStop();
    }

    @Override
    protected void onStart() {


        super.onStart();
    }



    /*@Override
    public void onSliderClick(BaseSliderView slider) {
        Toast.makeText(this, slider.getBundle().get("extra") + "", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        Log.e("Slider Demo", "Page Changed: " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }*/

    @Override
    public void onClick(View v) {

        if (v == labInfoClose) {
            finish();
        }
        if (v == show_lab_info) {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:"+Uri.encode(lab_phone.trim())));
            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(callIntent);
        }
        if (v == labInformationCall) {
            if (checkCallPermission()) {
                callPhone();
            } else {
                checkCallPermission();
                Toast.makeText(getApplicationContext(), "Call Permission required", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void callPhone(){
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + Uri.encode(lab_phone.trim())));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(callIntent);
    }

    public boolean checkCallPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CALL_PHONE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Call")
                        .setMessage("Call Permission Required")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(ShowLabInformation.this,
                                        new String[]{Manifest.permission.CALL_PHONE},
                                        MY_PERMISSIONS_REQUEST_CALL_PHONE);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        MY_PERMISSIONS_REQUEST_CALL_PHONE);
            }
            return false;
        } else {
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (String permission : permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                //denied
                Log.e("denied", permission);
                Toast.makeText(getApplicationContext(), "Permissions required", Toast.LENGTH_SHORT).show();
            } else {
                if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                    //allowed
                    Log.e("allowed", permission);
//                    gps = new GPSTrackers(getApplicationContext());
                    callPhone();
                } else {
                    //set to never ask again
                    Log.e("set to never ask again", permission);
                    //do something here.
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }
            }
        }
    }


    /*@Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (com.google.android.gms.location.LocationListener) this);
        }

        //mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }


        LatLng latLng = new LatLng(dtoLat, dtoLng);
        Marker markerOptions = mGoogleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(lab_name)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        markerOptions.showInfoWindow();
        mGoogleMap.setMyLocationEnabled(false);
        //move map camera
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(18 ));


        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (com.google.android.gms.location.LocationListener) this);
        }


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    @Override
    public void onLocationChanged(Location location) {

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }


        LatLng latLng = new LatLng(dtoLat, dtoLng);
        Marker markerOptions = mGoogleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(lab_name)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        markerOptions.showInfoWindow();
        mGoogleMap.setMyLocationEnabled(false);
        //move map camera
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(18 ));



        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (com.google.android.gms.location.LocationListener) this);
        }

    }
*/
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {


            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {


                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    /*@Override
    public void onMapReady(GoogleMap googleMap)
    {

        mGoogleMap=googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(true);
        }

        LatLng latLng = new LatLng(dtoLat, dtoLng);
        Marker markerOptions = mGoogleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(lab_name)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        markerOptions.showInfoWindow();
        mGoogleMap.setMyLocationEnabled(false);

        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(18 ));
    }

    protected synchronized void buildGoogleApiClient()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {



                    if (ContextCompat.checkSelfPermission(this,
                            android.Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {




                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mGoogleMap.setMyLocationEnabled(true);
                    }

                } else {


                    Toast.makeText(this, "permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }


        }
    }*/

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

        latitude = marker.getPosition().latitude;
        longitude = marker.getPosition().longitude;

        Log.d("MAPVALUES:",""+latitude+"///"+longitude);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(lat_istitute, longi_istitute);
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener()
        {
            @Override
            public void onMapClick(LatLng latLng)
            {
                String uri = String.format(Locale.ENGLISH, "geo:%f,%f", lat_istitute, longi_istitute);
                Log.e("LLLLLLL:",""+lat_istitute+"///"+longi_istitute);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

