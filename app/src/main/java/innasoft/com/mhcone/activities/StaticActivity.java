package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.fragments.DummyHomeFragment;
import innasoft.com.mhcone.fragments.HomeFragment;
import innasoft.com.mhcone.utilities.CustomTypefaceSpan;
import innasoft.com.mhcone.utilities.NetworkChecking;
import innasoft.com.mhcone.utilities.UserSessionManager;

import java.util.HashMap;

public class StaticActivity extends AppCompatActivity implements  View.OnClickListener
{

    Typeface typeface,typeface2;
    TextView toolbar_title;
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    private boolean statusFlag;
    private Boolean exit = false;
    Toast toast;
    ProgressDialog progressDialog;

    /*Navigation Headerview*/
    ImageView navdrawer_image,profile_imag;
    TextView txtViewName,login,register_action;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.robotoregular));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.robotobold));

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        progressDialog.setCancelable(false);

        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(typeface2);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.static_drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.static_nav_view);

        View navigationheadderView = mNavigationView.getHeaderView(0);
        navdrawer_image = (ImageView) navigationheadderView.findViewById(R.id.navdrawer_image);
        profile_imag = (ImageView) navigationheadderView.findViewById(R.id.profile_imag);
        txtViewName = (TextView) navigationheadderView.findViewById(R.id.txtViewName);
        txtViewName.setTypeface(typeface);
        login = (TextView) navigationheadderView.findViewById(R.id.login);
        login.setOnClickListener(this);
        login.setTypeface(typeface2);

        register_action = (TextView) navigationheadderView.findViewById(R.id.register_action);
        register_action.setOnClickListener(this);
        register_action.setTypeface(typeface);

        Menu m = mNavigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            applyFontToMenuItem(mi);
        }

        mNavigationView.setItemIconTintList(null);

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();


        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.fragment_switch, new DummyHomeFragment()).commit();

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                mDrawerLayout.closeDrawers();

                if (menuItem.getItemId() == R.id.home) {

                    if (statusFlag = NetworkChecking.isConnected(getApplicationContext())) {
                        Intent intent = new Intent(getApplicationContext(), StaticActivity.class);
                        startActivity(intent);
                    } else {
                        Snackbar snack = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet connection...!", Snackbar.LENGTH_SHORT);
                        View view = snack.getView();
                        TextView tv = (TextView) view.findViewById(R.id.snackbar_text);
                        tv.setTextColor(Color.WHITE);
                        snack.show();
                    }
                }

               /* if (menuItem.getItemId() == R.id.login)
                {

                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                }

                if (menuItem.getItemId() == R.id.profile) {

                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                }*/

               /* if (menuItem.getItemId() == R.id.change_password) {

                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                }*/

                if (menuItem.getItemId() == R.id.nav_item_view_user) {

                    Intent intent = new Intent(getApplicationContext(), StaticLogin.class);
                    intent.putExtra("getMessage", "I Care For");
                    startActivity(intent);
                }

                if (menuItem.getItemId() == R.id.nav_item_habits) {

                    Intent intent = new Intent(getApplicationContext(), StaticLogin.class);
                    intent.putExtra("getMessage", "Habits Info");
                    startActivity(intent);
                }

                if (menuItem.getItemId() == R.id.nav_item_habits_filter) {

                    Intent intent = new Intent(getApplicationContext(), StaticLogin.class);
                    intent.putExtra("getMessage", "Habits Notifications Update ");
                    startActivity(intent);
                }

                if (menuItem.getItemId() == R.id.nav_item_myappointments) {

                    Intent intent = new Intent(getApplicationContext(), StaticLogin.class);
                    intent.putExtra("getMessage", "My Appointments");
                    startActivity(intent);
                }

                if (menuItem.getItemId() == R.id.nav_item_Packages) {

                    Intent intent = new Intent(getApplicationContext(), StaticLogin.class);
                    intent.putExtra("getMessage", "My Packages");
                    startActivity(intent);
                }

                if (menuItem.getItemId() == R.id.nav_item_view_Packages) {

                    Intent intent = new Intent(getApplicationContext(), StaticLogin.class);
                    intent.putExtra("getMessage", "View Package");
                    startActivity(intent);
                }

                if (menuItem.getItemId() == R.id.nav_item_share) {

                    if (statusFlag = NetworkChecking.isConnected(getApplicationContext()))
                    {
                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Healthlike");
                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=innasoft.com.mhcone&hl=en");
                        startActivity(Intent.createChooser(sharingIntent, "Share via"));
                    } else {
                        Snackbar snack = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet connection...!", Snackbar.LENGTH_SHORT);
                        View view = snack.getView();
                        TextView tv = (TextView) view.findViewById(R.id.snackbar_text);
                        tv.setTextColor(Color.WHITE);
                        snack.show();
                    }
                }

                if (menuItem.getItemId() == R.id.nav_item_rate)
                {
                    Uri uri = Uri.parse("market://details?id=innasoft.com.mhcone&hl=en");
                    Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);

                    try {
                        startActivity(goToMarket);

                    } catch (ActivityNotFoundException e) {
                        startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse("https://play.google.com/store/apps/details?id=innasoft.com.mhcone&hl=en")));
                    }
                }

                if (menuItem.getItemId() == R.id.nav_item_about)
                {

                    Intent intent = new Intent(getApplicationContext(), AboutUs.class);
                    startActivity(intent);
                }


                return false;
            }

        });

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name, R.string.app_name);

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();
    }


    private void applyFontToMenuItem(MenuItem mi) {
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.robotoregular));
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , typeface), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @Override
    public void onBackPressed() {

        FragmentManager manager = getSupportFragmentManager();

        if (this.mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.mDrawerLayout.closeDrawer(GravityCompat.START);

            if (exit) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                toast.cancel();
                startActivity(intent);
                moveTaskToBack(true);

            } else {

                LayoutInflater inflater = getLayoutInflater();
                View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
                textView.setTypeface(typeface);
                textView.setText("Press Back again to Exit.");
                toast = new Toast(getApplicationContext());

                manager.popBackStack();

                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setView(toastLayout);
                toast.show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 2 * 1000);
            }

        } else {

            if (exit) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                toast.cancel();
                startActivity(intent);

                moveTaskToBack(true);

            } else {

                LayoutInflater inflater = getLayoutInflater();
                View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
                textView.setTypeface(typeface);
                textView.setText("Press Back again to Exit.");
                toast = new Toast(getApplicationContext());

                manager.popBackStack();

                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setView(toastLayout);
                toast.show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 2 * 1000);
            }
        }
    }

    @Override
    public void onClick(View view)
    {
       if(view==register_action)
       {
           Intent intentRegister=new Intent(StaticActivity.this,RegisterActivity.class);
            startActivity(intentRegister);
       }

        if(view==login)
        {
            Intent intentRegister=new Intent(StaticActivity.this,LoginActivity.class);
            startActivity(intentRegister);
        }
    }
}
