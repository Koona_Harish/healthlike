package innasoft.com.mhcone.activities;

import android.app.DatePickerDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;


public class ReScheduleForm extends AppCompatActivity implements View.OnClickListener {

    TextView reschedule_Tittle,reschedule_Booking_Id,reschedule_Lab_Name,reschedule_Package_Name,reschedule_Test_Name,reschedule_Date,reschedule_Time;
    TextView booking_id,lab_name,package_name,test_name;
    ImageButton reschedule_Close;
    Button btn_Reschedule_Date,btn_Reschedule_Time,reschedule_Save;
    Typeface typeface,typeface2;
    public int mYear, mMonth, mDay, mHour, mMinute,rmdyear, rmdmonth, rmdday, rmdhour, rmdmint;
    String sendDate,sendTime;
    String BookingId,LabName,PackageName,TestName;
    ProgressDialog progressDialog;
    String schedule_date_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_re_schedule_form2);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Re-Schedule Form");

        progressDialog = new ProgressDialog(ReScheduleForm.this, R.style.DialogTheme);
        progressDialog.setMessage("Please wait......");

        Bundle bundle = getIntent().getExtras();
        BookingId = bundle.getString("bookingId");
        LabName = bundle.getString("lab_name");
        PackageName = bundle.getString("package_name");
        TestName = bundle.getString("test_name");
        schedule_date_time = bundle.getString("schedule_date_time");

        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typeface2 = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        reschedule_Tittle = (TextView) findViewById(R.id.reschedule_tittle);
        reschedule_Tittle.setTypeface(typeface2);

        booking_id = (TextView) findViewById(R.id.booking_id);
        booking_id.setTypeface(typeface);

        lab_name = (TextView) findViewById(R.id.lab_name);
        lab_name.setTypeface(typeface);

        package_name = (TextView) findViewById(R.id.package_name);
        package_name.setTypeface(typeface);

        test_name = (TextView) findViewById(R.id.test_name);
        test_name.setTypeface(typeface);

        reschedule_Booking_Id = (TextView) findViewById(R.id.reschedule_booking_id);
        reschedule_Booking_Id.setTypeface(typeface2);
        reschedule_Booking_Id.setText(BookingId);

        reschedule_Lab_Name = (TextView) findViewById(R.id.reschedule_lab_name);
        reschedule_Lab_Name.setTypeface(typeface2);
        reschedule_Lab_Name.setText(LabName);

        reschedule_Package_Name = (TextView) findViewById(R.id.reschedule_package_name);
        reschedule_Package_Name.setTypeface(typeface2);
        if(PackageName.equals("null"))
        {
            reschedule_Package_Name.setVisibility(View.GONE);
            package_name.setVisibility(View.GONE);
        }
        else {
            reschedule_Package_Name.setText(PackageName);
        }

        reschedule_Test_Name = (TextView) findViewById(R.id.reschedule_test_name);
        reschedule_Test_Name.setTypeface(typeface2);
        if(TestName.equals("null"))
        {
            reschedule_Test_Name.setVisibility(View.GONE);
            test_name.setVisibility(View.GONE);
        }
        else {
            reschedule_Test_Name.setText(TestName);
        }

        reschedule_Date = (TextView) findViewById(R.id.reschedule_date);
        reschedule_Date.setTypeface(typeface);

        reschedule_Time = (TextView) findViewById(R.id.reschedule_time);
        reschedule_Time.setTypeface(typeface);

        reschedule_Close = (ImageButton) findViewById(R.id.reschedule_close);

        btn_Reschedule_Date = (Button) findViewById(R.id.btn_reschedule_date);
        btn_Reschedule_Date.setTypeface(typeface);

        btn_Reschedule_Time = (Button) findViewById(R.id.btn_reschedule_time);
        btn_Reschedule_Time.setTypeface(typeface);

        reschedule_Save = (Button) findViewById(R.id.reschedule_save);
        reschedule_Save.setTypeface(typeface2);

        reschedule_Close.setOnClickListener(this);
        btn_Reschedule_Date.setOnClickListener(this);
        btn_Reschedule_Time.setOnClickListener(this);
        reschedule_Save.setOnClickListener(this);


    }



    private void reschedule() {

        if (sendDate != null && sendTime != null)
        {
            progressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.ORDER_RESCHDULE, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {

                        Log.d("RESCHDULE ", "RESPONCE "+response);
                        JSONObject jsonObject = new JSONObject(response);
                        LayoutInflater inflater = getLayoutInflater();
                        View toastLayout = inflater.inflate(R.layout.custom_toast_package, (ViewGroup) findViewById(R.id.custom_toast_layout));
                        TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
                        textView.setTypeface(typeface);
                        textView.setText("Successfully Re-Schedule...!");
                        Toast toast = new Toast(getApplicationContext());
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.setDuration(Toast.LENGTH_LONG);
                        toast.setView(toastLayout);
                        toast.show();


                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);


                        progressDialog.cancel();




                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    progressDialog.cancel();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.cancel();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {


                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("booking_id", BookingId);
                    params.put("reschedule_date", sendDate + " " + sendTime);
                    params.put("booking_date", schedule_date_time);
                    Log.d("RESCHDULE ", "PARAMS "+params.toString());

                    return params;
                }
            };
            stringRequest.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);
        }
        else {
            Toast.makeText(getApplicationContext(), "Please provide all data...!", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onClick(View v) {
        if (v == reschedule_Close)
        {
            finish();
        }
        if (v == btn_Reschedule_Date)
        {
            final java.util.Calendar c = java.util.Calendar.getInstance();
            mYear = c.get(java.util.Calendar.YEAR);
            mMonth = c.get(java.util.Calendar.MONTH);
            mDay = c.get(java.util.Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this,android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            rmdyear = year;
                            rmdmonth = monthOfYear + 1;
                            rmdday = dayOfMonth;
                            sendDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                            reschedule_Date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        }

                    }, mYear, mMonth, mDay);

            datePickerDialog .getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

            datePickerDialog.show();

        }
        if (v == btn_Reschedule_Time)
        {
            final java.util.Calendar c = java.util.Calendar.getInstance();
            mHour = c.get(java.util.Calendar.HOUR_OF_DAY);
            mMinute = c.get(java.util.Calendar.MINUTE);

            final TimePickerDialog timePickerDialog = new TimePickerDialog(this,android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            rmdhour = Integer.valueOf(String.format("%02d", hourOfDay));
                            rmdmint = Integer.valueOf(String.format("%02d", minute));
                            String hourchange = String.format("%02d", hourOfDay);
                            String minitChange = String.format("%02d", minute);
                            String disptime = updateTime(hourOfDay, minute);
                            sendTime = disptime;
                            reschedule_Time.setText(disptime);

                        }
                    }, mHour, mMinute, false);
            timePickerDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialogInterface) {
                    timePickerDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setVisibility(View.GONE);
                }
            });

            timePickerDialog.show();

        }
        if (v == reschedule_Save)
        {
            reschedule();


        }
    }
    private String updateTime(int hours, int mins) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";

        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);
        String hourss = "";
        if(hours < 10)
            hourss = "0"  + hours;
        else
            hourss = String.valueOf(hours);


        String aTime = new StringBuilder().append(hourss).append(':')
                .append(minutes).append(" ").append(timeSet).toString();

        return aTime;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
