package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class UserPackageConfirmationActivity extends AppCompatActivity implements View.OnClickListener {

    Typeface type_lato,typebold;
    String FIRST_NAME,PHONE_NUMBER,phoneNumber,EMAIL_ADDRESS,RECHARGE_AMT,rmdyear,rmdmonth,rmdday,rmdhour,rmdmint,descr,user_id,lab_id,payment_type,booking_date,
            user_count,price,grand_total,form_type,package_id,sub_user_id,type,labName,sendpackTestandSubIDswithformat;

    /*Payment Methods*/
    String status_payment_lab,status_payment_online,status_tele_booking;
    ProgressDialog progressDialog;

    RelativeLayout tele_book_rl,cal_rl,billing_rl;
    Button bookpackage_btn_billing_cal,bookpackage_btn_billing_pay,bookpackage_btn_tele_book;
    TextView confirm_txt,confirm_bill_txt,payment_opt_txt,tv_tele_txt,tv_pal_txt,tv_pay_txt,tv_tele,tv_pal,tv_pay;
    Boolean isOnline = false;
    NetworkStatus ns;

    UserSessionManager userSessionManager;
    Button cancel_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_package_confirmation);

        type_lato = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typebold = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        progressDialog = new ProgressDialog(UserPackageConfirmationActivity.this, R.style.DialogTheme);
        progressDialog.setMessage("Please Wait......");
        progressDialog.setCancelable(false);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Payment Confirmation");

        ns = new NetworkStatus();
        isOnline = ns.isOnline(UserPackageConfirmationActivity.this);

        Bundle bundle = getIntent().getExtras();
        FIRST_NAME = bundle.getString("FIRST_NAME");
        PHONE_NUMBER = bundle.getString("PHONE_NUMBER");
        phoneNumber = bundle.getString("phoneNumber");
        EMAIL_ADDRESS = bundle.getString("EMAIL_ADDRESS");
        RECHARGE_AMT = bundle.getString("RECHARGE_AMT");
        rmdyear = bundle.getString("rmdyear");
        rmdmonth = bundle.getString("rmdmonth");
        rmdday = bundle.getString("rmdday");
        rmdhour = bundle.getString("rmdhour");
        rmdmint = bundle.getString("rmdmint");
        descr = bundle.getString("descr");
        user_id = bundle.getString("user_id");
        lab_id = bundle.getString("lab_id");
        payment_type = bundle.getString("payment_type");
        booking_date = bundle.getString("booking_date");
        user_count = bundle.getString("user_count");
        price = bundle.getString("price");
        grand_total = bundle.getString("grand_total").replace(".0","");
        form_type = bundle.getString("form_type");
        package_id = bundle.getString("package_id");
        sub_user_id = bundle.getString("sub_user_id");
        type = bundle.getString("type");
        labName = bundle.getString("labName");
        sendpackTestandSubIDswithformat = bundle.getString("sendpackTestandSubIDswithformat");

        status_payment_lab = bundle.getString("status_payment_lab");
        status_payment_online = bundle.getString("status_payment_online");
        status_tele_booking = bundle.getString("status_tele_booking");

        Log.d("BUNDLE",FIRST_NAME+"\n"+PHONE_NUMBER+"\n"+EMAIL_ADDRESS+"\n"+RECHARGE_AMT+"\n"+rmdyear+"\n"+rmdmonth+"\n"+rmdday+"\n"+rmdhour+"\n"+
                rmdmint+"\n"+descr+"\n"+user_id+"\n"+lab_id+"\n"+payment_type+"\n"+booking_date+"\n"+user_count+"\n"+price+"\n"+grand_total+"\n"+
                form_type+"\n"+package_id+"\n"+sub_user_id+"\n"+type+"\n"+status_payment_lab+"\n"+status_payment_online+"\n"+status_tele_booking);

        tele_book_rl = (RelativeLayout) findViewById(R.id.tele_book_rl);
        tele_book_rl.setOnClickListener(this);
        cal_rl = (RelativeLayout) findViewById(R.id.cal_rl);
        cal_rl.setOnClickListener(this);
        billing_rl = (RelativeLayout) findViewById(R.id.billing_rl);
        billing_rl.setOnClickListener(this);
        bookpackage_btn_tele_book = (Button) findViewById(R.id.bookpackage_btn_tele_book);
        bookpackage_btn_tele_book.setTypeface(type_lato);
        bookpackage_btn_tele_book.setOnClickListener(this);
        bookpackage_btn_billing_cal = (Button) findViewById(R.id.bookpackage_btn_billing_cal);
        bookpackage_btn_billing_cal.setTypeface(type_lato);
        bookpackage_btn_billing_cal.setOnClickListener(this);
        bookpackage_btn_billing_pay = (Button) findViewById(R.id.bookpackage_btn_billing);
        bookpackage_btn_billing_pay.setTypeface(type_lato);
        bookpackage_btn_billing_pay.setOnClickListener(this);

        confirm_txt = (TextView) findViewById(R.id.confirm_txt);
        confirm_txt.setTypeface(typebold);
        confirm_bill_txt = (TextView) findViewById(R.id.confirm_bill_txt);
        confirm_bill_txt.setTypeface(typebold);
        confirm_bill_txt.setText("Rs. "+grand_total);
        payment_opt_txt = (TextView) findViewById(R.id.payment_opt_txt);
        payment_opt_txt.setTypeface(typebold);
        SpannableString content = new SpannableString("Choose Payment Mode");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        payment_opt_txt.setText(content);
        tv_tele = (TextView) findViewById(R.id.tv_tele);
        tv_tele.setTypeface(typebold);
        tv_tele_txt = (TextView) findViewById(R.id.tv_tele_txt);
        tv_tele_txt.setTypeface(type_lato);
        tv_pal = (TextView) findViewById(R.id.tv_pal);
        tv_pal.setTypeface(typebold);
        tv_pal_txt = (TextView) findViewById(R.id.tv_pal_txt);
        tv_pal_txt.setTypeface(type_lato);
        tv_pay = (TextView) findViewById(R.id.tv_pay);
        tv_pay.setTypeface(typebold);
        tv_pay_txt = (TextView) findViewById(R.id.tv_pay_txt);
        tv_pay_txt.setTypeface(type_lato);

        if(status_payment_lab.equals("0"))
        {
            cal_rl.setVisibility(View.GONE);
            bookpackage_btn_billing_cal.setVisibility(View.GONE);
            tv_pal.setVisibility(View.GONE);
        }else {
            cal_rl.setVisibility(View.VISIBLE);
            bookpackage_btn_billing_cal.setVisibility(View.VISIBLE);
            tv_pal.setVisibility(View.VISIBLE);
        }

        if(status_tele_booking.equals("0"))
        {
            tele_book_rl.setVisibility(View.GONE);
            bookpackage_btn_tele_book.setVisibility(View.GONE);
            tv_tele.setVisibility(View.GONE);
        }else {
            tele_book_rl.setVisibility(View.VISIBLE);
            bookpackage_btn_tele_book.setVisibility(View.VISIBLE);
            tv_tele.setVisibility(View.VISIBLE);
        }
        if(status_payment_online.equals("0"))
        {
            billing_rl.setVisibility(View.GONE);
            bookpackage_btn_billing_pay.setVisibility(View.GONE);
            tv_pay.setVisibility(View.GONE);
        }else {
            billing_rl.setVisibility(View.VISIBLE);
            bookpackage_btn_billing_pay.setVisibility(View.VISIBLE);
            tv_pay.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        if(v == tele_book_rl)
        {
            makeACall(phoneNumber);
        }

        if (v == cal_rl){
            bookingButtonBillingCall();
        }

        if (v == billing_rl){
            bookingButtonBillingPay();
        }

        if(v == bookpackage_btn_tele_book)
        {
            makeACall(phoneNumber);
        }

        if(v == bookpackage_btn_billing_cal)
        {
            bookingButtonBillingCall();
        }

        if (v == bookpackage_btn_billing_pay) {
            bookingButtonBillingPay();
        }
    }

    public void makeACall(String phone_number){
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:"+Uri.encode(phoneNumber.trim())));
        Log.d("HHHHHHHH",callIntent.toString());
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(callIntent);
    }

    public void bookingButtonBillingCall(){
        if (isOnline) {

            progressDialog.show();

            bookpackage_btn_billing_cal.setClickable(false);

            StringRequest stringRequest2 = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>()
            {
                @Override
                public void onResponse(String response)
                {
                    try {
                        JSONObject jsonObject =new JSONObject(response);
                        String responceCode = jsonObject.getString("status");
                        if(responceCode.equals("19999"))
                        {
                            progressDialog.show();
                            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BILLINGURL,
                                    new Response.Listener<String>() {

                                        @Override
                                        public void onResponse(String response) {

                                            try {
                                                JSONObject jsonObject = new JSONObject(response);
                                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                                String paymentType = jsonObject1.getString("payment-type");
                                                String bookingId = jsonObject1.getString("booking-id");
                                                String message = jsonObject1.getString("msg");
                                                progressDialog.dismiss();

                                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(getApplicationContext(), BookingSuccessPage.class);
                                                intent.putExtra("bookingId", bookingId);

                                                intent.putExtra("rmdyear", rmdyear + "");
                                                intent.putExtra("rmdmonth", rmdmonth + "");
                                                intent.putExtra("rmdday", rmdday + "");
                                                intent.putExtra("rmdhour", rmdhour + "");
                                                intent.putExtra("rmdmint", rmdmint + "");
                                                intent.putExtra("descr", "You are book " + labName + " Appointment");
                                                startActivity(intent);

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {

                                            progressDialog.dismiss();

                                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                            } else if (error instanceof AuthFailureError) {

                                            } else if (error instanceof ServerError) {

                                            } else if (error instanceof NetworkError) {

                                            } else if (error instanceof ParseError) {
                                            }
                                        }
                                    })
                            {
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    Map<String, String> params = new HashMap<String, String>();
                                    params.put("user_id", user_id);
                                    params.put("lab_id", lab_id);
                                    params.put("payment_type", "cal");
                                    //params.put("booking_date", sendDate + " " + sendTime);
                                    params.put("booking_date", booking_date);
                                    params.put("user_count", user_count);
                                    params.put("transaction_id", "0");
                                    params.put("price", price);
                                    params.put("grand_total", grand_total);
                                    params.put("form_type", "package");
                                    //Log.d("BOOKINGINFO", "PACKAGESSBOOKING "+sendpackTestandSubIDswithformat.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", ""));
                                    params.put("package_id", sendpackTestandSubIDswithformat);
                                    params.put("sub_user_id", sub_user_id);
                                    params.put("type", "testlab");
                                    return params;
                                }
                            };
                            stringRequest.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );
                            RequestQueue requestQueue = Volley.newRequestQueue(UserPackageConfirmationActivity.this);
                            requestQueue.add(stringRequest);
                        }
                        if(responceCode.equals("10140"))
                        {
                            userSessionManager.logoutUser();
                            Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        }

                        if(responceCode.equals("10150"))
                        {
                            userSessionManager.logoutUser();
                            Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
            })
            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError
                {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("user_id",user_id);
                    return params;
                }
            };
            stringRequest2.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );

            RequestQueue requestQueue2 = Volley.newRequestQueue(UserPackageConfirmationActivity.this);
            requestQueue2.add(stringRequest2);

        }else {
            showInternetStatus();
        }
    }

    public void bookingButtonBillingPay(){
        if (isOnline) {
            bookpackage_btn_billing_pay.setClickable(false);
            StringRequest stringRequest2 = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>()
            {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();

                    try {
                        JSONObject jsonObject =new JSONObject(response);
                        String responceCode = jsonObject.getString("status");
                        if(responceCode.equals("19999"))
                        {

                            Intent intent = new Intent(getApplicationContext(), PayMentGateWay.class);
                            intent.putExtra("FIRST_NAME",FIRST_NAME);
                            intent.putExtra("PHONE_NUMBER",PHONE_NUMBER);
                            intent.putExtra("EMAIL_ADDRESS",EMAIL_ADDRESS);
                            intent.putExtra("RECHARGE_AMT",RECHARGE_AMT);
                            intent.putExtra("rmdyear", rmdyear+"");
                            intent.putExtra("rmdmonth", rmdmonth+"");
                            intent.putExtra("rmdday", rmdday+"");
                            intent.putExtra("rmdhour", rmdhour+"");
                            intent.putExtra("rmdmint", rmdmint+"");
                            intent.putExtra("descr", labName);
                            intent.putExtra("user_id", user_id);
                            intent.putExtra("lab_id", lab_id);
                            intent.putExtra("payment_type", "payment");
                            intent.putExtra("booking_date", booking_date);
                            intent.putExtra("user_count", user_count);
                            intent.putExtra("price", price);
                            intent.putExtra("grand_total", grand_total);
                            intent.putExtra("form_type", "test");
                            intent.putExtra("package_id", sendpackTestandSubIDswithformat);
                            intent.putExtra("sub_user_id", sub_user_id);
                            intent.putExtra("type", "testlab");
                            startActivity(intent);
                        }
                        if(responceCode.equals("10140"))
                        {
                            userSessionManager.logoutUser();
                            Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        }

                        if(responceCode.equals("10150"))
                        {
                            userSessionManager.logoutUser();
                            Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
            })
            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError
                {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("user_id",user_id);
                    return params;
                }
            };
            stringRequest2.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );
            RequestQueue requestQueue2 = Volley.newRequestQueue(UserPackageConfirmationActivity.this);
            requestQueue2.add(stringRequest2);

        }
    }

    private void showInternetStatus() {

        LayoutInflater inflater = getLayoutInflater();
        View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
        TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
        textView.setTypeface(type_lato);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(toastLayout);
        toast.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
