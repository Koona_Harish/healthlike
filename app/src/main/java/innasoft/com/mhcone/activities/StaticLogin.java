package innasoft.com.mhcone.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import innasoft.com.mhcone.R;

public class StaticLogin extends AppCompatActivity implements View.OnClickListener {

    ImageButton closeImageButton;
    TextView textViewforHeadding,textSuggest;
    Typeface typeface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_login);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");

        Bundle bundle = getIntent().getExtras();
        String message = bundle.getString("getMessage");

        closeImageButton = (ImageButton) findViewById(R.id.static_login_close);
        textSuggest = (TextView) findViewById(R.id.suggetion);
        textSuggest.setTypeface(typeface);
        textViewforHeadding = (TextView) findViewById(R.id.txt_package_name_headding);
        textViewforHeadding.setTypeface(typeface);
       // textViewforHeadding.setText(message);
        getSupportActionBar().setTitle(message);

        closeImageButton.setOnClickListener(this);
    }

    public void gotoLoginActivity(View view) {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        if(v == closeImageButton)
        {
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;



            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
