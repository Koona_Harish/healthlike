package innasoft.com.mhcone.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.RelatedHabitsListAdapter;
import innasoft.com.mhcone.models.HabitsListModel;
import innasoft.com.mhcone.models.RelatedHabitsListModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class HabitInformation extends AppCompatActivity implements View.OnClickListener {
    ImageView   habitInformation_image;
    ImageView habitInformation_cancel;
    TextView    habitInformation_txt,habitInformation,related_tittle_txt;
    WebView habitInformation_description;
    HabitsListModel habitsListModel;
    Typeface typeface,typeface2;
    UserSessionManager userSessionManager;
    String userID,hab_id;
    String habit_id,habitInformationCheck;
    RelatedHabitsListModel rhabitsListModel;

    RecyclerView relatedArticalsrecyclerview;
    ArrayList<RelatedHabitsListModel> relatedHabitsListModels;
    RelatedHabitsListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_habit_information);

        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typeface2 = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Article Information");

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = userSessionManager.getUserDetails();
        userID = user.get(UserSessionManager.USER_ID);

        habitInformation_cancel = (ImageView) findViewById(R.id.habit_information_cancel_btn);
        habitInformation_cancel.setOnClickListener(this);

        related_tittle_txt = (TextView) findViewById(R.id.related_tittle_txt);
        related_tittle_txt.setTypeface(typeface);

        Bundle bundle = getIntent().getExtras();

        habitInformationCheck = bundle.getString("habitInformationCheck");
        if (habitInformationCheck.equals("1")) {

            habitsListModel = (HabitsListModel) bundle.getSerializable("habitInfo");

            String habitTitle = habitsListModel.getHabitsName();
            String habitDesc = habitsListModel.getHabitsDescription();
            String habitImageUrl = habitsListModel.getHabitsImage();
            habit_id = habitsListModel.getHabitsCategory_id();
            hab_id =  habitsListModel.getHabit_id();



        /*habitInformation_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/

            habitInformation_image = (ImageView) findViewById(R.id.habit_information_image);
            habitInformation = (TextView) findViewById(R.id.habit_information);
            habitInformation.setTypeface(typeface2);

            habitInformation_txt = (TextView) findViewById(R.id.habit_information_txt);
            habitInformation_txt.setText(Html.fromHtml(habitTitle));
            habitInformation_txt.setTypeface(typeface2);

            Picasso.with(getApplicationContext())
                .load(habitImageUrl)
                .placeholder(R.drawable.myhealthlogo)
                .fit()
                .into(habitInformation_image);

            habitInformation_description = (WebView) findViewById(R.id.habit_information_description);
            WebSettings webSettings = habitInformation_description.getSettings();
            webSettings.setDefaultFontSize(12);
            habitInformation_description.getSettings().setJavaScriptEnabled(true);
            habitInformation_description.loadData(habitDesc, "text/html", "UTF-8");

            relatedHabitsListModels = new ArrayList<RelatedHabitsListModel>();
            relatedArticalsrecyclerview = (RecyclerView) findViewById(R.id.related_articles_list_recyclerview);
            relatedArticalsrecyclerview.setHasFixedSize(true);

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            relatedArticalsrecyclerview.setLayoutManager(layoutManager);
            adapter = new RelatedHabitsListAdapter(relatedHabitsListModels, HabitInformation.this, R.layout.row_related_habits);
            relateddata();
    }
        if(habitInformationCheck.equals("2")) {
            Log.d("CHANGES@@@@@@@", "SDFAFDAFAFAF");
            rhabitsListModel = (RelatedHabitsListModel) bundle.getSerializable("habitInfo");

            String habitTitle = rhabitsListModel.getRelatedhabitsName();
            String habitDesc = rhabitsListModel.getRelatedhabitsDescription();
            String habitImageUrl = rhabitsListModel.getRelatedhabitsImage();

            habit_id = rhabitsListModel.getRelatedhabitsCategory_id();
            hab_id = rhabitsListModel.getReHabitId();

            /*habitInformation_cancel = (ImageButton) findViewById(R.id.habit_information_cancel_btn);
            habitInformation_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });*/

            habitInformation_image = (ImageView) findViewById(R.id.habit_information_image);

            habitInformation = (TextView) findViewById(R.id.habit_information);
            habitInformation.setTypeface(typeface2);

            habitInformation_txt = (TextView) findViewById(R.id.habit_information_txt);
            habitInformation_txt.setText(Html.fromHtml(habitTitle));
            habitInformation_txt.setTypeface(typeface2);

            Picasso.with(getApplicationContext())
                    .load(habitImageUrl)
                    .placeholder(R.drawable.myhealthlogo)
                    .fit()
                    .into(habitInformation_image);

            habitInformation_description = (WebView) findViewById(R.id.habit_information_description);

            habitInformation_description.getSettings().setJavaScriptEnabled(true);
            habitInformation_description.loadData(habitDesc, "text/html", "UTF-8");

            relatedHabitsListModels = new ArrayList<RelatedHabitsListModel>();
            relatedArticalsrecyclerview = (RecyclerView) findViewById(R.id.related_articles_list_recyclerview);
            relatedArticalsrecyclerview.setHasFixedSize(true);

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            relatedArticalsrecyclerview.setLayoutManager(layoutManager);
            adapter = new RelatedHabitsListAdapter(relatedHabitsListModels, HabitInformation.this, R.layout.row_related_habits);
            relateddata();

        }
    }

    private void relateddata() {

        relatedHabitsListModels.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.RELATED_HABITS, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                try
                {
                    JSONObject jsonObject =new JSONObject(response);
                    Log.d("RELATEDARTICALS", response);
                    String status = jsonObject.getString("status");

                    if(status.equals("19999")) {
                        related_tittle_txt.setVisibility(View.VISIBLE);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("habit_user_related");
                            RelatedHabitsListModel hlm = new RelatedHabitsListModel();
                            hlm.setRelatedhabitsName(jsonObject1.getString("title"));
                            hlm.setReHabitId(jsonObject1.getString("id"));
                            hlm.setRelatedhabitsCategory_id(jsonObject1.getString("habits_category"));
                            hlm.setRelatedhabitsDescription(jsonObject1.getString("description"));
                            hlm.setRelatedhabitsImage(AppUrls.IMAGE_URL + jsonObject1.getString("habit_image"));
                            relatedHabitsListModels.add(hlm);
                        }
                        relatedArticalsrecyclerview.setAdapter(adapter);
                    }

                    if(status.equals("18888")){
                        related_tittle_txt.setVisibility(View.GONE);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                Log.d("SENDEDVALUES", userID+"----"+hab_id+"-----"+habit_id);
                params.put("user_id",userID);
                params.put("habit_category_id", habit_id);//habit_category_id
                params.put("habit_id",hab_id); //id
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v)
    {
        if(v==habitInformation_cancel)
        {

            finish();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

