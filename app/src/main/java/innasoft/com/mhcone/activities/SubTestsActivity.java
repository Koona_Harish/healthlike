package innasoft.com.mhcone.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.renderscript.Sampler;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.SubTestAdapter;
import innasoft.com.mhcone.holders.SubTestHolder;
import innasoft.com.mhcone.models.SubTestModel;
import innasoft.com.mhcone.models.TestsModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.DBHandler;
import innasoft.com.mhcone.utilities.GPSTracker;
import innasoft.com.mhcone.utilities.GPSTrackers;
import innasoft.com.mhcone.utilities.StartLocationAlert;

public class SubTestsActivity extends FragmentActivity implements SubTestHolder.MyViewHolder {
    ImageButton closeButton;
    TestsModel testsModel;
    TextView display_test_name;
    Typeface typeface;
    ProgressDialog progressDialog;
    ArrayList<SubTestModel> subTestModels;
    SubTestAdapter adapter;
    RecyclerView subTextrecyclerView;
    String test_id;

    Button showSeelctedSubTests,selectLabButton;

    GPSTrackers gps;
    StartLocationAlert startLocationAlert;
    DBHandler dbHandler;
    final static int SHOW_LABS_REQ_CODE = 26;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.sendbird_slide_in_from_bottom, R.anim.sendbird_slide_out_to_top);
        setContentView(R.layout.activity_sub_tests);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");

        progressDialog.setProgressStyle(R.style.DialogTheme);



        dbHandler = new DBHandler(getApplicationContext());
        display_test_name = (TextView) findViewById(R.id.txt_test_name);



        Bundle bundle = getIntent().getExtras();
        testsModel = (TestsModel) bundle.getSerializable("test_id");

        display_test_name.setTypeface(typeface);
        display_test_name.setText(testsModel.getTest_name());

        test_id = testsModel.getTest_id();



        showSeelctedSubTests = (Button) findViewById(R.id.btnShowSubTests);
        selectLabButton = (Button) findViewById(R.id.btnSelectLab);



        subTestModels = new ArrayList<SubTestModel>();
        subTextrecyclerView = (RecyclerView) findViewById(R.id.sub_tests_recycler_view);
        subTextrecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        subTextrecyclerView.setLayoutManager(layoutManager);
        adapter = new SubTestAdapter(subTestModels, SubTestsActivity.this, R.layout.row_sub_test_items);
        gettingSubTestList();





        closeButton = (ImageButton) findViewById(R.id.btn_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                finish();
                overridePendingTransition( R.anim.sendbird_slide_out_to_top , R.anim.sendbird_slide_in_from_bottom);
            }
        });

        showSeelctedSubTests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int x = subTestModels.size();
                for (int i = 0; i < x ; i++)
                {


                }


                finish();
                overridePendingTransition( R.anim.sendbird_slide_out_to_top , R.anim.sendbird_slide_in_from_bottom);
            }
        });
        selectLabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                gps = new GPSTrackers(SubTestsActivity.this);
                if(gps.canGetLocation()){

                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();

                    // \n is for new line
                    List<String> subtestIds = dbHandler.getAllSubTestIds();
                    List<String> subtestName = dbHandler.getAllSubTestNames();
                    List<String> testIds = dbHandler.getAllTestIds();



                    if(subtestIds.size() != 0) {

                        String subTestIDString = subtestIds.toString();
                        String subtestNames = subtestName.toString();
                        String SendSubTestIDs = subTestIDString.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", "");
                        String sendSubtestNames = subtestNames.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", "");

                        progressDialog.dismiss();

                        Intent intent = new Intent(getApplicationContext(), ShowLabsTest.class);
                        intent.putExtra("latValue", Double.toString(latitude));
                        intent.putExtra("longValue", Double.toString(longitude));
                        intent.putExtra("subTestId", SendSubTestIDs);
                        intent.putExtra("subTestName", sendSubtestNames);
                        Bundle args = new Bundle();
                        args.putSerializable("arraylistsubtestNames",(Serializable)subtestName);
                        args.putSerializable("arraylistsubtestIds", (Serializable)subtestIds);
                        args.putSerializable("arraylisttestIds", (Serializable)testIds);
                        intent.putExtra("bundelData", args);

                        //intent.putStringArrayListExtra("arraylistsubtestNames", ArrayList<String>subtestName);
                     //   startActivityForResult(intent, SHOW_LABS_REQ_CODE);
                        startActivity(intent);
                    }
                    else {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Please Select Atleast One Sub Tests...!", Toast.LENGTH_SHORT).show();
                    }
                }else{

                    progressDialog.dismiss();
                    startLocationAlert = new StartLocationAlert(SubTestsActivity.this);

                }
            }
        });


    }

    private void gettingSubTestList() {

        SubTestsActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.show();

            }
        });



        StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.SUB_TESTS_URL+test_id,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        subTestModels.clear();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0 ; i < jsonArray.length(); i++)
                            {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("test-sub");
                                SubTestModel stm = new SubTestModel();
                                stm.setSub_test_id(jsonObject1.getString("id"));
                                stm.setSub_test_name(jsonObject1.getString("test_sub_name"));
                                stm.setTest_id(jsonObject1.getString("test_id"));
                                stm.setTest_name(jsonObject1.getString("test_name"));

                                stm.setSub_test_status(jsonObject1.getString("status"));
                                subTestModels.add(stm);
                            }
                            subTextrecyclerView.setAdapter(adapter);

                            SubTestsActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    progressDialog.dismiss();

                                }
                            });




                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {


                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }

                    }
                }
        ){

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void cardViewListener(int position) {


            ArrayList<String> arrayList = null;




    }


}
