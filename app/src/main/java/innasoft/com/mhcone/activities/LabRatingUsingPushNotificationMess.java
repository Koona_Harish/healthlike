package innasoft.com.mhcone.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class LabRatingUsingPushNotificationMess extends AppCompatActivity implements View.OnClickListener {
    ImageView gotoHome;
    UserSessionManager session;
    String userId;
    TextView rating_labName;
    RatingBar lab_user_ratingbar;
    EditText comments_et;
    Button submitRating;
    String message,imageUrl,labName,labId,another,serveruserID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lab_rating_using_push_notification_mess);

        Bundle bundle = getIntent().getExtras();
        message = bundle.getString("message");
        imageUrl = bundle.getString("imageUrl");
        labName = bundle.getString("title");
        labId = bundle.getString("title2");
        serveruserID = bundle.getString("title3");
        another = bundle.getString("AnotherActivity");

        gotoHome = (ImageView) findViewById(R.id.notification_packages_cancel_btn);
        rating_labName = (TextView) findViewById(R.id.rating_labName);
        lab_user_ratingbar = (RatingBar) findViewById(R.id.lab_user_ratingbar);
        comments_et = (EditText) findViewById(R.id.comments_et);
        submitRating = (Button) findViewById(R.id.submitRating);
        rating_labName.setText(labName);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();

        userId = user.get(UserSessionManager.USER_ID);

        submitRating.setOnClickListener(this);
        gotoHome.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == gotoHome)
        {
//            if(session.checkLogin() != false) {
//                Intent i = new Intent(LabRatingUsingPushNotificationMess.this, StaticActivity.class);
//                startActivity(i);
//
//            }
//            else {
                Intent i = new Intent(LabRatingUsingPushNotificationMess.this, MainActivity.class);
                startActivity(i);
//            }
        }
        if(v == submitRating) {
            final String comment = comments_et.getText().toString();
            final float ratting = lab_user_ratingbar.getRating();
            String rattingvalueStr = ratting+"";

            if (!rattingvalueStr.equals("0.0")) {
                if (ratting <= 3) {
                    if (comment.trim().length() != 0 && !comment.equals(null) && ratting != 0) {
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.RATTING_ON_LAB,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                        try {
                                            JSONObject jsonObject = new JSONObject(response);

                                            Toast.makeText(LabRatingUsingPushNotificationMess.this, "Thank you for your participation...!", Toast.LENGTH_SHORT).show();
                                            Intent in = new Intent(LabRatingUsingPushNotificationMess.this, MainActivity.class);
                                            startActivity(in);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                        } else if (error instanceof AuthFailureError) {

                                        } else if (error instanceof ServerError) {

                                        } else if (error instanceof NetworkError) {

                                        } else if (error instanceof ParseError) {

                                        }
                                    }
                                })
                        {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {

                                Map<String, String> params = new HashMap<String, String>();

                                params.put("user_id", serveruserID);
                                params.put("lab_id", labId);
                                params.put("rating", ratting + "");
                                params.put("comments", comment + "");

                                return params;
                            }
                        };
                        RequestQueue requestQueue = Volley.newRequestQueue(this);
                        requestQueue.add(stringRequest);
                    } else {
                        Toast.makeText(getApplicationContext(), "Please provide your feedback", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.RATTING_ON_LAB,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);

                                        Toast.makeText(LabRatingUsingPushNotificationMess.this, "Thank you for your participation...!", Toast.LENGTH_SHORT).show();
                                        Intent in = new Intent(LabRatingUsingPushNotificationMess.this, MainActivity.class);
                                        startActivity(in);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    } else if (error instanceof AuthFailureError) {

                                    } else if (error instanceof ServerError) {

                                    } else if (error instanceof NetworkError) {

                                    } else if (error instanceof ParseError) {

                                    }

                                }
                            })
                    {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();

                            params.put("user_id", serveruserID);
                            params.put("lab_id", labId);
                            params.put("rating", ratting + "");
                            params.put("comments", comment + "");

                            return params;
                        }
                    };
                    RequestQueue requestQueue = Volley.newRequestQueue(this);
                    requestQueue.add(stringRequest);
                }
            }
            else {
                Toast.makeText(getApplicationContext(), "Please provide your feedback", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
