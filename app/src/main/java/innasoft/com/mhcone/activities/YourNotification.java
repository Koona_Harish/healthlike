package innasoft.com.mhcone.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;


import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class YourNotification extends AppCompatActivity {

    TextView yourNotification_txt,titleofYourNotification,yourNotification_desp;
    ImageButton yournotification_home_btn;
    UserSessionManager session;
    Typeface typeface,typebold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_notification);




        Bundle bundle = getIntent().getExtras();
        String tittle = bundle.getString("title");
        String msg = bundle.getString("message");

        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typebold = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        session = new UserSessionManager(getApplicationContext());

        yourNotification_txt = (TextView) findViewById(R.id.yourNotification_txt);
        yourNotification_txt.setTypeface(typebold);

        titleofYourNotification = (TextView) findViewById(R.id.titleofYourNotification);
        titleofYourNotification.setTypeface(typeface);
        titleofYourNotification.setText(Html.fromHtml(tittle));

        yourNotification_desp = (TextView) findViewById(R.id.yourNotification_desp);
        yourNotification_desp.setTypeface(typeface);
        yourNotification_desp.setText(Html.fromHtml(msg));

        yournotification_home_btn = (ImageButton) findViewById(R.id.yournotification_home_btn);
        yournotification_home_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(session.checkLogin() != false) {

//                    Intent i = new Intent(YourNotification.this, StaticActivity.class);
//                    startActivity(i);
//
//                }
//                else {
                    Intent i = new Intent(YourNotification.this, MainActivity.class);
                    startActivity(i);
//                    }
            }
        });
    }

    @Override
    public void onBackPressed() {
//        if(session.checkLogin() != false) {

//            Intent i = new Intent(YourNotification.this, StaticActivity.class);
//            startActivity(i);
//            finish();
//
//        }
//        else {
            Intent i = new Intent(YourNotification.this, MainActivity.class);
            startActivity(i);
            finish();
//        }
    }
}
