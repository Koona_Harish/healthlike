package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.UserPackageNamesAdapter;
import innasoft.com.mhcone.models.UserPackageNamesModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.ConnectionDetector;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class ViewMyPackageNames extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    ProgressDialog progressDialog;
    ArrayList<UserPackageNamesModel> userpackageNamesModels;
    UserPackageNamesAdapter adapter;
    RecyclerView userpackagesrecyclerView;
    UserSessionManager userSessionManager;
    ConnectionDetector cd;
    Boolean isInternetPresent = false;
    Typeface typeface;
    TextView defaultTExt, noInternetConnection, userPackageName;
    Button retryAfterInernetConnected;
    String user_id;
    ImageButton cancelUserPackageNames;
    TextView defaultTextValue;
    Typeface type_lato;
    SearchView searchView;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_my_package_names);

        searchView = (SearchView) findViewById(R.id.mSearchlabs_view_my_package);

        type_lato = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");

        userPackageName = (TextView) findViewById(R.id.txt_user_package_name);
        userPackageName.setTypeface(type_lato);

        progressDialog = new ProgressDialog(ViewMyPackageNames.this, R.style.DialogTheme);
        progressDialog.setMessage("Please wait......");

        cancelUserPackageNames = (ImageButton) findViewById(R.id.user_pakg_btn_close);
        userpackageNamesModels = new ArrayList<UserPackageNamesModel>();
        userpackagesrecyclerView = (RecyclerView) findViewById(R.id.user_package_names_recycler_view);
        defaultTextValue = (TextView) findViewById(R.id.defaultText);
        defaultTextValue.setTypeface(type_lato);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.view_my_packages_names);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,

                android.R.color.holo_green_light,

                android.R.color.holo_orange_light,

                android.R.color.holo_red_light);

        mSwipeRefreshLayout.setOnRefreshListener(this);

        userSessionManager = new UserSessionManager(this);
        cd = new ConnectionDetector(this);
        isInternetPresent = cd.isConnectingToInternet();

        HashMap<String, String> user = userSessionManager.getUserDetails();

        user_id = user.get(UserSessionManager.USER_ID);

        defaultTExt = (TextView) findViewById(R.id.defaultText);
        noInternetConnection = (TextView) findViewById(R.id.noInternetConnect);
        retryAfterInernetConnected = (Button) findViewById(R.id.retryAfterInternetConnected);

        userpackagesrecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        userpackagesrecyclerView.setLayoutManager(layoutManager);
        adapter = new UserPackageNamesAdapter(userpackageNamesModels, ViewMyPackageNames.this, R.layout.row_user_packages_name_items);


        if (isInternetPresent) {
            getttinguserPackagesListNames(user_id);
        } else {
            showInternetStatus();
        }

        cancelUserPackageNames.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                adapter.getFilter().filter(query);
                return false;
            }
        });

    }

    private void getttinguserPackagesListNames(final String user_id) {

        progressDialog.show();
        noInternetConnection.setVisibility(View.INVISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_USER_PACKAGE_NAMES,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        userpackageNamesModels.clear();


                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String stattusCode = jsonObject.getString("status");
                            if (stattusCode.equals("19999")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                defaultTExt.setVisibility(View.INVISIBLE);


                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("my_package_name");
                                    UserPackageNamesModel upm = new UserPackageNamesModel();
                                    upm.setUser_package_id(jsonObject1.getString("id"));
                                    upm.setCreated_time(jsonObject1.getString("created_time"));
                                    upm.setUser_user_id(jsonObject1.getString("user_id"));
                                    upm.setUser_package_name(jsonObject1.getString("user_package_name"));
                                    upm.setStatus(jsonObject1.getString("status"));
                                    userpackageNamesModels.add(upm);
                                    progressDialog.dismiss();
                                }
                                userpackagesrecyclerView.setAdapter(adapter);
                                mSwipeRefreshLayout.setRefreshing(false);
                            }
                            if (stattusCode.equals("18888")) {
                                progressDialog.dismiss();
                                mSwipeRefreshLayout.setRefreshing(false);
                                defaultTExt.setVisibility(View.VISIBLE);
                            }

                            if (stattusCode.equals("10140")) {

                                userSessionManager.logoutUser();
                                Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }

                            if (stattusCode.equals("10150")) {

                                userSessionManager.logoutUser();
                                Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }
                            progressDialog.dismiss();
                            mSwipeRefreshLayout.setRefreshing(false);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            showInternetStatus();
                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                            showInternetStatus();

                        } else if (error instanceof ParseError) {

                        }

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void showInternetStatus() {
        noInternetConnection.setTypeface(typeface);
        noInternetConnection.setVisibility(View.VISIBLE);
        retryAfterInernetConnected.setVisibility(View.VISIBLE);
    }

    public void cardViewListener(int position) {
        if (userSessionManager.checkLogin() != false) {
            Toast.makeText(this, "Please Login...!", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(this, ViewMyPackageDetails.class);
            intent.putExtra("package_id", userpackageNamesModels.get(position));
            startActivity(intent);
        }
    }


    @Override
    public void onRefresh() {
        getttinguserPackagesListNames(user_id);
    }
}
