package innasoft.com.mhcone.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;


import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;

public class OrderCancel extends AppCompatActivity implements View.OnClickListener {

    ImageButton cancel_order_gotoHomeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_cancel);

        cancel_order_gotoHomeButton = (ImageButton) findViewById(R.id.cancel_order_gotoHomeButton);
        cancel_order_gotoHomeButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if(view == cancel_order_gotoHomeButton)
        {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }
}
