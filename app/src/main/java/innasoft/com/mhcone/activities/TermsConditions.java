package innasoft.com.mhcone.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.TextView;


import innasoft.com.mhcone.R;

public class TermsConditions extends AppCompatActivity implements View.OnClickListener {

    ImageButton tc_gotoHomeButton;
    WebView tc_page;
    ProgressDialog progressDialog;
    TextView tc_txt;
    Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_conditions);

        typeface = Typeface.createFromAsset(this.getAssets(),"robotobold.ttf");

        tc_gotoHomeButton = (ImageButton) findViewById(R.id.tc_gotoHomeButton);
        tc_gotoHomeButton.setOnClickListener(this);

        tc_txt = (TextView) findViewById(R.id.tc_txt);

        tc_page = (WebView)findViewById(R.id.tc_page);

        progressDialog = new ProgressDialog(TermsConditions.this, R.style.DialogTheme);

        progressDialog.setMessage("Please Wait......");
        progressDialog.setCancelable(false);

        progressDialog.show();
        tc_page.setInitialScale(1);
        tc_page.getSettings().setJavaScriptEnabled(true);
        tc_page.getSettings().setLoadWithOverviewMode(true);
        tc_page.getSettings().setUseWideViewPort(true);
        tc_page.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        tc_page.setScrollbarFadingEnabled(false);
        tc_page.setWebViewClient(new WebViewClient());
        tc_page.loadUrl("http://www.healthlike.in/terms");
        tc_txt.setTypeface(typeface);
        tc_page.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {

                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == tc_gotoHomeButton) {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
