package innasoft.com.mhcone.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class
HelathTipsNotification extends AppCompatActivity {

    ImageView imageViewDisplayPushuNotificationImg;
    TextView messageofNotification;
    TextView titleofmessage;
    ImageView gotoHome;
    UserSessionManager session;
    String notification_id;
    public WebView webviewpage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_helath_tips_notification);
        Bundle bundle = getIntent().getExtras();
        String message = bundle.getString("message");
        String imageUrl = bundle.getString("imageUrl");
        String title = bundle.getString("title");

        notification_id = bundle.getString("notification_id");
        String another = bundle.getString("AnotherActivity");

        gotoHome = (ImageView) findViewById(R.id.notification_packages_cancel_btn);
        webviewpage = (WebView) findViewById(R.id.webviewpage);
        session = new UserSessionManager(getApplicationContext());
        imageViewDisplayPushuNotificationImg = (ImageView) findViewById(R.id.imageNotification);
        messageofNotification = (TextView) findViewById(R.id.informationTextView);
        titleofmessage = (TextView) findViewById(R.id.titleofNotification);
        Picasso.with(getApplicationContext())
                .load(imageUrl)
                .placeholder(R.drawable.myhealthlogo)
                .fit()
                .into(imageViewDisplayPushuNotificationImg);
        titleofmessage.setText(Html.fromHtml(title));

        getting(notification_id);

        gotoHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(session.checkLogin() != false) {

//                    Intent i = new Intent(HelathTipsNotification.this, StaticActivity.class);
//                    startActivity(i);
//                }
//                else {
                    Intent i = new Intent(HelathTipsNotification.this, MainActivity.class);
                    startActivity(i);
                }
            }
        });
    }
    private void getting(final String notification_id) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.GETTINGDESC_USING_NOTIFI_ID, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                try {
                    Log.d("DASAFAFAFAF", response);
                    JSONObject jsonObject =new JSONObject(response);
                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");


                    String dataString = jsonObject1.getString("description");

                    webviewpage.getSettings().setJavaScriptEnabled(true);
                    webviewpage.loadData(dataString, "text/html", "UTF-8");
                    //messageofNotification.setText(Html.fromHtml(dataString));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String,String> params = new HashMap<String, String>();
                Log.d("SENDVALEDSDSDS", notification_id);
                params.put("notification_id",notification_id);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(HelathTipsNotification.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        if(session.checkLogin() != false) {

//            Intent i = new Intent(HelathTipsNotification.this, StaticActivity.class);
//            startActivity(i);
//            finish();
//        }
//        else {
            Intent i = new Intent(HelathTipsNotification.this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }
}
