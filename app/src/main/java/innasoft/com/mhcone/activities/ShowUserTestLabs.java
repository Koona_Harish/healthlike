package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.ShowUserTestLabsAdapter;
import innasoft.com.mhcone.adapters.ShowUserTestLabsCitiesAdapter;
import innasoft.com.mhcone.holders.ShowUserTestLabsHolder;
import innasoft.com.mhcone.models.CitiesModel;
import innasoft.com.mhcone.models.ShowUserTestLabsModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.DividerItemDecoration;
import innasoft.com.mhcone.utilities.VerticalSpaceItemDecoration;

public class ShowUserTestLabs extends AppCompatActivity implements  View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    ArrayList<ShowUserTestLabsModel> showusertestlabsList;
    ShowUserTestLabsAdapter adapter;
    RecyclerView recyclerview;

    ArrayList<String> yourPackageArrayList = new ArrayList<String>();

    String lat,lng,sub_testId,sub_testName;

    final static int SHOW_LABS_REQ_CODE = 26;
    ImageButton closeShowUserTestLabsBtn;
    ProgressDialog progressDialog;
    String billingPageValuesIds,packageName;
    TextView userTestLabTittle,noInternetConnect_b;
    private FloatingActionButton fabUsertestLabs;
    Typeface type_lato,type_lato2;
    String labID,labName,price,total;
    String filterString;
    SearchView searchView;
    int first_time_flag = 0;
    ImageView no_labs_found_iv;
    int filterFlag = 0;

    RelativeLayout yourpackageRL;
    ImageView yourpackageLocation;
    TextView yourpackageCity;

    Geocoder geocoder;
    List<Address> cuurentaddresses;
    String address = null;
    String city;
    String state;
    String country;
    String pincode;
    double latitude;
    double longitude;

    private static final int VERTICAL_ITEM_SPACE = 0;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    ArrayList<CitiesModel> citiesModelArrayList= new ArrayList<CitiesModel>();
    ShowUserTestLabsCitiesAdapter citiesAdapter;

    AlertDialog city_dialog,dialog;
    String global_lat,global_lng;
    int fabaction_status = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_user_test_labs);

        noInternetConnect_b = (TextView) findViewById(R.id.noInternetConnect_b);

        type_lato = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        type_lato2 = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        noInternetConnect_b.setTypeface(type_lato);

        no_labs_found_iv = (ImageView) findViewById(R.id.no_labs_found_iv);

        userTestLabTittle = (TextView) findViewById(R.id.user_test_lab_tittle);
        userTestLabTittle.setTypeface(type_lato2);
        Bundle bundle = getIntent().getExtras();

        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.show_user_test_swipe_layout);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,

                android.R.color.holo_green_light,

                android.R.color.holo_orange_light,

                android.R.color.holo_red_light);

        mSwipeRefreshLayout.setOnRefreshListener(this);

        Intent intent = getIntent();

        searchView = (SearchView) findViewById(R.id.mSearchlabs_user_test_labs);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });

        lat = bundle.getString("latValue");
        lng = bundle.getString("longValue");
        sub_testId = bundle.getString("subtestIdswithformate");
        sub_testName = bundle.getString("subtestNameswithfromate");
        billingPageValuesIds = bundle.getString("forbillingPageIDs");
        packageName = bundle.getString("packageName");

        geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

        latitude =  Double.parseDouble(lat);
        longitude = Double.parseDouble(lng);


        try {

            cuurentaddresses = geocoder.getFromLocation(latitude, longitude, 1);

        } catch (IOException e) {
            // TODO Auto-generated catch block

            e.printStackTrace();
        }

        fabUsertestLabs = (FloatingActionButton) findViewById(R.id.filter_user_test_lab);
        fabUsertestLabs.setOnClickListener(this);

        yourpackageRL = (RelativeLayout) findViewById(R.id.yourpackage_rl);
        yourpackageRL.setOnClickListener(this);

        yourpackageLocation = (ImageView) findViewById(R.id.yourpackage_location);
        yourpackageLocation.setOnClickListener(this);

        yourpackageCity = (TextView) findViewById(R.id.yourpackage_city);
        yourpackageCity.setOnClickListener(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");

        progressDialog.setProgressStyle(R.style.DialogTheme);

        showusertestlabsList = new ArrayList<ShowUserTestLabsModel>();

        recyclerview = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerview.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(layoutManager);

        recyclerview.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));

        recyclerview.addItemDecoration(new DividerItemDecoration(getApplicationContext()));

        recyclerview.addItemDecoration(
                new DividerItemDecoration(getApplicationContext(), R.drawable.recycler_divider));

        closeShowUserTestLabsBtn = (ImageButton) findViewById(R.id.user_test_labs_btn_close);

        adapter = new ShowUserTestLabsAdapter(lat,lng,sub_testId,sub_testName,billingPageValuesIds,packageName,showusertestlabsList,
                ShowUserTestLabs.this, R.layout.row_user_tests_labs3);

        if(filterFlag == 0) {

            filterString = "distance_in_km asc";

            getLabsTest(filterString);
        }

        closeShowUserTestLabsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                setResult(SHOW_LABS_REQ_CODE ,intent);
                finish();
            }
        });


        if(cuurentaddresses.size() != 0) {

            city = cuurentaddresses.get(0).getLocality();


            yourpackageCity.setText(city);
        }
        else {

        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                adapter.getFilter().filter(query);
                return false;
            }
        });
    }

    private void getLabsTest(final String filterValue)
    {
        showusertestlabsList.clear();
        no_labs_found_iv.setVisibility(View.GONE);
        noInternetConnect_b.setVisibility(View.GONE);
        ShowUserTestLabs.this.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.show();
            }
        });
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_LABS_FOR_TESTS, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {

                try {

                    JSONObject jsonObject =new JSONObject(response);
                    String status = jsonObject.getString("status");
                    if(status.equals("19999")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("data");


                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");

                                ShowUserTestLabsModel sutlm = new ShowUserTestLabsModel();

                                sutlm.setShow_user_test_lab_id(jsonObject1.getString("id"));
                                sutlm.setShow_user_test_lab_user_name(jsonObject1.getString("user_name"));
                                sutlm.setShow_user_test_lab_email(jsonObject1.getString("email"));
                                sutlm.setShow_user_test_lab_mobile(jsonObject1.getString("mobile"));
                                sutlm.setShow_user_test_lab_role_id(jsonObject1.getString("role_id"));
                                sutlm.setShow_user_test_lab_created_time(jsonObject1.getString("created_time"));
                                sutlm.setCertificate(jsonObject1.getString("certificate"));
                                sutlm.setLocation(jsonObject1.getString("location"));


                                sutlm.setProfile_pic_url(jsonObject1.getString("profile_pic"));

                                if (jsonObject1.getString("rating").equals("null")) {
                                    sutlm.setShow_user_test_lab_ratting("0");
                                } else {
                                    sutlm.setShow_user_test_lab_ratting(jsonObject1.getString("rating"));
                                }
                                sutlm.setShow_user_test_user_test_lab_payment_lab(jsonObject1.getString("payment_lab"));
                                sutlm.setShow_user_test_lab_payment_online(jsonObject1.getString("payment_online"));
                                sutlm.setShow_user_test_lab_tele_booking(jsonObject1.getString("tele_booking"));



                                sutlm.setShow_user_test_lab_address(jsonObject1.getString("address"));


                                String workingtimefromServer = jsonObject1.getString("current_week_time");
                                String workingDayfromServer = jsonObject1.getString("current_week_name");

                                String removeSpace = workingtimefromServer.replace(" ", "");
                                String segments[] = removeSpace.split("-");
                                String conditionCheck = segments[segments.length - 1];
                                if (conditionCheck.equals("1")) {
                                    sutlm.setShow_user_test_lab_workingtime(segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                                }
                                if (conditionCheck.equals("0")) {
                                    sutlm.setShow_user_test_lab_workingtime("Today lab Close");
                                }
                                sutlm.setShow_user_test_lab_lab_price(jsonObject1.getString("lab_price"));


                                sutlm.setShow_user_test_lab_lat(jsonObject1.getString("lat"));
                                sutlm.setShow_user_test_lab_lang(jsonObject1.getString("lang"));
                                sutlm.setShow_user_test_lab_home_collections(jsonObject1.getString("home_collections"));
                                sutlm.setShow_user_test_lab_final_price(jsonObject1.getString("final_price"));


                                sutlm.setShow_user_test_lab_test_id(jsonObject1.getString("test_id"));
                                sutlm.setShow_user_test_lab_sub_test_id(jsonObject1.getString("sub_test_id"));

                                sutlm.setShow_user_test_lab_distance(jsonObject1.getString("distance_in_km"));
                                showusertestlabsList.add(sutlm);
                                fabaction_status = 0;
                            }
                            recyclerview.setAdapter(adapter);

                            ShowUserTestLabs.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    progressDialog.dismiss();

                                }
                            });
                            mSwipeRefreshLayout.setRefreshing(false);

                    }
                    else if(status.equals("18888"))
                    {
                        ShowUserTestLabs.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                        showusertestlabsList.clear();
                        fabaction_status = 1;
                        adapter.notifyDataSetChanged();
                        no_labs_found_iv.setVisibility(View.VISIBLE);
                        noInternetConnect_b.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnect_b.setVisibility(View.VISIBLE);

                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Please Try after some time...!", Toast.LENGTH_SHORT).show();
                    }

                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {

                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                String responseBody = null;


                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {


                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String,String> params = new HashMap<String, String>();

                params.put("test_id", sub_testId);

                if(first_time_flag == 1) {
                    Log.d("LABFINDSENDVALUES", sub_testId+"\n"+global_lat+"\n"+global_lng+"\n"+filterValue);
                    params.put("lat", global_lat);
                    params.put("lang",global_lng);
                }else {
                    Log.d("LABFINDSENDVALUES", sub_testId+"\n"+lat+"\n"+lng+"\n"+filterValue);
                    params.put("lat", lat);
                    params.put("lang",lng);
                }
                params.put("order_type", filterValue);
                Log.d("SDKAKFKAFKA", params.toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }




    public void cardViewListener(int position) {

        labID = showusertestlabsList.get(position).getShow_user_test_lab_id();
        labName = showusertestlabsList.get(position).getShow_user_test_lab_user_name();
        price = showusertestlabsList.get(position).getShow_user_test_lab_final_price();


        Intent intent = new Intent(this,BillingPageForUserPackage.class);

        intent.putExtra("LAT",lat);
        intent.putExtra("LNG",lng);
        intent.putExtra("SUBTESTID",sub_testId);
        intent.putExtra("selectedLabPone", showusertestlabsList.get(position).getShow_user_test_lab_mobile());
        intent.putExtra("SUBTESTNAME",sub_testName);
        intent.putExtra("BILLINGPAGEVALUESIDS",billingPageValuesIds);
        intent.putExtra("PACKAGENAME",packageName);
        intent.putExtra("LABID",labID);
        intent.putExtra("LABNAME",labName);
        intent.putExtra("PRICE",price);

        intent.putExtra("payment_lab", showusertestlabsList.get(position).getShow_user_test_user_test_lab_payment_lab());
        intent.putExtra("payment_online", showusertestlabsList.get(position).getShow_user_test_lab_payment_online());
        intent.putExtra("tele_booking", showusertestlabsList.get(position).getShow_user_test_lab_tele_booking());

        startActivity(intent);
    }

    @Override
    public void onClick(View v) {


        if(v == searchView)
        {
            userTestLabTittle.setVisibility(View.INVISIBLE);
        }

        if(v == fabUsertestLabs)
        {
            if (fabaction_status == 0) {
                no_labs_found_iv.setVisibility(View.GONE);

                AlertDialog.Builder builder = new AlertDialog.Builder(ShowUserTestLabs.this);
                LayoutInflater inflater = getLayoutInflater();

                View dialog_layout = inflater.inflate(R.layout.custom_dialog_filter, null);

                TextView dialog_title = (TextView) dialog_layout.findViewById(R.id.dialog_title);
                dialog_title.setTypeface(type_lato);

                TextView dlh_txt = (TextView) dialog_layout.findViewById(R.id.dlh_txt);
                dlh_txt.setTypeface(type_lato);

                final String cityName = yourpackageCity.getText().toString();

                dlh_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "distance_in_km asc";
                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                        } else {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                        }
                        dialog.dismiss();
                    }
                });

                TextView dhl_txt = (TextView) dialog_layout.findViewById(R.id.dhl_txt);
                dhl_txt.setTypeface(type_lato);
                dhl_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "distance_in_km desc";

                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                        } else {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                        }
                        dialog.dismiss();
                    }
                });

                TextView plh_txt = (TextView) dialog_layout.findViewById(R.id.plh_txt);
                plh_txt.setTypeface(type_lato);
                plh_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "final_price asc";
                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                        } else {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                        }
                        dialog.dismiss();
                    }
                });

                TextView phl_txt = (TextView) dialog_layout.findViewById(R.id.phl_txt);
                phl_txt.setTypeface(type_lato);
                phl_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "final_price desc";
                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                        } else {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                        }
                        dialog.dismiss();
                    }
                });

                TextView naz_txt = (TextView) dialog_layout.findViewById(R.id.naz_txt);
                naz_txt.setTypeface(type_lato);
                naz_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "users.user_name asc";
                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                        } else {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                        }
                        dialog.dismiss();
                    }
                });


                TextView nza_txt = (TextView) dialog_layout.findViewById(R.id.nza_txt);
                nza_txt.setTypeface(type_lato);
                nza_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "users.user_name desc";
                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                        } else {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                        }
                        dialog.dismiss();
                    }
                });

                TextView hc_txt = (TextView) dialog_layout.findViewById(R.id.hc_txt);
                hc_txt.setTypeface(type_lato);
                hc_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "distance_in_km asc";
                        String value2 = "lab_users.home_collections = 1";
                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilterTwo(vlaue, sub_testId, global_lat, global_lng, value2, cityName);
                        } else {
                            gettingLabswithCityNamewithFilterTwo(vlaue, sub_testId, lat, lng, value2, cityName);
                        }
                        dialog.dismiss();
                    }
                });

                TextView rhl_txt = (TextView) dialog_layout.findViewById(R.id.rhl_txt);
                rhl_txt.setTypeface(type_lato);
                rhl_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "rating desc";
                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                        } else {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                        }
                        dialog.dismiss();
                    }
                });


                TextView rlh_txt = (TextView) dialog_layout.findViewById(R.id.rlh_txt);
                rlh_txt.setTypeface(type_lato);
                rlh_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "rating asc";
                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                        } else {
                            gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                        }
                        dialog.dismiss();
                    }
                });


                TextView nabl_txt = (TextView) dialog_layout.findViewById(R.id.nabl_txt);
                nabl_txt.setTypeface(type_lato);
                nabl_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "distance_in_km asc";
                        String value2 = "lab_users.certificate = 1";

                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilterTwo(vlaue, sub_testId, global_lat, global_lng, value2, cityName);
                        } else {
                            gettingLabswithCityNamewithFilterTwo(vlaue, sub_testId, lat, lng, value2, cityName);
                        }
                        dialog.dismiss();
                    }
                });


                builder.setView(dialog_layout)
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });

                dialog = builder.create();
                dialog.show();

                /*final String[] country = {"Distance Low to High", "Distance High to Low","Names A to Z", "Names Z to A"
                    ,"Price Low to High", "Price High to Low","Home Collection","Ratting High to Low", "Ratting Low to High","NABL/CAP Accreditation"};
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Choose One")
                    .setItems(country, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            String selectedCountry = country[i];
                            String cityName = yourpackageCity.getText().toString();

                            if(selectedCountry.equals("Distance Low to High"))
                            {
                                String vlaue = "distance_in_km asc";

                               // gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng , cityName);

                                if (first_time_flag == 1) {
                                    gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                                } else {
                                    gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                                }

                            }
                            if(selectedCountry.equals("Distance High to Low"))
                            {
                                String vlaue = "distance_in_km desc";

                                if (first_time_flag == 1) {
                                    gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                                } else {
                                    gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                                }
                            }
                            if(selectedCountry.equals("Names A to Z"))
                            {
                                String vlaue = "users.user_name asc";

                                if (first_time_flag == 1) {
                                    gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                                } else {
                                    gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                                }
                            }
                            if(selectedCountry.equals("Names Z to A"))
                            {
                                String vlaue = "users.user_name desc";

                                if (first_time_flag == 1) {
                                    gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                                } else {
                                    gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                                }
                            }
                            if(selectedCountry.equals("Price Low to High"))
                            {
                                String vlaue = "final_price asc";


                                if (first_time_flag == 1) {
                                    gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                                } else {
                                    gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                                }
                            }
                            if(selectedCountry.equals("Price High to Low"))
                            {
                                String vlaue = "final_price desc";

                                if (first_time_flag == 1) {
                                    gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                                } else {
                                    gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                                }
                            }


                            if(selectedCountry.equals("Home Collection"))
                            {
                                String vlaue = "distance_in_km asc";
                                String value2 = "1";
                                if (first_time_flag == 1) {
                                    //gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng , cityName);
                                    gettingLabswithCityNamewithFilterTwo(vlaue, sub_testId, global_lat, global_lng, value2, cityName);
                                } else {
                                    //gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng , cityName);
                                    gettingLabswithCityNamewithFilterTwo(vlaue, sub_testId, lat, lng, value2, cityName);
                                }
                            }

                            if(selectedCountry.equals("NABL/CAP Accreditation"))
                            {
                                String vlaue = "distance_in_km asc";
                                String value2 = "lab_users.certificate = 1";

                                if (first_time_flag == 1) {
                                    //gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng , cityName);
                                    gettingLabswithCityNamewithFilterTwo(vlaue, sub_testId, global_lat, global_lng, value2, cityName);
                                } else {
                                    //gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng , cityName);
                                    gettingLabswithCityNamewithFilterTwo(vlaue, sub_testId, lat, lng, value2, cityName);
                                }
                            }


                            if(selectedCountry.equals("Ratting High to Low"))
                            {
                                String vlaue = "rating desc";

                                if (first_time_flag == 1) {
                                    gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                                } else {
                                    gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                                }
                            }


                            if(selectedCountry.equals("Ratting Low to High"))
                            {
                                String vlaue = "rating asc";

                                if (first_time_flag == 1) {
                                    gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng, cityName);
                                } else {
                                    gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng, cityName);
                                }
                            }

                        }
                    });

            AlertDialog dialog = builder.create();
            dialog.show();*/
            }else {
                no_labs_found_iv.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), "No Labs found....!", Toast.LENGTH_SHORT).show();
            }
        }

        if(v == yourpackageRL)
        {

            progressDialog.show();
            yourPackageArrayList.clear();

            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL+AppUrls.LAB_LOCATIONS, new Response.Listener<String>()
            {
                @Override
                public void onResponse(String response)
                {

                    try
                    {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        citiesModelArrayList.clear();

                        for (int i = 0; i < jsonArray.length(); i++)


                        {


                            CitiesModel citiesModel = new CitiesModel();


                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("city");


                            String city_name = jsonObject1.getString("city_name");
                            String city_latitude = jsonObject1.getString("lat");
                            String city_longitude = jsonObject1.getString("lang");



                            citiesModel.setCity_name(city_name);
                            citiesModel.setLat(city_latitude);
                            citiesModel.setLang(city_longitude);


                            // labArrayList.add(city_name);



                            citiesModelArrayList.add(citiesModel);



                        }

                        alertLocationList(citiesModelArrayList);






                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    progressDialog.dismiss();
                    String responseBody = null;


                    if (error instanceof TimeoutError || error instanceof NoConnectionError)
                    {

                    } else if (error instanceof AuthFailureError)
                    {
                    } else if (error instanceof ServerError)
                    {

                    } else if (error instanceof NetworkError)
                    {
                    } else if (error instanceof ParseError)
                    {
                    }
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(ShowUserTestLabs.this);
            requestQueue.add(stringRequest);
        }


        if(v == yourpackageLocation)
        {

            progressDialog.show();
            yourPackageArrayList.clear();

            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL+AppUrls.LAB_LOCATIONS, new Response.Listener<String>()
            {
                @Override
                public void onResponse(String response)
                {

                    try
                    {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        citiesModelArrayList.clear();

                        for (int i = 0; i < jsonArray.length(); i++)


                        {


                            CitiesModel citiesModel = new CitiesModel();


                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("city");


                            String city_name = jsonObject1.getString("city_name");
                            String city_latitude = jsonObject1.getString("lat");
                            String city_longitude = jsonObject1.getString("lang");



                            citiesModel.setCity_name(city_name);
                            citiesModel.setLat(city_latitude);
                            citiesModel.setLang(city_longitude);


                            // labArrayList.add(city_name);



                            citiesModelArrayList.add(citiesModel);



                        }

                        alertLocationList(citiesModelArrayList);

                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    progressDialog.dismiss();
                    String responseBody = null;

                    if (error instanceof TimeoutError || error instanceof NoConnectionError)
                    {

                    } else if (error instanceof AuthFailureError)
                    {
                    } else if (error instanceof ServerError)
                    {

                    } else if (error instanceof NetworkError)
                    {
                    } else if (error instanceof ParseError)
                    {
                    }
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(ShowUserTestLabs.this);
            requestQueue.add(stringRequest);
        }


        if(v == yourpackageCity)
        {

            progressDialog.show();
            yourPackageArrayList.clear();

            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL+AppUrls.LAB_LOCATIONS, new Response.Listener<String>()
            {
                @Override
                public void onResponse(String response)
                {

                    try
                    {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        citiesModelArrayList.clear();

                        for (int i = 0; i < jsonArray.length(); i++)


                        {


                            CitiesModel citiesModel = new CitiesModel();


                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("city");


                            String city_name = jsonObject1.getString("city_name");
                            String city_latitude = jsonObject1.getString("lat");
                            String city_longitude = jsonObject1.getString("lang");



                            citiesModel.setCity_name(city_name);
                            citiesModel.setLat(city_latitude);
                            citiesModel.setLang(city_longitude);


                            // labArrayList.add(city_name);



                            citiesModelArrayList.add(citiesModel);



                        }

                        alertLocationList(citiesModelArrayList);

                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    progressDialog.dismiss();
                    String responseBody = null;


                    if (error instanceof TimeoutError || error instanceof NoConnectionError)
                    {

                    } else if (error instanceof AuthFailureError)
                    {
                    } else if (error instanceof ServerError)
                    {

                    } else if (error instanceof NetworkError)
                    {
                    } else if (error instanceof ParseError)
                    {
                    }
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(ShowUserTestLabs.this);
            requestQueue.add(stringRequest);
        }

    }

    private void gettingLabswithCityNamewithFilterTwo(final String vlaue, final String sub_testId, final String lat, final String lng, final String value2, final String cityName) {

        showusertestlabsList.clear();
        noInternetConnect_b.setVisibility(View.INVISIBLE);
        ShowUserTestLabs.this.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.show();
            }
        });
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_LABS_FOR_TESTS, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {

                try {

                    JSONObject jsonObject =new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    if(responceCode.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");
                            ShowUserTestLabsModel sutlm = new ShowUserTestLabsModel();
                            sutlm.setShow_user_test_lab_id(jsonObject1.getString("id"));
                            sutlm.setShow_user_test_lab_user_name(jsonObject1.getString("user_name"));
                            sutlm.setShow_user_test_lab_email(jsonObject1.getString("email"));
                            sutlm.setShow_user_test_lab_mobile(jsonObject1.getString("mobile"));
                            sutlm.setShow_user_test_lab_role_id(jsonObject1.getString("role_id"));
                            sutlm.setShow_user_test_lab_created_time(jsonObject1.getString("created_time"));
                            sutlm.setCertificate(jsonObject1.getString("certificate"));
                            sutlm.setLocation(jsonObject1.getString("location"));


                            sutlm.setProfile_pic_url(jsonObject1.getString("profile_pic"));

                            if (jsonObject1.getString("rating").equals("null")) {
                                sutlm.setShow_user_test_lab_ratting("0");
                            } else {
                                sutlm.setShow_user_test_lab_ratting(jsonObject1.getString("rating"));
                            }
                            sutlm.setShow_user_test_user_test_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            sutlm.setShow_user_test_lab_payment_online(jsonObject1.getString("payment_online"));
                            sutlm.setShow_user_test_lab_tele_booking(jsonObject1.getString("tele_booking"));

                            sutlm.setShow_user_test_lab_address(jsonObject1.getString("address"));



                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String workingDayfromServer = jsonObject1.getString("current_week_name");
                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];
                            if (conditionCheck.equals("1")) {
                                sutlm.setShow_user_test_lab_workingtime(segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (conditionCheck.equals("0")) {
                                sutlm.setShow_user_test_lab_workingtime("Today lab Close");
                            }
                            sutlm.setShow_user_test_lab_lab_price(jsonObject1.getString("lab_price"));

                            sutlm.setShow_user_test_lab_lat(jsonObject1.getString("lat"));
                            sutlm.setShow_user_test_lab_lang(jsonObject1.getString("lang"));
                            sutlm.setShow_user_test_lab_home_collections(jsonObject1.getString("home_collections"));
                            sutlm.setShow_user_test_lab_test_id(jsonObject1.getString("test_id"));
                            sutlm.setShow_user_test_lab_sub_test_id(jsonObject1.getString("sub_test_id"));


                            sutlm.setShow_user_test_lab_final_price(jsonObject1.getString("final_price"));
                            sutlm.setShow_user_test_lab_distance(jsonObject1.getString("distance_in_km"));
                            showusertestlabsList.add(sutlm);
                            fabaction_status = 0;
                        }
                        recyclerview.setAdapter(adapter);

                        ShowUserTestLabs.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                    else {
                        ShowUserTestLabs.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                        showusertestlabsList.clear();
                        fabaction_status = 1;
                        noInternetConnect_b.setText("We are not in your location now, we will resume to your location shortly.");
                        noInternetConnect_b.setVisibility(View.VISIBLE);
                    }

                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {

                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                String responseBody = null;


                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {


                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String,String> params = new HashMap<String, String>();

                params.put("test_id", sub_testId);

                if(first_time_flag == 1) {
                    Log.d("LABFINDSENDVALUES", sub_testId+"\n"+global_lat+"\n"+global_lng+"\n"+vlaue);
                    params.put("lat", global_lat);
                    params.put("lang",global_lng);
                }else {
                    Log.d("LABFINDSENDVALUES", sub_testId+"\n"+lat+"\n"+lng+"\n"+vlaue);
                    params.put("lat", lat);
                    params.put("lang",lng);
                }
                params.put("order_type", vlaue);
                params.put("home_collections", value2);

                Log.d("SDKAKFKAFKA", params.toString());

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void gettingLabswithCityNamewithFilter(final String vlaue, final String sub_testId, final String lat, final String lng, final String cityName) {



        showusertestlabsList.clear();
        noInternetConnect_b.setVisibility(View.INVISIBLE);
        ShowUserTestLabs.this.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.show();
            }
        });
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_LABS_FOR_TESTS, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {

                try {

                    JSONObject jsonObject =new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    if(responceCode.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");
                            ShowUserTestLabsModel sutlm = new ShowUserTestLabsModel();
                            sutlm.setShow_user_test_lab_id(jsonObject1.getString("id"));
                            sutlm.setShow_user_test_lab_user_name(jsonObject1.getString("user_name"));
                            sutlm.setShow_user_test_lab_email(jsonObject1.getString("email"));
                            sutlm.setShow_user_test_lab_mobile(jsonObject1.getString("mobile"));
                            sutlm.setShow_user_test_lab_role_id(jsonObject1.getString("role_id"));
                            sutlm.setShow_user_test_lab_created_time(jsonObject1.getString("created_time"));
                            sutlm.setCertificate(jsonObject1.getString("certificate"));
                            sutlm.setLocation(jsonObject1.getString("location"));


                            sutlm.setProfile_pic_url(jsonObject1.getString("profile_pic"));

                            if (jsonObject1.getString("rating").equals("null")) {
                                sutlm.setShow_user_test_lab_ratting("0");
                            } else {
                                sutlm.setShow_user_test_lab_ratting(jsonObject1.getString("rating"));
                            }
                            sutlm.setShow_user_test_user_test_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            sutlm.setShow_user_test_lab_payment_online(jsonObject1.getString("payment_online"));
                            sutlm.setShow_user_test_lab_tele_booking(jsonObject1.getString("tele_booking"));

                            sutlm.setShow_user_test_lab_address(jsonObject1.getString("address"));



                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String workingDayfromServer = jsonObject1.getString("current_week_name");
                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];
                            if (conditionCheck.equals("1")) {
                                sutlm.setShow_user_test_lab_workingtime(segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (conditionCheck.equals("0")) {
                                sutlm.setShow_user_test_lab_workingtime("Today lab Close");
                            }
                            sutlm.setShow_user_test_lab_lab_price(jsonObject1.getString("lab_price"));

                            sutlm.setShow_user_test_lab_lat(jsonObject1.getString("lat"));
                            sutlm.setShow_user_test_lab_lang(jsonObject1.getString("lang"));
                            sutlm.setShow_user_test_lab_home_collections(jsonObject1.getString("home_collections"));
                            sutlm.setShow_user_test_lab_test_id(jsonObject1.getString("test_id"));
                            sutlm.setShow_user_test_lab_sub_test_id(jsonObject1.getString("sub_test_id"));


                            sutlm.setShow_user_test_lab_final_price(jsonObject1.getString("final_price"));
                            sutlm.setShow_user_test_lab_distance(jsonObject1.getString("distance_in_km"));
                            showusertestlabsList.add(sutlm);
                            fabaction_status = 0;
                        }
                        recyclerview.setAdapter(adapter);

                        ShowUserTestLabs.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                    }else {
                        ShowUserTestLabs.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                        showusertestlabsList.clear();
                        fabaction_status = 1;
                        noInternetConnect_b.setText("We are not in your location now, we will resume to your location shortly.");
                        noInternetConnect_b.setVisibility(View.VISIBLE);
                    }

                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {

                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                String responseBody = null;


                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {


                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String,String> params = new HashMap<String, String>();

                params.put("test_id", sub_testId);
                if(first_time_flag == 1) {
                    Log.d("LABFINDSENDVALUES", sub_testId+"\n"+global_lat+"\n"+global_lng+"\n"+vlaue);
                    params.put("lat", global_lat);
                    params.put("lang",global_lng);
                }else {
                    Log.d("LABFINDSENDVALUES", sub_testId+"\n"+lat+"\n"+lng+"\n"+vlaue);
                    params.put("lat", lat);
                    params.put("lang",lng);
                }
                params.put("order_type", vlaue);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void alertLocationList(ArrayList<CitiesModel> labArrayList)
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(ShowUserTestLabs.this);
        LayoutInflater inflater = this.getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.custom_dialog_layout_cities,null);
        RecyclerView cities_recycler_view = (RecyclerView) dialog_layout.findViewById(R.id.cities_recycler_view);
        final SearchView mSearch_list_cities = (SearchView) dialog_layout.findViewById(R.id.mSearch_list_cities);




        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ShowUserTestLabs.this);
        cities_recycler_view.setLayoutManager(layoutManager);



        citiesAdapter = new ShowUserTestLabsCitiesAdapter(labArrayList, ShowUserTestLabs.this,R.layout.row_cities);


       /* cities_recycler_view.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        cities_recycler_view.addItemDecoration(new DividerItemDecoration(getActivity()));
        cities_recycler_view.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.recycler_divider));*/
        cities_recycler_view.setAdapter(citiesAdapter);



        mSearch_list_cities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearch_list_cities.setIconified(false);
            }
        });





        builder.setView(dialog_layout)


                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        progressDialog.dismiss();
                        dialogInterface.dismiss();
                    }
                });

        city_dialog = builder.create();

        mSearch_list_cities.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                citiesAdapter.getFilter().filter(query);
                return false;
            }
        });
        city_dialog.show();

        /*final String[] items = yourPackageArrayList.toArray(new String[yourPackageArrayList.size()]);



        AlertDialog.Builder builder = new AlertDialog.Builder(ShowUserTestLabs.this);
        builder.setTitle("Select City").setItems(items, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {

                String selectedCountry = items[i];
                yourpackageCity.setText(selectedCountry);
                first_time_flag = 1;
                gettingLabsotherCities(selectedCountry);

            }
        });

        AlertDialog dialog = builder.create();
        progressDialog.dismiss();
        dialog.show();*/

    }

    private void gettingLabsotherCities(String selectedCountry, String strLat, String strLng) {


        recyclerview = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerview.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(layoutManager);

        recyclerview.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));

        recyclerview.addItemDecoration(new DividerItemDecoration(getApplicationContext()));

        recyclerview.addItemDecoration(
                new DividerItemDecoration(getApplicationContext(), R.drawable.recycler_divider));

        closeShowUserTestLabsBtn = (ImageButton) findViewById(R.id.user_test_labs_btn_close);

        adapter = new ShowUserTestLabsAdapter(lat,lng,sub_testId,sub_testName,billingPageValuesIds,packageName,showusertestlabsList, ShowUserTestLabs.this, R.layout.row_user_tests_labs3);

       /* if(filterFlag == 0) {*/

            filterString = "distance_in_km asc";

            gettingLabsOtherCity(selectedCountry, filterString, strLat, strLng);
        /*}*/

    }

    private void gettingLabsOtherCity(final String selectedCountry, final String filterString, final String strLat, final String strLng) {
        showusertestlabsList.clear();
        noInternetConnect_b.setVisibility(View.INVISIBLE);
        ShowUserTestLabs.this.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.show();
            }
        });
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_LABS_FOR_TESTS, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {

                try {

                    JSONObject jsonObject =new JSONObject(response);
                    String status = jsonObject.getString("status");
                    if(status.equals("19999")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("data");


                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");

                            ShowUserTestLabsModel sutlm = new ShowUserTestLabsModel();

                            sutlm.setShow_user_test_lab_id(jsonObject1.getString("id"));
                            sutlm.setShow_user_test_lab_user_name(jsonObject1.getString("user_name"));
                            sutlm.setShow_user_test_lab_email(jsonObject1.getString("email"));
                            sutlm.setShow_user_test_lab_mobile(jsonObject1.getString("mobile"));
                            sutlm.setShow_user_test_lab_role_id(jsonObject1.getString("role_id"));
                            sutlm.setShow_user_test_lab_created_time(jsonObject1.getString("created_time"));
                            sutlm.setCertificate(jsonObject1.getString("certificate"));
                            sutlm.setLocation(jsonObject1.getString("location"));


                            sutlm.setProfile_pic_url(jsonObject1.getString("profile_pic"));

                            if (jsonObject1.getString("rating").equals("null")) {
                                sutlm.setShow_user_test_lab_ratting("0");
                            } else {
                                sutlm.setShow_user_test_lab_ratting(jsonObject1.getString("rating"));
                            }
                            sutlm.setShow_user_test_user_test_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            sutlm.setShow_user_test_lab_payment_online(jsonObject1.getString("payment_online"));
                            sutlm.setShow_user_test_lab_tele_booking(jsonObject1.getString("tele_booking"));



                            sutlm.setShow_user_test_lab_address(jsonObject1.getString("address"));


                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String workingDayfromServer = jsonObject1.getString("current_week_name");

                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];
                            if (conditionCheck.equals("1")) {
                                sutlm.setShow_user_test_lab_workingtime(segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (conditionCheck.equals("0")) {
                                sutlm.setShow_user_test_lab_workingtime("Today lab Close");
                            }
                            sutlm.setShow_user_test_lab_lab_price(jsonObject1.getString("lab_price"));


                            sutlm.setShow_user_test_lab_lat(jsonObject1.getString("lat"));
                            sutlm.setShow_user_test_lab_lang(jsonObject1.getString("lang"));
                            sutlm.setShow_user_test_lab_home_collections(jsonObject1.getString("home_collections"));
                            sutlm.setShow_user_test_lab_final_price(jsonObject1.getString("final_price"));


                            sutlm.setShow_user_test_lab_test_id(jsonObject1.getString("test_id"));
                            sutlm.setShow_user_test_lab_sub_test_id(jsonObject1.getString("sub_test_id"));

                            sutlm.setShow_user_test_lab_distance(jsonObject1.getString("distance_in_km"));
                            showusertestlabsList.add(sutlm);
                            fabaction_status = 0;
                        }
                        recyclerview.setAdapter(adapter);

                        ShowUserTestLabs.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);

                    }
                    else if(status.equals("18888"))
                    {
                        ShowUserTestLabs.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();


                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                        showusertestlabsList.clear();
                        fabaction_status = 1;
                        noInternetConnect_b.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnect_b.setVisibility(View.VISIBLE);

                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Please Try after some time...!", Toast.LENGTH_SHORT).show();
                    }

                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {

                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                String responseBody = null;


                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {


                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String,String> params = new HashMap<String, String>();

                params.put("test_id", sub_testId);

                if(first_time_flag == 1) {
                    Log.d("LABFINDSENDVALUES", sub_testId+"\n"+global_lat+"\n"+global_lng+"\n"+filterString);
                    params.put("lat", global_lat);
                    params.put("lang",global_lng);
                }else {
                    Log.d("LABFINDSENDVALUES", sub_testId+"\n"+lat+"\n"+lng+"\n"+filterString);
                    params.put("lat", lat);
                    params.put("lang",lng);
                }
                params.put("order_type", filterString);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onRefresh() {
        showusertestlabsList = new ArrayList<ShowUserTestLabsModel>();

        recyclerview = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerview.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(layoutManager);

        recyclerview.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));

        recyclerview.addItemDecoration(new DividerItemDecoration(getApplicationContext()));

        recyclerview.addItemDecoration(
                new DividerItemDecoration(getApplicationContext(), R.drawable.recycler_divider));

        closeShowUserTestLabsBtn = (ImageButton) findViewById(R.id.user_test_labs_btn_close);

        adapter = new ShowUserTestLabsAdapter(lat,lng,sub_testId,sub_testName,billingPageValuesIds,packageName,showusertestlabsList, ShowUserTestLabs.this, R.layout.row_user_tests_labs3);

        if(filterFlag == 0) {

            filterString = "distance_in_km asc";

            getLabsTest(filterString);
        }

    }


    public void refreshMyList(String selectedCountry, String selected_city_lat, String selected_city_lng) {


        city_dialog.dismiss();



        first_time_flag = 1;
        global_lat = selected_city_lat;
        global_lng = selected_city_lng;

        yourpackageCity.setText(selectedCountry);

        gettingLabsotherCities(selectedCountry, global_lat, global_lng);
        adapter.notifyDataSetChanged();


    }

}
