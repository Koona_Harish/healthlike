package innasoft.com.mhcone.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.NetworkUtil;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class EditSubUser extends AppCompatActivity implements View.OnClickListener {

    TextInputLayout subuserNameTil,subuserEmailTil,subuserPhoneTil;
    EditText subuserNameEdt,subuserEmailEdt,subuserPhoneEdt;
    RadioGroup subuserGenderId;
    RadioButton subuserMale,subuserFemale,subuserGender;
    ImageButton view_user_close;
    Button subuserCancel,subuserUpdate;
    Spinner spinner;
    String spinnerString;
    TextView dispdob;
    ImageView dobPicker;
    private int mYear, mMonth, mDay;
    UserSessionManager userSessionManager;
    String access_key, disp_userName, disp_email;
    String userID;
    String strSubuserResult,strSubuserName,strSubuserGender,strSubuserAge,strSubuserEmail,strSubuserPhone;
    String subUserId,subId,subName,subAge,subGender,subRelation,subEmail,subMobile;
    private boolean statusFlag;
    Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_sub_user);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Edit I Care For");

        typeface = Typeface.createFromAsset(this.getAssets(),"robotoregular.ttf");

        spinner = (Spinner) findViewById(R.id.spinner_relation_edt);

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = userSessionManager.getUserDetails();
        access_key = user.get(UserSessionManager.KEY_ACCSES);

        disp_userName = user.get(UserSessionManager.USER_NAME);
        disp_email = user.get(UserSessionManager.USER_EMAIL);
        userID = user.get(UserSessionManager.USER_ID);

        view_user_close = (ImageButton) findViewById(R.id.view_user_close);
        view_user_close.setOnClickListener(this);

        dispdob = (TextView) findViewById(R.id.dobdisp);
        dobPicker = (ImageView) findViewById(R.id.dobPicker);

        List<String> spinnerList = new ArrayList<String>();
        spinnerList.add("Select Relation");
        spinnerList.add("Father");
        spinnerList.add("Mother");
        spinnerList.add("Brother");
        spinnerList.add("Sister");
        spinnerList.add("Spouse");
        spinnerList.add("Children");
        spinnerList.add("Others");

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerList);

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(position !=0 )
                {
                    String item = parent.getItemAtPosition(position).toString();
                    spinnerString = item;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        subuserNameTil = (TextInputLayout) findViewById(R.id.subuser_name_til);
        subuserEmailTil = (TextInputLayout) findViewById(R.id.subuser_email_til);
        subuserPhoneTil = (TextInputLayout) findViewById(R.id.subuser_phone_til);
        subuserNameEdt = (EditText) findViewById(R.id.subuser_name);
        subuserEmailEdt = (EditText) findViewById(R.id.subuser_email);
        subuserPhoneEdt = (EditText) findViewById(R.id.subuser_phone_number);
        subuserGenderId = (RadioGroup) findViewById(R.id.subuser_gender_id);
        subuserMale = (RadioButton) findViewById(R.id.subuser_male_radio);
        subuserFemale = (RadioButton) findViewById(R.id.subuser_female_radio);
        subuserCancel = (Button) findViewById(R.id.subuser_cancel);
        subuserCancel.setOnClickListener(this);
        subuserUpdate = (Button) findViewById(R.id.subuser_update);
        subuserUpdate.setOnClickListener(this);
        dobPicker.setOnClickListener(this);
        dispdob.setOnClickListener(this);

        Intent intent2 = getIntent();
        Bundle bundle = intent2.getExtras();

         subUserId = bundle.getString("subid");
         subId = bundle.getString("id");
         subName = bundle.getString("name");
         subAge = bundle.getString("age");
         subGender = bundle.getString("gender");
         subRelation = bundle.getString("relation");
         subEmail = bundle.getString("email");
         subMobile = bundle.getString("mobile");

        subuserNameEdt.setText(subName);
        dispdob.setText(subAge);
        int pos = spinnerList.indexOf(subRelation);
        spinner.setSelection(pos);
        subuserEmailEdt.setText(subEmail);
        subuserPhoneEdt.setText(subMobile);

        if(subGender.equals("male")){
            subuserMale.setChecked(true);
        }
        if(subGender.equals("female")){
            subuserFemale.setChecked(true);
        }
    }

    @Override
    public void onClick(View v)
    {
        if (v == view_user_close){
            finish();
        }
        if (v == dispdob)
        {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this,android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,int monthOfYear, int dayOfMonth) {

                            dispdob.setText(String.valueOf(dayOfMonth+" - "+(monthOfYear+1)+" - "+year));
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            Date birthDate = null;
                            try {
                                birthDate = sdf.parse(dayOfMonth+"/"+(monthOfYear+1)+"/"+year);

                                String age = calculateAge(birthDate);

                                String segments[] = age.split(",");
                                if(segments[0].equals("0"))
                                {
                                    if(segments[1].equals("0"))
                                    {
                                        if(segments[2].equals("0"))
                                        {

                                        }
                                    }
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                    }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }
        if (v == dobPicker)
        {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this,android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            dispdob.setText(String.valueOf(dayOfMonth+" - "+(monthOfYear+1)+" - "+year));

                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            Date birthDate = null;
                            try {
                                birthDate = sdf.parse(dayOfMonth+"/"+(monthOfYear+1)+"/"+year);

                                String age = calculateAge(birthDate);

                                String segments[] = age.split(",");
                                if(segments[0].equals("0"))
                                {
                                    if(segments[1].equals("0"))
                                    {
                                        if(segments[2].equals("0"))
                                        {
                                        }
                                    }
                                }

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }
        if(v == subuserUpdate)
        {
            Toast.makeText(getApplicationContext(),"working",Toast.LENGTH_LONG).show();
            statusFlag = NetworkUtil.isConnected(this);
            if (statusFlag) {

                if (validate()) {

                    if (dispdob.getText().toString().trim().length() != 0 && !dispdob.getText().toString().equals(null)) {

                        strSubuserName = subuserNameEdt.getText().toString();
                        strSubuserAge = dispdob.getText().toString();
                        strSubuserEmail = subuserEmailEdt.getText().toString();
                        strSubuserPhone = subuserPhoneEdt.getText().toString();

                        if (subuserMale.isChecked() || subuserFemale.isChecked()) {
                            subuserGender = (RadioButton) findViewById(subuserGenderId.getCheckedRadioButtonId());

                            if (subuserGender.getId() == subuserMale.getId()) {
                                strSubuserGender = "male";
                            }
                            if (subuserGender.getId() == subuserFemale.getId()) {
                                strSubuserGender = "female";
                            }
                        }
                        strSubuserResult = "Name : " + strSubuserName + ",Gender : " + strSubuserGender + ",Age : " + strSubuserAge + ",Relation : "
                                + ",Email : " + strSubuserEmail + ",PhoneNumber : " + strSubuserPhone;

                        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.EDIT_SUB_USER_PROFILE, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String responceCode = jsonObject.getString("status");
                                    if (responceCode.equals("20100")) {
                                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                        String message = jsonObject1.getString("msg");
                                        Toast.makeText(getApplicationContext(), "successfully Updated..!", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getApplicationContext(), ViewUsers.class);
                                        startActivity(intent);
                                    }
                                    if (responceCode.equals("10140")) {
                                        Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        startActivity(intent);
                                    }

                                    if (responceCode.equals("10150")) {
                                        userSessionManager.logoutUser();
                                        Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        startActivity(intent);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {

                                Map<String, String> params = new HashMap<String, String>();
                                params.put("sub_user_id", subUserId);
                                params.put("user_id", subId);
                                params.put("user_sub_name", strSubuserName);
                                params.put("relation", spinnerString);
                                params.put("mobile", strSubuserPhone);
                                params.put("gender", strSubuserGender);
                                params.put("dob", strSubuserAge);
                                params.put("email", strSubuserEmail);

                                return params;
                            }
                        };
                        RequestQueue requestQueue = Volley.newRequestQueue(this);
                        requestQueue.add(stringRequest);
                    } else {
                        Toast.makeText(getApplicationContext(), "Please provide Date of Birth..!!", Toast.LENGTH_SHORT).show();
                    }
                }
            }else {
                showInternetStatus();
            }
        }
        if(v == subuserCancel)
        {
            finish();
        }
    }

    private void showInternetStatus()
    {

        LayoutInflater inflater = getLayoutInflater();
        View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
        TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
        textView.setTypeface(typeface);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(toastLayout);
        toast.show();

    }

    private boolean validate()
    {
        boolean result = true;

         strSubuserName = subuserNameEdt.getText().toString();
        if (strSubuserName == null || strSubuserName.equals("")) {

            subuserNameTil.setError(getString(R.string.invalidName));
            result = false;
        }
        else
            subuserNameTil.setErrorEnabled(false);

        return result;
    }

    private String calculateAge(Date birthDate) {

        int years = 0;
        int months = 0;
        int days = 0;

        Calendar birthDay = Calendar.getInstance();
        birthDay.setTimeInMillis(birthDate.getTime());
        long currentTime = System.currentTimeMillis();
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(currentTime);
        years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
        int currMonth = now.get(Calendar.MONTH) + 1;
        int birthMonth = birthDay.get(Calendar.MONTH) + 1;
        months = currMonth - birthMonth;
        if (months < 0) {
            years--;
            months = 12 - birthMonth + currMonth;
            if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                months--;
        } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            years--;
            months = 11;
        }

        if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
            days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
        else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            int today = now.get(Calendar.DAY_OF_MONTH);
            now.add(Calendar.MONTH, -1);
            days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
        } else {
            days = 0;
            if (months == 12) {
                years++;
                months = 0;
            }
        }
        return years+","+months+","+days;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
