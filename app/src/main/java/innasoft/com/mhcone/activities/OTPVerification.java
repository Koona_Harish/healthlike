package innasoft.com.mhcone.activities;
import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;

public class OTPVerification extends AppCompatActivity implements View.OnClickListener {
    TextView otp_headding,resend_otp;
    TextInputLayout verify_otp_til,password_til,conformpassword_til;
    TextInputEditText password_et,conformpassword_et;
    EditText otp_et;
    Button verify_button;
    TextView displayEmailTV;
    String email,user_id,mobile;
    Typeface typeface;
    ProgressDialog progressDialog;
    SmsVerifyCatcher smsVerifyCatcher;
    public static final int MY_PERMISSIONS_REQUEST_READ_SMS = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpverification);

        Bundle bundle = getIntent().getExtras();
        email = bundle.getString("otpEmail");
        user_id = bundle.getString("user_id");
        mobile = bundle.getString("mobile");

        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");

        progressDialog = new ProgressDialog(OTPVerification.this, R.style.DialogTheme);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait......");

        otp_headding = (TextView) findViewById(R.id.otp_activity_headding);
        otp_headding.setTypeface(typeface);

        resend_otp = (TextView) findViewById(R.id.resend_otp);
        resend_otp.setTypeface(typeface);
        resend_otp.setOnClickListener(this);

        displayEmailTV = (TextView) findViewById(R.id.displayOTPEmail);

        displayEmailTV.setTypeface(typeface);
        displayEmailTV.setText(email);

        verify_otp_til = (TextInputLayout) findViewById(R.id.otp_activity_verify_til);
        verify_otp_til.setTypeface(typeface);

        password_til = (TextInputLayout) findViewById(R.id.otp_activity_password_til);
        password_til.setTypeface(typeface);

        conformpassword_til = (TextInputLayout) findViewById(R.id.otp_activity_conformpassword_til);
        conformpassword_til.setTypeface(typeface);

        otp_et = (EditText) findViewById(R.id.otp_et_verify);
        otp_et.setTypeface(typeface);

        password_et = (TextInputEditText) findViewById(R.id.otp_activity_password_et);
        password_et.setTypeface(typeface);

        conformpassword_et = (TextInputEditText) findViewById(R.id.otp_activity_conformpassword_et);
        conformpassword_et.setTypeface(typeface);

        verify_button = (Button) findViewById(R.id.submit_otp);
        verify_button.setTypeface(typeface);
        verify_button.setOnClickListener(this);

        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = message.replaceAll("[^0-9]", "");
                otp_et.setText(code);
                Log.v("message >>> ", message.replaceAll("[^0-9]", ""));
            }
        });
    }

    @Override
    protected void onResume() {
        checkCallPermission();
        super.onResume();
    }

    public boolean checkCallPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CALL_PHONE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Call")
                        .setMessage("Call Permission Required")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(OTPVerification.this,
                                        new String[]{Manifest.permission.CALL_PHONE},
                                        MY_PERMISSIONS_REQUEST_READ_SMS);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        MY_PERMISSIONS_REQUEST_READ_SMS);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }

    @Override
    public void onClick(View view) {
        if (view == resend_otp)
        {
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.RESEND_OTP,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successCodeOTP = jsonObject.getString("status");
                                if(successCodeOTP.equals("2000"))
                                {
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    String message = jsonObject1.getString("msg");
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                                }
                                if(successCodeOTP.equals("5000"))
                                {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Please Provide All Details....!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            String responseBody = null;

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    })
            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    Log.d("USEID:",user_id);
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);
        }
        if(view == verify_button) {
            if (validate()) {
                progressDialog.show();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.OTPVERIFICATION_URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String successCodeOTP = jsonObject.getString("status");
                                    if(successCodeOTP.equals("50100"))
                                    {
                                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                        String message = jsonObject1.getString("msg");
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                        startActivity(intent);
                                    }
                                    if(successCodeOTP.equals("40111"))
                                    {
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "Please Check OTP....!", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                String responseBody = null;


                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {


                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        })
                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("otp", otp_et.getText().toString().trim());
                        params.put("email", email);
                        params.put("password", password_et.getText().toString().trim());
                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                requestQueue.add(stringRequest);
            }
        }
    }

    private void otpVerification() {

    }

    private boolean validate()
    {
        boolean result = true;

        String verify = otp_et.getText().toString().trim();
        if(verify.length() == 0){
            verify_otp_til.setError(getString(R.string.otp));
            result = false;

        }else
            verify_otp_til.setErrorEnabled(false);


        String password = password_et.getText().toString().trim();
        if (password.length() == 0 || password.length() < 3) {
            password_til.setError(getString(R.string.invalidPasswor));
            result = false;
        }
        else
            password_til.setErrorEnabled(false);

        String repassword = conformpassword_et.getText().toString().trim();
        if (repassword.length() == 0 || repassword.length() < 3) {
            conformpassword_til.setError(getString(R.string.invalidPasswor));
            result = false;
        }
        else
            conformpassword_til.setErrorEnabled(false);

        if(!password.equals(repassword))
        {
            password_til.setError(getString(R.string.invalidPassworMisMatch));
            conformpassword_til.setError(getString(R.string.invalidPassworMisMatch));
        }
        else {
            password_til.setErrorEnabled(false);
            conformpassword_til.setErrorEnabled(false);
        }

        return result;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (String permission : permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                //denied
                Log.e("denied", permission);
                Toast.makeText(getApplicationContext(), "Permissions required", Toast.LENGTH_SHORT).show();
            } else {
                if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                    //allowed
                    Log.e("allowed", permission);
//                    gps = new GPSTrackers(getApplicationContext());

                } else {
                    //set to never ask again
                    Log.e("set to never ask again", permission);
                    //do something here.
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }
            }
        }
    }
}