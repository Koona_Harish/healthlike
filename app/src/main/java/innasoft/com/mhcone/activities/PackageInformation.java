package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.PackageInformationAdapter;
import innasoft.com.mhcone.models.PackageInformationModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.GPSTrackers;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class PackageInformation extends AppCompatActivity implements View.OnClickListener {

    ArrayList<PackageInformationModel> packageInformationArrayList;
    PackageInformationAdapter adapter;
    RecyclerView package_info_recycler_view;
    ImageView editMyPack,deleteMyPack;
    ImageButton package_btn_close;
    Typeface type_lato,type_lato2;
    ImageButton closeButton;
    TextView itemHeadding;
    ProgressDialog progressDialog;
    UserSessionManager userSessionManager;
    String access_key,disp_userName,disp_email;
    String userID,user_package_id,package_name;
    Button btnSelectLabPack;
    GPSTrackers gps;
    List<String> subtestName,subtestIds,stringFormateforIDs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_information);

        type_lato = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        type_lato2 = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        gps = new GPSTrackers(getApplicationContext());

        subtestName = new ArrayList<String>();
        subtestIds = new ArrayList<String>();
        stringFormateforIDs = new ArrayList<String>();

        btnSelectLabPack = (Button) findViewById(R.id.btnSelectLabPack);
        btnSelectLabPack.setOnClickListener(this);
        btnSelectLabPack.setTypeface(type_lato);

        itemHeadding = (TextView) findViewById(R.id.itemHeadding);
        itemHeadding.setTypeface(type_lato2);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        editMyPack = (ImageView) findViewById(R.id.editMyPack);
        editMyPack.setOnClickListener(this);

        deleteMyPack = (ImageView) findViewById(R.id.deleteMyPack);
        deleteMyPack.setOnClickListener(this);

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = userSessionManager.getUserDetails();
        access_key = user.get(UserSessionManager.KEY_ACCSES);

        disp_userName = user.get(UserSessionManager.USER_NAME);
        disp_email = user.get(UserSessionManager.USER_EMAIL);
        userID = user.get(UserSessionManager.USER_ID);

        Bundle bundle = getIntent().getExtras();
        user_package_id = bundle.getString("user_package_id");
        package_name = bundle.getString("package_name");

        Log.d("BUNDLEEEE",user_package_id+"\n"+package_name);
        getSupportActionBar().setTitle(package_name);
        itemHeadding.setText(package_name);

        packageInformationArrayList = new ArrayList<PackageInformationModel>();
        package_info_recycler_view = (RecyclerView) findViewById(R.id.package_info_recycler_view);
        package_info_recycler_view.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        package_info_recycler_view.setLayoutManager(layoutManager);

        gettingMyPackagesTests();

        package_btn_close = (ImageButton) findViewById(R.id.package_btn_close);
        package_btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), ViewMyPackage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });


    }

    private void gettingMyPackagesTests() {

        PackageInformation.this.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.show();

            }
        });

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.MY_PACKAGE_NAME_TESTS,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response)
                    {
                       packageInformationArrayList.clear();
                        Log.d("PACKAGEINFORMATION::",response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String statusCode = jsonObject.getString("status");
                            if(statusCode.equals("19999"))
                            {
                                ArrayList<PackageInformationModel> predictions = new ArrayList<PackageInformationModel>();

                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                Log.d("JSONARRAYYYY1",jsonArray.toString());

                                for (int i = 0; i < jsonArray.length(); i++)
                                {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);//.getJSONObject("my_package_test");
                                    Log.d("JSONOBJECT1",jsonObject1.toString());

                                    PackageInformationModel packageInformationModel = new PackageInformationModel();

                                    packageInformationModel.id = jsonObject1.getString("id");
                                    subtestIds.add(jsonObject1.getString("sub_test_id"));
                                    subtestName.add(jsonObject1.getString("sub_test_name"));
                                    stringFormateforIDs.add("0-"+jsonObject1.getString("test_id")+"-"+jsonObject1.getString("sub_test_id")+"-0");
                                    packageInformationModel.user_package_id = jsonObject1.getString("user_package_id");
                                    packageInformationModel.test_id = jsonObject1.getString("test_id");
                                    packageInformationModel.sub_test_id = jsonObject1.getString("sub_test_id");
                                    packageInformationModel.status = jsonObject1.getString("status");
                                    packageInformationModel.created_time = jsonObject1.getString("created_time");
                                    packageInformationModel.package_name = jsonObject1.getString("package_name");
                                    packageInformationModel.sub_test_name = jsonObject1.getString("sub_test_name");

                                    predictions.add(packageInformationModel);

                                }
                                adapter = new PackageInformationAdapter(predictions, PackageInformation.this,R.layout.inner_item_layout);
                                package_info_recycler_view.setAdapter(adapter);

                                PackageInformation.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        progressDialog.dismiss();

                                    }
                                });
                            }
                            if(statusCode.equals("18888"))
                            {
                                PackageInformation.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        progressDialog.dismiss();

                                    }
                                });
                                Toast.makeText(getApplicationContext(),
                                        getApplicationContext().getString(R.string.nodata_found),
                                        Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        progressDialog.dismiss();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", userID);
                params.put("user_package_id", user_package_id);
                Log.d("PARAMMMMMM:",params.toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View view) {
        if(view == editMyPack)
        {
            Intent intent = new Intent(getApplicationContext(), UpdateMyPackage.class);
            intent.putExtra("packageID", user_package_id);
            intent.putExtra("packageName", package_name);
            startActivity(intent);
        }
        if(view == deleteMyPack)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(PackageInformation.this);
            builder.setMessage("Do you really want to Delete This Package").setTitle("Alert..");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    deleteSelectetedPackage(user_package_id);
                }
            });

            builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    dialogInterface.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }

        if( view == btnSelectLabPack)
        {

            if (gps.canGetLocation()) {

                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();

                Intent intent = new Intent(PackageInformation.this, ShowUserTestLabs.class);
                intent.putExtra("latValue", Double.toString(latitude));
                intent.putExtra("longValue", Double.toString(longitude));
                intent.putExtra("packageName", package_name);

                intent.putExtra("subtestIdswithformate", subtestIds.toString().replaceAll("\\[", "").replaceAll("\\]","").replaceAll(" ", ""));
                intent.putExtra("subtestNameswithfromate", subtestName.toString().replaceAll("\\[", "").replaceAll("\\]","").replaceAll(" ", ""));
                intent.putExtra("forbillingPageIDs", stringFormateforIDs.toString().replaceAll("\\[", "").replaceAll("\\]","").replaceAll(" ", ""));
                Log.d("VALUESPRINTING", latitude+"\n"+longitude+"\n"+package_name+"\n"+subtestIds.toString()+"\n"+subtestName.toString()+"\n"+stringFormateforIDs.toString());
                startActivity(intent);
            }
            else {
                Toast.makeText(getApplicationContext(), "Please Check GPS connection ....!", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void deleteSelectetedPackage(final String user_package_id) {
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equals("19999")) {
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.DELETE_MYPACKAGE,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                        try {
                                            JSONObject jsonObject =new JSONObject(response);

                                            String responceCode = jsonObject.getString("status");
                                            if(responceCode.equals("20100")) {
                                                Intent intent = new Intent(getApplicationContext(), ViewMyPackage.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                progressDialog.dismiss();
                                                startActivity(intent);


                                            }
                                            if(responceCode.equals("10140"))
                                            {
                                                userSessionManager.logoutUser();
                                                progressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                startActivity(intent);
                                            }

                                            if(responceCode.equals("10150"))
                                            {
                                                userSessionManager.logoutUser();
                                                progressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                startActivity(intent);
                                            }
                                        }
                                        catch (JSONException e)
                                        {
                                            e.printStackTrace();
                                            progressDialog.dismiss();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        progressDialog.dismiss();

                                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                        } else if (error instanceof AuthFailureError) {

                                        } else if (error instanceof ServerError) {

                                        } else if (error instanceof NetworkError) {

                                        } else if (error instanceof ParseError) {

                                        }
                                    }
                                })
                        {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("user_id", userID);
                                params.put("package_id", user_package_id);
                                return params;
                            }
                        };

                        RequestQueue requestQueue = Volley.newRequestQueue(PackageInformation.this);
                        requestQueue.add(stringRequest);
                    }
                    if (responceCode.equals("10140")) {

                        userSessionManager.logoutUser();
                        Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }

                    if (responceCode.equals("10150")) {

                        userSessionManager.logoutUser();
                        Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", userID);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(PackageInformation.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
