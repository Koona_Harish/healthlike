package innasoft.com.mhcone.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.ConnectionDetector;
import innasoft.com.mhcone.utilities.NetworkStatus;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    TextView i_am_txt, tv_already_registered, tv_login_here;
    EditText et_firstName, et_lastName, et_email, et_phoneNumber;
    TextInputEditText et_password, et_re_password;
    Button submit_reg;
    RadioGroup genderGroup;
    RadioButton maleRadioButton, femaleRadioButtion, genderStatus;
    int gunder_status = 0;
    Button submit_register_cancel;
    Typeface typeface, typeface2;
    NetworkStatus ns;
    Boolean isOnline = false;
    TextInputLayout firstNameTIL, lastNameTIL, emailTIL, passwordTIL, repasswordTIL, phoneTIL;
    String send_first_name, send_last_name, send_age, send_gender, send_email, send_phone_number, send_password, send_role_id;
    ConnectionDetector cd;
    Boolean isInternetPresent = false;
    ProgressDialog progressDialog;
    AlertDialog.Builder alertDialogBuilder;
    public CheckBox terms_conditions_ck;

    TextView reg_activity_headding, dispdob, selectdob, terms_conditions_tv;
    ImageView dobPicker;

    public static List<String> temp = new ArrayList<String>();

    Map hashMapKeyValuTwo = new HashMap();

    private int mYear, mMonth, mDay, mHour, mMinute;
    String loginIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ns = new NetworkStatus();
        isOnline = ns.isOnline(RegisterActivity.this);

        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typeface2 = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        reg_activity_headding = (TextView) findViewById(R.id.reg_activity_headding);
        reg_activity_headding.setTypeface(typeface2);

        i_am_txt = (TextView) findViewById(R.id.i_am_txt);
        i_am_txt.setTypeface(typeface);

        dispdob = (TextView) findViewById(R.id.dobdisp);
        selectdob = (TextView) findViewById(R.id.selectdob);
        dobPicker = (ImageView) findViewById(R.id.dobPicker);

        terms_conditions_ck = (CheckBox) findViewById(R.id.terms_conditions_ck);
        terms_conditions_ck.setOnCheckedChangeListener(this);

        submit_register_cancel = (Button) findViewById(R.id.submit_register_cancel);

        cd = new ConnectionDetector(RegisterActivity.this);

        alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());

        progressDialog = new ProgressDialog(RegisterActivity.this, R.style.DialogTheme);

        progressDialog.setMessage("Registering......");

        ConnectivityManager connManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        isInternetPresent = cd.isConnectingToInternet();

        tv_already_registered = (TextView) findViewById(R.id.reg_already);
        tv_already_registered.setTypeface(typeface);
        terms_conditions_tv = (TextView) findViewById(R.id.terms_conditions_tv);
        terms_conditions_tv.setTypeface(typeface);
        tv_login_here = (TextView) findViewById(R.id.reg_loginhere);
        tv_login_here.setTypeface(typeface);

        firstNameTIL = (TextInputLayout) findViewById(R.id.firstnametil);
        lastNameTIL = (TextInputLayout) findViewById(R.id.lastnametil);
        emailTIL = (TextInputLayout) findViewById(R.id.emailtil);
        phoneTIL = (TextInputLayout) findViewById(R.id.phonetil);
        passwordTIL = (TextInputLayout) findViewById(R.id.passwordtil);
        repasswordTIL = (TextInputLayout) findViewById(R.id.cnfpasswordtil);
        genderGroup = (RadioGroup) findViewById(R.id.gender_id);
        maleRadioButton = (RadioButton) findViewById(R.id.male_radio);
        femaleRadioButtion = (RadioButton) findViewById(R.id.female_radio);

        et_firstName = (EditText) findViewById(R.id.first_name);
        et_firstName.setTypeface(typeface);
        et_lastName = (EditText) findViewById(R.id.last_name);
        et_lastName.setTypeface(typeface);
        et_email = (EditText) findViewById(R.id.email);
        et_email.setTypeface(typeface);
        et_phoneNumber = (EditText) findViewById(R.id.phone_number);
        et_phoneNumber.setTypeface(typeface);
        et_password = (TextInputEditText) findViewById(R.id.password);
        et_password.setTypeface(typeface);
        et_re_password = (TextInputEditText) findViewById(R.id.cnfpassword);
        et_re_password.setTypeface(typeface);
        dobPicker.setOnClickListener(this);
        dispdob.setOnClickListener(this);
        selectdob.setOnClickListener(this);
        terms_conditions_tv.setOnClickListener(this);

        submit_reg = (Button) findViewById(R.id.submit_registration);
        submit_reg.setTypeface(typeface);
        submit_reg.setClickable(false);
        // submit_reg.setBackgroundColor(Color.GRAY);
        // submit_reg.setTextColor(Color.BLACK);

        if (getIntent() != null) {
            if (getIntent().getStringExtra("Login") != null) {
                loginIntent = getIntent().getStringExtra("Login");
            }
        }

        submit_register_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        submit_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOnline) {
                    if (maleRadioButton.isChecked() || femaleRadioButtion.isChecked()) {
                        genderStatus = (RadioButton) findViewById(genderGroup.getCheckedRadioButtonId());

                        if (genderStatus.getId() == maleRadioButton.getId()) {
                            gunder_status = 1;
                        }
                        if (genderStatus.getId() == femaleRadioButtion.getId()) {
                            gunder_status = 1;
                        }
                    }

                    if (terms_conditions_ck.isChecked()) {
                        if (validateData()) {
                            if (dispdob.getText().toString().trim().length() != 0 && !dispdob.getText().toString().equals(null)) {
                                progressDialog.show();

                                if ((gunder_status == 1) && (et_firstName.getText().toString().trim().length() > 0)
                                        && (et_lastName.getText().toString().trim().length() > 0) && (et_email.getText().toString().trim().length() > 0)
                                        && (et_phoneNumber.getText().toString().trim().length() > 0)
                                        && (et_password.getText().toString().trim().length() > 0)
                                        && (et_re_password.getText().toString().trim().length() > 0)) {
                                    if (et_password.getText().toString().equals(et_re_password.getText().toString())) {
                                        if (isInternetPresent) {
                                            send_first_name = et_firstName.getText().toString().trim();
                                            send_last_name = et_lastName.getText().toString().trim();
                                            send_age = dispdob.getText().toString().trim();

                                            if (maleRadioButton.isChecked() || femaleRadioButtion.isChecked()) {
                                                genderStatus = (RadioButton) findViewById(genderGroup.getCheckedRadioButtonId());

                                                if (genderStatus.getId() == maleRadioButton.getId()) {
                                                    send_gender = "male";

                                                }
                                                if (genderStatus.getId() == femaleRadioButtion.getId()) {
                                                    send_gender = "female";

                                                }
                                            }
                                            send_email = et_email.getText().toString().trim();
                                            send_phone_number = et_phoneNumber.getText().toString().trim();
                                            send_password = et_password.getText().toString().trim();

                                            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.REGISTRATION_URL,
                                                    new Response.Listener<String>() {
                                                        @Override
                                                        public void onResponse(String response) {
                                                            progressDialog.dismiss();
                                                            try {
                                                                JSONObject jsonObject = new JSONObject(response);
                                                                String editSuccessResponceCode = jsonObject.getString("status");
                                                                if (editSuccessResponceCode.equalsIgnoreCase("20100")) {
                                                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                                                    String message = jsonObject1.getString("msg");
                                                                    String userId = jsonObject1.getString("user_id");
                                                                    progressDialog.dismiss();
                                                                    Toast.makeText(getApplicationContext(), "OTP has been sent to your registered Email Id...! ", Toast.LENGTH_SHORT).show();
                                                                    if (loginIntent.length() > 0 && loginIntent.equalsIgnoreCase("other")) {
                                                                        Intent intent = new Intent(RegisterActivity.this, AccountVerification.class);
                                                                        intent.putExtra("userId", userId);
                                                                        intent.putExtra("email", send_email);
                                                                        intent.putExtra("mobile", send_phone_number);
                                                                        intent.putExtra("Login", loginIntent);
                                                                        startActivity(intent);
                                                                    } else {
                                                                        Intent intent = new Intent(RegisterActivity.this, AccountVerification.class);
                                                                        intent.putExtra("userId", userId);
                                                                        intent.putExtra("email", send_email);
                                                                        intent.putExtra("mobile", send_phone_number);
                                                                        startActivity(intent);
                                                                    }
                                                                }
                                                                if (editSuccessResponceCode.equals("20120")) {
                                                                    Toast.makeText(getApplicationContext(), "Email already exits", Toast.LENGTH_SHORT).show();
                                                                }
                                                                if (editSuccessResponceCode.equals("20140")) {
                                                                    Toast.makeText(getApplicationContext(), "Mobile already exits", Toast.LENGTH_SHORT).show();
                                                                }
                                                                if (editSuccessResponceCode.equals("20130")) {
                                                                    Toast.makeText(getApplicationContext(), "Please check all details....!", Toast.LENGTH_SHORT).show();
                                                                }

                                                            } catch (JSONException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    },
                                                    new Response.ErrorListener() {
                                                        @Override
                                                        public void onErrorResponse(VolleyError error) {
                                                            progressDialog.dismiss();

                                                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                                            } else if (error instanceof AuthFailureError) {

                                                            } else if (error instanceof ServerError) {

                                                            } else if (error instanceof NetworkError) {

                                                            } else if (error instanceof ParseError) {

                                                            }
                                                        }
                                                    }) {
                                                @Override
                                                protected Map<String, String> getParams() throws AuthFailureError {
                                                    Map<String, String> params = new HashMap<String, String>();

                                                    params.put("user_name", send_first_name + " " + send_last_name);
                                                    params.put("dob", send_age);
                                                    params.put("gender", send_gender);
                                                    params.put("email", send_email);
                                                    params.put("mobile", send_phone_number);
                                                    params.put("role_id", "2");
                                                    params.put("password", send_password);

                                                    return params;
                                                }
                                            };
                                            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                                            RequestQueue requestQueue = Volley.newRequestQueue(RegisterActivity.this);
                                            requestQueue.add(stringRequest);
                                        } else {
                                            progressDialog.dismiss();
                                            showAlertDialog(RegisterActivity.this, "No Internet Connection", "Please Check Your Internet Connection", false);
                                        }
                                    } else {
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "Password and Confirm Password must be same..!!", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Please provide all the details..!!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Please provide Date of Birth..!!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Please provide all the details..!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    showInternetStatus();
                }
            }
        });

        tv_login_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOnline) {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                } else {
                    showInternetStatus();
                }
            }
        });
    }

    private void showInternetStatus() {

        LayoutInflater inflater = getLayoutInflater();
        View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
        TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
        textView.setTypeface(typeface);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(toastLayout);
        toast.show();
    }

    private void showAlertDialog(Context context, String title, String message, Boolean status) {

        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.sucess : R.drawable.fail);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private boolean validateData() {

        boolean result = true;
        String name = et_firstName.getText().toString().trim();
        if ((name == null || name.length() < 3) && name != "^[a-zA-Z\\\\s]+") {

            firstNameTIL.setError(getString(R.string.invalidName));
            result = false;
        } else
            firstNameTIL.setErrorEnabled(false);
        String surname = et_lastName.getText().toString().trim();
        if (surname == null || surname.length() < 3) {

            lastNameTIL.setError(getString(R.string.invalidSurnameName));
            result = false;
        } else
            lastNameTIL.setErrorEnabled(false);

        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";

        String email = et_email.getText().toString().trim();
        if (!email.matches(EMAIL_REGEX)) {
            emailTIL.setError(getString(R.string.invalidEmailID));
            result = false;
        } else
            emailTIL.setErrorEnabled(false);

        String MOBILE_REGEX = "^[789]\\d{9}$";
        String phone = et_phoneNumber.getText().toString().trim();

        if (!phone.matches(MOBILE_REGEX)) {
            phoneTIL.setError("Invalid Mobile Number");
            result = false;
        } else {
            phoneTIL.setError(null);
        }

        /*String phone = et_phoneNumber.getText().toString().trim();
        if ((phone == null || phone.equals("")) || phone.length() != 10) {
            phoneTIL.setError(getString(R.string.invalidNumber));
            result = false;
        }
        else
        phoneTIL.setErrorEnabled(false);*/

        String password = et_password.getText().toString().trim();
        if (password.length() < 6) {
            passwordTIL.setError("Minimum 6 characters");
            result = false;
        } else
            passwordTIL.setErrorEnabled(false);

        String repassword = et_re_password.getText().toString().trim();
        if (repassword.length() < 6) {
            repasswordTIL.setError(getString(R.string.reinvalidPasswor));
            result = false;
        } else
            repasswordTIL.setErrorEnabled(false);

        if (!password.equals(repassword) || password.equals("")) {
            passwordTIL.setError("Minimum 6 characters");
            repasswordTIL.setError(getString(R.string.invalidPassworMisMatch));
        } else {
            passwordTIL.setErrorEnabled(false);
            repasswordTIL.setErrorEnabled(false);
        }
        return result;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        if (v == dispdob) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    new DatePickerDialog.OnDateSetListener() {


                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            dispdob.setText(String.valueOf(dayOfMonth + " - " + (monthOfYear + 1) + " - " + year));
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            Date birthDate = null;
                            try {
                                birthDate = sdf.parse(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);

                                String age = calculateAge(birthDate);
                                String segments[] = age.split(",");

                                if (segments[0].equals("0")) {
                                    if (segments[1].equals("0")) {
                                        if (segments[2].equals("0")) {

                                        }
                                    }
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                    }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }

        if (v == selectdob) {

            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            dispdob.setText(String.valueOf(dayOfMonth + " - " + (monthOfYear + 1) + " - " + year));
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            Date birthDate = null;
                            try {
                                birthDate = sdf.parse(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);

                                String age = calculateAge(birthDate);
                                String segments[] = age.split(",");
                                if (segments[0].equals("0")) {
                                    if (segments[1].equals("0")) {
                                        if (segments[2].equals("0")) {

                                        }
                                    }
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }

                    }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }
        if (v == dobPicker) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            dispdob.setText(String.valueOf(dayOfMonth + " - " + (monthOfYear + 1) + " - " + year));

                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            Date birthDate = null;
                            try {
                                birthDate = sdf.parse(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);

                                String age = calculateAge(birthDate);
                                String segments[] = age.split(",");
                                if (segments[0].equals("0")) {
                                    if (segments[1].equals("0")) {
                                        if (segments[2].equals("0")) {

                                        } else {

                                        }
                                    } else {

                                    }
                                } else {

                                }

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }

        if (v == terms_conditions_tv) {
            terms_conditions_tv.setTextColor(Color.parseColor("#43A047"));
            Intent intent = new Intent(getApplicationContext(), TermsConditions.class);
            startActivity(intent);
        }
    }

    private String calculateAge(Date birthDate) {

        int years = 0;
        int months = 0;
        int days = 0;

        Calendar birthDay = Calendar.getInstance();
        birthDay.setTimeInMillis(birthDate.getTime());

        long currentTime = System.currentTimeMillis();
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(currentTime);

        years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
        int currMonth = now.get(Calendar.MONTH) + 1;
        int birthMonth = birthDay.get(Calendar.MONTH) + 1;

        months = currMonth - birthMonth;

        if (months < 0) {
            years--;
            months = 12 - birthMonth + currMonth;
            if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                months--;
        } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            years--;
            months = 11;
        }

        if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
            days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
        else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            int today = now.get(Calendar.DAY_OF_MONTH);
            now.add(Calendar.MONTH, -1);
            days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
        } else {
            days = 0;
            if (months == 12) {
                years++;
                months = 0;
            }
        }

        return years + "," + months + "," + days;
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

        if (terms_conditions_ck == compoundButton) {
            if (isChecked) {
                //submit_reg.setBackgroundColor(Color.parseColor("#1FB991"));
                //submit_reg.setTextColor(Color.WHITE);
                submit_reg.setClickable(true);
            } else {
                //submit_reg.setBackgroundColor(Color.GRAY);
                //  submit_reg.setTextColor(Color.WHITE);
                submit_reg.setClickable(false);
            }
        }
    }
}
