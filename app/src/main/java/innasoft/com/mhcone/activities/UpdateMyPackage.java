package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.UpdateMyPackageListAdapter;
import innasoft.com.mhcone.models.UpdateMyPackageSubtestModel;
import innasoft.com.mhcone.models.UpdateMyPackageTestsModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.UpdatePackageDBHandler;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class UpdateMyPackage extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    String myPackageId;
    private int lastExpandedPosition = -1;

    public ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<UpdateMyPackageSubtestModel> subItemList = null;
    List<UpdateMyPackageTestsModel> superitemList = null;
    UpdateMyPackageSubtestModel subtestModel = null;
    UpdateMyPackageTestsModel superItem = null;
    Button updateMyPackage;
    UpdatePackageDBHandler db;
    EditText mpackName;
    ImageButton update_cancel;
    String selecteList = null;
    Typeface typeface,typebold;
    ProgressDialog progressDialog;
    String myPackageName;
    TextView txt_test_name;
    UserSessionManager userSessionManager;
    String access_key,disp_userName,disp_email;
    String userID;
    public String selectedList = new String();
    private SwipeRefreshLayout mSwipeRefreshLayout;
    int expand_status = 0;
    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_my_package);

        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typebold = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("About Healthlike");

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        subtestModel = new UpdateMyPackageSubtestModel();

        db = new UpdatePackageDBHandler(getApplicationContext());
        mpackName = (EditText) findViewById(R.id.packNameetgh);

        txt_test_name = (TextView) findViewById(R.id.txt_test_name);
        txt_test_name.setTypeface(typebold);

        update_cancel = (ImageButton) findViewById(R.id.btn_close);
        update_cancel.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        myPackageId = bundle.getString("packageID");
        Log.d("MYPACKAGEID", myPackageId);
        myPackageName = bundle.getString("packageName");

        mpackName.setText(myPackageName);

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = userSessionManager.getUserDetails();
        access_key = user.get(UserSessionManager.KEY_ACCSES);

        disp_userName = user.get(UserSessionManager.USER_NAME);
        disp_email = user.get(UserSessionManager.USER_EMAIL);
        userID = user.get(UserSessionManager.USER_ID);



        progressDialog = new ProgressDialog(UpdateMyPackage.this, R.style.DialogTheme);


        progressDialog.setMessage("Please wait......");
        progressDialog.setCancelable(false);

        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.update_package_swipe_layout);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,

                android.R.color.holo_green_light,

                android.R.color.holo_orange_light,

                android.R.color.holo_red_light);

        mSwipeRefreshLayout.setOnRefreshListener(this);

        gettinMySelectedList(myPackageId);
        updateMyPackage = (Button) findViewById(R.id.updateMyPackage);
        updateMyPackage.setTypeface(typebold);
        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);

       // if(!selectedList.isEmpty())
        fillData();
       /* else {
            gettinMySelectedList(myPackageId);
            fillData();


        }*/

        updateMyPackage.setOnClickListener(this);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                expand_status = 1;
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    expandableListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;




            }
        });


        expandableListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                boolean enable = false;
                if(expandableListView != null && expandableListView.getChildCount() > 0){
                    boolean firstItemVisible = expandableListView.getFirstVisiblePosition() == 0;
                    boolean topOfFirstItemVisible = expandableListView.getChildAt(0).getTop() == 0;
                    enable = firstItemVisible && topOfFirstItemVisible;
                }
                mSwipeRefreshLayout.setEnabled(enable);
            }
        });


    }






    private void gettinMySelectedList(final String myPackageId) {

        StringRequest stringRequest1 = new StringRequest(Request.Method.POST, AppUrls.MYPACKAGES_INFO,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject jsonObject = new JSONObject(response);

                            selectedList = jsonObject.getString("sub_test_id");


                            returnSelectetdIds(selectedList);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {


                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }


                    }
                }


        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_package_id", myPackageId);


                return params;
            }
        };

        stringRequest1.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest1);

    }

    private void returnSelectetdIds(String selectedList) {
        Log.d("Chanti", "Selected Values \n"+selectedList);
        selectedList = selectedList;
    }



    private void fillData() {
        progressDialog.show();
        db.deleteTableData();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_TESTS_AND_SUBTESTS_TWO,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("UPDATEPACKAGEGETTING", response);
                            String responceCode = jsonObject.getString("status");
                            if(responceCode.equals("19999")) {
                                superitemList = new ArrayList<UpdateMyPackageTestsModel>();
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    superItem = new UpdateMyPackageTestsModel();
                                    subItemList = new ArrayList<UpdateMyPackageSubtestModel>();
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    String id = jsonObject1.getString("test_id");
                                    String data = jsonObject1.getString("test_name");

                                    String test_image = jsonObject1.getString("test_image");


                                    superItem.setTest_name(data);
                                    superItem.setTest_id(id);
                                    superItem.setTest_image(AppUrls.BASE_URL + test_image);

                                    JSONArray jsonArray1 = jsonObject1.getJSONArray("sub_test_wise");
                                    if (jsonArray1 != null && jsonArray1.length() != 0) {
                                        for (int j = 0; j < jsonArray1.length(); j++) {

                                            subtestModel = new UpdateMyPackageSubtestModel();

                                            JSONObject jsonObject2 = jsonArray1.getJSONObject(j);
                                            String test_id = jsonObject2.getString("test_id");
                                            String sub_test_id = jsonObject2.getString("sub_test_id");
                                            String data2 = jsonObject2.getString("sub_test_name");


                                            Log.d("Chanti", "Condition : "+selectedList+"===="+sub_test_id+",,,"+selectedList.length());

                                            if(selectedList.length()!=0)
                                            {

                                            if (selectedList.contains(sub_test_id)) {

                                                ContentValues values = new ContentValues();
                                                values.put(UpdatePackageDBHandler.KEY_SUB_TEST_ID, sub_test_id); // Shop Name
                                                values.put(UpdatePackageDBHandler.KEY_SUB_TEST_NAME, data2); // Shop Phone Number
                                                values.put(UpdatePackageDBHandler.KEY_SUB_TEST_STATUS, "true");


                                                values.put(UpdatePackageDBHandler.KEY_TEST_NAME, "ABC");
                                                values.put(UpdatePackageDBHandler.KEY_TEST_ID, test_id);
                                                db.addSubtest(values);
                                            }
                                            }

                                            subtestModel.setSub_test_name(data2);
                                            subtestModel.setTest_id(test_id);
                                            subtestModel.setSub_test_id(sub_test_id);
                                            subItemList.add(subtestModel);
                                        }
                                        superItem.setSubtestModel(subItemList);
                                        superitemList.add(superItem);
                                        progressDialog.dismiss();
                                    }

                                }

                                expandableListAdapter = new UpdateMyPackageListAdapter(UpdateMyPackage.this, superitemList, myPackageId);
                                expandableListView.setAdapter(expandableListAdapter);
                                mSwipeRefreshLayout.setRefreshing(false);
                                expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                                    @Override
                                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                                        Toast.makeText(getApplicationContext(), superitemList.get(groupPosition).getTest_id() + " : " + superitemList.get(groupPosition).getSubtestModel().get(childPosition).getSub_test_id(), Toast.LENGTH_SHORT).show();
                                        return false;
                                    }
                                });
                            }
                            if(responceCode.equals("18888")) {
                                progressDialog.dismiss();
                                mSwipeRefreshLayout.setRefreshing(false);
                                userSessionManager.logoutUser();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);

                            }

                            if(responceCode.equals("10140"))
                            {

                                userSessionManager.logoutUser();
                                Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }

                            if(responceCode.equals("10150"))
                            {

                                userSessionManager.logoutUser();
                                Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UpdateMyPackage.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();
                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);




                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {


                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }

                    }
                }


        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", userID);
                return params;
            }
        };
        //RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    @Override
    public void onClick(View v) {
        if(v == updateMyPackage)
        {




            List<String> testIdsList = db.getAllTestIds();
            List<String> subTestIdsList = db.getAllSubTestIds();
            List<String> sendList = new ArrayList<String>();
            for (int i = 0; i <testIdsList.size(); i++)
            {
                sendList.add(testIdsList.get(i)+"-"+subTestIdsList.get(i));
            }
            final String sendListString = sendList.toString().replaceAll("\\[", "").replaceAll("\\]","").replaceAll(" ", "");




            if(mpackName.getText().toString().trim().length() != 0 && !mpackName.getText().toString().equals(null))
            {

                if(subTestIdsList.size() != 0) {

                    if(expand_status == 1) {

                    progressDialog.show();
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.UPDATE_MYPACKAGE,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                    progressDialog.dismiss();
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        db.deleteTableData();

                                        Toast.makeText(UpdateMyPackage.this, "Successfully Updated...!", Toast.LENGTH_SHORT).show();
                                        Intent in = new Intent(UpdateMyPackage.this, MainActivity.class);
                                        startActivity(in);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    progressDialog.dismiss();




                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    } else if (error instanceof AuthFailureError) {

                                    } else if (error instanceof ServerError) {

                                    } else if (error instanceof NetworkError) {

                                    } else if (error instanceof ParseError) {

                                    }

                                }
                            }
                    )


                    {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            String pakName = mpackName.getText().toString();
                            Log.d("UPDATEPACKAGEVALUE", "user_package_name "+mpackName.getText().toString()+"\n user_id"+userID+"\n test_id"+sendListString+"\n id"+myPackageId);
                            params.put("user_package_name", mpackName.getText().toString());
                            params.put("user_id", userID);
                            params.put("test_id", sendListString);
                            params.put("id", myPackageId);


                            return params;
                        }
                    };
                    RequestQueue requestQueue = Volley.newRequestQueue(this);
                    requestQueue.add(stringRequest);
                    }else {
                        Toast.makeText(getApplicationContext(), "Nothing will be Updated...!", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(UpdateMyPackage.this, MainActivity.class);
                        startActivity(in);
                    }
                }else {
                    Toast.makeText(getApplicationContext(), "Please Select atleast one Test...!", Toast.LENGTH_SHORT).show();
                }
            }
            else {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Enter Your Package Name...!", Toast.LENGTH_SHORT).show();
            }





        }

        if(v == update_cancel){
            db.deleteTableData();
            finish();
        }
    }

    @Override
    public void onRefresh() {
        fillData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
