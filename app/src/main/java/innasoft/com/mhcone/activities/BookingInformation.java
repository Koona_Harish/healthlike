package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class BookingInformation extends AppCompatActivity implements View.OnClickListener {

    TextView booking_information_tittle_txt,labName_txt,packageName_txt,subTestsName_txt,bookingId_txt,whoBooked_txt,forWho_txt
            ,price,booking_information_booking_date,booking_information_booked_date,booking_information_status_successes,payment,payment_type;

    TextView package_Name,subTestsName,booking_Id,whoBooked,forWho
            ,price_txt,booking_information_booking_date_txt,booking_information_booked_date_txt;

    ImageButton bookingInformationCancel;
    String booking_date,booked_date,price_disp;
    ProgressDialog progressDialog;
LinearLayout linear_bookedfor;
    String bookingId;
    Typeface type_lato,type_lato2;
    Button can_resc_button,resc_button;
    String form_type,package_name = "null",lab_name = "null",test_name = "null";

    UserSessionManager userSessionManager;
    String access_key, disp_userName, disp_email;
    Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_information);

        type_lato = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        type_lato2 = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Booking Information");

        progressDialog = new ProgressDialog(BookingInformation.this, R.style.DialogTheme);
        progressDialog.setMessage("Please wait......");
        //progressDialog.setCancelable(false);

        Bundle bundle = getIntent().getExtras();
        bookingId = bundle.getString("BookingID");
        booking_date = bundle.getString("booking_date");
        booked_date = bundle.getString("booked_date");
        price_disp = bundle.getString("price");
        form_type = bundle.getString("form_type");
        typeface = Typeface.createFromAsset(this.getAssets(), "latobold.ttf");

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = userSessionManager.getUserDetails();
        access_key = user.get(UserSessionManager.KEY_ACCSES);
        disp_userName = user.get(UserSessionManager.USER_NAME);

        package_Name = (TextView) findViewById(R.id.package_name);
        payment = (TextView) findViewById(R.id.payment);
        payment_type = (TextView) findViewById(R.id.payment_type);
        package_Name.setTypeface(type_lato2);
        payment.setTypeface(type_lato2);
        payment_type.setTypeface(type_lato);
        payment_type.setText(bundle.getString("payment_type"));
        subTestsName = (TextView) findViewById(R.id.sub_tests);
        subTestsName.setTypeface(type_lato2);

        booking_Id = (TextView) findViewById(R.id.booking_id);
        booking_Id.setTypeface(type_lato2);

        whoBooked = (TextView) findViewById(R.id.who_booked);
        whoBooked.setTypeface(type_lato2);

        linear_bookedfor = (LinearLayout) findViewById(R.id.linear_bookedfor);

        forWho = (TextView) findViewById(R.id.for_who);
        forWho.setTypeface(type_lato2);

        booking_information_booking_date_txt = (TextView) findViewById(R.id.booking_date);
        booking_information_booking_date_txt.setTypeface(type_lato2);

        booking_information_booked_date_txt = (TextView) findViewById(R.id.booked_date);
        booking_information_booked_date_txt.setTypeface(type_lato2);

        price_txt = (TextView) findViewById(R.id.price);
        price_txt.setTypeface(type_lato2);

        booking_information_status_successes = (TextView) findViewById(R.id.booking_information_status_successes);
        booking_information_status_successes.setTypeface(type_lato);

        can_resc_button = (Button) findViewById(R.id.can_resc_button);
        can_resc_button.setTypeface(type_lato2);

        resc_button = (Button) findViewById(R.id.resc_button);
        resc_button.setTypeface(type_lato2);

        booking_information_tittle_txt = (TextView) findViewById(R.id.booking_information_tittle);
        booking_information_tittle_txt.setTypeface(type_lato2);

        labName_txt = (TextView) findViewById(R.id.booking_information_lab_name);
        labName_txt.setTypeface(type_lato2);
        packageName_txt = (TextView) findViewById(R.id.booking_information_package_name);
        packageName_txt.setTypeface(type_lato);
        subTestsName_txt = (TextView) findViewById(R.id.booking_information_sub_tests);
        subTestsName_txt.setTypeface(type_lato);
        bookingId_txt = (TextView) findViewById(R.id.booking_information_booking_id);
        bookingId_txt.setTypeface(type_lato);
        whoBooked_txt = (TextView) findViewById(R.id.booking_information_who_booked);
        whoBooked_txt.setTypeface(type_lato);
        forWho_txt = (TextView) findViewById(R.id.booking_information_for_who);
        forWho_txt.setTypeface(type_lato);
        booking_information_booking_date = (TextView) findViewById(R.id.booking_information_booking_date);
        booking_information_booking_date.setTypeface(type_lato);
        booking_information_booked_date = (TextView) findViewById(R.id.booking_information_booked_date);
        booking_information_booked_date.setTypeface(type_lato);

        price = (TextView) findViewById(R.id.booking_information_price);
        price.setTypeface(type_lato);

        bookingInformationCancel = (ImageButton) findViewById(R.id.booking_information_close);
        bookingInformationCancel.setOnClickListener(this);

        bookingInformation(bookingId);

        resc_button.setOnClickListener(this);
        can_resc_button.setOnClickListener(this);
    }

    private void bookingInformation( final String bookingId) {
        progressDialog.show();

String url=AppUrls.BOOKING_INFORMATION;
        Log.d("LABBBBBBBBBBBB:",url);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.BOOKING_INFORMATION, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                Log.d("LABBBBBBBBBBBB:",response.toString());
                try
                {
                    if(form_type.equals("booklab"))
                    {
                        JSONObject jsonObject =new JSONObject(response);
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        JSONObject jsonObject2 = jsonObject1.getJSONObject("booking-order-list");

                        String status = jsonObject2.getString("status");
                        String cancel_status = jsonObject2.getString("cancel_status");
                        String reschedule_status = jsonObject2.getString("reschedule_status");
                        if(cancel_status.equals("0"))
                        {
                            can_resc_button.setVisibility(View.VISIBLE);
                            can_resc_button.setBackgroundColor(Color.parseColor("#D3D3D3"));
                            can_resc_button.setTextColor(Color.parseColor("#000000"));
                            can_resc_button.setClickable(false);
                        }
                        else {
                            can_resc_button.setVisibility(View.VISIBLE);
                        }
                        if(reschedule_status.equals("0"))
                        {
                            resc_button.setVisibility(View.VISIBLE);
                            resc_button.setBackgroundColor(Color.parseColor("#D3D3D3"));
                            resc_button.setTextColor(Color.parseColor("#000000"));
                            resc_button.setClickable(false);
                        }
                        else {
                            resc_button.setVisibility(View.VISIBLE);
                        }
                        if(status.equals("2"))
                        {
                            booking_information_status_successes.setVisibility(View.VISIBLE);
                        }

                        packageName_txt.setVisibility(View.GONE);
                        subTestsName_txt.setVisibility(View.GONE);
                        package_Name.setVisibility(View.GONE);
                        subTestsName.setVisibility(View.GONE);

                        lab_name = jsonObject2.getString("lab_name");
                        if (lab_name.equals("null") || lab_name.equals("")){
                            labName_txt.setText("");
                        }else {
                            labName_txt.setText(lab_name);
                        }

                        JSONObject jsonObject3 = jsonObject.getJSONObject("user-data");
                        JSONObject jsonObject4 = jsonObject3.getJSONObject("booking-user-list");
                        Log.d("JSON444444:",jsonObject4.toString());


                        jsonObject4.getString("booking_id");
                        jsonObject4.getString("user_id");
                        bookingId_txt.setText(jsonObject4.getString("booking_id"));
                        whoBooked_txt.setText(jsonObject4.getString("user_name"));

                        String renameDispName = jsonObject4.getString("user_sub_name");
                        Log.d("renameDispName:",renameDispName);
                        String finalDispNames = renameDispName.replaceAll("/self/", disp_userName);
                        forWho_txt.setText(finalDispNames);
                        booking_information_booking_date.setText(booking_date);
                        booking_information_booked_date.setText(booked_date);
                        price.setText(price_disp);
                        progressDialog.cancel();
                    }
                    else if(form_type.equals("test") || form_type.equals("package"))
                    {

                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        JSONObject jsonObject2 = jsonObject1.getJSONObject("booking-order-list");
                        jsonObject2.getString("lab_id");
                        jsonObject2.getString("package_id");
                        jsonObject2.getString("test_id");
                        jsonObject2.getString("sub_test_id");
                        String status = jsonObject2.getString("status");
                        String cancel_status = jsonObject2.getString("cancel_status");
                        String reschedule_status = jsonObject2.getString("reschedule_status");
                        Log.d("CANCELSTATUS", cancel_status);
                        if(cancel_status.equals("1"))
                        {
                            can_resc_button.setVisibility(View.VISIBLE);
                           // can_resc_button.setBackgroundColor(Color.parseColor("#D3D3D3"));
                            //can_resc_button.setTextColor(Color.parseColor("#000000"));
                            can_resc_button.setClickable(true);
                        }else {
                            can_resc_button.setVisibility(View.GONE);
                        }
                        if(reschedule_status.equals("0"))
                        {
                            resc_button.setVisibility(View.GONE);
                            resc_button.setBackgroundColor(Color.parseColor("#D3D3D3"));
                            resc_button.setTextColor(Color.parseColor("#000000"));
                            resc_button.setClickable(false);
                        }else
                        {
                            resc_button.setVisibility(View.VISIBLE);
                        }
                        if (status.equals("2")) {
                            booking_information_status_successes.setVisibility(View.VISIBLE);
                        }
                        if (!jsonObject2.getString("package_name").equals("null")) {
                            package_name = jsonObject2.getString("package_name");
                            packageName_txt.setText(" "+package_name);
                        } else {
                            package_Name.setVisibility(View.GONE);
                            packageName_txt.setVisibility(View.GONE);
                        }
                        if (!jsonObject2.getString("sub_test").equals("null")) {
                            test_name = jsonObject2.getString("sub_test");
                            subTestsName_txt.setText(test_name );
                        } else {
                            subTestsName_txt.setVisibility(View.GONE);
                        }
                        lab_name = jsonObject2.getString("lab_name");
                        labName_txt.setText(lab_name);

                        JSONObject jsonObject3 = jsonObject.getJSONObject("user-data");
                        JSONObject jsonObject4 = jsonObject3.getJSONObject("booking-user-list");
                        jsonObject4.getString("booking_id");
                        jsonObject4.getString("user_id");
                        bookingId_txt.setText(jsonObject4.getString("booking_id"));
                        whoBooked_txt.setText( jsonObject4.getString("user_name"));
                        String renameDispName = jsonObject4.getString("user_sub_name");
                        String finalDispNames = renameDispName.replaceAll("/self/", disp_userName);
                        forWho_txt.setText( finalDispNames);
                        booking_information_booking_date.setText( booking_date);
                        booking_information_booked_date.setText(booked_date);
                        price.setText( price_disp);
                        progressDialog.cancel();
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
                progressDialog.cancel();
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.cancel();

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String,String> params = new HashMap<String, String>();

                params.put("booking_id",bookingId);
                if(form_type.equals("booklab"))
                {
                    params.put("form_type", form_type);
                }
                Log.d("PARAMAMM",params.toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {
       if(v == bookingInformationCancel) {
           finish();
       }
        if(v ==can_resc_button) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Cancel Appointment");
            builder.setMessage("Are you sure?");
            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    String value = can_resc_button.getText().toString();
                    progressDialog.show();

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.CANCEL_ORDER, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                               String status_code =  jsonObject.getString("status");
                                Log.d("CANCELORDER ", "REPONCE "+response );
                                if (status_code.equals("20100")){

                                    String payment_Type = jsonObject.getString("payment_type");

                                    if (payment_Type.equals("payment"))
                                    {
                                        Intent intent = new Intent(getApplicationContext(), OrderCancel.class);
                                        startActivity(intent);
                                    }
                                    if (payment_Type.equals("cal") || payment_Type.equals("appointment")){

                                        /*LayoutInflater inflater = getLayoutInflater();
                                        View toastLayout = inflater.inflate(R.layout.custom_toast_package, (ViewGroup) findViewById(R.id.custom_toast_layout));
                                        TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
                                        textView.setTypeface(typeface);
                                        textView.setText("Successfully Canceled....!");
                                        Toast toast = new Toast(getApplicationContext());
                                        toast.setGravity(Gravity.CENTER, 0, 0);
                                        toast.setDuration(Toast.LENGTH_SHORT);
                                        toast.setView(toastLayout);
                                        toast.show();
                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        startActivity(intent);
                                        toast.cancel();*/
                                        Intent intent = new Intent(BookingInformation.this, MainActivity.class);
                                        startActivity(intent);
                                        progressDialog.cancel();

                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("booking_id", bookingId);
                            Log.d("CANCELORDER ", "PARAMETER "+params.toString() );

                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );
                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);
                }
            });

            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
        }

        if (v == resc_button) {
            Intent intent = new Intent(this, ReScheduleForm.class);
            intent.putExtra("bookingId", bookingId);
            intent.putExtra("lab_name", lab_name);
            intent.putExtra("package_name", package_name);
            intent.putExtra("test_name", test_name);
            intent.putExtra("schedule_date_time", booking_date);
            Log.d("INTENTDETAILS",bookingId+"\n"+lab_name+"\n"+package_name+"\n"+test_name+"\n"+booking_date);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
