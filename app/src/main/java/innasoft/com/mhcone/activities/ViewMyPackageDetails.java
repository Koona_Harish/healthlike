package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.UserSubPackageAdapter;
import innasoft.com.mhcone.holders.UserSubPackageHolder;
import innasoft.com.mhcone.models.UserPackageNamesModel;
import innasoft.com.mhcone.models.UserSubPackageModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.GPSTrackers;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class ViewMyPackageDetails extends AppCompatActivity implements UserSubPackageHolder.MyViewHolder, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    ImageButton closeButton;
    UserPackageNamesModel userpackageNameModel;
    TextView display_package_name,defaultText;
    Typeface typeface;
    ProgressDialog pDialog;
    ArrayList<UserSubPackageModel> userSubPackageModel;
    UserSubPackageAdapter adapter;
    RecyclerView userSubPackagerecyclerView;
    String package_id, package_name;
    Button findLabs, updateUrPackage,btdeleteMyPack;
    GPSTrackers gps;

    Typeface type_lato,typebold;
    UserSessionManager userSessionManager;
    String user_id;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_my_package_details);

        type_lato = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typebold = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        updateUrPackage = (Button) findViewById(R.id.btEditPackage);
        updateUrPackage.setTypeface(typebold);
        btdeleteMyPack = (Button) findViewById(R.id.btdeleteMyPack);
        btdeleteMyPack.setTypeface(typebold);

        userSessionManager = new UserSessionManager(this);

        HashMap<String, String> user = userSessionManager.getUserDetails();

        user_id = user.get(UserSessionManager.USER_ID);
        Bundle bundle = getIntent().getExtras();
        userpackageNameModel = (UserPackageNamesModel) bundle.getSerializable("package_id");

        defaultText = (TextView) findViewById(R.id.defaultText);
        defaultText.setTypeface(type_lato);

        display_package_name = (TextView) findViewById(R.id.txt_user_package_name);
        display_package_name.setTypeface(typebold);
        closeButton = (ImageButton) findViewById(R.id.sub_pakg_btn_close);

        gps = new GPSTrackers(this);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setProgressStyle(R.style.DialogTheme);
        pDialog.setCancelable(false);




        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.view_package_details_swipe_layout);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,

                android.R.color.holo_green_light,

                android.R.color.holo_orange_light,

                android.R.color.holo_red_light);

        mSwipeRefreshLayout.setOnRefreshListener(this);

        display_package_name.setText(userpackageNameModel.getUser_package_name());

        package_id = userpackageNameModel.getUser_package_id();
        package_name = userpackageNameModel.getUser_package_name();


        findLabs = (Button) findViewById(R.id.btShowLabs);
        findLabs.setTypeface(typebold);

        userSubPackageModel = new ArrayList<UserSubPackageModel>();
        userSubPackagerecyclerView = (RecyclerView) findViewById(R.id.user_sub_package_recycler_view);
        userSubPackagerecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        userSubPackagerecyclerView.setLayoutManager(layoutManager);
        adapter = new UserSubPackageAdapter(userSubPackageModel, ViewMyPackageDetails.this, R.layout.row_user_sub_packages_items);
        gettingSubPackagesList(package_id);


        closeButton = (ImageButton) findViewById(R.id.sub_detail_pakg_btn_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });
        findLabs.setOnClickListener(this);
        updateUrPackage.setOnClickListener(this);
        btdeleteMyPack.setOnClickListener(this);



    }

    private void gettingSubPackagesList(final String package_id) {
        defaultText.setVisibility(View.INVISIBLE);
        ViewMyPackageDetails.this.runOnUiThread(new Runnable() {
            public void run() {
                pDialog.show();

            }
        });
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_USER_SUB_PACKAGE_DETAILS,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        userSubPackageModel.clear();

                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            String responceCode = jsonObject.getString("status");

                            if(responceCode.equals("19999")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("my_package_test");
                                    UserSubPackageModel uspm = new UserSubPackageModel();
                                    uspm.setUser_sub_package_test_id(jsonObject1.getString("id"));

                                    uspm.setUser_package_id(jsonObject1.getString("user_package_id"));
                                    uspm.setTest_id(jsonObject1.getString("test_id"));
                                    uspm.setSub_test_id(jsonObject1.getString("sub_test_id"));
                                    uspm.setStatus(jsonObject1.getString("status"));
                                    uspm.setCreated_time(jsonObject1.getString("created_time"));
                                    uspm.setPackage_name(jsonObject1.getString("package_name"));
                                    uspm.setSub_test_name(jsonObject1.getString("sub_test_name"));

                                    userSubPackageModel.add(uspm);
                                }
                                userSubPackagerecyclerView.setAdapter(adapter);
                                ViewMyPackageDetails.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        pDialog.dismiss();

                                    }
                                });
                                mSwipeRefreshLayout.setRefreshing(false);
                            }
                            if(responceCode.equals("18888")) {
                                ViewMyPackageDetails.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        pDialog.dismiss();

                                    }
                                });
                                mSwipeRefreshLayout.setRefreshing(false);
                                defaultText.setVisibility(View.VISIBLE);
                            }
                            if(responceCode.equals("10140"))
                            {

                                userSessionManager.logoutUser();
                                Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }

                            if(responceCode.equals("10150"))
                            {

                                userSessionManager.logoutUser();
                                Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {


                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_package_id", package_id);
                params.put("user_id", user_id);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void cardViewListener(int position) {

    }

    @Override
    public void onClick(View v) {
        if(v == findLabs)
        {
            List<String> subtestName = new ArrayList<String>();
            List<String> subtestIds = new ArrayList<String>();
            List<String> stringFormateforIDs = new ArrayList<String>();
            for (int i = 0; i < userSubPackageModel.size(); i++)
            {
                subtestIds.add(userSubPackageModel.get(i).getSub_test_id());
                subtestName.add(userSubPackageModel.get(i).getSub_test_name());
                stringFormateforIDs.add("0-"+userSubPackageModel.get(i).getTest_id()+"-"+userSubPackageModel.get(i).getSub_test_id()+"-0");
            }
            if (gps.canGetLocation())
            {

                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();

                Intent intent = new Intent(this, ShowUserTestLabs.class);
                intent.putExtra("latValue", Double.toString(latitude));
                intent.putExtra("longValue", Double.toString(longitude));

                intent.putExtra("packageName", package_name);
                intent.putExtra("subtestIdswithformate", subtestIds.toString().replaceAll("\\[", "").replaceAll("\\]","").replaceAll(" ", ""));
                intent.putExtra("subtestNameswithfromate", subtestName.toString().replaceAll("\\[", "").replaceAll("\\]","").replaceAll(" ", ""));
                intent.putExtra("forbillingPageIDs", stringFormateforIDs.toString().replaceAll("\\[", "").replaceAll("\\]","").replaceAll(" ", ""));
                //   intent.putExtra("subTestId", SendSubTestIDs);
                //   intent.putExtra("subTestName", sendSubtestNames);
                startActivity(intent);


            }
            else {
                Toast.makeText(this, "Please Check GPS connection ....!", Toast.LENGTH_SHORT).show();
            }
        }
        if(v == updateUrPackage)
        {
            Intent intent = new Intent(this, UpdateMyPackage.class);
            intent.putExtra("packageID", package_id);
            intent.putExtra("packageName", package_name);
            startActivity(intent);
        }

        if(v == btdeleteMyPack)
        {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Do you really want to Delete This Package")
                    .setTitle("Alert..");


            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    deleteSelectetedPackage(package_id);
                }
            });


            builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {


                    dialogInterface.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private void deleteSelectetedPackage(final String package_id) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.DELETE_MYPACKAGE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String responceCode = jsonObject.getString("status");
                            if (responceCode.equals("20100")) {

                                Intent intent = new Intent(ViewMyPackageDetails.this, ViewMyPackageNames.class);

                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                            if(responceCode.equals("10140"))
                            {

                                userSessionManager.logoutUser();
                                Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }

                            if(responceCode.equals("10150"))
                            {

                                userSessionManager.logoutUser();
                                Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }
                        }catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {


                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }

                    }
                }

        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("package_id", package_id);
                params.put("user_id", user_id);
                return params;
            }
        };



        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }



    @Override
    public void onRefresh() {
        gettingSubPackagesList(package_id);
    }
}
