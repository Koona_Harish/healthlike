package innasoft.com.mhcone.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.DBHandler;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class DummyTestBillingActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    TextView billing_tittle, billing_tittle_txt, billing_lab_name, billing_lab_name_txt, billing_total, billing_total_txt, tv_tele, tv_pal, tv_pay;

    Button btnSelectICareFor, btn_billing_cal, btn_billing_pay, btn_tele_booking, proceed;
    ImageView btnDatePicker, btnTimePicker;
    ImageButton closeWindow;
    String phoneNumber;
    TextView txtDate, txtTime, txtHeader;
    int mYear, mMonth, mDay, mHour, mMinute;
    String sendDate, sendTime;
    int userCount = 0;
    ProgressDialog progressDialog;
    DBHandler dbHandler;
    int rmdyear, rmdmonth, rmdday, rmdhour, rmdmint;
    String status_payment_lab, status_payment_online, status_tele_booking;
    Typeface typeface, typebold;
    CheckBox homecollection_chk;
    String access_key, disp_userName, disp_email;
    UserSessionManager userSessionManager;
    String userID, userMailId, userMobile, userName;
    NetworkStatus ns;
    Boolean isOnline = false;
    RelativeLayout row;

    ArrayList<Object> arraylistsubtestNamesObject;
    ArrayList<Object> arraylistsubtestIdsObject;
    ArrayList<Object> arraylisttestIdsObject;
    double dtotal, dgrandtotal;
    public static final String TAG = "Payment Status";

    String labId, labName, subTestName, packageName, labPrice, home_collection_charges = null, lab_home_collections = null;

    ArrayList<String> selectedItems = new ArrayList<>();
    Map<String, String> selectedItemmap = new HashMap();
    ArrayList<String> usersListId = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dummy_test_billing);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Payment");

        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typebold = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        txtHeader = (TextView) findViewById(R.id.txt_package_name);
        txtHeader.setTypeface(typebold);

        ns = new NetworkStatus();
        isOnline = NetworkStatus.isOnline(DummyTestBillingActivity.this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            labId = bundle.getString("selectedLabId");
            labName = bundle.getString("selectedLabName");
            subTestName = bundle.getString("subTestName");
            phoneNumber = bundle.getString("selecteLabPhone");
            status_payment_lab = bundle.getString("payment_lab");
            status_payment_online = bundle.getString("payment_online");
            status_tele_booking = bundle.getString("tele_booking");
            packageName = bundle.getString("packageName");
            labPrice = bundle.getString("selectedLabPrice");
        }

        dbHandler = new DBHandler(this);

        double labPriceDouble = Double.valueOf(labPrice);

        dtotal = Double.valueOf(new DecimalFormat("##.###").format(labPriceDouble));

        progressDialog = new ProgressDialog(DummyTestBillingActivity.this, R.style.DialogTheme);
        progressDialog.setMessage("Please Wait......");
        progressDialog.setCancelable(false);

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("bundelData");

        arraylistsubtestNamesObject = (ArrayList<Object>) args.getSerializable("arraylistsubtestNames");
        arraylistsubtestIdsObject = (ArrayList<Object>) args.getSerializable("arraylistsubtestIds");
        arraylisttestIdsObject = (ArrayList<Object>) args.getSerializable("arraylisttestIds");
        Log.d("dkbcvb", arraylistsubtestNamesObject.toString() + "-----" + arraylistsubtestIdsObject.toString() + "-----" + arraylisttestIdsObject.toString());
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = userSessionManager.getUserDetails();
        access_key = user.get(UserSessionManager.KEY_ACCSES);

        disp_userName = user.get(UserSessionManager.USER_NAME);
        disp_email = user.get(UserSessionManager.USER_EMAIL);
        userID = user.get(UserSessionManager.USER_ID);
        userMailId = user.get(UserSessionManager.USER_EMAIL);
        userName = user.get(UserSessionManager.USER_NAME);
        userMobile = user.get(UserSessionManager.USER_MOBILE);

        homecollection_chk = (CheckBox) findViewById(R.id.homecollection_chk);
        homecollection_chk.setTypeface(typeface);

        tv_tele = (TextView) findViewById(R.id.tv_tele);
        tv_tele.setTypeface(typeface);
        tv_pal = (TextView) findViewById(R.id.tv_pal);
        tv_pal.setTypeface(typeface);
        tv_pay = (TextView) findViewById(R.id.tv_pay);
        tv_pay.setTypeface(typeface);

        btn_billing_cal = (Button) findViewById(R.id.btn_billing_cal);
        btn_billing_cal.setTypeface(typeface);
        btn_billing_pay = (Button) findViewById(R.id.btn_billing_pay);
        btn_billing_pay.setTypeface(typeface);
        btn_tele_booking = (Button) findViewById(R.id.btn_tele_booking);
        btn_tele_booking.setTypeface(typeface);

        billing_tittle = (TextView) findViewById(R.id.billingtittle);
        billing_tittle.setTypeface(typebold);
        billing_lab_name = (TextView) findViewById(R.id.billinglab_name);
        billing_lab_name.setTypeface(typebold);
       /* billing_price = (TextView) findViewById(R.id.billinglab_price);
        billing_price.setTypeface(typeface);*/
        billing_total = (TextView) findViewById(R.id.billinglab_price_grandTotla);
        billing_total.setTypeface(typebold);
        billing_tittle_txt = (TextView) findViewById(R.id.billing_tittle);
        billing_tittle_txt.setTypeface(typeface);
        billing_tittle_txt.setText(subTestName);
        billing_lab_name_txt = (TextView) findViewById(R.id.billing_lab_name);
        billing_lab_name_txt.setTypeface(typeface);
        billing_lab_name_txt.setText(labName);
       /* billing_price_txt = (TextView) findViewById(R.id.billing_lab_price);
        billing_price_txt.setTypeface(typeface);
        billing_price_txt.setText("Rs. "+dtotal);*/
        billing_total_txt = (TextView) findViewById(R.id.billing_lab_price_grandTotla);
        billing_total_txt.setTypeface(typeface);

        btnDatePicker = (ImageView) findViewById(R.id.btn_date);
        btnTimePicker = (ImageView) findViewById(R.id.btn_time);
        btnSelectICareFor = (Button) findViewById(R.id.btn_select_icarefor);
        btnSelectICareFor.setTypeface(typeface);
        txtDate = (TextView) findViewById(R.id.in_date);
        txtDate.setTypeface(typeface);
        txtTime = (TextView) findViewById(R.id.in_time);
        txtTime.setTypeface(typeface);
        closeWindow = (ImageButton) findViewById(R.id.billing_close);

        closeWindow.setOnClickListener(this);
        btnDatePicker.setOnClickListener(this);
        btnTimePicker.setOnClickListener(this);
        btnSelectICareFor.setOnClickListener(this);
       /* btn_billing_pay.setOnClickListener(this);
        btn_billing_cal.setOnClickListener(this);
        btn_tele_booking.setOnClickListener(this);*/

        row = (RelativeLayout) findViewById(R.id.row);

        if (status_payment_lab.equals("0")) {
            btn_billing_cal.setVisibility(View.INVISIBLE);
            tv_pal.setVisibility(View.INVISIBLE);
        } else {
            btn_billing_cal.setVisibility(View.VISIBLE);
            tv_pal.setVisibility(View.VISIBLE);
        }

        if (status_tele_booking.equals("0")) {
            btn_tele_booking.setVisibility(View.INVISIBLE);
            tv_tele.setVisibility(View.INVISIBLE);
        } else {
            btn_tele_booking.setVisibility(View.VISIBLE);
            tv_tele.setVisibility(View.VISIBLE);
        }

        if (status_payment_online.equals("0")) {
            btn_billing_pay.setVisibility(View.INVISIBLE);
            tv_pay.setVisibility(View.INVISIBLE);
        } else {
            btn_billing_pay.setVisibility(View.VISIBLE);
            tv_pay.setVisibility(View.VISIBLE);
        }

        homeCollectionStatus();

        homecollection_chk.setOnCheckedChangeListener(this);

        proceed = (Button) findViewById(R.id.proceed);
        proceed.setTypeface(typeface);
        proceed.setOnClickListener(this);
    }

    private void homeCollectionStatus() {
        final ArrayList<String> subTestIds = new ArrayList<>();

        for (int i = 0; i < arraylistsubtestIdsObject.size(); i++) {
            subTestIds.add(arraylistsubtestIdsObject.get(i) + "");
        }
//        Log.d("TESTIDSANDLABIDS", subTestIds.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", "") + "\n" + labId);
        StringRequest stringRequest2 = new StringRequest(Request.Method.POST, AppUrls.HOME_COLLECTION_STATUS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equals("19990")) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        JSONObject jsonObject2 = jsonObject1.getJSONObject("check-home-collection");
                        lab_home_collections = jsonObject2.getString("lab_home_collections");
                        home_collection_charges = jsonObject2.getString("home_collection_price");
                        homecollection_chk.setText("Home collection Rs." + home_collection_charges);
                    }
                    if (responceCode.equals("18880")) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        JSONObject jsonObject2 = jsonObject1.getJSONObject("check-home-collection");
                        lab_home_collections = jsonObject2.getString("lab_home_collections");
                        home_collection_charges = jsonObject2.getString("home_collection_price");
                        homecollection_chk.setText("Home Collection Not available");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {


                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("test_id", subTestIds.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", ""));
                params.put("lab_id", labId);
                return params;
            }
        };
        stringRequest2.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue2 = Volley.newRequestQueue(DummyTestBillingActivity.this);
        requestQueue2.add(stringRequest2);
    }

    @Override
    public void onClick(View v) {

        if (v == btn_billing_cal) {
            if (home_collection_charges.equals("null") || home_collection_charges == null) {
                dgrandtotal = dtotal * userCount;
            } else {
                if (homecollection_chk.isChecked()) {
                    dgrandtotal = dtotal * userCount + Double.valueOf(home_collection_charges);
                } else {
                    dgrandtotal = dtotal * userCount;
                }
            }

            final ArrayList<String> subttestIdswithTestIds = new ArrayList<>();

            for (int i = 0; i < arraylistsubtestIdsObject.size(); i++) {
                subttestIdswithTestIds.add("0-" + arraylisttestIdsObject.get(i) + "-" + arraylistsubtestIdsObject.get(i) + "-0");
            }

            if (sendDate != null && sendTime != null && userCount != 0) {

                progressDialog.show();
                btn_billing_cal.setClickable(false);

                StringRequest stringRequest2 = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String responceCode = jsonObject.getString("status");
                            if (responceCode.equals("19999")) {
                                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BILLINGURL,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {

                                                try {
                                                    dbHandler.deleteTableData();
                                                    JSONObject jsonObject = new JSONObject(response);
                                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                                    String paymentType = jsonObject1.getString("payment-type");
                                                    String bookingId = jsonObject1.getString("booking-id");
                                                    String message = jsonObject1.getString("msg");
                                                    progressDialog.dismiss();

                                                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                                    Intent intent = new Intent(getApplicationContext(), BookingSuccessPage.class);

                                                    intent.putExtra("rmdyear", rmdyear + "");
                                                    intent.putExtra("rmdmonth", rmdmonth + "");
                                                    intent.putExtra("rmdday", rmdday + "");
                                                    intent.putExtra("rmdhour", rmdhour + "");
                                                    intent.putExtra("rmdmint", rmdmint + "");
                                                    intent.putExtra("descr", "You are book " + labName + " Appointment");

                                                    intent.putExtra("bookingId", bookingId);
                                                    startActivity(intent);

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {

                                                progressDialog.dismiss();

                                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                                } else if (error instanceof AuthFailureError) {

                                                } else if (error instanceof ServerError) {

                                                } else if (error instanceof NetworkError) {

                                                } else if (error instanceof ParseError) {

                                                }
                                            }
                                        }
                                ) {
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("user_id", userID);
                                        params.put("lab_id", labId);
                                        params.put("payment_type", "cal");
                                        params.put("booking_date", sendDate + " " + sendTime);
                                        params.put("user_count", userCount + "");
                                        params.put("price", dtotal + "");
                                        params.put("grand_total", dgrandtotal + "");
                                        params.put("form_type", "test");
                                        params.put("transaction_id", "0");
                                        Log.d("BOOKINGINFO", "TESTSBOOKING " + subttestIdswithTestIds.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", ""));
                                        params.put("package_id", subttestIdswithTestIds.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", ""));
                                        params.put("sub_user_id", usersListId.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(",", "-").replaceAll(" ", ""));
                                        params.put("type", "testlab");
                                        return params;
                                    }
                                };
                                stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                RequestQueue requestQueue = Volley.newRequestQueue(DummyTestBillingActivity.this);
                                requestQueue.add(stringRequest);
                            }
                            if (responceCode.equals("10140")) {
                                userSessionManager.logoutUser();
                                Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }

                            if (responceCode.equals("10150")) {
                                userSessionManager.logoutUser();
                                Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {


                        } else if (error instanceof ParseError) {

                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("user_id", userID);
                        return params;
                    }
                };
                stringRequest2.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue2 = Volley.newRequestQueue(DummyTestBillingActivity.this);
                requestQueue2.add(stringRequest2);

            } else {
                Toast.makeText(getApplicationContext(), "Please provide all data...!", Toast.LENGTH_SHORT).show();
            }
        }
        if (v == btn_billing_pay) {

            if (home_collection_charges.equals("null") || home_collection_charges == null) {
                dgrandtotal = dtotal * userCount;
            } else {
                if (homecollection_chk.isChecked()) {
                    dgrandtotal = dtotal * userCount + Double.valueOf(home_collection_charges);
                } else {
                    dgrandtotal = dtotal * userCount;
                }

            }
            final ArrayList<String> subttestIdswithTestIds = new ArrayList<>();

            for (int i = 0; i < arraylistsubtestIdsObject.size(); i++) {
                subttestIdswithTestIds.add("0-" + arraylisttestIdsObject.get(i) + "-" + arraylistsubtestIdsObject.get(i) + "-0");
            }

            if (sendDate != null && sendTime != null && userCount != 0) {
                if (dgrandtotal != 0.0) {

                    btn_billing_pay.setClickable(false);
                    StringRequest stringRequest2 = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("19999")) {

                                    String getFname = userName;
                                    String getPhone = userMobile;
                                    String getEmail = userMailId;
                                    String getAmt = dgrandtotal + "";

                                    Intent intent = new Intent(getApplicationContext(), PayMentGateWay.class);
                                    intent.putExtra("FIRST_NAME", getFname);
                                    intent.putExtra("PHONE_NUMBER", getPhone);
                                    intent.putExtra("EMAIL_ADDRESS", getEmail);
                                    intent.putExtra("RECHARGE_AMT", getAmt);
                                    intent.putExtra("rmdyear", rmdyear + "");
                                    intent.putExtra("rmdmonth", rmdmonth + "");
                                    intent.putExtra("rmdday", rmdday + "");
                                    intent.putExtra("rmdhour", rmdhour + "");
                                    intent.putExtra("rmdmint", rmdmint + "");
                                    intent.putExtra("descr", labName);
                                    intent.putExtra("user_id", userID);
                                    intent.putExtra("lab_id", labId);
                                    intent.putExtra("payment_type", "payment");
                                    intent.putExtra("booking_date", sendDate + " " + sendTime);
                                    intent.putExtra("user_count", userCount + "");
                                    intent.putExtra("price", dtotal + "");
                                    intent.putExtra("grand_total", dgrandtotal + "");
                                    intent.putExtra("form_type", "test");
                                    intent.putExtra("package_id", subttestIdswithTestIds.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", ""));
                                    intent.putExtra("sub_user_id", usersListId.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(",", "-").replaceAll(" ", ""));
                                    intent.putExtra("type", "testlab");
                                    startActivity(intent);
                                }
                                if (responceCode.equals("10140")) {
                                    userSessionManager.logoutUser();
                                    Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);
                                }

                                if (responceCode.equals("10150")) {
                                    userSessionManager.logoutUser();
                                    Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("user_id", userID);
                            return params;
                        }
                    };
                    stringRequest2.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue2 = Volley.newRequestQueue(DummyTestBillingActivity.this);
                    requestQueue2.add(stringRequest2);

                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Your are not allowed to pay Rs.0/- ", Toast.LENGTH_SHORT).show();
                }

            } else {
                Toast.makeText(getApplicationContext(), "Please provide all data...!", Toast.LENGTH_SHORT).show();
            }
        }

        if (v == btnDatePicker) {

            final java.util.Calendar c = java.util.Calendar.getInstance();
            mYear = c.get(java.util.Calendar.YEAR);
            mMonth = c.get(java.util.Calendar.MONTH);
            mDay = c.get(java.util.Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            rmdyear = year;
                            rmdmonth = monthOfYear + 1;
                            rmdday = dayOfMonth;
                            sendDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                            txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        }

                    }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();
        }
        if (v == btnTimePicker) {

            final java.util.Calendar c = java.util.Calendar.getInstance();
            mHour = c.get(java.util.Calendar.HOUR_OF_DAY);
            mMinute = c.get(java.util.Calendar.MINUTE);

            final TimePickerDialog timePickerDialog = new TimePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            rmdhour = Integer.valueOf(String.format("%02d", hourOfDay));
                            rmdmint = Integer.valueOf(String.format("%02d", minute));
                            String hourchange = String.format("%02d", hourOfDay);
                            String minitChange = String.format("%02d", minute);
                            String disptime = updateTime(hourOfDay, minute);
                            sendTime = disptime;
                            txtTime.setText(disptime);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialogInterface) {
                    timePickerDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setVisibility(View.GONE);
                }
            });

            timePickerDialog.show();
        }

        if (v == closeWindow) {

            finish();
        }
        if (v == btn_tele_booking) {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + Uri.encode(phoneNumber.trim())));
            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(callIntent);
        }

        if (v == btnSelectICareFor) {

            DummyTestBillingActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    progressDialog.show();

                }
            });

            StringRequest stringRequest2 = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String responceCode = jsonObject.getString("status");
                        if (responceCode.equals("19999")) {
                            userCount = 0;
                            usersListId.clear();

                            final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.VIEW_SUB_USERS_URL,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {

                                            try {
                                                selectedItems.clear();
                                                JSONObject jsonObject = new JSONObject(response);
                                                Log.d("RESPONCEEEE", response);
                                                String successCode = jsonObject.getString("status");
                                                if (successCode.equals("10190")) {
                                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                                    for (int i = 0; i < jsonArray.length(); i++) {
                                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("sub-users");
                                                        jsonObject1.getString("id");
                                                        jsonObject1.getString("user_id");
                                                        jsonObject1.getString("user_sub_name");
                                                        jsonObject1.getString("relation");
                                                        jsonObject1.getString("age");
                                                        jsonObject1.getString("gender");
                                                        jsonObject1.getString("email");
                                                        jsonObject1.getString("mobile");
                                                        jsonObject1.getString("status");
                                                        jsonObject1.getString("created_time");
                                                        selectedItemmap.put(jsonObject1.getString("user_sub_name"), jsonObject1.getString("id"));
                                                        selectedItems.add(jsonObject1.getString("user_sub_name"));
                                                    }
                                                }
                                                selectedItems.add("Self");
                                                selectedItemmap.put("Self", "0");
                                                progressDialog.dismiss();

                                                alertUsersList(selectedItems, selectedItemmap);

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {

                                            progressDialog.dismiss();
//                                            String responseBody = null;

                                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                            } else if (error instanceof AuthFailureError) {

                                            } else if (error instanceof ServerError) {

                                            } else if (error instanceof NetworkError) {

                                            } else if (error instanceof ParseError) {

                                            }
                                        }
                                    }
                            ) {
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    Map<String, String> params = new HashMap<>();
                                    params.put("user_id", userID);
//                                    Log.d("GETTTTTTTTTT", userID);
                                    return params;
                                }
                            };
                            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            RequestQueue requestQueue = Volley.newRequestQueue(DummyTestBillingActivity.this);
                            requestQueue.add(stringRequest);
                        }
                        if (responceCode.equals("10140")) {
                            userSessionManager.logoutUser();
                            Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        }

                        if (responceCode.equals("10150")) {
                            userSessionManager.logoutUser();
                            Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", userID);
                    return params;
                }
            };
            stringRequest2.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue2 = Volley.newRequestQueue(DummyTestBillingActivity.this);
            requestQueue2.add(stringRequest2);
        }

        if (v == proceed) {
            if (isOnline) {

                if (home_collection_charges.equals("null") || home_collection_charges == null) {
                    dgrandtotal = dtotal * userCount;
                } else {
                    if (homecollection_chk.isChecked()) {
                        dgrandtotal = dtotal * userCount + Double.valueOf(home_collection_charges);
                    } else {
                        dgrandtotal = dtotal * userCount;
                    }
                }

                if (sendDate != null && sendTime != null && userCount != 0) {

                    if (dgrandtotal != 0.0) {

                        String getFname = userName;
                        String getPhone = userMobile;
                        String getEmail = userMailId;
                        String getAmt = dgrandtotal + "";
                        String sub_user_id = usersListId.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(",", "-").replaceAll(" ", "");
                        String booking_date = sendDate + " " + sendTime;
                        dgrandtotal = Double.parseDouble(billing_total_txt.getText().toString().replace("Rs. ", ""));

                        final ArrayList<String> subttestIdswithTestIds = new ArrayList<String>();

                        for (int i = 0; i < arraylistsubtestIdsObject.size(); i++) {
                            subttestIdswithTestIds.add("0-" + arraylisttestIdsObject.get(i) + "-" + arraylistsubtestIdsObject.get(i) + "-0");
                        }

                        Intent intent = new Intent(DummyTestBillingActivity.this, TestConfirmationActivity.class);
                        intent.putExtra("FIRST_NAME", getFname);
                        intent.putExtra("PHONE_NUMBER", getPhone);
                        intent.putExtra("phoneNumber", phoneNumber);
                        intent.putExtra("EMAIL_ADDRESS", getEmail);
                        intent.putExtra("RECHARGE_AMT", getAmt);
                        intent.putExtra("rmdyear", rmdyear + "");
                        intent.putExtra("rmdmonth", rmdmonth + "");
                        intent.putExtra("rmdday", rmdday + "");
                        intent.putExtra("rmdhour", rmdhour + "");
                        intent.putExtra("rmdmint", rmdmint + "");
                        intent.putExtra("descr", labName);
                        intent.putExtra("user_id", userID);
                        intent.putExtra("lab_id", labId);
                        intent.putExtra("payment_type", "payment");
                        intent.putExtra("booking_date", booking_date);
                        intent.putExtra("user_count", userCount + "");
                        intent.putExtra("price", dtotal + "");
                        intent.putExtra("grand_total", dgrandtotal + "");
                        intent.putExtra("form_type", "test");
                        intent.putExtra("sub_user_id", sub_user_id);
                        intent.putExtra("type", "testlab");
                        intent.putExtra("labName", labName);
                        intent.putExtra("sendpackTestandSubIDswithformat", subttestIdswithTestIds.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", ""));

                        intent.putExtra("status_payment_lab", status_payment_lab);
                        intent.putExtra("status_payment_online", status_payment_online);
                        intent.putExtra("status_tele_booking", status_tele_booking);

//                        Log.d("BUNDLE", getFname + "\n" + getPhone + "\n" + getEmail + "\n" + getAmt + "\n" + rmdyear + "\n" + rmdmonth + "\n" + rmdday + "\n" + rmdhour + "\n" +
//                                rmdmint + "\n" + labName + "\n" + userID + "\n" + labId + "\n" + "payment" + "\n" + booking_date + "\n" + userCount + "\n" + dtotal + "\n" + dgrandtotal + "\n" +
//                                "test" + "\n" + sub_user_id + "\n" + "testlab" + "\n" + status_payment_lab + "\n" + status_payment_online + "\n" + status_tele_booking);

                        startActivity(intent);
                    } else {
                        Toast.makeText(this, "Please Provide All Details..!", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Please provide all data...!", Toast.LENGTH_SHORT).show();

                }
            } else {
                showInternetStatus();
            }
        }
    }

    private void showInternetStatus() {

        LayoutInflater inflater = getLayoutInflater();
        View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
        TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
        textView.setTypeface(typeface);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(toastLayout);
        toast.show();
    }

    private boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private double getAmount() {

        Double amount = 10.0;

        if (isDouble(dgrandtotal + "")) {
            amount = Double.parseDouble(dgrandtotal + "");
            return amount;
        } else {
            Toast.makeText(getApplicationContext(), "Paying Default Amount ₹10", Toast.LENGTH_SHORT).show();
            return amount;
        }
    }

    private void showDialogMessage(String message) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(TAG);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void alertUsersList(final ArrayList<String> dataArrayList, final Map<String, String> keyvaluePair) {
        if (dataArrayList.size() != 0) {
            final String[] items = dataArrayList.toArray(new String[dataArrayList.size()]);

            final ArrayList<String> mselectedItems = new ArrayList<>();

            if (!mselectedItems.isEmpty()) {

                mselectedItems.clear();
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(DummyTestBillingActivity.this);

            userCount = 0;
            builder.setTitle("Choose Your I Care For ")
                    .setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which, boolean isChecked) {

                            if (isChecked) {
                                String selectedItems = items[which];
                                mselectedItems.add(selectedItems);
                                String value = (String) keyvaluePair.get(selectedItems);
                                usersListId.add(value);
                                userCount++;
                            } else {
                                userCount--;
                                mselectedItems.remove(items[which]);
                                String value = (String) keyvaluePair.get(items[which]);
                                usersListId.remove(value);
                            }

                        }
                    })
                    .setPositiveButton(" DONE ", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            List<String> list = new ArrayList<String>();
                            for (String items : mselectedItems) {
                                list.add(items);
                            }

                            if (list.size() != 0) {
                                btnSelectICareFor.setText(list.toString().replaceAll("\\[", "").replaceAll("\\]", ""));

                                if (home_collection_charges.equals("null") || home_collection_charges.equals(null) || home_collection_charges.equals("0") || home_collection_charges.equals("")) {
                                    row.setVisibility(View.VISIBLE);
                                    String abc = ("Rs. " + dtotal * userCount + "").replace(".0", "");

                                    billing_total_txt.setText(abc);
                                    homecollection_chk.setClickable(false);
                                } else {
                                    Log.d("VALUESCHANGES", (dtotal * userCount + Double.valueOf(home_collection_charges)) + "");

                                    if (homecollection_chk.isChecked()) {
                                        row.setVisibility(View.VISIBLE);
                                        String abc = ("Rs. " + (dtotal * userCount + Double.valueOf(home_collection_charges)) + "").replace(".0", "");
                                        billing_total_txt.setText(abc);
                                    } else {
                                        String abc = ("Rs. " + dtotal * userCount + "").replace(".0", "");
                                        row.setVisibility(View.VISIBLE);
                                        billing_total_txt.setText(abc);
                                    }

                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Please Select...", Toast.LENGTH_SHORT).show();
                                btnSelectICareFor.setText("I Care For");
                            }

                        }
                    })
                    .setNegativeButton(" I Care For ", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(getApplicationContext(), AddUsers.class);
                            intent.putExtra("From", "Billing");
                            startActivity(intent);
                        }
                    });

            DummyTestBillingActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    progressDialog.dismiss();

                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
            Button positiveButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
            Button negativeButton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);

            if (positiveButton != null) {
                positiveButton.setTextColor(Color.WHITE);
                positiveButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                negativeButton.setTextColor(Color.WHITE);
                negativeButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins(10, 0, 20, 0);
                negativeButton.setLayoutParams(params);
            }
        }
    }

    private String updateTime(int hours, int mins) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";

        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);
        String hourss = "";
        if (hours < 10)
            hourss = "0" + hours;
        else
            hourss = String.valueOf(hours);

        String aTime = new StringBuilder().append(hourss).append(':').append(minutes).append(" ").append(timeSet).toString();

        return aTime;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void addNewEvent(int ryear, int rmonth, int rday, int rhour, int rmin) {
        final long oneHour = 1000 * 60 * 60;
        final long tenMinutes = 1000 * 60 * 1;

        long startMillis = 0;
        long endMillis = 0;
        java.util.Calendar beginTime = java.util.Calendar.getInstance();
        beginTime.set(ryear, rmonth, rday, rhour, rmin);
        startMillis = beginTime.getTimeInMillis();
        java.util.Calendar endTime = java.util.Calendar.getInstance();
        endTime.set(ryear, rmonth, rday, rhour, rmin + 10);
        endMillis = endTime.getTimeInMillis();
        TimeZone tz = TimeZone.getDefault();
        long oneHourFromNow = (new Date()).getTime() + oneHour;
        long tenMinutesFromNow = (new Date()).getTime() + tenMinutes;
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        if (homecollection_chk == compoundButton) {
            if (isChecked) {
                row.setVisibility(View.VISIBLE);

                if (btnSelectICareFor.equals("I Care For")) {
                    String abc = ("Rs. " + Double.valueOf(home_collection_charges) + "").replace(".0", "");
                    billing_total_txt.setText(abc);
                } else {
                    String abc = ("Rs. " + (dtotal * userCount + Double.valueOf(home_collection_charges)) + "").replace(".0", "");
                    billing_total_txt.setText(abc);
                }
            } else {
                row.setVisibility(View.VISIBLE);
                if (btnSelectICareFor.equals("I Care For")) {
                    String abc = ("Rs. " + Double.valueOf(home_collection_charges) + "").replace(".0", "");
                    billing_total_txt.setText(abc);
                } else {
                    String abc = ("Rs. " + (dtotal * userCount) + "").replace(".0", "");
                    billing_total_txt.setText(abc);
                }
            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}