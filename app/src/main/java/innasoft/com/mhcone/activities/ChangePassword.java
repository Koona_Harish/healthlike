package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class ChangePassword extends AppCompatActivity implements View.OnClickListener {
    TextInputLayout current_password_Til,new_password_Til,conform_password_Til;
    TextInputEditText current_password_edt,new_password_edt,conform_password_edt;
    Button btn_change_password_cancel,btn_change_password_change;
    TextView change_password_activity_headding;
    UserSessionManager userSessionManager;
    String userID;
    Typeface typeface,typeface2;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password_dummy);

        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typeface2 = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");


        progressDialog = new ProgressDialog(ChangePassword.this,R.style.DialogTheme);
        progressDialog.setMessage("Please wait......");

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = userSessionManager.getUserDetails();
        userID = user.get(UserSessionManager.USER_ID);

        change_password_activity_headding = (TextView) findViewById(R.id.change_password_activity_headding);
        change_password_activity_headding.setTypeface(typeface2);

        current_password_Til = (TextInputLayout) findViewById(R.id.current_password_til);
        current_password_Til.setTypeface(typeface);
        new_password_Til = (TextInputLayout) findViewById(R.id.new_password_til);
        new_password_Til.setTypeface(typeface);
        conform_password_Til = (TextInputLayout) findViewById(R.id.conform_password_til);
        conform_password_Til.setTypeface(typeface);
        current_password_edt = (TextInputEditText) findViewById(R.id.current_et_password);
        current_password_edt.setTypeface(typeface);
        new_password_edt = (TextInputEditText) findViewById(R.id.new_et_password);
        new_password_edt.setTypeface(typeface);
        conform_password_edt = (TextInputEditText) findViewById(R.id.conform_et_password);
        conform_password_edt.setTypeface(typeface);
        btn_change_password_cancel = (Button) findViewById(R.id.change_password_cancel);
        btn_change_password_cancel.setTypeface(typeface);
        btn_change_password_cancel.setOnClickListener(this);
        btn_change_password_change = (Button) findViewById(R.id.change_password_change);
        btn_change_password_change.setTypeface(typeface);
        btn_change_password_change.setOnClickListener(this);
        btn_change_password_cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if(v == btn_change_password_change) {

            if (validate()) {
                progressDialog.show();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.CHANGE_PASSWORD_URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                try {

                                    JSONObject jsonObject = new JSONObject(response);
                                    String successCode = jsonObject.getString("status");
                                    if(successCode.equals("30100"))
                                    {
                                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                        String message = jsonObject1.getString("msg");
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "Your password changed successfully..!", Toast.LENGTH_SHORT).show();
                                        userSessionManager.logoutUser();

                                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                        startActivity(intent);
                                    }
                                    if(successCode.equals("40100"))
                                    {
                                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                        String message = jsonObject1.getString("msg");
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                    }
                                    if(successCode.equals("20130"))
                                    {
                                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                        String message = jsonObject1.getString("msg");
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "Please provide required fields....!", Toast.LENGTH_SHORT).show();
                                    }

                                    if(successCode.equals("10140"))
                                    {
                                        userSessionManager.logoutUser();
                                        Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        startActivity(intent);
                                    }

                                    if(successCode.equals("10150"))
                                    {
                                        userSessionManager.logoutUser();
                                        Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        startActivity(intent);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        })
                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("user_id",userID);
                        params.put("old_pass", current_password_edt.getText().toString());
                        params.put("new_pass", new_password_edt.getText().toString());
                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(this);
                requestQueue.add(stringRequest);
            }
        }
        if(v ==  btn_change_password_cancel)
        {
            finish();
        }
    }

    private boolean validate()
    {
        boolean result = true;
        int flag = 0;

        String strCurrentPassword = current_password_edt.getText().toString();

        if (strCurrentPassword == null || strCurrentPassword.equals("") ||strCurrentPassword.length()<6)
        {
            current_password_Til.setError(getString(R.string.invalidPasswor));
            result = false;
        }
        else {
            current_password_Til.setErrorEnabled(false);
        }

        String strNewPassword = new_password_edt.getText().toString();

        if (strNewPassword == null || strNewPassword.equals("") || strNewPassword.matches("^-\\s")||strNewPassword.length()<6 )
        {
            new_password_edt.setError("Minimum 6 Characters");
            result = false;

        }
        else {
            new_password_Til.setErrorEnabled(false);

        }



        String strConformPassword = conform_password_edt.getText().toString();

        if (strConformPassword == null || strConformPassword.equals("")|| strConformPassword.matches("^-\\s") ||strConformPassword.length()<6)
        {
            conform_password_edt.setError("Minimum 6 Characters");
            result = false;

        }
        else {
            conform_password_Til.setErrorEnabled(false);

        }



        if(strNewPassword != "" && strConformPassword != "" &&!strConformPassword.equals(strNewPassword) && flag == 0 )
        {
            new_password_Til.setError(getString(R.string.passwordMismatch));
            conform_password_Til.setError(getString(R.string.passwordMismatch));
            result = false;
        }else {

             conform_password_Til.setErrorEnabled(false);
             new_password_Til.setErrorEnabled(false);
         }

        return result;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
