package innasoft.com.mhcone.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;

public class AccountVerification extends AppCompatActivity implements View.OnClickListener {

    TextView account_verification_tittle, resend_otp;
    TextInputLayout account_verify_til;
    EditText account_et_verify;
    Button verify_otp;
    String userId, email, mobile, loginIntent = "";
    Typeface typeface, typeface2;
    ProgressDialog progressDialog;
    SmsVerifyCatcher smsVerifyCatcher;
    public static final int MY_PERMISSIONS_REQUEST_READ_SMS = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_verification_dummy);

        Bundle bundle = getIntent().getExtras();
        userId = bundle.getString("userId");
        email = bundle.getString("email");
        mobile = bundle.getString("mobile");
        if (bundle.getString("Login") != null)
            loginIntent = bundle.getString("Login");

        progressDialog = new ProgressDialog(AccountVerification.this, R.style.DialogTheme);
        progressDialog.setMessage("Please wait......");
        progressDialog.setCancelable(false);

        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typeface2 = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        account_verification_tittle = (TextView) findViewById(R.id.account_verification_tittle);
        account_verification_tittle.setTypeface(typeface2);

        resend_otp = (TextView) findViewById(R.id.resend_otp);
        resend_otp.setTypeface(typeface);

        account_verify_til = (TextInputLayout) findViewById(R.id.account_verify_til);
        account_verify_til.setTypeface(typeface);

        account_et_verify = (EditText) findViewById(R.id.account_et_verify);
        account_et_verify.setTypeface(typeface);

        verify_otp = (Button) findViewById(R.id.verify_otp);
        verify_otp.setTypeface(typeface);

        verify_otp.setOnClickListener(this);
        resend_otp.setOnClickListener(this);


        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = message.replaceAll("[^0-9]", "");
                account_et_verify.setText(code);
                Log.v("message >>> ", message.replaceAll("[^0-9]", ""));
            }
        });

    }

    @Override
    public void onClick(View v) {

        if (v == verify_otp) {

            if (validate()) {
                final String verify = account_et_verify.getText().toString().trim();
                verify_otp.setClickable(false);
                progressDialog.show();

                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ACCOUNTVERIFICATION_URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String successResponceCode = jsonObject.getString("status");
                                    if (successResponceCode.equals("50100")) {
                                        progressDialog.cancel();
                                        verify_otp.setClickable(false);

                                        Toast.makeText(getApplicationContext(), "Your Account has been Successfully verified...!", Toast.LENGTH_SHORT).show();
                                        if (loginIntent.length() > 0 && loginIntent.equalsIgnoreCase("other")) {
                                            Intent intent = new Intent(AccountVerification.this, LoginActivity.class);
                                            intent.putExtra("Login", loginIntent);
                                            startActivity(intent);
                                        } else {
                                            Intent intent = new Intent(AccountVerification.this, LoginActivity.class);
                                            startActivity(intent);
                                        }
                                    }
                                    if (successResponceCode.equals("40111")) {
                                        progressDialog.cancel();
                                        verify_otp.setClickable(true);

                                    }

                                    if (successResponceCode.equals("60111")) {
                                        progressDialog.cancel();
                                        verify_otp.setClickable(true);

                                        Toast.makeText(getApplicationContext(), "Please Check your otp ...!", Toast.LENGTH_SHORT).show();
                                    }
                                    progressDialog.cancel();

                                } catch (JSONException e) {
                                    progressDialog.cancel();
                                    e.printStackTrace();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                verify_otp.setClickable(true);
                                progressDialog.cancel();


                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("user_id", userId);
                        params.put("otp", verify);
                        params.put("email", email);
                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(this);
                requestQueue.add(stringRequest);
            }
        }

        if (v == resend_otp) {
            //RESEND_OTP = "http://myhealthapp.net.in/Rest/resend_otp";
            //Toast.makeText(getApplicationContext(),"sadfesadfdsafds",Toast.LENGTH_LONG).show();
            progressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.RESEND_OTP,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("RESENDSRESPONCE", response);
                                String successResponceCode = jsonObject.getString("status");
                                if (successResponceCode.equals("2000")) {
                                    progressDialog.cancel();
                                    //verify_otp.setClickable(false);

                                    new Handler().postDelayed(new Runnable() {
                                                                  public void run() {
                                                                      resend_otp.setEnabled(false);
                                                                  }
                                                              }, 10000    //Specific time in milliseconds
                                    );

                                    Toast.makeText(getApplicationContext(), "New OTP Sent to Successfully..!", Toast.LENGTH_SHORT).show();
                                   /* Intent intent = new Intent(AccountVerification.this, LoginActivity.class);
                                    startActivity(intent);*/

                                }
                                if (successResponceCode.equals("40111")) {
                                    progressDialog.cancel();
                                    verify_otp.setClickable(true);

                                }

                                if (successResponceCode.equals("60111")) {
                                    progressDialog.cancel();
                                    verify_otp.setClickable(true);

                                    Toast.makeText(getApplicationContext(), "Please Check your otp ...!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            verify_otp.setClickable(true);
                            progressDialog.cancel();


                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", userId);
                    Log.d("MOBILE: ", userId);
                    Log.d("RESENDSRESPONCE", params.toString());

                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        }
    }

    private boolean validate() {
        boolean result = true;

        String verify = account_et_verify.getText().toString().trim();
        if (verify.length() != 6) {

            account_verify_til.setError(getString(R.string.otp));
            result = false;

        } else
            account_verify_til.setErrorEnabled(false);

        return result;
    }

    @Override
    protected void onResume() {
        checkCallPermission();
        super.onResume();
    }

    public boolean checkCallPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CALL_PHONE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Call")
                        .setMessage("Call Permission Required")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(AccountVerification.this,
                                        new String[]{Manifest.permission.CALL_PHONE},
                                        MY_PERMISSIONS_REQUEST_READ_SMS);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        MY_PERMISSIONS_REQUEST_READ_SMS);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (String permission : permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                //denied
                Log.e("denied", permission);
                Toast.makeText(getApplicationContext(), "Permissions required", Toast.LENGTH_SHORT).show();
            } else {
                if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                    //allowed
                    Log.e("allowed", permission);
//                    gps = new GPSTrackers(getApplicationContext());

                } else {
                    //set to never ask again
                    Log.e("set to never ask again", permission);
                    //do something here.
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }
            }
        }
    }

  /*  @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(),StaticActivity.class);
        startActivity(intent);
    }*/
}
