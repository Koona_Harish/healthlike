package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.CreatePackageListAdapter;
import innasoft.com.mhcone.models.CreatePackageSubtestModel;
import innasoft.com.mhcone.models.CreatePackageTestsModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.CreatePackageDBHandler;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class CreateMyPackage extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    EditText userPackageNameEt;
    Button savePackage;
    ImageButton cancelCreatePackage;
    public ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    TextView testName,noInternetConnect;
    UserSessionManager userSessionManager;
    String access_key,disp_userName,disp_email;
    String userID;

    ProgressDialog progressDialog;

    Typeface type_lato,typebold;
    List<CreatePackageSubtestModel> subItemList = null;
    List<CreatePackageTestsModel> superitemList = null;
    CreatePackageSubtestModel subtestModel = null;
    CreatePackageTestsModel superItem = null;
    private int lastExpandedPosition = -1;
    CreatePackageDBHandler db;

    Toast toast;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_my_package);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Create My Package");

        type_lato = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typebold = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        testName = (TextView) findViewById(R.id.txt_test_name);
        testName.setTypeface(type_lato);

        noInternetConnect = (TextView) findViewById(R.id.noInternetConnect);
        noInternetConnect.setTypeface(type_lato);

        userPackageNameEt = (EditText) findViewById(R.id.userPackageName);
        userPackageNameEt.setTypeface(type_lato);
        savePackage = (Button) findViewById(R.id.savePackage);
        savePackage.setTypeface(typebold);
        cancelCreatePackage = (ImageButton) findViewById(R.id.btn_close);
        cancelCreatePackage.setOnClickListener(this);

        progressDialog = new ProgressDialog(CreateMyPackage.this, R.style.DialogTheme);
        progressDialog.setMessage("Please wait......");

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = userSessionManager.getUserDetails();
        access_key = user.get(UserSessionManager.KEY_ACCSES);

        disp_userName = user.get(UserSessionManager.USER_NAME);
        disp_email = user.get(UserSessionManager.USER_EMAIL);
        userID = user.get(UserSessionManager.USER_ID);

        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.create_my_package_refresh);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,

                android.R.color.holo_green_light,

                android.R.color.holo_orange_light,

                android.R.color.holo_red_light);

        mSwipeRefreshLayout.setOnRefreshListener(this);

        db = new CreatePackageDBHandler(this);
        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        fillData();

        savePackage.setOnClickListener(this);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    expandableListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

        expandableListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                boolean enable = false;
                if(expandableListView != null && expandableListView.getChildCount() > 0){

                    boolean firstItemVisible = expandableListView.getFirstVisiblePosition() == 0;
                    boolean topOfFirstItemVisible = expandableListView.getChildAt(0).getTop() == 0;
                    enable = firstItemVisible && topOfFirstItemVisible;
                }
                mSwipeRefreshLayout.setEnabled(enable);
            }
        });
    }

    private void fillData() {

        CreateMyPackage.this.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.show();
            }
        });
        noInternetConnect.setVisibility(View.INVISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_TESTS_AND_SUBTESTS_TWO,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            superitemList = new ArrayList<CreatePackageTestsModel>();
                            String responceCode = jsonObject.getString("status");
                            if(responceCode.equals("19999")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    superItem = new CreatePackageTestsModel();
                                    subItemList = new ArrayList<CreatePackageSubtestModel>();
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    String data = jsonObject1.getString("test_name");
                                    String id = jsonObject1.getString("test_id");
                                    String test_image = AppUrls.IMAGE_URL+jsonObject1.getString("test_image");

                                    superItem.setTest_name(data);
                                    superItem.setTest_id(id);
                                    superItem.setTest_image(AppUrls.BASE_URL + test_image);

                                    JSONArray jsonArray1 = jsonObject1.getJSONArray("sub_test_wise");
                                    if (jsonArray1 != null && jsonArray1.length() != 0) {
                                        for (int j = 0; j < jsonArray1.length(); j++) {

                                            subtestModel = new CreatePackageSubtestModel();

                                            JSONObject jsonObject2 = jsonArray1.getJSONObject(j);
                                            String data2 = jsonObject2.getString("sub_test_name");
                                            String test_id = jsonObject2.getString("test_id");
                                            String sub_test_id = jsonObject2.getString("sub_test_id");

                                            subtestModel.setSub_test_name(data2);
                                            subtestModel.setTest_id(test_id);
                                            subtestModel.setSub_test_id(sub_test_id);
                                            subItemList.add(subtestModel);
                                        }
                                        superItem.setSubtestModel(subItemList);
                                        superitemList.add(superItem);
                                        CreateMyPackage.this.runOnUiThread(new Runnable() {
                                            public void run() {
                                                progressDialog.dismiss();
                                            }
                                        });
                                    }
                                }

                                expandableListAdapter = new CreatePackageListAdapter(CreateMyPackage.this, superitemList);
                                expandableListView.setAdapter(expandableListAdapter);
                                mSwipeRefreshLayout.setRefreshing(false);
                                expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                                    @Override
                                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                                        Toast.makeText(getApplicationContext(), superitemList.get(groupPosition).getTest_id() + " : " + superitemList.get(groupPosition).getSubtestModel().get(childPosition).getSub_test_id(), Toast.LENGTH_SHORT).show();
                                        return false;
                                    }
                                });
                            }if(responceCode.equals("18888")) {
                                CreateMyPackage.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        progressDialog.dismiss();
                                    }
                                });
                                mSwipeRefreshLayout.setRefreshing(false);
                                noInternetConnect.setText("No Tests found");
                                noInternetConnect.setVisibility(View.VISIBLE);
                            }

                            if(responceCode.equals("10140"))
                            {
                                userSessionManager.logoutUser();
                                Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }

                            if(responceCode.equals("10150"))
                            {
                                userSessionManager.logoutUser();
                                Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        CreateMyPackage.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();
                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", userID);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {
        if(v == savePackage)
        {

            List<String> testIdsList = db.getAllTestIds();
            List<String> subTestIdsList = db.getAllSubTestIds();
            List<String> sendList = new ArrayList<String>();
            for (int i = 0; i < testIdsList.size(); i++) {
                sendList.add(testIdsList.get(i) + "-" + subTestIdsList.get(i));
            }
            final String sendListString = sendList.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", "");
            if(testIdsList.size() !=0 ) {

                    if (userPackageNameEt.getText().toString().trim().length() != 0 && !userPackageNameEt.getText().toString().equals(null)) {

                        progressDialog.show();

                        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.SAVE_MY_PACKAGE,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                        progressDialog.dismiss();
                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            String responceCode = jsonObject.getString("status");
                                            if (responceCode.equals("20100")) {
                                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                                String mesage = jsonObject1.getString("msg");

                                                if (mesage.equals("Your Package Successfully Addes")) {
                                                    db.deleteTableData();
                                                    final Toast toast = Toast.makeText(CreateMyPackage.this, "Package Created Successfully....!", Toast.LENGTH_SHORT);
                                                    toast.show();

                                                    Handler handler = new Handler();
                                                    handler.postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            toast.cancel();
                                                        }
                                                    }, 100);
                                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                    startActivity(intent);
                                                }
                                                if (mesage.equals("Package Already Exits")) {
                                                    Toast.makeText(getApplicationContext(), "Package Name Already Exits", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            if(responceCode.equals("10140"))
                                            {
                                                userSessionManager.logoutUser();
                                                Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                startActivity(intent);
                                            }

                                            if(responceCode.equals("10150"))
                                            {
                                                userSessionManager.logoutUser();
                                                Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                startActivity(intent);
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        progressDialog.dismiss();

                                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                        } else if (error instanceof AuthFailureError) {

                                        } else if (error instanceof ServerError) {

                                        } else if (error instanceof NetworkError) {

                                        } else if (error instanceof ParseError) {

                                        }
                                    }
                                })
                        {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {

                                Map<String, String> params = new HashMap<String, String>();
                                String pakName = userPackageNameEt.getText().toString();
                                params.put("user_package_name", pakName);
                                params.put("user_id", userID);
                                params.put("test_id", sendListString);
                                return params;
                            }
                        };
                        RequestQueue requestQueue = Volley.newRequestQueue(this);
                        requestQueue.add(stringRequest);
                }
                else {
                    Toast.makeText(getApplicationContext(), "Enter Your Package Name...!", Toast.LENGTH_SHORT).show();
                }
            }
            else {
                Toast.makeText(getApplicationContext(), "Please Select at least one Test ...!", Toast.LENGTH_SHORT).show();
            }
        }
        if (v == cancelCreatePackage){
            finish();
        }
    }


    @Override
    public void onRefresh() {
        fillData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop () {
        super.onStop();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
