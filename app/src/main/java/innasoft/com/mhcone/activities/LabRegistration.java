package innasoft.com.mhcone.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;


import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.FilePath;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class LabRegistration extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private static final int SELECT_PICTURE = 200;
    private static final int SELECT_PICTURE1 = 300;

    private static final int SELECT_PICK = 100;
    private static final int SELECT_PICK2 = 200;
    private static final int SELECT_PICK3 = 300;
    private static final int SELECT_PICK4 = 400;
    private static final int SELECT_PICK5 = 500;
    private static final int SELECT_PICK6 = 600;
    private static final int SELECT_PICK7 = 700;
    private static final int REQUEST_GALLERY = 0;
    private String selectedFilePath = "NONE",selectedFilePath1 = "NONE";
    private String imageSelectedPath,imageSelectedPath2,imageSelectedPath3,imageSelectedPath4,imageSelectedPath5,imageSelectedPath6,imageSelectedPath7;
    private static final int PLACE_PICKER_REQUEST = 1;
    EditText labName_et,labEmail_et,labPhone_et;
    TextInputEditText labPassword_et;
    TextInputLayout name_til,email_til,phone_til,passwor_til;
    Button addressPicker,lab_submit,lab_cancel;
    TextView address_tv,image_name_tv,image_name_tv2,image_name_tv3,image_name_tv4,lab_reg_activity_headding,labWorkingTittle,terms_conditions_tv;
    String latitude,longitude,finaladdress,starttime;
    private int mHour,mMinute;
    CheckBox cb_sun,cb_mon,cb_tue,cb_wed,cb_thu,cb_fri,cb_sat;
    public String labname,labemail,labpassword,labphone;
    public TextView mon_time_tv,tue_time_tv,wed_time_tv,thu_time_tv,fri_time_tv,sat_time_tv,sun_time_tv;
    int mon_flag,tue_flag,wed_flag,thu_flag,fri_flag,sat_flag,sun_flag;
    ProgressDialog progressDialog;
    public CheckBox terms_conditions_ck;
    int browseStatusCheck = 0,browseStatusCheck2 = 0, browseStatusCheck3 = 0, browseStatusCheck4 = 0, browseStatusCheck5 = 0, browseStatusCheck6 = 0, browseStatusCheck7 = 0;
    CheckBox sample_collectCK,onlinePaymentModeCK,cashatLabCK,telebook_checkboxCK;
    UserSessionManager session;

    String sunSendTime,monSendTime,tuesSendTime,wedSendTime,thuSendTime,friSendTime,satSendTime;
    Typeface typeface;

    List<String> workingdays,workinghours;
    String workingdaysString,workinghoursString;
    TextView lab_profile_pics_tv,lab_profile_pics_tv2,lab_profile_pics_tv3;
    private static final  int PICK_LAB_PICKS = 225;

    HttpEntity resEntity;

    public ArrayList<String> map = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lab_registration);

        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");

        workingdays = new LinkedList<>();
        workinghours = new LinkedList<>();

        session = new UserSessionManager(getApplicationContext());

        lab_reg_activity_headding = (TextView) findViewById(R.id.lab_reg_activity_headding);
        lab_reg_activity_headding.setTypeface(typeface);

        name_til = (TextInputLayout) findViewById(R.id.lab_name_til);
        name_til.setTypeface(typeface);
        email_til = (TextInputLayout) findViewById(R.id.lab_email_til);
        email_til.setTypeface(typeface);
        phone_til = (TextInputLayout) findViewById(R.id.lab_phone_til);
        phone_til.setTypeface(typeface);
        passwor_til = (TextInputLayout) findViewById(R.id.lab_password_til);
        passwor_til.setTypeface(typeface);
        terms_conditions_tv = (TextView) findViewById(R.id.terms_conditions_tv);
        terms_conditions_tv.setTypeface(typeface);
        terms_conditions_tv.setOnClickListener(this);
        terms_conditions_ck = (CheckBox) findViewById(R.id.terms_conditions_ck);
        terms_conditions_ck.setOnCheckedChangeListener(this);

        labName_et = (EditText) findViewById(R.id.lab_name_et);
        labName_et.setTypeface(typeface);
        labEmail_et = (EditText) findViewById(R.id.lab_email_et);
        labEmail_et.setTypeface(typeface);
        labPhone_et = (EditText) findViewById(R.id.lab_phone_number_et);
        labPhone_et.setTypeface(typeface);
        labPassword_et = (TextInputEditText) findViewById(R.id.lab_password_et);
        labPassword_et.setTypeface(typeface);

        addressPicker = (Button) findViewById(R.id.pick_address);
        addressPicker.setTypeface(typeface);

        lab_submit = (Button) findViewById(R.id.lab_submit_registraton);
        lab_submit.setTypeface(typeface);

        lab_submit.setBackgroundColor(Color.GRAY);
        lab_submit.setTextColor(Color.BLACK);
        lab_cancel = (Button) findViewById(R.id.lab_submit_cancel);
        lab_cancel.setTypeface(typeface);

        sample_collectCK = (CheckBox) findViewById(R.id.smple_checkbox);
        sample_collectCK.setTypeface(typeface);
        onlinePaymentModeCK = (CheckBox) findViewById(R.id.onlinePaymentMode_checkbox);
        onlinePaymentModeCK.setTypeface(typeface);
        cashatLabCK = (CheckBox) findViewById(R.id.casatlab_checkbox);
        cashatLabCK.setTypeface(typeface);
        telebook_checkboxCK = (CheckBox) findViewById(R.id.telebook_checkbox);
        telebook_checkboxCK.setTypeface(typeface);

        labWorkingTittle = (TextView) findViewById(R.id.labworking_tittle);
        labWorkingTittle.setTypeface(typeface);

        address_tv = (TextView) findViewById(R.id.display_text);
        address_tv.setTypeface(typeface);

        cb_sun = (CheckBox) findViewById(R.id.ck_sun);
        cb_sun.setTypeface(typeface);
        cb_mon = (CheckBox) findViewById(R.id.ck_mon);
        cb_mon.setTypeface(typeface);
        cb_tue = (CheckBox) findViewById(R.id.ck_tus);
        cb_tue.setTypeface(typeface);
        cb_wed = (CheckBox) findViewById(R.id.ck_wed);
        cb_wed.setTypeface(typeface);
        cb_thu = (CheckBox) findViewById(R.id.ck_thu);
        cb_thu.setTypeface(typeface);
        cb_fri = (CheckBox) findViewById(R.id.ck_fri);
        cb_fri.setTypeface(typeface);
        cb_sat = (CheckBox) findViewById(R.id.ck_sat);
        cb_sat.setTypeface(typeface);

        mon_time_tv = (TextView) findViewById(R.id.monday_time);
        mon_time_tv.setTypeface(typeface);
        tue_time_tv = (TextView) findViewById(R.id.tuesday_time);
        tue_time_tv.setTypeface(typeface);
        wed_time_tv = (TextView) findViewById(R.id.wednesday_time);
        wed_time_tv.setTypeface(typeface);
        thu_time_tv = (TextView) findViewById(R.id.thursday_time);
        thu_time_tv.setTypeface(typeface);
        fri_time_tv = (TextView) findViewById(R.id.friday_time);
        fri_time_tv.setTypeface(typeface);
        sat_time_tv = (TextView) findViewById(R.id.saturday_time);
        sat_time_tv.setTypeface(typeface);
        sun_time_tv = (TextView) findViewById(R.id.sunday_time);
        sun_time_tv.setTypeface(typeface);

        addressPicker.setOnClickListener(this);
        lab_submit.setOnClickListener(this);
        lab_cancel.setOnClickListener(this);

        cb_sun.setOnCheckedChangeListener(this);
        cb_mon.setOnCheckedChangeListener(this);
        cb_tue.setOnCheckedChangeListener(this);
        cb_wed.setOnCheckedChangeListener(this);
        cb_thu.setOnCheckedChangeListener(this);
        cb_fri.setOnCheckedChangeListener(this);
        cb_sat.setOnCheckedChangeListener(this);
    }

    private String doFileUpload() {

        String response_str = null;
        File file = null,file2 = null,file3 = null,file4 = null,file5 = null,file6 = null,file7 = null;
        int filecheck = 0,filecheck2 = 0,filecheck3 = 0,filecheck4 = 0,filecheck5 = 0,filecheck6 = 0,filecheck7 = 0;
        FileBody bin = null,bin2 ,bin3 ,bin4 ,bin5 ,bin6 ,bin7 ;

        labname = labName_et.getText().toString().trim();
        labemail = labEmail_et.getText().toString().trim();
        labphone = labPhone_et.getText().toString().trim();
        labpassword = labPassword_et.getText().toString().trim();

        String urlString = AppUrls.LAB_REGISTRATION_URL;
        try
        {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(urlString);
            MultipartEntity reqEntity = new MultipartEntity();

            reqEntity.addPart("user_name", new StringBody(labname));
            reqEntity.addPart("email", new StringBody(labemail));
            reqEntity.addPart("mobile", new StringBody(labphone));
            reqEntity.addPart("password", new StringBody(labpassword));
            String s_day,m_day,tu_day,w_day,thu_day,f_day,sat_day;
            if(cb_mon.isChecked())
            {
                String sendDataVlaue = monSendTime+"-1";
                m_day = sendDataVlaue;

                reqEntity.addPart("mon", new StringBody(sendDataVlaue));
            }
            else {
                String sendDataValue = "09:00 AM - 09:00 PM-0";
                m_day = sendDataValue;
                reqEntity.addPart("mon", new StringBody(sendDataValue));
            }

            if(cb_tue.isChecked())
            {
                String sendDataVlaue = tuesSendTime+"-1";
                tu_day = sendDataVlaue;
                reqEntity.addPart("tue", new StringBody(sendDataVlaue));
            }
            else {
                String sendDataValue = "09:00 AM - 09:00 PM-0";
                tu_day = sendDataValue;
                reqEntity.addPart("tue", new StringBody(sendDataValue));
            }

            if(cb_wed.isChecked())
            {
                String sendDataVlaue = wedSendTime+"-1";
                w_day = sendDataVlaue;
                reqEntity.addPart("wed", new StringBody(sendDataVlaue));
            }
            else {
                String sendDataValue = "09:00 AM - 09:00 PM-0";
                w_day = sendDataValue;
                reqEntity.addPart("wed", new StringBody(sendDataValue));
            }

            if(cb_thu.isChecked())
            {
                String sendDataVlaue = thuSendTime+"-1";
                thu_day = sendDataVlaue;
                reqEntity.addPart("thu", new StringBody(sendDataVlaue));
            }
            else {
                String sendDataValue = "09:00 AM - 09:00 PM-0";
                thu_day = sendDataValue;
                reqEntity.addPart("thu", new StringBody(sendDataValue));
            }

            if(cb_fri.isChecked())
            {
                String sendDataVlaue = friSendTime+"-1";
                f_day = sendDataVlaue;
                reqEntity.addPart("fri", new StringBody(sendDataVlaue));
            }
            else {
                String sendDataValue = "09:00 AM - 09:00 PM-0";
                f_day = sendDataValue;
                reqEntity.addPart("fri", new StringBody(sendDataValue));
            }

            if(cb_sat.isChecked())
            {
                String sendDataVlaue = satSendTime+"-1";
                sat_day = sendDataVlaue;
                reqEntity.addPart("sat", new StringBody(sendDataVlaue));
            }
            else {
                String sendDataValue = "09:00 AM - 09:00 PM-0";
                sat_day = sendDataValue;
                reqEntity.addPart("sat", new StringBody(sendDataValue));
            }

            if(cb_sun.isChecked())
            {
                String sendDataVlaue = sunSendTime+"-1";
                s_day = sendDataVlaue;
                reqEntity.addPart("sun", new StringBody(sendDataVlaue));
            }
            else {
                String sendDataValue = "09:00 AM - 09:00 PM-0";
                s_day = sendDataValue;
                reqEntity.addPart("sun", new StringBody(sendDataValue));
            }

            reqEntity.addPart("address", new StringBody(finaladdress) );
            reqEntity.addPart("lat", new StringBody(latitude) );
            reqEntity.addPart("lang", new StringBody(longitude) );

            if(sample_collectCK.isChecked()) {
                reqEntity.addPart("home_collections", new StringBody("1"));
            }else {
                reqEntity.addPart("home_collections", new StringBody("0"));
            }
            if(onlinePaymentModeCK.isChecked()) {
                reqEntity.addPart("payment_online", new StringBody("1"));
            }else {
                reqEntity.addPart("payment_online", new StringBody("0"));
            }

            if(cashatLabCK.isChecked()) {
                reqEntity.addPart("payment_lab", new StringBody("1"));
            }else {
                reqEntity.addPart("payment_lab", new StringBody("0"));
            }
            if(telebook_checkboxCK.isChecked()) {
                reqEntity.addPart("tele_booking", new StringBody("1"));
            }else {
                reqEntity.addPart("tele_booking", new StringBody("0"));
            }

            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post);
            resEntity = response.getEntity();
            response_str = EntityUtils.toString(resEntity);

        }
        catch (Exception ex){
            Log.e("Debug", "error: " + ex.getMessage(), ex);
        }
        return response_str;
    }

    private boolean checkGalarryPermission() {

        if ((ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) && (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)) {
            return false;

        } else {

            return true;

        }
    }

    private void selectImagefromGallery(int selectPIC) {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), selectPIC);
    }

    private void requestExternalStoragePermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_PICK)
        {
            if (resultCode == RESULT_OK) {

                Uri selectedFileUri = data.getData();

                imageSelectedPath = FilePath.getPath(this, selectedFileUri);

                if (imageSelectedPath != null && !imageSelectedPath.equals("")) {

                    String filename = imageSelectedPath.substring(imageSelectedPath.lastIndexOf("/") + 1);
                    image_name_tv.setText(filename);
                    browseStatusCheck = 1;

                } else {
                    Toast.makeText(this, "Cannot upload file to server", Toast.LENGTH_SHORT).show();
                    browseStatusCheck = 0;
                }

            } else if (resultCode == RESULT_CANCELED) {

                Toast.makeText(getApplicationContext(), "You cancelled select image", Toast.LENGTH_SHORT).show();

            } else {

                Toast.makeText(getApplicationContext(), "Sorry! Failed to record video", Toast.LENGTH_SHORT).show();
            }
        }
        if (requestCode == SELECT_PICK2)
        {
            if (resultCode == RESULT_OK) {

                Uri selectedFileUri = data.getData();
                imageSelectedPath2 = FilePath.getPath(this, selectedFileUri);

                if (imageSelectedPath2 != null && !imageSelectedPath2.equals("")) {

                    String filename = imageSelectedPath2.substring(imageSelectedPath2.lastIndexOf("/") + 1);
                    image_name_tv2.setText(filename);
                    browseStatusCheck2 = 1;

                } else {
                    Toast.makeText(this, "Cannot upload file to server", Toast.LENGTH_SHORT).show();
                }

            } else if (resultCode == RESULT_CANCELED) {

                Toast.makeText(getApplicationContext(), "You cancelled select image", Toast.LENGTH_SHORT).show();
                browseStatusCheck2 = 0;

            } else {

                Toast.makeText(getApplicationContext(), "Sorry! Failed to record video", Toast.LENGTH_SHORT).show();
            }
        }
        if (requestCode == SELECT_PICK3)
        {
            if (resultCode == RESULT_OK) {

                Uri selectedFileUri = data.getData();
                imageSelectedPath3 = FilePath.getPath(this, selectedFileUri);

                if (imageSelectedPath3 != null && !imageSelectedPath3.equals("")) {

                    String filename = imageSelectedPath3.substring(imageSelectedPath3.lastIndexOf("/") + 1);
                    image_name_tv3.setText(filename);
                    browseStatusCheck3 = 1;

                } else {
                    Toast.makeText(this, "Cannot upload file to server", Toast.LENGTH_SHORT).show();
                }

            } else if (resultCode == RESULT_CANCELED) {

                Toast.makeText(getApplicationContext(), "You cancelled select image", Toast.LENGTH_SHORT).show();
                browseStatusCheck3 = 0;

            } else {

                Toast.makeText(getApplicationContext(), "Sorry! Failed to record video", Toast.LENGTH_SHORT).show();
            }
        }
        if (requestCode == SELECT_PICK4)
        {
            if (resultCode == RESULT_OK) {

                Uri selectedFileUri = data.getData();
                imageSelectedPath4 = FilePath.getPath(this, selectedFileUri);

                if (imageSelectedPath4 != null && !imageSelectedPath4.equals("")) {

                    String filename = imageSelectedPath4.substring(imageSelectedPath4.lastIndexOf("/") + 1);
                    image_name_tv4.setText(filename);
                    browseStatusCheck4 = 1;

                } else {
                    Toast.makeText(this, "Cannot upload file to server", Toast.LENGTH_SHORT).show();
                }

            } else if (resultCode == RESULT_CANCELED) {
                browseStatusCheck4 = 0;

                Toast.makeText(getApplicationContext(), "You cancelled select image", Toast.LENGTH_SHORT).show();

            } else {

                Toast.makeText(getApplicationContext(), "Sorry! Failed to record video", Toast.LENGTH_SHORT).show();
            }
        }

        if (requestCode == SELECT_PICK5)
        {
            if (resultCode == RESULT_OK) {

                Uri selectedFileUri = data.getData();
                imageSelectedPath5 = FilePath.getPath(this, selectedFileUri);

                if (imageSelectedPath5 != null && !imageSelectedPath5.equals("")) {

                    String filename = imageSelectedPath5.substring(imageSelectedPath5.lastIndexOf("/") + 1);
                    lab_profile_pics_tv.setText(filename);
                    browseStatusCheck5 = 1;

                } else {
                    Toast.makeText(this, "Cannot upload file to server", Toast.LENGTH_SHORT).show();
                }

            } else if (resultCode == RESULT_CANCELED) {

                Toast.makeText(getApplicationContext(), "You cancelled select image", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(getApplicationContext(), "Sorry! Failed to record video", Toast.LENGTH_SHORT).show();
            }
        }

        if (requestCode == SELECT_PICK6)
        {
            if (resultCode == RESULT_OK) {

                Uri selectedFileUri = data.getData();
                imageSelectedPath6 = FilePath.getPath(this, selectedFileUri);
                if (imageSelectedPath6 != null && !imageSelectedPath6.equals("")) {
                    String filename = imageSelectedPath6.substring(imageSelectedPath6.lastIndexOf("/") + 1);
                    lab_profile_pics_tv2.setText(filename);
                    browseStatusCheck6 = 1;

                } else {
                    Toast.makeText(this, "Cannot upload file to server", Toast.LENGTH_SHORT).show();
                }

            } else if (resultCode == RESULT_CANCELED) {
                browseStatusCheck6 = 0;
                Toast.makeText(getApplicationContext(),"You cancelled select image", Toast.LENGTH_SHORT).show();

            } else {

                Toast.makeText(getApplicationContext(),"Sorry! Failed to record video", Toast.LENGTH_SHORT).show();
            }
        }

        if (requestCode == SELECT_PICK7)
        {
            if (resultCode == RESULT_OK) {

                Uri selectedFileUri = data.getData();
                imageSelectedPath7 = FilePath.getPath(this, selectedFileUri);

                if (imageSelectedPath7 != null && !imageSelectedPath7.equals("")) {

                    String filename = imageSelectedPath7.substring(imageSelectedPath7.lastIndexOf("/") + 1);
                    lab_profile_pics_tv3.setText(filename);
                    browseStatusCheck7 = 1;

                } else {
                    Toast.makeText(this, "Cannot upload file to server", Toast.LENGTH_SHORT).show();
                }

            } else if (resultCode == RESULT_CANCELED) {

                browseStatusCheck7 = 0;

                Toast.makeText(getApplicationContext(), "You cancelled select image", Toast.LENGTH_SHORT).show();

            } else {

                Toast.makeText(getApplicationContext(), "Sorry! Failed to record video", Toast.LENGTH_SHORT).show();
            }
        }

        if (requestCode == PLACE_PICKER_REQUEST  && resultCode == Activity.RESULT_OK) {

            final Place place = PlacePicker.getPlace(this, data);
            String stringlat = place.getLatLng().toString();
            final CharSequence name = place.getName();
            final CharSequence address = place.getAddress();
            String attributions = (String) place.getAttributions();
            if (attributions == null) {
                attributions = "";
            }

            address_tv.setText(address);
            finaladdress = address.toString();

            stringlat = stringlat.substring(stringlat.indexOf("(") + 1);
            stringlat = stringlat.substring(0, stringlat.indexOf(")"));
            String latValue = stringlat.split(",")[0];
            latitude = latValue;
            String lngValue = stringlat.split(",")[1];
            longitude = lngValue;

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }


    private boolean validateData() {
        boolean result = true;
        String name = labName_et.getText().toString().trim();
        if ((name == null || name.length() < 3) && name == "^[a-zA-Z\\\\s]+") {
            name_til.setError(getString(R.string.invalidName));
            result = false;
        }
        else
            name_til.setErrorEnabled(false);

        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        String email = labEmail_et.getText().toString().trim();
        if (!email.matches(EMAIL_REGEX)) {
            email_til.setError(getString(R.string.invalidEmailID));
            result = false;
        }
        else
            email_til.setErrorEnabled(false);

        String phone = labPhone_et.getText().toString().trim();
        if (phone == null || phone.equals("")) {
            phone_til.setError(getString(R.string.invalidNumber));
            result = false;
        }
        else
            phone_til.setErrorEnabled(false);


        String password = labPassword_et.getText().toString().trim();
        if (password.length() < 3) {
            passwor_til.setError(getString(R.string.invalidPasswor));
            result = false;
        }
        else
            passwor_til.setErrorEnabled(false);

        return result;
    }

    @Override
    public void onClick(View view) {

        if(view == terms_conditions_tv)
        {
            terms_conditions_tv.setTextColor(Color.parseColor("#43A047"));
            Intent intent = new Intent(getApplicationContext(), TermsConditions.class);
            startActivity(intent);
        }
        if(view == addressPicker)
        {
            try {
                PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
                Intent intent = intentBuilder.build(LabRegistration.this);
                startActivityForResult(intent, PLACE_PICKER_REQUEST);

            } catch (GooglePlayServicesRepairableException
                    | GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        }

        if(view == lab_submit)
        {
            if(validateData())
            {
                progressDialog = ProgressDialog.show(LabRegistration.this, "", "Please wait.....", false);

                Thread thread=new Thread(new Runnable(){
                    public void run(){
                        String responce_value =  doFileUpload();

                        try {
                            JSONObject jsonObject = new JSONObject(responce_value);
                            String statusCode = jsonObject.getString("status");
                            if (statusCode.equals("20100"))
                            {
                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                String mesage = jsonObject1.getString("msg");
                                runOnUiThread(new Runnable(){
                                    public void run() {
                                        try {
                                            runOnUiThread(new Runnable(){
                                                public void run() {
                                                    if(progressDialog.isShowing())
                                                        progressDialog.dismiss();
                                                }
                                            });
                                            Toast.makeText(getApplicationContext(),"Registration Completed...!", Toast.LENGTH_SHORT).show();

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }


                                });
                                if(session.checkLogin() != false) {

                                    Intent i = new Intent(LabRegistration.this, Static2Activity.class);
                                    startActivity(i);
                                }
                                else {
                                    Intent i = new Intent(LabRegistration.this, Main2Activity.class);
                                    startActivity(i);
                                }

                            }
                            if ((statusCode.equals("20120")))
                            {
                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                final String mesage = jsonObject1.getString("msg");
                                runOnUiThread(new Runnable(){
                                    public void run() {
                                        try {

                                            runOnUiThread(new Runnable(){
                                                public void run() {
                                                    if(progressDialog.isShowing())
                                                        progressDialog.dismiss();
                                                }
                                            });
                                            Toast.makeText(getApplicationContext(), mesage, Toast.LENGTH_SHORT).show();

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }


                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
            }
        }

        if (view == lab_cancel){

            finish();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

        if(terms_conditions_ck == compoundButton)
        {
            if(isChecked)
            {
                lab_submit.setBackgroundColor(Color.parseColor("#1FB991"));
                lab_submit.setTextColor(Color.WHITE);
                lab_submit.setClickable(true);
            }
            else {
                lab_submit.setBackgroundColor(Color.GRAY);
                lab_submit.setTextColor(Color.WHITE);
                lab_submit.setClickable(false);
            }
        }

        if(compoundButton == cb_sun)
        {
            if (isChecked) {
                sun_flag = 1;
                workingdays.add("sunday");
                startTimePicker();
            } else {
                sun_time_tv.setText("");
                sun_flag = 0;
                int x = workingdays.indexOf("sunday");
                workingdays.remove("sunday");
                workinghours.remove(x);
            }
        }
        if(compoundButton == cb_mon)
        {
            if (isChecked) {
                mon_flag = 1;
                workingdays.add("monday");
                startTimePicker();

            } else {
                mon_time_tv.setText("");
                mon_flag = 0;
                int x = workingdays.indexOf("monday");
                workingdays.remove("monday");
                workinghours.remove(x);
            }
        }
        if(compoundButton == cb_tue)
        {
            if (isChecked) {
                tue_flag = 1;
                workingdays.add("tuesday");
                startTimePicker();

            } else {
                tue_flag = 0;
                tue_time_tv.setText("");
                int x = workingdays.indexOf("tuesday");
                workingdays.remove("tuesday");
                workinghours.remove(x);
            }
        }
        if(compoundButton == cb_wed)
        {
            if (isChecked) {
                wed_flag = 1;
                workingdays.add("wednesday");
                startTimePicker();

            } else {
                wed_flag = 0;
                wed_time_tv.setText("");
                int x = workingdays.indexOf("wednesday");
                workingdays.remove("wednesday");
                workinghours.remove(x);
            }
        }
        if(compoundButton == cb_thu)
        {
            if (isChecked) {
                thu_flag = 1;
                workingdays.add("thursday");
                startTimePicker();

            } else {
                thu_flag = 0;
                thu_time_tv.setText("");
                int x = workingdays.indexOf("thursday");
                workingdays.remove("thursday");
                workinghours.remove(x);
            }
        }
        if(compoundButton == cb_fri)
        {
            if (isChecked) {
                fri_flag =1;
                workingdays.add("friday");
                startTimePicker();

            } else {
                fri_flag = 0;
                fri_time_tv.setText("");
                int x = workingdays.indexOf("friday");
                workingdays.remove("friday");
                workinghours.remove(x);
            }
        }
        if(compoundButton == cb_sat)
        {
            if (isChecked) {
                sat_flag = 1;
                workingdays.add("saturday");
                startTimePicker();

            } else {
                sat_flag = 0;
                sat_time_tv.setText("");
                int x = workingdays.indexOf("saturday");
                workingdays.remove("saturday");
                workinghours.remove(x);
            }
        }
        workingdaysString = workingdays.toString();
        workingdaysString = workingdaysString.replaceAll("\\[", "").replaceAll("\\]","").replaceAll(" ", "");
    }

    private void startTimePicker() {
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        final TimePickerDialog timePickerDialog = new TimePickerDialog(this,android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        String hourchange = String.format("%02d", hourOfDay);
                        String minitChange = String.format("%02d", minute);
                        starttime = hourchange + ":" + minitChange;
                        String disptime = updateTime(hourOfDay , minute);
                        endTimePicker(disptime);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                timePickerDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setVisibility(View.GONE);
            }
        });
        timePickerDialog.setTitle("Opening Time");
        timePickerDialog.setCancelable(false);
        timePickerDialog.show();
    }

    private void endTimePicker(final String disptime) {
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        final TimePickerDialog timePickerDialog = new TimePickerDialog(this,android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        String hourchange = String.format("%02d", hourOfDay);
                        String minitChange = String.format("%02d", minute);
                        starttime = hourchange + ":" + minitChange;
                        String endTime = updateTime(hourOfDay , minute);
                        String dispTimeValue = disptime+" to "+endTime;
                        String sendDaysTime = disptime+" - "+endTime;
                        if(mon_flag == 1)
                        {
                            mon_time_tv.setText(dispTimeValue);
                            monSendTime = sendDaysTime;
                            mon_flag = 0;
                        }

                        if(tue_flag == 1)
                        {
                            tue_time_tv.setText(dispTimeValue);
                            tuesSendTime = sendDaysTime;
                            tue_flag = 0;
                        }

                        if(wed_flag == 1)
                        {
                            wed_time_tv.setText(dispTimeValue);
                            wedSendTime = sendDaysTime;
                            wed_flag = 0;
                        }

                        if(thu_flag == 1)
                        {
                            thu_time_tv.setText(dispTimeValue);
                            thuSendTime = sendDaysTime;
                            thu_flag = 0;
                        }

                        if(fri_flag == 1)
                        {
                            fri_time_tv.setText(dispTimeValue);
                            friSendTime = sendDaysTime;
                            fri_flag = 0;
                        }

                        if(sat_flag == 1)
                        {
                            sat_time_tv.setText(dispTimeValue);
                            satSendTime = sendDaysTime;
                            sat_flag = 0;
                        }

                        if(sun_flag == 1)
                        {
                            sun_time_tv.setText(dispTimeValue);
                            sunSendTime = sendDaysTime;
                            sun_flag = 0;
                        }

                        workinghours.add(disptime+" to "+endTime);
                        workinghoursString = workinghours.toString();
                        workinghoursString = workinghoursString.replaceAll("\\[","").replaceAll("\\]", "");

                    }
                }, mHour, mMinute, false);
        timePickerDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                timePickerDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setVisibility(View.GONE);
            }
        });
        timePickerDialog.setTitle("Closing Time");
        timePickerDialog.setCancelable(false);
        timePickerDialog.show();

    }

    private String updateTime(int hours, int mins) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";

        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);
        String hourss = "";
        if(hours < 10)
            hourss = "0"  + hours;
        else
            hourss = String.valueOf(hours);

        String aTime = new StringBuilder().append(hourss).append(':').append(minutes).append(" ").append(timeSet).toString();

        return aTime;
    }
}
