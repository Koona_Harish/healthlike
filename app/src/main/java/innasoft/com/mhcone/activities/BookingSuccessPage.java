package innasoft.com.mhcone.activities;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.CalendarHelper;

public class BookingSuccessPage extends AppCompatActivity implements View.OnClickListener {
    ImageButton gotoHomeButton;
    TextView displayOrderId,redirectingTxt;
    Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_success_page);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Booking Confirmation");

        gotoHomeButton = (ImageButton) findViewById(R.id.gotoHomeButton);
        displayOrderId = (TextView) findViewById(R.id.bookingIdTxt);
        redirectingTxt = (TextView) findViewById(R.id.redirectingTxt);

        Bundle bundle = getIntent().getExtras();
        String bookingId = bundle.getString("bookingId");
        displayOrderId.setText(bookingId);

        String srmdyear = bundle.getString("rmdyear");
        String srmdmonth = bundle.getString("rmdmonth");
        String srmdday = bundle.getString("rmdday");
        String srmdhour = bundle.getString("rmdhour");
        String srmdmint = bundle.getString("rmdmint");
        String desc = bundle.getString("descr");

        int isrmdyear = Integer.valueOf(srmdyear);
        int isrmdmonth = Integer.valueOf(srmdmonth) -1;
        int isrmdday = Integer.valueOf(srmdday);
        int isrmdhour = Integer.valueOf(srmdhour);
        int isrmdmint = Integer.valueOf(srmdmint);

        final long halfanHour = 1000 * 60 * 30;
        final long oneHour = 1000 * 60 * 60;
        final long tenMinutes = 1000 * 60 * 1;

        long calID = 3;
        long startMillis = 0;
        long endMillis = 0;

        Calendar beginTime = Calendar.getInstance();
        beginTime.set(isrmdyear, isrmdmonth, isrmdday, isrmdhour, isrmdmint);
        startMillis = beginTime.getTimeInMillis() - oneHour;
        Calendar endTime = Calendar.getInstance();

        endMillis = startMillis+halfanHour+oneHour;

        TimeZone tz = TimeZone.getDefault();

        ContentResolver cr = getContentResolver();

        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, startMillis);
        values.put(CalendarContract.Events.DTEND, endMillis);
        values.put(CalendarContract.Events.TITLE, "My HealthApp Reminder");
        values.put(CalendarContract.Events.DESCRIPTION, desc);
        values.put(CalendarContract.Events.CALENDAR_ID, calID);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, tz.getID());

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED)
        {

            return;
        }
        if (android.os.Build.VERSION.SDK_INT <= 7 )
        {
            uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
        }

        Uri.Builder builder = CalendarContract.CONTENT_URI.buildUpon();
        builder.appendPath("time");
        ContentUris.appendId(builder, startMillis);

        gotoHomeButton.setOnClickListener(this);
        redirectingTxt.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        if(v == gotoHomeButton)
        {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }

        if(v == redirectingTxt)
        {
            redirectingTxt.setTextColor(Color.parseColor("#43A047"));
            Intent i = new Intent(getApplicationContext(), OrdersList.class);
            startActivity(i);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
