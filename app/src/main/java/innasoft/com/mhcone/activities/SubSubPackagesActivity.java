package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.SubPackagesAdapter;
import innasoft.com.mhcone.adapters.SubSubPackagesAdapter;
import innasoft.com.mhcone.holders.SubSubPackagesHolder;
import innasoft.com.mhcone.models.PackagesModel;
import innasoft.com.mhcone.models.SubPackagesModel;
import innasoft.com.mhcone.models.SubSubPackagesModel;
import innasoft.com.mhcone.utilities.AppUrls;

public class SubSubPackagesActivity extends AppCompatActivity implements SubSubPackagesHolder.MyViewHolder{
    ImageButton closeButton;
    SubPackagesModel subPackagesModel;
    TextView display_sub_package_name;
    Typeface typeface,typeface2;
    ProgressDialog progressDialog;
    ArrayList<SubSubPackagesModel> subSubPackageModel;
    SubSubPackagesAdapter adapter;
    RecyclerView subSubPackagerecyclerView;
    String sub_package_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.sendbird_slide_in_from_bottom, R.anim.sendbird_slide_out_to_top);
        setContentView(R.layout.activity_sub_sub_packages);

        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typeface2 = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Bundle bundle = getIntent().getExtras();
        subPackagesModel = (SubPackagesModel) bundle.getSerializable("sub_package_id");

        display_sub_package_name = (TextView) findViewById(R.id.txt_sub_package_name);
        closeButton = (ImageButton) findViewById(R.id.sub_sub_pakg_btn_close);

        display_sub_package_name.setTypeface(typeface);
        display_sub_package_name.setText(subPackagesModel.getSub_package_name());

        sub_package_id = subPackagesModel.getSub_package_id();




        subSubPackageModel = new ArrayList<SubSubPackagesModel>();
        subSubPackagerecyclerView = (RecyclerView) findViewById(R.id.sub_sub_package_recycler_view);
        subSubPackagerecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        subSubPackagerecyclerView.setLayoutManager(layoutManager);
        adapter = new SubSubPackagesAdapter(subSubPackageModel, SubSubPackagesActivity.this, R.layout.row_sub_sub_packages_items);
        gettingSubSubPackagesList();


        closeButton = (ImageButton) findViewById(R.id.sub_sub_pakg_btn_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });
    }

    private void gettingSubSubPackagesList() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.SUB_SUB_PACKAGES_URL+sub_package_id,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        subSubPackageModel.clear();


                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0 ; i < jsonArray.length(); i++)
                            {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("package-sub-super");
                                SubSubPackagesModel sspm = new SubSubPackagesModel();
                                sspm.setSub_sub_pack_id(jsonObject1.getString("id"));
                                sspm.setPack_id(jsonObject1.getString("package_id"));
                                sspm.setSub_sub_pack_name(jsonObject1.getString("package_sub_super_name"));
                                sspm.setSub_pack_name(jsonObject1.getString("package_sub_name"));
                                sspm.setSub_sub_pack_status(jsonObject1.getString("status"));
                                sspm.setPack_name(jsonObject1.getString("package_name"));
                                subSubPackageModel.add(sspm);
                            }
                            subSubPackagerecyclerView.setAdapter(adapter);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {


                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }

                    }
                }
        ){

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void cardViewListener(int position) {


    }
}
