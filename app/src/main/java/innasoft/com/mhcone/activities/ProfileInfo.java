package innasoft.com.mhcone.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class ProfileInfo extends AppCompatActivity implements View.OnClickListener {
    EditText et_update_name;
    Button submit_update_bt,cancel_update_bt;
    RadioGroup update_genderGroup;
    RadioButton update_maleRadioButton,update_femaleRadioButtion,update_genderStatus;
    String p_name,p_age,p_gunder,p_mail,p_phone;
    UserSessionManager userSessionManager;
    String userID;
    String gunderValue;
    ProgressDialog progressDialog;
    public int mYear, mMonth, mDay;
    TextView et_update_phoneNumber,dispdob,tittle,et_update_email;
    ImageView dobPicker;
    Typeface typeface;
    TextInputLayout update_name_til,update_email_til,update_phone_til;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_info);

        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");

        progressDialog = new ProgressDialog(ProfileInfo.this,R.style.DialogTheme);

        progressDialog.setMessage("Registering......");

        dispdob = (TextView) findViewById(R.id.dobdisp);
        dobPicker = (ImageView) findViewById(R.id.dobPicker);

        tittle = (TextView) findViewById(R.id.reg_activity_headding);
        tittle.setTypeface(typeface);
        update_name_til = (TextInputLayout) findViewById(R.id.update_name_til);
        update_name_til.setTypeface(typeface);

        update_email_til = (TextInputLayout) findViewById(R.id.update_email_til);
        update_email_til.setTypeface(typeface);
        update_phone_til = (TextInputLayout) findViewById(R.id.update_phone_til);
        update_phone_til.setTypeface(typeface);

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = userSessionManager.getUserDetails();

        userID = user.get(UserSessionManager.USER_ID);

        Bundle bundle = getIntent().getExtras();

        p_name = bundle.getString("pName");
        p_age = bundle.getString("pAge");
        p_gunder = bundle.getString("pGunder");
        p_mail = bundle.getString("pMail");
        p_phone = bundle.getString("pPhone");

        et_update_name = (EditText) findViewById(R.id.update_name_et);
        et_update_name.setTypeface(typeface);

        et_update_email = (TextView) findViewById(R.id.update_email_et);
        et_update_email.setTypeface(typeface);
        et_update_phoneNumber = (TextView) findViewById(R.id.update_phone_number_et);
        et_update_phoneNumber.setTypeface(typeface);

        update_femaleRadioButtion = (RadioButton) findViewById(R.id.update_female_radio);
        update_femaleRadioButtion.setTypeface(typeface);
        update_maleRadioButton = (RadioButton) findViewById(R.id.update_male_radio);
        update_maleRadioButton.setTypeface(typeface);
        update_genderGroup = (RadioGroup) findViewById(R.id.update_gender_id);

        et_update_name.setText(p_name);
        dispdob.setText(p_age);
        et_update_email.setText(p_mail);
        et_update_phoneNumber.setText(p_phone);


        if(p_gunder.equals("male"))
        {
            update_maleRadioButton.setChecked(true);
        }
        if(p_gunder.equals("female"))
        {
            update_femaleRadioButtion.setChecked(true);
        }

        submit_update_bt = (Button) findViewById(R.id.submit_update);
        submit_update_bt.setTypeface(typeface);
        cancel_update_bt = (Button) findViewById(R.id.cancel_update);
        cancel_update_bt.setTypeface(typeface);

        submit_update_bt.setOnClickListener(this);
        cancel_update_bt.setOnClickListener(this);

        dobPicker.setOnClickListener(this);
        dispdob.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (view == dispdob)
        {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {


                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            dispdob.setText(String.valueOf(dayOfMonth+" - "+(monthOfYear+1)+" - "+year));
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            Date birthDate = null; //Yeh !! It's my date of birth :-)
                            try {
                                birthDate = sdf.parse(dayOfMonth+"/"+(monthOfYear+1)+"/"+year);

                                String age = calculateAge(birthDate);

                                String segments[] = age.split(",");
                                if(segments[0].equals("0"))
                                {
                                    if(segments[1].equals("0"))
                                    {
                                        if(segments[2].equals("0"))
                                        {

                                        }
                                    }
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }

                    }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }
        if (view == dobPicker)
        {

            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            dispdob.setText(String.valueOf(dayOfMonth+" - "+(monthOfYear+1)+" - "+year));
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            Date birthDate = null;
                            try {
                                birthDate = sdf.parse(dayOfMonth+"/"+(monthOfYear+1)+"/"+year);

                                String age = calculateAge(birthDate);

                                String segments[] = age.split(",");
                                if(segments[0].equals("0"))
                                {
                                    if(segments[1].equals("0"))
                                    {
                                        if(segments[2].equals("0"))
                                        {

                                        }
                                    }
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }

        if(view == submit_update_bt)
        {

            if(validateData()) {

                if(dispdob.getText().toString().trim().length() != 0 && !dispdob.getText().toString().equals(null)) {


                    progressDialog.show();

                    if (update_maleRadioButton.isChecked() || update_femaleRadioButtion.isChecked()) {
                        update_genderStatus = (RadioButton) findViewById(update_genderGroup.getCheckedRadioButtonId());

                        if (update_genderStatus.getId() == update_maleRadioButton.getId()) {
                            gunderValue = "male";


                        }
                        if (update_genderStatus.getId() == update_femaleRadioButtion.getId()) {
                            gunderValue = "female";

                        }
                    }
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.UPDATE_USER_PROFILE_URL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                    try {
                                        progressDialog.dismiss();
                                        JSONObject jsonObject = new JSONObject(response);
                                        String successResponceCode = jsonObject.getString("status");
                                        if (successResponceCode.equals("20100")) {
                                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                            String message = jsonObject1.getString("msg");

                                            Toast.makeText(getApplicationContext(), "Your profile has been successfully updated", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                            startActivity(intent);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    String responseBody = null;
                                    progressDialog.dismiss();


                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    } else if (error instanceof AuthFailureError) {

                                    } else if (error instanceof ServerError) {

                                    } else if (error instanceof NetworkError) {

                                    } else if (error instanceof ParseError) {

                                    }
                                }
                            }

                    ) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();


                            params.put("user_id", userID);
                            params.put("user_name", et_update_name.getText().toString().trim());
                            params.put("dob", dispdob.getText().toString().trim());
                            params.put("email", et_update_email.getText().toString().trim());
                            params.put("mobile", et_update_phoneNumber.getText().toString().trim());
                            params.put("gender", gunderValue);
                            return params;
                        }
                    };
                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);
                }else {
                    Toast.makeText(getApplicationContext(), "Please provide Date of Birth..!!", Toast.LENGTH_SHORT).show();
                }
            }
        }
        if(view == cancel_update_bt)
        {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }
    }

    private String calculateAge(Date birthDate) {

        int years = 0;
        int months = 0;
        int days = 0;

        Calendar birthDay = Calendar.getInstance();
        birthDay.setTimeInMillis(birthDate.getTime());

        long currentTime = System.currentTimeMillis();
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(currentTime);

        years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
        int currMonth = now.get(Calendar.MONTH) + 1;
        int birthMonth = birthDay.get(Calendar.MONTH) + 1;

        months = currMonth - birthMonth;

        if (months < 0) {
            years--;
            months = 12 - birthMonth + currMonth;
            if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                months--;
        } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            years--;
            months = 11;
        }

        if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
            days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
        else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            int today = now.get(Calendar.DAY_OF_MONTH);
            now.add(Calendar.MONTH, -1);
            days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
        } else {
            days = 0;
            if (months == 12) {
                years++;
                months = 0;
            }
        }
        return years+","+months+","+days;

    }

    private boolean validateData() {
        boolean result = true;

        String name = et_update_name.getText().toString().trim();
        if (name == null || name.length() < 3) {

            update_name_til.setError(getString(R.string.invalidName));
            result = false;
        }
        else
            update_name_til.setErrorEnabled(false);

        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        String email = et_update_email.getText().toString().trim();
        if (!email.matches(EMAIL_REGEX)) {
            update_email_til.setError(getString(R.string.invalidEmailID));
            result = false;
        }
        else
            update_email_til.setErrorEnabled(false);

        String phone = et_update_phoneNumber.getText().toString().trim();
        if ((phone == null || phone.equals("")) || phone.length() != 10) {
            update_phone_til.setError(getString(R.string.invalidNumber));
            result = false;
        }
        else
            update_phone_til.setErrorEnabled(false);

        return result;
    }
}
