package innasoft.com.mhcone.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.ShowLabsPackageAdapter;
import innasoft.com.mhcone.adapters.ShowLabsPackageCitiesAdapter;
import innasoft.com.mhcone.models.CitiesModel;
import innasoft.com.mhcone.models.ShowLabsPackageModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.DividerItemDecoration;
import innasoft.com.mhcone.utilities.GPSTrackers;
import innasoft.com.mhcone.utilities.StartLocationAlert;
import innasoft.com.mhcone.utilities.UserSessionManager;
import innasoft.com.mhcone.utilities.VerticalSpaceItemDecoration;

public class ShowLabsPackage extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private static final int SHOW_LABS_REQ_CODE = 27;
    ArrayList<ShowLabsPackageModel> showlabspackageList;
    ShowLabsPackageAdapter adapter;
    RecyclerView showlabspackagerecyclerview;
    Button btnList, btnGrid;
    int flag;
    String package_id, package_name;
    String subtestNames;
    GPSTrackers gps;
    StartLocationAlert startLocationAlert;
    ImageButton closeShowLabsPackgesBt;
    String latitude, longitude;
    ProgressDialog progressDialog;
    private Typeface type_lato, type_lato2;
    SearchView sv;
    AlertDialog dialog;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    ArrayList<String> labsListArrayList = new ArrayList<String>();
    RelativeLayout yourpackageRL;
    ImageView yourpackageLocation, no_labs_found_iv;
    TextView yourpackageCity;
    UserSessionManager session;

    /*Current Address*/
    Geocoder geocoder;
    List<Address> cuurentaddresses;
    String address = null;
    String city;
    String state;
    String country;
    String pincode;
    double latitude1, longitude1;
    int first_time_flag = 0;

    TextView list_of_labs_Package, noInternetConnect;
    int filterFlag = 0;
    private FloatingActionButton fabPackage;
    String bridgepackTestandSubIDswithformat, subTestIdsList;

    private static final int VERTICAL_ITEM_SPACE = 0;

    ArrayList<String> packagesTestIdsList = new ArrayList<String>();
    String filterString;

    ArrayList<CitiesModel> citiesModelArrayList = new ArrayList<CitiesModel>();

    ShowLabsPackageCitiesAdapter citiesAdapter;

    AlertDialog city_dialog;
    public String global_lat, global_lng;
    int fabaction_status = 0;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_labs_package);

        type_lato = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        type_lato2 = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        session = new UserSessionManager(getApplicationContext());

        noInternetConnect = (TextView) findViewById(R.id.noInternetConnect);
        noInternetConnect.setTypeface(type_lato);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.show_labs_package_swipe_layout);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,

                android.R.color.holo_green_light,

                android.R.color.holo_orange_light,

                android.R.color.holo_red_light);

        Bundle bundle = getIntent().getExtras();

        if (bundle.size() != 0) {
            package_id = bundle.getString("packageId");
            package_name = bundle.getString("packageName");
            subtestNames = bundle.getString("subtestNames");
            bridgepackTestandSubIDswithformat = bundle.getString("packTestandSubIDswithformat");
            subTestIdsList = bundle.getString("subTestIdsList");
            Log.d("SHIOLABLASTWEEK", subTestIdsList);
        }

        gps = new GPSTrackers(ShowLabsPackage.this);
        sv = (SearchView) findViewById(R.id.mSearchlabs_packages);
        sv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sv.setIconified(false);
            }
        });

        sv.clearFocus();

        fabPackage = (FloatingActionButton) findViewById(R.id.filter_package);
        fabPackage.setOnClickListener(this);

        yourpackageRL = (RelativeLayout) findViewById(R.id.yourpackage_rl);
        yourpackageRL.setOnClickListener(this);

        yourpackageLocation = (ImageView) findViewById(R.id.yourpackage_location);
        yourpackageLocation.setOnClickListener(this);
        no_labs_found_iv = (ImageView) findViewById(R.id.no_labs_found_iv);

        yourpackageCity = (TextView) findViewById(R.id.yourpackage_city);
        yourpackageCity.setOnClickListener(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        list_of_labs_Package = (TextView) findViewById(R.id.txt_package_name);
        list_of_labs_Package.setTypeface(type_lato2);

        showlabspackageList = new ArrayList<ShowLabsPackageModel>();
        showlabspackagerecyclerview = (RecyclerView) findViewById(R.id.showlabspackage_recycler_view);
        showlabspackagerecyclerview.setHasFixedSize(true);

        showlabspackagerecyclerview.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));

        showlabspackagerecyclerview.addItemDecoration(new DividerItemDecoration(getApplicationContext()));

        showlabspackagerecyclerview.addItemDecoration(
                new DividerItemDecoration(getApplicationContext(), R.drawable.recycler_divider));

        closeShowLabsPackgesBt = (ImageButton) findViewById(R.id.sub_pakgshow_lab_btn_close);
        if (flag == 0) {
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            showlabspackagerecyclerview.setLayoutManager(layoutManager);
            adapter = new ShowLabsPackageAdapter(package_id, package_name, subtestNames, bridgepackTestandSubIDswithformat, showlabspackageList,
                    ShowLabsPackage.this, R.layout.row_labs_package3, subTestIdsList);
            if (gps.canGetLocation()) {
                latitude1 = gps.getLatitude();
                longitude1 = gps.getLongitude();
                latitude = Double.toString(latitude1);
                longitude = Double.toString(longitude1);
                geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

                try {
                    cuurentaddresses = geocoder.getFromLocation(latitude1, longitude1, 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (cuurentaddresses.size() != 0) {
                    city = cuurentaddresses.get(0).getLocality();
                    yourpackageCity.setText(city);
                }

                if (filterFlag == 0) {
                    String filterString = "distance_in_km asc";
                    getLabsTest(filterString);
                }
            } else {

                startLocationAlert = new StartLocationAlert(ShowLabsPackage.this);
            }
        } else if (flag == 1) {
            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 3);
            showlabspackagerecyclerview.setLayoutManager(layoutManager);
            adapter = new ShowLabsPackageAdapter(package_id, package_name, subtestNames, bridgepackTestandSubIDswithformat, showlabspackageList, ShowLabsPackage.this, R.layout.row_labs_package3, subTestIdsList);
            if (gps.canGetLocation()) {

                if (filterFlag == 0) {
                    filterString = "distance_in_km asc";
                    getLabsTest(filterString);
                }

            } else {
                startLocationAlert = new StartLocationAlert(ShowLabsPackage.this);
            }
        }


        closeShowLabsPackgesBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                if(session.checkLogin() != false) {

//                    Intent i = new Intent(ShowLabsPackage.this, StaticActivity.class);
//                    startActivity(i);
//                    finish();
//                }
//                else {
//                    Intent i = new Intent(ShowLabsPackage.this, MainActivity.class);
//                    startActivity(i);
                finish();
//                }

            }
        });

        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                adapter.getFilter().filter(query);
                return false;
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    private void getLabsTest(final String filterValue) {

        showlabspackageList.clear();
        noInternetConnect.setVisibility(View.GONE);
        no_labs_found_iv.setVisibility(View.GONE);
        ShowLabsPackage.this.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.show();

            }
        });
        Log.d("UUU", AppUrls.GETTING_LABS_FOR_PACKAGES);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_LABS_FOR_PACKAGES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("hhh", "jjjj");
                showlabspackageList.clear();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");

                            ShowLabsPackageModel slpm = new ShowLabsPackageModel();

                            slpm.setShow_lab_package_id(jsonObject1.getString("id"));
                            slpm.setShow_lab_package_user_name(jsonObject1.getString("user_name"));
                            slpm.setShow_lab_package_email(jsonObject1.getString("email"));
                            slpm.setShow_lab_package_mobile(jsonObject1.getString("mobile"));
                            slpm.setShow_lab_package_role_id(jsonObject1.getString("role_id"));
                            slpm.setShow_lab_package_created_time(jsonObject1.getString("created_time"));
                            slpm.setCertificate(jsonObject1.getString("certificate"));
                            slpm.setProfile_pic_url(AppUrls.IMAGE_URL + jsonObject1.getString("profile_pic"));
                            slpm.setLocation(jsonObject1.getString("location"));
                            if (jsonObject1.getString("rating").equals("null")) {
                                slpm.setShow_lab_ratting("0");
                            } else {
                                slpm.setShow_lab_ratting(jsonObject1.getString("rating"));
                            }
                            slpm.setShow_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            slpm.setShow_lab_payment_online(jsonObject1.getString("payment_online"));
                            slpm.setShow_lab_tele_booking(jsonObject1.getString("tele_booking"));


                            slpm.setShow_lab_package_workingdays(jsonObject1.getString("workingdays"));
                            slpm.setShow_lab_package_address(jsonObject1.getString("address"));
                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String workingDayfromServer = jsonObject1.getString("current_week_name");
                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];
                            if (conditionCheck.equals("1")) {
                                slpm.setShow_lab_package_workingtime("OpenToday:" + segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (conditionCheck.equals("0")) {
                                slpm.setShow_lab_package_workingtime("Today lab Close");
                            }
                            slpm.setShow_lab_package_lab_price(jsonObject1.getString("lab_price"));


                            slpm.setShow_lab_package_final_price(jsonObject1.getString("final_price"));

                            slpm.setShow_lab_package_lat(jsonObject1.getString("lat"));
                            slpm.setShow_lab_package_lang(jsonObject1.getString("lang"));
                            slpm.setShow_lab_package_home_collections(jsonObject1.getString("home_collections"));
                            slpm.setShow_lab_package_test_id(jsonObject1.getString("package_sub_id"));
                            slpm.setShow_lab_package_package_id(jsonObject1.getString("package_id"));
                            slpm.setShow_lab_package_distance(jsonObject1.getString("distance_in_km"));

                            showlabspackageList.add(slpm);
                            fabaction_status = 0;

                        }
                        showlabspackagerecyclerview.setAdapter(adapter);

                        ShowLabsPackage.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                    } else {
                        ShowLabsPackage.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                        fabaction_status = 1;
                        no_labs_found_iv.setVisibility(View.VISIBLE);
                        noInternetConnect.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnect.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);


                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {


                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("package_id", package_id);


                if (first_time_flag == 1) {
                    Log.d("LABFINDSENDVALUES", global_lat + "\n" + global_lng + "\n" + filterValue);
                    params.put("lat", global_lat);
                    params.put("lang", global_lng);
                } else {
                    Log.d("LABFINDSENDVALUES", latitude + "\n" + longitude + "\n" + filterValue);
                    params.put("lat", latitude);
                    params.put("lang", longitude);
                }

                params.put("order_type", filterValue);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }


    private void getLabsTest(final String filterValue, final String packId, final String lat, final String lng) {
        showlabspackageList.clear();
        noInternetConnect.setVisibility(View.GONE);
        no_labs_found_iv.setVisibility(View.GONE);
        ShowLabsPackage.this.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.show();

            }
        });
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_LABS_FOR_PACKAGES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                showlabspackageList.clear();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");

                            ShowLabsPackageModel slpm = new ShowLabsPackageModel();

                            slpm.setShow_lab_package_id(jsonObject1.getString("id"));
                            slpm.setShow_lab_package_user_name(jsonObject1.getString("user_name"));
                            slpm.setShow_lab_package_email(jsonObject1.getString("email"));
                            slpm.setShow_lab_package_mobile(jsonObject1.getString("mobile"));
                            slpm.setShow_lab_package_role_id(jsonObject1.getString("role_id"));
                            slpm.setShow_lab_package_created_time(jsonObject1.getString("created_time"));
                            slpm.setShow_lab_package_workingdays(jsonObject1.getString("workingdays"));
                            slpm.setShow_lab_package_address(jsonObject1.getString("address"));
                            slpm.setCertificate(jsonObject1.getString("certificate"));
                            slpm.setProfile_pic_url(jsonObject1.getString("profile_pic"));
                            slpm.setLocation(jsonObject1.getString("location"));
                            if (jsonObject1.getString("rating").equals("null")) {
                                slpm.setShow_lab_ratting("0");
                            } else {
                                slpm.setShow_lab_ratting(jsonObject1.getString("rating"));
                            }
                            slpm.setShow_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            slpm.setShow_lab_payment_online(jsonObject1.getString("payment_online"));
                            slpm.setShow_lab_tele_booking(jsonObject1.getString("tele_booking"));

                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String workingDayfromServer = jsonObject1.getString("current_week_name");
                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];
                            if (conditionCheck.equals("1")) {
                                slpm.setShow_lab_package_workingtime("OpenToday:" + segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (conditionCheck.equals("0")) {
                                slpm.setShow_lab_package_workingtime("Today lab Close");
                            }
                            slpm.setShow_lab_package_lab_price(jsonObject1.getString("lab_price"));


                            slpm.setShow_lab_package_final_price(jsonObject1.getString("final_price"));

                            slpm.setShow_lab_package_lat(jsonObject1.getString("lat"));
                            slpm.setShow_lab_package_lang(jsonObject1.getString("lang"));
                            slpm.setShow_lab_package_home_collections(jsonObject1.getString("home_collections"));
                            slpm.setShow_lab_package_test_id(jsonObject1.getString("package_sub_id"));
                            slpm.setShow_lab_package_package_id(jsonObject1.getString("package_id"));
                            slpm.setShow_lab_package_distance(jsonObject1.getString("distance_in_km"));

                            showlabspackageList.add(slpm);
                            fabaction_status = 0;

                        }
                        showlabspackagerecyclerview.setAdapter(adapter);

                        ShowLabsPackage.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                    } else {
                        ShowLabsPackage.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                        showlabspackageList.clear();
                        fabaction_status = 1;
                        no_labs_found_iv.setVisibility(View.VISIBLE);
                        noInternetConnect.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnect.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                String responseBody = null;


                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {


                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("package_id", packId);
                if (first_time_flag == 1) {
                    Log.d("LABFINDSENDVALUES", global_lat + "\n" + global_lng + "\n" + filterValue);
                    params.put("lat", global_lat);
                    params.put("lang", global_lng);
                } else {
                    Log.d("LABFINDSENDVALUES", lat + "\n" + lng + "\n" + filterValue);
                    params.put("lat", lat);
                    params.put("lang", lng);
                }
                params.put("order_type", filterValue);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }


    private void getLabsTest(final String filterValue, final String packId, final String lat, final String lng, final String value2) {
        showlabspackageList.clear();
        noInternetConnect.setVisibility(View.INVISIBLE);
        ShowLabsPackage.this.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.show();

            }
        });
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_LABS_FOR_PACKAGES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showlabspackageList.clear();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");

                            ShowLabsPackageModel slpm = new ShowLabsPackageModel();

                            slpm.setShow_lab_package_id(jsonObject1.getString("id"));
                            slpm.setShow_lab_package_user_name(jsonObject1.getString("user_name"));
                            slpm.setShow_lab_package_email(jsonObject1.getString("email"));
                            slpm.setShow_lab_package_mobile(jsonObject1.getString("mobile"));
                            slpm.setShow_lab_package_role_id(jsonObject1.getString("role_id"));
                            slpm.setShow_lab_package_created_time(jsonObject1.getString("created_time"));
                            slpm.setShow_lab_package_workingdays(jsonObject1.getString("workingdays"));
                            slpm.setCertificate(jsonObject1.getString("certificate"));
                            slpm.setShow_lab_package_address(jsonObject1.getString("address"));
                            slpm.setProfile_pic_url(jsonObject1.getString("profile_pic"));
                            slpm.setLocation(jsonObject1.getString("location"));
                            if (jsonObject1.getString("rating").equals("null")) {
                                slpm.setShow_lab_ratting("0");
                            } else {
                                slpm.setShow_lab_ratting(jsonObject1.getString("rating"));
                            }
                            slpm.setShow_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            slpm.setShow_lab_payment_online(jsonObject1.getString("payment_online"));
                            slpm.setShow_lab_tele_booking(jsonObject1.getString("tele_booking"));

                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String workingDayfromServer = jsonObject1.getString("current_week_name");
                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];
                            if (conditionCheck.equals("1")) {
                                slpm.setShow_lab_package_workingtime("OpenToday:" + segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (conditionCheck.equals("0")) {
                                slpm.setShow_lab_package_workingtime("Today lab Close");
                            }
                            slpm.setShow_lab_package_lab_price(jsonObject1.getString("lab_price"));


                            slpm.setShow_lab_package_final_price(jsonObject1.getString("final_price"));

                            slpm.setShow_lab_package_lat(jsonObject1.getString("lat"));
                            slpm.setShow_lab_package_lang(jsonObject1.getString("lang"));
                            slpm.setShow_lab_package_home_collections(jsonObject1.getString("home_collections"));
                            slpm.setShow_lab_package_test_id(jsonObject1.getString("package_sub_id"));
                            slpm.setShow_lab_package_package_id(jsonObject1.getString("package_id"));
                            slpm.setShow_lab_package_distance(jsonObject1.getString("distance_in_km"));


                            showlabspackageList.add(slpm);
                            fabaction_status = 0;

                        }
                        showlabspackagerecyclerview.setAdapter(adapter);

                        ShowLabsPackage.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                    } else {
                        ShowLabsPackage.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                        fabaction_status = 1;
                        noInternetConnect.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnect.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                String responseBody = null;


                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {


                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("package_id", packId);
                if (first_time_flag == 1) {
                    Log.d("LABFINDSENDVALUES", global_lat + "\n" + global_lng + "\n" + filterValue);
                    params.put("lat", global_lat);
                    params.put("lang", global_lng);
                } else {
                    Log.d("LABFINDSENDVALUES", lat + "\n" + lng + "\n" + filterValue);
                    params.put("lat", lat);
                    params.put("lang", lng);
                }
                params.put("order_type", filterValue);
                params.put("home_collections", value2);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }


    public void cardViewListener(int position) {

        String selectedlabId = showlabspackageList.get(position).getShow_lab_package_id();
        String selectedLabName = showlabspackageList.get(position).getShow_lab_package_user_name();
        String selectedLabAddress = showlabspackageList.get(position).getShow_lab_package_address();
        String selectedLabPrice = showlabspackageList.get(position).getShow_lab_package_final_price();

        Intent intent = new Intent(ShowLabsPackage.this, BillingPageForPackageBooking.class);
        intent.putExtra("selectedLabPhone", showlabspackageList.get(position).getShow_lab_package_mobile());
        intent.putExtra("selecteLabId", selectedlabId);
        intent.putExtra("packageId", package_id);


        intent.putExtra("payment_lab", showlabspackageList.get(position).getShow_lab_payment_lab());
        intent.putExtra("payment_online", showlabspackageList.get(position).getShow_lab_payment_online());
        intent.putExtra("tele_booking", showlabspackageList.get(position).getShow_lab_tele_booking());
        intent.putExtra("subTestName", package_name);
        intent.putExtra("subTestsNAMES", subtestNames);
        intent.putExtra("selectedLabPrice", selectedLabPrice);
        intent.putExtra("selectedLabName", selectedLabName);
        intent.putExtra("selectedLabAddress", selectedLabAddress);  //packageName,selectedLabPrice
        intent.putExtra("packTestandSubIDswithformat", bridgepackTestandSubIDswithformat);
        intent.putExtra("subTestIdsList", subTestIdsList);
        Log.d("SHOWLASSDFAFAFSADFAf", subTestIdsList);

        startActivity(intent);

    }

    @Override
    public void onClick(View v) {
        if (v == btnList) {
            flag = 0;

            Intent intent = new Intent(getApplicationContext(), ShowLabsPackage.class);
            startActivity(intent);

        } else if (v == btnGrid) {
            flag = 1;
            Intent intent = new Intent(getApplicationContext(), ShowLabsPackage.class);
            startActivity(intent);
        }


        if (v == fabPackage) {
            if (fabaction_status == 0) {

                AlertDialog.Builder builder = new AlertDialog.Builder(ShowLabsPackage.this);
                LayoutInflater inflater = getLayoutInflater();

                View dialog_layout = inflater.inflate(R.layout.custom_dialog_filter, null);

                TextView dialog_title = (TextView) dialog_layout.findViewById(R.id.dialog_title);
                dialog_title.setTypeface(type_lato);

                TextView dlh_txt = (TextView) dialog_layout.findViewById(R.id.dlh_txt);
                dlh_txt.setTypeface(type_lato);

                final String cityName = yourpackageCity.getText().toString();

                dlh_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "distance_in_km asc";
                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilter(vlaue, package_id, global_lat, global_lng, cityName);
                        } else {
                            gettingLabswithCityNamewithFilter(vlaue, package_id, latitude, longitude, cityName);
                        }
                        dialog.dismiss();
                    }
                });

                TextView dhl_txt = (TextView) dialog_layout.findViewById(R.id.dhl_txt);
                dhl_txt.setTypeface(type_lato);
                dhl_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "distance_in_km desc";
                        if (first_time_flag == 1) {
                            getLabsTest(vlaue, package_id, global_lat, global_lng);
                        } else {
                            getLabsTest(vlaue, package_id, latitude, longitude);
                        }
                        dialog.dismiss();
                    }
                });

                TextView plh_txt = (TextView) dialog_layout.findViewById(R.id.plh_txt);
                plh_txt.setTypeface(type_lato);
                plh_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "lab_package.final_price asc";
                        if (first_time_flag == 1) {
                            getLabsTest(vlaue, package_id, global_lat, global_lng);
                        } else {
                            getLabsTest(vlaue, package_id, latitude, longitude);
                        }
                        dialog.dismiss();
                    }
                });

                TextView phl_txt = (TextView) dialog_layout.findViewById(R.id.phl_txt);
                phl_txt.setTypeface(type_lato);
                phl_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "lab_package.final_price desc";
                        if (first_time_flag == 1) {
                            getLabsTest(vlaue, package_id, global_lat, global_lng);
                        } else {
                            getLabsTest(vlaue, package_id, latitude, longitude);
                        }
                        dialog.dismiss();
                    }
                });

                TextView naz_txt = (TextView) dialog_layout.findViewById(R.id.naz_txt);
                naz_txt.setTypeface(type_lato);
                naz_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "users.user_name asc";
                        if (first_time_flag == 1) {
                            getLabsTest(vlaue, package_id, global_lat, global_lng);
                        } else {
                            getLabsTest(vlaue, package_id, latitude, longitude);
                        }
                        dialog.dismiss();
                    }
                });


                TextView nza_txt = (TextView) dialog_layout.findViewById(R.id.nza_txt);
                nza_txt.setTypeface(type_lato);
                nza_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "users.user_name desc";
                        if (first_time_flag == 1) {
                            getLabsTest(vlaue, package_id, global_lat, global_lng);
                        } else {
                            getLabsTest(vlaue, package_id, latitude, longitude);
                        }
                        dialog.dismiss();
                    }
                });

                TextView hc_txt = (TextView) dialog_layout.findViewById(R.id.hc_txt);
                hc_txt.setTypeface(type_lato);
                hc_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "distance_in_km asc";
                        String value2 = "lab_users.home_collections = 1";

                        if (first_time_flag == 1) {
                            gettingLabswithCityNamewithFilterTwo(vlaue, package_id, global_lat, global_lng, value2, cityName);
                        } else {
                            gettingLabswithCityNamewithFilterTwo(vlaue, package_id, latitude, longitude, value2, cityName);
                        }
                        dialog.dismiss();
                    }
                });

                TextView rhl_txt = (TextView) dialog_layout.findViewById(R.id.rhl_txt);
                rhl_txt.setTypeface(type_lato);
                rhl_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "rating desc";
                        if (first_time_flag == 1) {
                            getLabsTest(vlaue, package_id, global_lat, global_lng);
                        } else {
                            getLabsTest(vlaue, package_id, latitude, longitude);
                        }
                        dialog.dismiss();
                    }
                });


                TextView rlh_txt = (TextView) dialog_layout.findViewById(R.id.rlh_txt);
                rlh_txt.setTypeface(type_lato);
                rlh_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "rating asc";
                        if (first_time_flag == 1) {
                            getLabsTest(vlaue, package_id, global_lat, global_lng);
                        } else {
                            getLabsTest(vlaue, package_id, latitude, longitude);
                        }
                        dialog.dismiss();
                    }
                });


                TextView nabl_txt = (TextView) dialog_layout.findViewById(R.id.nabl_txt);
                nabl_txt.setTypeface(type_lato);
                nabl_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String vlaue = "distance_in_km asc";
                        String value2 = "lab_users.certificate = 1";
                        gettingLabswithCityNamewithFilterTwo(vlaue, package_id, latitude, longitude, value2, cityName);
                        dialog.dismiss();
                    }
                });


                builder.setView(dialog_layout)
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });

                dialog = builder.create();
                dialog.show();




                /*showlabspackageList.clear();
                no_labs_found_iv.setVisibility(View.GONE);
                final String[] country = {"Distance Low to High", "Distance High to Low", "Names A to Z", "Names Z to A", "Price Low to High"
                        , "Price High to Low", "Home Collection", "Ratting High to Low", "Ratting Low to High", "NABL/CAP Accreditation"};
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Choose One")
                        .setItems(country, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                String selectedCountry = country[i];
                                String cityName = yourpackageCity.getText().toString();
                                if (selectedCountry.equals("Home Collection")) {
                                    String vlaue = "distance_in_km asc";
                                    String value2 = "lab_users.home_collections = 1";
                                   // gettingLabswithCityNamewithFilterTwo(vlaue, package_id, latitude, longitude, value2, cityName);
                                    if (first_time_flag == 1) {
                                        //gettingLabswithCityNamewithFilter(vlaue, sub_testId, global_lat, global_lng , cityName);
                                        gettingLabswithCityNamewithFilterTwo(vlaue,  package_id, global_lat, global_lng, value2, cityName);
                                    } else {
                                        //gettingLabswithCityNamewithFilter(vlaue, sub_testId, lat, lng , cityName);
                                        gettingLabswithCityNamewithFilterTwo(vlaue, package_id, latitude, longitude, value2, cityName);
                                    }
                                }
                                if (selectedCountry.equals("NABL/CAP Accreditation")) {
                                    String vlaue = "distance_in_km asc";
                                    String value2 = "lab_users.certificate = 1";
                                    gettingLabswithCityNamewithFilterTwo(vlaue, package_id, latitude, longitude, value2, cityName);
                                }
                                if (selectedCountry.equals("Distance Low to High")) {
                                    String vlaue = "distance_in_km asc";
                                    //gettingLabswithCityNamewithFilter(vlaue, package_id, latitude, longitude, cityName);



                                    if (first_time_flag == 1) {
                                        gettingLabswithCityNamewithFilter(vlaue,  package_id,global_lat, global_lng, cityName);
                                    } else {
                                        gettingLabswithCityNamewithFilter(vlaue,  package_id,latitude, longitude, cityName);
                                    }
                                }
                                if (selectedCountry.equals("Distance High to Low")) {
                                    String vlaue = "distance_in_km desc";
                                    //getLabsTest(vlaue, package_id, latitude, longitude);
                                    if (first_time_flag == 1) {
                                        getLabsTest(vlaue,  package_id, global_lat, global_lng);
                                    } else {
                                        getLabsTest(vlaue,  package_id,latitude, longitude);
                                    }
                                }
                                if (selectedCountry.equals("Names A to Z")) {
                                    String vlaue = "users.user_name asc";
                                    //getLabsTest(vlaue, package_id, latitude, longitude);
                                    if (first_time_flag == 1) {
                                        getLabsTest(vlaue,  package_id, global_lat, global_lng);
                                    } else {
                                        getLabsTest(vlaue,  package_id,latitude, longitude);
                                    }
                                }
                                if (selectedCountry.equals("Names Z to A")) {
                                    String vlaue = "users.user_name desc";
                                    //getLabsTest(vlaue, package_id, latitude, longitude);
                                    if (first_time_flag == 1) {
                                        getLabsTest(vlaue,  package_id, global_lat, global_lng);
                                    } else {
                                        getLabsTest(vlaue,  package_id,latitude, longitude);
                                    }
                                }
                                if (selectedCountry.equals("Price Low to High")) {
                                    String vlaue = "lab_package.final_price asc";
                                   // getLabsTest(vlaue, package_id, latitude, longitude);
                                    if (first_time_flag == 1) {
                                        getLabsTest(vlaue,  package_id, global_lat, global_lng);
                                    } else {
                                        getLabsTest(vlaue,  package_id,latitude, longitude);
                                    }
                                }
                                if (selectedCountry.equals("Price High to Low")) {
                                    String vlaue = "lab_package.final_price desc";
                                    //getLabsTest(vlaue, package_id, latitude, longitude);
                                    if (first_time_flag == 1) {
                                        getLabsTest(vlaue,  package_id, global_lat, global_lng);
                                    } else {
                                        getLabsTest(vlaue,  package_id,latitude, longitude);
                                    }
                                }

                                if (selectedCountry.equals("Ratting High to Low")) {
                                    String vlaue = "rating desc";
                                    //getLabsTest(vlaue, package_id, latitude, longitude);
                                    if (first_time_flag == 1) {
                                        getLabsTest(vlaue,  package_id, global_lat, global_lng);
                                    } else {
                                        getLabsTest(vlaue,  package_id,latitude, longitude);
                                    }
                                }

                                if (selectedCountry.equals("Ratting Low to High")) {
                                    String vlaue = "rating asc";
                                    //getLabsTest(vlaue, package_id, latitude, longitude);
                                    if (first_time_flag == 1) {
                                        getLabsTest(vlaue,  package_id, global_lat, global_lng);
                                    } else {
                                        getLabsTest(vlaue,  package_id,latitude, longitude);
                                    }
                                }

                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();*/
            } else {
                no_labs_found_iv.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), "No Labs found....!", Toast.LENGTH_SHORT).show();
            }
        }


        if (v == yourpackageRL) {

            progressDialog.show();
            labsListArrayList.clear();

            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.LAB_LOCATIONS, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        citiesModelArrayList.clear();
                        for (int i = 0; i < jsonArray.length(); i++)

                        {

                            CitiesModel citiesModel = new CitiesModel();

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("city");

                            String city_name = jsonObject1.getString("city_name");

                            citiesModel.setCity_name(city_name);

                            // labArrayList.add(city_name);


                            citiesModelArrayList.add(citiesModel);


                        }

                        alertLabsList(citiesModelArrayList);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    String responseBody = null;


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(ShowLabsPackage.this);
            requestQueue.add(stringRequest);
        }


        if (v == yourpackageLocation) {
            if (checkLocationPermission()) {
                getDataFromLocation();
            } else {
                checkLocationPermission();
                Toast.makeText(getApplicationContext(), "Location Permission required", Toast.LENGTH_SHORT).show();
            }
        }


        if (v == yourpackageCity) {

            progressDialog.show();
            labsListArrayList.clear();

            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.LAB_LOCATIONS, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        citiesModelArrayList.clear();
                        for (int i = 0; i < jsonArray.length(); i++)

                        {

                            CitiesModel citiesModel = new CitiesModel();


                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("city");


                            String city_name = jsonObject1.getString("city_name");
                            String city_latitude = jsonObject1.getString("lat");
                            String city_longitude = jsonObject1.getString("lang");


                            citiesModel.setCity_name(city_name);
                            citiesModel.setLat(city_latitude);
                            citiesModel.setLang(city_longitude);

                            // labArrayList.add(city_name);


                            citiesModelArrayList.add(citiesModel);


                        }

                        alertLabsList(citiesModelArrayList);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    String responseBody = null;


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(ShowLabsPackage.this);
            requestQueue.add(stringRequest);
        }


    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location")
                        .setMessage("Location Permission Required")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(ShowLabsPackage.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

/*    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getDataFromLocation();
        }

    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (String permission : permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                //denied
                Log.e("denied", permission);
                Toast.makeText(getApplicationContext(), "Location Permission required", Toast.LENGTH_SHORT).show();
            } else {
                if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                    //allowed
                    Log.e("allowed", permission);
//                    gps = new GPSTrackers(getApplicationContext());
                    getDataFromLocation();
                } else {
                    //set to never ask again
                    Log.e("set to never ask again", permission);
                    //do something here.
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }
            }
        }
    }


    public void getDataFromLocation() {
        progressDialog.show();
        labsListArrayList.clear();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.LAB_LOCATIONS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    citiesModelArrayList.clear();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        CitiesModel citiesModel = new CitiesModel();
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("city");
                        String city_name = jsonObject1.getString("city_name");
                        String city_latitude = jsonObject1.getString("lat");
                        String city_longitude = jsonObject1.getString("lang");
                        citiesModel.setCity_name(city_name);
                        citiesModel.setLat(city_latitude);
                        citiesModel.setLang(city_longitude);
                        // labArrayList.add(city_name);
                        citiesModelArrayList.add(citiesModel);
                    }
                    alertLabsList(citiesModelArrayList);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String responseBody = null;


                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {
                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {
                } else if (error instanceof ParseError) {
                }
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(ShowLabsPackage.this);
        requestQueue.add(stringRequest);
    }

    private void gettingLabswithCityNamewithFilter(final String vlaue, final String package_id, final String latitude, final String longitude, String cityName) {
        showlabspackageList.clear();
        noInternetConnect.setVisibility(View.GONE);
        no_labs_found_iv.setVisibility(View.GONE);
        ShowLabsPackage.this.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.show();

            }
        });
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_LABS_FOR_PACKAGES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showlabspackageList.clear();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");

                            ShowLabsPackageModel slpm = new ShowLabsPackageModel();

                            slpm.setShow_lab_package_id(jsonObject1.getString("id"));
                            slpm.setShow_lab_package_user_name(jsonObject1.getString("user_name"));
                            slpm.setShow_lab_package_email(jsonObject1.getString("email"));
                            slpm.setShow_lab_package_mobile(jsonObject1.getString("mobile"));
                            slpm.setShow_lab_package_role_id(jsonObject1.getString("role_id"));
                            slpm.setShow_lab_package_created_time(jsonObject1.getString("created_time"));
                            slpm.setShow_lab_package_workingdays(jsonObject1.getString("workingdays"));
                            slpm.setShow_lab_package_address(jsonObject1.getString("address"));
                            slpm.setCertificate(jsonObject1.getString("certificate"));
                            slpm.setProfile_pic_url(jsonObject1.getString("profile_pic"));
                            slpm.setLocation(jsonObject1.getString("location"));
                            if (jsonObject1.getString("rating").equals("null")) {
                                slpm.setShow_lab_ratting("0");
                            } else {
                                slpm.setShow_lab_ratting(jsonObject1.getString("rating"));
                            }
                            slpm.setShow_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            slpm.setShow_lab_payment_online(jsonObject1.getString("payment_online"));
                            slpm.setShow_lab_tele_booking(jsonObject1.getString("tele_booking"));

                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String workingDayfromServer = jsonObject1.getString("current_week_name");
                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];
                            if (conditionCheck.equals("1")) {
                                slpm.setShow_lab_package_workingtime("OpenToday:" + segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (conditionCheck.equals("0")) {
                                slpm.setShow_lab_package_workingtime("Today lab Close");
                            }
                            slpm.setShow_lab_package_lab_price(jsonObject1.getString("lab_price"));


                            slpm.setShow_lab_package_final_price(jsonObject1.getString("final_price"));

                            slpm.setShow_lab_package_lat(jsonObject1.getString("lat"));
                            slpm.setShow_lab_package_lang(jsonObject1.getString("lang"));
                            slpm.setShow_lab_package_home_collections(jsonObject1.getString("home_collections"));
                            slpm.setShow_lab_package_test_id(jsonObject1.getString("package_sub_id"));
                            slpm.setShow_lab_package_package_id(jsonObject1.getString("package_id"));
                            slpm.setShow_lab_package_distance(jsonObject1.getString("distance_in_km"));

                            showlabspackageList.add(slpm);
                            fabaction_status = 0;

                        }
                        showlabspackagerecyclerview.setAdapter(adapter);

                        ShowLabsPackage.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                    } else {
                        ShowLabsPackage.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                        showlabspackageList.clear();
                        fabaction_status = 1;
                        no_labs_found_iv.setVisibility(View.VISIBLE);
                        noInternetConnect.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnect.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                String responseBody = null;


                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {


                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("package_id", package_id);
                if (first_time_flag == 1) {
                    Log.d("LABFINDSENDVALUES", global_lat + "\n" + global_lng);
                    params.put("lat", global_lat);
                    params.put("lang", global_lng);
                } else {
                    Log.d("LABFINDSENDVALUES", latitude + "\n" + longitude);
                    params.put("lat", latitude);
                    params.put("lang", longitude);
                }
                params.put("order_type", vlaue);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void gettingLabswithCityNamewithFilterTwo(final String vlaue, final String package_id, final String latitude, final String longitude, final String value2, final String cityName) {
        showlabspackageList.clear();
        noInternetConnect.setVisibility(View.GONE);
        no_labs_found_iv.setVisibility(View.GONE);
        ShowLabsPackage.this.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.show();

            }
        });
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_LABS_FOR_PACKAGES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showlabspackageList.clear();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    Log.d("RESPONCECHECK", response);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");

                            ShowLabsPackageModel slpm = new ShowLabsPackageModel();

                            slpm.setShow_lab_package_id(jsonObject1.getString("id"));
                            slpm.setShow_lab_package_user_name(jsonObject1.getString("user_name"));
                            slpm.setShow_lab_package_email(jsonObject1.getString("email"));
                            slpm.setShow_lab_package_mobile(jsonObject1.getString("mobile"));
                            slpm.setShow_lab_package_role_id(jsonObject1.getString("role_id"));
                            slpm.setShow_lab_package_created_time(jsonObject1.getString("created_time"));
                            slpm.setShow_lab_package_workingdays(jsonObject1.getString("workingdays"));
                            slpm.setCertificate(jsonObject1.getString("certificate"));
                            slpm.setShow_lab_package_address(jsonObject1.getString("address"));
                            slpm.setProfile_pic_url(jsonObject1.getString("profile_pic"));
                            slpm.setLocation(jsonObject1.getString("location"));
                            if (jsonObject1.getString("rating").equals("null")) {
                                slpm.setShow_lab_ratting("0");
                            } else {
                                slpm.setShow_lab_ratting(jsonObject1.getString("rating"));
                            }
                            slpm.setShow_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            slpm.setShow_lab_payment_online(jsonObject1.getString("payment_online"));
                            slpm.setShow_lab_tele_booking(jsonObject1.getString("tele_booking"));

                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String workingDayfromServer = jsonObject1.getString("current_week_name");
                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];
                            if (conditionCheck.equals("1")) {
                                slpm.setShow_lab_package_workingtime("OpenToday:" + segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (conditionCheck.equals("0")) {
                                slpm.setShow_lab_package_workingtime("Today lab Close");
                            }
                            slpm.setShow_lab_package_lab_price(jsonObject1.getString("lab_price"));


                            slpm.setShow_lab_package_final_price(jsonObject1.getString("final_price"));

                            slpm.setShow_lab_package_lat(jsonObject1.getString("lat"));
                            slpm.setShow_lab_package_lang(jsonObject1.getString("lang"));
                            slpm.setShow_lab_package_home_collections(jsonObject1.getString("home_collections"));
                            slpm.setShow_lab_package_test_id(jsonObject1.getString("package_sub_id"));
                            slpm.setShow_lab_package_package_id(jsonObject1.getString("package_id"));
                            slpm.setShow_lab_package_distance(jsonObject1.getString("distance_in_km"));

                            fabaction_status = 0;
                            showlabspackageList.add(slpm);

                        }
                        showlabspackagerecyclerview.setAdapter(adapter);

                        ShowLabsPackage.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                    if (responceCode.equals("18888")) {

                        showlabspackageList.clear();
                        ShowLabsPackage.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                        adapter.notifyDataSetChanged();
                        fabaction_status = 1;
                        no_labs_found_iv.setVisibility(View.VISIBLE);
                        noInternetConnect.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnect.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                String responseBody = null;


                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {


                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("package_id", package_id);
                if (first_time_flag == 1) {
                    Log.d("LABFINDSENDVALUES", global_lat + "\n" + global_lng);
                    params.put("lat", global_lat);
                    params.put("lang", global_lng);
                } else {
                    Log.d("LABFINDSENDVALUES", latitude + "\n" + longitude);
                    params.put("lat", latitude);
                    params.put("lang", longitude);
                }
                params.put("order_type", vlaue);

                params.put("home_collections", value2);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void alertLabsList(ArrayList<CitiesModel> labArrayList) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ShowLabsPackage.this);
        LayoutInflater inflater = this.getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.custom_dialog_layout_cities, null);
        RecyclerView cities_recycler_view = (RecyclerView) dialog_layout.findViewById(R.id.cities_recycler_view);
        final SearchView mSearch_list_cities = (SearchView) dialog_layout.findViewById(R.id.mSearch_list_cities);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ShowLabsPackage.this);
        cities_recycler_view.setLayoutManager(layoutManager);

        citiesAdapter = new ShowLabsPackageCitiesAdapter(labArrayList, ShowLabsPackage.this, R.layout.row_cities);
        cities_recycler_view.setAdapter(citiesAdapter);

        mSearch_list_cities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearch_list_cities.setIconified(false);
            }
        });

        builder.setView(dialog_layout)

                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        progressDialog.dismiss();
                        dialogInterface.dismiss();
                    }
                });

        city_dialog = builder.create();

        mSearch_list_cities.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                citiesAdapter.getFilter().filter(query);
                return false;
            }
        });
        city_dialog.show();

    }

    private void gettingLabsotherCities(String selectedCountry, String strLat, String strLng) {
        showlabspackagerecyclerview = (RecyclerView) findViewById(R.id.showlabspackage_recycler_view);
        showlabspackagerecyclerview.setHasFixedSize(true);

        showlabspackagerecyclerview.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        //or
        showlabspackagerecyclerview.addItemDecoration(new DividerItemDecoration(getApplicationContext()));
        //or
        showlabspackagerecyclerview.addItemDecoration(
                new DividerItemDecoration(getApplicationContext(), R.drawable.recycler_divider));

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        showlabspackagerecyclerview.setLayoutManager(layoutManager);
        adapter = new ShowLabsPackageAdapter(package_id, package_name, subtestNames, bridgepackTestandSubIDswithformat, showlabspackageList, ShowLabsPackage.this, R.layout.row_labs_package3, subTestIdsList);


        String filterString = "distance_in_km asc";
        getLabsTest(filterString);

        gettingLabsOtherCity(selectedCountry, filterString, strLat, strLng);


    }

    private void gettingLabsOtherCity(final String selectedCountry, final String filterString, final String strLat, final String strLng) {
        showlabspackageList.clear();
        noInternetConnect.setVisibility(View.GONE);
        no_labs_found_iv.setVisibility(View.GONE);
        ShowLabsPackage.this.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.show();

            }
        });
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_LABS_FOR_PACKAGES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showlabspackageList.clear();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");

                            ShowLabsPackageModel slpm = new ShowLabsPackageModel();

                            slpm.setShow_lab_package_id(jsonObject1.getString("id"));
                            slpm.setShow_lab_package_user_name(jsonObject1.getString("user_name"));
                            slpm.setShow_lab_package_email(jsonObject1.getString("email"));
                            slpm.setShow_lab_package_mobile(jsonObject1.getString("mobile"));
                            slpm.setShow_lab_package_role_id(jsonObject1.getString("role_id"));
                            slpm.setShow_lab_package_created_time(jsonObject1.getString("created_time"));
                            slpm.setCertificate(jsonObject1.getString("certificate"));
                            slpm.setProfile_pic_url(jsonObject1.getString("profile_pic"));
                            slpm.setLocation(jsonObject1.getString("location"));
                            if (jsonObject1.getString("rating").equals("null")) {
                                slpm.setShow_lab_ratting("0");
                            } else {
                                slpm.setShow_lab_ratting(jsonObject1.getString("rating"));
                            }
                            slpm.setShow_lab_payment_lab(jsonObject1.getString("payment_lab"));
                            slpm.setShow_lab_payment_online(jsonObject1.getString("payment_online"));
                            slpm.setShow_lab_tele_booking(jsonObject1.getString("tele_booking"));


                            slpm.setShow_lab_package_workingdays(jsonObject1.getString("workingdays"));
                            slpm.setShow_lab_package_address(jsonObject1.getString("address"));
                            String workingtimefromServer = jsonObject1.getString("current_week_time");
                            String workingDayfromServer = jsonObject1.getString("current_week_name");
                            String removeSpace = workingtimefromServer.replace(" ", "");
                            String segments[] = removeSpace.split("-");
                            String conditionCheck = segments[segments.length - 1];
                            if (conditionCheck.equals("1")) {
                                slpm.setShow_lab_package_workingtime("OpenToday:" + segments[segments.length - 3] + " to " + segments[segments.length - 2]);
                            }
                            if (conditionCheck.equals("0")) {
                                slpm.setShow_lab_package_workingtime("Today lab Close");
                            }
                            slpm.setShow_lab_package_lab_price(jsonObject1.getString("lab_price"));


                            slpm.setShow_lab_package_final_price(jsonObject1.getString("final_price"));

                            slpm.setShow_lab_package_lat(jsonObject1.getString("lat"));
                            slpm.setShow_lab_package_lang(jsonObject1.getString("lang"));
                            slpm.setShow_lab_package_home_collections(jsonObject1.getString("home_collections"));
                            slpm.setShow_lab_package_test_id(jsonObject1.getString("package_sub_id"));
                            slpm.setShow_lab_package_package_id(jsonObject1.getString("package_id"));
                            slpm.setShow_lab_package_distance(jsonObject1.getString("distance_in_km"));
                            fabaction_status = 0;
                            showlabspackageList.add(slpm);

                        }
                        showlabspackagerecyclerview.setAdapter(adapter);

                        ShowLabsPackage.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                    } else {
                        ShowLabsPackage.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();

                            }
                        });
                        mSwipeRefreshLayout.setRefreshing(false);
                        fabaction_status = 1;
                        no_labs_found_iv.setVisibility(View.VISIBLE);
                        noInternetConnect.setText("We are not in your location now, we will resume to your location shortly");
                        noInternetConnect.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                String responseBody = null;


                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {


                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("package_id", package_id);
                if (first_time_flag == 1) {
                    Log.d("LABFINDSENDVALUES", global_lat + "\n" + global_lng);
                    params.put("lat", global_lat);
                    params.put("lang", global_lng);
                } else {
                    Log.d("LABFINDSENDVALUES", latitude + "\n" + longitude);
                    params.put("lat", latitude);
                    params.put("lang", longitude);
                }
                params.put("order_type", filterString);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onRefresh() {

        showlabspackagerecyclerview = (RecyclerView) findViewById(R.id.showlabspackage_recycler_view);
        showlabspackagerecyclerview.setHasFixedSize(true);

        showlabspackagerecyclerview.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));

        showlabspackagerecyclerview.addItemDecoration(new DividerItemDecoration(getApplicationContext()));

        showlabspackagerecyclerview.addItemDecoration(
                new DividerItemDecoration(getApplicationContext(), R.drawable.recycler_divider));

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        showlabspackagerecyclerview.setLayoutManager(layoutManager);
        adapter = new ShowLabsPackageAdapter(package_id, package_name, subtestNames, bridgepackTestandSubIDswithformat, showlabspackageList, ShowLabsPackage.this, R.layout.row_labs_package3, subTestIdsList);
        if (gps.canGetLocation()) {
            latitude = Double.toString(gps.getLatitude());
            longitude = Double.toString(gps.getLongitude());
            if (filterFlag == 0) {
                String filterString = "distance_in_km asc";
                getLabsTest(filterString);
            }
        } else {

            startLocationAlert = new StartLocationAlert(ShowLabsPackage.this);
        }
    }


    public void refreshMyList(String selectedCountry, String selected_city_lat, String selected_city_lng) {

        city_dialog.dismiss();

        first_time_flag = 1;
        yourpackageCity.setText(selectedCountry);
        global_lat = selected_city_lat;
        global_lng = selected_city_lng;
        gettingLabsotherCities(selectedCountry, global_lat, global_lng);
        adapter.notifyDataSetChanged();

    }
}

