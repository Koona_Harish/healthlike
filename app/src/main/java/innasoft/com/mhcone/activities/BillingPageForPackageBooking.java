package innasoft.com.mhcone.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import innasoft.com.mhcone.MainActivity;
import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class BillingPageForPackageBooking extends AppCompatActivity implements  View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    TextView bookpackage_package_name,bookpackage_test_name, bookpackage_lab_name,bookpackage_total,bookingPackage,tv_tele,tv_pal,tv_pay;
    TextView bookpackage_package_name_txt,bookpackage_test_name_txt, bookpackage_lab_name_txt,bookpackage_total_txt,bookpackage_amount;
    Button bookpackage_btn_select_icarefor,bookpackage_btn_billing_cal,bookpackage_btn_billing_pay,bookpackage_btn_tele_book;
    ImageButton closeWindow;
    ImageView bookpackage_btn_date,bookpackage_btn_time;
    TextView bookpackage_in_date, bookpackage_in_time;
    private int mYear, mMonth, mDay, mHour, mMinute;
    String phoneNumber;
    double dtotal,dgrandtotal;
    String status_payment_lab,status_payment_online,status_tele_booking;
    NetworkStatus ns;
    Boolean isOnline = false;
    public static final String TAG = "Payment Status";
    Typeface type_lato,typebold;
    ProgressDialog progressDialog;
    TextView totalAmount,grandTotalAmount;
    String sendDate,sendTime;
    int userCount = 0;
    DatePickerDialog datePickerDialog;
    public String labId,labName,subtestNAMES,subTestName,packageName,labPrice, home_collection_chargess = null;

    ArrayList<String> selectedItems = new ArrayList<String>();
    Map<String, String> selectedItemmap = new HashMap();
    ArrayList<String> usersListId = new ArrayList<String>();

    public String sendpackTestandSubIDswithformat,subTestIdsList;

    UserSessionManager userSessionManager;
    String access_key,disp_userName,disp_email;
    String userID,userMailId,userMobile,userName,lab_home_collections = null;
    CheckBox homecollection_chk;

    int rmdyear,rmdmonth,rmdday,rmdhour,rmdmint;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing_page_for_package_booking2);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Package Payment");

        type_lato = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typebold = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        ns = new NetworkStatus();
        isOnline = ns.isOnline(BillingPageForPackageBooking.this);

        bookingPackage = (TextView) findViewById(R.id.txt_package_name);
        bookingPackage.setTypeface(typebold);

        bookpackage_package_name_txt = (TextView) findViewById(R.id.book_package_package_name);
        bookpackage_package_name_txt.setTypeface(type_lato);

        bookpackage_test_name_txt = (TextView) findViewById(R.id.book_package_test_name);
        bookpackage_test_name_txt.setTypeface(type_lato);

        tv_tele = (TextView) findViewById(R.id.tv_tele);
        tv_pal = (TextView) findViewById(R.id.tv_pal);
        tv_pay = (TextView) findViewById(R.id.tv_pay);

        bookpackage_lab_name_txt = (TextView) findViewById(R.id.book_package_lab_name);
        bookpackage_lab_name_txt.setTypeface(type_lato);

        bookpackage_total_txt = (TextView) findViewById(R.id.grandtotal_Amount);
        bookpackage_total_txt.setTypeface(type_lato);

        bookpackage_amount = (TextView) findViewById(R.id.total_Amount);
        bookpackage_amount.setTypeface(type_lato);

        totalAmount = (TextView) findViewById(R.id.totalAmount);
        totalAmount.setTypeface(type_lato);
        homecollection_chk = (CheckBox) findViewById(R.id.homecollection_chk);
        homecollection_chk.setTypeface(type_lato);

        grandTotalAmount = (TextView) findViewById(R.id.grandtotalAmount);
        grandTotalAmount.setTypeface(type_lato);

        bookpackage_btn_tele_book = (Button) findViewById(R.id.bookpackage_btn_tele_book);
        bookpackage_btn_tele_book.setTypeface(type_lato);

        progressDialog = new ProgressDialog(BillingPageForPackageBooking.this, R.style.DialogTheme);
        progressDialog.setMessage("Please Wait......");
        progressDialog.setCancelable(false);

        bookpackage_btn_billing_cal = (Button) findViewById(R.id.bookpackage_btn_billing_cal);
        bookpackage_btn_billing_cal.setTypeface(type_lato);
        bookpackage_btn_billing_pay = (Button) findViewById(R.id.bookpackage_btn_billing);
        bookpackage_btn_billing_pay.setTypeface(type_lato);
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = userSessionManager.getUserDetails();
        access_key = user.get(UserSessionManager.KEY_ACCSES);

        disp_userName = user.get(UserSessionManager.USER_NAME);
        disp_email = user.get(UserSessionManager.USER_EMAIL);
        userID = user.get(UserSessionManager.USER_ID);
        userMailId = user.get(UserSessionManager.USER_EMAIL);
        userName = user.get(UserSessionManager.USER_NAME);
        userMobile = user.get(UserSessionManager.USER_MOBILE);

        Bundle bundle = getIntent().getExtras();
        labId = bundle.getString("selecteLabId");
        labName = bundle.getString("selectedLabName");
        subTestName = bundle.getString("subTestName");
        subtestNAMES = bundle.getString("subTestsNAMES");
        packageName = bundle.getString("packageName");
        phoneNumber = bundle.getString("selectedLabPhone");
        labPrice = bundle.getString("selectedLabPrice");
        subTestIdsList = bundle.getString("subTestIdsList");

        double labPriceDouble = Double.valueOf(labPrice);
        dtotal = Double.valueOf(new DecimalFormat("##.###").format(labPriceDouble));

        status_payment_lab = bundle.getString("payment_lab");
        status_payment_online = bundle.getString("payment_online");
        status_tele_booking = bundle.getString("tele_booking");
        totalAmount.setText("Rs. "+dtotal);
        sendpackTestandSubIDswithformat = bundle.getString("packTestandSubIDswithformat");

        bookpackage_package_name = (TextView) findViewById(R.id.bookpackage_package_name);
        bookpackage_package_name.setTypeface(type_lato);
        String pName = subTestName.substring(0, 1).toUpperCase()+ subTestName.substring(1);
        bookpackage_package_name.setText(pName);

        bookpackage_lab_name = (TextView) findViewById(R.id.bookpackage_lab_name);
        bookpackage_lab_name.setTypeface(type_lato);
        String lName = labName.substring(0, 1).toUpperCase()+ labName.substring(1);
        bookpackage_lab_name.setText(lName);
        bookpackage_test_name = (TextView) findViewById(R.id.bookpackage_test_name);
        bookpackage_test_name.setTypeface(type_lato);
        String tName = subtestNAMES.substring(0, 1).toUpperCase()+ subtestNAMES.substring(1);
        bookpackage_test_name.setText(subtestNAMES.replaceAll("\\[", "").replaceAll("\\]",""));
        bookpackage_total = (TextView) findViewById(R.id.grandtotalAmount);
        bookpackage_total.setTypeface(type_lato);
        bookpackage_btn_date = (ImageView) findViewById(R.id.bookpackage_btn_date);
        bookpackage_btn_time = (ImageView) findViewById(R.id.bookpackage_btn_time);
        bookpackage_btn_select_icarefor = (Button) findViewById(R.id.bookpackage_btn_select_icarefor);
        bookpackage_btn_select_icarefor.setTypeface(type_lato);
        bookpackage_in_date = (TextView) findViewById(R.id.bookpackage_in_date);
        bookpackage_in_date.setTypeface(type_lato);
        bookpackage_in_time = (TextView) findViewById(R.id.bookpackage_in_time);
        bookpackage_in_time.setTypeface(type_lato);
        closeWindow = (ImageButton) findViewById(R.id.package_billing_close);
        closeWindow.setOnClickListener(this);
        bookpackage_btn_date.setOnClickListener(this);
        bookpackage_btn_time.setOnClickListener(this);
        bookpackage_btn_select_icarefor.setOnClickListener(this);
        bookpackage_btn_billing_pay.setOnClickListener(this);
        bookpackage_btn_billing_cal.setOnClickListener(this);
        bookpackage_btn_tele_book.setOnClickListener(this);

        if(status_payment_lab.equals("0"))
        {
            bookpackage_btn_billing_cal.setVisibility(View.INVISIBLE);
            tv_pal.setVisibility(View.INVISIBLE);
        }else {
            bookpackage_btn_billing_cal.setVisibility(View.VISIBLE);
            tv_pal.setVisibility(View.VISIBLE);
        }

        if(status_tele_booking.equals("0"))
        {
            bookpackage_btn_tele_book.setVisibility(View.INVISIBLE);
            tv_tele.setVisibility(View.INVISIBLE);
        }else {
            bookpackage_btn_tele_book.setVisibility(View.VISIBLE);
            tv_tele.setVisibility(View.VISIBLE);
        }
        if(status_payment_online.equals("0"))
        {
            bookpackage_btn_billing_pay.setVisibility(View.INVISIBLE);
            tv_pay.setVisibility(View.INVISIBLE);
        }else {
            bookpackage_btn_billing_pay.setVisibility(View.VISIBLE);
            tv_pay.setVisibility(View.VISIBLE);
        }

        homeCollectionStatus();

        homecollection_chk.setOnCheckedChangeListener(this);
    }

    private void homeCollectionStatus() {
       /* final ArrayList<String> subTestIds = new ArrayList<String>();

        for (int i = 0; i < arraylistsubtestIdsObject.size(); i++) {
            subTestIds.add(arraylistsubtestIdsObject.get(i)+"");
        }
        Log.d("TESTIDSANDLABIDS", subTestIds.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", "")+"\n"+labId);*/
        StringRequest stringRequest2 = new StringRequest(Request.Method.POST, AppUrls.HOME_COLLECTION_STATUS, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                progressDialog.dismiss();

                try {
                    JSONObject jsonObject =new JSONObject(response);
                    Log.d("HOMECOLLECTIONSDFFA", response);
                    String responceCode = jsonObject.getString("status");
                    if(responceCode.equals("19990"))
                    {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        JSONObject jsonObject2 = jsonObject1.getJSONObject("check-home-collection");
                        lab_home_collections = jsonObject2.getString("lab_home_collections");
                        home_collection_chargess = jsonObject2.getString("home_collection_price");
                        homecollection_chk.setText("Home collection Rs."+home_collection_chargess);


                    }
                    if(responceCode.equals("18880"))
                    {
                        homecollection_chk.setEnabled(false);
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        JSONObject jsonObject2 = jsonObject1.getJSONObject("check-home-collection");
                        lab_home_collections = jsonObject2.getString("lab_home_collections");
                        home_collection_chargess = jsonObject2.getString("home_collection_price");
                        homecollection_chk.setText("Home Collection Not available");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {


                } else if (error instanceof ParseError) {

                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String,String> params = new HashMap<String, String>();
                Log.d("HOMECOLLECTIONSDFFA", "SEND   "+subTestIdsList+"\n"+labId);
                params.put("test_id", subTestIdsList);
                params.put("lab_id", labId);
                return params;
            }
        };
        stringRequest2.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );
        RequestQueue requestQueue2 = Volley.newRequestQueue(BillingPageForPackageBooking.this);
        requestQueue2.add(stringRequest2);

    }

    @Override
    public void onClick(View v) {

        if(v == bookpackage_btn_billing_cal)
        {
            if (isOnline) {
                if(home_collection_chargess.equals("null") || home_collection_chargess.equals(null)){
                    dgrandtotal = dtotal * userCount;
                }else {
                    if(homecollection_chk.isChecked()) {
                        dgrandtotal = dtotal * userCount + Double.valueOf(home_collection_chargess);
                    }else {
                        dgrandtotal = dtotal * userCount;
                    }

                }

                if (sendDate != null && sendTime != null && userCount != 0) {

                    progressDialog.show();

                    bookpackage_btn_billing_cal.setClickable(false);
                    StringRequest stringRequest2 = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>()
                    {
                        @Override
                        public void onResponse(String response)
                        {
                            try {
                                JSONObject jsonObject =new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if(responceCode.equals("19999"))
                                {
                                    progressDialog.show();
                                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BILLINGURL,
                                            new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String response) {

                                                    try {
                                                        JSONObject jsonObject = new JSONObject(response);
                                                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                                        String paymentType = jsonObject1.getString("payment-type");
                                                        String bookingId = jsonObject1.getString("booking-id");
                                                        String message = jsonObject1.getString("msg");
                                                        progressDialog.dismiss();

                                                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                                        Intent intent = new Intent(getApplicationContext(), BookingSuccessPage.class);
                                                        intent.putExtra("bookingId", bookingId);

                                                        intent.putExtra("rmdyear", rmdyear + "");
                                                        intent.putExtra("rmdmonth", rmdmonth + "");
                                                        intent.putExtra("rmdday", rmdday + "");
                                                        intent.putExtra("rmdhour", rmdhour + "");
                                                        intent.putExtra("rmdmint", rmdmint + "");
                                                        intent.putExtra("descr", "You are book " + labName + " Appointment");
                                                        startActivity(intent);

                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            },
                                            new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {

                                                    progressDialog.dismiss();

                                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                                    } else if (error instanceof AuthFailureError) {

                                                    } else if (error instanceof ServerError) {

                                                    } else if (error instanceof NetworkError) {

                                                    } else if (error instanceof ParseError) {
                                                    }
                                                }
                                            })
                                    {
                                        @Override
                                        protected Map<String, String> getParams() throws AuthFailureError {
                                            Map<String, String> params = new HashMap<String, String>();
                                            params.put("user_id", userID);
                                            params.put("lab_id", labId);
                                            params.put("payment_type", "cal");
                                            params.put("booking_date", sendDate + " " + sendTime);
                                            params.put("user_count", userCount + "");
                                            params.put("transaction_id", "0");
                                            params.put("price", dtotal+"");
                                            params.put("grand_total", dgrandtotal + "");
                                            params.put("form_type", "package");
                                            Log.d("BOOKINGINFO", "PACKAGESSBOOKING "+sendpackTestandSubIDswithformat.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", ""));
                                            params.put("package_id", sendpackTestandSubIDswithformat);
                                            params.put("sub_user_id", usersListId.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(",", "-").replaceAll(" ", ""));
                                            params.put("type", "testlab");
                                            return params;
                                        }
                                    };
                                    stringRequest.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );
                                    //stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                    RequestQueue requestQueue = Volley.newRequestQueue(BillingPageForPackageBooking.this);
                                    requestQueue.add(stringRequest);
                                }
                                if(responceCode.equals("10140"))
                                {
                                    userSessionManager.logoutUser();
                                    Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);
                                }

                                if(responceCode.equals("10150"))
                                {
                                    userSessionManager.logoutUser();
                                    Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            progressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    })
                    {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError
                        {
                            Map<String,String> params = new HashMap<String, String>();
                            params.put("user_id",userID);
                            return params;
                        }
                    };
                    stringRequest2.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );
                    //stringRequest2.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue2 = Volley.newRequestQueue(BillingPageForPackageBooking.this);
                    requestQueue2.add(stringRequest2);

                } else {
                    Toast.makeText(getApplicationContext(), "Please provide all data...!", Toast.LENGTH_SHORT).show();

                }
            }else {
                showInternetStatus();
            }

        }
        if (v == bookpackage_btn_billing_pay) {
            if (isOnline) {


                if(home_collection_chargess.equals("null") || home_collection_chargess.equals(null)){
                    dgrandtotal = dtotal * userCount;
                }else {
                    if(homecollection_chk.isChecked()) {
                        dgrandtotal = dtotal * userCount + Double.valueOf(home_collection_chargess);
                    }else {
                        dgrandtotal = dtotal * userCount;
                    }

                }

                if (sendDate != null && sendTime != null && userCount != 0) {

                    if(dgrandtotal != 0.0) {

                        bookpackage_btn_billing_pay.setClickable(false);
                    StringRequest stringRequest2 = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>()
                    {
                        @Override
                        public void onResponse(String response)
                        {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject =new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if(responceCode.equals("19999"))
                                {

                                    String getFname = userName;
                                    String getPhone = userMobile;
                                    String getEmail = userMailId;
                                    String getAmt   = dgrandtotal+"";

                                    Intent intent = new Intent(getApplicationContext(), PayMentGateWay.class);
                                    intent.putExtra("FIRST_NAME",getFname);
                                    intent.putExtra("PHONE_NUMBER",getPhone);
                                    intent.putExtra("EMAIL_ADDRESS",getEmail);
                                    intent.putExtra("RECHARGE_AMT",getAmt);
                                    intent.putExtra("rmdyear", rmdyear+"");
                                    intent.putExtra("rmdmonth", rmdmonth+"");
                                    intent.putExtra("rmdday", rmdday+"");
                                    intent.putExtra("rmdhour", rmdhour+"");
                                    intent.putExtra("rmdmint", rmdmint+"");
                                    intent.putExtra("descr", labName);
                                    intent.putExtra("user_id", userID);
                                    intent.putExtra("lab_id", labId);
                                    intent.putExtra("payment_type", "payment");
                                    intent.putExtra("booking_date", sendDate + " " + sendTime);
                                    intent.putExtra("user_count", userCount + "");
                                    intent.putExtra("price", dtotal+"");
                                    intent.putExtra("grand_total", dgrandtotal + "");
                                    intent.putExtra("form_type", "test");
                                    intent.putExtra("package_id", sendpackTestandSubIDswithformat);
                                    intent.putExtra("sub_user_id", usersListId.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(",", "-").replaceAll(" ", ""));
                                    intent.putExtra("type", "testlab");
                                    startActivity(intent);
                                }
                                if(responceCode.equals("10140"))
                                {
                                    userSessionManager.logoutUser();
                                    Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);
                                }

                                if(responceCode.equals("10150"))
                                {
                                    userSessionManager.logoutUser();
                                    Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            progressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    })
                    {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError
                        {
                            Map<String,String> params = new HashMap<String, String>();
                            params.put("user_id",userID);
                            return params;
                        }
                    };
                        stringRequest2.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );
                    RequestQueue requestQueue2 = Volley.newRequestQueue(BillingPageForPackageBooking.this);
                    requestQueue2.add(stringRequest2);

                    }else {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Your are not allowed to pay Rs.0/- ", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Please provide all data...!", Toast.LENGTH_SHORT).show();

                }
            }else {
                showInternetStatus();
            }
        }

        if (v == bookpackage_btn_date) {

            final java.util.Calendar c = java.util.Calendar.getInstance();
            mYear = c.get(java.util.Calendar.YEAR);
            mMonth = c.get(java.util.Calendar.MONTH);
            mDay = c.get(java.util.Calendar.DAY_OF_MONTH);

            datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,int monthOfYear, int dayOfMonth) {

                            rmdyear = year;
                            rmdmonth = monthOfYear+1;
                            rmdday = dayOfMonth;
                            sendDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                            bookpackage_in_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        }

                    }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

            datePickerDialog.show();
        }
        if (v == bookpackage_btn_time) {

            final java.util.Calendar c = java.util.Calendar.getInstance();
            mHour = c.get(java.util.Calendar.HOUR_OF_DAY);
            mMinute = c.get(java.util.Calendar.MINUTE);

            final TimePickerDialog timePickerDialog = new TimePickerDialog(this,android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            rmdhour = Integer.valueOf(String.format("%02d", hourOfDay));
                            rmdmint = Integer.valueOf(String.format("%02d", minute));
                            String hourchange = String.format("%02d", hourOfDay);
                            String minitChange = String.format("%02d", minute);
                            String disptime = updateTime(hourOfDay , minute);
                            sendTime = disptime;
                            bookpackage_in_time.setText(disptime);

                        }
                    }, mHour, mMinute, false);
            timePickerDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialogInterface) {
                    timePickerDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setVisibility(View.GONE);
                }
            });

            timePickerDialog.show();

        }


        if (v == closeWindow){

            finish();
        }
        if(v == bookpackage_btn_tele_book)
        {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:"+Uri.encode(phoneNumber.trim())));
            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(callIntent);
        }

        if (v == bookpackage_btn_select_icarefor) {
            if (isOnline) {
                BillingPageForPackageBooking.this.runOnUiThread(new Runnable() {
                    public void run() {
                        progressDialog.show();
                    }
                });

                StringRequest stringRequest2 = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        try {
                            JSONObject jsonObject =new JSONObject(response);
                            String responceCode = jsonObject.getString("status");
                            if(responceCode.equals("19999"))
                            {
                                userCount = 0;
                                usersListId.clear();
                                final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.VIEW_SUB_USERS_URL,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {

                                                try {
                                                    selectedItems.clear();
                                                    JSONObject jsonObject = new JSONObject(response);
                                                    String successCode = jsonObject.getString("status");
                                                    if (successCode.equals("10190")) {
                                                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                                                        for (int i = 0; i < jsonArray.length(); i++) {
                                                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("sub-users");
                                                            jsonObject1.getString("id");
                                                            jsonObject1.getString("user_id");
                                                            jsonObject1.getString("user_sub_name");
                                                            jsonObject1.getString("relation");
                                                            jsonObject1.getString("age");
                                                            jsonObject1.getString("gender");
                                                            jsonObject1.getString("email");
                                                            jsonObject1.getString("mobile");
                                                            jsonObject1.getString("status");
                                                            jsonObject1.getString("created_time");
                                                            selectedItemmap.put(jsonObject1.getString("user_sub_name"), jsonObject1.getString("id"));
                                                            selectedItems.add(jsonObject1.getString("user_sub_name"));
                                                        }
                                                    }
                                                    selectedItems.add("Self");
                                                    selectedItemmap.put("Self", "0");
                                                    progressDialog.dismiss();
                                                    alertUsersList(selectedItems, selectedItemmap);

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                progressDialog.dismiss();
                                                String responseBody = null;

                                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                                } else if (error instanceof AuthFailureError) {

                                                } else if (error instanceof ServerError) {

                                                } else if (error instanceof NetworkError) {

                                                } else if (error instanceof ParseError) {

                                                }
                                            }
                                        }

                                ) {
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("user_id", userID);
                                        return params;
                                    }
                                };
                                stringRequest.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );
                                RequestQueue requestQueue = Volley.newRequestQueue(BillingPageForPackageBooking.this);
                                requestQueue.add(stringRequest);
                            }
                            if(responceCode.equals("10140"))
                            {

                                userSessionManager.logoutUser();
                                Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }

                            if(responceCode.equals("10150"))
                            {
                                userSessionManager.logoutUser();
                                Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        progressDialog.dismiss();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {


                        } else if (error instanceof ParseError) {

                        }
                    }
                })
                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError
                    {
                        Map<String,String> params = new HashMap<String, String>();
                        params.put("user_id",userID);
                        return params;
                    }
                };
                stringRequest2.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );
                RequestQueue requestQueue2 = Volley.newRequestQueue(BillingPageForPackageBooking.this);
                requestQueue2.add(stringRequest2);

            }else {
                showInternetStatus();
            }
        }
    }

    private void showDialogMessage(String message) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(TAG);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void showInternetStatus() {

        LayoutInflater inflater = getLayoutInflater();
        View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
        TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
        textView.setTypeface(type_lato);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(toastLayout);
        toast.show();
    }


    public void alertUsersList(final ArrayList<String> dataArrayList, final Map<String, String> keyvaluePair)
    {
        Log.d("HOMECOLLECTIONSTATUSAA", home_collection_chargess);
        if (dataArrayList.size() != 0)
        {
            final String[] items = dataArrayList.toArray(new String[dataArrayList.size()]);

            final ArrayList<String> mselectedItems = new ArrayList<>();

            if (!mselectedItems.isEmpty()) {

                mselectedItems.clear();
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(BillingPageForPackageBooking.this);

            userCount = 0;
            builder.setTitle("Choose Your I Care For ")
                    .setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which, boolean isChecked) {

                            if (isChecked) {

                                String selectedItems = items[which];
                                mselectedItems.add(selectedItems);
                                String value = (String) keyvaluePair.get(selectedItems);
                                usersListId.add(value);
                                userCount++;
                            } else {
                                userCount--;
                                mselectedItems.remove(items[which]);
                                String value = (String) keyvaluePair.get(items[which]);
                                usersListId.remove(value);
                            }
                        }
                    })
                    .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            List<String> list = new ArrayList<String>();
                            for (String items : mselectedItems) {
                                list.add(items);
                            }

                            if (list.size() != 0) {
                                bookpackage_btn_select_icarefor.setText(list.toString().replaceAll("\\[", "").replaceAll("\\]", ""));
                            } else {
                                Toast.makeText(getApplicationContext(), "Please Select...", Toast.LENGTH_LONG).show();
                                bookpackage_btn_select_icarefor.setText("Select I Care For");
                            }


                          //  bookpackage_total.setText("Rs. "+dtotal*userCount+"");

                            if(lab_home_collections.equals("null") || lab_home_collections.equals(null) || lab_home_collections.equals("0") || lab_home_collections.equals("")){
                                bookpackage_total.setText("Rs. "+dtotal*userCount+"");
                                homecollection_chk.setClickable(false);
                            }else {
                                Log.d("VALUESCHANGES", (dtotal*userCount+Double.valueOf(home_collection_chargess))+"");

                                if(homecollection_chk.isChecked()) {
                                    bookpackage_total.setText("Rs. " + (dtotal * userCount + Double.valueOf(home_collection_chargess)) + "");
                                }else {
                                    bookpackage_total.setText("Rs. "+dtotal*userCount+"");
                                }

                            }
                        }
                    });

            BillingPageForPackageBooking.this.runOnUiThread(new Runnable() {
                public void run() {
                    progressDialog.dismiss();

                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private String updateTime(int hours, int mins) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";

        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);
        String hourss = "";
        if(hours < 10)
            hourss = "0"  + hours;
        else
            hourss = String.valueOf(hours);

        String aTime = new StringBuilder().append(hourss).append(':').append(minutes).append(" ").append(timeSet).toString();

        return aTime;
    }

    @Override
    public void onBackPressed() {

        finish();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        if(homecollection_chk == compoundButton)
        {
            if(isChecked)
            {
                grandTotalAmount.setText("Rs. " + (dtotal * userCount + Double.valueOf(home_collection_chargess)) + "");
            }
            else {
                grandTotalAmount.setText("Rs. " + (dtotal * userCount)+"");
            }
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}