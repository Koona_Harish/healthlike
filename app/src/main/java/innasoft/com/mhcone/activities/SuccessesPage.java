package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.DBHandler;

public class SuccessesPage extends AppCompatActivity {

    String paymentID,user_id1,lab_id1,payment_type,booking_date,user_count,price1,grand_total,form_type,package_id1,sub_user_id,type1,
            name1,rmdyear,rmdmonth,rmdday,rmdhour,rmdmint,descr;
    ProgressDialog progressDialog;
    DBHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_successes_page);

        Bundle bundle = getIntent().getExtras();
        paymentID = bundle.getString("paymentID");
        user_id1 = bundle.getString("user_id");
        lab_id1 = bundle.getString("lab_id");
        payment_type = bundle.getString("payment_type");
        booking_date = bundle.getString("booking_date");
        user_count = bundle.getString("user_count");
        price1 = bundle.getString("price");
        grand_total = bundle.getString("grand_total");
        form_type = bundle.getString("form_type");
        package_id1 = bundle.getString("package_id");
        sub_user_id = bundle.getString("sub_user_id");
        type1 = bundle.getString("type");
        name1 = bundle.getString("name");
        rmdyear = bundle.getString("rmdyear");
        rmdmonth = bundle.getString("rmdmonth");
        rmdday = bundle.getString("rmdday");
        rmdhour = bundle.getString("rmdhour");
        rmdmint = bundle.getString("rmdmint");
        descr = bundle.getString("descr");

        dbHandler = new DBHandler(this);

        progressDialog = new ProgressDialog(SuccessesPage.this, R.style.DialogTheme);
        progressDialog.setMessage("Please wait......");
        progressDialog.show();

        sendingToServerData();

    }

    private void sendingToServerData() {
        StringRequest  stringRequest = new StringRequest(Request.Method.POST, AppUrls.BILLINGURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);


                            dbHandler.deleteTableData();
                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                            String paymentType = jsonObject1.getString("payment-type");
                            String bookingId = jsonObject1.getString("booking-id");
                            String message = jsonObject1.getString("msg");
                            progressDialog.dismiss();

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), BookingSuccessPage.class);
                            intent.putExtra("rmdyear", rmdyear + "");
                            intent.putExtra("rmdmonth", rmdmonth + "");
                            intent.putExtra("rmdday", rmdday + "");
                            intent.putExtra("rmdhour", rmdhour + "");
                            intent.putExtra("rmdmint", rmdmint + "");
                            intent.putExtra("descr", "You are book " + descr + " Appointment");

                            intent.putExtra("bookingId", bookingId);
                            startActivity(intent);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        progressDialog.dismiss();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        )

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", user_id1);
                params.put("lab_id", lab_id1);
                params.put("payment_type", payment_type);
                params.put("booking_date", booking_date);
                params.put("user_count", user_count);
                params.put("price", price1);
                params.put("grand_total", grand_total);
                params.put("form_type", form_type);
                params.put("package_id", package_id1);
                params.put("transaction_id", paymentID);
                params.put("sub_user_id", sub_user_id);
                params.put("type", type1);
                return params;
            }
        };
        stringRequest.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );

        RequestQueue requestQueue = Volley.newRequestQueue(SuccessesPage.this);
        requestQueue.add(stringRequest);
    }
}
