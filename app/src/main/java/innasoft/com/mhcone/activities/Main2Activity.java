package innasoft.com.mhcone.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


import innasoft.com.mhcone.R;
import innasoft.com.mhcone.fragments.BookAppointmentFragment;
import innasoft.com.mhcone.fragments.TestSubTestFragment;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.CustomTypefaceSpan;
import innasoft.com.mhcone.utilities.FCM.Config;
import innasoft.com.mhcone.utilities.FilePath;
import innasoft.com.mhcone.utilities.ImagePermissions;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.NetworkUtil;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class Main2Activity extends AppCompatActivity implements View.OnClickListener {

    DrawerLayout mDrawerLayout;
    NavigationView navigationView;
    UserSessionManager userSessionManager;
    boolean doubleBackToExitPressedOnce = false;
    String userName,mobile,email,age;
    String access_key,disp_userName,disp_email;
    String userID;
    ImageView imageViewProfilePick;
    TextView editImageButton;
    private String userChoosenTask;
    private String selectedFilePath;
    ProgressDialog progressDialog;
    private Uri fileUri;
    Typeface typeface,typeface2;
    private Boolean exit = false;
    Toast toast;
    NetworkStatus ns;
    Boolean isOnline = false;

    private static final String TAG = Main2Activity.class.getSimpleName();

    private static final int CAMERA_REQUEST_IMAGE = 1888;
    private static final int GALLERY_REQUEST_IMAGE = 1999;
    public static final int MEDIA_TYPE_VIDEO = 2;
    TextView nameProfile;
    TextView emailProfile;
    HttpEntity resEntity;
    public static String PACKAGE_NAME;
    public static final int MEDIA_TYPE_IMAGE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       /* final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);*/

        typeface2 = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typeface = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        PACKAGE_NAME = getApplicationContext().getPackageName();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        progressDialog.setCancelable(false);

        ns = new NetworkStatus();
        isOnline = ns.isOnline(Main2Activity.this);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null)
        {
            userName = bundle.getString("username");
            mobile = bundle.getString("mobile");
            email = bundle.getString("email");
            age = bundle.getString("age");
        }

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = userSessionManager.getUserDetails();
        access_key = user.get(UserSessionManager.KEY_ACCSES);
        disp_userName = user.get(UserSessionManager.USER_NAME);
        disp_email = user.get(UserSessionManager.USER_EMAIL);
        userID = user.get(UserSessionManager.USER_ID);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            navigationView.setItemIconTintList(null);
            setupDrawerContent(navigationView);
        }

        View navigationheadderView = navigationView.getHeaderView(0);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        Menu m = navigationView.getMenu();
        for (int i=0;i<m.size();i++) {
            MenuItem mi = m.getItem(i);

            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }
            applyFontToMenuItem(mi);
        }

        nameProfile = (TextView) navigationheadderView.findViewById(R.id.txtViewName);
        imageViewProfilePick = (ImageView) navigationheadderView.findViewById(R.id.prifileImage);
        editImageButton = (TextView) navigationheadderView.findViewById(R.id.editButton);

        if(isOnline)
        {
            gettingProfileInformation(userID);
        }

        else {
            progressDialog.dismiss();
        }

        editImageButton.setOnClickListener(this);
        emailProfile = (TextView) navigationheadderView.findViewById(R.id.txtViewEmail);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        if (viewPager != null) {
            setupViewPager(viewPager);
        }

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setNightMode(@AppCompatDelegate.NightMode int nightMode) {
        AppCompatDelegate.setDefaultNightMode(nightMode);

        if (Build.VERSION.SDK_INT >= 11) {
            recreate();
        }
    }

    private void setupViewPager(ViewPager viewPager)
    {
        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(new BookAppointmentFragment(), "Packages");
        adapter.addFragment(new TestSubTestFragment(), "Tests");

        viewPager.setAdapter(adapter);
    }
    private void applyFontToMenuItem(MenuItem mi) {

        Typeface font = Typeface.createFromAsset(getAssets(), "latobold.ttf");

        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(final MenuItem menuItem) {
                        //menuItem.setChecked(true);
                        final MenuItem muMenuItem1 = menuItem;
                        Handler h = new Handler();
                        h.postDelayed(new Runnable()
                        {
                            public void run() {
                                switch (muMenuItem1.getItemId()) {
                                    case R.id.nav_item_inbox:
                                        Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                                        startActivity(intent);
                                        break;
                                    case R.id.nav_item_view_Packages:
                                        if (NetworkUtil.isConnected(Main2Activity.this)) {
                                            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String response) {
                                                    progressDialog.dismiss();

                                                    try {
                                                        JSONObject jsonObject = new JSONObject(response);
                                                        String responceCode = jsonObject.getString("status");
                                                        if (responceCode.equals("19999")) {
                                                            Intent intent = new Intent(getApplicationContext(), ViewMyPackage.class);
                                                            startActivity(intent);
                                                        }
                                                        if (responceCode.equals("10140")) {
                                                            userSessionManager.logoutUser();
                                                            Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                                            Intent intent = new Intent(getApplicationContext(), Static2Activity.class);
                                                            startActivity(intent);
                                                        }

                                                        if (responceCode.equals("10150")) {
                                                            userSessionManager.logoutUser();
                                                            Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                                            Intent intent = new Intent(getApplicationContext(), Static2Activity.class);
                                                            startActivity(intent);
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }, new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    progressDialog.dismiss();
                                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                                                    } else if (error instanceof AuthFailureError) {

                                                    } else if (error instanceof ServerError) {

                                                    } else if (error instanceof NetworkError) {

                                                    } else if (error instanceof ParseError) {

                                                    }
                                                }
                                            }) {
                                                @Override
                                                protected Map<String, String> getParams() throws AuthFailureError {
                                                    Map<String, String> params = new HashMap<String, String>();
                                                    params.put("user_id", userID);
                                                    return params;
                                                }
                                            };
                                            RequestQueue requestQueue = Volley.newRequestQueue(Main2Activity.this);
                                            requestQueue.add(stringRequest);
                                        }else {
                                            showInternetStatus();
                                        }
                                        break;
                                    case R.id.nabl_txt:
                                        if (NetworkUtil.isConnected(Main2Activity.this)) {
                                            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String response) {
                                                    progressDialog.dismiss();

                                                    try {
                                                        JSONObject jsonObject = new JSONObject(response);
                                                        String responceCode = jsonObject.getString("status");
                                                        if (responceCode.equals("19999")) {

                                                            Intent intent = new Intent(getApplicationContext(), CreateMyPackage.class);
                                                            startActivity(intent);
                                                        }
                                                        if (responceCode.equals("10140")) {
                                                            userSessionManager.logoutUser();
                                                            Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                                            Intent intent = new Intent(getApplicationContext(), Static2Activity.class);
                                                            startActivity(intent);
                                                        }

                                                        if (responceCode.equals("10150")) {
                                                            userSessionManager.logoutUser();
                                                            Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                                            Intent intent = new Intent(getApplicationContext(), Static2Activity.class);
                                                            startActivity(intent);
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }, new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    progressDialog.dismiss();
                                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                                    } else if (error instanceof AuthFailureError) {

                                                    } else if (error instanceof ServerError) {

                                                    } else if (error instanceof NetworkError) {

                                                    } else if (error instanceof ParseError) {

                                                    }
                                                }
                                            }) {
                                                @Override
                                                protected Map<String, String> getParams() throws AuthFailureError {
                                                    Map<String, String> params = new HashMap<String, String>();
                                                    params.put("user_id", userID);
                                                    return params;
                                                }
                                            };
                                            RequestQueue requestQueue = Volley.newRequestQueue(Main2Activity.this);
                                            requestQueue.add(stringRequest);
                                        }else {
                                            showInternetStatus();
                                        }
                                        break;
                                    case R.id.nav_item_habits:
                                            Intent intent2 = new Intent(getApplicationContext(), HabitsList.class);
                                            intent2.putExtra("userId", userID);
                                            startActivity(intent2);
                                        break;
                                    case R.id.about_us_tittle:
                                        Intent intent5 = new Intent(getApplicationContext(), ViewUsers.class);
                                        startActivity(intent5);
                                        break;

                                    case R.id.Accordion:
                                        if (NetworkUtil.isConnected(Main2Activity.this)) {
                                            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String response) {
                                                    progressDialog.dismiss();

                                                    try {
                                                        JSONObject jsonObject = new JSONObject(response);
                                                        String responceCode = jsonObject.getString("status");
                                                        if (responceCode.equals("19999")) {

                                                            Intent intent = new Intent(getApplicationContext(), ChangePassword.class);
                                                            startActivity(intent);
                                                        }
                                                        if (responceCode.equals("10140")) {

                                                            userSessionManager.logoutUser();
                                                            Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                                            Intent intent = new Intent(getApplicationContext(), Static2Activity.class);
                                                            startActivity(intent);
                                                        }

                                                        if (responceCode.equals("10150")) {

                                                            userSessionManager.logoutUser();
                                                            Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                                            Intent intent = new Intent(getApplicationContext(), Static2Activity.class);
                                                            startActivity(intent);
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }


                                                }
                                            }, new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    progressDialog.dismiss();
                                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                                                    } else if (error instanceof AuthFailureError) {

                                                    } else if (error instanceof ServerError) {

                                                    } else if (error instanceof NetworkError) {

                                                    } else if (error instanceof ParseError) {

                                                    }
                                                }
                                            }) {
                                                @Override
                                                protected Map<String, String> getParams() throws AuthFailureError {
                                                    Map<String, String> params = new HashMap<String, String>();
                                                    params.put("user_id", userID);
                                                    return params;
                                                }
                                            };
                                            RequestQueue requestQueue = Volley.newRequestQueue(Main2Activity.this);
                                            requestQueue.add(stringRequest);
                                        }else {
                                            showInternetStatus();
                                        }
                                        break;
                                    case R.id.nav_item_habits_filter:
                                        if (NetworkUtil.isConnected(Main2Activity.this)) {

                                            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String response) {
                                                    progressDialog.dismiss();

                                                    try {
                                                        JSONObject jsonObject = new JSONObject(response);
                                                        String responceCode = jsonObject.getString("status");
                                                        if (responceCode.equals("19999")) {

                                                            Intent intent = new Intent(getApplicationContext(), HabitNotificationUpdate.class);
                                                            startActivity(intent);
                                                        }
                                                        if (responceCode.equals("10140")) {

                                                            userSessionManager.logoutUser();
                                                            Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                                            Intent intent = new Intent(getApplicationContext(), Static2Activity.class);
                                                            startActivity(intent);
                                                        }

                                                        if (responceCode.equals("10150")) {

                                                            userSessionManager.logoutUser();
                                                            Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                                            Intent intent = new Intent(getApplicationContext(), Static2Activity.class);
                                                            startActivity(intent);
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }, new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    progressDialog.dismiss();
                                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                                                    } else if (error instanceof AuthFailureError) {

                                                    } else if (error instanceof ServerError) {

                                                    } else if (error instanceof NetworkError) {

                                                    } else if (error instanceof ParseError) {

                                                    }
                                                }
                                            }) {
                                                @Override
                                                protected Map<String, String> getParams() throws AuthFailureError {
                                                    Map<String, String> params = new HashMap<String, String>();
                                                    params.put("user_id", userID);
                                                    return params;
                                                }
                                            };
                                            RequestQueue requestQueue = Volley.newRequestQueue(Main2Activity.this);
                                            requestQueue.add(stringRequest);
                                        }else {
                                            showInternetStatus();
                                        }
                                        break;
                                    case R.id.nav_item_share:
                                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                                        sharingIntent.setType("text/plain");
                                        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "My Health");
                                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=innasoft.com.mhcone&hl=en");
                                        startActivity(Intent.createChooser(sharingIntent, "Share via"));
                                        break;
                                    case R.id.nav_item_about:
                                        Intent intent3 = new Intent(getApplicationContext(), AboutUs.class);
                                        startActivity(intent3);
                                        break;
                                    case R.id.nav_item_myappointments:
                                        Intent intent4 = new Intent(getApplicationContext(), OrdersList.class);
                                        startActivity(intent4);
                                        break;
                                    case R.id.nav_item_rate:
                                        Uri uri = Uri.parse("market://details?id=innasoft.com.mhcone&hl=en");
                                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                                        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);

                                        try {
                                            startActivity(goToMarket);

                                        } catch (ActivityNotFoundException e) {
                                            startActivity(new Intent(Intent.ACTION_VIEW,
                                                    Uri.parse("https://play.google.com/store/apps/details?id=innasoft.com.mhcone&hl=en")));
                                        }
                                        break;
                                    case R.id.about_txt:
                                        if (NetworkUtil.isConnected(Main2Activity.this)) {
                                            if (userSessionManager.isUserLoggedIn()) {

                                                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.LOGOUT_URL,
                                                        new Response.Listener<String>() {
                                                            @Override
                                                            public void onResponse(String response) {
                                                                Log.d("Purushotham", "Sucesses-MainActivity - Logout : " + response);
                                                                userSessionManager.logoutUser();
                                                                Toast.makeText(getApplicationContext(), "You Logged Out Successfully...!", Toast.LENGTH_SHORT).show();
                                                                Intent intent = new Intent(getApplicationContext(), Static2Activity.class);
                                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                                startActivity(intent);
                                                            }
                                                        },
                                                        new Response.ErrorListener() {
                                                            @Override
                                                            public void onErrorResponse(VolleyError error) {
                                                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                                                                } else if (error instanceof AuthFailureError) {

                                                                } else if (error instanceof ServerError) {

                                                                } else if (error instanceof NetworkError) {

                                                                } else if (error instanceof ParseError) {

                                                                }
                                                            }
                                                        }
                                                ) {
                                                    @Override
                                                    protected Map<String, String> getParams() throws AuthFailureError {

                                                        Map<String, String> params = new HashMap<String, String>();

                                                        params.put("user_id", userID);
                                                        return params;
                                                    }
                                                };
                                                RequestQueue requestQueue = Volley.newRequestQueue(Main2Activity.this);
                                                requestQueue.add(stringRequest);
                                            }
                                        }else {
                                            showInternetStatus();
                                        }
                                        break;
                                    case R.id.about_btn_close:
                                        if (NetworkUtil.isConnected(Main2Activity.this)) {
                                            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.FINDING_ACCOUNT_STATUS, new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String response) {
                                                    progressDialog.dismiss();

                                                    try {
                                                        JSONObject jsonObject = new JSONObject(response);
                                                        String responceCode = jsonObject.getString("status");
                                                        if (responceCode.equals("19999")) {
                                                            progressDialog.show();
                                                            StringRequest stringRequest = new StringRequest(Request.Method.POST,
                                                                    AppUrls.BASE_URL+AppUrls.VIEW_PROFILE_URL,
                                                                    new Response.Listener<String>() {
                                                                        @Override
                                                                        public void onResponse(String response) {


                                                                            try {
                                                                                JSONObject jsonObject = new JSONObject(response);
                                                                                String successCode = jsonObject.getString("status");
                                                                                if (successCode.equals("10190")) {
                                                                                    String profilename, profileage, profilegunder, profileemail, profilephone;
                                                                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                                                                    profilename = jsonObject1.getString("user_name");
                                                                                    profileage = jsonObject1.getString("dob");
                                                                                    profilegunder = jsonObject1.getString("gender");
                                                                                    profileemail = jsonObject1.getString("email");
                                                                                    profilephone = jsonObject1.getString("mobile");
                                                                                    Intent intent = new Intent(getApplicationContext(), ProfileInfo.class);
                                                                                    intent.putExtra("pName", profilename);
                                                                                    intent.putExtra("pAge", profileage);
                                                                                    intent.putExtra("pGunder", profilegunder);
                                                                                    intent.putExtra("pMail", profileemail);
                                                                                    intent.putExtra("pPhone", profilephone);
                                                                                    progressDialog.dismiss();
                                                                                    startActivity(intent);
                                                                                }
                                                                            } catch (JSONException e) {
                                                                                progressDialog.dismiss();
                                                                                e.printStackTrace();
                                                                            }
                                                                        }
                                                                    },
                                                                    new Response.ErrorListener() {
                                                                        @Override
                                                                        public void onErrorResponse(VolleyError error) {
                                                                            progressDialog.dismiss();

                                                                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                                                                            } else if (error instanceof AuthFailureError) {

                                                                            } else if (error instanceof ServerError) {

                                                                            } else if (error instanceof NetworkError) {

                                                                            } else if (error instanceof ParseError) {

                                                                            }
                                                                        }
                                                                    }
                                                            ) {
                                                                @Override
                                                                protected Map<String, String> getParams() throws AuthFailureError {
                                                                    Map<String, String> params = new HashMap<String, String>();
                                                                    params.put("user_id", userID);
                                                                    return params;
                                                                }
                                                            };
                                                            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                                                            requestQueue.add(stringRequest);
                                                        }
                                                        if (responceCode.equals("10140")) {

                                                            userSessionManager.logoutUser();
                                                            Toast.makeText(getApplicationContext(), "Admin Aproved Required....!", Toast.LENGTH_SHORT).show();
                                                            Intent intent = new Intent(getApplicationContext(), Static2Activity.class);
                                                            startActivity(intent);
                                                        }

                                                        if (responceCode.equals("10150")) {

                                                            userSessionManager.logoutUser();
                                                            Toast.makeText(getApplicationContext(), "Your Account is deleted.....!", Toast.LENGTH_SHORT).show();
                                                            Intent intent = new Intent(getApplicationContext(), Static2Activity.class);
                                                            startActivity(intent);
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }


                                                }
                                            }, new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    progressDialog.dismiss();
                                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                                                    } else if (error instanceof AuthFailureError) {

                                                    } else if (error instanceof ServerError) {

                                                    } else if (error instanceof NetworkError) {

                                                    } else if (error instanceof ParseError) {

                                                    }
                                                }
                                            }) {
                                                @Override
                                                protected Map<String, String> getParams() throws AuthFailureError {
                                                    Map<String, String> params = new HashMap<String, String>();
                                                    params.put("user_id", userID);
                                                    return params;
                                                }
                                            };
                                            RequestQueue requestQueue = Volley.newRequestQueue(Main2Activity.this);
                                            requestQueue.add(stringRequest);
                                        }else {
                                            showInternetStatus();
                                        }
                                        break;
                                    default:
                                        break;

                                }
                            }
                        }, 400);


                        mDrawerLayout.closeDrawers();
                        return true;
                    }
                });
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }


    private void gettingProfileInformation(final String userID)
    {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_PROFILE_INFO,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Purushotham","Sucesses-Main2Activity - GETTINGPROFILE : "+response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("my-pic");

                                String userNameStringFromUrl = jsonObject1.getString("user_name");
                                String emailStringUrl = jsonObject1.getString("email");
                                String imageURL = AppUrls.BASE_URL+jsonObject1.getString("profile_pic");

                                nameProfile.setText(userNameStringFromUrl);
                                emailProfile.setText(emailStringUrl);
                                Picasso.with(getApplicationContext())
                                        .load(imageURL)
                                        .placeholder(R.drawable.myhealthlogo)
                                        .into(imageViewProfilePick);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                Log.d("IDFIND", userID);
                params.put("user_id", userID);
                return params;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(Main2Activity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed()
    {
        FragmentManager manager = getSupportFragmentManager();

        if (exit) {

            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            toast.cancel();
            startActivity(intent);
            moveTaskToBack(true);

        } else {

            LayoutInflater inflater = getLayoutInflater();
            View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
            TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
            textView.setTypeface(typeface);
            textView.setText("Press Back again to Exit.");
            toast = new Toast(getApplicationContext());
            manager.popBackStack();
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(toastLayout);
            toast.show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }
    }

    @Override
    public void onClick(View v) {
        if(v == editImageButton)
        {
            if(isOnline) {

                final String[] country = {"Select from Gallery", "Take a Photo"};
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Select One")
                        .setItems(country, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                String selectedCountry = country[i];

                                boolean result = ImagePermissions.checkPermission(Main2Activity.this);

                                if (selectedCountry.equals("Take a Photo")) {
                                    userChoosenTask = "Take a Photo";
                                    if (result)
                                        cameraIntent();

                                } else if (selectedCountry.equals("Select from Gallery")) {
                                    userChoosenTask = "Select from Gallery";
                                    if (result)
                                        galleryIntent();

                                }

                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();
            }else
                showInternetStatus();
        }
    }


    private void showInternetStatus()
    {

        LayoutInflater inflater = getLayoutInflater();
        View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
        TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
        textView.setTypeface(typeface);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(toastLayout);
        toast.show();

    }



    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),GALLERY_REQUEST_IMAGE);



    }

    private void cameraIntent()
    {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file=getOutputMediaFile(1);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, CAMERA_REQUEST_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST_IMAGE) {
                try {
                    onSelectFromGalleryResult(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            else if (requestCode == CAMERA_REQUEST_IMAGE) {
                try {
                    onCaptureImageResult(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private void onCaptureImageResult(Intent data) throws IOException {


        selectedFilePath = FilePath.getPath(this, fileUri);
        if (selectedFilePath != null && !selectedFilePath.equals("")) {

            try {
                File f = new File(selectedFilePath);
                ExifInterface exif = new ExifInterface(f.getPath());
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                int angle = 0;

                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                    angle = 90;
                }
                else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                    angle = 180;
                }
                else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                    angle = 270;
                }

                Matrix mat = new Matrix();
                mat.postRotate(angle);

                Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, null);
                Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);
                imageViewProfilePick.setImageBitmap(correctBmp);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                correctBmp.compress(Bitmap.CompressFormat.JPEG, 70, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                final  String sendingimagepath = destination.getPath();

                Thread thread=new Thread(new Runnable(){
                    public void run(){
                        String responce_value =  doFileUpload(sendingimagepath);
                        try {
                            JSONObject jsonObject = new JSONObject(responce_value);
                            String responceCode = jsonObject.getString("status");
                            if(responceCode.equals("2000"))
                            {
                                runOnUiThread(new Runnable(){
                                    public void run() {
                                        try {
                                            runOnUiThread(new Runnable(){
                                                public void run() {
                                                    if(progressDialog.isShowing())
                                                        progressDialog.dismiss();
                                                }
                                            });

                                            Toast.makeText(getApplicationContext(), "Successfully Uploaded...!", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(Main2Activity.this, Main2Activity.class);
                                            startActivity(intent);


                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }


                                });
                            }
                            if(responceCode.equals("5000"))
                            {
                                runOnUiThread(new Runnable(){
                                    public void run() {
                                        try {
                                            runOnUiThread(new Runnable(){
                                                public void run() {
                                                    if(progressDialog.isShowing())
                                                        progressDialog.dismiss();
                                                }
                                            });



                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }


                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                        Log.d("RASPONCEDATAVALUE" , responce_value);


                    }
                });
                thread.start();
            }
            catch (IOException e) {
                Log.w("TAG", "-- Error in setting image");
            }
            catch(OutOfMemoryError oom) {
                Log.w("TAG", "-- OOM Error in setting image");
            }


        }


    }

    private String doFileUpload(String selectedFilePath) {

        String response_str = null;
        File file = null;
        FileBody bin = null;
        file = new File(selectedFilePath);
        String urlString = AppUrls.UPDATE_PROFILE_PIC;
        try
        {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(urlString);
            bin = new FileBody(file);
            MultipartEntity reqEntity = new MultipartEntity();

            reqEntity.addPart("user_id", new StringBody(userID));
            reqEntity.addPart("userfile", bin);

            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post);
            resEntity = response.getEntity();
            response_str = EntityUtils.toString(resEntity);

        }
        catch (Exception ex){
            Log.e("Debug", "error: " + ex.getMessage(), ex);
        }
        return response_str;
    }



    private void onSelectFromGalleryResult(Intent data) throws IOException {


       /* Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Uri selectedFileUri = data.getData();
        selectedFilePath = FilePath.getPath(this, selectedFileUri);
        Log.i(TAG, "Selected File Path:" + selectedFilePath);
        Thread thread=new Thread(new Runnable(){
            public void run(){
                String responce_value =  doFileUpload(selectedFilePath);
                try {
                    JSONObject jsonObject = new JSONObject(responce_value);
                    String responceCode = jsonObject.getString("status");
                    if(responceCode.equals("2000"))
                    {
                        runOnUiThread(new Runnable(){
                            public void run() {
                                try {
                                    runOnUiThread(new Runnable(){
                                        public void run() {
                                            if(progressDialog.isShowing())
                                                progressDialog.dismiss();
                                        }
                                    });
                                    Toast.makeText(getApplicationContext(), "Successfully Uploaded...!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(MainActivity.this, MainActivity.class);
                                    startActivity(intent);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }


                        });
                    }
                    if(responceCode.equals("5000"))
                    {
                        runOnUiThread(new Runnable(){
                            public void run() {
                                try {
                                    runOnUiThread(new Runnable(){
                                        public void run() {
                                            if(progressDialog.isShowing())
                                                progressDialog.dismiss();
                                        }
                                    });



                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }


                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d("RASPONCEDATAVALUE" , responce_value);

            }
        });
        thread.start();
        String name = data.getData().getPath();
        imageViewProfilePick.setImageBitmap(bm);*/




        Uri selectedFileUri = data.getData();
        selectedFilePath = FilePath.getPath(this, selectedFileUri);
        if (selectedFilePath != null && !selectedFilePath.equals("")) {

            try {
                File f = new File(selectedFilePath);
                ExifInterface exif = new ExifInterface(f.getPath());
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                int angle = 0;

                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                    angle = 90;
                }
                else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                    angle = 180;
                }
                else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                    angle = 270;
                }

                Matrix mat = new Matrix();
                mat.postRotate(angle);

                Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, null);
                Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);
                imageViewProfilePick.setImageBitmap(correctBmp);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                correctBmp.compress(Bitmap.CompressFormat.JPEG, 70, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                final  String sendingimagepath = destination.getPath();

                Thread thread=new Thread(new Runnable(){
                    public void run(){
                        String responce_value =  doFileUpload(sendingimagepath);
                        try {
                            JSONObject jsonObject = new JSONObject(responce_value);
                            String responceCode = jsonObject.getString("status");
                            if(responceCode.equals("2000"))
                            {
                                runOnUiThread(new Runnable(){
                                    public void run() {
                                        try {
                                            runOnUiThread(new Runnable(){
                                                public void run() {
                                                    if(progressDialog.isShowing())
                                                        progressDialog.dismiss();
                                                }
                                            });

                                            Toast.makeText(getApplicationContext(), "Successfully Uploaded...!", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(Main2Activity.this, Main2Activity.class);
                                            startActivity(intent);


                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }


                                });
                            }
                            if(responceCode.equals("5000"))
                            {
                                runOnUiThread(new Runnable(){
                                    public void run() {
                                        try {
                                            runOnUiThread(new Runnable(){
                                                public void run() {
                                                    if(progressDialog.isShowing())
                                                        progressDialog.dismiss();
                                                }
                                            });



                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }


                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                        Log.d("RASPONCEDATAVALUE" , responce_value);


                    }
                });
                thread.start();
            }
            catch (IOException e) {
                Log.w("TAG", "-- Error in setting image");
            }
            catch(OutOfMemoryError oom) {
                Log.w("TAG", "-- OOM Error in setting image");
            }


        }
    }




    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case ImagePermissions.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {

                }
                break;
        }
    }


    private static File getOutputMediaFile(int type) {


        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), Config.IMAGE_DIRECTORY_NAME);


        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create " + Config.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }


        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    private Uri getOutputMediaFileUri(int mediaTypeVideo) {

        return Uri.fromFile(getOutputMediaFile(mediaTypeVideo));
    }

}
