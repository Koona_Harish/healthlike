package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.SubPackagesAdapter;
import innasoft.com.mhcone.adapters.SubTestAdapter;
import innasoft.com.mhcone.holders.SubPackagesHolder;
import innasoft.com.mhcone.models.PackagesModel;
import innasoft.com.mhcone.models.SubPackagesModel;
import innasoft.com.mhcone.models.SubTestModel;
import innasoft.com.mhcone.models.TestsModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.GPSTrackers;
import innasoft.com.mhcone.utilities.StartLocationAlert;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class SubPackagesActivity extends AppCompatActivity implements SubPackagesHolder.MyViewHolder, SwipeRefreshLayout.OnRefreshListener {

    private static final int SHOW_LABS_REQ_CODE = 27;
    ImageButton closeButton;
    PackagesModel packagesModel;
    TextView display_package_name,noInternetConnect;
    Typeface typeface,typebold;
    ProgressDialog pDialog;
    ArrayList<SubPackagesModel> subPackageModel;
    SubPackagesAdapter adapter;
    RecyclerView subPackagerecyclerView;
    String package_id, package_name;
    Button findLabs;
    GPSTrackers gps;
    StartLocationAlert startLocationAlert;
    UserSessionManager userSessionManager;
    String access_key, disp_userName, disp_email;
    String userID;
    private SwipeRefreshLayout mSwipeRefreshLayout;



    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.sendbird_slide_in_from_bottom, R.anim.sendbird_slide_out_to_top);
        setContentView(R.layout.activity_sub_packages);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typebold = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.admin_sub_packages_swipe_layout);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Bundle bundle = getIntent().getExtras();
        packagesModel = (PackagesModel) bundle.getSerializable("package_id");

        display_package_name = (TextView) findViewById(R.id.txt_package_name);
        noInternetConnect = (TextView) findViewById(R.id.noInternetConnect);
        noInternetConnect.setTypeface(typeface);


        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,

                android.R.color.holo_green_light,

                android.R.color.holo_orange_light,

                android.R.color.holo_red_light);

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = userSessionManager.getUserDetails();
        access_key = user.get(UserSessionManager.KEY_ACCSES);

        disp_userName = user.get(UserSessionManager.USER_NAME);
        disp_email = user.get(UserSessionManager.USER_EMAIL);
        userID = user.get(UserSessionManager.USER_ID);

        closeButton = (ImageButton) findViewById(R.id.sub_pakg_btn_close);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setProgressStyle(R.style.DialogTheme);
        pDialog.setCancelable(false);



        display_package_name.setTypeface(typeface);
        display_package_name.setText(packagesModel.getP_name());
        String name = packagesModel.getP_name();
        getSupportActionBar().setTitle(name);

        package_id = packagesModel.getP_id();
        package_name = packagesModel.getP_name();


        findLabs = (Button) findViewById(R.id.btnSelectLabPack);
        findLabs.setTypeface(typebold);


        subPackageModel = new ArrayList<SubPackagesModel>();

        gettingSubPackagesList();


        closeButton = (ImageButton) findViewById(R.id.sub_pakg_btn_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(this);

        findLabs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SubPackagesActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        pDialog.show();

                    }
                });
                noInternetConnect.setVisibility(View.INVISIBLE);

                gps = new GPSTrackers(SubPackagesActivity.this);

                if(gps.canGetLocation()) {

                    pDialog.dismiss();

                    List<String> subtestNames = new ArrayList<String>();

                   for (int i = 0; i < subPackageModel.size(); i++)
                   {
                       subtestNames.add(subPackageModel.get(i).getSub_package_name());
                   }



                    ArrayList<String> packagesTestIdsList = new ArrayList<String>();


                    for (int i = 0 ; i < subPackageModel.size(); i++)
                    {
                        packagesTestIdsList.add(subPackageModel.get(i).getPackage_id()+"-"+subPackageModel.get(i).getTest_id()+"-"+subPackageModel.get(i).getSub_test_id());
                    }


                    Intent intent = new Intent(getApplicationContext(), ShowLabsPackage.class);
                    intent.putExtra("packageId", subPackageModel.get(0).getPackage_id());
                    intent.putExtra("packageName", package_name);
                    intent.putExtra("packTestandSubIDswithformat", packagesTestIdsList.toString().replaceAll("\\[", "").replaceAll("\\]","").replaceAll(" ", ""));


                    intent.putExtra("subtestNames", subtestNames.toString());
                    startActivity(intent);

                }
                else {
                    pDialog.dismiss();
                    startLocationAlert = new StartLocationAlert(SubPackagesActivity.this);
                }
            }
        });



    }

    private void gettingSubPackagesList() {
        noInternetConnect.setVisibility(View.INVISIBLE);
        SubPackagesActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                pDialog.show();

            }
        });
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.SUB_PACKAGES_URL,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        subPackageModel.clear();


                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            if(jsonArray.length() != 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("package-sub");
                                    SubPackagesModel spm = new SubPackagesModel();
                                    spm.setSub_package_id(jsonObject1.getString("id"));
                                    spm.setSub_package_name(jsonObject1.getString("sub_test_name"));
                                    spm.setTest_id(jsonObject1.getString("test_id"));
                                    spm.setSub_test_id(jsonObject1.getString("sub_test_id"));

                                    spm.setPackage_id(jsonObject1.getString("package_id"));
                                    spm.setPackage_name(jsonObject1.getString("package_name"));
                                    spm.setSub_package_status(jsonObject1.getString("status"));

                                    subPackageModel.add(spm);
                                }
                                subPackagerecyclerView.setAdapter(adapter);
                                SubPackagesActivity.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        pDialog.dismiss();
                                        mSwipeRefreshLayout.setRefreshing(false);

                                    }
                                });
                            }else {
                                SubPackagesActivity.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        pDialog.dismiss();
                                        mSwipeRefreshLayout.setRefreshing(false);

                                    }
                                });
                                noInternetConnect.setText("No Tests Found");
                                noInternetConnect.setVisibility(View.VISIBLE);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {


                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("package_id", package_id);
                params.put("user_id", userID);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void cardViewListener(int position) {



        LayoutInflater inflater = getLayoutInflater();
        View toastLayout = inflater.inflate(R.layout.custom_toast_package, (ViewGroup) findViewById(R.id.custom_toast_layout));
        TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
        textView.setTypeface(typeface);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(toastLayout);
        toast.show();

    }

    @Override
    public void onRefresh() {
        gettingSubPackagesList();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
