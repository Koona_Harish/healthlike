package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.LabPackagesAdapter;
import innasoft.com.mhcone.holders.LabPackagesHolder;
import innasoft.com.mhcone.models.LabPackagesModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.DividerItemDecoration;
import innasoft.com.mhcone.utilities.UserSessionManager;
import innasoft.com.mhcone.utilities.VerticalSpaceItemDecoration;

public class LabPackages extends AppCompatActivity implements LabPackagesHolder.MyLabPackagesHolder, SwipeRefreshLayout.OnRefreshListener {

    ArrayList<LabPackagesModel> labpackagesList;
    LabPackagesAdapter adapter;
    RecyclerView labpackagesrecyclerview;
    Typeface type_lato, typebold;
    private static final int VERTICAL_ITEM_SPACE = 0;
    TextView labPackages_txt, noInternetConnect;
    ImageButton cancelLabPackage;
    String getLabId, getLabName, getLabPhone;
    String status_payment_lab, status_payment_online, status_tele_booking;
    SearchView searchView;
    ProgressDialog progressDialog;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    ImageView no_labs_found_iv;
    SearchManager manager;
    SearchView search;
    UserSessionManager userSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lab_packages);

        type_lato = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typebold = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Lab Packages");

        userSessionManager = new UserSessionManager(getApplicationContext());

        progressDialog = new ProgressDialog(LabPackages.this, R.style.DialogTheme);
        progressDialog.setMessage("Please wait......");

        Bundle bundle = getIntent().getExtras();
        getLabId = bundle.getString("labId");
        getLabName = bundle.getString("labName");
        getLabPhone = bundle.getString("labPhone");

        status_payment_lab = bundle.getString("payment_lab");
        status_payment_online = bundle.getString("payment_online");
        status_tele_booking = bundle.getString("tele_booking");

        labPackages_txt = (TextView) findViewById(R.id.lab_packages_txt);
        labPackages_txt.setTypeface(typebold);
        labPackages_txt.setText(getLabName);
        noInternetConnect = (TextView) findViewById(R.id.noInternetConnect);
        noInternetConnect.setTypeface(type_lato);

        no_labs_found_iv = (ImageView) findViewById(R.id.no_labs_found_iv);

        searchView = (SearchView) findViewById(R.id.mSearchpacks_lab_packages);

        cancelLabPackage = (ImageButton) findViewById(R.id.lab_packages_cancel_btn);
        cancelLabPackage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        labpackagesList = new ArrayList<LabPackagesModel>();
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.labs_packages_activity_swipe_layout);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,

                android.R.color.holo_green_light,

                android.R.color.holo_orange_light,

                android.R.color.holo_red_light);


        labpackagesrecyclerview = (RecyclerView) findViewById(R.id.lab_packages_recyclerview);
        labpackagesrecyclerview.setHasFixedSize(true);
        labpackagesrecyclerview.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        labpackagesrecyclerview.addItemDecoration(new DividerItemDecoration(getApplicationContext()));
        labpackagesrecyclerview.addItemDecoration(new DividerItemDecoration(getApplicationContext(), R.drawable.recycler_divider));
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        labpackagesrecyclerview.setLayoutManager(layoutManager);

        adapter = new LabPackagesAdapter(labpackagesList, LabPackages.this, R.layout.lab_packages_row2);
        getLabPackages();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    private void getLabPackages() {
        noInternetConnect.setVisibility(View.GONE);
        no_labs_found_iv.setVisibility(View.GONE);
        progressDialog.show();
        labpackagesList.clear();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_PACKAGES_USING_LAB_ID, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    Log.d("PURUSHOTHAMMMsfafaf", response);

                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equals("19999")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        if (jsonArray.length() != 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("lab");
                                LabPackagesModel lpm = new LabPackagesModel();
                                lpm.setLab_packages_id(jsonObject1.getString("id"));
                                lpm.setLab_packages_package_id(jsonObject1.getString("package_id"));
                                Log.d("INPUTVALUE", "" + jsonObject1.getString("sub_test_id"));
                                lpm.setLab_packages_sub_test_id(jsonObject1.getString("sub_test_id"));
                                lpm.setLab_packages_final_price(jsonObject1.getString("final_price"));
                                lpm.setLab_packages_package_name(jsonObject1.getString("package_name"));
                                lpm.setLab_packages_sub_test(jsonObject1.getString("sub_test"));
                                lpm.setLab_packages_package_image(AppUrls.IMAGE_URL + jsonObject1.getString("package_image"));
                                lpm.setLab_packages_test_id(jsonObject1.getString("package_sub_id"));
                                lpm.setLab_packages_lab_price(jsonObject1.getString("mrp_price"));
                                labpackagesList.add(lpm);
                            }
                            labpackagesrecyclerview.setAdapter(adapter);
                            progressDialog.dismiss();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    } else {
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                        no_labs_found_iv.setVisibility(View.VISIBLE);
                        noInternetConnect.setText("No Packages found You Selected Lab");
                        noInternetConnect.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                noInternetConnect.setText("No Packages found You Selected Lab");
                noInternetConnect.setVisibility(View.VISIBLE);

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("LABIDVALUE", getLabId);
                params.put("lab_id", getLabId);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void cardViewListener(int position) {
        if (!userSessionManager.isUserLoggedIn()) {

            Toast.makeText(getApplicationContext(), "Please Login...!", Toast.LENGTH_SHORT).show();
            Intent loginIntent = new Intent(LabPackages.this, LoginActivity.class);
            loginIntent.putExtra("Login", "other");
            startActivity(loginIntent);
        } else {
            Intent intent = new Intent(LabPackages.this, DummyLabBillingActivity.class);
            intent.putExtra("selecteLabId", getLabId);
            intent.putExtra("selectedLabName", getLabName);
            intent.putExtra("subTestName", labpackagesList.get(position).getLab_packages_package_name());
            intent.putExtra("subTestsNAMES", labpackagesList.get(position).getLab_packages_sub_test());
            intent.putExtra("packageName", labpackagesList.get(position).getLab_packages_package_name());
            intent.putExtra("selectedLabPrice", labpackagesList.get(position).getLab_packages_final_price());
            intent.putExtra("selectedLabPhone", getLabPhone);
            intent.putExtra("payment_lab", status_payment_lab);
            intent.putExtra("payment_online", status_payment_online);
            intent.putExtra("tele_booking", status_tele_booking);
            intent.putExtra("package_id", labpackagesList.get(position).getLab_packages_package_id());

            ArrayList<String> sendArrayListData = new ArrayList<String>();
            ArrayList<String> sendArraySubTestIds = new ArrayList<String>();

            for (int i = 0; i < labpackagesList.size(); i++) {
                Log.d("SUBTESTIDFINDOUT", labpackagesList.get(i).getLab_packages_sub_test_id());
                sendArraySubTestIds.add(labpackagesList.get(i).getLab_packages_sub_test_id());
                sendArrayListData.add(labpackagesList.get(position).getLab_packages_package_id() + "-0-"
                        + labpackagesList.get(i).getLab_packages_sub_test_id() + "-" + labpackagesList.get(i).getLab_packages_test_id());
            }

            Log.d("SENDINGVALUES", sendArrayListData.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", ""));
            intent.putExtra("packTestandSubIDswithformat", sendArrayListData.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", ""));
            intent.putExtra("subtestIds", sendArraySubTestIds.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", ""));
            startActivity(intent);
        }
    }

    @Override
    public void onRefresh() {
        getLabPackages();
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.search_labpackage_list, menu);

        manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        search = (SearchView) menu.findItem(R.id.lab_list_search).getActionView();

        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });


        return true;
    }*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

