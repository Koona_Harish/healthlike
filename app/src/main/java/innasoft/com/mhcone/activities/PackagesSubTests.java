package innasoft.com.mhcone.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.adapters.MyExpandablePackageListAdapter;
import innasoft.com.mhcone.models.MainPackageModel;
import innasoft.com.mhcone.models.MainSubPackageModel;
import innasoft.com.mhcone.models.PackagesModel;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.ConnectionDetector;
import innasoft.com.mhcone.utilities.GPSTrackers;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.StartLocationAlert;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class PackagesSubTests extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    public ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<MainSubPackageModel> subItemList = null;
    List<MainPackageModel> superitemList = null;
    MainSubPackageModel subtestModel = null;
    MainPackageModel superItem = null;
    Button btnSelectLabPack, clear_data;
    //DBHandler db;
    ProgressDialog progressDialog;
    ConnectionDetector cd;
    GPSTrackers gps;
    StartLocationAlert startLocationAlert;
    private int lastExpandedPosition = -1;
    Typeface typeface;
    TextView defaultTExt, noInternetConnection, display_package_name;
    UserSessionManager userSessionManager;
    String access_key, disp_userName, disp_email;
    String userID, package_id, package_name;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    View view;
    ArrayList<String> packagesTestIdsList = new ArrayList<String>();
    ArrayList<String> subTestIdsList = new ArrayList<String>();
    ArrayList<String> subtestNames = new ArrayList<String>();
    NetworkStatus ns, ns2, ns3;
    Boolean isOnline = false, isOnline2 = false, isOnline3 = false;

    PackagesModel packagesModel;
    ImageButton closeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_packages_sub_tests);

        Bundle bundle = getIntent().getExtras();
        package_id = bundle.getString("package_id");
        ;
        package_name = bundle.getString("package_name");
        ;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String p_name = package_name.substring(0, 1).toUpperCase() + package_name.substring(1);
        getSupportActionBar().setTitle(p_name);

        btnSelectLabPack = (Button) findViewById(R.id.btnSelectLabPack);

        Log.d("PACKINFO:", package_id + " / " + package_name);

        typeface = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> user = userSessionManager.getUserDetails();
        access_key = user.get(UserSessionManager.KEY_ACCSES);

        disp_userName = user.get(UserSessionManager.USER_NAME);
        disp_email = user.get(UserSessionManager.USER_EMAIL);
        userID = user.get(UserSessionManager.USER_ID);

        display_package_name = (TextView) findViewById(R.id.txt_package_name);
        display_package_name.setTypeface(typeface);
        //display_package_name.setText(package_name);

        defaultTExt = (TextView) findViewById(R.id.nodefaultText);
        noInternetConnection = (TextView) findViewById(R.id.nonoInternetConnect);
        closeButton = (ImageButton) findViewById(R.id.sub_pakg_btn_close);

        ns = new NetworkStatus();
        isOnline = ns.isOnline(this);

        progressDialog = new ProgressDialog(PackagesSubTests.this, R.style.DialogTheme);
        // mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.admin_sub_packages_swipe_layout);
      /*  mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,

                android.R.color.holo_green_light,

                android.R.color.holo_orange_light,

                android.R.color.holo_red_light);*/


        progressDialog.setMessage("Please wait......");
        progressDialog.setCancelable(false);

        expandableListView = (ExpandableListView) findViewById(R.id.expandableListViewPackages);

        if (isOnline) {
            fillData();
        } else {
            showInternetStatus2();
        }

        // mSwipeRefreshLayout.setOnRefreshListener(this);
        closeButton.setOnClickListener(this);
        btnSelectLabPack.setOnClickListener(this);
    }

    private void fillData() {
        if (isOnline) {
            progressDialog.show();
            noInternetConnection.setVisibility(View.INVISIBLE);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.GETTING_SUBPACK_SUBTESTS,

                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {


                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                superitemList = new ArrayList<MainPackageModel>();
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if (jsonArray.length() != 0) {
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        superItem = new MainPackageModel();
                                        subItemList = new ArrayList<MainSubPackageModel>();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        String data = jsonObject1.getString("package_sub_name");
                                        String id = jsonObject1.getString("package_sub_id");


                                        superItem.setPackage_sub_name(data);
                                        superItem.setPackage_sub_id(id);


                                        JSONArray jsonArray1 = jsonObject1.getJSONArray("sub_test_wise");
                                        if (jsonArray1 != null && jsonArray1.length() != 0) {
                                            for (int j = 0; j < jsonArray1.length(); j++) {

                                                subtestModel = new MainSubPackageModel();

                                                JSONObject jsonObject2 = jsonArray1.getJSONObject(j);
                                                String sub_test_name = jsonObject2.getString("sub_test_name");
                                                String package_id = jsonObject2.getString("package_id");
                                                String package_sub_id = jsonObject2.getString("package_sub_id");
                                                String sub_test_id = jsonObject2.getString("sub_test_id");
                                                Log.d("MYVALUESE", "" + package_id + "-0-" + sub_test_id + "-" + package_sub_id);
                                                packagesTestIdsList.add(package_id + "-0-" + sub_test_id + "-" + package_sub_id);
                                                subTestIdsList.add(sub_test_id);
                                                subtestNames.add(sub_test_name);
                                                subtestModel.setSub_package_name(sub_test_name);
                                                subtestModel.setPackage_id(package_id);
                                                subtestModel.setSub_package_id(package_sub_id);
                                                subtestModel.setPackage_sub_id(sub_test_id);
                                                subItemList.add(subtestModel);
                                            }
                                            superItem.setSubPackageModel(subItemList);
                                            superitemList.add(superItem);
                                            progressDialog.dismiss();
                                            //    mSwipeRefreshLayout.setRefreshing(false);
                                        }

                                    }

                                    expandableListAdapter = new MyExpandablePackageListAdapter(PackagesSubTests.this, superitemList);
                                    expandableListView.setAdapter(expandableListAdapter);
                                    expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                                        @Override
                                        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                                            return false;
                                        }
                                    });
                                } else {
                                    progressDialog.dismiss();
                                    // mSwipeRefreshLayout.setRefreshing(false);
                                    noInternetConnection.setTypeface(typeface);
                                    noInternetConnection.setText("No Tests Found");
                                    noInternetConnection.setVisibility(View.VISIBLE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            progressDialog.dismiss();
                            //  mSwipeRefreshLayout.setRefreshing(false);
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }

                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("package_id", package_id);
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);
        } else {
            progressDialog.dismiss();
            // mSwipeRefreshLayout.setRefreshing(false);
            noInternetConnection.setTypeface(typeface);
            noInternetConnection.setVisibility(View.VISIBLE);

        }
    }


    private void showInternetStatus2() {
        progressDialog.dismiss();
        // mSwipeRefreshLayout.setRefreshing(false);
        noInternetConnection.setTypeface(typeface);
        noInternetConnection.setVisibility(View.VISIBLE);

    }

    @Override
    public void onRefresh() {
        ns2 = new NetworkStatus();
        isOnline2 = ns2.isOnline(this);
        if (isOnline2) {
            progressDialog.show();
            //mSwipeRefreshLayout.setRefreshing(false);
            noInternetConnection.setVisibility(View.INVISIBLE);
            fillData();
        } else {
            //  mSwipeRefreshLayout.setRefreshing(false);
            progressDialog.dismiss();
            //   mSwipeRefreshLayout.setRefreshing(false);
            noInternetConnection.setTypeface(typeface);
            noInternetConnection.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public void onClick(View view) {
        if (view == closeButton) {
            finish();
        }
        if (view == btnSelectLabPack) {

            Log.d("SUBTESTPRINT", subTestIdsList.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", ""));

            Intent intent = new Intent(getApplicationContext(), ShowLabsPackage.class);
            intent.putExtra("packageId", package_id);
            intent.putExtra("packageName", package_name);
            intent.putExtra("packTestandSubIDswithformat", packagesTestIdsList.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", ""));
            intent.putExtra("subTestIdsList", subTestIdsList.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", ""));
            intent.putExtra("subtestNames", subtestNames.toString());
            startActivity(intent);
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
