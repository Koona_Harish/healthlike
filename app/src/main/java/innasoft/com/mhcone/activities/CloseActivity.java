package innasoft.com.mhcone.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import innasoft.com.mhcone.R;

public class CloseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_close);

        if (getIntent().getExtras() != null && getIntent().getExtras().getBoolean("EXIT", true)) {
            finish();
        }
    }
}
