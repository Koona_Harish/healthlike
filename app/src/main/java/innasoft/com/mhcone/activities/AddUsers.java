package innasoft.com.mhcone.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import innasoft.com.mhcone.R;
import innasoft.com.mhcone.utilities.AppUrls;
import innasoft.com.mhcone.utilities.NetworkStatus;
import innasoft.com.mhcone.utilities.UserSessionManager;

public class AddUsers extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    ImageButton closeWindow;
    TextInputLayout au_name_til, au_email_til, au_phone_til;
    EditText au_name_et, au_email_et, au_phone_et;
    RadioGroup au_rg;
    RadioButton au_male_rb, au_female_rb, genderStatus;
    Button btnAdd, btnCancel;
    String s, send_gender;
    int gender_flag = 0;
    String user_id;
    Spinner spinner;
    String spinnerString;
    UserSessionManager userSessionManager;
    TextView selectdob, i_am_txt, disdob, dispdob, txt_package_name;
    ImageView dobPicker;
    RelativeLayout spinner_img;
    NetworkStatus ns;
    Boolean isOnline = false;
    Typeface typeface, typeface2;
    private int mYear, mMonth, mDay;
    ProgressDialog progressDialog;
    public String strName, strEmail, strMobile, fromScreen = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.sendbird_slide_in_from_bottom, R.anim.sendbird_slide_out_to_top);
        setContentView(R.layout.activity_add_users_dummy);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add I Care For");

        typeface2 = Typeface.createFromAsset(this.getAssets(), "robotoregular.ttf");
        typeface = Typeface.createFromAsset(this.getAssets(), "robotobold.ttf");

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        i_am_txt = (TextView) findViewById(R.id.i_am_txt);
        i_am_txt.setTypeface(typeface2);
        selectdob = (TextView) findViewById(R.id.selectdob);
        selectdob.setTypeface(typeface2);
        txt_package_name = (TextView) findViewById(R.id.txt_package_name);
        txt_package_name.setTypeface(typeface2);
        dispdob = (TextView) findViewById(R.id.dobdisp);
        dispdob.setTypeface(typeface2);
        /*disdob = (TextView) findViewById(R.id.dobdis);
        disdob.setTypeface(typeface2);*/
        dobPicker = (ImageView) findViewById(R.id.dobPicker);
        spinner_img = (RelativeLayout) findViewById(R.id.spinner_img);

        progressDialog = new ProgressDialog(AddUsers.this, R.style.DialogTheme);
        progressDialog.setMessage("Please wait......");

        ns = new NetworkStatus();
        isOnline = ns.isOnline(AddUsers.this);

        if (getIntent().getExtras() != null)
            fromScreen = getIntent().getStringExtra("From");

        spinner = (Spinner) findViewById(R.id.spinner_relation);

        closeWindow = (ImageButton) findViewById(R.id.add_user_close);
        closeWindow.setOnClickListener(this);

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = userSessionManager.getUserDetails();
        user_id = user.get(UserSessionManager.USER_ID);

        au_name_til = (TextInputLayout) findViewById(R.id.name);
        au_name_til.setTypeface(typeface2);
        au_email_til = (TextInputLayout) findViewById(R.id.email);
        au_email_til.setTypeface(typeface2);
        au_phone_til = (TextInputLayout) findViewById(R.id.phone);
        au_phone_til.setTypeface(typeface2);

        au_rg = (RadioGroup) findViewById(R.id.gender_select);

        au_male_rb = (RadioButton) findViewById(R.id.gender_male);
        au_male_rb.setTypeface(typeface2);
        au_female_rb = (RadioButton) findViewById(R.id.gender_female);
        au_female_rb.setTypeface(typeface2);

        au_name_et = (EditText) findViewById(R.id.txtname);
        au_name_et.setTypeface(typeface2);
        au_email_et = (EditText) findViewById(R.id.txtemail);
        au_email_et.setTypeface(typeface2);
        au_phone_et = (EditText) findViewById(R.id.txtphone);
        au_phone_et.setTypeface(typeface2);

        au_rg = (RadioGroup) findViewById(R.id.gender_select);

        btnAdd = (Button) findViewById(R.id.add);
        btnAdd.setTypeface(typeface2);
        btnCancel = (Button) findViewById(R.id.au_cancel);
        btnCancel.setTypeface(typeface2);

        btnAdd.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        dobPicker.setOnClickListener(this);
        dispdob.setOnClickListener(this);
        //disdob.setOnClickListener(this);
        au_rg.setOnCheckedChangeListener(this);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClick(View view) {
        if (view == dispdob) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    new DatePickerDialog.OnDateSetListener() {


                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            dispdob.setText(String.valueOf(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year));

                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            Date birthDate = null;
                            try {
                                birthDate = sdf.parse(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);

                                String age = calculateAge(birthDate);

                                String segments[] = age.split(",");
                                if (segments[0].equals("0")) {
                                    if (segments[1].equals("0")) {
                                        if (segments[2].equals("0")) {

                                        }
                                    }
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                    }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }


        if (view == disdob) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    new DatePickerDialog.OnDateSetListener() {


                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            dispdob.setText(String.valueOf(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year));
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            Date birthDate = null;
                            try {
                                birthDate = sdf.parse(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);

                                String age = calculateAge(birthDate);

                                String segments[] = age.split(",");
                                if (segments[0].equals("0")) {
                                    if (segments[1].equals("0")) {
                                        if (segments[2].equals("0")) {

                                        }
                                    }
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                    }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }


        if (view == dobPicker) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            dispdob.setText(String.valueOf(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year));
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            Date birthDate = null;
                            try {
                                birthDate = sdf.parse(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);

                                String age = calculateAge(birthDate);

                                String segments[] = age.split(",");
                                if (segments[0].equals("0")) {
                                    if (segments[1].equals("0")) {
                                        if (segments[2].equals("0")) {

                                        }
                                    }
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }
        if (view == btnAdd) {
            if (isOnline) {
                if (au_male_rb.isChecked() || au_female_rb.isChecked()) {
                    genderStatus = (RadioButton) findViewById(au_rg.getCheckedRadioButtonId());

                    if (genderStatus.getId() == au_male_rb.getId()) {
                        gender_flag = 1;
                        send_gender = "male";
                    }

                    if (genderStatus.getId() == au_female_rb.getId()) {
                        gender_flag = 1;
                        send_gender = "female";
                    }
                }
                if (validate()) {
                    if (dispdob.getText().length() != 0) {
                        //if ((dispdob.getText().toString().trim().length() != 0 || !dispdob.getText().toString().equals(null)))
                        if (gender_flag == 1) {
                            if (spinnerString != null && !spinnerString.isEmpty() && !spinnerString.equals("Select Relation")) {
                                final String strName, strEmail, strPhone;

                                strName = au_name_et.getText().toString();
                                strEmail = au_email_et.getText().toString();
                                strPhone = au_phone_et.getText().toString();

                                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.ADD_SUB_USER_URL,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {

                                                try {
                                                    JSONObject jsonObject = new JSONObject(response);
                                                    String statusCode = jsonObject.getString("status");
                                                    if (statusCode.equals("20100")) {
                                                        progressDialog.dismiss();
                                                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                                        String message = jsonObject1.getString("msg");
                                                        Toast.makeText(getApplicationContext(), "You Added a user successfully..!", Toast.LENGTH_SHORT).show();
                                                        if (fromScreen.equalsIgnoreCase("Billing")) {
                                                            finish();
                                                        } else {
                                                            Intent intent = new Intent(getApplicationContext(), ViewUsers.class);
                                                            startActivity(intent);
                                                            finish();
                                                        }
                                                    } else
                                                        progressDialog.dismiss();
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                progressDialog.dismiss();

                                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                                } else if (error instanceof AuthFailureError) {

                                                } else if (error instanceof ServerError) {

                                                } else if (error instanceof NetworkError) {

                                                } else if (error instanceof ParseError) {

                                                }
                                            }
                                        }) {
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("email", strEmail);
                                        params.put("user_sub_name", strName);
                                        params.put("relation", spinnerString);
                                        params.put("mobile", strPhone);
                                        params.put("gender", send_gender);
                                        params.put("user_id", user_id);
                                        params.put("dob", dispdob.getText().toString());
                                        return params;
                                    }
                                };
                                RequestQueue requestQueue = Volley.newRequestQueue(AddUsers.this);
                                requestQueue.add(stringRequest);
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Please select Relation....!", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Please provide all the details..!!", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Please Select DOB..!!", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Please provide all the details..!!", Toast.LENGTH_SHORT).show();
                }
            } else {
                LayoutInflater inflater = getLayoutInflater();
                View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
                TextView textView = (TextView) toastLayout.findViewById(R.id.custom_toast_message);
                textView.setTypeface(typeface);
                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(toastLayout);
                toast.show();
            }
        }
        if (view == btnCancel) {
            overridePendingTransition(R.anim.sendbird_slide_out_to_top, R.anim.sendbird_slide_in_from_bottom);
            finish();
        }
        if (view == closeWindow) {
            overridePendingTransition(R.anim.sendbird_slide_out_to_top, R.anim.sendbird_slide_in_from_bottom);
            finish();
        }
    }

    private String calculateAge(Date birthDate) {
        int years = 0;
        int months = 0;
        int days = 0;
        Calendar birthDay = Calendar.getInstance();
        birthDay.setTimeInMillis(birthDate.getTime());
        long currentTime = System.currentTimeMillis();
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(currentTime);
        years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
        int currMonth = now.get(Calendar.MONTH) + 1;
        int birthMonth = birthDay.get(Calendar.MONTH) + 1;
        months = currMonth - birthMonth;
        if (months < 0) {
            years--;
            months = 12 - birthMonth + currMonth;
            if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                months--;
        } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            years--;
            months = 11;
        }
        if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
            days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
        else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            int today = now.get(Calendar.DAY_OF_MONTH);
            now.add(Calendar.MONTH, -1);
            days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
        } else {
            days = 0;
            if (months == 12) {
                years++;
                months = 0;
            }
        }
        return years + "," + months + "," + days;
    }

    private boolean validate() {
        boolean result = true;
        strName = au_name_et.getText().toString().trim();
        strEmail = au_email_et.getText().toString().trim();
        strMobile = au_phone_et.getText().toString().trim();
        if ((strName == null || strName.equals(""))) {
            au_name_til.setError(getString(R.string.invalidData));
            result = false;
        } else
            au_name_til.setErrorEnabled(false);


        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        //

        if (au_email_et.length() != 0) {
            if ((strEmail == null) || (!strEmail.matches(EMAIL_REGEX))) {
                //Log.d("CHECKONETWO","CHECKONETWO");
                au_email_til.setError(getString(R.string.invalidEmailID));
                result = false;
            } else
                au_email_til.setErrorEnabled(false);
        }

        if (au_phone_et.length() != 0) {
            if ((strMobile == null || strMobile.equals("")) || strMobile.length() != 10) {
                au_phone_til.setError(getString(R.string.invalidNumber));
                result = false;
            } else
                au_phone_til.setErrorEnabled(false);
        }
        return result;
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {

        if (au_male_rb.isChecked()) {

            List<String> spinnerList = new ArrayList<String>();
            spinnerList.add("Select Relation");
            spinnerList.add("Father");
            spinnerList.add("Brother");
            spinnerList.add("Spouse");
            spinnerList.add("Children");
            spinnerList.add("Others");

            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerList);
            spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(spinnerAdapter);
            spinner.setVisibility(View.VISIBLE);
            spinner_img.setVisibility(View.VISIBLE);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0) {
                        String item = parent.getItemAtPosition(position).toString();
                        spinnerString = item;
                    } else {
                        spinnerString = "Select Relation";
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } else if (au_female_rb.isChecked()) {
            List<String> spinnerList = new ArrayList<String>();
            spinnerList.add("Select Relation");
            spinnerList.add("Mother");
            spinnerList.add("Sister");
            spinnerList.add("Spouse");
            spinnerList.add("Children");
            spinnerList.add("Others");

            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerList);

            spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(spinnerAdapter);
            spinner.setVisibility(View.VISIBLE);
            spinner_img.setVisibility(View.VISIBLE);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0) {
                        String item = parent.getItemAtPosition(position).toString();

                        spinnerString = item;
                    } else {
                        spinnerString = "";
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
