package innasoft.com.mhcone.itemclicklistener;

import android.view.View;

public interface PopularTestItemClickListener
{
    void onItemClick(View v, int pos);
}
