package innasoft.com.mhcone.itemclicklistener;

import android.view.View;

public interface PopularLabItemClickListener
{
    void onItemClick(View v, int pos);
}
