package innasoft.com.mhcone.itemclicklistener;

import android.view.View;

/**
 * Created by Purushotham-Book on 11/14/2016.
 */

public interface BookLabClickListener
{
    void onItemClick(View v, int pos);
}
