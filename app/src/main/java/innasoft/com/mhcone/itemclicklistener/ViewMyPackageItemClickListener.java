package innasoft.com.mhcone.itemclicklistener;

import android.view.View;

/**
 * Created by purushotham on 9/1/17.
 */

public interface ViewMyPackageItemClickListener
{
    void onItemClick(View v, int pos);
}
