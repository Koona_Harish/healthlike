package innasoft.com.mhcone.itemclicklistener;

import android.view.View;


public interface PackageInfoItemClickListener
{
    void onItemClick(View v, int pos);
}
