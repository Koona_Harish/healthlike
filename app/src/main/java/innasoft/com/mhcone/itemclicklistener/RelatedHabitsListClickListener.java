package innasoft.com.mhcone.itemclicklistener;

import android.view.View;

/**
 * Created by androiddev2 on 23/11/16.
 */
public interface RelatedHabitsListClickListener {
    void onItemClick(View v, int pos);
}
