package innasoft.com.mhcone.itemclicklistener;

import android.view.View;

/**
 * Created by purushotham on 27/1/17.
 */

public interface CitiesClickListener
{
    void onItemClick(View v, int pos);
}
