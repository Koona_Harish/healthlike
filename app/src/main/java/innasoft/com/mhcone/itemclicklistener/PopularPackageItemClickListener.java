package innasoft.com.mhcone.itemclicklistener;

import android.view.View;

public interface PopularPackageItemClickListener
{
    void onItemClick(View v, int pos);
}
