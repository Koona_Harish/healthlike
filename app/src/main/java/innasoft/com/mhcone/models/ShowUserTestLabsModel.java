package innasoft.com.mhcone.models;

/**
 * Created by androiddev2 on 6/10/16.
 */

public class ShowUserTestLabsModel {

    public String show_user_test_lab_id;
    public String show_user_test_lab_user_name;
    public String show_user_test_lab_email;
    public String show_user_test_lab_mobile;
    public String show_user_test_lab_role_id;
    public String show_user_test_lab_created_time;
    public String show_user_test_lab_workingdays;
    public String show_user_test_lab_address;
    public String show_user_test_lab_workingtime;
    public String show_user_test_lab_lat;
    public String show_user_test_lab_lang;
    public String show_user_test_lab_home_collections;
    public String show_user_test_lab_test_id;
    public String show_user_test_lab_sub_test_id;
    public String show_user_test_lab_distance;
    public String show_user_test_lab_final_price;
    public String show_user_test_lab_lab_price;
    public String show_user_test_lab_ratting;
    public String show_user_test_lab_payment_online;
    public String show_user_test_user_test_lab_payment_lab;
    public String show_user_test_lab_tele_booking;

    public String certificate;


    public String location;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String profile_pic_url;

    public String getProfile_pic_url() {
        return profile_pic_url;
    }

    public void setProfile_pic_url(String profile_pic_url) {
        this.profile_pic_url = profile_pic_url;
    }

    public String getShow_user_test_lab_ratting() {
        return show_user_test_lab_ratting;
    }

    public void setShow_user_test_lab_ratting(String show_user_test_lab_ratting) {
        this.show_user_test_lab_ratting = show_user_test_lab_ratting;
    }

    public String getShow_user_test_lab_payment_online() {
        return show_user_test_lab_payment_online;
    }

    public void setShow_user_test_lab_payment_online(String show_user_test_lab_payment_online) {
        this.show_user_test_lab_payment_online = show_user_test_lab_payment_online;
    }

    public String getShow_user_test_user_test_lab_payment_lab() {
        return show_user_test_user_test_lab_payment_lab;
    }

    public void setShow_user_test_user_test_lab_payment_lab(String show_user_test_user_test_lab_payment_lab) {
        this.show_user_test_user_test_lab_payment_lab = show_user_test_user_test_lab_payment_lab;
    }

    public String getShow_user_test_lab_tele_booking() {
        return show_user_test_lab_tele_booking;
    }

    public void setShow_user_test_lab_tele_booking(String show_user_test_lab_tele_booking) {
        this.show_user_test_lab_tele_booking = show_user_test_lab_tele_booking;
    }

    public String getShow_user_test_lab_lab_price() {
        return show_user_test_lab_lab_price;
    }

    public void setShow_user_test_lab_lab_price(String show_user_test_lab_lab_price) {
        this.show_user_test_lab_lab_price = show_user_test_lab_lab_price;
    }

    public String getShow_user_test_lab_final_price() {
        return show_user_test_lab_final_price;
    }

    public void setShow_user_test_lab_final_price(String show_user_test_lab_final_price) {
        this.show_user_test_lab_final_price = show_user_test_lab_final_price;
    }

    public String getShow_user_test_lab_id() {
        return show_user_test_lab_id;
    }

    public void setShow_user_test_lab_id(String show_user_test_lab_id) {
        this.show_user_test_lab_id = show_user_test_lab_id;
    }

    public String getShow_user_test_lab_user_name() {
        return show_user_test_lab_user_name;
    }

    public void setShow_user_test_lab_user_name(String show_user_test_lab_user_name) {
        this.show_user_test_lab_user_name = show_user_test_lab_user_name;
    }

    public String getShow_user_test_lab_email() {
        return show_user_test_lab_email;
    }

    public void setShow_user_test_lab_email(String show_user_test_lab_email) {
        this.show_user_test_lab_email = show_user_test_lab_email;
    }

    public String getShow_user_test_lab_mobile() {
        return show_user_test_lab_mobile;
    }

    public void setShow_user_test_lab_mobile(String show_user_test_lab_mobile) {
        this.show_user_test_lab_mobile = show_user_test_lab_mobile;
    }

    public String getShow_user_test_lab_role_id() {
        return show_user_test_lab_role_id;
    }

    public void setShow_user_test_lab_role_id(String show_user_test_lab_role_id) {
        this.show_user_test_lab_role_id = show_user_test_lab_role_id;
    }

    public String getShow_user_test_lab_created_time() {
        return show_user_test_lab_created_time;
    }

    public void setShow_user_test_lab_created_time(String show_user_test_lab_created_time) {
        this.show_user_test_lab_created_time = show_user_test_lab_created_time;
    }

    public String getShow_user_test_lab_workingdays() {
        return show_user_test_lab_workingdays;
    }

    public void setShow_user_test_lab_workingdays(String show_user_test_lab_workingdays) {
        this.show_user_test_lab_workingdays = show_user_test_lab_workingdays;
    }

    public String getShow_user_test_lab_address() {
        return show_user_test_lab_address;
    }

    public void setShow_user_test_lab_address(String show_user_test_lab_address) {
        this.show_user_test_lab_address = show_user_test_lab_address;
    }

    public String getShow_user_test_lab_workingtime() {
        return show_user_test_lab_workingtime;
    }

    public void setShow_user_test_lab_workingtime(String show_user_test_lab_workingtime) {
        this.show_user_test_lab_workingtime = show_user_test_lab_workingtime;
    }

    public String getShow_user_test_lab_lat() {
        return show_user_test_lab_lat;
    }

    public void setShow_user_test_lab_lat(String show_user_test_lab_lat) {
        this.show_user_test_lab_lat = show_user_test_lab_lat;
    }

    public String getShow_user_test_lab_lang() {
        return show_user_test_lab_lang;
    }

    public void setShow_user_test_lab_lang(String show_user_test_lab_lang) {
        this.show_user_test_lab_lang = show_user_test_lab_lang;
    }

    public String getShow_user_test_lab_home_collections() {
        return show_user_test_lab_home_collections;
    }

    public void setShow_user_test_lab_home_collections(String show_user_test_lab_home_collections) {
        this.show_user_test_lab_home_collections = show_user_test_lab_home_collections;
    }

    public String getShow_user_test_lab_test_id() {
        return show_user_test_lab_test_id;
    }

    public void setShow_user_test_lab_test_id(String show_user_test_lab_test_id) {
        this.show_user_test_lab_test_id = show_user_test_lab_test_id;
    }

    public String getShow_user_test_lab_sub_test_id() {
        return show_user_test_lab_sub_test_id;
    }

    public void setShow_user_test_lab_sub_test_id(String show_user_test_lab_sub_test_id) {
        this.show_user_test_lab_sub_test_id = show_user_test_lab_sub_test_id;
    }

    public String getShow_user_test_lab_distance() {
        return show_user_test_lab_distance;
    }

    public void setShow_user_test_lab_distance(String show_user_test_lab_distance) {
        this.show_user_test_lab_distance = show_user_test_lab_distance;
    }
}
