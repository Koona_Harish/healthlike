package innasoft.com.mhcone.models;


public class ShowLabsPackageModel {
    public String show_lab_package_id;
    public String show_lab_package_user_name;
    public String show_lab_package_email;
    public String show_lab_package_mobile;
    public String show_lab_package_role_id;
    public String show_lab_package_created_time;
    public String show_lab_package_workingdays;
    public String show_lab_package_address;
    public String show_lab_package_workingtime;
    public String show_lab_package_lat;
    public String show_lab_package_lang;
    public String show_lab_package_home_collections;
    public String show_lab_package_test_id;
    public String show_lab_package_package_id;
    public String show_lab_package_distance;
    public String show_lab_package_lab_price;

    public String show_lab_ratting;
    public String show_lab_payment_online;
    public String show_lab_payment_lab;
    public String show_lab_tele_booking;
    public String certificate;
    public String location;


    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String profile_pic_url;

    public String getProfile_pic_url() {
        return profile_pic_url;
    }

    public void setProfile_pic_url(String profile_pic_url) {
        this.profile_pic_url = profile_pic_url;
    }

    public String getShow_lab_ratting() {
        return show_lab_ratting;
    }

    public void setShow_lab_ratting(String show_lab_ratting) {
        this.show_lab_ratting = show_lab_ratting;
    }

    public String getShow_lab_payment_online() {
        return show_lab_payment_online;
    }

    public void setShow_lab_payment_online(String show_lab_payment_online) {
        this.show_lab_payment_online = show_lab_payment_online;
    }

    public String getShow_lab_payment_lab() {
        return show_lab_payment_lab;
    }

    public void setShow_lab_payment_lab(String show_lab_payment_lab) {
        this.show_lab_payment_lab = show_lab_payment_lab;
    }

    public String getShow_lab_tele_booking() {
        return show_lab_tele_booking;
    }

    public void setShow_lab_tele_booking(String show_lab_tele_booking) {
        this.show_lab_tele_booking = show_lab_tele_booking;
    }

    public String getShow_lab_package_lab_price() {
        return show_lab_package_lab_price;
    }

    public void setShow_lab_package_lab_price(String show_lab_package_lab_price) {
        this.show_lab_package_lab_price = show_lab_package_lab_price;
    }

    public String getShow_lab_package_final_price() {
        return show_lab_package_final_price;
    }

    public void setShow_lab_package_final_price(String show_lab_package_final_price) {
        this.show_lab_package_final_price = show_lab_package_final_price;
    }

    public String show_lab_package_final_price;

    public String getShow_lab_package_id()
    {
        return show_lab_package_id;
    }

    public void setShow_lab_package_id(String show_lab_package_id)
    {
        this.show_lab_package_id = show_lab_package_id;
    }

    public String getShow_lab_package_user_name()
    {
        return show_lab_package_user_name;
    }

    public void setShow_lab_package_user_name(String show_lab_package_user_name)
    {
        this.show_lab_package_user_name = show_lab_package_user_name;
    }

    public String getShow_lab_package_email()
    {
        return show_lab_package_email;
    }

    public void setShow_lab_package_email(String show_lab_package_email)
    {
        this.show_lab_package_email = show_lab_package_email;
    }

    public String getShow_lab_package_mobile()
    {
        return show_lab_package_mobile;
    }

    public void setShow_lab_package_mobile(String show_lab_package_mobile)
    {
        this.show_lab_package_mobile = show_lab_package_mobile;
    }

    public String getShow_lab_package_role_id()
    {
        return show_lab_package_role_id;
    }

    public void setShow_lab_package_role_id(String show_lab_package_role_id)
    {
        this.show_lab_package_role_id = show_lab_package_role_id;
    }

    public String getShow_lab_package_created_time()
    {
        return show_lab_package_created_time;
    }

    public void setShow_lab_package_created_time(String show_lab_package_created_time)
    {
        this.show_lab_package_created_time = show_lab_package_created_time;
    }

    public String getShow_lab_package_workingdays()
    {
        return show_lab_package_workingdays;
    }

    public void setShow_lab_package_workingdays(String show_lab_package_workingdays)
    {
        this.show_lab_package_workingdays = show_lab_package_workingdays;
    }

    public String getShow_lab_package_address()
    {
        return show_lab_package_address;
    }

    public void setShow_lab_package_address(String show_lab_package_address)
    {
        this.show_lab_package_address = show_lab_package_address;
    }

    public String getShow_lab_package_workingtime()
    {
        return show_lab_package_workingtime;
    }

    public void setShow_lab_package_workingtime(String show_lab_package_workingtime)
    {
        this.show_lab_package_workingtime = show_lab_package_workingtime;
    }

    public String getShow_lab_package_lat()
    {
        return show_lab_package_lat;
    }

    public void setShow_lab_package_lat(String show_lab_package_lat)
    {
        this.show_lab_package_lat = show_lab_package_lat;
    }

    public String getShow_lab_package_lang()
    {
        return show_lab_package_lang;
    }

    public void setShow_lab_package_lang(String show_lab_package_lang)
    {
        this.show_lab_package_lang = show_lab_package_lang;
    }

    public String getShow_lab_package_home_collections()
    {
        return show_lab_package_home_collections;
    }

    public void setShow_lab_package_home_collections(String show_lab_package_home_collections)
    {
        this.show_lab_package_home_collections = show_lab_package_home_collections;
    }

    public String getShow_lab_package_test_id()
    {
        return show_lab_package_test_id;
    }

    public void setShow_lab_package_test_id(String show_lab_package_test_id)
    {
        this.show_lab_package_test_id = show_lab_package_test_id;
    }

    public String getShow_lab_package_package_id()
    {
        return show_lab_package_package_id;
    }

    public void setShow_lab_package_package_id(String show_lab_package_package_id)
    {
        this.show_lab_package_package_id = show_lab_package_package_id;
    }

    public String getShow_lab_package_distance()
    {
        return show_lab_package_distance;
    }

    public void setShow_lab_package_distance(String show_lab_package_distance)
    {
        this.show_lab_package_distance = show_lab_package_distance;
    }
}
