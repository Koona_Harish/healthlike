package innasoft.com.mhcone.models;

import java.io.Serializable;

/**
 * Created by User on 8/9/2016.
 */
public class SubSubPackagesModel implements Serializable
{
    public String sub_sub_pack_id;
    public String sub_pack_id;
    public String pack_id;
    public String sub_sub_pack_name;
    public String sub_pack_name;
    public String pack_name;
    public String sub_sub_pack_price;
    public String sub_sub_pack_status;

    public String getSub_sub_pack_id() {
        return sub_sub_pack_id;
    }

    public void setSub_sub_pack_id(String sub_sub_pack_id) {
        this.sub_sub_pack_id = sub_sub_pack_id;
    }

    public String getSub_pack_id() {
        return sub_pack_id;
    }

    public void setSub_pack_id(String sub_pack_id) {
        this.sub_pack_id = sub_pack_id;
    }

    public String getPack_id() {
        return pack_id;
    }

    public void setPack_id(String pack_id) {
        this.pack_id = pack_id;
    }

    public String getSub_sub_pack_name() {
        return sub_sub_pack_name;
    }

    public void setSub_sub_pack_name(String sub_sub_pack_name) {
        this.sub_sub_pack_name = sub_sub_pack_name;
    }

    public String getSub_pack_name() {
        return sub_pack_name;
    }

    public void setSub_pack_name(String sub_pack_name) {
        this.sub_pack_name = sub_pack_name;
    }

    public String getPack_name() {
        return pack_name;
    }

    public void setPack_name(String pack_name) {
        this.pack_name = pack_name;
    }

    public String getSub_sub_pack_price() {
        return sub_sub_pack_price;
    }

    public void setSub_sub_pack_price(String sub_sub_pack_price) {
        this.sub_sub_pack_price = sub_sub_pack_price;
    }

    public String getSub_sub_pack_status() {
        return sub_sub_pack_status;
    }

    public void setSub_sub_pack_status(String sub_sub_pack_status) {
        this.sub_sub_pack_status = sub_sub_pack_status;
    }
}
