package innasoft.com.mhcone.models;

import java.util.List;

/**
 * Created by Purushotham-Book on 9/28/2016.
 */
public class UpdateMyPackageTestsModel
{
    private String test_id;
    private String test_name;
    private String test_image;
    private List<UpdateMyPackageSubtestModel> subtestModel;

    public String getTest_id() {
        return test_id;
    }

    public void setTest_id(String test_id) {
        this.test_id = test_id;
    }

    public String getTest_name() {
        return test_name;
    }

    public void setTest_name(String test_name) {
        this.test_name = test_name;
    }

    public String getTest_image() {
        return test_image;
    }

    public void setTest_image(String test_image) {
        this.test_image = test_image;
    }

    public List<UpdateMyPackageSubtestModel> getSubtestModel() {
        return subtestModel;
    }

    public void setSubtestModel(List<UpdateMyPackageSubtestModel> subtestModel) {
        this.subtestModel = subtestModel;
    }
}
