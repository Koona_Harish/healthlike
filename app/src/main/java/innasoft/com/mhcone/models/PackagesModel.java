package innasoft.com.mhcone.models;

import java.io.Serializable;

/**
 * Created by User on 8/9/2016.
 */
public class PackagesModel implements Serializable
{
    public String p_id;
    public String p_name;
    public String P_status;
    public String package_id;

    public String getPackage_id() {
        return package_id;
    }

    public void setPackage_id(String package_id) {
        this.package_id = package_id;
    }

    public String getP_images_url() {
        return p_images_url;
    }

    public void setP_images_url(String p_images_url) {
        this.p_images_url = p_images_url;
    }

    public String p_images_url;

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getP_status() {
        return P_status;
    }

    public void setP_status(String p_status) {
        P_status = p_status;
    }
}
