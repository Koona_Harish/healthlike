package innasoft.com.mhcone.models;

import java.io.Serializable;

/**
 * Created by User on 8/9/2016.
 */
public class SubTestModel implements Serializable
{
    public String sub_test_id;
    public String test_id;
    public String sub_test_name;
    public String test_name;
    public String ischecked = "false";

    public SubTestModel() {
    }

    public SubTestModel(String sub_test_id, String sub_test_name, String ischecked) {
        this.sub_test_id = sub_test_id;
        this.sub_test_name = sub_test_name;
        this.ischecked = ischecked;
    }

    public String getIschecked() {
        return ischecked;
    }

    public void setIschecked(String ischecked) {
        this.ischecked = ischecked;
    }

    public String getSub_test_id() {
        return sub_test_id;
    }

    public void setSub_test_id(String sub_test_id) {
        this.sub_test_id = sub_test_id;
    }

    public String getTest_id() {
        return test_id;
    }

    public void setTest_id(String test_id) {
        this.test_id = test_id;
    }

    public String getSub_test_name() {
        return sub_test_name;
    }

    public void setSub_test_name(String sub_test_name) {
        this.sub_test_name = sub_test_name;
    }

    public String getTest_name() {
        return test_name;
    }

    public void setTest_name(String test_name) {
        this.test_name = test_name;
    }

    public String getSub_test_price() {
        return sub_test_price;
    }

    public void setSub_test_price(String sub_test_price) {
        this.sub_test_price = sub_test_price;
    }

    public String getSub_test_status() {
        return sub_test_status;
    }

    public void setSub_test_status(String sub_test_status) {
        this.sub_test_status = sub_test_status;
    }

    public String sub_test_price;
    public String sub_test_status;
}
