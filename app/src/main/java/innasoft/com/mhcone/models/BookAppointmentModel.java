package innasoft.com.mhcone.models;

/**
 * Created by androiddev2 on 23/9/16.
 */

public class BookAppointmentModel {

    public String show_lab_book_id;
    public String show_lab_book_user_name;
    public String show_lab_book_email;
    public String show_lab_book_mobile;
    public String show_lab_book_role_id;
    public String show_lab_book_created_time;
    public String show_lab_book_workingdays;
    public String show_lab_book_address;
    public String show_lab_book_workingtime;
    public String show_lab_book_lat;
    public String show_lab_book_lang;
    public String show_lab_book_home_collections;
    public String show_lab_book_distance;



    public String show_lab_ratting;
    public String show_lab_payment_online;
    public String show_lab_payment_lab;
    public String show_lab_tele_booking;

    public String profile_pic_url;

    public String certificate;
    public String location;


    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getProfile_pic_url() {
        return profile_pic_url;
    }

    public void setProfile_pic_url(String profile_pic_url) {
        this.profile_pic_url = profile_pic_url;
    }

    public String getShow_lab_ratting() {
        return show_lab_ratting;
    }

    public void setShow_lab_ratting(String show_lab_ratting) {
        this.show_lab_ratting = show_lab_ratting;
    }

    public String getShow_lab_payment_online() {
        return show_lab_payment_online;
    }

    public void setShow_lab_payment_online(String show_lab_payment_online) {
        this.show_lab_payment_online = show_lab_payment_online;
    }

    public String getShow_lab_payment_lab() {
        return show_lab_payment_lab;
    }

    public void setShow_lab_payment_lab(String show_lab_payment_lab) {
        this.show_lab_payment_lab = show_lab_payment_lab;
    }

    public String getShow_lab_tele_booking() {
        return show_lab_tele_booking;
    }

    public void setShow_lab_tele_booking(String show_lab_tele_booking) {
        this.show_lab_tele_booking = show_lab_tele_booking;
    }

    public String getShow_lab_book_id() {
        return show_lab_book_id;
    }

    public void setShow_lab_book_id(String show_lab_book_id) {
        this.show_lab_book_id = show_lab_book_id;
    }

    public String getShow_lab_book_user_name() {
        return show_lab_book_user_name;
    }

    public void setShow_lab_book_user_name(String show_lab_book_user_name) {
        this.show_lab_book_user_name = show_lab_book_user_name;
    }

    public String getShow_lab_book_email() {
        return show_lab_book_email;
    }

    public void setShow_lab_book_email(String show_lab_book_email) {
        this.show_lab_book_email = show_lab_book_email;
    }

    public String getShow_lab_book_mobile() {
        return show_lab_book_mobile;
    }

    public void setShow_lab_book_mobile(String show_lab_book_mobile) {
        this.show_lab_book_mobile = show_lab_book_mobile;
    }

    public String getShow_lab_book_role_id() {
        return show_lab_book_role_id;
    }

    public void setShow_lab_book_role_id(String show_lab_book_role_id) {
        this.show_lab_book_role_id = show_lab_book_role_id;
    }

    public String getShow_lab_book_created_time() {
        return show_lab_book_created_time;
    }

    public void setShow_lab_book_created_time(String show_lab_book_created_time) {
        this.show_lab_book_created_time = show_lab_book_created_time;
    }

    public String getShow_lab_book_workingdays() {
        return show_lab_book_workingdays;
    }

    public void setShow_lab_book_workingdays(String show_lab_book_workingdays) {
        this.show_lab_book_workingdays = show_lab_book_workingdays;
    }

    public String getShow_lab_book_address() {
        return show_lab_book_address;
    }

    public void setShow_lab_book_address(String show_lab_book_address) {
        this.show_lab_book_address = show_lab_book_address;
    }

    public String getShow_lab_book_workingtime() {
        return show_lab_book_workingtime;
    }

    public void setShow_lab_book_workingtime(String show_lab_book_workingtime) {
        this.show_lab_book_workingtime = show_lab_book_workingtime;
    }

    public String getShow_lab_book_lat() {
        return show_lab_book_lat;
    }

    public void setShow_lab_book_lat(String show_lab_book_lat) {
        this.show_lab_book_lat = show_lab_book_lat;
    }

    public String getShow_lab_book_lang() {
        return show_lab_book_lang;
    }

    public void setShow_lab_book_lang(String show_lab_book_lang) {
        this.show_lab_book_lang = show_lab_book_lang;
    }

    public String getShow_lab_book_home_collections() {
        return show_lab_book_home_collections;
    }

    public void setShow_lab_book_home_collections(String show_lab_book_home_collections) {
        this.show_lab_book_home_collections = show_lab_book_home_collections;
    }

    public String getShow_lab_book_distance() {
        return show_lab_book_distance;
    }

    public void setShow_lab_book_distance(String show_lab_book_distance) {
        this.show_lab_book_distance = show_lab_book_distance;
    }
}
