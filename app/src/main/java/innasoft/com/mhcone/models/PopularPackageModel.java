package innasoft.com.mhcone.models;

import java.io.Serializable;

public class PopularPackageModel //implements Serializable
{
    public String id;
    public String package_name;
    public String package_image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }

    public String getPackage_image() {
        return package_image;
    }

    public void setPackage_image(String package_image) {
        this.package_image = package_image;
    }
}
