package innasoft.com.mhcone.models;

/**
 * Created by admin on 12/18/2017.
 */

public class GetMemInGroupModel {
    public String id;
    public String name;
    public String sub_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSub_id() {
        return sub_id;
    }

    public void setSub_id(String sub_id) {
        this.sub_id = sub_id;
    }
}
