package innasoft.com.mhcone.models;

/**
 * Created by User on 8/22/2016.
 */
public class ViewUsersModel
{
        /*
        * "id": "10",
        "user_id": "4",
        "user_sub_name": "sri",
        "relation": "Friend",
        "age": "25",
        "gender": "male",
        "email": "sri@gmail.com",
        "mobile": "9030225696",
        "status": "1",
        "created_time": "2016-08-22 14:13:27"*/
    String vu_id;
    String vu_user_id;
    String vu_user_sub_name;
    String vu_relation;
    String vu_gender;
    String vu_email;
    String vu_mobile;
    String vu_status;
    String vu_age;

    public String getVu_dob() {
        return vu_dob;
    }

    public void setVu_dob(String vu_dob) {
        this.vu_dob = vu_dob;
    }

    public String getVu_created_time() {
        return vu_created_time;
    }

    public void setVu_created_time(String vu_created_time) {
        this.vu_created_time = vu_created_time;
    }

    String vu_dob;
    String vu_created_time;

    public String getVu_id() {
        return vu_id;
    }

    public void setVu_id(String vu_id) {
        this.vu_id = vu_id;
    }

    public String getVu_user_id() {
        return vu_user_id;
    }

    public void setVu_user_id(String vu_user_id) {
        this.vu_user_id = vu_user_id;
    }

    public String getVu_user_sub_name() {
        return vu_user_sub_name;
    }

    public void setVu_user_sub_name(String vu_user_sub_name) {
        this.vu_user_sub_name = vu_user_sub_name;
    }

    public String getVu_relation() {
        return vu_relation;
    }

    public void setVu_relation(String vu_relation) {
        this.vu_relation = vu_relation;
    }

    public String getVu_gender() {
        return vu_gender;
    }

    public void setVu_gender(String vu_gender) {
        this.vu_gender = vu_gender;
    }

    public String getVu_email() {
        return vu_email;
    }

    public void setVu_email(String vu_email) {
        this.vu_email = vu_email;
    }

    public String getVu_mobile() {
        return vu_mobile;
    }

    public void setVu_mobile(String vu_mobile) {
        this.vu_mobile = vu_mobile;
    }

    public String getVu_status() {
        return vu_status;
    }

    public void setVu_status(String vu_status) {
        this.vu_status = vu_status;
    }

    public String getVu_age() {
        return vu_age;
    }

    public void setVu_age(String vu_age) {
        this.vu_age = vu_age;
    }
}
