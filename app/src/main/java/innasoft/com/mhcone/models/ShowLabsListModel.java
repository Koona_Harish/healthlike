package innasoft.com.mhcone.models;


public class ShowLabsListModel {
    public String show_lab_list_id;
    public String show_lab_list_user_name;
    public String show_lab_list_email;
    public String show_lab_list_mobile;
    public String show_lab_list_role_id;
    public String show_lab_list_created_time;
    public String show_lab_list_workingdays;
    public String show_lab_list_address;
    public String show_lab_list_workingtime;
    public String show_lab_list_lat;
    public String show_lab_list_lang;
    public String show_lab_list_home_collections;
    public String show_lab_list_distance;

    public String profile_pic_url;

    public String show_lab_ratting;
    public String show_lab_payment_online;
    public String show_lab_payment_lab;
    public String show_lab_tele_booking;

    public String certificate;
    public String location;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getProfile_pic_url() {
        return profile_pic_url;
    }

    public void setProfile_pic_url(String profile_pic_url) {
        this.profile_pic_url = profile_pic_url;
    }

    public String getShow_lab_ratting() {
        return show_lab_ratting;
    }

    public void setShow_lab_ratting(String show_lab_ratting) {
        this.show_lab_ratting = show_lab_ratting;
    }

    public String getShow_lab_payment_online() {
        return show_lab_payment_online;
    }

    public void setShow_lab_payment_online(String show_lab_payment_online) {
        this.show_lab_payment_online = show_lab_payment_online;
    }

    public String getShow_lab_payment_lab() {
        return show_lab_payment_lab;
    }

    public void setShow_lab_payment_lab(String show_lab_payment_lab) {
        this.show_lab_payment_lab = show_lab_payment_lab;
    }

    public String getShow_lab_tele_booking() {
        return show_lab_tele_booking;
    }

    public void setShow_lab_tele_booking(String show_lab_tele_booking) {
        this.show_lab_tele_booking = show_lab_tele_booking;
    }

    public String getShow_lab_list_id() {
        return show_lab_list_id;
    }

    public void setShow_lab_list_id(String show_lab_list_id) {
        this.show_lab_list_id = show_lab_list_id;
    }

    public String getShow_lab_list_user_name() {
        return show_lab_list_user_name;
    }

    public void setShow_lab_list_user_name(String show_lab_list_user_name) {
        this.show_lab_list_user_name = show_lab_list_user_name;
    }

    public String getShow_lab_list_email() {
        return show_lab_list_email;
    }

    public void setShow_lab_list_email(String show_lab_list_email) {
        this.show_lab_list_email = show_lab_list_email;
    }

    public String getShow_lab_list_mobile() {
        return show_lab_list_mobile;
    }

    public void setShow_lab_list_mobile(String show_lab_list_mobile) {
        this.show_lab_list_mobile = show_lab_list_mobile;
    }

    public String getShow_lab_list_role_id() {
        return show_lab_list_role_id;
    }

    public void setShow_lab_list_role_id(String show_lab_list_role_id) {
        this.show_lab_list_role_id = show_lab_list_role_id;
    }

    public String getShow_lab_list_created_time() {
        return show_lab_list_created_time;
    }

    public void setShow_lab_list_created_time(String show_lab_list_created_time) {
        this.show_lab_list_created_time = show_lab_list_created_time;
    }

    public String getShow_lab_list_workingdays() {
        return show_lab_list_workingdays;
    }

    public void setShow_lab_list_workingdays(String show_lab_list_workingdays) {
        this.show_lab_list_workingdays = show_lab_list_workingdays;
    }

    public String getShow_lab_list_address() {
        return show_lab_list_address;
    }

    public void setShow_lab_list_address(String show_lab_list_address) {
        this.show_lab_list_address = show_lab_list_address;
    }

    public String getShow_lab_list_workingtime() {
        return show_lab_list_workingtime;
    }

    public void setShow_lab_list_workingtime(String show_lab_list_workingtime) {
        this.show_lab_list_workingtime = show_lab_list_workingtime;
    }

    public String getShow_lab_list_lat() {
        return show_lab_list_lat;
    }

    public void setShow_lab_list_lat(String show_lab_list_lat) {
        this.show_lab_list_lat = show_lab_list_lat;
    }

    public String getShow_lab_list_lang() {
        return show_lab_list_lang;
    }

    public void setShow_lab_list_lang(String show_lab_list_lang) {
        this.show_lab_list_lang = show_lab_list_lang;
    }

    public String getShow_lab_list_home_collections() {
        return show_lab_list_home_collections;
    }

    public void setShow_lab_list_home_collections(String show_lab_list_home_collections) {
        this.show_lab_list_home_collections = show_lab_list_home_collections;
    }

    public String getShow_lab_list_distance() {
        return show_lab_list_distance;
    }

    public void setShow_lab_list_distance(String show_lab_list_distance) {
        this.show_lab_list_distance = show_lab_list_distance;
    }
}
