package innasoft.com.mhcone.models;

/**
 * Created by Purushotham-Book on 9/28/2016.
 */
public class UpdateMyPackageSubtestModel
{
    private String test_id;
    private String sub_test_id;
    private String sub_test_name;
    private boolean isChecked;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getTest_id() {
        return test_id;
    }

    public void setTest_id(String test_id) {
        this.test_id = test_id;
    }

    public String getSub_test_id() {
        return sub_test_id;
    }

    public void setSub_test_id(String sub_test_id) {
        this.sub_test_id = sub_test_id;
    }

    public String getSub_test_name() {
        return sub_test_name;
    }

    public void setSub_test_name(String sub_test_name) {
        this.sub_test_name = sub_test_name;
    }
}
