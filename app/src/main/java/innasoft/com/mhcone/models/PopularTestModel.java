package innasoft.com.mhcone.models;

import java.io.Serializable;

public class PopularTestModel implements Serializable
{
    public String id;
    public String test_name;
    public String test_image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTest_name() {
        return test_name;
    }

    public void setTest_name(String test_name) {
        this.test_name = test_name;
    }

    public String getTest_image() {
        return test_image;
    }

    public void setTest_image(String test_image) {
        this.test_image = test_image;
    }
}
