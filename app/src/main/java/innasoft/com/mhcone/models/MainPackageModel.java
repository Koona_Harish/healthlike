package innasoft.com.mhcone.models;

import java.util.List;

/**
 * Created by Purushotham-Book on 9/28/2016.
 */
public class MainPackageModel
{
    private String package_sub_id;
    private String package_sub_name;
    private String test_image;
    private List<MainSubPackageModel> subPackageModel;

    public String getPackage_sub_id() {
        return package_sub_id;
    }

    public void setPackage_sub_id(String package_sub_id) {
        this.package_sub_id = package_sub_id;
    }

    public String getPackage_sub_name() {
        return package_sub_name;
    }

    public void setPackage_sub_name(String package_sub_name) {
        this.package_sub_name = package_sub_name;
    }

    public String getTest_image() {
        return test_image;
    }

    public void setTest_image(String test_image) {
        this.test_image = test_image;
    }

    public List<MainSubPackageModel> getSubPackageModel() {
        return subPackageModel;
    }

    public void setSubPackageModel(List<MainSubPackageModel> subPackageModel) {
        this.subPackageModel = subPackageModel;
    }
}
