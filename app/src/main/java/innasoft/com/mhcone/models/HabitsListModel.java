package innasoft.com.mhcone.models;

import java.io.Serializable;

/**
 * Created by User on 9/14/2016.
 */
public class HabitsListModel implements Serializable
{
    public String habitsName;
    public String habitsDescription;
    public String habitsImage;
    public String habitsCategory;
    public String habitsCategory_id;
    public String user_id;
    public String status;
    public String created_time;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    public String getHabit_id() {
        return habit_id;
    }

    public void setHabit_id(String habit_id) {
        this.habit_id = habit_id;
    }

    public String habit_id;



    public String getHabitsCategory_id() {
        return habitsCategory_id;
    }

    public void setHabitsCategory_id(String habitsCategory_id) {
        this.habitsCategory_id = habitsCategory_id;
    }

    public String getHabitsCategory() {
        return habitsCategory;
    }

    public void setHabitsCategory(String habitsCategory) {
        this.habitsCategory = habitsCategory;
    }

    public String getHabitsImage() {
        return habitsImage;
    }

    public void setHabitsImage(String habitsImage) {
        this.habitsImage = habitsImage;
    }

    public String getHabitsName() {
        return habitsName;
    }

    public void setHabitsName(String habitsName) {
        this.habitsName = habitsName;
    }

    public String getHabitsDescription() {
        return habitsDescription;
    }

    public void setHabitsDescription(String habitsDescription) {
        this.habitsDescription = habitsDescription;
    }


}
