package innasoft.com.mhcone.models;

import java.io.Serializable;

public class PopularLabModel implements Serializable
{
    public String id;
    public String user_name;
    public String lat;
    public String lang;
    public String mobile;
    public String payment_online;
    public String payment_lab;
    public String tele_booking;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPayment_online() {
        return payment_online;
    }

    public void setPayment_online(String payment_online) {
        this.payment_online = payment_online;
    }

    public String getPayment_lab() {
        return payment_lab;
    }

    public void setPayment_lab(String payment_lab) {
        this.payment_lab = payment_lab;
    }

    public String getTele_booking() {
        return tele_booking;
    }

    public void setTele_booking(String tele_booking) {
        this.tele_booking = tele_booking;
    }

    public String profile_pic;


    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }
}
