package innasoft.com.mhcone.models;

import java.io.Serializable;

/**
 * Created by purushotham on 17/10/16.
 */

public class UserPackageNamesModel implements Serializable
{
    public String user_package_id;
    public String user_user_id;
    public String user_package_name;
    public String status;
    public String created_time;

    public String getUser_package_id() {
        return user_package_id;
    }

    public void setUser_package_id(String user_package_id) {
        this.user_package_id = user_package_id;
    }

    public String getUser_user_id() {
        return user_user_id;
    }

    public void setUser_user_id(String user_user_id) {
        this.user_user_id = user_user_id;
    }

    public String getUser_package_name() {
        return user_package_name;
    }

    public void setUser_package_name(String user_package_name) {
        this.user_package_name = user_package_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }
}
