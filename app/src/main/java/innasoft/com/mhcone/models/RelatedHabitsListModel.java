package innasoft.com.mhcone.models;

import java.io.Serializable;

/**
 * Created by androiddev2 on 23/11/16.
 */

public class RelatedHabitsListModel implements Serializable {
    public String relatedhabitsName;
    public String relatedhabitsDescription;
    public String relatedhabitsImage;
    public String relatedhabitsCategory;
    public String relatedhabitsCategory_id;
    public String reHabitId;

    public String getReHabitId() {
        return reHabitId;
    }

    public void setReHabitId(String reHabitId) {
        this.reHabitId = reHabitId;
    }

    public String getRelatedhabitsName() {
        return relatedhabitsName;
    }

    public void setRelatedhabitsName(String relatedhabitsName) {
        this.relatedhabitsName = relatedhabitsName;
    }

    public String getRelatedhabitsDescription() {
        return relatedhabitsDescription;
    }

    public void setRelatedhabitsDescription(String relatedhabitsDescription) {
        this.relatedhabitsDescription = relatedhabitsDescription;
    }

    public String getRelatedhabitsImage() {
        return relatedhabitsImage;
    }

    public void setRelatedhabitsImage(String relatedhabitsImage) {
        this.relatedhabitsImage = relatedhabitsImage;
    }

    public String getRelatedhabitsCategory() {
        return relatedhabitsCategory;
    }

    public void setRelatedhabitsCategory(String relatedhabitsCategory) {
        this.relatedhabitsCategory = relatedhabitsCategory;
    }

    public String getRelatedhabitsCategory_id() {
        return relatedhabitsCategory_id;
    }

    public void setRelatedhabitsCategory_id(String relatedhabitsCategory_id) {
        this.relatedhabitsCategory_id = relatedhabitsCategory_id;
    }
}
