package innasoft.com.mhcone.models;

public class LabPackagesModel
{

    public String lab_packages_id;
    public String lab_packages_package_id;
    public String lab_packages_test_id;
    public String lab_packages_sub_test_id;
    public String lab_packages_final_price;
    public String lab_packages_package_name;
    public String lab_packages_sub_test;
    public String lab_packages_package_image;
    public String lab_packages_lab_price;

    public String getLab_packages_lab_price() {
        return lab_packages_lab_price;
    }

    public void setLab_packages_lab_price(String lab_packages_lab_price) {
        this.lab_packages_lab_price = lab_packages_lab_price;
    }

    public String getLab_packages_package_image() {
        return lab_packages_package_image;
    }

    public void setLab_packages_package_image(String lab_packages_package_image) {
        this.lab_packages_package_image = lab_packages_package_image;
    }


    public String getLab_packages_id()
    {
        return lab_packages_id;
    }

    public void setLab_packages_id(String lab_packages_id)
    {
        this.lab_packages_id = lab_packages_id;
    }

    public String getLab_packages_package_id()
    {
        return lab_packages_package_id;
    }

    public void setLab_packages_package_id(String lab_packages_package_id)
    {
        this.lab_packages_package_id = lab_packages_package_id;
    }

    public String getLab_packages_test_id()
    {
        return lab_packages_test_id;
    }

    public void setLab_packages_test_id(String lab_packages_test_id)
    {
        this.lab_packages_test_id = lab_packages_test_id;
    }

    public String getLab_packages_sub_test_id()
    {
        return lab_packages_sub_test_id;
    }

    public void setLab_packages_sub_test_id(String lab_packages_sub_test_id)
    {
        this.lab_packages_sub_test_id = lab_packages_sub_test_id;
    }

    public String getLab_packages_final_price()
    {
        return lab_packages_final_price;
    }

    public void setLab_packages_final_price(String lab_packages_final_price)
    {
        this.lab_packages_final_price = lab_packages_final_price;
    }

    public String getLab_packages_package_name()
    {
        return lab_packages_package_name;
    }

    public void setLab_packages_package_name(String lab_packages_package_name)
    {
        this.lab_packages_package_name = lab_packages_package_name;
    }

    public String getLab_packages_sub_test()
    {
        return lab_packages_sub_test;
    }

    public void setLab_packages_sub_test(String lab_packages_sub_test)
    {
        this.lab_packages_sub_test = lab_packages_sub_test;
    }
}
