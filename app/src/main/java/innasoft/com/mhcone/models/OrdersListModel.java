package innasoft.com.mhcone.models;

public class OrdersListModel {

    public String orders_list_id;
    public String orders_list_lab_name;
    public String orders_list_bookingdate;
    public String orders_list_status;
    public String orders_list_bookeddate;
    public String order_list_price;
    public String order_list_order_type;
    public String orders_list_booking_id;
    public String orders_list_user_id;
    public String orders_list_lab_id;
    public String payment_type;


    public String getOrders_list_booking_id() {
        return orders_list_booking_id;
    }

    public void setOrders_list_booking_id(String orders_list_booking_id) {
        this.orders_list_booking_id = orders_list_booking_id;
    }

    public String getOrders_list_user_id() {
        return orders_list_user_id;
    }

    public void setOrders_list_user_id(String orders_list_user_id) {
        this.orders_list_user_id = orders_list_user_id;
    }

    public String getOrders_list_lab_id() {
        return orders_list_lab_id;
    }

    public void setOrders_list_lab_id(String orders_list_lab_id) {
        this.orders_list_lab_id = orders_list_lab_id;
    }


    public String orders_list_btn;

    public String getOrder_list_order_type() {
        return order_list_order_type;
    }

    public void setOrder_list_order_type(String order_list_order_type) {
        this.order_list_order_type = order_list_order_type;
    }

    public String getOrder_list_price() {
        return order_list_price;
    }

    public void setOrder_list_price(String order_list_price) {
        this.order_list_price = order_list_price;
    }


    public String getOrders_list_btn() {
        return orders_list_btn;
    }

    public void setOrders_list_btn(String orders_list_btn) {
        this.orders_list_btn = orders_list_btn;
    }

    public String getOrders_list_id() {
        return orders_list_id;
    }

    public void setOrders_list_id(String orders_list_id) {
        this.orders_list_id = orders_list_id;
    }

    public String getOrders_list_lab_name() {
        return orders_list_lab_name;
    }

    public void setOrders_list_lab_name(String orders_list_lab_name) {
        this.orders_list_lab_name = orders_list_lab_name;
    }

    public String getOrders_list_bookingdate() {
        return orders_list_bookingdate;
    }

    public void setOrders_list_bookingdate(String orders_list_bookingdate) {
        this.orders_list_bookingdate = orders_list_bookingdate;
    }

    public String getOrders_list_status() {
        return orders_list_status;
    }

    public void setOrders_list_status(String orders_list_status) {
        this.orders_list_status = orders_list_status;
    }

    public String getOrders_list_bookeddate() {
        return orders_list_bookeddate;
    }

    public void setOrders_list_bookeddate(String orders_list_bookeddate) {
        this.orders_list_bookeddate = orders_list_bookeddate;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }
}
