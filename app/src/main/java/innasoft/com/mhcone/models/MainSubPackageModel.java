package innasoft.com.mhcone.models;

/**
 * Created by Purushotham-Book on 9/28/2016.
 */
public class MainSubPackageModel
{
    private String package_id;
    private String package_sub_id;
    private String sub_package_id;
    private String sub_package_name;

    public String getPackage_id() {
        return package_id;
    }

    public void setPackage_id(String package_id) {
        this.package_id = package_id;
    }

    public String getPackage_sub_id() {
        return package_sub_id;
    }

    public void setPackage_sub_id(String package_sub_id) {
        this.package_sub_id = package_sub_id;
    }

    public String getSub_package_id() {
        return sub_package_id;
    }

    public void setSub_package_id(String sub_package_id) {
        this.sub_package_id = sub_package_id;
    }

    public String getSub_package_name() {
        return sub_package_name;
    }

    public void setSub_package_name(String sub_package_name) {
        this.sub_package_name = sub_package_name;
    }
}
