package innasoft.com.mhcone.models;

/**
 * Created by purushotham on 17/10/16.
 */

public class UserSubPackageModel
{
    public String user_sub_package_test_id;
    public String user_package_id;
    public String test_id;
    public String sub_test_id;
    public String status;
    public String created_time;
    public String package_name;
    public String sub_test_name;

    public String getUser_sub_package_test_id() {
        return user_sub_package_test_id;
    }

    public void setUser_sub_package_test_id(String user_sub_package_test_id) {
        this.user_sub_package_test_id = user_sub_package_test_id;
    }

    public String getUser_package_id() {
        return user_package_id;
    }

    public void setUser_package_id(String user_package_id) {
        this.user_package_id = user_package_id;
    }

    public String getTest_id() {
        return test_id;
    }

    public void setTest_id(String test_id) {
        this.test_id = test_id;
    }

    public String getSub_test_id() {
        return sub_test_id;
    }

    public void setSub_test_id(String sub_test_id) {
        this.sub_test_id = sub_test_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }

    public String getSub_test_name() {
        return sub_test_name;
    }

    public void setSub_test_name(String sub_test_name) {
        this.sub_test_name = sub_test_name;
    }
}
