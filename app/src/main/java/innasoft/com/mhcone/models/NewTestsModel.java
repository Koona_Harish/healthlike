package innasoft.com.mhcone.models;

import java.io.Serializable;

/**
 * Created by User on 8/9/2016.
 */
public class NewTestsModel implements Serializable
{
    public String id;
    public String test_id;
    public String test_sub_name;
    public String home_collection_status;
    public String status;
    public String created_time;
    public String updated_time;
    public String test_name;
    public String test_image;
    public boolean ischecked;
    public boolean isIschecked() {
        return ischecked;
    }

    public void setIschecked(boolean ischecked) {
        this.ischecked = ischecked;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTest_id() {
        return test_id;
    }

    public void setTest_id(String test_id) {
        this.test_id = test_id;
    }

    public String getTest_sub_name() {
        return test_sub_name;
    }

    public void setTest_sub_name(String test_sub_name) {
        this.test_sub_name = test_sub_name;
    }

    public String getHome_collection_status() {
        return home_collection_status;
    }

    public void setHome_collection_status(String home_collection_status) {
        this.home_collection_status = home_collection_status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    public String getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(String updated_time) {
        this.updated_time = updated_time;
    }

    public String getTest_name() {
        return test_name;
    }

    public void setTest_name(String test_name) {
        this.test_name = test_name;
    }

    public String getTest_image() {
        return test_image;
    }

    public void setTest_image(String test_image) {
        this.test_image = test_image;
    }
}
