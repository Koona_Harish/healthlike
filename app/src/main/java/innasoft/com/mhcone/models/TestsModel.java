package innasoft.com.mhcone.models;

import java.io.Serializable;

/**
 * Created by User on 8/8/2016.
 */
public class TestsModel implements Serializable {
    public String test_id;
    public String test_image;

    public String getTest_image() {
        return test_image;
    }

    public void setTest_image(String test_image) {
        this.test_image = test_image;
    }

    public String getTest_id() {
        return test_id;
    }

    public void setTest_id(String test_id) {
        this.test_id = test_id;
    }

    public String getTest_name() {
        return test_name;
    }

    public void setTest_name(String test_name) {
        this.test_name = test_name;
    }

    public String getTest_status() {
        return test_status;
    }

    public void setTest_status(String test_status) {
        this.test_status = test_status;
    }

    public String test_name;
    public String test_status;


}
