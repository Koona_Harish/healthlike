package innasoft.com.mhcone.models;

import java.io.Serializable;

/**
 * Created by User on 8/9/2016.
 */
public class SubPackagesModel implements Serializable
{
    public String sub_package_id;
    public String sub_package_name;
    public String sub_package_status;
    public String package_id;
    public String package_name;
    public String test_id;
    public String sub_test_id;

    public String getTest_id() {
        return test_id;
    }

    public void setTest_id(String test_id) {
        this.test_id = test_id;
    }

    public String getSub_test_id() {
        return sub_test_id;
    }

    public void setSub_test_id(String sub_test_id) {
        this.sub_test_id = sub_test_id;
    }

    public String getSub_package_id() {
        return sub_package_id;
    }

    public void setSub_package_id(String sub_package_id) {
        this.sub_package_id = sub_package_id;
    }

    public String getSub_package_name() {
        return sub_package_name;
    }

    public void setSub_package_name(String sub_package_name) {
        this.sub_package_name = sub_package_name;
    }

    public String getSub_package_status() {
        return sub_package_status;
    }

    public void setSub_package_status(String sub_package_status) {
        this.sub_package_status = sub_package_status;
    }

    public String getPackage_id() {
        return package_id;
    }

    public void setPackage_id(String package_id) {
        this.package_id = package_id;
    }

    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }
}
